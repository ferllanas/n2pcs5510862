//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PAdIssueID_h__
#define __N2PAdIssueID_h__

#include "SDKDef.h"


// Company:
#define kN2PAdIssueCompanyKey	"Interlasa"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PAdIssueCompanyValue	"Interlasa"	// Company name displayed externally.

// Plug-in:
#define kN2PAdIssuePluginName	"N2PAdornmentIssue"			// Name of this plug-in.
#define kN2PAdIssuePrefixNumber	0x96E00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PAdIssueVersion		"v1.0.8.4 sd"						// Version of this plug-in (for the About Box).
#define kN2PAdIssueAuthor		"Interlasa.com"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PAdIssuePrefixNumber above to modify the prefix.)
#define kN2PAdIssuePrefix		RezLong(kN2PAdIssuePrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PAdIssueStringPrefix	SDK_DEF_STRINGIZE(kN2PAdIssuePrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PAdIssueMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PAdIssueMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PAdIssuePluginID, kN2PAdIssuePrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2PAdIssueActionComponentBoss, kN2PAdIssuePrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2PAdIssuePanelWidgetBoss, kN2PAdIssuePrefix + 1)
DECLARE_PMID(kClassIDSpace, kN2PAdIssueDialogBoss, kN2PAdIssuePrefix + 2)
DECLARE_PMID(kClassIDSpace, kFrmLblAdornmentBoss, kN2PAdIssuePrefix + 3)
DECLARE_PMID(kClassIDSpace, kFrmLblCmdBoss, kN2PAdIssuePrefix + 4)
DECLARE_PMID(kClassIDSpace, kN2PAdIssueUtilsBoss, kN2PAdIssuePrefix + 5)
DECLARE_PMID(kClassIDSpace, kN2PSqlPenUnlockAdornmentBoss, kN2PAdIssuePrefix + 6)
DECLARE_PMID(kClassIDSpace, kN2PSqlPenUnlockCmdBoss, kN2PAdIssuePrefix + 7)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 8)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 9)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 10)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 11)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 12)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 13)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 14)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 15)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 16)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 17)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 18)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 19)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 20)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 21)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 22)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 23)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 24)
//DECLARE_PMID(kClassIDSpace, kN2PAdIssueBoss, kN2PAdIssuePrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IFRMLBLDATA, kN2PAdIssuePrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEUTILS, kN2PAdIssuePrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQLPenUnLockDATA, kN2PAdIssuePrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PADISSUEINTERFACE, kN2PAdIssuePrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueActionComponentImpl, kN2PAdIssuePrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueDialogControllerImpl, kN2PAdIssuePrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueDialogObserverImpl, kN2PAdIssuePrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataImpl, kN2PAdIssuePrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kFrmLblAdornmentImpl, kN2PAdIssuePrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kFrmLblCmdImpl, kN2PAdIssuePrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueUtilsImpl, kN2PAdIssuePrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kN2PSqlPenUnLockDataImpl, kN2PAdIssuePrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kN2PSqlPenUnLockCmdImpl, kN2PAdIssuePrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kN2PSqlPenUnlockAdornmentImpl, kN2PAdIssuePrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kN2PAdIssueImpl, kN2PAdIssuePrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2PAdIssueAboutActionID, kN2PAdIssuePrefix + 0)
DECLARE_PMID(kActionIDSpace, kN2PAdIssuePanelWidgetActionID, kN2PAdIssuePrefix + 1)
DECLARE_PMID(kActionIDSpace, kN2PAdIssueSeparator1ActionID, kN2PAdIssuePrefix + 2)
DECLARE_PMID(kActionIDSpace, kN2PAdIssuePopupAboutThisActionID, kN2PAdIssuePrefix + 3)
DECLARE_PMID(kActionIDSpace, kN2PAdIssueDialogActionID, kN2PAdIssuePrefix + 4)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 5)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 6)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 7)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 8)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 9)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 10)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 11)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 12)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 13)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 14)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 15)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 16)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 17)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 18)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 19)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 20)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 21)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 22)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 23)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 24)
//DECLARE_PMID(kActionIDSpace, kN2PAdIssueActionID, kN2PAdIssuePrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kN2PAdIssuePanelWidgetID, kN2PAdIssuePrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueDialogWidgetID, kN2PAdIssuePrefix + 1)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kN2PAdIssueWidgetID, kN2PAdIssuePrefix + 25)


// "About Plug-ins" sub-menu:
#define kN2PAdIssueAboutMenuKey			kN2PAdIssueStringPrefix "kN2PAdIssueAboutMenuKey"
#define kN2PAdIssueAboutMenuPath		kSDKDefStandardAboutMenuPath kN2PAdIssueCompanyKey

// "Plug-ins" sub-menu:
#define kN2PAdIssuePluginsMenuKey 		kN2PAdIssueStringPrefix "kN2PAdIssuePluginsMenuKey"
#define kN2PAdIssuePluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PAdIssueCompanyKey kSDKDefDelimitMenuPath kN2PAdIssuePluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kN2PAdIssueAboutBoxStringKey	kN2PAdIssueStringPrefix "kN2PAdIssueAboutBoxStringKey"
#define kN2PAdIssuePanelTitleKey					kN2PAdIssueStringPrefix	"kN2PAdIssuePanelTitleKey"
#define kN2PAdIssueStaticTextKey kN2PAdIssueStringPrefix	"kN2PAdIssueStaticTextKey"
#define kN2PAdIssueInternalPopupMenuNameKey kN2PAdIssueStringPrefix	"kN2PAdIssueInternalPopupMenuNameKey"
#define kN2PAdIssueTargetMenuPath kN2PAdIssueInternalPopupMenuNameKey

// Menu item positions:

#define	kN2PAdIssueSeparator1MenuItemPosition		10.0
#define kN2PAdIssueAboutThisMenuItemPosition		11.0

#define kN2PAdIssueDialogTitleKey kN2PAdIssueStringPrefix "kN2PAdIssueDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kN2PAdIssueDialogMenuItemKey kN2PAdIssueStringPrefix "kN2PAdIssueDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kN2PAdIssueDialogMenuItemPosition	12.0

#define kFrmLblCmdStringKey kN2PAdIssueStringPrefix "kFrmLblCmdStringKey"
#define kN2PSqlPenUnLockCmdUndoKey kN2PAdIssueStringPrefix "kN2PSqlPenUnLockCmdUndoKey"

// Initial data format version numbers
#define kN2PAdIssueFirstMajorFormatNumber  RezLong(1)
#define kN2PAdIssueFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PAdIssueCurrentMajorFormatNumber kN2PAdIssueFirstMajorFormatNumber
#define kN2PAdIssueCurrentMinorFormatNumber kN2PAdIssueFirstMinorFormatNumber

// Schema field IDs:
#define kFrmLblLabel 0
#define kFrmLblLabelWidth 1
#define kFrmLblSize 2
#define kFrmLblVisibility 3

// other constants
#define kFrmLblDefaultLabel ""
#define kFrmLblDefaultWidth 0
#define kFrmLblDefaultPointSize 12
#define kFrmLblDefaultVisibility kFalse

#endif // __N2PAdIssueID_h__

//  Code generated by DollyXs code generator
