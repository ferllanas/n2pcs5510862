//========================================================================================
//  
//  $File: //depot/indesign_5.0/highprofile/source/sdksamples/framelabel/N2PSqlPenUnLockCmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:27:55 $
//  
//  $Revision: #1 $
//  
//  $Change: 505962 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
// None.

// General includes:
#include "Command.h"
#include "ErrorUtils.h"
#include "IPageItemUtils.h"	 

// Project includes:
#include "IN2PSqlPenUnLockData.h"
#include "N2PAdIssueID.h"

/** Stores persistent data to control the display of
	the frame label adornment; implements ICommand.

	@ingroup framelabel
*/
class N2PSqlPenUnLockCmd : public Command
{
	public:
		/** Constructor.
		 * 	We initialize all the private members and pass the 
		 * 	boss pointer to the parent class (Command).
		 * 	@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSqlPenUnLockCmd(IPMUnknown* boss);
		
		/** Destructor 
		 */
		virtual ~N2PSqlPenUnLockCmd(void);

		/**	See Command::LowMemIsOk
		 * 	@return bool16 
		 */
		bool16 LowMemIsOK(void) const 
		{ return kFalse; }


	protected:

		/**	Execute the main behaviour of the command.
		 * 	The Do method is where all the action happens. In this case we just
		 * 	need to copy the information from the commands data interface to the 
		 * 	frames data interface.
		 * 	@see Command::Do
		 */
		virtual void Do(void);

		/**	Notify interested observers.
		 * 	In our case, we broadcast a change at the document level.
		 * 	@see Command::DoNotify 
		 */
		virtual void DoNotify(void);

		/** Report the name of the command
		 * 	@see Command::CreateName 
		*/
		virtual PMString* CreateName(void);

	private:
		
		int32*		fPreviouslockOnUnLock;
		bool16*		fPreviousVisibility;
		
		/** The size of the property arrays */
		int32		fItemCount;	
};

CREATE_PMINTERFACE(N2PSqlPenUnLockCmd, kN2PSqlPenUnLockCmdImpl)

/*	Constructor
*/
N2PSqlPenUnLockCmd::N2PSqlPenUnLockCmd(IPMUnknown* boss) :
	Command(boss), 
	fPreviouslockOnUnLock(nil), 
	fPreviousVisibility(nil), 
	fItemCount(0)
{
}

/* Destructor
*/
N2PSqlPenUnLockCmd::~N2PSqlPenUnLockCmd(void)
{
	delete [] fPreviouslockOnUnLock;
	delete [] fPreviousVisibility;
}

/* Do
*/
void N2PSqlPenUnLockCmd::Do(void)
{
	// Verify the length of the item list
	fItemCount = fItemList.Length();
	
	if (fItemCount == 0)
	{
		ASSERT_FAIL("N2PSqlPenUnLockCmd::Do: empty item list");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return;
	}
	
	fPreviouslockOnUnLock = new int32[fItemCount];
	fPreviousVisibility = new bool16[fItemCount];
	
	InterfacePtr<IN2PSqlPenUnLockData> commandData(this, UseDefaultIID());
	if (commandData == nil)
	{
		ASSERT_FAIL("LabelFrame::Do cannot get IFrmLblData for the command");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
	}
	else
	{		
		// Walk through all of the items in the list and change the IFrmLblData information
		// for each one:
		for(int32 frameCount = 0; frameCount < fItemCount; frameCount++)
		{
			InterfacePtr<IN2PSqlPenUnLockData> frameData(fItemList.GetRef(frameCount), UseDefaultIID());
			if(frameData != nil)
			{
				// Change the settings for the current page item:
				fPreviouslockOnUnLock[frameCount] = frameData->GetLockOrUnlock();
				fPreviousVisibility[frameCount] = frameData->GetVisibility();
				
				// Change the settings for the current page item:
				frameData->Set
				(
					commandData->GetLockOrUnlock(), 
					commandData->GetVisibility()
				);
			}	
		}
	}
}

/*	DoNotify
*/
void N2PSqlPenUnLockCmd::DoNotify(void)
{
	if (fItemCount == 0)
	{
		ASSERT_FAIL("N2PSqlPenUnLockCmd::DoNotify: empty item list");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
	}
	else
	{
		// For notfication we'll let PageItemUtils do the work.
		// We have to choose some type of change message, so use 
		// a location change to cause a redraw.
		Utils<IPageItemUtils>()->NotifyDocumentObservers
		(
			fItemList.GetDataBase(), 
			kLocationChangedMessage, 
			IID_ITRANSFORM_DOCUMENT, 
			this,
			nil /* nil cookie */
		);
	}
}

/*	CreateName
*/
PMString* N2PSqlPenUnLockCmd::CreateName(void)
{
	// Core resource for string:
	PMString* string = new PMString(kN2PSqlPenUnLockCmdUndoKey);
	return string;
}

// End, N2PSqlPenUnLockCmd.cpp.






