//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/framelabel/FrmLblDataSuiteASB.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "CPMUnknown.h"

// ----- Interfaces files

//#include "IIntegratorTarget.h"
#include "IActiveContext.h"
#include "ISelectionManager.h"
#include "ILayoutTarget.h"
#include "IPageItemAdornmentList.h"
#include "IClassIDData.h"
#include "IConcreteSelection.h"
#include "ISelectionUtils.h"

// ----- ID files
#include "CAlert.h"
#include "IN2PAdIssueUtils.h"
#include "N2PadIssueID.h"
#include "IFrmLblData.h"
#include "IN2PSqlPenUnLockData.h"


#include "SelectionASBTemplates.tpp"	// for make_functor

/**
	Abstract selection class implementation of IFrmLblDataSuite.
	@ingroup framelabel

*/
class N2PAdIssueUtils : public CPMUnknown<IN2PAdIssueUtils>
{
	public:
		/** Constructor
		@param boss
		*/
		N2PAdIssueUtils (IPMUnknown* boss);
		  

		/**	Description
			@param none
			@return ~FrmLblDataSuiteASB 
		 */
		virtual  ~N2PAdIssueUtils () {};


		/**	See IFrmLblDataSuite::GetFrameLabelAndVisibility
			@param label 
			@param visible 
		 */
		virtual void GetFrameLabelAndVisibility(PMString& label, bool16& visible);

		void GetFrameLabelAndVisibilityUIDListLabelfor(UIDList& selectUIDList,PMString& theLabel, bool16& visibility);

		/**	See IFrmLblDataSuite::UpdateFrameLabel
			@param label 
			@param visible 
			@return ErrorCode 
		 */
		virtual ErrorCode UpdateFrameLabel(const PMString& label, const bool16& visible);
		
		
		virtual ErrorCode UpdateUIDListLabelfor(UIDList& ListUId,const PMString& label, const bool16& visible);
		
		
		virtual void GetVisibilityOfPenUnLockofUIDListLabelfor(UIDList& selectUIDList, bool16& visibility);

		ErrorCode UpdateUIDListLabelforPenUnLock(UIDList& selectUIDList, const int32& LockUnLock, const bool16& visibility);
};

CREATE_PMINTERFACE (N2PAdIssueUtils, kN2PAdIssueUtilsImpl)


N2PAdIssueUtils::N2PAdIssueUtils(IPMUnknown* boss) :
		CPMUnknown<IN2PAdIssueUtils>(boss)
{
}



/**
	Get the frame label and its visibility of the selected page items.
*/
void N2PAdIssueUtils::GetFrameLabelAndVisibility(PMString& theLabel, bool16& visibility)
{
	do
	{
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL("iSelectionUtils");
			break;
		}

		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL("iSelectionManager");
			break;
		}
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			ASSERT_FAIL("pTextSel");
			break;
		}
	
		InterfacePtr<ILayoutTarget>	iLayoutTarget (pTextSel, UseDefaultIID ());
		if(iLayoutTarget==nil)
		{
			
			break;
		}
		UIDList						targetItemList = iLayoutTarget->GetUIDList (kStripStandoffs);

		// --- Go through each item in the list and filter out those non-guide items.
		//     We assume that we will never get in a mixed guide/non-guide selection. 
		for (int32 i = targetItemList.Length () - 1; i >= 0; i--)
		{
			UIDRef item = targetItemList.GetRef (i);

			InterfacePtr<IFrmLblData> frameData(item, UseDefaultIID());
			if (frameData != nil)
			{
				theLabel = frameData->GetString();
				visibility = frameData->GetVisibility();
				break;
			}
		}
	}while(false);
	
}



/**
	update frame labels for selected page items.
*/
ErrorCode N2PAdIssueUtils::UpdateFrameLabel(const PMString& theLabel, const bool16& visibility)	
{
	ErrorCode error;
	do
	{
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			
			break;
		}

		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			
			break;
		}
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			
			break;
		}
	
		InterfacePtr<ILayoutTarget>	iLayoutTarget (pTextSel, UseDefaultIID ());
		if(iLayoutTarget==nil)
		{
			
			break;
		}
		UIDList						selectUIDList = iLayoutTarget->GetUIDList (kStripStandoffs);
		
		if (selectUIDList.Length() < 1)
		{
			// How did we get here?  If we have a selection but no items something's
			// really messed up:
			ASSERT_FAIL("FrameLabelDlgController::ApplyFields: UIDList invalid");
			break;
		}
		
		// The selection list can have items in it that will not have an IFrmLblData interface
		// so those will be removed from the selection list before the LabelFrameCmd is sent.
		// Walking backwards through the list makes removing items easier.
		int32 item = selectUIDList.Length();
		while (item-- > 0)
		{
			InterfacePtr<IFrmLblData> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}
		// If there are no items left in the list, then report an error and leave.
		if (selectUIDList.Length() == 0)
		{
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> labelCommand(CmdUtils::CreateCommand(kFrmLblCmdBoss));
		if (labelCommand == nil)
		{
			// For some reason we couldn't get our label command pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("FrameLabelDlgController::ApplyFields: labelCommand invalid");
			break;
		}

		labelCommand->SetItemList(selectUIDList);

		InterfacePtr<IFrmLblData> labelData(labelCommand, UseDefaultIID());		
		if (labelData == nil)
		{
			// For some reason we couldn't get our label data pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("FrameLabelDlgController::ApplyFields: labelData invalid");
			break;
		}
		// Text width and point size are passed as constants because no where does this
		// plug-in allow them to be changed... er, that's left as an 'exercise for the 
		// reader'
		const int32 kTextWidth = 60;
		const int32 kPointSize = 12;
		// updating the attributes of the label
		labelData->Set(theLabel, kTextWidth, kPointSize, visibility);

		// Finally, the command is processed:
		error = CmdUtils::ProcessCommand(labelCommand);
		
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			CAlert::WarningAlert("kFrmLblNoValidPageItemsSelectedErrorKey");
		}

		// need to check whether the item already has the adornment
		item = selectUIDList.Length();
		if (visibility) // we want to see the adornment, so add it to all the items that don't already have it
		{
			while (item-- > 0) //take them off the back of the list
			{
				InterfacePtr<IPageItemAdornmentList> iPageItemAdornmentList(selectUIDList.GetRef(item), IID_IPAGEITEMADORNMENTLIST);
				if(iPageItemAdornmentList)
				{
					InterfacePtr<IAdornmentIterator> pIt(iPageItemAdornmentList->CreateIterator(IAdornmentShape::kAfterShape));
					IPMUnknown* punkAdorn = nil;
					while( pIt && ((punkAdorn = pIt->GetNextAdornment()) != nil))
					{
						InterfacePtr<IAdornmentShape> tShape(punkAdorn, UseDefaultIID());
						if (tShape==nil) // nothing left in the list...
							break;
						// need to check whether the item already has the adornment
						if (::GetClass(tShape) == kFrmLblAdornmentBoss)
						{
							selectUIDList.Remove(item);
							break;
						}
					}	
				}
			}
			
			if (selectUIDList.Length() > 0)
			{
				InterfacePtr<ICommand>	cmd((ICommand*)::CreateObject(kAddPageItemAdornmentCmdBoss, IID_ICOMMAND));
				cmd->SetItemList(selectUIDList);
				InterfacePtr<IClassIDData>	classIDData(cmd, UseDefaultIID());
				classIDData->Set(kFrmLblAdornmentBoss);

				error = CmdUtils::ProcessCommand(cmd);
			}
		}
		else // now we don't want to see the adornment, so take it off all the items that already have it
		{
			while (item-- > 0) //take them off the back of the list
			{
				InterfacePtr<IPageItemAdornmentList> iPageItemAdornmentList(selectUIDList.GetRef(item), IID_IPAGEITEMADORNMENTLIST);
				if(iPageItemAdornmentList)
				{
					InterfacePtr<IAdornmentIterator> pIt(iPageItemAdornmentList->CreateIterator(IAdornmentShape::kAfterShape));
					IPMUnknown* punkAdorn = nil;
					while( pIt && ((punkAdorn = pIt->GetNextAdornment()) != nil))
					{
						InterfacePtr<IAdornmentShape> tShape(punkAdorn, UseDefaultIID());
						if (tShape==nil) // no page items left in the list...
						{
							selectUIDList.Remove(item);
							break;
						}
						if (::GetClass(tShape) == kFrmLblAdornmentBoss)
							break; // In this case, we remove the items with the adornment
					}
				}
			}
			if (selectUIDList.Length() > 0)
			{
				InterfacePtr<ICommand>	cmd((ICommand*)::CreateObject(kRemovePageItemAdornmentCmdBoss, IID_ICOMMAND));
				cmd->SetItemList(selectUIDList);
				InterfacePtr<IClassIDData>	classIDData(cmd, UseDefaultIID());
				classIDData->Set(kFrmLblAdornmentBoss);

				error = CmdUtils::ProcessCommand(cmd);
			}
		}
	}while(false);
	return	error;
}

/**
	update frame labels for selected page items.
*/
ErrorCode N2PAdIssueUtils::UpdateUIDListLabelfor(UIDList& selectUIDList,const PMString& theLabel, const bool16& visibility)
{
	ErrorCode error;
	do
	{
		//CAlert::InformationAlert("UpdateUIDListLabelfor");
		//UIDList	selectUIDList = ListUId;
		
		if (selectUIDList.Length() < 1)
		{
			// How did we get here?  If we have a selection but no items something's
			// really messed up:
			CAlert::InformationAlert("FrameLabelDlgController::ApplyFields: UIDList invalid");
			break;
		}
		
		// The selection list can have items in it that will not have an IFrmLblData interface
		// so those will be removed from the selection list before the LabelFrameCmd is sent.
		// Walking backwards through the list makes removing items easier.
		int32 item = selectUIDList.Length();
		while (item-- > 0)
		{
			InterfacePtr<IFrmLblData> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}
		
		// If there are no items left in the list, then report an error and leave.
		if (selectUIDList.Length() == 0)
		{
			CAlert::InformationAlert("kFrmLblNoValidPageItemsSelectedErrorKey 1");
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> labelCommand(CmdUtils::CreateCommand(kFrmLblCmdBoss));
		if (labelCommand == nil)
		{
			// For some reason we couldn't get our label command pointer, so assert and
			// issue a user warning and leave:
			CAlert::InformationAlert("FrameLabelDlgController::ApplyFields: labelCommand invalid");
			break;
		}

		labelCommand->SetItemList(selectUIDList);

		InterfacePtr<IFrmLblData> labelData(labelCommand, UseDefaultIID());		
		if (labelData == nil)
		{
			// For some reason we couldn't get our label data pointer, so assert and
			// issue a user warning and leave:
			CAlert::InformationAlert("FrameLabelDlgController::ApplyFields: labelData invalid");
			break;
		}
		// Text width and point size are passed as constants because no where does this
		// plug-in allow them to be changed... er, that's left as an 'exercise for the 
		// reader'
		const int32 kTextWidth = 60;
		const int32 kPointSize = 12;
		// updating the attributes of the label
		labelData->Set(theLabel, kTextWidth, kPointSize, visibility);

		// Finally, the command is processed:
		error = CmdUtils::ProcessCommand(labelCommand);
		
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("Check for errors, issue warning if so:");
		}
		 
		// need to check whether the item already has the adornment
		item = selectUIDList.Length();
		if (visibility) // we want to see the adornment, so add it to all the items that don't already have it
		{
			while (item-- > 0) //take them off the back of the list
			{
				InterfacePtr<IPageItemAdornmentList> iPageItemAdornmentList(selectUIDList.GetRef(item), IID_IPAGEITEMADORNMENTLIST);
				if(iPageItemAdornmentList)
				{
					InterfacePtr<IAdornmentIterator> pIt(iPageItemAdornmentList->CreateIterator(IAdornmentShape::kAfterShape));
					IPMUnknown* punkAdorn = nil;
					while( pIt && ((punkAdorn = pIt->GetNextAdornment()) != nil))
					{
						InterfacePtr<IAdornmentShape> tShape(punkAdorn, UseDefaultIID());
						if (tShape==nil) // nothing left in the list...
							break;
						// need to check whether the item already has the adornment
						if (::GetClass(tShape) == kFrmLblAdornmentBoss)
						{
							selectUIDList.Remove(item);
							break;
						}
					}	
				}
			}
			
			
			if (selectUIDList.Length() > 0)
			{
				
				InterfacePtr<ICommand>	cmd((ICommand*)::CreateObject(kAddPageItemAdornmentCmdBoss, IID_ICOMMAND));
				cmd->SetItemList(selectUIDList);
				InterfacePtr<IClassIDData>	classIDData(cmd, UseDefaultIID());
				classIDData->Set(kFrmLblAdornmentBoss);

				error = CmdUtils::ProcessCommand(cmd);
			}
			
		}
		else // now we don't want to see the adornment, so take it off all the items that already have it
		{
			
			while (item-- > 0) //take them off the back of the list
			{
				InterfacePtr<IPageItemAdornmentList> iPageItemAdornmentList(selectUIDList.GetRef(item), IID_IPAGEITEMADORNMENTLIST);
				if(iPageItemAdornmentList)
				{
					InterfacePtr<IAdornmentIterator> pIt(iPageItemAdornmentList->CreateIterator(IAdornmentShape::kAfterShape));
					IPMUnknown* punkAdorn = nil;
					while( pIt && ((punkAdorn = pIt->GetNextAdornment()) != nil))
					{
						InterfacePtr<IAdornmentShape> tShape(punkAdorn, UseDefaultIID());
						if (tShape==nil) // no page items left in the list...
						{
							selectUIDList.Remove(item);
							break;
						}
						if (::GetClass(tShape) == kFrmLblAdornmentBoss)
							break; // In this case, we remove the items with the adornment
					}
				}
			}
			if (selectUIDList.Length() > 0)
			{
				InterfacePtr<ICommand>	cmd((ICommand*)::CreateObject(kRemovePageItemAdornmentCmdBoss, IID_ICOMMAND));
				cmd->SetItemList(selectUIDList);
				InterfacePtr<IClassIDData>	classIDData(cmd, UseDefaultIID());
				classIDData->Set(kFrmLblAdornmentBoss);

				error = CmdUtils::ProcessCommand(cmd);
			}
		}
		
	}while(false);
	return(error);
}


/**
	Get the frame label and its visibility of the selected page items.
*/						
void N2PAdIssueUtils::GetFrameLabelAndVisibilityUIDListLabelfor(UIDList& selectUIDList,PMString& theLabel, bool16& visibility)
{
		

		// --- Go through each item in the list and filter out those non-guide items.
		//     We assume that we will never get in a mixed guide/non-guide selection. 
		for (int32 i = selectUIDList.Length () - 1; i >= 0; i--)
		{
			UIDRef item = selectUIDList.GetRef (i);

			InterfacePtr<IFrmLblData> frameData(item, UseDefaultIID());
			if (frameData != nil)
			{
				theLabel = frameData->GetString();
				visibility = frameData->GetVisibility();
				break;
			}
		}
	
}





////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

/**
	Get the frame label and its visibility of the selected page items.
*/
/**
	Get the frame label and its visibility of the selected page items.
*/						
void N2PAdIssueUtils::GetVisibilityOfPenUnLockofUIDListLabelfor(UIDList& selectUIDList, bool16& visibility)
{
		

		// --- Go through each item in the list and filter out those non-guide items.
		//     We assume that we will never get in a mixed guide/non-guide selection. 
		for (int32 i = selectUIDList.Length () - 1; i >= 0; i--)
		{
			UIDRef item = selectUIDList.GetRef (i);

			InterfacePtr<IN2PSqlPenUnLockData> frameData(item, UseDefaultIID());
			if (frameData != nil)
			{
				visibility = frameData->GetVisibility();
				break;
			}
		}
	
}


/**
	update frame labels for selected page items.
*/
ErrorCode N2PAdIssueUtils::UpdateUIDListLabelforPenUnLock(UIDList& selectUIDList, const int32& LockUnLock, const bool16& visibility)
{
	ErrorCode error;
	do
	{
		
		//UIDList	selectUIDList = ListUId;
		
		if (selectUIDList.Length() < 1)
		{
			// How did we get here?  If we have a selection but no items something's
			// really messed up:
			ASSERT_FAIL("UpdateUIDListLabelforPenUnLock::ApplyFields: UIDList invalid");
			break;
		}
		
		// The selection list can have items in it that will not have an  IN2PSqlPenUnLockData interface
		// so those will be removed from the selection list before the LabelFrameCmd is sent.
		// Walking backwards through the list makes removing items easier.
		int32 item = selectUIDList.Length();
		while (item-- > 0)
		{
			InterfacePtr<IN2PSqlPenUnLockData> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}
		
		// If there are no items left in the list, then report an error and leave.
		if (selectUIDList.Length() == 0)
		{
			ASSERT_FAIL("UpdateUIDListLabelforPenUnLock 1");
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> labelCommand(CmdUtils::CreateCommand(kN2PSqlPenUnlockCmdBoss));
		if (labelCommand == nil)
		{
			// For some reason we couldn't get our label command pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("FrameLabelDlgController::ApplyFields: labelCommand invalid");
			break;
		}

		labelCommand->SetItemList(selectUIDList);

		InterfacePtr<IN2PSqlPenUnLockData> labelData(labelCommand, UseDefaultIID());		
		if (labelData == nil)
		{
			// For some reason we couldn't get our label data pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("FrameLabelDlgController::ApplyFields: labelData invalid");
			break;
		}
		
		
		
		// updating the attributes of the label
		labelData->Set(LockUnLock, kTrue);

		// Finally, the command is processed:
		error = CmdUtils::ProcessCommand(labelCommand);
		
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			CAlert::WarningAlert("Check for errors, issue warning if so:");
		}

		// need to check whether the item already has the adornment
		item = selectUIDList.Length();
		if (visibility) // we want to see the adornment, so add it to all the items that don't already have it
		{
			
			while (item-- > 0) //take them off the back of the list
			{
				InterfacePtr<IPageItemAdornmentList> iPageItemAdornmentList(selectUIDList.GetRef(item), IID_IPAGEITEMADORNMENTLIST);
				if(iPageItemAdornmentList)
				{
					InterfacePtr<IAdornmentIterator> pIt(iPageItemAdornmentList->CreateIterator(IAdornmentShape::kAfterShape));
					IPMUnknown* punkAdorn = nil;
					while( pIt && ((punkAdorn = pIt->GetNextAdornment()) != nil))
					{
						InterfacePtr<IAdornmentShape> tShape(punkAdorn, UseDefaultIID());
						if (tShape==nil) // nothing left in the list...
							break;
						// need to check whether the item already has the adornment
						if (::GetClass(tShape) == kN2PSqlPenUnlockAdornmentBoss)
						{
							selectUIDList.Remove(item);
							break;
						}
					}	
				}
			}
			
			
			
			if (selectUIDList.Length() > 0)
			{
				InterfacePtr<ICommand>	cmd((ICommand*)::CreateObject(kAddPageItemAdornmentCmdBoss, IID_ICOMMAND));
				cmd->SetItemList(selectUIDList);
				InterfacePtr<IClassIDData>	classIDData(cmd, UseDefaultIID());
				classIDData->Set(kN2PSqlPenUnlockAdornmentBoss);

				error = CmdUtils::ProcessCommand(cmd);
			}
		}
		else // now we don't want to see the adornment, so take it off all the items that already have it
		{
			
			while (item-- > 0) //take them off the back of the list
			{
				InterfacePtr<IPageItemAdornmentList> iPageItemAdornmentList(selectUIDList.GetRef(item), IID_IPAGEITEMADORNMENTLIST);
				if(iPageItemAdornmentList)
				{
					InterfacePtr<IAdornmentIterator> pIt(iPageItemAdornmentList->CreateIterator(IAdornmentShape::kAfterShape));
					IPMUnknown* punkAdorn = nil;
					while( pIt && ((punkAdorn = pIt->GetNextAdornment()) != nil))
					{
						InterfacePtr<IAdornmentShape> tShape(punkAdorn, UseDefaultIID());
						if (tShape==nil) // no page items left in the list...
						{
							selectUIDList.Remove(item);
							break;
						}
						if (::GetClass(tShape) == kN2PSqlPenUnlockAdornmentBoss)
							break; // In this case, we remove the items with the adornment
					}
				}
			}
			if (selectUIDList.Length() > 0)
			{
				InterfacePtr<ICommand>	cmd((ICommand*)::CreateObject(kRemovePageItemAdornmentCmdBoss, IID_ICOMMAND));
				cmd->SetItemList(selectUIDList);
				InterfacePtr<IClassIDData>	classIDData(cmd, UseDefaultIID());
				classIDData->Set(kN2PSqlPenUnlockAdornmentBoss);

				error = CmdUtils::ProcessCommand(cmd);
			}
		}
	}while(false);
	return(error);
}
