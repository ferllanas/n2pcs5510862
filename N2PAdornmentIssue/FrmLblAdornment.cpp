//========================================================================================
//  
//  $File: //depot/indesign_5.0/highprofile/source/sdksamples/framelabel/FrmLblAdornment.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:27:55 $
//  
//  $Revision: #1 $
//  
//  $Change: 505962 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IAdornmentShape.h"
#include "IGraphicsPort.h"
#include "IHierarchy.h"
#include "IDocument.h"
#include "ISwatchList.h"
#include "IPMFont.h"
#include "IFontInstance.h"
#include "IGeometry.h"
#include "iuicolorutils.h"
#include "IBoolData.h"
#include "ISession.h"
#include "ITextParcelList.h"
#include "IFrameList.h"
#include "IPageItemUtils.h"
#include "ICommandMgr.h"
#include "ISelectionUtils.h"
#include "IGeometry.h"
#include "IGraphicFrameData.h"
#include "ITextColumnSizer.h"
#include "ITextFrameColumn.h"

// General includes:
#include "AutoGSave.h"
#include "ILayoutUIUtils.h"
#include "CPMUnknown.h"

#include "CAlert.h"
#include "TransformUtils.h"
#include "SelectionObserver.h"
#include "IGeometryFacade.h"
#include "IRefPointUtils.h"
#include "ITransformCmdData.h"

// Project includes:
#include "IFrmLblData.h"

#include "N2PAdIssueID.h"

#ifdef WINDOWS
	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif

#ifdef MACINTOSH
	#include "IN2PCTUtilities.h"
#endif
/** Provides the adornment implementation to draw a text label
	on drawable page items; implements IAdornmentShape. 
	@ingroup framelabel
	
*/

class FrmLblAdornment : public CPMUnknown<IAdornmentShape>
{

	public:

		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		FrmLblAdornment(IPMUnknown* boss);

		/** 
			Destructor.
		*/
		~FrmLblAdornment();


		/**
			Tells the draw manager when in the drawing sequence the adornment
			should be asked to draw.
			@return Enum describing when this adornment should be drawn as part of the draw sequence.  See IAdornmentShape.h.
		*/
		virtual AdornmentDrawOrder GetDrawOrderBits();


		/**
			Draws the adornment in the inner coordinates of the page item to which this adornment is attached.
			Note that for the draw to work correctly, it must be within the bounds reported by GetPaintedBBox() below.
			It's confusing, because we draw in the page item's inner coordinates, but we report bounds in the view coordinates.
			@param	iShape		Ptr to page item requesting this adornment to draw.  Handy for getting information about the owning page item, such as its bounding box.
			@param	drawOrder	Tells the adornment when in the (AdornmentDrawOrder) sequence it's being asked to draw.  Used when an adornment is registered for more than one point in the draw order.
			@param	gd			Graphics data structure.  Used to acquire the drawing port, view, draw manager, etc.
			@param	flags		IShape flags describing mode of draw: dynamic, printing, patient user, etc.  Used for deciding when an adornment should not draw, or draw differently to a particular port. For example, draw to the screen but not to print.		
		*/
		virtual void DrawAdornment
			(
				IShape* 			iShape,
				AdornmentDrawOrder	drawOrder,
				GraphicsData*		gd,
				int32				flags
			);


		/** 
			Reports the dimensions of the adornment in the coordinates of the view in which this adornment will draw.
			Note the bounds reported by this method must accurately reflect the bounds used by Draw().
			It's confusing, because we draw in the page item's inner coordinates, but we report bounds in the view coordinates.
			@param	iShape		Ptr to page item requesting this adornment to draw.  Handy for getting information about the owning page item, such as its bounding box.
			@param	drawOrder	Tells the adornment when in the (AdornmentDrawOrder) sequence it's being asked to draw.  Used when an adornment is registered for more than one point in the draw order.
			@param	itemBounds	The painted bounds of the owing page item in the page item's inner coordinates.
			@param	innertoview	The transformation from the inner bounds coordinates of the page item to which this adornment is attached, to the coordinate system of the view in which the adornment will appear.
		*/
		virtual PMRect GetPaintedAdornmentBounds
			(
				IShape*				iShape,
				AdornmentDrawOrder	drawOrder,
				const PMRect&		itemBounds,
				const PMMatrix&		innertoview
			);

		/** 
			Here for completeness.  See IAdornmentShape for an explanation.
		*/
		virtual void Inval
			(
				IShape*				iShape,
				AdornmentDrawOrder	drawOrder,
				GraphicsData*		gd, 
				ClassID 			reasonForInval, 
				int32 				flags
			);

		/**
			Adornments affect when they are called by the priority value they return
			via their GetPriority() method. Higher priorities (smaller numbers) are
			called before lower priorities (larger numbers).
			@return Always returns the highest priority.
		*/
		virtual PMReal GetPriority() 
				{return 0;}


		/** 
			Adornments can modify their inking bounds if they exceed the content bounds.
			For example, if content adds a drop shadow under some conditions, then the inking
			bounds would be different than the painted bbox calculated in this class.  (Which
			is based on the adornment text and fonts, etc.)
			This class doesn't support any inking changes that would make the inking bounds
			different from the painted bbox, so we just have a do-nothing implementation.
			@param iShape IN Points to interface on page item boss to which this adornment is applied.
			@param inOutBounds IN Points to rectangle describing the current ink bounds. Modify this PMRect to reflect accurate ink bounds.
		*/
		virtual void AddToContentInkBounds(IShape* iShape, PMRect* inOutBounds) {}


		/**
			Adornments can declare they want to draw in TextOffscreen mode.  This will suspend drawing of
			offscreen drawing, so the default is to not draw during text offscreen mode.
			See the Draw() documentation for an explanation of parameters. 
		*/
		virtual bool16 WillDraw
		(
			IShape* 			iShape,		// owning page item
			AdornmentDrawOrder	drawOrder,	// for items that registered for more than one order
			GraphicsData*		gd,
			int32			flags
		)
		{ return kFalse; }


		/**	
		  	See IAdornmentShape::WillPrint
		 */
		virtual bool16 WillPrint(void)
		{ return kFalse; }
	
	/** See IAdornmentShape::HitTest
	 */
	virtual bool16 HitTest 
	( 
	 IShape*		          iShape,         // The owning page item 
	 AdornmentDrawOrder    adornmentDrawOrder,    // In case this adornment is used more than once 
	 IControlView *		layoutView,
	 const PMRect&         r 
	 ){ return kFalse; }
		
		private:
		UID GetUIDOfTextModelOfShape(IShape*	iShape);
		
		UID getUIDRefOfSelectionObserver();
		
		UID GetTextContentUID(const UIDRef& graphicFrameUIDRef);
};


CREATE_PMINTERFACE(FrmLblAdornment, kFrmLblAdornmentImpl)


/*	Constructor.
*/
FrmLblAdornment::FrmLblAdornment(IPMUnknown* boss) 
	: CPMUnknown<IAdornmentShape>(boss)
{
	TRACE("Creating the adornment\n");
}

/* Destructor
*/
FrmLblAdornment::~FrmLblAdornment()
{
	TRACE("Releasing the Adronemnt\n");
}


/* Draw
*/
IAdornmentShape::AdornmentDrawOrder FrmLblAdornment::GetDrawOrderBits()
{
	IAdornmentShape::AdornmentDrawOrder flags = (IAdornmentShape::AdornmentDrawOrder)(kAfterShape);
	return flags;
}


/* Draw
	Performs the following steps:
	1) Get the graphics port from the graphics data
	2) Set the graphics port for drawing the label
	3) Compute the starting coordinates for drawing the label
	4) Draw the label
	5) Restore the graphics port
*/
void FrmLblAdornment::DrawAdornment
	(
		IShape* 			shape,
		AdornmentDrawOrder	drawOrder,
		GraphicsData*		gd,
		int32				flags 
	)
{

	// Only draw when the frame edge flag is set
	TRACE("Drawing the frame adornment!\n");
	if ( !(flags & IShape::kDrawFrameEdge) )
	{
		return;
	}
	
	// We choose not to draw for printing.  (Although we could.)
	if ( flags & IShape::kPrinting )
	{
		return;
	}

	TRACE("Got a call to FrmLblAdornment::Draw\n");		
	do {
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		if (gd == nil)
		{
			ASSERT_FAIL("Nil GraphicsData*");
			break;
		}
		//PMMatrix currentMat = gd->GetContentToViewTransform();//GetTransform();
			
		// Get access to the data describing the label
		InterfacePtr<IFrmLblData> labelData(shape, UseDefaultIID());
		if (labelData == nil)
		{
			TRACE("Label data == nil for this item");
			// We are often called in the general list to process, so return without assert.
			break; 
		}
		PMString labelString = labelData->GetString();
		
		PMString Estatus="";
		
		if(labelString.Contains("/"))
		{
			PMString *TokensOnElemnt = labelString.GetItem("/",1);
			PMString DirigidoA="";
			
		
			
			if(TokensOnElemnt!=nil && TokensOnElemnt->NumUTF16TextChars()>0)
			{
				
				DirigidoA = *TokensOnElemnt;
				
				PMString *Extra = labelString.Substring(DirigidoA.NumUTF16TextChars(), (labelString.NumUTF16TextChars()-DirigidoA.NumUTF16TextChars()));
			
			
				labelString = *TokensOnElemnt+" "+*Extra;
				Estatus=*Extra;
				Estatus.Remove(0,1);
			}
		}
		else
		{
			Estatus=labelString;
		}
		
		
		
		Estatus.ToUpper();
		labelString.ToUpper();
		
		if (labelString.empty())
		{
			TRACE("Label length is zero");
			break;
		}
		
		// The labels are created using the default font.
		InterfacePtr<IFontMgr> fontMgr(GetExecutionContextSession(), UseDefaultIID());
		if (fontMgr == nil)
		{
			ASSERT_FAIL("LabelDrawHandler::HandleEvent: fontMgr invalid");
			break;
		}

		// The default font, and the items geometry & hierarchy should be
		// available, but if they aren't a return is sufficient (an
		// assert here might come up a lot on a heavily populated page
		// and if this code is in error, the labels won't draw anyway.)
		InterfacePtr<IPMFont> theFont(fontMgr->QueryFont(fontMgr->GetDefaultFontName()));
		InterfacePtr<IGeometry> itemGeometry(labelData, UseDefaultIID());
		InterfacePtr<IHierarchy> itemHier(labelData, UseDefaultIID());
		if (theFont == nil || itemGeometry == nil || itemHier == nil)
		{
			// ASSERT_FAIL("LabelDrawHandler::HandleEvent: font, geometry, or hierachy unavailable");
			break;
		}

		InterfacePtr<IPMPersist> persist(itemHier, UseDefaultIID());
		if (persist == nil)
		{
			ASSERT_FAIL("Nil IPMPersist*");
			break;
		}
		IDataBase* theDB = persist->GetDataBase();
		InterfacePtr<IDocument> theDoc(theDB, theDB->GetRootUID(), UseDefaultIID());
		if (theDoc == nil)
		{
			ASSERT_FAIL("Nil IDocument*");
			break;
		}

		// The graphics port is the port that the page item is currently drawing to.
		// That could be the screen, a printer, a PDF, or some other port:
		IGraphicsPort* gPort = gd->GetGraphicsPort();
		if (gPort == nil)
		{
			ASSERT_FAIL("Nil IGraphicsPort*");
			break;
		}

		// Drawing methods are much like the PostScript commands. 
		// A good reference for the IGraphicsPort methods is the  PostScript
		// Language Reference Manual.

		// Save the current port settings.  This is the equivalent of pushing them on a stack
		AutoGSave autoGSave(gPort);

		// Set the drawing color for the port to a special value for the label
		//gPort->setrgbcolor(203/256.0, 43/256.0, 138/256.0);
		//CAlert::InformationAlert(labelString);
		if(Estatus=="F PAGE DONE" )
		{
			// Set the drawing color for the port to a special value for the label
			gPort->setrgbcolor(0/256.0, 162/256.0, 0/256.0);
		}
		else
		{
			//Para la pr
			if(Estatus=="A REPORTING" )
			{
				gPort->setrgbcolor(255/256.0, 255/256.0, 0/256.0);
			}
			else
			{
				if(Estatus=="B EDITING" )
				{
					gPort->setrgbcolor(255/256.0, 0/256.0, 0/256.0);
				}
				else
				{
					if(Estatus=="C IN PAGE" )
					{
						gPort->setrgbcolor(255/256.0, 138/256.0, 0/256.0);
					}
					else
					{
						if(Estatus=="D COPY" )
						{
							gPort->setrgbcolor(0/256.0, 0/256.0, 255/256.0);
						}
						else
						{
							if(Estatus=="E ADJUSTE PAGE" )
							{
								gPort->setrgbcolor(143/256.0, 0/256.0, 255/256.0);
							}
							else
							{
								if(Estatus=="F PAGE DONE" )
								{
									gPort->setrgbcolor(255/256.0, 255/256.0, 0/256.0);
								}
								else
								{
									if(Estatus=="I TRASH" )
									{
										gPort->setrgbcolor(0/256.0, 164/256.0, 0/256.0);
									}
									else
									{
										if(Estatus=="G PROOF" )
										{
											gPort->setrgbcolor(0/256.0, 0/256.0, 0/256.0);
										}
										else
										{
											if(Estatus=="H HOLD" )
											{
											gPort->setrgbcolor(0/256.0, 0/256.0, 0/256.0);
											}
										}
									}
								}
							}
						}
						
					}
				}
			}
		}
		
/*		if(Estatus=="F LISTO" || Estatus=="F LISTO" || Estatus=="F LISTO")
		{
			// Set the drawing color for the port to a special value for the label
			gPort->setrgbcolor(0/256.0, 162/256.0, 0/256.0);
		}
		else
		{
			//Para la pr
			if(Estatus=="A BORRADOR" )
			{
				gPort->setrgbcolor(255/256.0, 255/256.0, 0/256.0);
			}
			else
			{
				if(Estatus=="B EDITA" )
				{
					gPort->setrgbcolor(255/256.0, 0/256.0, 0/256.0);
				}
				else
				{
					if(Estatus=="C DISENO" )
					{
						gPort->setrgbcolor(255/256.0, 138/256.0, 0/256.0);
					}
					else
					{
						if(Estatus=="D PRINTER" )
						{
							gPort->setrgbcolor(0/256.0, 0/256.0, 255/256.0);
						}
						else
						{
							if(Estatus=="E AJUSTE" )
							{
								gPort->setrgbcolor(143/256.0, 0/256.0, 255/256.0);
							}
							else
							{
								if(Estatus=="F LISTO" )
								{
									gPort->setrgbcolor(255/256.0, 255/256.0, 0/256.0);
								}
								else
								{
									if(Estatus=="I BASURA" )
									{
										gPort->setrgbcolor(0/256.0, 164/256.0, 0/256.0);
									}
									else
									{
										if(Estatus=="G PROOF" )
										{
											gPort->setrgbcolor(0/256.0, 0/256.0, 0/256.0);
										}
										else
										{
											if(Estatus=="H HOLD" )
											{
											gPort->setrgbcolor(0/256.0, 0/256.0, 0/256.0);
											}
										}
									}
								}
							}
						}
						
					}
				}
			}
		}

*/
		
		// Set the font in the port
		gPort->selectfont(theFont, static_cast<PMReal>(labelData->GetSize()));

		// Compute the starting position for drawing the label in the graphics port
		// Both the width and the height have some room added so the label isn't right
		// on the bounding box:
		PMRect fontBB = theFont->GetFontBBox();
		PMReal fontHeight = fontBB.Height();
		PMReal fontWidth = fontBB.Width();
		PMReal x, y;
		TRACE("Font height is set to %f\n",::ToDouble(fontHeight));
		TRACE("Font width is set to %f\n",::ToDouble(fontWidth));
		PMReal fHeightOfText = fontHeight*static_cast<PMReal>(labelData->GetSize());
		PMRect bBox = itemGeometry->GetPathBoundingBox();

		// Place the label beneath and aligned with the left side of the frame
		//x = bBox.Left(); 
		//y = bBox.Bottom() + fHeightOfText; 
		x = bBox.Right() - 80;
		y =(bBox.Bottom()-(fHeightOfText/4)) ;
		TRACE("the label is going in at (%f,%f) and is %d characters long\n",::ToDouble(x),::ToDouble(y),labelString.CharCount());

		// Draw the string in the port
		gPort->show(x, y, labelString.NumUTF16TextChars(), labelString.GrabUTF16Buffer(nil));
		
		////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
 			
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
					(
						kN2PCTUtilitiesBoss,	// Object boss/class
						IN2PCTUtilities::kDefaultIID
					)));
					
		int32 NumTxtFmOverset = N2PCTUtils->LengthXMP("N2PTxtFmeOversetArray");
 		StructOfTextFrameConOverset *ListTextFrOrSt = new StructOfTextFrameConOverset[NumTxtFmOverset]; 
		N2PCTUtils->ObtenerTextFrameOrStOnXMP(ListTextFrOrSt,NumTxtFmOverset);
 		
 		bool16 EsUnOversetFrame = kFalse;
 		
 		//UIDRef del Frame que contiene el Adorno
 		UIDRef UIDRefFrameWithAdment = ::GetUIDRef( shape );
 		
 		//Lista que conten dra el Frame el UID del Frame que contiene el overtSet Text
 		UIDList uidList(UIDRefFrameWithAdment.GetDataBase());
 		
 		//UI del Textmodel que contiene el Frame del Adorno
 		UID UIDTextModelSelected = this->GetUIDOfTextModelOfShape(shape);
 		int32 intUITextModel = UIDTextModelSelected.Get(); //Convierte a entero
 		
 		int32 i=0;
 		int32 numRegistroActual=0;
 		//Ciclo para buscar el TextFrame con overset sobre la Estructura del XMP que corresponde al UID Texmodel anterior	
 		for(i=0 ;i<NumTxtFmOverset && EsUnOversetFrame==kFalse;i++)
 		{

 			if(ListTextFrOrSt[i].UIDTextModel==intUITextModel  )//UIDTextFrameOrSt
 			{
 				numRegistroActual=i;   										//guarda el numero de registro cual estamos interezados
 				UID UIDFrameOverset = ListTextFrOrSt[i].UIDTextFrameOrSt;	//Obtiene el UID del TextFrame a mover
 				uidList.Append(UIDFrameOverset);							//adiciona el UId del TextFrame a mover
 				EsUnOversetFrame=kTrue;										//indica que ya se encointro el correxpondiente TextFrame
 			}		
 		}
 		
 		//si se encontro el TextFrame
 		if(EsUnOversetFrame)
 		{
 			
 			
			
 			//Para verificar si existe el Texframe que contiene el text overset
 			InterfacePtr<IHierarchy> hierarchy(uidList.GetRef(0), UseDefaultIID());
			ASSERT(hierarchy);
			if (!hierarchy) 
			{
				UIDRef FrameWhitOversetText;		//gurda el nuevo UIDRef del Frame creado
				
				
				//Quiere Decir que no exite el Frame, entonces se debe de crear nuevo Frame y cambiar la estructura del XMP
				if(N2PCTUtils->CanFlowTextOversetOnNewFrame( UIDTextModelSelected, FrameWhitOversetText ))
				{
					ListTextFrOrSt[ numRegistroActual ].UIDTextModel = UIDTextModelSelected.Get();					//Actualiza el registro clau estamos interesado
					ListTextFrOrSt[ numRegistroActual ].UIDTextFrameOrSt = (FrameWhitOversetText.GetUID()).Get();	//
					
					N2PCTUtils->DeleteXMP("N2PTxtFmeOversetArray");							//borra el contenido de los registros en XMP
					N2PCTUtils->GuardarTextFrameOrStOnXMP(ListTextFrOrSt,NumTxtFmOverset);	//Guarda el nuevo contenido de registros en XMP
				}
				
				
				break;
			}
			
			UIDRef parentUIDRef = UIDRef(UIDRefFrameWithAdment.GetDataBase(), hierarchy->GetLayerUID());
			
			
			
 			//obtiene el punto de la izquierda y el tope del TextFrame que tiene el Overset
			PMRect itemsBoundingBox = Utils<Facade::IGeometryFacade>()->GetItemBounds(uidList.GetRef(0),Transform::PasteboardCoordinates(),Geometry::PathBounds());
			PMPoint itemsBoundsCenter = itemsBoundingBox.LeftTop();
 	
 			//Obtiene el Punto del Frame que contiene el TextModel
			PMMatrix inner2parent = ::InnerToParentMatrix(itemGeometry);
			PMRect boundsInParentCoords = itemGeometry->GetStrokeBoundingBox(inner2parent);
			boundsInParentCoords.MoveRel(boundsInParentCoords.Width() , boundsInParentCoords.Height());
			PMPoint points = boundsInParentCoords.LeftTop();
			// Parent the new text frame on the same spread layer as the given text frame.
			PMPoint nosemovio(0.0,0.0);
			if((points -  itemsBoundsCenter)==nosemovio)
			{
				//para salir cuando no se a cambiado la posicion
				break;
			}
			
			
				// Get the number of columns in the given text frame con Adornment.
			InterfacePtr<ITextColumnSizer> fmeColumnSizerOfFmeWhitAdonment(UIDRefFrameWithAdment.GetDataBase(), this->GetTextContentUID(UIDRefFrameWithAdment), UseDefaultIID());
			ASSERT(fmeColumnSizerOfFmeWhitAdonment);
			if (!fmeColumnSizerOfFmeWhitAdonment) {
			
				break;
			}
			
			int32 numberOfColumns = fmeColumnSizerOfFmeWhitAdonment->GetNumberOfColumns();
			PMReal AnchoDColumns=fmeColumnSizerOfFmeWhitAdonment->GetFixedWidth();
			
			
		//////////////////////////////
			
		// Determine if the given text frame is vertical.
			InterfacePtr<ITextFrameColumn> textFrame(fmeColumnSizerOfFmeWhitAdonment, UseDefaultIID());
			if (!textFrame) 
			{
				
				break;
			}
		//
		UIDRef UIDRefOfElementoNota(theDB,textFrame->GetTextModelUID());
		
		///////
		InterfacePtr<ITextModel> myTextModel(textFrame->QueryTextModel(), UseDefaultIID());
		if(!myTextModel)
		{
			break;
		}
		
		
		
		/////
		InterfacePtr<IFrameList> frameList(textFrame->QueryFrameList());
		if (!frameList) {
			
			break;
		}
		InterfacePtr<ITextParcelList> textParcelList(frameList,UseDefaultIID());
		if (!textParcelList) {
			
			break;
		}
		bool16 isVertical = textParcelList->GetIsVertical();
		
		PMReal TamanoDeInterlineado = 0.0;
 		if(!N2PCTUtils->GetTamanoInterlineado(frameList,TamanoDeInterlineado))
 			break;
		
		
			//ebo obtener la cantidad de lineas ue faltan para llenar el ultimo frame para calcular a cuanto se tiene que minimizar el Frame
			PMReal estimatedDepth;
			int32 NumLinesToOverset=0;
			
			////////////////////////////////////////////////////////////////////
			IDocument* documentActual = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
		
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			db->BeginTransaction();
 			InterfacePtr<ICommandMgr> cmdManager(document,IID_ICOMMANDMGR);
 			bool16 olUndo = cmdManager->SetUndoSupport(kFalse);
 			
			PBPMPoint referencePoint = Utils<IRefPointUtils>()->CalculateReferencePoint(IReferencePointData::kTopLeft,uidList);
			
			//Para el ancho del textframe
			//InterfacePtr<ICommand> SetWidhtCmd(PageItemUtils::CreateDefaultSetWidthCommand(uidList,AnchoDColumns,kTrue,kTrue,IGeometry::kResizeItemOnly));
			//CmdUtils::ProcessCommand(SetWidhtCmd);
			
			//InterfacePtr<ICommand> SetHeigthCmd(PageItemUtils::CreateDefaultSetHeightCommand(uidList,(1000),kTrue,kTrue,IGeometry::kResizeItemOnly));
			//CmdUtils::ProcessCommand(SetHeigthCmd);
			Utils<Facade::IGeometryFacade>()->ResizeItems(uidList,Transform::PasteboardCoordinates(),Geometry::PathBounds(),referencePoint,Geometry::ResizeTo(AnchoDColumns,1000));
			
			//
			//Obtiene el numero de lkineas a las que se paso
			int32 CantidadLinesOverset = Utils<ITextUtils>()->CountOversetLines(UIDRefOfElementoNota);
			//Obtiene le alto del frame
			PMRect Altoitem = Utils<Facade::IGeometryFacade>()->GetItemBounds(uidList.GetRef(0),Transform::PasteboardCoordinates(),Geometry::OuterStrokeBounds());//PageItemUtils::GetStrokeDimensions(uidList);
			
			//Obtiene el numero de Lineas que faltan para completar el textframe
			N2PCTUtils->EstimateStoryDepth(myTextModel,estimatedDepth,NumLinesToOverset);
			
				
			//Para el Alto
			if(NumLinesToOverset>0)
			{
				
				if(((Altoitem.Height()) - TamanoDeInterlineado * NumLinesToOverset)<0)
				{
					//aqui se deberia borrar el Frame
				//	InterfacePtr<ICommand> SetHeigthCmd(PageItemUtils::CreateDefaultSetHeightCommand(uidList,(1),kTrue,kTrue,IGeometry::kResizeItemOnly));
				//	CmdUtils::ProcessCommand(SetHeigthCmd);
					
				//	InterfacePtr<ICommand> SetWidhtCmd(PageItemUtils::CreateDefaultSetWidthCommand(uidList,(1),kTrue,kTrue,IGeometry::kResizeItemOnly));
				//	CmdUtils::ProcessCommand(SetWidhtCmd);
					Utils<Facade::IGeometryFacade>()->ResizeItems(uidList,Transform::PasteboardCoordinates(),Geometry::PathBounds(),referencePoint,Geometry::ResizeTo(1,1));

				}
				else
				{
					//InterfacePtr<ICommand> SetHeigthCmd(PageItemUtils::CreateDefaultSetHeightCommand(uidList,((Altoitem.Height()) - TamanoDeInterlineado * NumLinesToOverset),kTrue,kTrue,IGeometry::kResizeItemOnly));
					//CmdUtils::ProcessCommand(SetHeigthCmd);
					Utils<Facade::IGeometryFacade>()->ResizeItems(uidList,Transform::PasteboardCoordinates(),Geometry::PathBounds(),referencePoint,Geometry::ResizeTo(Geometry::KeepCurrentValue(),((Altoitem.Height()) - TamanoDeInterlineado * NumLinesToOverset)));
					
				}
				
			}
			else
			{
				if(CantidadLinesOverset>0)
				{
					
					//InterfacePtr<ICommand> SetHeigthCmd(PageItemUtils::CreateDefaultSetHeightCommand(uidList,((Altoitem.Height()) + TamanoDeInterlineado * CantidadLinesOverset),kTrue,kTrue,IGeometry::kResizeItemOnly));
					//CmdUtils::ProcessCommand(SetHeigthCmd);
					Utils<Facade::IGeometryFacade>()->ResizeItems(uidList,Transform::PasteboardCoordinates(),Geometry::PathBounds(),referencePoint,Geometry::ResizeTo(Geometry::KeepCurrentValue(),((Altoitem.Height()) + TamanoDeInterlineado * CantidadLinesOverset)));
				}
				
			}
			
			
			////////
			InterfacePtr<ICommand> moveRelativeCmd(CmdUtils::CreateCommand(kMoveRelativeCmdBoss));
			ASSERT(moveRelativeCmd);
			if (!moveRelativeCmd) {
				//stat = kFailure;
				break;
			}
			InterfacePtr<ITransformCmdData> moveRelativeCmdData(moveRelativeCmd, UseDefaultIID());
			ASSERT(moveRelativeCmdData);
			if (!moveRelativeCmdData) {
				//stat = kFailure;
				break;
			}
			PMPoint delta = points -  itemsBoundsCenter;
			moveRelativeCmdData->SetTransformData(Transform::PasteboardCoordinates(),PMPoint(0,0),Transform::TranslateBy(delta.X(),delta.Y()));
			moveRelativeCmd->SetItemList(uidList);
			CmdUtils::ProcessCommand(moveRelativeCmd);
 			
 			
 			
 			cmdManager->SetUndoSupport(kTrue);
 			theDB->EndTransaction();
 			theDB->SetModified(kFalse);
 		}
		
	} while (false); // Only do once.
}


/* GetPaintedBBox
*/
PMRect FrmLblAdornment::GetPaintedAdornmentBounds
	(
		IShape*				shape,
		AdornmentDrawOrder	drawOrder,
		const PMRect&		itemBounds,
		const PMMatrix&		innertoview	
	)
{

	// Create a rectangle to describe the bounds of the adornment in the inner coordinates of
	// of the page item for which this adornment will draw.
	PMRect aChildRect(itemBounds);
	PMMatrix viewToInner = innertoview;
	viewToInner.Invert();
	viewToInner.Transform( &aChildRect );

	PMPoint characterCountDimensionPt;
 
	do {

		// Get access to the data describing the label
		InterfacePtr<IFrmLblData> labelData(shape, UseDefaultIID());
		if (labelData == nil)
		{
			TRACE("Label data == nil for this item");
			// We are often called in the general list to process, so return without assert.
			break; 
		}

		// Get the default font
		InterfacePtr<IFontMgr> fontMgr(GetExecutionContextSession(), UseDefaultIID());
		if (fontMgr == nil)
		{
			ASSERT_FAIL("Nil IFontMgr*");
			break; 
		}
		InterfacePtr<IPMFont> font(fontMgr->QueryFont(fontMgr->GetDefaultFontName()));
 		if (font == nil)
		{
			ASSERT_FAIL("Nil IPMFont*");
			break; 
		}

		// Compute the length of the string, given the font and point size
 		PMString labelString = labelData->GetString();
		if ( !labelString.empty() )
		{
			// Create a matrix describing the label character size in points
			PMMatrix matrix( static_cast<PMReal>(labelData->GetSize()), 0.0, 0.0,
							 static_cast<PMReal>(labelData->GetSize()), 0.0, 0.0 );

			// Get access to a default instance of the font in the label point size
			InterfacePtr<IFontInstance> fontInst(fontMgr->QueryFontInstance(font, matrix));
			if (fontInst == nil)
			{
				ASSERT_FAIL("Nil IFontInstance*");
				break; 
			}

			// Ask the font instance to calculate the size of the label string
			PMReal width, height;
			fontInst->MeasureWText((textchar*) labelString.GrabUTF16Buffer(nil), labelString.WCharLength(), width, &height);
        
			// Hack safety net for height...
			if(height == 0)
				height = 1.5 * labelData->GetSize();

			// Extract the w,h of the string in the requested font and point size
			characterCountDimensionPt =  PMPoint(width, height);
		}
		else
		{
			characterCountDimensionPt =  PMPoint(0.0, 0.0);
		}

		// Compute the (top,right) position of the label in the page item's inner coordiantes
		// At this point aChildRect should not be smaller than the area used by the Draw() method.
		aChildRect.Right( aChildRect.Left() +  characterCountDimensionPt.X());
		aChildRect.Bottom( aChildRect.Bottom() + characterCountDimensionPt.Y());

		// Now transform the description to the coordinates of the view in which this adornment will draw
		innertoview.Transform(&aChildRect);

	} while (false);

	return aChildRect;

}

/* Inval
*/
void FrmLblAdornment::Inval
	(
		IShape*				iShape,
		AdornmentDrawOrder	drawOrder,
		GraphicsData*		gd, 
		ClassID 			reasonForInval, 
		int32 				flags
	)
{
}


UID FrmLblAdornment::getUIDRefOfSelectionObserver()
{
	UID result = kInvalidUID;
	do
	{		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			break;
		}
		
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			break;
		}
		
		//	Para textFrame 
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			ASSERT_FAIL("pTextSel");
			break;
		}
		
		
		UIDRef selectedFrameRef = UIDRef::gNull;
		UID selectedFrameItem = kInvalidUID;
		InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
		if (pLayoutTarget)
		{
			UIDList itemList = pLayoutTarget->GetUIDList(kStripStandoffs);
			if (itemList.Length() == 1) 
			{
				// get the database
				db = itemList.GetDataBase();
				// get the UID of the selected item
				selectedFrameItem = itemList.At(0);
				
				selectedFrameRef=itemList.GetRef(0);
			}
		}
		else
		{
			ASSERT_FAIL("False");
			break;
		}
		
		InterfacePtr<IGraphicFrameData> graphicFrameDataFlow(selectedFrameRef, UseDefaultIID());
		if (!graphicFrameDataFlow) 
		{
			break;
		}
		
		result = graphicFrameDataFlow->GetTextContentUID();
		
	}	while(false);
	return(result);
}



/*
*/
UID FrmLblAdornment::GetTextContentUID(const UIDRef& graphicFrameUIDRef)
{
	UID result = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameData(graphicFrameUIDRef, UseDefaultIID());
	if (graphicFrameData)
	{
		result = graphicFrameData->GetTextContentUID();
	}
	return result;
}

/*
*/
UID FrmLblAdornment::GetUIDOfTextModelOfShape(IShape*	iShape)
{	
	UID result = kInvalidUID;
	do
	{
		 UIDRef boxRef = ::GetUIDRef( iShape );
		InterfacePtr<IGraphicFrameData> graphicFrameDataFlow(boxRef, UseDefaultIID());
		if (!graphicFrameDataFlow) 
		{
			ASSERT_FAIL("graphicFrameDataFlow");
			break;
		}
		
		result = graphicFrameDataFlow->GetTextContentUID();
		
		// Get the number of columns in the given text frame.
		InterfacePtr<ITextColumnSizer> frameColumnSizer(boxRef.GetDataBase(), result, UseDefaultIID());
		ASSERT(frameColumnSizer);
		if (!frameColumnSizer) {
			ASSERT_FAIL("frameColumnSizer");
			break;
		}
		
		InterfacePtr<ITextFrameColumn> textFrame(frameColumnSizer, UseDefaultIID());
		if (!textFrame) 
		{
			ASSERT_FAIL("textFrame");
			break;
		}
		
		result = textFrame->GetTextModelUID();
	}while(false);
	return(result);
}