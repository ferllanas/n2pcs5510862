//========================================================================================
//  
//  $File: $
//  
//  Owner: Juan Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__

// English string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_enUS)
{
        k_enUS,									// Locale Id
        kEuropeanWinToMacEncodingConverter,		// Character encoding converter (irp) I made this WinToMac as we have a bias to generate on Win...
        {
/*		
		kN2PsqlIdiamaActualStringKey,		"English",
		kN2PsqlDlgDetachArticleTitleKey,	"Separar nota",
               	// ----- Menu strings
		kN2PsqlCompanyKey,					kN2PsqlCompanyValue,					
		kN2PsqlAboutMenuKey,					kN2PsqlPluginName,
		kN2PsqlPluginsMenuKey,				kN2PsqlPluginName,
		kN2PsqlItemOneMenuItemKey,	"ItemOne[US]",
		kN2PsqlAboutThisPlugInMenuKey,			"Acerca de News2page",	
		
		kN2PsqlStcThisArticleTextKey,			"Guia:",
		kN2PSQLShowOverTextStringKey,			"Muestra texto sobrante",
		kN2PsqlStTextRoutedToTextKey,				"Dirigido a",
		// ----- Command strings

		
		// ----- Window strings

		kN2PsqlDlgBusquedaTitleKey,		"Busqueda",
		// ----- Panel/dialog strings
		kN2PsqlPanelTitleKey,				kN2PsqlPluginName,
		kN2PsqlStaticTextKey,				kN2PsqlPluginName,
		kN2PsqlVistaPreviaTextKey,			"Previo de Nota",
		kN2PsqlStcTextPageTextKey,				"Pagina:",
		kN2PsqlStTextTituloTextKey,				"Titulo:",
		kN2PsqlStTextBalazoTextKey,				"Balazo:",
		kN2PsqlStTextNotaTextKey,					"Nota:",
		kN2PsqlStTextPieTextKey,					"Pie foto:",
		kN2PsqlStTextStatusTextKey,				"Estatus:",
		kN2PsqlStTextPubliTextKey,				"Publicacion:",
		kN2PsqlStTextSeccionTextKey,				"Seccion:",
		kN2PsqlStTextFechaTextKey,				"Fecha:",
		kN2PsqlBotonSubmitFechaTextKey,			"Ingresa Nota",
		kN2PsqlBotonPDFFechaTextKey,				"Genera PDF",
		kN2PsqlBotonLimpiarCamposTextKey,			"Limpiar campos",
		kN2PsqlDepositarMenuItemKey,		"Depositar Pagina",
		kN2PsqlRetirarMenuItemKey,		"Retirar Pagina",
		kN2PsqlSalvarMenuItemKey,			"Guardar Revision",
		kN2PsqlDepInCopyMenuItemKey,		"Deposita a InCopy",
		kN2PsqlBotonPDFBusquedaTextKey,			"Busqueda",
		kN2PsqlBotonBuscarNotaTextKey,			"Buscar Nota",
		kN2PsqlVistaPreviaTextKey,				"Vista Previa",
		kN2PsqlStTextArticlesTextKey,				"Notas",
		kN2PsqlStTextMAkeUpdateTextKey,			"Hacer actualizacion",
		kN2PsqlStTextReciveUpdateTextKey,			"Recibir actualizacion",
		kN2PsqlMsgAlertHacerSeleccionTextKey,		"Debe hacer almenos una seleccion",
		// ----- Error strings

		kN2PsqlNoExisteDocumentoStringKey,			"No hay un documento abierto",
		kN2PsqlUpdateFinishedStringKey,				"Actualizacion finalizada",
		kN2PsqlPDFExportadoStringKey,				"Archivo PDF generado",
		kN2PsqlNewRegisterOnDBStringKey,			"Se ha generado un nuevo registro en la base de datos.",
		
		
		
		kN2PSQLPreferenciasMenuItemStringKey,		"Preferencias de News2Page",
		kN2PSQLPreferDlgTitleKey,			"Preferencias de News2Page",
		kN2PsqlRefreshStatusNotaStringKey,	"Actualiza estatus",
		kN2PSQLServerIPLabelKey	,			"Direccion IP",
		kN2PSQLOptionStringKey,				"Servidor MySQL",				
		kN2PSQLFileMAkerStringKey	,		"FileMaker"	,
		kN2PSQLDBOpcionesStringKey	,		"Opciones",	
		kN2PSQLNameDBLabelKey		,		"Nombre DB",
		kN2PSQLDSNNameLabelKey	,		"DSN:",			
		kN2PSQLPasswordDBLabelKey	,		"ContraseÒa",
		kN2PSQLNameUsuarioDBLabelKey,		"Usuario",
		kN2PsqlNewDBConexionStringKey,		"Nueva DB",
		kN2PsqlDeleteDBConexionStringKey,		"Elimina DB",
		kN2PsqlSetLocalDBConexionStringKey,	"Predeterminado",
		kN2PSQLNameConecctionDBLabelKey,	"Nombre Cconexion",
		kN2PsqlTestDBConexionStringKey,		"Prueba",
		kN2PsqlCancelDBConexionStringKey,	"Cancela",
		kN2PsqlGuardarDBConexionStringKey,	"Guarda",
		kN2PsqlEditDBConexionStringKey,		"Editar",
		kN2PSQLSeguroBorrarPrefereStringKey,	"Esta a punto de eliminar esta preferencia",
		kN2PSQLAlmenosUnaSeleccionStringKey,		"Debe seleccionar una preferencia",
		kN2PSQLErrorDuranteBorradoStringKey,		"Error al eliminar el archivo",
		kN2PSQLPreferenciaBorradaStringKey,		"La preferencia fue eliminada",
		kN2PSQLSeguroBorrarPrefereDefaultStringKey, "Esta a punto de eliminar la preferencia predeterminada",
		
		
		kN2PsqlCheckInNotaMenuItemKey,			"Depositar nota",
		kN2PsqlCheckOutNotaMenuItemKey,			"Retirar nota",
		kN2PsqlImportNotaToActServerMenuItemKey, "Importar nota",
		kN2PsqlDetachNotaMenuItemKey,			"Separar nota",
		
		kN2PSQLShowEdgesStringKey,					"Oculta estatus de nota",
		// ----- Misc strings
		// ----- Misc strings
		kN2PsqlAboutBoxStringKey,			kN2PsqlPluginName ", version " kN2PsqlVersion " por " kN2PsqlAuthor "\n\nhttp:www.interlasa.com",
		kN2PsqlItemOneStringKey,	"ExpPDFAndDB::ItemOne",
		
		kN2PInOutNoServerPrefSelectedStringKey,	"Debe seleccionar una conexion a la DB",
		
		
		kN2PSQLNoExistenNotasSobreDocumentoStringKey,		"No hay notas en este documento.",
        
        kN2PSQLAlertSeleccionarCajaDeTextoStringKey,		"Esta no es una caja de texto o no tiene una seleccionada",
       
       	kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey,			"Esta no es una nota de News2Page",
       	
       	kN2PsqlStTextUpdateMakeNotasTextKey,				"Actualiza Contenidos",
       	
       	kN2PsqlMakeUpdateFinishedStringKey,					"Actualizacion finalizada",
       	kInfoAlertNotaEditPorOtroUsuarioStringKey,          "Esta nota esta siendo editada por: ",
       	kInfoAlertCheckOutArticleCorrectStringKey,			"Retirar nota",
       	kInfoAlertNotaDeOtraBDStringKey, 					"Esta nota no puede ser editada pues pertenece a una Base de datos ajena",
       	kInfoAlertCheckInNotaStringKey,                     "Nota editada",
       	kN2PSQLOkConecctionDSNStringKey, 						"La conexion DSN fue probada satisfactoriamente",
       	kN2PSQLErrorConecctionDSNStringKey,					"La conexion DSN fallÛ",
       	kN2PsqlDlgCheckInArticleTitleKey,					"Depositar nota",
       	
       	kN2PsqlStcTextInCopyInDesingTextKey,				"InCopy",
       	
       	kN2PsqlStcTextDBConectionStringKey,					"Conexion de BD:",
       	
       	kN2PImpNomPagInvalidoStringKey,				"Nombre de la p·gina",
       	kN2PsqlNotaDeOtroServidorStringKey, "Esta nota pertenece a un servidor remoto",
		kN2PSqlNotaPerteneceaOtroGrupoStringKey, "El grupo del usuario no esta autorizado",
		
		kN2PsqlListPhotoPanelTitleKey, 						"Foto News2Page",
		kN2PSQLDeseaImportNotaStringKey,					"øDesea importar esta nota al servidor local?",
		kCantCheckOutserverRemoteStringKey,					"No puede retirar esta nota pues se encuentra en un servidor remoto   ", 
		kN2PsqlServidorLocalStringKey, 						"Solo puede retirar notas del servidor local",
		kFotoYaFueColocadaStringKey,						"La foto ya ha sido colocada.",
		kN2PSQLPathOfServerFileLabelKey,					"Ruta al servidor de archivos:",
		kN2PsqlSelectPathStringKey,							"Seleccione ruta",
		kN2PSQLDeseaFluirNotaCIdSecDifteAPageStringKey,		"La seccion de la nota es diferente de la seccion de la p·gina. Deseas permitir?",
		kN2PSQLAllowString,								"Permitir",
		
		kN2PSQLYouCouldSaveThisPageString,				"Debe guardar esta p·gina en el servidor de News2Page",
		kN2PsqlCrearSpaceAndNotaMenuItemKey,			"Crear nota",
		
		kN2PsqlAlertCNSinTextoStringKey,			"El contenido de la nota se encuentra vacio",
		kN2PsqlAlertPFSinTextoStringKey,			"El pie de foto de la nota se encuentra vacio",
 		kN2PsqlAlertBaSinTextoStringKey,			"El balazo de la nota se encuentra vacio",
		kN2PsqlAlertTiSinTextoStringKey,			"El tÌtulo de la nota se encuentra vacio",
		
		kN2PsqlCantCheckInArticleStringKey, 		"Un elemento de la nota esta vacio, la nota no puede depositarse",
		kN2PSQLUserCantLigarNotaAPagStringKey, 	"El usuario no esta autorizado para colocar esta nota.",
		kN2PSQLUserCantDesligarNotaAPagStringKey,	"El usuario no esta autorizado para remover esta nota", 
		kN2PSQLCantCrearNotaInDStringKey,			"El usuario no esta autorizado para crear esta nota",
		kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey, 	"El usuario no esta autorizado para retirar esta nota.",

		kN2PsqlCrearPaseDeNotaMenuItemKey,			"Establecer pase",
		kN2PSQLOnlyPageJumpsStringKey,				"Solo pases",
		
		kN2PSQLCantFotoAsignadasStringKey,			"Fotos asignadas",
		kN2PSQLLogOffTaskKey,						"N2PLogOffTask",
		kN2PSQLTimeTaskLogOffStringKey,				"Tiempo busqueda act.:",
		kN2PsqlLogInMenuItemKey,					"Iniciar sesion",
		kN2PsqlLogOutMenuItemKey,					"Cerrar sesion",
		kN2PSQLFailedWhenSavePrefStringKey,			"OcurriÛ un error al tratar de guardar las preferencias",
		kN2PSQLPhotoActivarStringKey,				"a ser colocado",
		kN2PSQLElementosMostradosEstanEnUsoStringKey,		"Los siguientes elementos de la p·gina estan siendo editados por otro usuario",
		
		kN2PsqlDetachPhotoStringMenuKey,			"Separar imagen.",
		kN2PSQLPathOfServerFileImagenesLabelKey,	"Servidor de imagen:",
		kN2PSQLRutaDServidorRespNotasLabelKey,		"Servidor de resplado:",
		kN2PSQLDebeSerChecauteadaStringKey,			"Checkout articulo, antes de desligar",
		kN2PDeseasDesligarEstaNotaStringKey,		"Separar nota seleccionada?",
		kN2PDesarReemplazarelElementoStringKey, 		"Este elemento de articulo ya ha sido colocado. Deseas reemplazarlo?",
		kN2PsqlStcTextEnGuiaTextKey,				"Texto en Guia:",
		kN2PMensagCrearComoNotaHijaStringKey,		"Deseas crear como nota hija?",
		kN2PsqlDlgCrearNotaHijaTitleKey,			"News2Page",
		
		
		kN2PBuscarNotaDPaginaStringKey,				"Mostar notas de pagina",
		kN2PBuscarNotasStringKey,					"Buscar notas",
		kN2PActualizarTodoStringKey,				"Actualizar",
		kN2PSQLDebeDepositarNotaPadreStringKey,		"La nota padre debe estar depositada.",
		kInfoAlertNotaASidoDirigidoAUsuarioStringKey,	 "El usuario no esta autorizado para retirar esta nota. Esta Nota a sido enviada a ",
		kN2PSQLExportNotasATablaWEBStringKey,			"Exportar notas a tabla web.",
		kN2PSQLEstatusParaExportarStringKey,			"Estado para exportar:",
		kN2PsqlUltimoUsuarioQModNotaStringKey,			"Ult. usuario:",
		kN2PSQLMuestraDirigidoAEnCajasDNotasStringKey,	"Mostrar dirigido a",
		
		kN2PsqlLigarImagensANotaStringMenuKey,			"Ligar Notas",
		kN2PsqlAceptarImagenesDNotaMenuItemKey,			"Relacionar imagen",
		kN2PsqlNoexistenCajasDImagenSelecStringKey,		"No existen cajas de imagen en tu seleccion.",
		kN2PsqlImegenesLigadasANotasStringKey,			"Las imagenes han sido ligadas a la nota seleccionada.",
		kN2PsqlDuplicarPaginaMenuItemKey,				"Duplicar pagina",
		kN2PsqlNoSeEncontraronNotasPlugInMenuKey,		"No se encontraron notas relacionadas.",
		
		kN2PSQNombreEstatusParaNotasCompletasStringKey,	"Estado final de notas.",
	 	kN2PSQLExportarPaginaComoPDFStringKey,		"Exportar PDF.",
	 	kN2PSQRutaDePaginaComoPDFStringKey,				"Ruta de archivos pdf",
	 	kN2PsqlErrorAlertPaginaBorradaEnBDString,		"Esta pagina contiene registros que no existen sobre la base de datos. Por favor utilize la opcion desligar pagina, para ingresar nuevamente la pagina hacia el sistema.",
		kN2PSQLBackUpPaginasEnCheckInStringKey,			"Crear respaldo de paginas",
		kN2PsqlDetachPaginaMenuItemKey,					"Separar Pagina",
		kN2PDeseasDesligarPaginaStringKey,				"Esta seguro que desea separar la pagina del sistema editorial?",
		kN2PPaginaDesligadaStringKey,					"Pagina separada",
		kN2PHacerUpdateAntesDContinuarStringKey,					"El nombre de la pagina cambio, haga un update antes de continuar.",
		kN2PSQLValidaComilasYDComillasStringKey,		"La guia no debe contener ningun tipo de comilla o apostrofo.",
		kN2PSQLEsCampoGuiaObligatorioStringKey,			"Guia de nota obligatoria.",
		kN2PSQLGuiaNoVaciaStringKey,			"La guia de la nota no debe estar vacia.",
		kN2PSQLEsEnviarNotasHijasAWEBoStringKey,		"Enviar notas hijas a notasweb.",
		
		kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoStringKey,	"Envia en mayusculas titulo en web.",
		kN2PSQLCkBoxEnviaAWEBSinRetornosTituloStringKey,	"Borra enters sobre titulo en web",
		kN2PSQLCkExportPDFPresetsStringKey,				"PDF 1 en Listo",
		kN2PSQLCkExportPDFPresetsEditWEBStringKey,		"PDF 2 en Listo",
		kN2PSQLAcabasDBorrarUnElementoDLaNotaPendienteStringKey,	"La caja que esta eliminando se desligara de la pagina, si desea mantenerla seleccione Deshacer del menu Edicion.",
		kN2PSQLAlertCajaDeTextoConNotaN2PDesligarNotaStringKey,	"No puede borrar una caja de una nota de N2P, debes desligar la caja o separar la nota de la pagina.",
		
		kN2PSQLNoSeFechaValidaKey,			"Fecha incorrecta.",
		kN2PsqlCreatePieDFotoStringMenuKey,	"Crear pie de foto",
		
		kN2PSQnameParaStyleCreditoStringKey,			"Estilo de Parrafo de Credito.",
		kN2PSQLImagesFolderInServerFileImagesLabelKey,	"Carpeta de imagen:",
		
			kYinPanelTitleKey,								"News2Page",
		kYangPanelTitleKey,								"General",
		kBscSlDlgDialogTitleKey,						"Preferencias",
		
		kN2PHemerotecaPanelTitleKey,					"Hemeroteca",
		kN2PHmeDSNNameStringKey,						"DSN:",
		kN2PHmeDSNUserLoginStringKey,					"Usario:",
		kN2PHmeDSNPwdLoginStringKey,					"ContraseÒa:",
		
		kN2PHmeFTPServerStringKey,						"Servidor FTP:",
		kN2PHmeFTPUserStringKey,						"Usario:",
		kN2PHmeFTPPwdStringKey,						"ContraseÒa:",
		kN2PHmeFTPFolderStringKey,					"Path:",
		kN2PHmteSendToHmcaStringKey,				"Enviar a Hemeroteca",
		
		kN2PResizePreviewViewStringKey,	"Resize Preview",
		kN2PGenerals2PanelTitleKey,		"Otros",
		kN2PHeightSizeStringKey,		"Alto",
		kN2PWidthSizeStringKey,			"Ancho",
		kN2PUsarGuiasStringKey,		"Utilizar guias predefinidas",
		kN2PSQLURLWebServicesLabelKey,				"URL webservices",
		kN2PsqlActulizarDisenioStrKey,	"Update Design",

*/               	// ----- Menu strings
		kN2PsqlCompanyKey,					kN2PsqlCompanyValue,					
		kN2PsqlAboutMenuKey,					kN2PsqlPluginName,
		kN2PsqlPluginsMenuKey,				kN2PsqlPluginName,
		kN2PsqlItemOneMenuItemKey,	"ItemOne[US]",
		kN2PsqlAboutThisPlugInMenuKey,			"About News2page",	

		// ----- Command strings

		
		// ----- Window strings

		kN2PsqlDlgDetachArticleTitleKey,	"Detach article",
		// ----- Panel/dialog strings
		kN2PsqlPanelTitleKey,				kN2PsqlPluginName,
		kN2PsqlStaticTextKey,				kN2PsqlPluginName,
		kN2PsqlDlgBusquedaTitleKey,		"Search",
		
		kN2PsqlStcTextPageTextKey,				"Page ID",
		kN2PsqlStTextTituloTextKey,				"Headline",
		kN2PsqlStTextBalazoTextKey,				"Summary",
		kN2PsqlStTextNotaTextKey,					"Article",
		kN2PsqlStTextPieTextKey,					"Caption",
		
		kN2PsqlStTextSeccionTextKey,				"Section",
		kN2PsqlStTextStatusTextKey,				"Status",
		kN2PsqlStTextPubliTextKey,				"Issue",
		
		kN2PsqlStTextFechaTextKey,				"Date",
		kN2PsqlBotonSubmitFechaTextKey,			"Article check in",
		kN2PsqlBotonPDFFechaTextKey,				"To generate PDF",
		kN2PsqlBotonPDFBusquedaTextKey,			"Search",
		kN2PsqlBotonLimpiarCamposTextKey,			"Clean Items",
		kN2PsqlDepositarMenuItemKey,		"Page check in",
		kN2PsqlRetirarMenuItemKey,		"Page check out",
		kN2PsqlSalvarMenuItemKey,			"Save revision",
		kN2PsqlDepInCopyMenuItemKey,		"InCopy check In",
		kN2PsqlStTextArticlesTextKey,		"Other articles",
		
		kN2PsqlBotonBuscarNotaTextKey,			"New search",
		kN2PsqlVistaPreviaTextKey,				"Article preview",
		kN2PsqlStcThisArticleTextKey,				"Guide:",
		kN2PsqlStTextRoutedToTextKey,				"Routed to",
		kN2PsqlStTextMAkeUpdateTextKey,			"Make update",
		kN2PsqlStTextReciveUpdateTextKey,			"Receive update",
		kN2PsqlMsgAlertHacerSeleccionTextKey,		"You must select an option.",
		// ----- Error strings

		kN2PsqlNoExisteDocumentoStringKey,			"There is not document opened.",
		kN2PsqlUpdateFinishedStringKey,				"Update finished.",
		kN2PsqlPDFExportadoStringKey,				"",
		kN2PsqlNewRegisterOnDBStringKey
			
		kN2PSQLPreferenciasMenuItemStringKey,		"News2Page Preferences",
		kN2PSQLPreferDlgTitleKey,			"News2Page Preferences",
		kN2PsqlRefreshStatusNotaStringKey,	"Refresh status",
		kN2PSQLServerIPLabelKey	,			"IP Address",
		kN2PSQLOptionStringKey,				"SQL server",				
		kN2PSQLFileMAkerStringKey	,		"FileMaker"	,
		kN2PSQLDBOpcionesStringKey	,		"Options",	
		kN2PSQLNameDBLabelKey		,		"DB name",
		kN2PSQLDSNNameLabelKey	,		"DSN:",			
		kN2PSQLPasswordDBLabelKey	,		"Password",
		kN2PSQLNameUsuarioDBLabelKey,		"User",
		kN2PsqlNewDBConexionStringKey,		"New DB",
		kN2PsqlDeleteDBConexionStringKey,		"Delete DB",
		kN2PsqlSetLocalDBConexionStringKey,	"Default",
		kN2PSQLNameConecctionDBLabelKey,	"Conection name",
		kN2PsqlTestDBConexionStringKey,		"Test",
		kN2PsqlCancelDBConexionStringKey,	"Cancel",
		kN2PsqlGuardarDBConexionStringKey,	"Save",
		kN2PsqlEditDBConexionStringKey,		"Edit",
		
		kN2PSQLSeguroBorrarPrefereStringKey,	"You are about to delete this preference",
		kN2PSQLAlmenosUnaSeleccionStringKey,		"You must select a preference",
		kN2PSQLErrorDuranteBorradoStringKey,		"Error during the deletion of the file.",
		kN2PSQLPreferenciaBorradaStringKey,		"The preference was deleted",
		kN2PSQLSeguroBorrarPrefereDefaultStringKey, "You are about to delete the default preference",
 		kN2PSQLShowEdgesStringKey,					"Hide article status ",
 		
 		kN2PsqlCheckInNotaMenuItemKey,			"Check in Article  ",
 		kN2PsqlCheckOutNotaMenuItemKey,			"Check out Article  ",
 		kN2PsqlImportNotaToActServerMenuItemKey, "Import article",
 		kN2PsqlDetachNotaMenuItemKey,			"Detach article",
 		
		// ----- Misc strings
		kN2PsqlAboutBoxStringKey,			kN2PsqlPluginName ", version " kN2PsqlVersion " by " kN2PsqlAuthor "\n\nhttp://www.interlasa.com",
		kN2PsqlItemOneStringKey,	"ExpPDFAndDB::ItemOne",	
		
		
		kN2PInOutNoServerPrefSelectedStringKey,	"You must select a database connection",
		
		
		kN2PSQLNoExistenNotasSobreDocumentoStringKey,		"There aren´t any articles on this document",
		
		kN2PSQLAlertSeleccionarCajaDeTextoStringKey,		"This is not a text frame or you dont have a text frame selected.",
		kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey,			"This is not an article from News2Page",
		kN2PsqlStTextUpdateMakeNotasTextKey,				"Update Articles",
		kN2PsqlMakeUpdateFinishedStringKey,					"Update finished",
		
		kInfoAlertNotaEditPorOtroUsuarioStringKey,          "The article is being edited by :",
		kInfoAlertCheckOutArticleCorrectStringKey,			"Article check out",
		kInfoAlertNotaDeOtraBDStringKey, 					"User is not authorized to modify this page",
		kInfoAlertCheckInNotaStringKey,                     "Article check in",
		kN2PSQLOkConecctionDSNStringKey, 					"The DSN connection was tested successfully",
		kN2PSQLErrorConecctionDSNStringKey,					"The DSN connection has failed",
		kN2PsqlDlgCheckInArticleTitleKey,					"Article check In",
		
		kN2PsqlStcTextInCopyInDesingTextKey,				"InCopy",
		kN2PsqlStcTextDBConectionStringKey,					"Database Connection:",
		
		kN2PImpNomPagInvalidoStringKey,				"Page name",
		
		kN2PsqlNotaDeOtroServidorStringKey, "This article is from a remote server",
		
		kN2PSqlNotaPerteneceaOtroGrupoStringKey, "The user´s group is not authorized",
		
		kN2PsqlListPhotoPanelTitleKey, 						"News2page Photo",
		kN2PSQLDeseaImportNotaStringKey,					"Do you wish import this article to local server?",
		kCantCheckOutserverRemoteStringKey,					"You can´t check out the article because it´s on a remote server", 
		kN2PsqlServidorLocalStringKey, "Only articles from the local server can",
		kFotoYaFueColocadaStringKey,						"The photo is already placed.",
		kN2PSQLPathOfServerFileLabelKey,					"File Server Path:",
		kN2PsqlSelectPathStringKey,							"Select path",
		kN2PSQLDeseaFluirNotaCIdSecDifteAPageStringKey,		"The article section is different from the page section. You want Allow?",
		kN2PSQLAllowString,								"Allow",
		
		kN2PSQLYouCouldSaveThisPageString,				"You should save this page in a News2Page server",
		kN2PSQLThisIsElemDNotaString,					"You must select an article element",
		kN2PsqlCrearSpaceAndNotaMenuItemKey,			"Create Article",
		
		kN2PsqlAlertCNSinTextoStringKey,			"The story article is empty",
		kN2PsqlAlertPFSinTextoStringKey,			"The story caption is empty.",
 		kN2PsqlAlertBaSinTextoStringKey,			"The story summary is empty.",
		kN2PsqlAlertTiSinTextoStringKey,			"The story headline is empty.",
		kN2PsqlCantCheckInArticleStringKey, 		"an element is empty, the article can´t be checked in",
		
		kN2PSQLUserCantLigarNotaAPagStringKey, 		"User is not authorized to place this article",
		kN2PSQLUserCantDesligarNotaAPagStringKey,	"User is not authorized to remove this article", 
		kN2PSQLCantCrearNotaInDStringKey,			"User is not authorized to create this article",
		kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey, 	"User is not authorized to remove this article",
		
		kN2PsqlCrearPaseDeNotaMenuItemKey,			"Setup page jump",
		kN2PSQLOnlyPageJumpsStringKey,				"Only page jumps",
		
		kN2PSQLCantFotoAsignadasStringKey,			"Assigned photos",
		kN2PSQLLogOffTaskKey,						"N2PLogOffTask",
		kN2PSQLTimeTaskLogOffStringKey,				"Update time",
		kN2PsqlLogInMenuItemKey,					"Login",
		kN2PsqlLogOutMenuItemKey,					"Logout",
		kN2PSQLFailedWhenSavePrefStringKey,			"An error occurred while trying to save preferences",
		kN2PSQLPhotoActivarStringKey,				"to be placed",
		kN2PSQLElementosMostradosEstanEnUsoStringKey,		"The following items  of a page are being edited by another user",
		
		kN2PsqlDetachPhotoStringMenuKey,			"Detach Picture.",
		kN2PSQLPathOfServerFileImagenesLabelKey,	"Images server path:",
		kN2PSQLRutaDServidorRespNotasLabelKey,		"Backup server path:",
		kN2PSQLDebeSerChecauteadaStringKey,			"Checkout Article before detach it",
		kN2PDeseasDesligarEstaNotaStringKey,		"Detach selected article?",
		kN2PDesarReemplazarelElementoStringKey, 		"This article element it is already placed. Do you want to replace it?",
		
		kN2PSQLShowOverTextStringKey, "Show Oveset Text",
		
		kN2PsqlStcTextEnGuiaTextKey,				"Text on guide:",
		kN2PMensagCrearComoNotaHijaStringKey,		"Do you wish create a children article?",
		kN2PsqlDlgCrearNotaHijaTitleKey,			"News2Page",
		
		kN2PBuscarNotaDPaginaStringKey,				"Page articles",
		kN2PBuscarNotasStringKey,					"Articles search",
		kN2PActualizarTodoStringKey,				"Update",
		kN2PSQLDebeDepositarNotaPadreStringKey,		"The main article must be checked in.",
		kInfoAlertNotaASidoDirigidoAUsuarioStringKey,	"User is not authorized to remove this article. This Article sended to ",
		kN2PSQLExportNotasATablaWEBStringKey,			"Export tables to web table.",
		kN2PSQLEstatusParaExportarStringKey,			"Status to export:",
		kN2PsqlUltimoUsuarioQModNotaStringKey,			"Last user:",
		kN2PSQLMuestraDirigidoAEnCajasDNotasStringKey,	"Show routed to",
		
		kN2PsqlLigarImagensANotaStringMenuKey,			"Link Articles",
		kN2PsqlAceptarImagenesDNotaMenuItemKey,			"Link image",
		kN2PsqlNoexistenCajasDImagenSelecStringKey,		"There aren't any image frames in your selection.",
		kN2PsqlImegenesLigadasANotasStringKey,			"The images had been linked.",
		kN2PsqlDuplicarPaginaMenuItemKey,				"Duplicate page",
		kN2PsqlNoSeEncontraronNotasPlugInMenuKey,		"No related stories found.",
		
		kN2PSQNombreEstatusParaNotasCompletasStringKey,	"Final story status.",
	 	kN2PSQLExportarPaginaComoPDFStringKey,		"Export PDF.",
	 	kN2PSQRutaDePaginaComoPDFStringKey,				"PDF file path",
	 	
	 	kN2PsqlErrorAlertPaginaBorradaEnBDString,		"This page contains registries that do not exist on the data base. Please utilize the option to separate page, to again enter the page towards the system.",
	 	kN2PSQLBackUpPaginasEnCheckInStringKey,			"Create page backup.",
	 	kN2PsqlDetachPaginaMenuItemKey,					"Detach Page",
	 	kN2PDeseasDesligarPaginaStringKey,				"You are about to detache this page of system?",
	 	kN2PPaginaDesligadaStringKey,					"Page detached.",
	 	kN2PHacerUpdateAntesDContinuarStringKey,					"The page name has changed, make an update before continue.",
	 	kN2PSQLValidaComilasYDComillasStringKey,		"The guide reference must not contain any kind of quotation marks.",
	 	kN2PSQLEsCampoGuiaObligatorioStringKey,			"Obligatory note guide.",
	 	kN2PSQLGuiaNoVaciaStringKey,					"The guide reference must not emty.",
	 	kN2PSQLEsEnviarNotasHijasAWEBoStringKey,		"Send children to notasweb.",
		
		kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoStringKey,	"All caps in web photocaption.",
		kN2PSQLCkBoxEnviaAWEBSinRetornosTituloStringKey,	"Delete enters in web headline",
		kN2PSQLCkExportPDFPresetsStringKey,				"PDF 1 on Page Done",
		kN2PSQLCkExportPDFPresetsEditWEBStringKey,		"PDF 2 on Page Done",
		kN2PSQLAcabasDBorrarUnElementoDLaNotaPendienteStringKey,	"The text frame that you just deleted will be unlinked from the page, select undo if you want to keep it",
		kN2PSQLAlertCajaDeTextoConNotaN2PDesligarNotaStringKey,		"You can't erase a N2P text box, to erase it you must unlink it or detach it from the N2P System.",
		
		kN2PSQLNoSeFechaValidaKey,			"Incorrect Date",
		kN2PsqlCreatePieDFotoStringMenuKey,	"Create caption",
		
		kN2PSQLAlertCajaDeTextoYaEsElemNotaN2PStringKey,	"This text box already is a note element, verifique seleccion.",
		kN2PsqlSoloSelect1CajaDImagenSelecStringKey,		"It only must select a box of image.",
		kN2Psql_NoEsFotoLigadaAPagina_LigarPrimeroSelecStringKey,"This image is not a photo of the system, please to link image.",
		kN2Psql_Yatiene_UnPiedFotoLigado_FluirSelecStringKey,	"This image already has a photo foot, it must flow from window.",
		kN2PsqlPieDFotoCreadoStringKey,					"Added photo caption.",
		
		kN2PSQnameParaStyleCreditoStringKey,			"Credit paragraph style",
		kN2PSQLImagesFolderInServerFileImagesLabelKey,	"Image folder:",
		
		
		kYinPanelTitleKey,								"News2Page",
		kYangPanelTitleKey,								"General",
		kBscSlDlgDialogTitleKey,						"Preferences",
		
		kN2PHemerotecaPanelTitleKey,					"Hemeroteca",
		kN2PHmeDSNNameStringKey,						"DSN:",
		kN2PHmeDSNUserLoginStringKey,					"User:",
		kN2PHmeDSNPwdLoginStringKey,					"Password:",
		
		kN2PHmeFTPServerStringKey,						"FTP Server:",
		kN2PHmeFTPUserStringKey,						"User:",
		kN2PHmeFTPPwdStringKey,						"Password:",
		kN2PHmeFTPFolderStringKey,					"Path:",
		kN2PHmteSendToHmcaStringKey,				"Send to hemeroteca",
		
		kN2PResizePreviewViewStringKey,	"Rezise Preview",
		kN2PGenerals2PanelTitleKey,		"Others",
		kN2PHeightSizeStringKey,		"Height",
		kN2PWidthSizeStringKey,			"Width",
		kN2PUsarGuiasStringKey,			"Utilizar guias predefinidas",
		kN2PSQLURLWebServicesLabelKey,				"URL webservices",
		kN2PsqlActulizarDisenioStrKey,	"Update Design",

	}
};

#endif // __ODFRC__

//  Code generated by DollyXS code generator