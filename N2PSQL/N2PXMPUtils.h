/*
 *  N2PXMPUtils.h
 *  News2Page
 *
 *  Created by ADMINISTRADOR on 19/03/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "IMetaDataAccess.h"
#ifndef __XMPClass_H_DEFINED__
#define __XMPClass_H_DEFINED__

class IDocument;


/** A collection of miscellaneous utility methods used by this plug-in.
 @ingroup paneltreeview
 */
class N2PXMPUtils
	{
	public:
		N2PXMPUtils(IDocument* doc);				
		
		~N2PXMPUtils();
		
		bool16 initMannager(IDocument* doc);
		

		PMString Get(PMString variableXMP);
		
		bool16	 Set(PMString variableXMP,PMString value);
	private: 
		
		
		void setWhiteValor(PMString& valor);
		int32 NextNumberElementInPathArticle(PMString PathArticle);
		
		IMetaDataAccess* dummyMetaDataAccess;
		PMString kNS1;
		PMString kNS2;
		PMString dummy_type;
		int32 fnumDArticles;
	};
#endif // __XMPClass_H_DEFINED__