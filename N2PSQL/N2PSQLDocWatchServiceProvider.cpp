/*
//	File:	N2PsqlDocWchServiceProvider.cpp
//
//	Date:	6-Mar-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:

// Implementation includes:
#include "CServiceProvider.h"
#include "K2Vector.h"
#include "DocumentID.h"
#include "N2PsqlID.h"


/** N2PsqlDocWchServiceProvider
	registers as providing the service of responding to a group of document
	file action signals.  See the constructor code for a list of the
	signals this service responds.

	N2PsqlDocWchServiceProvider implements IK2ServiceProvider based on
	the partial implementation CServiceProvider.


	@author John Hake
*/
class N2PsqlDocWchServiceProvider : public CServiceProvider
{
	public:

		/**
			Constructor initializes a list of service IDs, one for each file action signal that N2PsqlDocWchResponder will handle.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PsqlDocWchServiceProvider(IPMUnknown *boss);
		
		/**
			Destructor.  
		*/
		virtual	~N2PsqlDocWchServiceProvider();

		/**
			GetName initializes the name of the service.
			@param pName Ptr to PMString to receive the name.
		*/
		virtual void GetName(PMString * pName);

		/**
			GetServiceID returns a single service ID.  This is required, even though
			GetServiceIDs() will return the complete list initialized in the constructor.
			This method just returns the first service ID in the list.
		*/
		virtual ServiceID GetServiceID();

		/**
			IsDefaultServiceProvider tells the application this service is not the default service.
		*/
		virtual bool16 IsDefaultServiceProvider();
		
		/**
			GetInstantiationPolicy returns a InstancePerX value to indicate that only
			one instance per session is needed.
		*/
		virtual InstancePerX GetInstantiationPolicy();

		/**
			HasMultipleIDs returns kTrue in order to force a call to GetServiceIDs().
		*/
		virtual bool16 HasMultipleIDs() const;

		/**
			GetServiceIDs returns a list of services provided.
			@param serviceIDs List of IDs describing the services that N2PsqlDocWchServiceProvider registers to handle.
		*/
		virtual void GetServiceIDs(K2Vector<ServiceID>& serviceIDs);

	private:

		K2Vector<ServiceID> fSupportedServiceIDs;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PsqlDocWchServiceProvider, kN2PsqlDocWchServiceProviderImpl)


/* DocWchActionComponent Constructor
*/
N2PsqlDocWchServiceProvider::N2PsqlDocWchServiceProvider(IPMUnknown *boss)
	: CServiceProvider(boss)
{
	// Add the service IDs we want the associated responder to handle.
	//  (See DocumentID.h)
	fSupportedServiceIDs.clear();
	
	//	NewDoc
	fSupportedServiceIDs.push_back(kBeforeNewDocSignalResponderService);
	fSupportedServiceIDs.push_back(kDuringNewDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterNewDocSignalResponderService);

	//	OpenDoc
	fSupportedServiceIDs.push_back(kBeforeOpenDocSignalResponderService);
	fSupportedServiceIDs.push_back(kDuringOpenDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterOpenDocSignalResponderService);

	//	SaveDoc
	fSupportedServiceIDs.push_back(kBeforeSaveDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterSaveDocSignalResponderService);

	//	SaveAsDoc
	fSupportedServiceIDs.push_back(kBeforeSaveAsDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterSaveAsDocSignalResponderService);

	//	SaveACopyDoc
	fSupportedServiceIDs.push_back(kBeforeSaveACopyDocSignalResponderService);
// 	fSupportedServiceIDs.push_back(kDuringSaveACopyDocSignalResponderService);  Broken in b188
	fSupportedServiceIDs.push_back(kAfterSaveACopyDocSignalResponderService);

	//	RevertDoc
	fSupportedServiceIDs.push_back(kBeforeRevertDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterRevertDocSignalResponderService);

	//	CloseDoc
	fSupportedServiceIDs.push_back(kBeforeCloseDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterCloseDocSignalResponderService);

	if (fSupportedServiceIDs.size()<=0)
	{
		ASSERT_FAIL("N2PsqlDocWchServiceProvider must support at least 1 service ID");
		fSupportedServiceIDs.push_back(kInvalidService);
	}

}

/* DocWchActionComponent Dtor
*/
N2PsqlDocWchServiceProvider::~N2PsqlDocWchServiceProvider()
{
}

/* GetName
*/
void N2PsqlDocWchServiceProvider::GetName(PMString * pName)
{
	pName->SetCString("DocWatch Responder Service");
}

/* GetServiceID
*/
ServiceID N2PsqlDocWchServiceProvider::GetServiceID() 
{
	// Should never be called given that HasMultipleIDs() returns kTrue.
	return fSupportedServiceIDs[0];
}

/* IsDefaultServiceProvider
*/
bool16 N2PsqlDocWchServiceProvider::IsDefaultServiceProvider()
{
	return kFalse;
}

/* GetInstantiationPolicy
*/
IK2ServiceProvider::InstancePerX N2PsqlDocWchServiceProvider::GetInstantiationPolicy()
{
	return IK2ServiceProvider::kInstancePerSession;
}

/* HasMultipleIDs
*/
bool16 N2PsqlDocWchServiceProvider::HasMultipleIDs() const
{
	return kTrue;
}

/* GetServiceIDs
*/
void N2PsqlDocWchServiceProvider::GetServiceIDs(K2Vector<ServiceID>& serviceIDs)
{
	// Append a service IDs for each service provided. 
	for (int32 i = 0; i<fSupportedServiceIDs.size(); i++)
		serviceIDs.push_back(fSupportedServiceIDs[i]);

}


// End, N2PsqlDocWchServiceProvider.cpp.


