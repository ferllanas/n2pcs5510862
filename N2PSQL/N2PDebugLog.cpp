/*
 *  N2PDebugLog.cpp
 *  News2Page
 *
 *  Created by Fernando Llanas on 05/01/09.
 *  Copyright 2009 Interlasa.com. All rights reserved.
 *
 */

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "FileUtils.h"

#include "N2PDebugLog.h"

#include"N2PsqlID.h"


FILE* stream;


class N2PSQLDebugLogpl : public CPMUnknown<IN2PSQLDebugLog>
{
public:
		N2PSQLDebugLogpl (IPMUnknown *boss);
	
	virtual void IniciaDebug();
	
	virtual void printmsg(const PMString& msn);
	
	virtual void TerminaDebug();
	
private:
	PMString getHora();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
 */
CREATE_PMINTERFACE(N2PSQLDebugLogpl, kN2PSQLDebugLogImpl)


/* HelloWorld Constructor
 */
N2PSQLDebugLogpl::N2PSQLDebugLogpl(IPMUnknown* boss) 
: CPMUnknown<IN2PSQLDebugLog>(boss)
{
}

/*
 Update TextFrame To DB
 */
void N2PSQLDebugLogpl::printmsg(const PMString& msn)
{	
	if( stream!=NULL)//cfn->fileopen(&mode))!=NULL)// 
	{
		fprintf(stream,"%s->%s\n", this->getHora().GrabCString(),msn.GrabCString());
		
	}
	
}

void N2PSQLDebugLogpl::IniciaDebug()
{	
#if defined MACINTOSH
	PMString Default_Prefer_Path = "Desarrollo9:debugTimeLog.log";
#else
	PMString Default_Prefer_Path = "C:\\Documents and Settings\\ruben\\Escritorio\\debugTimeLog.log";
#endif
	
	PMString  msn = "Inicio debug...";
 	stream  =FileUtils::OpenFile(Default_Prefer_Path.GrabCString(),"w+");
	
	if( stream!=NULL)//cfn->fileopen(&mode))!=NULL)// 
	{
		fprintf(stream,"%s->%s\n", this->getHora().GrabCString(),msn.GrabCString());
		
	}	
}

void N2PSQLDebugLogpl::TerminaDebug()
{	
	PMString  msn = "Fin debug...";
  	if( stream  !=NULL)//cfn->fileopen(&mode))!=NULL)// 
	{
		fprintf(stream,"%s->%s\n", this->getHora().GrabCString(),msn.GrabCString());
		fclose(stream);
		
	}
	
}

PMString N2PSQLDebugLogpl::getHora()
{
   	time_t hora_actual;
   	struct tm *tiempohora_actual;
	
   	hora_actual = time( NULL );		
	
	tiempohora_actual = gmtime( &hora_actual );
	PMString hora_actualString = asctime(tiempohora_actual) ;
	
	return hora_actualString;
	
}
