//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/common/N2PSQLPhotoListBoxHelper.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #8 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __SDKListBoxHelper_h__
#define __SDKListBoxHelper_h__

class IPMUnknown;
class IControlView;

/**
	Sample code that hides some of the detail of working with the list-box API.
	The list box could be on a panel or a dialog.

	
*/
class N2PSQLPhotoListBoxHelper
{
public:
	/**
		Constructor.
		@param fOwner reference to boss object using this helper.
		@param pluginID plug-in where resources can be found
		@param listBoxID the list box widget ID
		@param owningWidgetID could be a panel or a dialog this listbox is on
	*/
	N2PSQLPhotoListBoxHelper(IPMUnknown* fOwner, const PluginID& pluginID, const WidgetID& listBoxID, const WidgetID& owningWidgetID);
	virtual ~N2PSQLPhotoListBoxHelper();

	/**
		Add an element with the specified name at a given location in the listbox.
		@param displayName string value
		@param updateWidgetId the text widget ID within the cell
		@param atIndex specify the location, by default, the end of the list-box.
	*/
	//void AddElement( const PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2 /* kEnd */);
	void AddElement(  const PMString & RupaPhoto,
								const PMString & Titulo, 
								const PMString & DescripcionPhoto,
								const PMString & TituloPhoto, 
								const PMString & AutorPhoto, 
								const PMString & IDElementoPhoto,  int atIndex = -2 /* kEnd */);
	
	/**
		Method to remove a list-box element by position.
		@param indexRemove position of element to remove.
	*/
	void RemoveElementAt(int indexRemove);
	
	/**
		Method to remove the last element in the list-box.
	*/
	void RemoveLastElement();

	/**
		Query for the list-box on the panel or a dialog that is currently visible, assumes one visible at a time.
		@return reference to the current listbox, not add-ref'd.
	*/
	IControlView * FindCurrentListBox();

	/**
		Method to delete all the elements in the list-box.
	*/
	void EmptyCurrentListBox();

	/**
		Accessor for the number of elements in encapsulated list-box.
	*/
	int GetElementCount();
	
	
	/**
		Accesor para buscar el elemento seleccionado de la lista a partir de un widget que se encuentra sobre este elemento.
	*/
	int SelectElemListOfPictureDraged(InterfacePtr<IControlView> controlView);
	
	/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
	*/
	void ObtenerTextoDeSeleccionActual( WidgetID widget1,PMString &Text1, int32 indexSelected);
	
	
	void SetTextoEnSeleccionActual( WidgetID widget1, PMString &Text1, int32 indexSelected);
	
	void Mostrar_IconFotoColocada(int32 indexSelected, const WidgetID&  widgetId, const bool16& show );
	
	void ChechedCheckBox(int32 indexSelected,const WidgetID& widget1,const bool16& check);
	
private:
	// impl methods
	bool16 verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	/**
		helper method to a new list element widget.
	*/
	void AddListElementWidget(InterfacePtr<IControlView> & elView, 
								const PMString & RupaPhoto, 
								const PMString & Titulo, 
								const PMString & DescripcionPhoto,
								const PMString & TituloPhoto, 
								const PMString & AutorPhoto, 
								const PMString & IDElementoPhoto, 
								int atIndex);

	/**
		Helper method to remove the specified index.
	*/
	void removeCellWidget(IControlView * listBox, int removeIndex);
	
	void doPreview(const PMString& path, InterfacePtr<IControlView> & elView, WidgetID CustomPanelViewWidgetID);
	
	void SETStringOnWidgetOfElementList(InterfacePtr<IPanelControlData>& newElPanelData, const WidgetID & widget,  const PMString & stringText);


	IPMUnknown * fOwner;
	PluginID fOwningPluginID;
	WidgetID fListboxID;
	WidgetID fOwningWidgetID;
};


#endif // __SDKListBoxHelper_h__




