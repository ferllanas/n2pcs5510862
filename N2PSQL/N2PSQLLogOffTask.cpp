
//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/N2PSQLLogOffTask.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #6 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// interface includes
#include "IApplication.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IIdleTaskMgr.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "CAlert.h"
#include "IProgressBarManager.h"
#include "ProgressBar.h"
//nclude "RangeProgressBar.h"


#include "ISelectionManager.h" // required by selection templates.
#include "ICommandSequence.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IDialog.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ISelectableDialogSwitcher.h"
#include "IDocument.h"
#include "IDocumentUtils.h"
#include "IHierarchy.h"
#include "IPageList.h"

#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "SnapshotUtils.h"
#include "SDKLayoutHelper.h"
#include "StreamUtil.h"

// General includes:
#include "CPMUnknown.h"
#include "CmdUtils.h"	// so that the thing will compile (selectionasbtemplates.tpp)
#include "SelectionASBTemplates.tpp"
#include "ILayoutUIUtils.h"
#include "CAlert.h"
#include "FileUtils.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"
#include "SDKUtilities.h"

// General includes:

#include "CIdleTask.h"
#include "IDTime.h"

// Project includes
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"
#include "UpdateStorysAndDBUtilis.h"


#ifdef WINDOWS

	#include "..\N2PCheckInOut\ICheckInOutSuite.h"
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"
	#include "..\N2PCheckInOut\IN2PXMPUtilities.h"
	
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"
	
	#include "..\N2PFrameOverset\IN2PCTUtilities.h"	
	
#endif

#ifdef MACINTOSH


	#include "ICheckInOutSuite.h"
	#include "N2PCheckInOutID.h"
	#include "IN2PXMPUtilities.h"
	
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "IN2PSQLUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"
	
	#include "IN2PCTUtilities.h"	
	
#endif



const static int kN2PSQLLogOffExecInterval = 10*1000;	// Refresh every 5 minutes in case we add new images
// milliseconds between checks

/** Implements IIdleTask, to refresh the view onto the file system.
	Just lets us keep up to date when people add new assets.
	This implementation isn't too respectful; it just clears the tree and calls ChangeRoot.

	
	@ingroup paneltreeview
*/

class N2PSQLLogOffTask : public CIdleTask
{
public:

	/**	Constructor
		@param boss boss object on which this interface is aggregated
	*/
	N2PSQLLogOffTask(IPMUnknown* boss);

	/**	Destructor
	*/
	virtual ~N2PSQLLogOffTask() {}


	/**	Called by the idle task manager in the app core when the task is running
		@param appFlags [IN] specifies set of flags which task can check to see if it should do something
			this time round
		@param timeCheck [IN] specifies how many milliseconds before the next active task becomes overdue.
		@return uint32 giving the interval (msec) that should elapse before calling this back
	 */
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
protected:

	/**	Update the treeview.
	 */
	void refresh();
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PSQLLogOffTask, kN2PSQLLogOffTaskImpl)

/* Constructor
*/
N2PSQLLogOffTask::N2PSQLLogOffTask(IPMUnknown *boss)
	:CIdleTask(boss)
{
}


/* RunTask
*/
uint32 N2PSQLLogOffTask::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{
	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		return kOnFlagChange;
	}
	
	this->refresh();
	
	PreferencesConnection PrefConections;
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			return kNextEventCycle;
		}
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			return kNextEventCycle;
		}
	PMString AAS="";
	AAS.AppendNumber( PrefConections.TimeToCheckUpdateElements);
	int32  kN2PSQLLogOffExecInterval2 =   AAS.GetAsNumber() * 1000;
	// Has FS changed?
	return kN2PSQLLogOffExecInterval2;
}


/* TaskName
*/
const char* N2PSQLLogOffTask::TaskName()
{
	return kN2PSQLLogOffTaskKey;
}


/* refresh
*/
void N2PSQLLogOffTask::refresh()
{
	do
	{
		//CAlert::InformationAlert("refresh");
		bool16 canceled=kFalse;
		PreferencesConnection PrefConections;
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			
			break;
		}
		
		IDocument* document=Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document != nil)
		{
			IDataBase* db = ::GetDataBase(document);
			
			
			if (db == nil)
			{
			
				ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB db is invalid");	
				break;
			}
			
			GlobalTime timeStartUp;
			IDTime TimeUltMod;
			GlobalTime ActualTime;
			ActualTime.CurrentTime();
			
			timeStartUp = GetExecutionContextSession()->GetStartupTime();
			db->GetModificationTime(TimeUltMod);
			
			PMString ASAQ="";
			//ASAQ.AppendNumber(ActualTime.GetTime());
			//ASAQ.Append(" , ");
			//ASAQ.AppendNumber(TimeUltMod.GetTime());
			ASAQ.Append(" ,TiempoPasado: ");
			
			uint64 seonds=(ActualTime.GetTime() - TimeUltMod.GetTime())/10000000;
			ASAQ.AppendNumber(seonds,0);
			//CAlert::InformationAlert(ASAQ);
			
			if((PrefConections.TimeToCheckUpdateElements*60)>=seonds)
			{
				//CAlert::InformationAlert(ASAQ);
				break;
			}
			
		/*	RangeProgressBar aMainBar("MAIN",0,100,kTrue,kTrue,nil,kFalse);

			aMainBar.SetTaskText("Main task text");

			for(int iNoTask=0;iNoTask<100 && canceled!=kTrue;iNoTask++) 
			{ 
				RangeProgressBar *ptBar1;

				// Set steps for task - just for fun
				int iMaxVal = 2*(iNoTask+1);

				PMString pmstrTask;
				pmstrTask = "Task : ";
				pmstrTask.AppendNumber(iNoTask+1);
				pmstrTask+= ". Steps : ";
				pmstrTask.AppendNumber(iMaxVal+1);

				ptBar1 = new RangeProgressBar(pmstrTask,0,iMaxVal,kTrue,kFalse,nil,kFalse);
				for(int iProg=0;iProg<=iMaxVal && canceled!=kTrue;iProg++) 
				{ 
					ptBar1->SetPosition(iProg);
					if(ptBar1->WasCancelled()==kTrue)
					{
						aMainBar.Abort();
						ptBar1->Abort();
						delete(ptBar1);
						canceled=kTrue;
						break;
					}
				}

				delete(ptBar1);

				// Don't miss it...
				// Otherwise first bar doesn't progress during next loops...
				aMainBar.SetPosition(iNoTask+1); 
			}
			
			if(canceled==kFalse)
				N2PsqlLogInLogOutUtilities::DoLogOutUser();
		*/	
			///////
		//	InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		//	ASSERT(application);
		//	if (application == nil) {	
		//		break;
		//	}
		//	InterfacePtr<IProgressBarManager> progressBar(application, UseDefaultIID());
		//	ASSERT(progressBar);
		//	if (progressBar == nil) {
		//		break;
		//	}
		//	progressBar->SuppressProgressBarDisplay(kTrue);
			
			
		//	progressBar->SuppressProgressBarDisplay(kFalse);
			
			/*if(document->IsModified())
			{
				//CAlert::InformationAlert("A  sido modificado");
				break;
			}*/
			
			//CAlert::InformationAlert("DoLogOutUser");
			//N2PsqlLogInLogOutUtilities::DoLogOutUser();
		}
		
	
	} while(kFalse);
}

//	end, File:	N2PSQLLogOffTask.cpp
