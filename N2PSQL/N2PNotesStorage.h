//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/browsenotes/N2PNotesStorage.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:08:25 $
//  
//  $Revision: #1 $
//  
//  $Change: 323503 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __N2PNotesStorage_h__
#define __N2PNotesStorage_h__

// General includes:
#include "PMString.h"
#include "BaseType.h"
#include "UIDRef.h"

// Project includes:


/** This class stores Notes data that we have parsed from a document's stories, 
	so we can manipulate it from one handy spot.

	
	@ingroup browsenotes
	@ingroup sdk_incopy
*/
class N2PNotesStorage
{
public:

	/** Constructor. */
	N2PNotesStorage();

	/** Destructor. */
	~N2PNotesStorage();

	/** Stores user name of note author. */
	PMString fauthor;

	/** Stores date/time of Note creation. */
	PMString fcreated;

	/** Stores date/time of last note modification. */
	PMString flastmodified;

	/** Stores Note content. */
	PMString ftext;

	/** Stores index of Note anchor in story text. */
	TextIndex findex;

	/** Stores UIDRef of story. */
	UIDRef fstoryref;

	/** Stores UID of note. */
	UID fnoteUID;
};

#endif

// End, N2PNotesStorage.h.




