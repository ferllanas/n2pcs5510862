/*
//	$File: //depot/devtech/sdkanna/utilities/N2PSQLListPasesBoxHelper.cpp $
// 
//	Owner:	Ian Paterson
//
//	$Author: ipaterso $
//
//	$DateTime: 2002/10/16 08:12:04 $
//
//	$Revision: #6 $
//
//	$Change: 163058 $
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2002 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
//	Purpose:
//
*/
#include "VCPlugInHeaders.h"
// Interface includes

#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
// implem includes
#include "PersistUtils.h" // GetDatabase
#include "IPalettePanelUtils.h"
#include "CAlert.h"


#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "N2PsqlID.h"
#include "N2PSQLListPasesBoxHelper.h"

N2PSQLListPasesBoxHelper::N2PSQLListPasesBoxHelper(IPMUnknown * owner, int32 pluginId, WidgetID widget) : fOwner(owner), fOwnerPluginID(pluginId), fwidget(widget)
{

}

N2PSQLListPasesBoxHelper::~N2PSQLListPasesBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
	fwidget=nil;
}



IControlView * N2PSQLListPasesBoxHelper ::FindCurrentListBox()
{
	if(!verifyState())
	{
		//CAlert::ErrorAlert("13");
		return nil;
	}

	IControlView * listBoxControlView = nil;
	do {
	
		/*WidgetID listBoxID = Utils<IPalettePanelUtils>()->GetCurrentListBoxID(fOwnerPluginID);
		if(listBoxID== 0) 
		{
			break;
		}*/

		WidgetID listBoxID = fwidget;


		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		ASSERT_MSG(iPanelControlData != nil, "N2PSQLListPasesBoxHelper ::FindCurrentListBox() iPanelControlData nil");
		if(iPanelControlData == nil) 
		{
			break;
		}
		listBoxControlView = 	iPanelControlData->FindWidget(listBoxID);
		ASSERT_MSG(listBoxControlView != nil, "N2PSQLListPasesBoxHelper ::FindCurrentListBox() no listbox");
		if(listBoxControlView == nil) 
		{
			break;
		}
	
	} while(0);

	return listBoxControlView;
}

 

void N2PSQLListPasesBoxHelper::AddElement( const PMString & displayName, WidgetID updateWidgetId, int atIndex)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		// Create an instance of a list element
		InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
		if(listAttr == nil) {
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
				return;
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "N2PSQLListPasesBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) {
			break;
		}
		this->addListElementWidget(newElView, displayName, updateWidgetId, atIndex);
	}
	while (false);			// false loop
}


void N2PSQLListPasesBoxHelper::RemoveElementAt(int indexRemove)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "N2PSQLListPasesBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) {
			// Don't remove outside of list data bounds
			break;
		}
		listControlData->Remove(indexRemove);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);			// false loop
}



void N2PSQLListPasesBoxHelper::RemoveLastElement()
{
	if(!verifyState())
		return;

	do
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "N2PSQLListPasesBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) {		
			listControlData->Remove(lastIndex);
			removeCellWidget(listBox, lastIndex);
		}
		
	}
	while (false);
}



int N2PSQLListPasesBoxHelper::GetElementCount() 
{
	int retval=0;
	do {
	
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "N2PSQLListPasesBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void N2PSQLListPasesBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {

		if(listBox==nil) break;
		// recall that when the element is added, it is added as a child of the cell-panel
		// widget. Therefore, navigate to the cell panel and remove the child at the specified
		// index. Simultaneously, remove the corresponding element from the list controldata.
		// +
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListPasesBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) {
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
		// -

	} while(0);

}


void N2PSQLListPasesBoxHelper::addListElementWidget(InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex)
{
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) {
		return;
	}

	do {
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) {
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if ( (nameTextView == nil)  ) {
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) {
			break;
		}	
		newEltext->SetString(displayName, kTrue, kTrue);
		
		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) {
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "N2PSQLListPasesBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) {
			break;
		}

		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "N2PSQLListPasesBoxHelper::addListElementWidget() listData nil");
		if(listData == nil) { 
			break;
		}
		listData->Add(elView, atIndex);
	} while(0);

}


void N2PSQLListPasesBoxHelper::EmptyCurrentListBox()
{
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox();
		if(listBoxControlView == nil) {
			break;
		}
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) {
			break;
		}
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) {
			break;
		}
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}




void N2PSQLListPasesBoxHelper::addListPagElementWidget(InterfacePtr<IControlView> & elView, const PMString & PagName,const PMString & SeccionName,const PMString & EstadoName, WidgetID updateWidgetId, int atIndex)
{
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) 
	{
		
		return;
	}

	do {
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) 
		{
			
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(kN2PsqlNotaLabelWidgetID);
		if ( (nameTextView == nil)  ) 
		{
			
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) 
		{
			
			break;
		}	
		newEltext->SetString(PagName, kTrue, kTrue);

	/*****************************/
		IControlView* nameSeccion = newElPanelData->FindWidget(kN2PsqlSeccionLabelWidgetID);
		if ( (nameSeccion == nil)  ) 
		{
			
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newSeccion (nameSeccion,UseDefaultIID());
		if (newSeccion == nil) 
		{
			
			break;
		}	
		newSeccion->SetString(SeccionName, kTrue, kTrue);

	/**********************/
		IControlView* nameEstado = newElPanelData->FindWidget(kN2PsqlEstadoLabelWidgetID);
		if ( (nameSeccion == nil)  ) 
		{
			
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEstado (nameEstado,UseDefaultIID());
		if (newEstado == nil) 
		{
			
			break;
		}	
		newEstado->SetString(EstadoName, kTrue, kTrue);
		


		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
		{
			
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "N2PSQLListPasesBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
		{
			
			break;
		}

		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "N2PSQLListPasesBoxHelper::addListElementWidget() listData nil");
		if(listData == nil) 
		{
			
			break;
		}
		listData->Add(elView, atIndex);
	} while(0);

}

/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
*/
void N2PSQLListPasesBoxHelper::ObtenerTextoDeSeleccionActual( WidgetID widget1,PMString &Text1,WidgetID widget2,PMString &Text2,int32 indexSelected)
{
	
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) {
			break;
		}
			

		if(listBox==nil) break;
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListPasesBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tamaÒo de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView = PanelCCell->FindWidget(widget1);
		ASSERT_MSG(TextWigetCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView == nil) 
		{
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto( TextWigetCView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			break;
		}

		//Extraigo el texto del Widget
		Text1=ControlDETexto->GetString();

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView2 = PanelCCell->FindWidget(widget2);
		ASSERT_MSG(TextWigetCView2 != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView2 == nil) 
		{
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto2( TextWigetCView2, UseDefaultIID());
		if(ControlDETexto2==nil)
		{
			break;
		}

		Text2=ControlDETexto2->GetString();
	}while (false);			// false loop
}


/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
*/
void N2PSQLListPasesBoxHelper::SetTextoEnSeleccionActual( WidgetID widget1, PMString &Text1, int32 indexSelected)
{
	
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) 
		{
			
			break;
		}
			

		if(listBox==nil) 
		{
			
			break;
		}
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListPasesBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tamaÒo de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			//CAlert::InformationAlert("6");
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView = PanelCCell->FindWidget(widget1);
		ASSERT_MSG(TextWigetCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView == nil) 
		{
			
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto( TextWigetCView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			
			break;
		}

		//Extraigo el texto del Widget
		ControlDETexto->SetString(Text1);
	}while (false);			// false loop
}


/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
*/
void N2PSQLListPasesBoxHelper::GetTextoEnSeleccionActual( WidgetID widget1, PMString &Text1, int32 indexSelected)
{
	
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) 
		{
			
			break;
		}
			

		if(listBox==nil) 
		{
			
			break;
		}
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListPasesBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tamaÒo de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView = PanelCCell->FindWidget(widget1);
		ASSERT_MSG(TextWigetCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView == nil) 
		{
			
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto( TextWigetCView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			
			break;
		}

		//Extraigo el texto del Widget
		Text1=ControlDETexto->GetString();
	}while (false);			// false loop
}


void N2PSQLListPasesBoxHelper::Mostrar_IconFluido(int32 indexSelected)
{
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) 
		{
			//CAlert::ErrorAlert("ListBox");
			break;
		}
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListPasesBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			//CAlert::ErrorAlert("Panel data");
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListPasesBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tamaÒo de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* PenWigetCView = PanelCCell->FindWidget(kN2PsqlCmpListElementPenWidgetID);
		//IControlView* LockPenWigetCView = PanelCCell->FindWidget(kWLBCmpListElementLockPenWidgetID);

		ASSERT_MSG(PenWigetCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(PenWigetCView == nil) 
		{
			break;
		}

/*		ASSERT_MSG(LockPenWigetCView != nil, "N2PSQLListPasesBoxHelper::removeCellWidget() cannot find cellControlView");
		if(LockPenWigetCView == nil) 
		{
			break;
		}
*/
		PenWigetCView->Show(kTrue);
//		LockPenWigetCView->Show(kTrue);
		
	}while (false);			// false loop
}