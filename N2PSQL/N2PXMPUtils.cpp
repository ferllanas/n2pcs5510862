/*
 *  N2PXMPUtils.cpp
 *  News2Page
 *
 *  Created by ADMINISTRADOR on 19/03/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */



#include "VCPlugInHeaders.h"
#include "CAlert.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h" //GetFrontDocument

#include "N2PXMPUtils.h"
N2PXMPUtils::N2PXMPUtils(IDocument* doc)
{
	initMannager(doc);
}

N2PXMPUtils::~N2PXMPUtils()
{
	//delete dummyMetaDataAccess;
}


bool16 N2PXMPUtils::initMannager(IDocument* doc)
{
	bool16 valor = kFalse;
	do
	{
		kNS1 =  "N2PSQL";
		kNS2 = kNS1;
		dummy_type = "Articles";
		dummyMetaDataAccess = nil;
		//IDocument* doc = Utils<ILayoutUIUtils>()-> GetFrontDocument();
		if (doc == nil) 
		{
			break;
		}
		
		InterfacePtr<IMetaDataAccess> iMetaDataAccess(doc, UseDefaultIID());
		if (iMetaDataAccess == nil)
			break;
		
		dummyMetaDataAccess = iMetaDataAccess;
		
		
		PMString registeredPrefix;
		dummyMetaDataAccess->RegisterNamespace (kNS1, "N2P", registeredPrefix);
		dummyMetaDataAccess->RegisterNamespace (kNS2, "N2P", registeredPrefix);
		
		//fnumDArticles=CountArticles();
		valor = kTrue;
	}while(false);
	
	return valor;
	
}



PMString N2PXMPUtils::Get(PMString variableXMP)
{
	PMString retval="";
	if(dummyMetaDataAccess != nil)
	{
		dummyMetaDataAccess->Get(kNS1, variableXMP, retval);//"DocumentMySQLID"
		
	}
	return retval;
}

bool16	 N2PXMPUtils::Set(PMString variableXMP,PMString value)
{
	bool16 retval=kFalse;
	if(dummyMetaDataAccess != nil)
	{
		dummyMetaDataAccess->Set(kNS1, variableXMP, value);//"DocumentMySQLID"
	}
	return retval;
}