/*
//	File:	N2PsqlWidgetObserver.cpp
//
//	Date:	28-Jan-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#include "VCPlugInHeaders.h"


////////////////////
#include "AttributeBossList.h"
#include "CAlert.h"
#include "CActionComponent.h"
#include "CmdUtils.h"
#include "CObserver.h"
#include "ErrorUtils.h"
#include "IFrameUtils.h"
#include "IImageUtils.h"
#include "ILayoutUIUtils.h"
#include "IPathUtils.h"
#include "PersistUtils.h"
#include "PMString.h"
#include "PMReal.h"
#include "PMRect.h"
#include "PreferenceUtils.h"
#include "UIDList.h"
#include "UIDRef.h"
#include "Utils.h"
#include "StreamUtil.h"
#include "SysFileList.h"
#include "Trace.h" 
#include "TransformUtils.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "WideString.h"
#include "IInCopyDocUtils.h"
#include "IRangeData.h"
#include "IIntData.h"

/////////////////////
// includes IDs://///
/////////////////////
//#include "DocumentID.h"
#include "LayoutID.h"
#include "OpenPlaceID.h"
#include "SpreadID.h"
#include "SplineID.h"
#include "ITextAttrUID.h"
#include "TextID.h"

///////////////////
// Interface includes
#include "IActiveContext.h"
#include "IApplication.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IPDFExportPrefs.h"
#include "IPDFSecurityPrefs.h"
#include "IDocument.h"
#include "FileUtils.h"
#include "IComposeScanner.h"
#include "IPageItemTypeUtils.h"
#include "IGraphicFrameData.h"
#include "ITriStateControlData.h"
#include "IFrameEdgePrefsCmdData.h"

#include "ICloseWinCmdData.h"
#include "ISysFileData.h"
#include "ICommand.h"
#include "IWorkspace.h"
#include "ISpreadList.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "IPageList.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"

#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IMultiColumnTextFrame.h"
#include "ITextModel.h"
#include "ITextFocus.h"
#include "ITextModelCmds.h"
#include "ITextTarget.h"

//#include "ISelection.h"
#include "IStyleNameTable.h"
//#include "ISpecifier.h"
#include "IListBoxController.h"
#include "ILayerList.h"
#include "ICreateMCFrameData.h"
#include "INewPageItemCmdData.h"
#include "ILayoutCmdData.h"
#include "INewLayerCmdData.h"
#include "IPageItemTypeUtils.h"
#include "IDocumentLayer.h"

#include "IBoolData.h"
#include "IUIFlagData.h"
#include "IOutputPages.h"
#include "IPDFExptStyleListMgr.h"
#include "IPrintContentPrefs.h"
#include "BookID.h" // for IID_IBOOKEXPORT
//#include "DocumentID.h" // for IID_IDOCUMENT, IID_ISYSFILEDATA and IID_IUIFLAGDATA
#include "PDFID.h" // for kPDFExportCmdBoss and IID_IPDFEXPORTPREFS
#include "PrintID.h" // for IID_IPRINTCONTENTPREFS
#include "CmdUtils.h"
#include "K2SmartPtr.h"
#include "ProgressBar.h"
#include "SDKUtilities.h"		
#include "UIDList.h"

#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ErrorUtils.h"
#include "IWindowUtils.h"
#include "N2PSQLListBoxHelper.h"
#include "Utils.h"
#include "IPalettePanelUtils.h"

#include "ISelectionUtils.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextSelectionSuite.h"
#include "IConcreteSelection.h"
#include "IFrameList.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"

#include "DocWchUtils.h"
#include "UIDList.h"
#include "SDKLayoutHelper.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"
#include "CAlert.h"
#include "CObserver.h"
#include "ICompositeFont.h"
// Implem includes


#ifdef MACINTOSH
	#include <sys/stat.h>
	#include "N2PSQLUtilities.h"
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "InterlasaUtilities.h"
	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"
	#include "N2PCheckInOutID.h"
	#include "ICheckInOutSuite.h"
	#include "IN2PAdIssueUtils.h"
	#include "IN2PCTUtilities.h"

#endif

#ifdef WINDOWS
	#include <sys/stat.h>
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"
	#include "..\N2PCheckInOut\ICheckInOutSuite.h"
	#include "..\N2PAdornmentIssue\IN2PAdIssueUtils.h"
	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
	#include "..\InterlasaUltraProKeys\InterUltraProKyID.h"
	#include "..\InterlasaUltraProKeys\IInterUltraProKy.h"
	#include "..\InterlasaUltraProKeys\IInterUltraObjectProKy.h"
#endif



/**
	Implements IObserver. The N2PsqlWidgetObserver class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class provides the capability to observe the widgets on the panel and respond appropriately
	to changes in their state.
	
	@author Juan Fernando Llanas Rdz

*/
class N2PsqlWidgetObserver : public CObserver
{
public:
	/**
		Constructor for N2PsqlWidgetObserver class
	*/		
	N2PsqlWidgetObserver(IPMUnknown *boss);
	/**
		Destructor for N2PsqlWidgetObserver class - 
		performs cleanup 
	*/
	~N2PsqlWidgetObserver();
	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	*/		
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when the widget is hidden.
	*/		
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	void handleWidgetHit(InterfacePtr<IControlView>& controlView, const ClassID& theChange, WidgetID widgetID) ;
	void attachWidget(InterfacePtr<IPanelControlData>&  panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);


	
	const PMIID fObserverIID;
	

	void crearPDF();
	void CreateAndProcessCloseWinCmd(IWindow *win);

	void LlenarTextoFechaActual();
	

	void LimpiarWidgets();

	void LipiaWidget(WidgetID widget);

	PMString AbrirDialogoBusqueda(bool16& FindOnlyPases);
	
	int32 AplicaStyle(PMString NomStyle);
	
	int32 PlaceTextAndAplicaStyle(PMString NomStyle,PMString texto);

	//int32 PlaceTextAndAplicaStyleOnTextModel(InterfacePtr<ITextModel> iTextModel,PMString NomStyle, PMString Texto);
	
	bool16 ApplicaColumnBreakSiNecesita();
	
	bool16 InsertBreakColumnCmd(const UIDRef storyUIDRef, const TextIndex finishIndex );


	bool16 CambiarIconoPen(PMString NombreAviso);

	/*bool16 UpdateDB(PMString NamePreferenceServer,PMString Consulta);*/

	/*PMString ReadDBByItem(PMString Consulta,PMString Item);*/

	void ClearWidgets();

	//Crea Capa Update
	//Ya no se aplica
	//void creatCapaN2PSQL(PMString NombredeCapa);

	//BuscaTextFrameUpdate
	UIDRef BucaTextFrameUpdate();

	//CreaTextFrameUpdate
	//void CrearTextFrameUpdate(PMString CadenaAImprimir);

	//ActivaCapa Update
	void ActivarCapa(PMString NomCapa);

	void MostrarCapa(PMString NombreCapa,bool16 Mostrar);

	void BloquearCapa(PMString NombreCapa,bool16 Bloqueo);
	
	void LlenarComboPreferenciasOnPanel();
	
	bool16 HacerRevisionPaDocNuevo();

	UIDRef fPageUIDRef;		// Remembers which page we were on
		unsigned char fStep;	// Indicates how many frames we've created this round.


};

CREATE_PMINTERFACE(N2PsqlWidgetObserver, kN2PsqlWidgetObserverImpl)

/*
Constructor
*/
N2PsqlWidgetObserver::N2PsqlWidgetObserver(IPMUnknown* boss)
: CObserver(boss), fObserverIID(IID_IN2PsqlWIDGETOBSERVER)
{
	
}

/*
Constructor
*/
N2PsqlWidgetObserver::~N2PsqlWidgetObserver()
{
	
}

/*
Constructor
*/
void N2PsqlWidgetObserver::AutoAttach()
{
	do {
		
		//CAlert::InformationAlert("AAAAA");
			
		//Coloca el nombre del usuario sobre la paleta
			InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				break;
			}		
			
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			
			if(UpdateStorys==nil)
			{
				ASSERT_FAIL("No pudo Obtener UpdateStorys*");
				break;
			}
			
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		if(PrefConections.N2PUtilizarGuias==1)
		{
			PMString Id_Seccion = "";
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document != nil)
			{
				Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
			}
			
			
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kTrue);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kFalse);
			
			if(Id_Seccion.NumUTF16TextChars()>0 )
				N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
			
		}
		else
		{
			//CAlert::InformationAlert("BBB");
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kFalse );
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kTrue );
			
		}
			
			PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
			
			if(ID_Usuario.NumUTF16TextChars()<=0)
			{
				UpdateStorys->PlaceUserNameLogedOnPalette(" ");
			}
			else
			{
				
				PMString Userstrng="";
				Userstrng.Append(kN2PInOutAUserLogedStringKey);
				Userstrng.Translate();
			
				
			
				Userstrng.Append(" ");
				Userstrng.Append(ID_Usuario);
				Userstrng.SetTranslatable(kFalse);
				UpdateStorys->PlaceUserNameLogedOnPalette(Userstrng);
			}
			
		////////////////
		
		
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		ASSERT(app);
			if (app == nil) {	
				break;
			}
		PMString nameApp=app->GetApplicationName();
		nameApp.SetTranslatable(kFalse);
		
		if(nameApp.Contains("InCopy"))
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextTituloWidgetID,kFalse);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextBalazoWidgetID,kFalse);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextNotaWidgetID,kFalse);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextPieWidgetID,kFalse);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlInCopyPctureWidgetID,kTrue);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PSQLCkBoxShowTxtOversetEditWidgetID,kFalse); //Oculta CheckBox de TextOverset
			
			
		}
		else
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PSQLCkBoxShowTxtOversetEditWidgetID,kTrue); //Muestra CheckBox de TextOverset
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlInCopyPctureWidgetID,kFalse);
		}
		
		
		// this->LlenarComboPreferenciasOnPanel();
		 
		
		if (DocWchUtils::QueryDocResponderMode())
		{
			DocWchUtils::StopDocResponderMode();
		}
		else
		{
			DocWchUtils::StartDocResponderMode();
		}
		
		
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) 
		{
			break;
		}
		
		
		// TODO attach to widgets to observe here 
		
		 this->attachWidget(panelControlData,kN2PsqlBotonPDFWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlBotonBorrarTextoWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlBotonBuscarWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlBotonUpdateDBToTextFrameWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlBotonUpdateTextToDBFrameWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlBotonRefreshStatusNotaWidgetID,IID_IBOOLEANCONTROLDATA);
		 
		 this->attachWidget(panelControlData,kN2PsqlBotonCancelNewNoteWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlBotonSubirNewNoteWidgetID,IID_IBOOLEANCONTROLDATA);
		 
		 
		 this->attachWidget(panelControlData,kN2PsqlBotonUpdateMakeDBNotasTextFrameWidgetID,IID_IBOOLEANCONTROLDATA);
		
		 this->attachWidget(panelControlData,kN2PSQLCkBoxShowEdgesEditWidgetID,ITriStateControlData::kDefaultIID);
		 this->attachWidget(panelControlData,kN2PSQLCkBoxShowTxtOversetEditWidgetID,ITriStateControlData::kDefaultIID);
		
		
		 this->attachWidget(panelControlData,kN2PsqlAlertNotaConFotoWidgetID,IID_ITRISTATECONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlStTextTituloWidgetID,IID_ITRISTATECONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlStTextBalazoWidgetID,IID_ITRISTATECONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlStTextNotaWidgetID,IID_ITRISTATECONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlStTextPieWidgetID,IID_ITRISTATECONTROLDATA);
		 
		 this->attachWidget(panelControlData,kN2PsqlStTextPruebaWidgetID,IID_ITRISTATECONTROLDATA);
		 this->attachWidget(panelControlData,kN2PsqlStTextPrueba2WidgetID,IID_ITRISTATECONTROLDATA);
		 
		
		 //this->ClearWidgets();
		 this->LlenarTextoFechaActual();
		 
		//CAlert::InformationAlert("BBBBB");
	} while(kFalse);
}

/*
Constructor
*/
void N2PsqlWidgetObserver::AutoDetach()
{
	do {
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// TODO detach from widgets here 
		
		 this->detachWidget(panelControlData,kN2PsqlBotonPDFWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonBorrarTextoWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonBuscarWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonUpdateDBToTextFrameWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonUpdateTextToDBFrameWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonRefreshStatusNotaWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonUpdateMakeDBNotasTextFrameWidgetID,IID_IBOOLEANCONTROLDATA);
			
		 this->detachWidget(panelControlData,kN2PsqlBotonCancelNewNoteWidgetID,IID_IBOOLEANCONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlBotonSubirNewNoteWidgetID,IID_IBOOLEANCONTROLDATA);
		  
		this->detachWidget(panelControlData,kN2PsqlAlertNotaConFotoWidgetID,IID_ITRISTATECONTROLDATA);
		 this->detachWidget(panelControlData,kN2PSQLCkBoxShowEdgesEditWidgetID,ITriStateControlData::kDefaultIID);
		 this->detachWidget(panelControlData,kN2PSQLCkBoxShowTxtOversetEditWidgetID,ITriStateControlData::kDefaultIID);

		 this->detachWidget(panelControlData,kN2PsqlStTextTituloWidgetID,IID_ITRISTATECONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlStTextBalazoWidgetID,IID_ITRISTATECONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlStTextNotaWidgetID,IID_ITRISTATECONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlStTextPieWidgetID,IID_ITRISTATECONTROLDATA);
			
		 this->detachWidget(panelControlData,kN2PsqlStTextPruebaWidgetID,IID_ITRISTATECONTROLDATA);
		 this->detachWidget(panelControlData,kN2PsqlStTextPrueba2WidgetID,IID_ITRISTATECONTROLDATA);
		 
	} while(kFalse);
}



/* attachWidget
*/
void N2PsqlWidgetObserver::attachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData != nil);

	//TRACE("TblAttWidgetObserver::AttachWidget(widgetID=0x%x, interfaceID=0x%x\n", widgetID, interfaceID);
	do
	{
		if(!panelControlData) break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView != nil);
		if (controlView == nil)
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject != nil);
		if (subject == nil)
		{
			break;
		}
		subject->AttachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

/* detachWidget
*/
void N2PsqlWidgetObserver::detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData != nil);
	do
	{
		if(panelControlData == nil) break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView != nil);
		if (controlView == nil)
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject != nil);
		if (subject == nil)
		{
			break;
		}
		subject->DetachObserver(this, interfaceID, fObserverIID);
	}
	while (false);
}




/*
Update
*/
void N2PsqlWidgetObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update ini");
	
	do
	 {
			ASSERT(theSubject);
			if(!theSubject) 
			{
				break;
			}
			InterfacePtr<IControlView>  icontrolView(theSubject, UseDefaultIID());
			ASSERT(icontrolView);
			if(!icontrolView) 
			{
				break;
			}

			WidgetID thisID = icontrolView->GetWidgetID();
			
		if(theChange == kTrueStateMessage)
		{
			PMString DSNName="";
			bool16 SetLockStory=kTrue;
			PMString ID_Usuario="";	
			PMString Applicacion="";	
				
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			
			InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				break;
			}		
		
			
			ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
			Applicacion.Append(wrkSpcPrefs->GetNameApp());
	
			if(Applicacion.NumUTF16TextChars()<1)
			{
				break;
			}
		
		
		
		
		
			///////Actualiza nbotas sobre el documento y en base de datos asi como realiza revision de Documento///////
			switch(thisID.Get())
			{
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
					case kN2PsqlBotonUpdateMakeDBNotasTextFrameWidgetID:
					{
						//Realiza Updates de todo
						UpdateStorys->ActualizaTodasLNotasYDisenoPagina("N2PActualizacion");
						break;
					}
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}
				
			}
			
		
			if(Applicacion.NumUTF16TextChars()<1)
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				break;
			}
			
			DSNName="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
			if(DSNName.NumUTF16TextChars()<1)
			{
				CAlert::InformationAlert(kN2PInOutNoServerPrefSelectedStringKey);
				break;
			}
			
			
			
			
			//Para saber si se va a bloquear el story
			
		
			PreferencesConnection PrefConections;
			if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
				
			//solo si la nota proviene del servidor de Preferencia
			if(PrefConections.NameConnection==DSNName)
			{
				//CAlert::InformationAlert("Local");
				//CAlert::InformationAlert( N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlNoteIsLocketTextEditWidgetID));
				if( N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlNoteIsLocketTextEditWidgetID).GetAsNumber()==1)
					SetLockStory=kTrue;
				else
					SetLockStory=kFalse;
			}
			else
			{	//se bloquea si es un sewrvidor remoto
				//CAlert::InformationAlert("Remoto");
				SetLockStory=kTrue;
			}
					
		
		
			//Hace revision si el documento es nuevo para el sistema N2P SQL
			if(!this->HacerRevisionPaDocNuevo())
			{
				//si no se pudo hacer revision
				break;
			}
		
		
			PMString textoUpdate="";
			PMString texto;
			PMString UIDModelString="";
			PMString ID_Elem_Padre="";
			PMString ItemToSearchString="";
			PMString Consulta="";
			int32 UIDModel=0;
			int32 IDNota=0;
			//PMString NameDoc=""; CS3 parece que no se necesita
			//int32 IndexString=0; CS3 parece que no se necesita
		
		///////////////////
		//obtiener el nombre del documento

		//obtengo la interfaz  ILayoutControlData de la capa que se encuentra en frente o activa
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("document pointer nil");
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		
		
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		//Para obtener el numero de la pagina en que nos encontramos
		int32 numeroPaginaActual=0;
		UID pageUID = layout->GetPage(); 
		
		InterfacePtr<IPageList> PageList(document,IID_IPAGELIST); 
		if(PageList!=nil)
		{
			
			numeroPaginaActual=PageList->GetPageIndex(pageUID)+1;
		}
		
		
		//
		UID docLayerUID=layout->GetActiveDocLayerUID();

		
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		
		int32 NumLayerAActivar=layerList->GetLayerIndex(docLayerUID);
		
		
		IDocumentLayer* documentLayer = layerList->QueryLayer(NumLayerAActivar);

		
		//obtiene el nombre de la capa activa 
		PMString NameLayerActiva=documentLayer->GetName();

		
		
		/*IWindow *window = Utils<IWindowUtils>()->GetFrontDocWindow();
		NameDoc=window->GetTitle();
		IndexString=NameDoc.IndexOfString(" ");
		if(IndexString!=-1)
			NameDoc.Remove(IndexString, NameDoc.NumUTF16TextChars()-IndexString );
		*/
		////////////
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
		
		
		
		
		
		
		switch(thisID.Get())
		{
			
				
			case kN2PsqlBotonPDFWidgetID:
			{
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
					this->crearPDF();
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}
				
				break;
			}
			
			case kN2PsqlBotonBorrarTextoWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlBotonBuscarWidgetID");
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
					this->LimpiarWidgets();	
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}
					
				break;
			}
			
			case kN2PsqlBotonBuscarWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlBotonBuscarWidgetID");
				N2PSQLUtilities::BusquedaPorDialogo();
				break;
			}
			
			
			///Validar
			
			
			case kN2PsqlStTextTituloWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlStTextTituloWidgetID");
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
				
					PMString StringRefTextModelOfTagegedText="";
					StringRefTextModelOfTagegedText = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlUIDRefTexModelDeTitulo_WidgetID);  
					UID uidItemplace=StringRefTextModelOfTagegedText.GetAsNumber();
					UIDRef itemToPlace(db, uidItemplace);
					
					
					//Item a buscar
					ItemToSearchString="Titulo";
					
					//Obtiene texto del widget que se va a poner sobre el Textframe
					texto= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextTituloWidgetID);
					
					//Obtiene el Id del Elemento Padre de la nota			
					ID_Elem_Padre= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
					
					
					if(!UpdateStorys->ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(ID_Elem_Padre,kTrue))
					{
						CAlert::InformationAlert(kN2PSQLDebeDepositarNotaPadreStringKey);
						break ;
					}
					
					//Obtiene el UIOD del TextModel del frame que se esta seleccionando
					UIDModel = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
					if(UIDModel==0)
						break;
					
					UID uidMod=UIDModel;
					UIDRef graphicFrame(db, uidMod);
					
					//
					UIDModelString.AppendNumber(UIDModel);
								
					//Obtiene estatus de nota
					PMString EstadoNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextStatusWidgetID);
					PMString FYHUCheckinNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlLastCheckInNoteTextEditWidgetID);
					PMString ID_Elemento =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_Titulo_WidgetID);
					
					///////////////////////////////////
					//Para saber si se puede fluir la nota
						PMString IDSeccionDeElemPadre="";
					PMString Consulta="";
					PMString StringConection="";
					K2Vector<PMString> QueryVector;
					PreferencesConnection PrefConectionsOfServerRemote;
		
					if( !UpdateStorys->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
					{
						break;
					}
					
					
					//
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					ASSERT(SQLInterface);
					if (SQLInterface == nil) {	
						break;
					}
					StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
					
					
					
					Consulta=" SELECT Id_Seccion, Dirigido_a FROM Elemento WHERE Id_Elemento=" +ID_Elem_Padre ;
					
					PMString dirigido_a="";
					//solo si la nota proviene del servidor de Preferencia
					//if(PrefConectionsOfServerRemote.NameConnection == DSNName)
					//{
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						
						if(QueryVector.Length()>0)
						{
							IDSeccionDeElemPadre = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
							dirigido_a = SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[0]);
						}
						else
							break;
						
						QueryVector.clear();
						Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "' , "+IDSeccionDeElemPadre+" , 24, @X)" ;
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						else
						{
							PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
							if(RESULT=="0")
							{
								CAlert::InformationAlert(kN2PSQLUserCantLigarNotaAPagStringKey);
								break;
							}
							
						}
					//}
										
					
					if(dirigido_a.NumUTF16TextChars()>0)
						EstadoNota=dirigido_a+"/"+EstadoNota;
					
					bool16 FueReemplazado = N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_Elem_Padre,ID_Elemento,UIDModelString,ItemToSearchString,DSNName,EstadoNota,SetLockStory,FYHUCheckinNota, kFalse/*no es elem foto*/);
					
					//si no se desea reemplazar 
					if(FueReemplazado==kFalse)
						break;		
								
								
								
					//Coloca Texto y aplica estilo a TextFlame y regresa el UID del TextModel
					this->PlaceTextAndAplicaStyle(ItemToSearchString,texto);
					
					
					/*bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
					if(!sa)
						break;
					
					this->AplicaStyle(ItemToSearchString);
					*/
					
					
					/*QueryVector.clear();
					Consulta= "EXEC Return_Contenido_De_Id_Elemento '" + ID_Elemento + "'";
					SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
			
			
					for(int32 j=0;j<QueryVector.Length();j++)
					{
				
						PMString Contenido = SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryVector[j]);
				
						K2Vector<N2PNotesStorage*> fBNotesStore;
						//Importa TextoTageado si es que el texto viene en formato tageado
						UIDRef RefTagetTextImported = N2PSQLUtilities::ImportTaggedTextOfPMString(Contenido, fBNotesStore);
				
						//if(RefTagetTextImported != UIDRef::gNull)
						//{	//convierte el texto tageado a texto plano
						//	Contenido = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
						//}
						bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(RefTagetTextImported, graphicFrame, document);
						if(!sa)
						{
							CAlert::InformationAlert("Salio");
							break;
						}
						CAlert::InformationAlert("OK conjmadres");
							
					}
					*/
					//Obtiene le ID de la nota que se encuentra actualmente en vista
					PMString string="";
					PMString FecvhaSalida="0";
					UpdateStorys->EstableceParametrosToCheckInElementoNota(ID_Elem_Padre,
														ID_Elemento,
														UIDModelString.GetAsNumber(), 
														FecvhaSalida, 
														"0",
														"0",
														ID_Usuario,
														"0",
														"0",
														kFalse,
														document,
														string,kFalse,"");
					N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(ID_Elem_Padre,FecvhaSalida, document);
					
				//	bool16 FueReemplazado = N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_Elem_Padre,ID_Elemento,UIDModelString,ItemToSearchString,DSNName,EstadoNota,SetLockStory,FYHUCheckinNota, kFalse/*no es elem foto*/);
					
					if(FueReemplazado)
					{
						UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
					}
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}
				
								
				break;
			}
			
			
			case kN2PsqlStTextBalazoWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlStTextBalazoWidgetID");
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
					//Item a buscar
					ItemToSearchString="Balazo";
					//Obtiene texto del widget que se va a poner sobre el Textframe
					texto= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextBalazoWidgetID);
				
					ID_Elem_Padre= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
					
					if(!UpdateStorys->ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(ID_Elem_Padre,kTrue))
					{
						CAlert::InformationAlert(kN2PSQLDebeDepositarNotaPadreStringKey);
						break ;
					}
					
					//Obtiene le ID de la nota que se encuentra actualmente en vista
					PMString StringRefTextModelOfTagegedText="";
					StringRefTextModelOfTagegedText = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlUIDRefTexModelDeBalazo_WidgetID);  
					UID uidItemplace=StringRefTextModelOfTagegedText.GetAsNumber();
					UIDRef itemToPlace(db, uidItemplace);
					
					UIDModel = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
					if(UIDModel==0)
						break;
					UID uidMod=UIDModel;
					UIDRef graphicFrame(db, uidMod);
					
					
					
				
					UIDModelString.AppendNumber(UIDModel);
									
					//Obtiene estatus de nota
					PMString EstadoNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextStatusWidgetID);
					PMString FYHUCheckinNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlLastCheckInNoteTextEditWidgetID); 
					PMString ID_Elemento =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_Balazo_WidgetID);
					
					///////////////////////////////////
					//Para saber si se puede fluir la nota
						PMString IDSeccionDeElemPadre="";
					PMString Consulta="";
					PMString StringConection="";
					K2Vector<PMString> QueryVector;
					PreferencesConnection PrefConectionsOfServerRemote;
		
					if( !UpdateStorys->GetDefaultPreferencesConnection( PrefConectionsOfServerRemote,kFalse ) )
					{
						break;
					}
					
					
					//
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					ASSERT(SQLInterface);
					if (SQLInterface == nil) {	
						break;
					}
					StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
					
					
					
					Consulta=" SELECT Id_Seccion,Dirigido_a FROM Elemento WHERE Id_Elemento=" +ID_Elem_Padre ;
					
					PMString dirigido_a="";
					//solo si la nota proviene del servidor de Preferencia
					//if(PrefConectionsOfServerRemote.NameConnection == DSNName)
					//{
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						
						if(QueryVector.Length()>0)
						{
							IDSeccionDeElemPadre = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
							dirigido_a = SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[0]);
						}
						else
							break;
						
						QueryVector.clear();
						Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "' , "+IDSeccionDeElemPadre+" , 24, @X)" ;
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						else
						{
							PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
							if(RESULT=="0")
							{
								CAlert::InformationAlert(kN2PSQLUserCantLigarNotaAPagStringKey);
								break;
							}
							
						}
					//}
					///////////////////////////////////
					if(dirigido_a.NumUTF16TextChars()>0)
						EstadoNota=dirigido_a+"/"+EstadoNota;
					bool16 FueReemplazado=N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_Elem_Padre,ID_Elemento,UIDModelString,ItemToSearchString,DSNName,EstadoNota,SetLockStory,FYHUCheckinNota, kFalse/*no es elemento foto*/);
					
					//si no se desea reemplazar 
					if(FueReemplazado==kFalse)
						break;

					//Coloca Texto y aplica estilo a TextFlame y regresa el UID del TextModel
					this->PlaceTextAndAplicaStyle(ItemToSearchString,texto);
					
					/*bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
					if(!sa)
					{
						CAlert::InformationAlert("Salio no se por que");
						break;
					}
					
					this->AplicaStyle(ItemToSearchString);
					*/
					/*QueryVector.clear();
					Consulta= "EXEC Return_Contenido_De_Id_Elemento '" + ID_Elemento + "'";
					SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
			
			
					for(int32 j=0;j<QueryVector.Length();j++)
					{
				
						PMString Contenido = SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryVector[j]);
				
						K2Vector<N2PNotesStorage*> fBNotesStore;
						//Importa TextoTageado si es que el texto viene en formato tageado
						UIDRef RefTagetTextImported = N2PSQLUtilities::ImportTaggedTextOfPMString(Contenido, fBNotesStore);
				
						//if(RefTagetTextImported != UIDRef::gNull)
						//{	//convierte el texto tageado a texto plano
						//	Contenido = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
						//}
						bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(RefTagetTextImported, graphicFrame, document);
						if(!sa)
						{
							CAlert::InformationAlert("Salio");
							break;
						}
						CAlert::InformationAlert("OK conjmadres");
							
					}
						
					
					this->ApplicaColumnBreakSiNecesita();*/
					
					PMString string="";			
					PMString FecvhaSalida="0";
					UpdateStorys->EstableceParametrosToCheckInElementoNota(ID_Elem_Padre,
														ID_Elemento,
														UIDModelString.GetAsNumber(), 
														FecvhaSalida, 
														"0",
														"0",
														ID_Usuario,
														"0",
														"0",
														kFalse,
														document,
														string,kFalse,"");
					N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(ID_Elem_Padre,FecvhaSalida, document);
					
					//Coloca el Lock state del stori como bloquedo
					if(FueReemplazado)
					{
						//Coloca el Lock state del stori como bloquedo
						UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
					}
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}
								
				break;
			}
			
			case kN2PsqlStTextNotaWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlStTextNotaWidgetID");
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
				
					PMString Publicacion =  N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlComboPubliWidgetID);
						
					PMString Seccion	 =  N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlComboSeccionWidgetID);
										
					PMString FechaPubli	 =  N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextFechaWidgetID);
																
					//Item a buscar
					ItemToSearchString="ContenidoNota";
					//Obtiene texto del widget que se va a poner sobre el Textframe
					texto= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextNotaWidgetID);
				
					
					
					ID_Elem_Padre= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
					if(!UpdateStorys->ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(ID_Elem_Padre,kTrue))
					{
						CAlert::InformationAlert(kN2PSQLDebeDepositarNotaPadreStringKey);
						break ;
					}
					
					//Obtiene le ID de la nota que se encuentra actualmente en vista
					PMString StringRefTextModelOfTagegedText="";
					StringRefTextModelOfTagegedText = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlUIDRefTexModelDeNota_WidgetID);  
					UID uidItemplace=StringRefTextModelOfTagegedText.GetAsNumber();
					UIDRef itemToPlace(db, uidItemplace);
					
					UIDModel = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
					if(UIDModel==0)
						break;
					UID uidMod=UIDModel;
					UIDRef graphicFrame(db, uidMod);
				
					UIDModelString.AppendNumber(UIDModel);
											
					//Obtiene estatus de nota
					PMString EstadoNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextStatusWidgetID);
					PMString FYHUCheckinNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlLastCheckInNoteTextEditWidgetID); 
					PMString ID_Elemento =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_ContentNota_WidgetID);
					
					PMString ID_SeccionNota =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlComboSeccionWidgetID);
					PMString ID_PubliNota =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlComboPubliWidgetID);
					
					PMString ID_PubliPagina = N2PSQLUtilities::GetXMPVar("N2PPublicacion", document); 
					PMString ID_SeccionPagina =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
					
					
					///////////////////////////////////
					//Para saber si se puede fluir la nota
					
						PMString IDSeccionDeElemPadre="";
					PMString Consulta="";
					PMString StringConection="";
					K2Vector<PMString> QueryVector;
					PreferencesConnection PrefConectionsOfServerRemote;
		
					if( !UpdateStorys->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
					{
						break;
					}
					
					
					//
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					ASSERT(SQLInterface);
					if (SQLInterface == nil) {	
						break;
					}
					StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
					
					
					
					Consulta=" SELECT Id_Seccion, Dirigido_a FROM Elemento WHERE Id_Elemento=" +ID_Elem_Padre ;
					
					PMString dirigido_a="";
					//solo si la nota proviene del servidor de Preferencia
					//if(PrefConectionsOfServerRemote.NameConnection == DSNName)
					//{
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						
						if(QueryVector.Length()>0)
						{
							IDSeccionDeElemPadre = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
							dirigido_a = SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[0]);
						}
						else
							break;
						
						QueryVector.clear();
						Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "' , "+IDSeccionDeElemPadre+" , 24, @X)" ;
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						else
						{
							PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
							if(RESULT=="0")
							{
								CAlert::InformationAlert(kN2PSQLUserCantLigarNotaAPagStringKey);
								break;
							}
							
						}
					//}
					
					if(dirigido_a.NumUTF16TextChars()>0)
						EstadoNota=dirigido_a+"/"+EstadoNota;
					
					///////////////////////////////////
					
					//CAlert::InformationAlert(ID_SeccionNota + " , " + ID_SeccionPagina );
					if(ID_SeccionNota!=ID_SeccionPagina)
					{
						const int ActionAgreedValue=1;
						int32 result=-1;
						result=CAlert::ModalAlert(kN2PSQLDeseaFluirNotaCIdSecDifteAPageStringKey,
												kYesString, 
												kCancelString,
												kNullString,
												ActionAgreedValue,CAlert::eQuestionIcon);
						if(result==1)
						{
							//si se desea fluir la nota apesar de ser diferente seccion
							
						}
						else
						{
							break;
						}
					}
					
					
					
					bool16 FueReemplazado=N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_Elem_Padre,ID_Elemento,UIDModelString,ItemToSearchString,DSNName,EstadoNota,SetLockStory,FYHUCheckinNota, kFalse /*no es elemento foto*/);
						
				//si no se desea reemplazar 
					if(FueReemplazado==kFalse)
						break;
					
					
					N2PSQLUtilities::ColocaDatosDePaginaOnXMP(numeroPaginaActual,Seccion,Publicacion,FechaPubli);
											
					//Coloca Texto y aplica estilo a TextFlame y regresa el UID del TextModel
					this->PlaceTextAndAplicaStyle(ItemToSearchString,texto);
					
					/*bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
					if(!sa)
						break;
					
					this->AplicaStyle(ItemToSearchString);
					*/
					PMString string="";
					PMString FecvhaSalida="0";
					UpdateStorys->EstableceParametrosToCheckInElementoNota(ID_Elem_Padre,
														ID_Elemento,
														UIDModelString.GetAsNumber(), 
														FecvhaSalida, 
														"0",
														"0",
														ID_Usuario,
														"0",
														"0",
														kFalse, 
														document,
														string,kFalse,"");
					N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(ID_Elem_Padre,FecvhaSalida, document);
					
											
					PMString N2PSQL_ID_Pag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
					
					
					/*v1035 para evitar preguntar que venga de otro servidor
					no nos interesa por lo pronto en esta version.
					if(PrefConections.NameConnection!=DSNName)
					{
						//PREGUNTAMOS SI SE DEA IMPORTAR LA NOTA
						const int ActionAgreedValue=1;
						int32 result=-1;
						result=CAlert::ModalAlert(kN2PSQLDeseaImportNotaStringKey,
												kYesString, 
												kNoString,
												kNullString,
												ActionAgreedValue,CAlert::eQuestionIcon);
						if(result==1)
						{
							//////////////////////////////
							//   SI desea importar la nota
							//////////////////////////////
							//retval = kNoInvalidWidgets;
							UpdateStorys->ImportarNota();
							
							this->CambiarIconoPen("Cualquiera");
				
									
							if(UpdateStorys->SeRequiereActualizacionDeNotasDeDocument(document))
							{
								//Actualizacion de contenido de elementos de una nota desde la base de datos
								UpdateStorys->ActualizaNotasDesdeBD(document);
							}
					
							//Coloca el Lock state del stori como bloquedo
							if(FueReemplazado)
							{
								//Coloca el Lock state del stori como bloquedo
								UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
							}
											
							//Solo si nos encontramos en InDesign realiza el Save Revision
							if(Applicacion.Contains("InDesign"))
							{
								//Para Gurdar Revision
								ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
								(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
								if(checkIn==nil)
									break;
						
								checkIn->AutomaticSaveRevi(document);
								//CAlert::InformationAlert("SALio aqui 3");
							}
											
							//Realiza Updates
							UpdateStorys->MakeUpdate(document,kTrue);
							
							
						}
						else
						{
							//////////////////////////////
							//   NO desea importar la nota
							//////////////////////////////
							//retval = kComboBoxStatusWidgetID;
							
							PMString ParametersProc="CALL NOTA_COLOCA_Y_LIGA_A_PAGINA(";	
					
							ParametersProc.Append(ID_Elem_Padre);	//Id de la nota que fue colocada
							ParametersProc.Append(",");
					
							ParametersProc.Append(N2PSQL_ID_Pag);	//Folio o Id de la pagina en que fue colocada la nota
							ParametersProc.Append(",");
					
							if(PrefConections.NameConnection==DSNName)
								ParametersProc.Append("0"); //indicamos que esta nota proviene del servidor local
							else
								ParametersProc.Append("1"); //indicamos que esta nota proviene de otro servidor
							
							ParametersProc.Append(",'");
							ParametersProc.Append(ID_Usuario);	//Id_Usuario logeado actualmente
							ParametersProc.Append("',@A,@X)");
							
							
					
					
					
							PreferencesConnection PrefConectionsOfServerRemote;
		
							if( !UpdateStorys->GetPreferencesConnectionForNameConection( DSNName , PrefConectionsOfServerRemote ) )
							{
								break;
							}
		
							PMString StringConection="";
	
							StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
							InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
							(
								kN2PSQLUtilsBoss,	// Object boss/class
								IN2PSQLUtils::kDefaultIID
							)));
							ASSERT(SQLInterface);
							if (SQLInterface == nil) 
							{	
								break;
							}
							
							QueryVector.clear();
							//solo si la nota proviene del servidor de Preferencia
							if(PrefConectionsOfServerRemote.NameConnection == DSNName)
							{
								if(!SQLInterface->SQLQueryDataBase(StringConection,ParametersProc,QueryVector))
								{
									break;
								}
								
								///if(!SQLInterface->StoreProcedureSET_NOTA_COLO_CADA(StringConection,ParametersProc))
								//{
								//	CAlert::InformationAlert("Error al crar nueva nota");
								//	break;
								//}
						
							}
							
							
							/////////////////////////////////////////
							 // si la nota proviene de un servidor Remoto se obtiene los id de los elementos de la nota
							 // Para reemplazarlos por los Id de la nota que fue fluida que se encuentra en el XMP
							//if(PrefConections.NameConnection!=DSNName)
							//{
								//Codigo que se forma en el Store Procedure para almacenar datos de la nota nueva
											
							//	PMString Id_Nota = SQLInterface->ReturnItemContentbyNameColumn("Id_Nota",ParametersProc[5]);
							//	PMString Id_Titulo = SQLInterface->ReturnItemContentbyNameColumn("Id_Titulo",ParametersProc[5]);
							//	PMString Id_Balazo = SQLInterface->ReturnItemContentbyNameColumn("Id_Balazo",ParametersProc[5]);
							//	PMString Id_PieFoto = SQLInterface->ReturnItemContentbyNameColumn("Id_PieFoto",ParametersProc[5]);
							//	PMString Id_Content = SQLInterface->ReturnItemContentbyNameColumn("Id_Content",ParametersProc[5]);
							//	PMString FechaNueva = SQLInterface->ReturnItemContentbyNameColumn("Fecha",ParametersProc[5]);
					
							//	N2PSQLUtilities::Busca_y_Reemplaza_En_XMP_IDlementosNotas( ID_Elem_Padre , Id_Nota,
							//					Id_Titulo, Id_PieFoto, 
							//					  Id_Balazo, Id_Content,
							//					  FYHUCheckinNota,  DSNName,
							//					  FechaNueva);
							//}
				
					
							
					
				
							///////////////		
					
							this->CambiarIconoPen("Cualquiera");
				
									
							if(UpdateStorys->SeRequiereActualizacionDeNotasDeDocument(document))
							{
									//Actualizacion de contenido de elementos de una nota desde la base de datos
									UpdateStorys->ActualizaNotasDesdeBD(document);
							}
					
							//Coloca el Lock state del stori como bloquedo
							if(FueReemplazado)
							{
								//Coloca el Lock state del stori como bloquedo
								UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
							}
											
							//Solo si nos encontramos en InDesign realiza el Save Revision
							if(Applicacion.Contains("InDesign"))
							{
								//Para Gurdar Revision
								ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
								(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
								if(checkIn==nil)
									break;
						
								checkIn->AutomaticSaveRevi(document);
								//CAlert::InformationAlert("SALio aqui 3");
							}
											
							//Realiza Updates
							UpdateStorys->MakeUpdate(document,kTrue);
						}
					}
					else
					{*/
							//////////////////////////////
							//   la nota es del servidor local
							//////////////////////////////
							//retval = kComboBoxStatusWidgetID;
							
						
							
							PMString ParametersProc="CALL NOTA_COLOCA_Y_LIGA_A_PAGINA(";	
							
							if(N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID))
	 						{	//Toma el Id del Pase																	
	 								ParametersProc.Append( N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_ContentNota_WidgetID) );
	 						}
	 						else
	 						{
	 							ParametersProc.Append(ID_Elem_Padre);
	 						}
								//Id de la nota que fue colocada
							ParametersProc.Append(",");
					
							ParametersProc.Append(N2PSQL_ID_Pag);	//Folio o Id de la pagina en que fue colocada la nota
							ParametersProc.Append(",");
					
							//if(PrefConections.NameConnection==DSNName)
								ParametersProc.Append("0"); //indicamos que esta nota proviene del servidor local
							//else
							//	ParametersProc.Append("1"); //indicamos que esta nota proviene de otro servidor
							ParametersProc.Append(",'");
							
							ParametersProc.Append(ID_Usuario);	//Id_Usuario logeado actualmente
							ParametersProc.Append("',@A,@X)");
							
							
					
							
							
							
		
							if( !UpdateStorys->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
							{
								break;
							}
		
							StringConection="";
	
							StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
							/*InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
							(
								kN2PSQLUtilsBoss,	// Object boss/class
								IN2PSQLUtils::kDefaultIID
							)));
							ASSERT(SQLInterface);
							if (SQLInterface == nil) {	
								break;
							}*/
							//solo si la nota proviene del servidor de Preferencia
							QueryVector.clear();
							//solo si la nota proviene del servidor de Preferencia
							//if(PrefConectionsOfServerRemote.NameConnection == DSNName)
							//{
								if(!SQLInterface->SQLQueryDataBase(StringConection,ParametersProc,QueryVector))
								{
									break;
								}
								
								/*if(!SQLInterface->StoreProcedureSET_NOTA_COLO_CADA(StringConection,ParametersProc))
								{
									CAlert::InformationAlert("Error al crar nueva nota");
									break;
								}*/
						
							//}
							
							
							 // si la nota proviene de un servidor Remoto se obtiene los id de los elementos de la nota
							 // Para reemplazarlos por los Id de la nota que fue fluida que se encuentra en el XMP
							/*if(PrefConections.NameConnection!=DSNName)
							{
								//Codigo que se forma en el Store Procedure para almacenar datos de la nota nueva
											
								PMString Id_Nota = SQLInterface->ReturnItemContentbyNameColumn("Id_Nota",ParametersProc[0]);
								PMString Id_Titulo = SQLInterface->ReturnItemContentbyNameColumn("Id_Titulo",ParametersProc[0]);
								PMString Id_Balazo = SQLInterface->ReturnItemContentbyNameColumn("Id_Balazo",ParametersProc[0]);
								PMString Id_PieFoto = SQLInterface->ReturnItemContentbyNameColumn("Id_PieFoto",ParametersProc[0]);
								PMString Id_Content = SQLInterface->ReturnItemContentbyNameColumn("Id_Content",ParametersProc[0]);
								PMString FechaNueva = SQLInterface->ReturnItemContentbyNameColumn("Fecha",ParametersProc[0]);
								
								
								
								N2PSQLUtilities::Busca_y_Reemplaza_En_XMP_IDlementosNotas( ID_Elem_Padre , Id_Nota,
												Id_Titulo, Id_PieFoto, 
												  Id_Balazo, Id_Content,
												  FYHUCheckinNota,  DSNName,
												  FechaNueva);
							}*/
				
					
							
							/*	
								CREATE PROCEDURE NOTA_COLOCA_Y_LIGA_A_PAGINA 
								@ID_Elem_Padre VARCHAR (50) ,
								@N2PSQL_ID_Pag VARCHAR(50),
								@OTROSERVIDOR INTEGER,
								@ID_Usuario VARCHAR(50),
								@ErrorOcurrido INTEGER OUTPUT

							*/
					
				
							///////////////		
							
							
							this->CambiarIconoPen("Cualquiera");
				
									
							
							
							
							if(UpdateStorys->SeRequiereActualizacionDeNotasDeDocument(document))
							{
								//Actualizacion de contenido de elementos de una nota desde la base de datos
								UpdateStorys->ActualizaNotasDesdeBD(document);
							}
							
							
							
							//Coloca el Lock state del stori como bloquedo
							if(FueReemplazado)
							{
								//Coloca el Lock state del stori como bloquedo
								UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
							}
											
							
							//Solo si nos encontramos en InDesign realiza el Save Revision
							if(Applicacion.Contains("InDesign"))
							{
								//Para Gurdar Revision
								ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
								(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
								if(checkIn==nil)
									break;
						
								checkIn->AutomaticSaveRevi(document);
								//CAlert::InformationAlert("SALio aqui 3");s
							}
							
							
							//Realiza Updates
							//UpdateStorys->MakeUpdate(document,kTrue);
							
					//}
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}
								
				break;
			}
			
			
			case kN2PsqlStTextPieWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlStTextPieWidgetID");
				if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
				{
					//Item a buscar
					ItemToSearchString="PieFoto";
				
					//Obtiene texto del widget que se va a poner sobre el Textframe
					texto= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextPieWidgetID);
				
					ID_Elem_Padre= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
					if(!UpdateStorys->ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(ID_Elem_Padre,kTrue))
					{
						CAlert::InformationAlert(kN2PSQLDebeDepositarNotaPadreStringKey);
						break ;
					}
					
					//Obtiene le ID de la nota que se encuentra actualmente en vista
					PMString StringRefTextModelOfTagegedText="";
					StringRefTextModelOfTagegedText = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlUIDRefTexModelDePieFoto_WidgetID);  
					UID uidItemplace=StringRefTextModelOfTagegedText.GetAsNumber();
					UIDRef itemToPlace(db, uidItemplace);
					
					UIDModel = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
					if(UIDModel==0)
						break;
					UID uidMod=UIDModel;
					UIDRef graphicFrame(db, uidMod);
											
					UIDModelString.AppendNumber(UIDModel);

					//Obtiene estatus de nota
					PMString EstadoNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextStatusWidgetID);
					PMString FYHUCheckinNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlLastCheckInNoteTextEditWidgetID); 
					PMString ID_Elemento =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_Pie_WidgetID);
					
					///////////////////////////////////
					//Para saber si se puede fluir la nota
					PMString IDSeccionDeElemPadre="";
					PMString Consulta="";
					PMString StringConection="";
					K2Vector<PMString> QueryVector;
					PreferencesConnection PrefConectionsOfServerRemote;
		
					if( !UpdateStorys->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
					{
						break;
					}
					
					
					//
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					ASSERT(SQLInterface);
					if (SQLInterface == nil) {	
						break;
					}
					StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
					
					
					
					Consulta=" SELECT Id_Seccion, Dirigido_a FROM Elemento WHERE Id_Elemento=" +ID_Elem_Padre ;
					
					PMString dirigido_a="";
					//solo si la nota proviene del servidor de Preferencia
					//if(PrefConectionsOfServerRemote.NameConnection == DSNName)
					//{
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						
						if(QueryVector.Length()>0)
						{
							IDSeccionDeElemPadre = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
							dirigido_a = SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[0]);
						}
						else
							break;
						
						QueryVector.clear();
						Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "' , "+IDSeccionDeElemPadre+" , 24, @X)" ;
						if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
						{
							break;
						}
						else
						{
							PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
							if(RESULT=="0")
							{
								CAlert::InformationAlert(kN2PSQLUserCantLigarNotaAPagStringKey);
								break;
							}
							
						}
					//}
					///////////////////////////////////
					if(dirigido_a.NumUTF16TextChars()>0)
						EstadoNota=dirigido_a+"/"+EstadoNota;
					bool16 FueReemplazado=N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_Elem_Padre,ID_Elemento,UIDModelString,ItemToSearchString,DSNName,EstadoNota,SetLockStory,FYHUCheckinNota, kFalse /*No es elemento foto*/);
					
					//si no se desea reemplazar 
					if(FueReemplazado==kFalse)
						break;
					//Coloca Texto y aplica estilo a TextFlame y regresa el UID del TextModel
					this->PlaceTextAndAplicaStyle(ItemToSearchString,texto);
					
					/*bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
					if(!sa)
						break;
					
					this->AplicaStyle(ItemToSearchString);
					
					this->ApplicaColumnBreakSiNecesita();*/
					PMString string="";					
					PMString FecvhaSalida="0";
					UpdateStorys->EstableceParametrosToCheckInElementoNota(ID_Elem_Padre,
														ID_Elemento,
														UIDModelString.GetAsNumber(), 
														FecvhaSalida, 
														"0",
														"0",
														ID_Usuario,
														"0",
														"0",
														kFalse,
														document,
														string,kFalse,"");
					N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(ID_Elem_Padre,FecvhaSalida, document);
					
					//Coloca el Lock state del stori como bloquedo
					if(FueReemplazado)
					{
						//Coloca el Lock state del stori como bloquedo
						UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
					}
				}
				else
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
				}			
				break;
			}
			
			case kN2PsqlAlertNotaConFotoWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlAlertNotaConFotoWidgetID");
				InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
				if(panel==nil)
				{
			
					break;
				}
		
		
				InterfacePtr<IControlView>		iControViewList( panel->FindWidget(kN2PsqlNotasListBoxWidgetID), UseDefaultIID() );
				if(iControViewList==nil)
				{
			
					break;
				}
		
				N2PSQLListBoxHelper listHelper( iControViewList , kN2PsqlPluginID , kN2PsqlNotasListBoxWidgetID);
				IControlView * listBox = listHelper.FindCurrentListBox();
				if(listBox == nil) {
					break;
				}
				InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER); // kDefaultIID not def'd
				ASSERT_MSG(listCntl != nil, "listCntl nil");
				if(listCntl == nil) {
					break;
				}
				//  Get the first item in the selection (should be only one if the list box
				// has been set up correctly in the framework resource file)
				PMString Titulo="";
				PMString Seccion="";
				PMString Estado="";
				K2Vector<int32> multipleSelection ;
				listCntl->GetSelected( multipleSelection ) ;
				const int kSelectionLength =  multipleSelection.Length() ;
				if (kSelectionLength> 0 )
				{
				
					int indexSelected = multipleSelection[0];
					listHelper.ObtenerTextoDeSeleccionActual(kN2PsqlIDNotaLabelWidgetID,Titulo,kN2PsqlSeccionLabelWidgetID,Seccion,indexSelected);
					
					N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPhotoPanelWidgetID, kTrue ) ;
				
					PMString Busqueda="CALL N2PV1055_QueryFotoFromElemPadre (" + Titulo + ")" ;
 					 N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto(Busqueda);
				}
				
				
 				 
 				 break;
			}
			
			
			///Para cancelar la nueva nota
			case kN2PsqlBotonCancelNewNoteWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlBotonCancelNewNoteWidgetID");
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
			
				if(UpdateStorys==nil)
				{
					break;
				}
			
				UpdateStorys->CancelaNuevaNota();
				break;
			}
			
			///Para adicionar la nueva nota la nueva nota
			case kN2PsqlBotonSubirNewNoteWidgetID:
			{
				//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update kN2PsqlBotonSubirNewNoteWidgetID");
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
			
				if(UpdateStorys==nil)
				{
					break;
				}
			
				UpdateStorys->DoSubirABDNuevaNota();
				break;
			}
		 
		}
		

		
					
							

			
										
			
	}
		
		
		
			if(thisID == kN2PSQLCkBoxShowEdgesEditWidgetID) 
			{
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
			
				if(UpdateStorys==nil)
				{
					break;
				}
			
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								
				if(N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowEdgesEditWidgetID))
				{	
					
					UpdateStorys->ShowFrameEdgesDocument( document, kFalse);	
				}
				else
				{
					UpdateStorys->ShowFrameEdgesDocument( document, kTrue);	
																
				}
			}
			else
			{
			
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
					(
						kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
						IUpdateStorysAndDBUtils::kDefaultIID
					)));
			
				if(UpdateStorys==nil)
				{
					break;
				}
				
				
				if(thisID == kN2PSQLCkBoxShowTxtOversetEditWidgetID)
				{
					if(N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowTxtOversetEditWidgetID))
					{	
					
						UpdateStorys->DoMuestraNotasConFrameOverset();
					}
					else
					{
						
							InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
							(
								kN2PCTUtilitiesBoss,	// Object boss/class
								IN2PCTUtilities::kDefaultIID
							)));
			
							if(N2PCTUtils==nil)
							{
								break;
							}
				
							N2PCTUtils->OcultaNotasConFrameOverset();					
					}
				}
			}
		
		
	} while(kFalse);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlWidgetObserver::Update fin");
}

/*int32 N2PsqlWidgetObserver::PlaceTextAndAplicaStyleOnTextModel(InterfacePtr<ITextModel> iTextModel,PMString NomStyle, PMString Texto)
{
	int32 UIDTexModelOfMyFrame=0;
	
	do
	{
		ASSERT(iTextModel);
		if (iTextModel == nil) {	
			break;
		}

		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		WideString * inserthis= new WideString(Texto);
		
		
	
		//Obtiene el ID del TextModel
		PMString f="";
		IFrameList* Listffame=iTextModel->QueryFrameList();
		UID UIDTextModel=Listffame->GetTextModelUID();
		UIDTexModelOfMyFrame=UIDTextModel.Get();
		
	
		if(iTextModel->Insert(kTrue, 0, inserthis, nil)!=kSuccess)
		{
			break;
		}
		
	
		InterfacePtr<ITextModelCmds> iTextModelCmd(iTextModel,UseDefaultIID());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}
		

		///insertar texto
		const bool16 copyString=kFalse;
		CAlert::InformationAlert("A chinga chinga");
	
		//aplicar Estilo
		InterfacePtr<IWorkspace> theWS(::QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}

	
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);
		if(ParaStyleNameTable==nil)
		{
			break;
		}
	
		UID myStyleUID=ParaStyleNameTable->FindByName(NomStyle);
		if(myStyleUID==nil)
		{
			break;
		}

		InterfacePtr<ICommand> applyCmd(iTextModelCmd->ApplyStyleCmd(0,iTextModel->TotalLength(), myStyleUID,kParaAttrStrandBoss,kFalse));
		if(applyCmd==nil)
		{
			break;
		}

		if(CmdUtils::ProcessCommand(applyCmd)!=kSuccess)
		{
			break;
		}
		
		
	}while(false);
	return(UIDTexModelOfMyFrame);
}
*/

bool16 N2PsqlWidgetObserver::HacerRevisionPaDocNuevo()
{
	bool16 retval=kFalse;
	do
	{
		//CAlert::InformationAlert("1");
		PMString NameDocumentActual="";
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		//CAlert::InformationAlert("2");
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
			break;
		}
		
		//Verifica que no sea un documento nuevo
			document->GetName(NameDocumentActual);
			
		//CAlert::InformationAlert("3");
		//Para Gurdar Revision
			ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			
		
		//CAlert::InformationAlert("4");
			if(NameDocumentActual.Contains("Untitled") || NameDocumentActual.Contains("Sin tÌtulo-") || NameDocumentActual.Contains(".indt"))
			{
				//Hace revision
				if(checkIn->SaveRevi(kTrue))
				{
					retval=kTrue;
				}
				break;
			}
			
			
		///Obtiene las Ultimas Preferencias
			PrefParamDialogInOutPage PreferencesPara;
		//CAlert::InformationAlert("5");
			checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
			//checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
		
		//Hay que validar que ya se ha realizadon un chech in o save revition
		// y que la pagina se llame igual a nombre de pagina actual
			if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars() == 0)
			{
				//CAlert::InformationAlert(PreferencesPara.Pagina_To_SavePage);
				
				//si no se ha realizado un previo save as o  save se habre el diago save as 
				if(checkIn->SaveRevi(kTrue))
				{
					retval=kTrue;
				}
				break;
			}
		//CAlert::InformationAlert("6");
			PMString NameDocUltimoSaveAs=PreferencesPara.Pagina_To_SavePage;
			NameDocUltimoSaveAs.Append(".indd");
		
			
			///Para ver si el documento de enfrente se llama igual que el ultimo salvado
			if(NameDocumentActual!=NameDocUltimoSaveAs)
			{
			
				if(checkIn->SaveRevi(kTrue))
				{
					retval=kTrue;
				}
				break;
			}
		//CAlert::InformationAlert("8");
			retval=kTrue;
	}while(false);
	return(retval);
}
	
/*
handleWidgetHit
*/
void N2PsqlWidgetObserver::handleWidgetHit(InterfacePtr<IControlView> & controlView,const ClassID & theChange, WidgetID widgetID) 
{
	do {

		ASSERT(widgetID != kInvalidWidgetID);
		ASSERT(controlView);
		if(controlView==nil) {
			break;

		}
	
		// TODO: add code to handle this event

	} while(kFalse);
}



void N2PsqlWidgetObserver::crearPDF()
{
	do
	{




		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars()-5,nombre.NumUTF16TextChars());
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

			//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		   UIDList pageUIDs = UIDList(::GetDataBase(document));
		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		   ASSERT(iPageList);
		   if(!iPageList) {
			   break;
		   }
		   // Setup the page UID list to include
		   // the pages you want to export
		   int32 cPages = iPageList->GetPageCount();
		   for (int32 iPage = 0; iPage < cPages; iPage++ )
		   {
			   UID uidPage = iPageList->GetNthPageUID(iPage);
			   pageUIDs.Append(uidPage);
		   }


		PMString filePath="C:\\Archivos de programa\\FileMaker\\FileMaker Pro 5.5\\Web\\Archivos PDF\\";
			filePath.Append(nombre);
			filePath.Append(".pdf");
		/********/
		// obtain command boss
        InterfacePtr<ICommand> pdfExportCmd(CmdUtils::CreateCommand(kPDFExportCmdBoss));
		
		// set export prefs
		InterfacePtr<IPDFExportPrefs> pdfExportPrefs(pdfExportCmd, UseDefaultIID());
		// todo: call specific methods...
		
		// set SysFileData
		PMString pmFilePath(filePath);
		pmFilePath.SetTranslatable(kFalse);
		IDFile sysFilePath = FileUtils::PMStringToSysFile(pmFilePath);
		InterfacePtr<ISysFileData> trgSysFileData(pdfExportCmd, IID_ISYSFILEDATA);
		trgSysFileData->Set(sysFilePath);
		
		// set security preferences
		InterfacePtr<IPDFSecurityPrefs> securityPrefs(pdfExportCmd, UseDefaultIID());
		// todo: add calls to specific methods...

		// set print content prefs
		InterfacePtr<IPrintContentPrefs> printContentPrefs(pdfExportCmd, IID_IPRINTCONTENTPREFS);
		// todo: add calls to specific methods...

		// set to true if outputting a book file 
		InterfacePtr<IBoolData> bookExport(pdfExportCmd, IID_IBOOKEXPORT);
		bookExport->Set(kFalse); 

		// set UI flags
		InterfacePtr<IUIFlagData> uiFlagData(pdfExportCmd, IID_IUIFLAGDATA);
		uiFlagData->Set(uiFlags );
		
		// set UIDList containing pages to output (item list of command)
		UIDList theList(pageUIDs);
		pdfExportCmd->SetItemList(theList);
		
		// set UIDRefs containing pages to output (IOutputPages)
		InterfacePtr<IOutputPages> outputPages(pdfExportCmd, UseDefaultIID());
		outputPages->Clear();
	//	IDataBase* db = theList.GetDataBase();
		outputPages->SetMasterDataBase(db);
		int32 nProgressItems = theList.Length(); 
		for (int32 index = 0 ; index < nProgressItems ; index++) 
		{
        	outputPages->AppendOutputPageUIDRef(theList.GetRef(index));
		}

		// set PDF document name (get name from IDocument)
		InterfacePtr<IDocument> doc ((IDocument*)db->QueryInstance(db->GetRootUID(), IID_IDOCUMENT));
		PMString documentName; 
		doc->GetName(documentName);
		outputPages->SetName(documentName);
		
		// setup progress bar, if not suppressing the UI.
		K2::scoped_ptr<RangeProgressBar> deleteProgressBar;
		bool16 bShowImmediate = kTrue;
		if( uiFlags != kSuppressUI )
		{
			RangeProgressBar *progress = new RangeProgressBar( "Generating PDF", 0, nProgressItems, bShowImmediate, kTrue, nil, kTrue); 
			pdfExportPrefs->SetProgress(progress);
			deleteProgressBar.reset( progress );
		}
		
		// finally process command
		status = CmdUtils::ProcessCommand(pdfExportCmd);
		CAlert::InformationAlert(kN2PsqlPDFExportadoStringKey);		
	}while(false);
}




void N2PsqlWidgetObserver::CreateAndProcessCloseWinCmd(IWindow *win)
{
	// Create a CloseWinCmd:
	InterfacePtr<ICommand> closeCmd(CmdUtils::CreateCommand(kCloseWinCmdBoss));
	// Get an ICloseWinCmdData Interface for the CloseWinCmd:
	InterfacePtr<ICloseWinCmdData> closeData(closeCmd, IID_ICLOSEWINCMDDATA);
	// Set the ICloseWinCmdData Interface's data:
	closeData->Set(win);
	// Process the CloseWinCmd:
	if (CmdUtils::ProcessCommand(closeCmd) != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure, kFalse);
	}
}





/**
*/
void N2PsqlWidgetObserver::LlenarTextoFechaActual()
{
	PMString Tfecha;
	struct tm *tp;
	long t;
	time(&t);
	tp=localtime(&t);
	asctime(tp);

	
	Tfecha.AppendNumber(tp->tm_mday+1);
	Tfecha.Append("/");
	Tfecha.AppendNumber(tp->tm_mon+1);
	Tfecha.Append("/");
	Tfecha.AppendNumber(tp->tm_year+1900);
	do
	{
		InterfacePtr<IWidgetParent> MyParent(this, UseDefaultIID());
		if(!MyParent) {
			break;
		}
		InterfacePtr<IPanelControlData> panelControlData(MyParent, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		IControlView *iControlView = panelControlData->FindWidget(kN2PsqlTextFechaWidgetID);
		InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
		if (TextControl == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		Tfecha.SetTranslatable(kFalse);
		TextControl->SetString(Tfecha);
	}while(false);
}


void N2PsqlWidgetObserver::LimpiarWidgets()
{

	this->LipiaWidget(kN2PsqlTextNotaWidgetID);
	//this->LipiaWidget(kN2PsqlTextFechaWidgetID);
	this->LipiaWidget(kN2PsqlComboPubliWidgetID);
	this->LipiaWidget(kN2PsqlComboSeccionWidgetID);
	this->LipiaWidget(kN2PsqlTextTituloWidgetID);
	this->LipiaWidget(kN2PsqlTextBalazoWidgetID);
	this->LipiaWidget(kN2PsqlTextPieWidgetID);
	this->LipiaWidget(kN2PsqlTextPageWidgetID);
	this->LipiaWidget(kN2PsqlTextEnGuiaWidgetID);
	this->LipiaWidget(kN2PsqlTextStatusWidgetID);
}

void N2PsqlWidgetObserver::LipiaWidget(WidgetID widget)
{
	do
	{
		InterfacePtr<IWidgetParent> MyParent(this, UseDefaultIID());
		if(!MyParent) {
			break;
		}
		InterfacePtr<IPanelControlData> panelControlData(MyParent, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		IControlView *iControlView = panelControlData->FindWidget(widget);
		InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
		if (TextControl == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		TextControl->SetString(" ");
	}while(false);
}

PMString N2PsqlWidgetObserver::AbrirDialogoBusqueda(bool16& FindOnlyPases)
{
	PMString Busqueda="";
	PMString TablasBusqueda="";
	PMString CondicionesTerminales="";
	
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PsqlPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kN2PsqlBusquedaDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
				
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			CAlert::ErrorAlert("N2PsqlWidgetObserver::AbrirDialogoBusqueda: dialogController invalid");
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		dialog->WaitForDialog();
		//Consulta Parametros
		
		PMString Pagina=dialogController->GetTextControlData(kN2PsqlTextPageWidgetID);
		PMString Fecha=dialogController->GetTextControlData(kN2PsqlTextFechaWidgetID);
		PMString GuiaNota=dialogController->GetTextControlData(kN2PsqlTextEnGuiaWidgetID);
		
		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha);
		PMString Estado=dialogController->GetTextControlData(kN2PsqlTextStatusWidgetID);
		PMString Seccion=dialogController->GetTextControlData(kN2PsqlComboSeccionWidgetID);
		PMString Publicacion=dialogController->GetTextControlData(kN2PsqlComboPubliWidgetID);
		PMString RoutedTo=dialogController->GetTextControlData(kN2PsqlComboRoutedToWidgetID);
		ITriStateControlData::TriState stateCheckBox =	dialogController->GetTriStateControlData(kN2PSQLCkBoxOnlyPageJumpsWidgetID);
		if(Pagina=="Cancel" || Pagina=="Cancelar")
		{
			Busqueda="";
			break;
		}
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			break;
		}	
		
		PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if (app == nil)
		{	
			break;
		}	
		PMString nameApp=app->GetApplicationName();
		nameApp.SetTranslatable(kFalse);
		
		if(stateCheckBox==ITriStateControlData::kSelected)		
		{
			FindOnlyPases=kTrue;
			if(nameApp.Contains("InDesign"))
			{
				Busqueda="EXEC BuscaNota2OnlyPases '" + Pagina + "' , '" + Fecha + "' , '" + Seccion  + "' , '" + Publicacion  + "' , '"  + RoutedTo  + "' , '"  + Estado + "', '" + ID_Usuario +"',''";
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlNotasListBoxWidgetID, kFalse);
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID, kTrue);
			}
		}
		else
		{
			FindOnlyPases = kFalse;
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlNotasListBoxWidgetID, kTrue);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID, kFalse);
			if(nameApp.Contains("InDesign"))
			{
				Busqueda="CALL Busca_Notas_Por_Parametros3 ('" + Pagina + "' , '" + MySQLFecha + "' , '" + Seccion  + "' , '" + Publicacion  + "' , '"  + RoutedTo  + "' , '"  + Estado + "', '" + ID_Usuario +"',4,'"+GuiaNota+"')";
				//CAlert::InformationAlert(Busqueda);
			}
			else
			{
				Busqueda="CALL Busca_Notas_Por_Parametros3 ('" + Pagina + "' , '" + MySQLFecha + "' , '" + Seccion  + "' , '" + Publicacion  + "' , '"  + RoutedTo  + "' , '"  + Estado + "', '" + ID_Usuario +"',12,'"+GuiaNota+"')";
			}
			//CAlert::InformationAlert(Busqueda);		
				
		}
		
	} while (false);	
	//	
	
	return(Busqueda);
}





int32 N2PsqlWidgetObserver::PlaceTextAndAplicaStyle(PMString NomStyle, PMString Texto)
{
	int32 UIDTexModelOfMyFrame=0;
	
	do
	{

		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		WideString * inserthis= new WideString(Texto);
		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			break;
		}

	
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			break;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
		if(pTextTarget==nil)
		{
			break;
		}

		InterfacePtr<ITextFocus> pFocus(pTextTarget->QueryTextFocus());
		if(pFocus==nil)
		{
			break;
		}
	
	
		InterfacePtr<ITextModel> iTextModel(pFocus->QueryModel());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			CAlert::ErrorAlert("Error al intentar obtener el Modelo de Texto.");
			break;
		}
		int32 length = iTextModel->TotalLength()-1;
	
		
		/////////////////////
		//Obtiene el ID del TextModel
		PMString f="";
		IFrameList* Listffame=iTextModel->QueryFrameList();
		UID UIDTextModel=Listffame->GetTextModelUID();
		UIDTexModelOfMyFrame=UIDTextModel.Get();
		
		////////////////////
		if(iTextModel->Insert( 0, inserthis)!=kSuccess)
		{
			break;
		}
		
		TextIndex start = inserthis->NumUTF16TextChars();
		
		//PMString FF="";
		//FF.AppendNumber(start);
		//FF.Append(",");
		//FF.AppendNumber(length);
		//CAlert::InformationAlert(FF);
		N2PSQLUtilities::DeleteText(iTextModel,start,length);
		
		InterfacePtr<ITextModelCmds> iTextModelCmd(iTextModel,UseDefaultIID());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}
		

		///insertar texto
		const bool16 copyString=kFalse;
		
	
/*		//aplicar Estilo
		InterfacePtr<IWorkspace> theWS(::QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}

	
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);
		if(ParaStyleNameTable==nil)
		{
			break;
		}
	
		UID myStyleUID=ParaStyleNameTable->FindByName(NomStyle);
		if(myStyleUID==nil)
		{
			break;
		}

		InterfacePtr<ICommand> applyCmd(iTextModelCmd->ApplyStyleCmd(0,iTextModel->TotalLength(), myStyleUID,kParaAttrStrandBoss));
		if(applyCmd==nil)
		{
			break;
		}

		if(CmdUtils::ProcessCommand(applyCmd)!=kSuccess)
		{
			break;
		}

*/

	/*	////////////////////////////////////////////////
		
		UIDRef TextModelUIDRef(db,UIDTextModel);
		UIDTextModel=TextModelUIDRef.GetUID();
		
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		

		f.Append(",");
		f.AppendNumber(UIDTextModel.Get());
	
		WideString * myinserthis= new WideString("OK MAN");
		if(iTextModel->Insert(kTrue, 0, myinserthis, nil)!=kSuccess)
		{
			break;
		}
		///////////////////////////////////////////////*/
	}while(false);
	return(UIDTexModelOfMyFrame);
}


bool16 N2PsqlWidgetObserver::ApplicaColumnBreakSiNecesita()
{
	
	
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		
		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			break;
		}

	
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			break;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
		if(pTextTarget==nil)
		{
			break;
		}

		InterfacePtr<ITextFocus> pFocus(pTextTarget->QueryTextFocus());
		if(pFocus==nil)
		{
			break;
		}
	
	
		InterfacePtr<ITextModel> iTextModel(pFocus->QueryModel());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			CAlert::ErrorAlert("Error al intentar obtener el Modelo de Texto.");
			break;
		}
		
			InterfacePtr<IComposeScanner> cs(iTextModel, UseDefaultIID()); 
			// Setup a variable to retrieve the length of the text 
			if(cs==nil)
			{
				break;
			}
			// Setup a variable to retrieve the length of the text 
			int32 length = iTextModel->TotalLength();//obtengo el total de caracteres de la cadena en el textframe
			int32 index=0;//numero de caracter leido
			WideString Temp;
			cs->CopyText(index, length-1, &Temp);
			// Setup a PMString to hold the text (depending on what you want to do with the text, the textchar may be ok) 
			// Load the text into the buf variable 
			//buf.SetXString(buffer, length); 
			PMString texto="";
			texto.Append(Temp);
			
			index = texto.LastIndexOfWChar(13);
			
			while(index>0)
			{	
				PMString numindex="";
				numindex.AppendNumber(index);
				//CAlert::InformationAlert(numindex);
				//CAlert::InformationAlert(texto);
				
				InterfacePtr<ITextModelCmds> textModelCmds(iTextModel, UseDefaultIID());
				ASSERT(textModelCmds);
				if (!textModelCmds) 
				{
					CAlert::ErrorAlert("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota textModelCmds is invalid");
					ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota textModelCmds is invalid");	
					break;
				}
				InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(index,1));
				ASSERT(deleteCmd);
				if (!deleteCmd) 
				{
					CAlert::ErrorAlert("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota deleteCmdΩ is invalid");
					ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota deleteCmd is invalid");
					break;
				}
				CmdUtils::ProcessCommand(deleteCmd);
				
				texto.Remove(index,1);
				//CAlert::InformationAlert(texto);
				UIDRef storyUIDRef(::GetUIDRef(iTextModel));		
				InsertBreakColumnCmd(storyUIDRef, index );
				
				index = texto.LastIndexOfWChar(13);
			}
			
	}while(false);
	
	return(kTrue);
}

bool16 N2PsqlWidgetObserver::InsertBreakColumnCmd(const UIDRef storyUIDRef, const TextIndex finishIndex )
{
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<ICommand> breakCommand(CmdUtils::CreateCommand(kInsertBreakCharacterCmdBoss));
   		ASSERT(breakCommand);
     	if(breakCommand == nil) 
         {
            break;
         }
        
         breakCommand->SetItemList(UIDList(storyUIDRef));
         									
         InterfacePtr<IRangeData> rangeData(breakCommand, UseDefaultIID());
         ASSERT(rangeData);
         if(rangeData == nil) 
         {
             break;
         }
         rangeData->Set(finishIndex-1,finishIndex-1);
         									
         InterfacePtr<IIntData> intData(breakCommand,UseDefaultIID());
         ASSERT(intData);
         if(intData == nil) 
         {
             break;
         }
         									
         intData->Set(Text::kAtColumn);
         CmdUtils::ProcessCommand(breakCommand);
	}while(false);
	return(retval);
}






/**
*/
bool16 N2PsqlWidgetObserver::CambiarIconoPen(PMString NombreAviso)
{
	int32 indexSelected;
	bool16 Accion=kFalse;
	do
	{
		InterfacePtr<IPanelControlData> panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID));	
		ASSERT_MSG( panelData !=nil, "panelData nil");
		if(panelData==nil) 
		{
			CAlert::ErrorAlert("No se encontro un panel");
			break;
		}
		IControlView * listBox = panelData->FindWidget(kN2PsqlNotasListBoxWidgetID);
		if(listBox == nil)
		{
			CAlert::ErrorAlert("No se encontro el control vista de la lista");
			break;
		}

		N2PSQLListBoxHelper listHelper(listBox, kN2PsqlPluginID,kN2PsqlNotasListBoxWidgetID);
		
		InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		
		if(listCntl == nil) 
		{
			Accion=kFalse;
			break;
		}
		//  Get the first item in the selection (should be only one if the list box
		// has been set up correctly in the framework resource file)
			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;
			const int kSelectionLength =  multipleSelection.Length() ;
			if (kSelectionLength> 0 )
			{
				PMString dbgInfoString("Selected item(s): ");
				for(int i=0; i < multipleSelection.Length() ; i++) 
				{
					indexSelected = multipleSelection[i];
					dbgInfoString.AppendNumber(indexSelected);
					if(i < kSelectionLength - 1) {
						dbgInfoString += ", ";
					}
				}
					
					listHelper.Mostrar_IconFluido(indexSelected);
			}
			else
			{
				Accion=kFalse;
				//CAlert::InformationAlert(kN2PsqlMsgAlertHacerSeleccionTextKey);
			}
	} while(0);
	return(Accion);
}

/*
bool16 N2PsqlWidgetObserver::UpdateDB(PMString NamePreferenceConection,PMString Consulta)
{
	bool16 retval=kFalse;
	do
	{	
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
	
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		//solo si la nota proviene del servidor de Preferencia
		if(PrefConections.NameConnection == NamePreferenceConection)
		{
			
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
	
			if(SQLInterface->SQLSetInDataBase(StringConection,Consulta))
				retval=kTrue;
			else
				retval=kFalse;
		}
			
			
	}while(false);
	return(retval);
}
*/

/*PMString N2PsqlWidgetObserver::ReadDBByItem(PMString Consulta,PMString Item)
{
	PMString cadena;

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn(Item,QueryVector[i]);
		}
	}while(false);
		
	
	return(cadena);
	

}

*/
//Nombre de la capa Update N2PUpdate
/*void N2PsqlWidgetObserver::creatCapaN2PSQL(PMString NombredeCapa)
{

		ErrorCode error = kCancel;
	
		// We'll access this later, so we must create it here:
		IAbortableCmdSeq* newLayerCmdSequence = nil;
		 
		UID LayerUID=nil;
		// We'll use a do-while(false) to break out on bad pointers:
		do
		{
		
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
				break;
			}
			
			
			InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
			if (layerList == nil)
			{
				ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
				//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
				break;
			}
			
			//Busca de una capa por nombre.
			LayerUID=layerList->FindByName(NombredeCapa);
			if(LayerUID!=nil)//salir en caso de que exista esta capa
				break;
				
			// To create a new layer requires the new layer command followed by the
			// set active layer command, so we'll do it as a sequence:
			newLayerCmdSequence = CmdUtils::BeginAbortableCmdSeq();
			if (newLayerCmdSequence == nil)
			{
				ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: newLayerCmdSequence invalid");
				break;
			}
			
			error = kFailure; // Indicates we successfully started command sequence.

			//pone el nombre de la capa
			newLayerCmdSequence->SetName(NombredeCapa);
			////CAlert::ErrorAlert("2");
			
			// Create a new layer in the frontmost document:
			InterfacePtr<ICommand> newLayerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));
			////CAlert::ErrorAlert("3");
			InterfacePtr<INewLayerCmdData> newLayerCmdData(newLayerCmd, IID_INEWLAYERCMDDATA);
			////CAlert::ErrorAlert("4");
			if (newLayerCmd == nil || newLayerCmdData == nil)
			{
				ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: newLayerCmdData invalid");
				////CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
				break;
			}
					// Now set the name of this layer:
			//SetLayerName(NombredeCapa);
			////CAlert::ErrorAlert("5");
			
			// Create the document UID Ref to use here and in the active layer cmd:
			const UIDRef docUIDRef = ::GetUIDRef(document);
			////CAlert::ErrorAlert("6");
			
			// Create it in the front most document and set the name:
			newLayerCmdData->Set(docUIDRef, &NombredeCapa);
			////CAlert::ErrorAlert("7");
			// Process the command
			error = CmdUtils::ProcessCommand(newLayerCmd);
			////CAlert::ErrorAlert("8");
			// Check for error, and alert the user if so:
			if (error != kSuccess)
			{
				ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: newLayerCmdData failed");
				////CAlert::WarningAlert("Error al intentar Crear nueva Capa");
				break;
			}

			// We've created a new layer.  Let's get its UID.  First we get the pointer to the last layer:
			IDocumentLayer* documentLayer = layerList->QueryLayer(layerList->GetCount()-1);
			////CAlert::ErrorAlert("9");
			// Now we've created the new layer, we must set it as the active layer:
			InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
			////CAlert::ErrorAlert("10");
			InterfacePtr<ILayoutCmdData> layoutCmdData(setActiveLayerCmd, UseDefaultIID());
			////CAlert::ErrorAlert("11");
			InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			////CAlert::ErrorAlert("12");
			if (setActiveLayerCmd == nil || layoutCmdData == nil || layoutControlData == nil)
			{
				ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd, layoutCmdData, or layoutControlData invalid");
				////CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
				break;
			}

			setActiveLayerCmd->SetItemList(UIDList(documentLayer));
			////CAlert::ErrorAlert("13");
			layoutCmdData->Set(docUIDRef, layoutControlData);
			////CAlert::ErrorAlert("14");
			// Process the command
			error = CmdUtils::ProcessCommand(setActiveLayerCmd);
			////CAlert::ErrorAlert("15");
			// Check for error, and alert the user if so:
			if (error != kSuccess)
			{
				ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd failed");
				////CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			}
		}while(false);
		if (newLayerCmdSequence != nil)
		{
			// End command sequence, either aborting if we had an error or
			// ending it if we're successful:
			if (error == kFailure)
			{
				CmdUtils::AbortCommandSequence(newLayerCmdSequence);
			}
			else if (error == kSuccess)
			{
				// We were successful.
				CmdUtils::EndCommandSequence(newLayerCmdSequence);
			}
		}
		// Otherwise we never were able to start the sequence.
}
*/
/*void N2PsqlWidgetObserver::CrearTextFrameUpdate(PMString CadenaAImprimir)
{
	
		//en caso de que no exista se crea y se posiciona sobre esta capa
	do
	{

		UIDRef Ref=this->BucaTextFrameUpdate();
		if(Ref.ExistsInDB())
		{//ya existe el UIDRef
			InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layout == nil)
			{
				ASSERT_FAIL("ILayoutControlData pointer nil");
				break;
			}

			//obtengo la interfaz de jerarquia de la capa
			InterfacePtr<IHierarchy> Hierarchy(layout->QueryActiveLayer());
			if (Hierarchy == nil)
			{
				ASSERT_FAIL("IHierarchy pointer nil");
				
				break;
			}

			// Get the UIDRef of the layer:
			UIDRef layerRef(::GetUIDRef(Hierarchy));

			IDataBase* db = layerRef.GetDataBase();

			//UIDRef Ref(db,TextFrameUpdate);

			PMString textToInsert(CadenaAImprimir);
			textToInsert.Translate(); // Look up our string and replace.
					
			//WideString* myText = new WideString(textToInsert);
			K2::shared_ptr<WideString> myText(new WideString(textToInsert));

			InterfacePtr<IGeometry> geometry(Ref, IID_IGEOMETRY); 
			if(geometry==nil)
			{
				CAlert::ErrorAlert("geometri");
				
				break;
			}

			InterfacePtr<IMultiColumnTextFrame > textframe(Ref, IID_IMULTICOLUMNTEXTFRAME ); 
			if(textframe==nil)
			{
				CAlert::ErrorAlert("Error no encontro el Frame2");
				
				break;
			}

			ITextModel* textModel=textframe->QueryTextModel();
			if(textModel==nil)
			{
				CAlert::ErrorAlert("No encontro el Modelo detexto");
				
				break;
			}

			InterfacePtr<ITextModelCmds> iTextModelCmd(textModel,UseDefaultIID());
			if(iTextModelCmd==nil)
			{
				ASSERT_FAIL("ITextModel pointer nil");
				break;
			}

			InterfacePtr<ICommand> insertTextCommand (iTextModelCmd->InsertCmd(textModel->TotalLength()-1, myText, kFalse));
			if (insertTextCommand == nil)
			{
				ASSERT_FAIL("ICommand pointer nil");
				break;
			}
			if(CmdUtils::ProcessCommand(insertTextCommand) != kSuccess)
			{
				CAlert::ModalAlert
				(
				CadenaAImprimir,			// Alert string
				kYesString,	// OK button
				kNullString,	// No second button
				kNullString,	// No third button
				1,									// Set OK button to default
				CAlert::eWarningIcon					// Warning icon.
				);			
			}

		}
		else
		{//No Existe El UIDRef
			//obtiene la intefaz ILayoutControlData de la capa de enfrente pues es en la capa update donde
			//se debe crear este text frame
			InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layout == nil)
			{
				ASSERT_FAIL("ILayoutControlData pointer nil");
				break;
			}

			// Create the NewFrameCmd:
			InterfacePtr<ICommand> newFrameCommand(CmdUtils::CreateCommand(kCreateMultiColumnItemCmdBoss));
			if (newFrameCommand == nil)
			{
				ASSERT_FAIL("ICommand pointer nil");
				break;
			}

			// Instantiate a data page item data interface:
			InterfacePtr<INewPageItemCmdData> newData(newFrameCommand, UseDefaultIID());
			if (newData == nil)
			{
				ASSERT_FAIL("INewPageItemCmdData pointer nil");
				break;
			}

			//Obtien la jerarquia de la capa
			InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
			if (layerHier == nil)
			{
				ASSERT_FAIL("IHierarchy pointer nil");
				break;
			}

			// Get the UIDRef of the layer:
			UIDRef layerRef(::GetUIDRef(layerHier));

			// Get the bounding box for the current page
			IDataBase* db = layerRef.GetDataBase();
			UIDRef pageUIDRef = UIDRef(db,layout->GetPage());
			InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
			if (pageGeometry == nil)
			{
				ASSERT_FAIL("IGeometry pointer nil");
				break;
			}
			PMReal leftMargin=0,rightMargin=500,topMargin=0,bottomMargin=500;
			//obtiene las coordenadas del textframe
			
			// Reset stepping if we've changed page
			if (fPageUIDRef != pageUIDRef)
			{
				fStep = 0; 
				fPageUIDRef = pageUIDRef;
			}


			// we need to bump the text box along a bit for the next time...
			const int16 addStep = fStep*20;		
			// Here are the coordinates for our frame to be created at. 
			//bottomMargin=topMargin+((bottomMargin-topMargin)/2);
			PMPointList points(2);
			PMPoint startPoint(leftMargin,topMargin);	// left, top
			PMPoint endPoint(rightMargin, bottomMargin);	// right, bottom	

			// convert them to pasteboard coords now
			TransformInnerPointToPasteboard(pageGeometry,&startPoint);
			TransformInnerPointToPasteboard(pageGeometry,&endPoint);
			// Fill in the PointList:
			points.push_back( startPoint );
			points.push_back( endPoint );

			//obtiene la interfaz para crear un frame de datos
			InterfacePtr<ICreateMCFrameData> frameData( newFrameCommand, UseDefaultIID() );
			if (frameData == nil)
			{
				ASSERT_FAIL("ICreateMCFrameData pointer nil");
				break;
			}
			
			//pone los atributos de la capa
			newData->Set(layerRef.GetDataBase(), kSplineItemBoss, layerRef.GetUID(), points);
						//bd del layer, el jefe que se utiliza, la referencia a la capa, coordenadas

			//procesa comando
			if(CmdUtils::ProcessCommand(newFrameCommand) == kSuccess)
			{
				//Adiciona el texto igual que la cracioon de la etiqueta
				fStep++; // Increment how many frames we've created.

				PMString textToInsert(CadenaAImprimir);
				textToInsert.Translate(); // Look up our string and replace.
					
				//WideString* myText = new WideString(textToInsert);
				K2::shared_ptr<WideString> myText(new WideString(textToInsert));
				
				InterfacePtr<ITextModel> textModel(frameData->GetDataBase(), frameData->GetStory(), ITextModel::kDefaultIID);
				if (textModel == nil)				//database					UID frame			lo mismo
				{
					ASSERT_FAIL("ITextModel pointer nil");
					break;
				}

				InterfacePtr<ITextModelCmds> iTextModelCmd(textModel,UseDefaultIID());
				if(iTextModelCmd==nil)
				{
					ASSERT_FAIL("ITextModel pointer nil");
					break;
				}

				InterfacePtr<ICommand> insertTextCommand (iTextModelCmd->InsertCmd(0, myText, kFalse));
				if (insertTextCommand == nil)
				{
					ASSERT_FAIL("ICommand pointer nil");
					break;
				}
				if(CmdUtils::ProcessCommand(insertTextCommand) != kSuccess)
				{
					CAlert::ModalAlert
					(
					CadenaAImprimir,			// Alert string
					kOKString,	// OK button
					kNullString,	// No second button
					kNullString,	// No third button
					1,									// Set OK button to default
					CAlert::eWarningIcon					// Warning icon.
					);			
				}

			}
			else
			{
				CAlert::ModalAlert
				(
				"No se pudo crear el txt",			// Alert string
				kOKString,	// OK button
				kNullString,	// No second button
				kNullString,	// No third button
				1,									// Set OK button to default
				CAlert::eWarningIcon					// Warning icon.
				);			
			}
		}
	}while(false);
}
*/

UIDRef N2PsqlWidgetObserver::BucaTextFrameUpdate()
{	
	UIDRef Ref;
	WideString buf; 
	PMString Cadena="";
	do
	{
		//obtengo la interfaz  ILayoutControlData de la capa que se encuentra en frente o activa
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			return(Ref);
			break;
		}

		//obtengo la interfaz de jerarquia de la capa
		InterfacePtr<IHierarchy> Hierarchy(layout->QueryActiveLayer());
		if (Hierarchy == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			return(Ref);
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(Hierarchy));

		//obtengo la interfaz de IDataBase apartir de la referencia de la capa
		IDataBase* db = layerRef.GetDataBase();
		
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				return(Ref);
				break;
			}

			//obtengo el numero de hijos de la capa
			int32 Cont=Hierarchy->GetChildCount();
			
			//obtengo el UID del ultimo elemento creado en esta capa
			//el ultimo elemento que se creo debio ser el textUpdate
			UID RefUPdate=Hierarchy->GetChildUID(Cont-1);
			

			if(RefUPdate==kInvalidUID)
			{
				return(Ref);
				break;
			}

			//obtengo el UIDRef de el ultimo elelmento que se creo
			 Ref=UIDRef(db,RefUPdate);

			//pregunto si es un text frame
			if(Utils<IPageItemTypeUtils>()->IsTextFrame(Ref) == kTrue)
			{
				return(Ref);
			}
		}while (false); // only do once
		return(Ref);
}

void N2PsqlWidgetObserver::ActivarCapa(PMString NomCapa)
{

	ErrorCode error = kCancel;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			CAlert::WarningAlert("Error al intentar mostrar documento");
			break;
		}

		const UIDRef docUIDRef = ::GetUIDRef(document);
						
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}

		UID Capa= layerList->FindByName(NomCapa);
		if(Capa==nil)
		{
			break;
		}
		
		int32 NumLayerAActivar=layerList->GetLayerIndex(Capa);
		
		IDocumentLayer* documentLayer = layerList->QueryLayer(NumLayerAActivar);
		
		// Now we've created the new layer, we must set it as the active layer:
		InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
		
		InterfacePtr<ILayoutCmdData> layoutCmdData(setActiveLayerCmd, UseDefaultIID());
		
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		
		if (setActiveLayerCmd == nil || layoutCmdData == nil || layoutControlData == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd, layoutCmdData, or layoutControlData invalid");
			break;
		}

		setActiveLayerCmd->SetItemList(UIDList(documentLayer));
		
		layoutCmdData->Set(docUIDRef, layoutControlData);
		// Process the command
		error = CmdUtils::ProcessCommand(setActiveLayerCmd);

		if (error != kSuccess)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd failed");
		}
	}while(false);
}


void N2PsqlWidgetObserver::BloquearCapa(PMString NombreCapa,bool16 Bloqueo)
{
	ErrorCode success = kFailure;
	UID LayerUID=nil; //ID de la capa deseada
	UIDRef LayerUIDR;//Referencia a la capa deseada
	// We'll use a do-while(false) to break out on bad pointers:
	do
	{
		
		//obtiene un interfaz IDocument del documento de enfrente
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//obtiene un interfaz IDataBase del documento de enfrente
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		//obtiene un interfaz ILayerList del documento de enfrente
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//Busqueda de la capa por su nombre
		LayerUID=layerList->FindByName(NombreCapa);

		//obtencion del UIDRef de la capa por medio de la base de de datos del documento
		LayerUIDR=UIDRef(db,LayerUID);
		
	
		// Lock or unlock the layer: Creacion del comando a partir del kLockLayerCmdBoss
		InterfacePtr<ICommand>	lockLayerCmd(CmdUtils::CreateCommand(kLockLayerCmdBoss));
		if (lockLayerCmd == nil)
		{
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: lockLayerCmd is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}

		//pone la la referencia de la capa a ocultar
		lockLayerCmd->SetItemList(UIDList(LayerUIDR));
		
		//obtencion de la interfaz IBoolData a partr del comando lockLayerCmd 
		InterfacePtr<IBoolData> lockLayerCmdData(lockLayerCmd, UseDefaultIID());
		if (lockLayerCmdData == nil)
		{			
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: lockLayerCmdData is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}
		
		//bloque o desbloque la capa dependiendo la variable booleana bloqueo
		lockLayerCmdData->Set(Bloqueo);
		
		//ejecuta comando
		success = CmdUtils::ProcessCommand(lockLayerCmd);
		if (success != kSuccess)
		{
			//CAlert::WarningAlert("Error");
		}
		
	} while (false); // Only do once.
}


/**
	Funcion para mostra o ocultar una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaVisible,	Variable entero que indicara un valor p·ra determinar el estado de muestra de la capa a crear; 0 para no visible, 1 para visible
*/		
void N2PsqlWidgetObserver::MostrarCapa(PMString NombreCapa,bool16 Mostrar)
{
	ErrorCode success = kFailure;
		UID LayerUID=nil;
	// We'll use a do-while(false) to break out on bad pointers:
	do
	{
	
			//obtiene un interfaz IDocument del documento de enfrente
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//obtiene un interfaz IDataBase del documento de enfrente
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		//obtiene un interfaz ILayerList del documento de enfrente
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//Busqueda de la capa por su nombre
		LayerUID=layerList->FindByName(NombreCapa);

		//obtencion del UIDRef de la capa por medio de la base de de datos del documento
		UIDRef parent=UIDRef(db,LayerUID);
		// Lock or unlock the layer: creacion de un comando a partir del jefe kShowLayerCmdBoss
		InterfacePtr<ICommand>	showLayerCmd(CmdUtils::CreateCommand(kShowLayerCmdBoss));
		if (showLayerCmd == nil)
		{
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: showLayerCmd is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}

		//pone la la referencia de la capa a ocultar
		showLayerCmd->SetItemList(UIDList(parent));
		
		InterfacePtr<IBoolData> showLayerCmdData(showLayerCmd, UseDefaultIID());
		if (showLayerCmdData == nil)
		{			
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: showLayerCmdData is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}
		//popne la accion que se debe realizar si ocultarla o mostrar dependiendo la variable booleana Mostrar
		showLayerCmdData->Set(Mostrar);
		
		//ejecuta comando
		success = CmdUtils::ProcessCommand(showLayerCmd);//
		if (success != kSuccess)
		{
			//CAlert::WarningAlert("Error");
		}
		
	} while (false); // Only do once.
}


/**
	Llena el combo por medio de las preferencias actuales
*/
void N2PsqlWidgetObserver::LlenarComboPreferenciasOnPanel()
{

	
	int32 posicion=-1;
	PMString NomPref="";
	do
	{
	
		InterfacePtr<IPanelControlData>	panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID));	
		// Don't assert, fail silently, the tree view panel may be closed.
		if(panelData == nil)
		{
			CAlert::ErrorAlert("LlenarComboPreferenciasOnPanel panelData");
			break;
		}
		
		IControlView* ComboCView = panelData->FindWidget(kN2PsqlDataBaseComboWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::ErrorAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		

		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener dropListData*");
			CAlert::ErrorAlert("LlenarComboPreferenciasOnPanel dropListData");
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
				CAlert::ErrorAlert("LlenarComboPreferenciasOnPanel IDDLDrComboBoxSelecPrefer");
				break;
			}
		
		int32 IndexSelected=0;
		if(dropListData->Length()>0)
		{
			//PMString mns="";
			
			IndexSelected=IDDLDrComboBoxSelecPrefer->GetSelected();
			
			//mns.AppendNumber(IndexSelected);	
			
			if(IndexSelected>=0)
			{
				
				
				PMString StringSelected=dropListData->GetString(IndexSelected);
				
				
				dropListData->Clear();//lIMPIA LA LISTA ACTUAL
				IndexSelected=-1;
				//Llena la lista
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    	PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   		#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   		#endif
  			
  				if(PathPMS.IsEmpty() == kTrue)
				{
					break;
				}
				SDKFileHelper rootFileHelper(PathPMS);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				PlatformFileSystemIterator iter;
				/*if(!iter.IsDirectory(rootSysFile))
				{
					CAlert::InformationAlert("SAlio");
					break;
				}*/
			

				
				iter.SetStartingPath(rootSysFile);
				
				PMString TypeFile="";
				
				#if WINDOWS
					TypeFile="\\*.pfa";
				#else
					TypeFile="*.pfa";
				#endif
				
				
				IDFile sysFile;
				bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
				while(hasNext)
				{
					SDKFileHelper fileHelper(sysFile);
					PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
					
					if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
					{
						truncP=N2PSQLUtilities::TruncateExtencion(truncP);
						truncP.SetTranslatable(kFalse);
						dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
					}
					hasNext= iter.FindNextFile(sysFile);
				}
			
				
			
				if(dropListData->Length()>0)
				{
					posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
					posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
					posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				////////////////////////////
				////////////////////////////
				
				if(dropListData->Length()>0)//si se lleno con almenos un path el combo
				{
					
					//Seleccion si existe del ultimo path seleccionado
					if(dropListData->Length()>0)//si hay elementos en la lista
					{
						//IndexSelected=dropListData->GetIndex(StringSelected);
						//si exite en la lista el anterior path
						if(IndexSelected>=0)
						{	
							//mns="";
							//mns.AppendNumber(IndexSelected);	
							
							IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
						}
						else
						{	//si existe alguno que contenga la leyenda default
							//mns="alguno que contenga la leyenda default?";
							
							IndexSelected=dropListData->GetIndexOfPartialString("Default",kFalse);
							if(IndexSelected>=0)
							{	
								//mns="Si existe alguno que contenga la leyenda default";
								
								//mns="";
								//mns.AppendNumber(IndexSelected);	
								
								IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
							}
							else
							{//si no selecciona el primer elemento de la lista
								//mns="Primer elemeneto de la lista";
								
								IDDLDrComboBoxSelecPrefer->Select(0);
							}
						}
					}
				}
			}
			else
			{
				//si no existe alguno seleccionado se limpia la lista se vuelve a llenar busca el que contenga el termino default
				IndexSelected=-1;
				dropListData->Clear();//lIMPIA LA LISTA ACTUAL
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    	PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   		#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   		#endif
  			
  				if(PathPMS.IsEmpty() == kTrue)
				{
					break;
				}
				SDKFileHelper rootFileHelper(PathPMS);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				PlatformFileSystemIterator iter;
				/*if(!iter.IsDirectory(rootSysFile))
				{
					
					break;
				}*/
			

				
				iter.SetStartingPath(rootSysFile);
				PMString TypeFile="";
				
				#if WINDOWS
					TypeFile="\\*.pfa";
				#else
					TypeFile="*.pfa";
				#endif
				
				IDFile sysFile;
				bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
				while(hasNext)
				{
					SDKFileHelper fileHelper(sysFile);
					PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
					
					if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
					{
						truncP=N2PSQLUtilities::TruncateExtencion(truncP);
						truncP.SetTranslatable(kFalse);
						dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
					}
					hasNext= iter.FindNextFile(sysFile);
				}
			
				
			
				if(dropListData->Length()>0)
				{
					posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
					posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
					posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				////////////////////////////
				////////////////////////////
				
				if(dropListData->Length()>0)//si se lleno con almenos un path el combo
				{
					if(IndexSelected>=0)
						IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
					else
						IDDLDrComboBoxSelecPrefer->Select(0);
				}
			}
		}
		else
		{
		
			//no existen Path en combo llenado del combo
			IndexSelected=-1;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    	PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   		#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   		#endif
  			
  				if(PathPMS.IsEmpty() == kTrue)
				{
					break;
				}
				SDKFileHelper rootFileHelper(PathPMS);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				PlatformFileSystemIterator iter;
				/*if(!iter.IsDirectory(rootSysFile))
				{
				
					break;
				}*/
			

				
				iter.SetStartingPath(rootSysFile);
				
				PMString TypeFile="";
				
				#if WINDOWS
					TypeFile="\\*.pfa";
				#else
					TypeFile="*.pfa";
				#endif
				
				IDFile sysFile;
				bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
				while(hasNext)
				{
					SDKFileHelper fileHelper(sysFile);
					PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
					
					if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
					{
						truncP=N2PSQLUtilities::TruncateExtencion(truncP);
						truncP.SetTranslatable(kFalse);
						dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
					}
					hasNext= iter.FindNextFile(sysFile);
				}
			
				
			
				if(dropListData->Length()>0)
				{
					posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
					posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
					posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				////////////////////////////
				////////////////////////////
			
			
			//si existe alguno con la leyenda default
			if(dropListData->Length()>0)//si se lleno con almenos un path el combo
			{
				if(IndexSelected>=0)
					IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
				else
					IDDLDrComboBoxSelecPrefer->Select(0);
			}
		}
	}while(false);
}





int32 N2PsqlWidgetObserver::AplicaStyle(PMString NomStyle)
{
	int32 UIDTexModelOfMyFrame=0;
	
	do
	{

		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		
		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			break;
		}

	
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			break;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
		if(pTextTarget==nil)
		{
			break;
		}

		InterfacePtr<ITextFocus> pFocus(pTextTarget->QueryTextFocus());
		if(pFocus==nil)
		{
			break;
		}
	
	
		InterfacePtr<ITextModel> iTextModel(pFocus->QueryModel());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			CAlert::ErrorAlert("Error al intentar obtener el Modelo de Texto.");
			break;
		}
		
	
		
		
		
	
		InterfacePtr<ITextModelCmds> iTextModelCmd(iTextModel,UseDefaultIID());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}
		

		
	
		//aplicar Estilo
		InterfacePtr<IWorkspace> theWS(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}

	
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);
		if(ParaStyleNameTable==nil)
		{
			break;
		}
	
		UID myStyleUID=ParaStyleNameTable->FindByName(NomStyle);
		if(myStyleUID==nil)
		{
			break;
		}

		InterfacePtr<ICommand> applyCmd(iTextModelCmd->ApplyStyleCmd(0,iTextModel->TotalLength(), myStyleUID,kParaAttrStrandBoss,kFalse/*replaceOverrides*/));
		if(applyCmd==nil)
		{
			break;
		}

		if(CmdUtils::ProcessCommand(applyCmd)!=kSuccess)
		{
			break;
		}
	}while(false);
	return(UIDTexModelOfMyFrame);
}


