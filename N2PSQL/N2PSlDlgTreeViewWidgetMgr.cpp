/*
 *  N2PSlDlgTreeViewWidgetMgr.cpp
 *  N2PSQL
 *
 *  Created by Fernando  Llanas on 12/09/10.
 *  Copyright 2010 Interlasa. All rights reserved.
 *
 */

//========================================================================================
//  
//  $File: //depot/indesign_6.0/gm/source/sdksamples/basicselectabledialog/BscSlDlgTreeViewWidgetMgr.cpp $
//  
//  Owner: Vikram Sethi
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2008/08/18 16:29:43 $
//  
//  $Revision: #1 $
//  
//  $Change: 643585 $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "CTreeViewWidgetMgr.h"

#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "ISelectableDialogSwitcher.h"

#include "ListIndexNodeID.h"
#include "N2PsqlID.h"

/** Implements tree view widget manager. 
 
 @ingroup basicselectabledialog
 
 */

class BscSlDlgTreeViewWidgetMgr : public CTreeViewWidgetMgr
	{
	public:
		BscSlDlgTreeViewWidgetMgr(IPMUnknown *boss);
		
		virtual bool16	ApplyDataToWidget( const NodeID& node, IPanelControlData* widgetList, int32 message ) const;
		virtual void	RegisterStyles();
	};

CREATE_PMINTERFACE(BscSlDlgTreeViewWidgetMgr, kBscSlDlgTreeViewWidgetMgrImpl)

BscSlDlgTreeViewWidgetMgr::BscSlDlgTreeViewWidgetMgr(IPMUnknown *boss) :
CTreeViewWidgetMgr(boss, kList)
{
}

bool16 BscSlDlgTreeViewWidgetMgr::ApplyDataToWidget( const NodeID& node, IPanelControlData* widgetList, int32 message ) const
{
	TreeNodePtr<ListIndexNodeID>	myNode(node);
	ASSERT(myNode != nil);
	
	if (myNode)
	{
		int32 nodeIndex = myNode->GetIndex();
		InterfacePtr<IWidgetParent> iWidgetParent(this, IID_IWIDGETPARENT);
		InterfacePtr<ISelectableDialogSwitcher> iSelectableDialog((ISelectableDialogSwitcher*)iWidgetParent->QueryParentFor(IID_ISELECTABLEDIALOGSWITCHER));
		WidgetID nthPanelWidgetID = iSelectableDialog->GetPanelWidgetID(nodeIndex);
		if (nthPanelWidgetID != kInvalidWidgetID)
		{
			// Get panel name.
			IControlView* nthPanelView = iSelectableDialog->GetDialogPanel(nthPanelWidgetID);
			InterfacePtr<ITextControlData> iPanelName(nthPanelView, IID_ITEXTCONTROLDATA);
			PMString panelName = iPanelName->GetString();
			panelName.Translate();
			SetNodeName( widgetList, panelName, kBscSlDlgTreeNodeNameWidgetID );
		}
 	}
	
	return kTrue;
}

void BscSlDlgTreeViewWidgetMgr::RegisterStyles()
{
	RegisterStyleWidget(kBscSlDlgTreeNodeTreeStyle, kBscSlDlgTreeNodeRsrcID, kN2PsqlPluginID);
}

