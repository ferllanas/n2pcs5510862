/*
 *  N2PSQLRollOverIconPieFotoButtonObserver.h
 *  News2Page
 *
 *  Created by Fernando  Llanas on 26/05/09.
 *  Copyright 2009 Interlasa. All rights reserved.
 *
 */

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"
#include "ITextControlData.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"

#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "ILayoutUIUtils.h"
#include "ILayoutControlData.h"
#include "ISelectionManager.h"
#include "IConcreteSelection.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "ITextModel.h"
#include "IFrameList.h"
#include "ITextModelCmds.h"

#include "TextEditorID.h".h

#include "ILayoutUIUtils.h"
#include "ISelectionUtils.h"


// Interface includes
#include "ISubject.h"
#include "IPageList.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// Implem includes

#include "CAlert.h"
#include "CObserver.h"
#include "N2PsqlID.h"

#include "UpdateStorysAndDBUtilis.h"
#include "N2PSQLUtilities.h"

#include "IN2PSQLUtils.h"
#include "N2PRegisterUsers.h"
#include "IRegisterUsersUtils.h"
#include "N2PsqlLogInLogOutUtilities.h"

/**
 Adding an IObserver interface to a widget boss class creates a button
 whose presses can be registered as Update messages to the client code. 
 
 @ingroup pictureicon
 
 */
class N2PSQLRollOverIconPieFotoButtonObserver : public CObserver
	{
	public:
		/**
		 Constructor 
		 */		
		N2PSQLRollOverIconPieFotoButtonObserver(IPMUnknown *boss);
		/**
		 Destructor
		 */
		~N2PSQLRollOverIconPieFotoButtonObserver();
		/**
		 AutoAttach is only called for registered observers
		 of widgets.  This method is called when the 
		 widget boss object is shown.
		 */		
		virtual void AutoAttach();
		
		/**
		 AutoDetach is only called for registered observers
		 of widgets. Called when the widget is hidden.
		 */		
		virtual void AutoDetach();
		
		/**
		 Update is called for all registered observers, and is
		 the method through which changes are broadcast. 
		 @param theChange this is specified by the agent of change; it can be the class ID of the agent,
		 or it may be some specialised message ID.
		 @param theSubject this provides a reference to the object which has changed; in this case, the button
		 widget boss object that is being observed.
		 @param protocol the protocol along which the change occurred.
		 @param changedBy this can be used to provide additional information about the change or a reference
		 to the boss object that caused the change.
		 */
		virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
	
	private:
		int32 PlaceTextAndAplicaStyle(PMString NomStyle, PMString Texto);
		
		void colocarpieDeFoto(PMString texto,PMString ID_Elemento,PMString ID_Elem_Padre,PMString ID_Elem_Padre_Nota,PMString StringRefTextModelOfTagegedText);

		
	};

CREATE_PMINTERFACE(N2PSQLRollOverIconPieFotoButtonObserver, kN2PSQLRollOverIconPieFotoButtonObserverImpl)


N2PSQLRollOverIconPieFotoButtonObserver::N2PSQLRollOverIconPieFotoButtonObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


N2PSQLRollOverIconPieFotoButtonObserver::~N2PSQLRollOverIconPieFotoButtonObserver()
{
	
}


void N2PSQLRollOverIconPieFotoButtonObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}


void N2PSQLRollOverIconPieFotoButtonObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}



void N2PSQLRollOverIconPieFotoButtonObserver::Update
(
 const ClassID& theChange, 
 ISubject* theSubject, 
 const PMIID &protocol, 
 void* changedBy
)
{
	if(theChange == kTrueStateMessage) 
	{
		// No behaviour in this sample: but indicate at least that the message has been received.
		// Dialog-specific resource includes:
		do
		{
			
			//CAlert::InformationAlert("jajaja");
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				break;
			}
			
			
			IControlView *iControlView = panel->FindWidget(kN2PsqlTextPieWidgetID);
			InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
			if (TextControl == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			PMString texto=TextControl->GetString();
			
			///
			IControlView *iControlViewIdElePhoto = panel->FindWidget(kN2PsqlIdElePhotoLabelWidgetID);
			InterfacePtr<ITextControlData> TextControlIdElePhoto(iControlViewIdElePhoto,ITextControlData::kDefaultIID);
			if (TextControlIdElePhoto == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			PMString ID_Elem_Padre=TextControlIdElePhoto->GetString();
			
			////
			IControlView *iControlViewIdEleNota = panel->FindWidget(kN2PsqlIdPadreElePhotoLabelWidgetID);
			InterfacePtr<ITextControlData> TextControlIdEleNota(iControlViewIdEleNota,ITextControlData::kDefaultIID);
			if (TextControlIdEleNota == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			PMString ID_Elem_Padre_Nota=TextControlIdEleNota->GetString();
			////
			IControlView *iControlViewUIDTextModelPie = panel->FindWidget(kN2PsqlUIDRefTexModelDePieFoto_WidgetID);
			InterfacePtr<ITextControlData> TextControlUIDTextModelPie(iControlViewUIDTextModelPie,ITextControlData::kDefaultIID);
			if (TextControlUIDTextModelPie == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			PMString StringRefTextModelOfTagegedText=TextControlUIDTextModelPie->GetString();
			
			////
			IControlView *iControlViewIDElemPie = panel->FindWidget(kN2PsqlText_ID_Elem_Pie_WidgetID);
			InterfacePtr<ITextControlData> TextControlId_Eleme_Pie(iControlViewIDElemPie,ITextControlData::kDefaultIID);
			if (TextControlId_Eleme_Pie == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			PMString ID_Elemento=TextControlId_Eleme_Pie->GetString();
			
			this->colocarpieDeFoto( texto, ID_Elemento, ID_Elem_Padre, ID_Elem_Padre_Nota, StringRefTextModelOfTagegedText);
		
		} while (false);	
	}	
}


void N2PSQLRollOverIconPieFotoButtonObserver::colocarpieDeFoto(PMString texto,PMString ID_Elemento,PMString ID_Elem_Padre,PMString ID_Elem_Padre_Nota,PMString StringRefTextModelOfTagegedText)
{
	do
	{
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
				
			PMString ID_Usuario="";
			bool16 SetLockStory=kTrue;
			
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				ASSERT_FAIL("document pointer nil");
				break;
			}
			
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layout == nil)
			{
				ASSERT_FAIL("ILayoutControlData pointer nil");
				break;
			}
			
			//Para obtener el numero de la pagina en que nos encontramos
			int32 numeroPaginaActual=0;
			UID pageUID = layout->GetPage(); 
			
			InterfacePtr<IPageList> PageList(document,IID_IPAGELIST); 
			if(PageList!=nil)
			{
				
				numeroPaginaActual=PageList->GetPageIndex(pageUID)+1;
			}
			
			
			PreferencesConnection PrefConections;
			PMString StringConection="";
			
			
			
			
			
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																				(
																				 kN2PSQLUtilsBoss,	// Object boss/class
																				 IN2PSQLUtils::kDefaultIID
																				 )));
			if(SQLInterface==nil)
			{
				continue;
			} 	
			
			K2Vector<PMString> QueryContenido;
			K2Vector<PMString> QueryVector;
			PMString Busqueda="";
			///////
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			
			if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				continue;
			}
			///////////////////////
			//Item a buscar
			PMString ItemToSearchString="PieFoto";
			
			//Obtiene texto del widget que se va a poner sobre el Textframe
			//texto= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextPieWidgetID);
			//ID_Elem_Padre= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
			
			
			if(!UpdateStorys->ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(ID_Elem_Padre_Nota,kTrue))
			{
				CAlert::InformationAlert(kN2PSQLDebeDepositarNotaPadreStringKey);
				break ;
			}
			
			//Obtiene le ID de la nota que se encuentra actualmente en vista
			//PMString StringRefTextModelOfTagegedText="";
			//StringRefTextModelOfTagegedText = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlUIDRefTexModelDePieFoto_WidgetID); //UID de la caja de texto del Pie de fot en pagina 
			UID uidItemplace=StringRefTextModelOfTagegedText.GetAsNumber();
			UIDRef itemToPlace(db, uidItemplace);
			
			int32 UIDModel=N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
			if(UIDModel==0)
				break;
			UID uidMod=UIDModel;
			UIDRef graphicFrame(db, uidMod);
			
			PMString UIDModelString ="";
			UIDModelString.AppendNumber(UIDModel);
			
			//Obtiene estatus de nota
			PMString EstadoNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextStatusWidgetID);
			PMString FYHUCheckinNota= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlLastCheckInNoteTextEditWidgetID); 
			//PMString ID_Elemento =	N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_Pie_WidgetID);//Obtiene el id del elemento del pie de foto
			
			///////////////////////////////////
			//Para saber si se puede fluir la nota
			PMString IDSeccionDeElemPadre="";
			PMString Consulta="";
			
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);

			
			
			Consulta=" SELECT Id_Seccion,(SELECT Dirigido_a FROM Elemento WHERE id_Elemento=(SELECT id_Elemento_padre FROM Elemento WHERE id_Elemento=" +ID_Elem_Padre+")) as Dirigido_a FROM Elemento WHERE Id_Elemento=" +ID_Elem_Padre ;
			
			
			
			
			
			
			
			PMString DSNName="Default";
			PMString Dirigido_a="";
			//solo si la nota proviene del servidor de Preferencia
			//if(PrefConectionsOfServerRemote.NameConnection == DSNName)
			{
				if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
				{
					break;
				}
				
				if(QueryVector.Length()>0)
				{
					IDSeccionDeElemPadre = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
					Dirigido_a=SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[0]);
				}
				else
					break;
				
				/*QueryVector.clear();
				Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "' , "+IDSeccionDeElemPadre+" , 24, @X)" ;
				if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
				{
					break;
				}
				else
				{
					PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
					if(RESULT=="0")
					{
						CAlert::InformationAlert(kN2PSQLUserCantLigarNotaAPagStringKey);
						break;
					}
					
				}*/
			}
			///////////////////////////////////
			
			if(Dirigido_a.NumUTF16TextChars()>0)
				EstadoNota=Dirigido_a+"/"+EstadoNota;
			bool16 FueReemplazado=N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_Elem_Padre,ID_Elemento,UIDModelString,ItemToSearchString,DSNName,EstadoNota,SetLockStory,FYHUCheckinNota, kFalse );
			
			//si no se desea reemplazar 
			if(FueReemplazado==kFalse)
				break;
			//Coloca Texto y aplica estilo a TextFlame y regresa el UID del TextModel
			this->PlaceTextAndAplicaStyle(ItemToSearchString,texto);
			
			//bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
			//if(!sa)
			//	break;
			
			//this->AplicaStyle(ItemToSearchString);
			
			//this->ApplicaColumnBreakSiNecesita();
			PMString string="";					
			PMString FecvhaSalida="0";
			UpdateStorys->EstableceParametrosToCheckInElementoNota(ID_Elem_Padre,
																   ID_Elemento,
																   UIDModelString.GetAsNumber(), 
																   FecvhaSalida, 
																   "0",
																   "0",
																   ID_Usuario,
																   "0",
																   "0",
																   kFalse,
																   document,
																   string,kFalse,"");
			N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(ID_Elem_Padre,FecvhaSalida, document);
			
			//Coloca el Lock state del stori como bloquedo
			if(FueReemplazado)
			{
				//Coloca el Lock state del stori como bloquedo
				UpdateStorys->CambiaModoDeEscrituraDElementoNota(UIDModelString,kTrue);
			}
		}
		else
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
		}			
		
	}while(false);
}



int32 N2PSQLRollOverIconPieFotoButtonObserver::PlaceTextAndAplicaStyle(PMString NomStyle, PMString Texto)
{
	int32 UIDTexModelOfMyFrame=0;
	
	do
	{
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		WideString * inserthis= new WideString(Texto);
		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			break;
		}
		
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			break;
		}
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
		if(pTextTarget==nil)
		{
			break;
		}
		
		InterfacePtr<ITextFocus> pFocus(pTextTarget->QueryTextFocus());
		if(pFocus==nil)
		{
			break;
		}
		
		
		InterfacePtr<ITextModel> iTextModel(pFocus->QueryModel());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			CAlert::ErrorAlert("Error al intentar obtener el Modelo de Texto.");
			break;
		}
		int32 length = iTextModel->TotalLength()-1;
		
		
		/////////////////////
		//Obtiene el ID del TextModel
		PMString f="";
		IFrameList* Listffame=iTextModel->QueryFrameList();
		UID UIDTextModel=Listffame->GetTextModelUID();
		UIDTexModelOfMyFrame=UIDTextModel.Get();
		
		////////////////////
		if(iTextModel->Insert( 0, inserthis)!=kSuccess)
		{
			break;
		}
		
		TextIndex start = inserthis->NumUTF16TextChars();
		
		//PMString FF="";
		//FF.AppendNumber(start);
		//FF.Append(",");
		//FF.AppendNumber(length);
		//CAlert::InformationAlert(FF);
		N2PSQLUtilities::DeleteText(iTextModel,start,length);
		
		InterfacePtr<ITextModelCmds> iTextModelCmd(iTextModel,UseDefaultIID());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}
		
		
		///insertar texto
		const bool16 copyString=kFalse;
		
		
	
	}while(false);
	return(UIDTexModelOfMyFrame);
}
