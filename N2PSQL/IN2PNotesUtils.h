//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/browsenotes/IN2PNotesUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:08:25 $
//  
//  $Revision: #1 $
//  
//  $Change: 323503 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IBNotesUtils_h__
#define __IBNotesUtils_h__

// Interface includes:
#include "IPMUnknown.h"
#include "IDocument.h"
#include "IControlView.h"

// Project includes:
#include "N2PNotesStorage.h"
#include "N2PsqlID.h"


/** From SDK sample; utility interface used to update information in the BNotes panel.

	
	@ingroup browsenotes
	@ingroup sdk_incopy
*/

class IN2PNotesUtils : public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_IN2PNOTESUTILS };
		enum { kByUserName = 1, kByDateTime = 2 } SortBy;


	/**
		Change state of entire listbox.
		@param listHelper Specifies reference to listbox helper.
		@param doc Specifies document being parsed for notes.
		@param delStoryList specifies list of stories to be excluded from listbox.
		@param noteAnchorPos specifies anchor index of note that has changed.
	
	virtual void UpdateListBox(
		SDKListBoxHelper& listHelper, 
		IDocument* doc,
		UIDList delStoryList, 
		TextIndex noteAnchorPos) = 0;
	*/		

	/**
		Ease process of manipulating a listbox. 
		@param widgetID Specifies desired widget.
		@return reference to ???
	*/
	virtual IControlView* GetAssociatedWidget(WidgetID widgetID) = 0;


	/**
		Find corresponding note in text. 
		@param iNote Specifies index of note within listbox.
	*/
	virtual void GoToSpecifiedNote(int32 iNote) = 0;

	/**
		Helper to set note sort criterion. 
		@param sortBy Specifies sort criterion.
		1 == sort by user name, 2 == sort by date/time.
	*/
	virtual void SetSortBy(int32 sortBy) = 0;

	/**
		Helper to get note sort criterion. 
		@return Returns sort criterion.
		1 == sort by user name, 2 == sort by date/time.
	*/
	virtual int32 GetSortBy() = 0;
	
	/*
	*/
	virtual void GetNotesOnElementArticle(K2Vector<N2PNotesStorage*>& fN2PNotesStore,const UIDRef& StoryElementOfArticle)=0;
	
	virtual ErrorCode AddNotesToElementArticle(ITextModel* mytextmodel, const  K2Vector<N2PNotesStorage*>& fBNotesStore)=0;
};

#endif

// End, IBNotesUtils.h

