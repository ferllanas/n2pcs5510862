//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/browsenotes/N2PNotesStorage.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:08:25 $
//  
//  $Revision: #1 $
//  
//  $Change: 323503 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:

// Project includes:
#include "N2PNotesStorage.h"


/* Constructor
*/
N2PNotesStorage::N2PNotesStorage() : 
	fauthor(""), 
	fcreated(""), 
	flastmodified(""), 
	ftext("TBD"), 
	findex(kInvalidTextIndex),
	fstoryref(UIDRef::gNull),
	fnoteUID(kInvalidUID)
{
}


/* Destructor
*/
N2PNotesStorage::~N2PNotesStorage()
{
}

// End, N2PNotesStorage.cpp.




