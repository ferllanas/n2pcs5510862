//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/basicdragdrop/N2PSQLDNDDropTarget.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:08:25 $
//  
//  $Revision: #1 $
//  
//  $Change: 323503 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPageItemScrapData.h"
#include "IPathGeometry.h"
#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IPageItemTypeUtils.h"
#include "IGraphicFrameData.h"
#include "IWidgetParent.h"
#include "IMultiColumnTextFrame.h"
#include "IHierarchy.h"
#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "IComposeScanner.h"
#include "ITextModel.h"
#include "IXMLReferenceData.h"
#include "XMLReference.h"
#include "IIDXMLElement.h"
#include "IXMLUtils.h"

#include "ILinkManager.h"
#include "ILinkResource.h"
#include "ILinkUtils.h"
#include "IDataLink.h"
#include "ILink.h"

#include "IWidgetUtils.h"
#include "ITreeViewMgr.h"
#include "IStringListData.h"


// General includes:
#include "CDragDropTarget.h"
#include "CAlert.h"
#include "PMFlavorTypes.h"
#include "DataObjectIterator.h"
#include "UIDList.h"
#include "IPathUtils.h"
#include "PMString.h"
#include "SDKLayoutHelper.h"
#include "FileUtils.h"
#include "LinksID.h"

// Project includes:
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"

#ifdef WINDOWS


	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"

#endif

#ifdef MACINTOSH


	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"
	

#endif


/** the flavour our target will handle */
#define dropTgtFlavor PMFlavor(kPageItemFlavor)

/** N2PSQLDNDDropTarget
	Provides the drop behaviour. Our drag and drop target accepts page items and reasons about their shape. 
	We override the DoDragWithin method to replace the custom cursor behaviour (open hand rather than copy).
	This drag and drop target is bound to a panel...
	
	@ingroup basicdragdrop
	
*/
class N2PSQLDNDDropTarget : public CDragDropTarget
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLDNDDropTarget(IPMUnknown* boss);
		/**
			when we enter the panel, we change the cursor to be an open hand. If we had some dynamic cursor behaviour
			(the cursor changing dependent on where the mouse is within the panel), we would provide this behaviour in
			the DoDragWithin method.
		*/
		void DoDragEnter();
		/**
			this method defines the target response to the drag. It is called when the mouse enters the panel. We
			inidcate we can accept drags with a kPageItem flavor, that we do not want the default cursor and we do
			want the default panel highlight behaviour.

			@param dataIter IN iterator providing access to the data objects within the drag.
			@param fromSource IN the source of the drag.
			@param controller IN the drag drop controller mediating the drag.
			@return a target response (either won't accept or drop will copy).

			@see DragDrop::TargetResponse
		*/
		DragDrop::TargetResponse CouldAcceptTypes(DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const;
		/**
			When the drop is performed, this method is called. We get the data item from the scrap and test its shape.
			We then change the static text widget associated with the panel to reflect the shape of this widget.
			@param controller IN the drag drop controller mediating the drag.
			@param type IN drag and drop command type
			@see DragDrop::eCommandType
		*/
		ErrorCode	ProcessDragDropCommand(IDragDropController* controller, DragDrop::eCommandType type);
		
	private:
		/**
			Determine the type of page item.
			@param pageItemUIDRef IN reference to page item to be typed.
			@param pageItemType OUT a string that describes the type of item e.g. "Group", GraphicFrame".
		*/
		void GetPageItemType(const UIDRef& pageItemUIDRef, PMString& pageItemType) const;
		/**
		Determine the shape of a given path page item.
		@param pageItemUIDRef IN reference to page item looked at.
		@param shape OUT a string that describes the shape e.g. "Line", "Circle", etc.
		@return kTrue if the item is a spline, kFalse otherwise.
		*/
		bool16 IsSplineShape(const UIDRef& itemUIDRef, PMString& shape) const;

		/**
			@return kTrue if the given item is a group, kFalse otherwise.
		*/
		bool16 IsGroup(const UIDRef& pageItemUIDRef) const;

		/**
			@return kTrue if the given item is an empty graphic frame, kFalse otherwise.
		*/
		bool16 IsEmptyGraphicFrame(const UIDRef& pageItemUIDRef) const;
		
		/**
			@return kTrue if the given item is an empty spline, kFalse otherwise.
		*/
		bool16 IsSpline(const UIDRef& pageItemUIDRef) const;


};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLDNDDropTarget, kN2PSQLDNDDropTargetImpl)

/* N2PSQLDNDDropTarget Constructor
*/
N2PSQLDNDDropTarget::N2PSQLDNDDropTarget(IPMUnknown* boss)
: CDragDropTarget(boss)
{
}

/* DoDragEnter. The drag enters the panel.
*/
void N2PSQLDNDDropTarget::DoDragEnter()
{
	InterfacePtr<IDragDropController> ddController(GetExecutionContextSession(), IID_IDRAGDROPCONTROLLER);
	ddController->SetTrackingCursorFeedback(CursorSpec(kCrsrOpenHand));		
}

DragDrop::TargetResponse 
N2PSQLDNDDropTarget::CouldAcceptTypes(DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController*) const
{
	DataExchangeResponse response;
	
	
	response = dataIter->FlavorExistsWithPriorityInAllObjects(dropTgtFlavor);
			if(response.CanDo())
			{
 				return DragDrop::TargetResponse( response, DragDrop::kDropWillCopy,
					 DragDrop::kUseDefaultTrackingFeedback,
					 DragDrop::kTargetWillProvideCursorFeedback);

			}
	return DragDrop::kWontAcceptTargetResponse;
}

/*
	the drop has occured, we need to handle it.
*/
ErrorCode	
N2PSQLDNDDropTarget::ProcessDragDropCommand(IDragDropController* controller,DragDrop::eCommandType type)
{
	bool16 returnCode = kFailure;
	
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		/////
		if(!N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlBotonSubirNewNoteWidgetID))
		{
			break;
		}
		//Si existen usuarios Logeados
		if(!N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			break;
		}
		
		PMString N2PSQLFolioPag = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		
		//Si es una pagina de N2PSQL
		if(N2PSQLFolioPag.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert(kN2PSQLYouCouldSaveThisPageString);
			break;
		}
		
		
	
		// we should ensure the drag has been internalized, if we are coming from a 
		// custom source, the handler may not be initialised
		if (controller->InternalizeDrag(dropTgtFlavor, dropTgtFlavor) != kSuccess)	
		{
			CAlert::InformationAlert("Can't internalize drag?");
			ASSERT_FAIL("Can't internalize drag?");
			break;
		}

		// get the data exchnage handler for this object...
		InterfacePtr<IDataExchangeHandler> handler(controller->QueryTargetHandler());
		if (handler == nil)
		{
			CAlert::InformationAlert("Data exchange handler is nil?");
			ASSERT_FAIL("Data exchange handler is nil?");
			break;
		}
		
		// the item that is being dragged is on the scrap
		InterfacePtr<IPageItemScrapData> data(handler,UseDefaultIID());
		if (data == nil)
		{
			CAlert::InformationAlert("Page item scrap data is nil?");
			ASSERT_FAIL("Page item scrap data is nil?");
			break;
		}
		
		// we capture the shape as a string
		PMString shape;
		
		// get the list of objects captured by the drag
		UIDList*	theUIDs = data->CreateUIDList();
		// we are only interested in the first object dragged.
		UIDRef primaryObject = theUIDs->GetRef(0);

		this->GetPageItemType(primaryObject,shape);
		
		//si no es textFrame sale de la funcion
		if(shape!="TextFrame")
		{
			break;
		}
		
		//Para obtener el UIDModel del textFrame
		 SDKLayoutHelper layoutHelper;
         InterfacePtr<IGraphicFrameData> graphicFrameData(primaryObject, UseDefaultIID());
         ASSERT(graphicFrameData);
         if (!graphicFrameData) 
         {
         	CAlert::InformationAlert("textFrame control data interface for widget?");
            break;
         }
         UIDRef storyUIDRef = layoutHelper.GetTextModelRef(graphicFrameData);


        UID textModelUID = storyUIDRef.GetUID(); //UIDTextModel
        int32 UIDint32Textmodel=textModelUID.Get();//Pasa a entero el UID del TextModel
        
		bool16 esnota=kFalse;
        PMString IDNota="";
		if(N2PSQLUtilities::GetIDNota_ConIDTextFrame_SobreXMPNotasFluidas(UIDint32Textmodel, IDNota,esnota))
		{
			//este es un elemento de una nota de N2PSQL
			CAlert::InformationAlert(kN2PSQLThisIsElemDNotaString);
			break;
		}
		
		 
		//
		InterfacePtr<IControlView>  icontrolView(this, UseDefaultIID());
		ASSERT(icontrolView);
		if(!icontrolView) 
		{
			CAlert::InformationAlert("icontrolView for text widget is nil?");
			break;
		}
		
		
		WidgetID thisID = icontrolView->GetWidgetID();
		
		
		 
		PMString UIDTextModelString="";
		UIDTextModelString.AppendNumber(UIDint32Textmodel);
		
		
		
		switch(thisID.Get())
		{
			//Obtiene el WidgetID donde se va a colocar el texto
			// Y Guarda en XML el UID del TextModel del Correspondiente al elemento de cada nota
			
			
			case kN2PsqlStTextTituloTextWidgetID:
			{
				
				N2PSQLUtilities::SaveXMPVar("UIDTextModelTitulo", UIDTextModelString, document);
				
				thisID = kN2PsqlTextTituloWidgetID;
			
				break;
			}
			
			case kN2PsqlStTextBalazoTextWidgetID:
			{
				//kN2PsqlTextBalazoWidgetID
				N2PSQLUtilities::SaveXMPVar("UIDTextModelBalazo", UIDTextModelString,document);
				thisID = kN2PsqlTextBalazoWidgetID;
				break;
			}
			
			
			
			case kN2PsqlStTextNotaTextWidgetID:
			{
				//kN2PsqlTextNotaWidgetID
				N2PSQLUtilities::SaveXMPVar("UIDTextModelConteNota", UIDTextModelString, document);
				thisID = kN2PsqlTextNotaWidgetID;
				break;
			}
			
			case kN2PsqlStTextPieTextWidgetID:
			{
				//kN2PsqlTextPieWidgetID
				N2PSQLUtilities::SaveXMPVar("UIDTextModelPieFoto", UIDTextModelString, document);
				thisID = kN2PsqlTextPieWidgetID;
				break;
			}
			
		}
			
		
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("sasaasasassasasaassasa?");
			ASSERT_FAIL("C2PFilePreferencesUtils::ApplyPreferences myParent");
			break;
		}
			
		InterfacePtr<IPanelControlData>	thisWidget((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(thisWidget==nil)
		{
			CAlert::InformationAlert("sasasaas?");
			ASSERT_FAIL("C2PFilePreferencesUtils::ApplyPreferences panel");
			break;
		}
		
		IControlView*	textWid = thisWidget->FindWidget(thisID);
		if (textWid == nil)
		{
			CAlert::InformationAlert("Control view for text widget is nil?");
			ASSERT_FAIL("Control view for text widget is nil?");
			break;
		}
		InterfacePtr<ITextControlData> theText(textWid,UseDefaultIID());
		if (theText == nil)
		{
			CAlert::InformationAlert("No text control data interface for widget?");
			ASSERT_FAIL("No text control data interface for widget?");
			break;
		}

		
		
		shape = N2PSQLUtilities::GetPMStringOfTextFrame(UIDint32Textmodel);
		
		
		// we are not going to localise the possible values.
		shape.SetTranslatable(kFalse);

		theText->SetString(shape);
		delete theUIDs;
		returnCode = kSuccess;
	}while(false);
	return returnCode;
}

/* GetPageItemType
*/
void N2PSQLDNDDropTarget::GetPageItemType(const UIDRef& pageItemUIDRef, PMString& pageItemType) const
{
	do
	{
		if (Utils<IPageItemTypeUtils>()->IsGraphicFrame(pageItemUIDRef) == kTrue)
		{
			pageItemType = "GraphicFrame";
		}
		else if (this->IsEmptyGraphicFrame(pageItemUIDRef) == kTrue )
		{
			pageItemType = "EmptyGraphicFrame";
		}
		else if (Utils<IPageItemTypeUtils>()->IsTextFrame(pageItemUIDRef) == kTrue)
		{
			pageItemType = "TextFrame";
		}
		else if (this->IsGroup(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Group";
		}
		else if (Utils<IPageItemTypeUtils>()->IsInline(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Inline";
		}
		else if (Utils<IPageItemTypeUtils>()->IsGraphic(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Graphic";
		}
		else if (Utils<IPageItemTypeUtils>()->IsTextOnAPath(pageItemUIDRef) == kTrue)
		{
			pageItemType = "TextOnAPath";
		}
		else if (Utils<IPageItemTypeUtils>()->IsStandOff(pageItemUIDRef) == kTrue)
		{
			pageItemType = "StandOff";
		}
		else if (Utils<IPageItemTypeUtils>()->IsGuide(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Guide";
		}
		else if (this->IsSplineShape(pageItemUIDRef, pageItemType) == kTrue)
		{
			// see if we can determine if it is a spline, and what shape it is...
			// we know it is a spline...
			pageItemType.Append(" spline");
		}
		else
		{
			pageItemType = "Unknown";
		}
	} while (false);
}

/* GetShape
*/
bool16 
N2PSQLDNDDropTarget::IsSplineShape(const UIDRef& itemUIDRef, PMString& shape) const
{
	bool16 result = kFalse;
	InterfacePtr<IPathGeometry> pathGeometry(itemUIDRef,UseDefaultIID());
	if (pathGeometry != nil)
	{
		result = kTrue;
		PMPageItemType pathType = Utils<IPathUtils>()->WhichKindOfPageItem(pathGeometry);
		switch (pathType)
		{
			case kIsLine:
				shape = "Line";
				break;
			case kIsRectangle:
				shape = "Rectangle";
				break;
			case kIsCircle:
				shape = "Circle";
				break;
			case kIsOval:
				shape = "Oval";
				break;
		/*	case kIsRegularPoly:
				shape = "RegularPoly";
				break;
			case kIsIrregularPoly:
				shape = "IrregularPoly";
				break;*/
			case kIsSquare:
				shape = "Square";
				break;
			default:
				shape = "Unknown";
				break;
		}
	}
	return result;
}

/* IsGroup
*/
bool16 
N2PSQLDNDDropTarget::IsGroup(const UIDRef& itemUIDRef) const
{
	bool16 result = kFalse;
	do
	{
		// Groups have a hierarchy.
		InterfacePtr<IHierarchy> hierarchy(itemUIDRef, UseDefaultIID());
		if (hierarchy == nil)
			break;

		// Groups don't have an IGraphicFrameData
		InterfacePtr<IGraphicFrameData> graphicFrameData(hierarchy, UseDefaultIID());
		if (graphicFrameData != nil)
			break;

		// Groups have more than one child.
		if (hierarchy->GetChildCount() <= 1)
			break;

		// OK it's a group.
		result = kTrue;
	} while (false);
	return result;
}

/* IsEmptyGraphicFrame
*/
bool16 
N2PSQLDNDDropTarget::IsEmptyGraphicFrame(const UIDRef& itemUIDRef) const
{
	bool16 result = kFalse;
	do
	{
		// Graphic frames have an IGraphicFrameData.
		InterfacePtr<IGraphicFrameData> graphicFrameData(itemUIDRef, UseDefaultIID());
		if (graphicFrameData == nil)
			break;
		if (graphicFrameData->IsGraphicFrame() != kTrue)
			break;

		// Empty graphic frames don't have content.
		if (graphicFrameData->HasContent() == kTrue)
			break;

		// OK it's an empty graphic frame.
		result = kTrue;
	} while (false);
	return result;
}


// End, N2PSQLDNDDropTarget.cpp.






