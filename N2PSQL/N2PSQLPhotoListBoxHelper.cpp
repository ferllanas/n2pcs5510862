//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/common/N2PSQLPhotoListBoxHelper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #10 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
// Interface includes

#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IWidgetUtils.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "ITriStateControlData.h"

// implem includes
#include "PersistUtils.h" // GetDatabase
#include "IPalettePanelUtils.h"
#include "SDKFileHelper.h"
#include "ISysFileData.h"
#include "IBoolData.h"
#include "IPMStream.h"
#include "StreamUtil.h"

#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CAlert.h"
#include "FileUtils.h"
#include "SDKUtilities.h"

#include "N2PSQLPhotoListBoxHelper.h"
#include "N2PSQLID.h"

N2PSQLPhotoListBoxHelper::N2PSQLPhotoListBoxHelper(IPMUnknown* owner, const PluginID& pluginID, 
								   const WidgetID& listBoxID, const WidgetID& owningWidgetID) : fOwner(owner), 
													fOwningPluginID(pluginID), fListboxID(listBoxID),
													fOwningWidgetID(owningWidgetID)
{

}

N2PSQLPhotoListBoxHelper::~N2PSQLPhotoListBoxHelper()
{
	fOwner=nil;
}




IControlView * N2PSQLPhotoListBoxHelper ::FindCurrentListBox()
{
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView = nil;
	do {
		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		ASSERT_MSG(iPanelControlData != nil, "N2PSQLPhotoListBoxHelper ::FindCurrentListBox() iPanelControlData nil");
		if(iPanelControlData == nil) {
			break;
		}
		listBoxControlView = 	iPanelControlData->FindWidget(fListboxID);
		ASSERT_MSG(listBoxControlView != nil, "N2PSQLPhotoListBoxHelper ::FindCurrentListBox() no listbox");
		if(listBoxControlView == nil) {
			break;
		}
	
	} while(0);

	return listBoxControlView;
}

 

void N2PSQLPhotoListBoxHelper::AddElement( const PMString & RutaPhoto,
								const PMString & Titulo, 
								const PMString & DescripcionPhoto,
								const PMString & TituloPhoto, 
								const PMString & AutorPhoto, 
								const PMString & IDElementoPhoto, int atIndex)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		
		InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
		if(listAttr == nil) 
		{
			break;	
		}
		
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
				return;
		
	
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwningPluginID, kViewRsrcType, widgetRsrcID);
		
	
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "N2PSQLPhotoListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) {
			break;
		}
		
	
		this->AddListElementWidget(newElView, RutaPhoto,Titulo, DescripcionPhoto, TituloPhoto, AutorPhoto,IDElementoPhoto, atIndex);
	
	}
	while (false);			// false loop
}


void N2PSQLPhotoListBoxHelper::RemoveElementAt(int indexRemove)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "N2PSQLPhotoListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) {
			// Don't remove outside of list data bounds
			break;
		}
		removeCellWidget(listBox, indexRemove);
		listControlData->Remove(indexRemove);
	}
	while (false);			// false loop
}



void N2PSQLPhotoListBoxHelper::RemoveLastElement()
{
	if(!verifyState())
		return;

	do
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "N2PSQLPhotoListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex >= 0) {		
			listControlData->Remove(lastIndex);
			removeCellWidget(listBox, lastIndex);
		}
		
	}
	while (false);
}



int N2PSQLPhotoListBoxHelper::GetElementCount() 
{
	int retval=0;
	do {
	
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "N2PSQLPhotoListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void N2PSQLPhotoListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {

		if(listBox==nil) break;
		// recall that when the element is added, it is added as a child of the cell-panel
		// widget. Therefore, navigate to the cell panel and remove the child at the specified
		// index. Simultaneously, remove the corresponding element from the list controldata.
		// +
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLPhotoListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLPhotoListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLPhotoListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) {
			break;
		}
		
		InterfacePtr<IControlView> view(cellPanelData->GetWidget(removeIndex), IID_ICONTROLVIEW);
		ASSERT_MSG(view != nil,"N2PSQLPhotoListBoxHelper::removeCellWidget() view nil");
		
		cellPanelData->RemoveWidget(removeIndex);
		
		if( view )
			Utils<IWidgetUtils>()->DeleteWidgetAndChildren(view);
		// -

	} while(0);

}


void N2PSQLPhotoListBoxHelper::AddListElementWidget(InterfacePtr<IControlView> & elView, 
													const PMString & RupaPhoto, 
													const PMString & Titulo, 
													const PMString & DescripcionPhoto,
													const PMString & TituloPhoto, 
													const PMString & AutorPhoto, 
													const PMString & IDElementoPhoto, 
													int atIndex)
{
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) {
		return;
	}

	do {
	
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) {
			break;
		}
		
		//CAlert::InformationAlert("En Lista:"+RupaPhoto);
		this->doPreview(RupaPhoto, elView, kN2PPhotoCustomPanelViewWidgetID);
		
		this->SETStringOnWidgetOfElementList(newElPanelData, kN2PsqlIdElePhotoLabelWidgetID, IDElementoPhoto);
		
		this->SETStringOnWidgetOfElementList(newElPanelData, kN2PsqlTituloPhotoLabelWidgetID, Titulo);
		
		this->SETStringOnWidgetOfElementList(newElPanelData, kN2PsqlDescripcionPhotoLabelWidgetID, DescripcionPhoto);
		
		this->SETStringOnWidgetOfElementList(newElPanelData, kN2PsqlAutorPhotoLabelWidgetID, AutorPhoto);
		
		this->SETStringOnWidgetOfElementList(newElPanelData, kN2PsqlDescrAutorPhotoLabelWidgetID, Titulo);
		
		this->SETStringOnWidgetOfElementList(newElPanelData, kN2PsqlTituloDeLaPhotoLabelWidgetID, TituloPhoto);
		
		
		/*// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(kN2PsqlNombrePhotoLabelWidgetID);
		if ( (nameTextView == nil)  ) {
			break;
		}
		
		
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) {
			break;
		}	
		
		
		newEltext->SetString(Titulo, kTrue, kTrue);*/
		
		
		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLPhotoListBoxHelper::AddListElementWidget() Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		
		

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLPhotoListBoxHelper::AddListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

	
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "N2PSQLPhotoListBoxHelper::AddListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) {
			break;
		}


		
		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		
		
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "N2PSQLPhotoListBoxHelper::AddListElementWidget() listData nil");
		if(listData == nil) { 
			break;
		}
		
		listData->Add(elView, atIndex);
		
	} while(0);

}


void N2PSQLPhotoListBoxHelper::EmptyCurrentListBox()
{
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox();
		if(listBoxControlView == nil) {
			break;
		}
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) {
			break;
		}
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) {
			break;
		}
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}



/* doPreview
*/
void N2PSQLPhotoListBoxHelper::doPreview(const PMString& path, InterfacePtr<IControlView> & elView, WidgetID CustomPanelViewWidgetID)
{
	do
	{
		PMString PathCorrect="";
		bool16 ExisteListo=kFalse;
		//Para la administracion de fotos
    					
		PMString FullNameOfLink=path;
						
		PMString Separator="";
#ifdef WINDOWS
						Separator="\\";
#endif
#ifdef MACINTOSH
						SDKUtilities::AppendPathSeparator(Separator);
#endif

		int32 LastSeparator=FullNameOfLink.LastIndexOfCharacter(Separator.GetChar(0));
		PMString numebre="";
		numebre.AppendNumber(LastSeparator);
		
		if(LastSeparator>0)
		{
			PMString FullNameOfLinkListo=FullNameOfLink;
			FullNameOfLinkListo.Insert("@",1,LastSeparator+1);
						
			IDFile FileList;
			FileUtils::PMStringToIDFile(FullNameOfLinkListo,FileList);
						
			SDKFileHelper fileHelper(FileList);
			if(fileHelper.IsExisting())
			{
				PathCorrect=FullNameOfLinkListo;
				ExisteListo=kTrue;
			}
		}
						
		if(!ExisteListo)
			PathCorrect=path;
		
		SDKFileHelper fileHelper(PathCorrect);
		if(!fileHelper.IsExisting())
			break;
		IDFile xFile = fileHelper.GetIDFile();
		
		
		InterfacePtr<IPanelControlData>	iPanelControlData( elView,  UseDefaultIID());
	
		ASSERT(iPanelControlData);
		if(!iPanelControlData)
		{
			break;
		}
		
		
		IControlView* iWidgetView = iPanelControlData->FindWidget(CustomPanelViewWidgetID);
		ASSERT(iWidgetView);
		if(!iWidgetView)
		{
			break;
		}
		
		
		InterfacePtr<IBoolData> 
			iBoolData(iWidgetView, UseDefaultIID());
		ASSERT(iBoolData);
		if(!iBoolData)
		{
			break;
		}
		
		iBoolData->Set(kFalse);

		// below, if we could find the stream, we'll set this true
		InterfacePtr<IPMStream> iDataFileStream(StreamUtil::CreateFileStreamReadLazy(xFile));
		// If nil, we couldn't have opened this
		if (iDataFileStream == nil)
		{
			break;
		}

		
		iDataFileStream->Close();

		
		InterfacePtr<ISysFileData> 	iSysFileData(iWidgetView, IID_ISYSFILEDATA);
		ASSERT(iSysFileData);
		if(!iSysFileData)
		{
			break;
		}
		
		iSysFileData->Set(xFile);

		
		// It should be a good file since we got this far
		iBoolData->Set(kTrue);
		
		iWidgetView->Invalidate();
		
	} while(kFalse);
}



void N2PSQLPhotoListBoxHelper::SETStringOnWidgetOfElementList(InterfacePtr<IPanelControlData>& newElPanelData, const WidgetID & widget,  const PMString &  stringText)
{
	
	do
	{
		if(newElPanelData==nil)
			break;
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(widget);
		if ( (nameTextView == nil)  ) {
			break;
		}
		
		
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) {
			break;
		}	
		
		
		newEltext->SetString(stringText, kTrue, kTrue);
	}while(false);
	
}


/**
		Accesor para buscar el elemento seleccionado de la lista a partir de un widget que se encuentra sobre este elemento.
*/
int N2PSQLPhotoListBoxHelper::SelectElemListOfPictureDraged(InterfacePtr<IControlView> controlView)
{
	int32 indexRetval=-1;
	do
	{
		if(controlView==nil)
			break;
		//Interface para preguntar por el padre ya sea controlview o IPanelControlData
		InterfacePtr<IWidgetParent> iWidgetParent(controlView,UseDefaultIID());
		ASSERT(iWidgetParent);
		if(!iWidgetParent)
		{
			CAlert::InformationAlert("failed iWidgetParent");
			break;
		}
		
		//Este es el ControlView del Elemento de la lista del widget que se desea dragear
		InterfacePtr<IControlView> PanelElemListView((IControlView*)iWidgetParent->QueryParentFor(IID_ICONTROLVIEW));
		ASSERT(PanelElemListView);
		if(!PanelElemListView)
		{
			CAlert::InformationAlert("failed PanelElemListView");
			break;
		}
		
		//Este es el IPanelControlData del Elemento de la lista del widget que se desea dragear
		InterfacePtr<IPanelControlData> PanelElemListData(PanelElemListView, UseDefaultIID());//(IPanelControlData*)iWidgetParent->QueryParentFor(IID_IPANELCONTROLDATA));
		ASSERT(PanelElemListData);
		if(!PanelElemListData)
		{
			CAlert::InformationAlert("failed PanelElemListData");
			break;
		}
		
		
		InterfacePtr<IWidgetParent> iWidgetCellParent(PanelElemListView,UseDefaultIID());
		ASSERT(iWidgetCellParent);
		if(!iWidgetCellParent)
		{
			CAlert::InformationAlert("failed iWidgetCellParent");
			break;
		}
		
		//Este es el IPanelControlData del Elemento de la lista del widget que se desea dragear
		InterfacePtr<IPanelControlData> cellPanelData((IPanelControlData*)iWidgetCellParent->QueryParentFor(IID_IPANELCONTROLDATA));//(IPanelControlData*)iWidgetParent->QueryParentFor(IID_IPANELCONTROLDATA));
		ASSERT(cellPanelData);
		if(!cellPanelData)
		{
			CAlert::InformationAlert("failed cellPanelData");
			break;
		}
		
		
		indexRetval = cellPanelData->GetIndex(PanelElemListView);
		
		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) {
			break;
		}
		
		InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER); // kDefaultIID not def'd
		ASSERT_MSG(listCntl != nil, "listCntl nil");
		if(listCntl == nil) 
		{
			break;
		}
		
		listCntl->Select(indexRetval);
		
	}while(false);
	return(indexRetval);
}


/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
*/
void N2PSQLPhotoListBoxHelper::ObtenerTextoDeSeleccionActual( WidgetID widget1,PMString &Text1, int32 indexSelected)
{
	
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) {
			break;
		}
			

		if(listBox==nil) break;
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tama�o de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView = PanelCCell->FindWidget(widget1);
		ASSERT_MSG(TextWigetCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView == nil) 
		{
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto( TextWigetCView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			break;
		}

		//Extraigo el texto del Widget
		Text1=ControlDETexto->GetString();

	}while (false);			// false loop
}


/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
*/
void N2PSQLPhotoListBoxHelper::SetTextoEnSeleccionActual( WidgetID widget1, PMString &Text1, int32 indexSelected)
{
	
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) 
		{
			
			break;
		}
			

		if(listBox==nil) 
		{
			
			break;
		}
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tama�o de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			//CAlert::InformationAlert("6");
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView = PanelCCell->FindWidget(widget1);
		ASSERT_MSG(TextWigetCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView == nil) 
		{
			
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto( TextWigetCView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			
			break;
		}

		//Extraigo el texto del Widget
		ControlDETexto->SetString(Text1);
	}while (false);			// false loop
}


void N2PSQLPhotoListBoxHelper::Mostrar_IconFotoColocada(int32 indexSelected, const WidgetID&  widgetId, const bool16& show )
{
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) 
		{
			//CAlert::ErrorAlert("ListBox");
			break;
		}
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			//CAlert::ErrorAlert("Panel data");
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tama�o de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* PenWigetCView = PanelCCell->FindWidget(widgetId);
		//IControlView* LockPenWigetCView = PanelCCell->FindWidget(kWLBCmpListElementLockPenWidgetID);

		ASSERT_MSG(PenWigetCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(PenWigetCView == nil) 
		{
			break;
		}

		if(PenWigetCView->IsVisible())//actualmente esta visible
		{
			if(!show)//se desea que no se muestre
				PenWigetCView->Hide();
		}
		else
		{
			if(show)
				PenWigetCView->Show(kTrue);
		}
			

		
	}while (false);			// false loop
}

void N2PSQLPhotoListBoxHelper::ChechedCheckBox(int32 indexSelected,const WidgetID& widget1,const bool16& check)
{
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) 
		{
			//CAlert::ErrorAlert("ListBox");
			break;
		}
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "N2PSQLListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			//CAlert::ErrorAlert("Panel data");
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"N2PSQLListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tama�o de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "N2PSQLListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* PenWigetCView = PanelCCell->FindWidget(widget1);
		//IControlView* LockPenWigetCView = PanelCCell->FindWidget(kWLBCmpListElementLockPenWidgetID);

		InterfacePtr<ITriStateControlData> triState(PenWigetCView, UseDefaultIID());
		if(triState==nil)
		{
			break;
		}
		
		if(check==kTrue)
		{
			if(triState->GetState()!=ITriStateControlData::kSelected)
				triState->SetState(ITriStateControlData::kSelected,kFalse,kFalse);
		}
		else
		{
			if(triState->GetState()!=ITriStateControlData::kUnselected)
				triState->SetState(ITriStateControlData::kUnselected,kFalse,kFalse);
		}
		
	}while (false);			// false loop
}