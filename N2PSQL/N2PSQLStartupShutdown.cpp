/*
 *  N2PSQLStartupShutdown.cpp
 *  News2Page
 *
 *  Created by Fernando  Llanas on 11/09/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */



#include "VCPluginHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IIdleTask.h"
#include "IStartupShutdownService.h"
#include "IControlView.h"
#include "ICommandInterceptor.h"

// General includes:
#include "CPMUnknown.h"
#include "CAlert.h"

// Project includes:
#include "N2PsqlID.h"
//#include "N2PsqlLogInLogOutUtilities.h"

/** Implements IStartupShutdownService; purpose is to install the idle task.

	
	@ingroup paneltreeview
*/
class N2PSQLStartupShutdown : 
	public CPMUnknown<IStartupShutdownService>
{
public:
	/**	Constructor
		@param boss interface ptr from boss object on which this interface is aggregated.
	 */
    N2PSQLStartupShutdown(IPMUnknown* boss );

	/**	Destructor
	 */
	virtual ~N2PSQLStartupShutdown() {}

	/**	Called by the core when app is starting up
	 */
    virtual void Startup();

	/**	Called by the core when app is shutting down
	 */
    virtual void Shutdown();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PSQLStartupShutdown, kN2PSQLStartupShutdownImpl)

/* Constructor
*/
N2PSQLStartupShutdown::N2PSQLStartupShutdown(IPMUnknown* boss) :
    CPMUnknown<IStartupShutdownService>(boss)
{
}


/* Startup
*/
void N2PSQLStartupShutdown::Startup()
{
    do 
	{
		InterfacePtr<ICommandInterceptor> cmdInterceptor ( this, UseDefaultIID() );
		if( cmdInterceptor )
		{
			InterfacePtr<ICommandProcessor> proc ( GetExecutionContextSession()->QueryCommandProcessor() );
			proc->InstallInterceptor( cmdInterceptor );
		} 
	} while(kFalse);
}


/* Shutdown
*/
void N2PSQLStartupShutdown::Shutdown()
{
	do 
	{
		
		InterfacePtr<ICommandInterceptor> cmdInterceptor ( this, UseDefaultIID() );
		if( cmdInterceptor )
		{
			InterfacePtr<ICommandProcessor> proc ( GetExecutionContextSession()->QueryCommandProcessor() );
			proc->DeinstallInterceptor( cmdInterceptor );
		} 
		
	} while(kFalse);
}

 

