/*
//
//	File:	UpdateStorysAndDBUtilis.h
//
//
//	Date:	6-Mar-20010
//  
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#ifndef __UpdateStorysAndDBUtilis_h__
#define __UpdateStorysAndDBUtilis_h__

#include"IPMUnknown.h"
#include"IDocument.h"

#include"N2PsqlID.h"

//Strructuras que guarda las preferencias de la coneccion a la base de datos
struct PreferencesConnection
{
	//PMString TypeConnection;
	//PMString IPServerConnection;
	PMString NameConnection,nameParaStyleCredito;
	PMString DSNNameConnection;
	PMString NameUserDBConnection;
	PMString PwdUserDBConnection;
	PMString PathOfServerFile;
	PMString PathOfServerFileImages;
	PMString ImagCarpInServFImages;
	PMString RutaDServidorRespNotas;
	PMString URLWebServices;
	PMReal 	 TimeToCheckUpdateElements;
	int32 	 LlenarTablaWEB;
	PMString EstadoParaExportarATWEB;
	PMString NombreEstatusParaNotasCompletas;
	int32 	 ExportarPaginaComoPDF;
	PMString RutaDePaginaComoPDF;
	int32 	 CreateBackUpPagina;
	int32 	 EsCampoGuiaObligatorio;
	int32	EsEnviarNotasHijasAWEB;
	int32	EnviaAWEBSinRetornosTitulo;
	int32	EnviaAWEBEnAltasPieFoto;
	int32 	ExportPDFPresets;
	int32 	ExportPDFPresetsEditWEB;
	PMString PresetTOPDFEditorial;
	PMString PresetTOPDFEditorialWEB;
	PMString RutaDePaginaComoPDFWEB;
	
	int32 N2PHmteSendToHmca;// Indica si debe ser enviado al hacer page done las informacion hacia la base de datos de hemeroteca
	PMString N2PHmeDSNName;// Direccion URL del servicio wed(webservice)
	PMString N2PHmeDSNUserLogin;
	PMString N2PHmeDSNPwdLogin;
	PMString N2PHmeFTPServer;
	PMString N2PHmeFTPUser;
	PMString N2PHmeFTPPwd;
	PMString N2PHmeFTPFolder;
	
	int32 N2PResizePreview;
	PMReal N2PResizeHeight;
	PMReal N2PResizeWidth;
	int32 N2PUtilizarGuias;
	//PMString nameParaStyleCredito;
};

	
	
//Structura que almacena las notas que se han fluido sobre un documento
struct StructIdFramesElementosXIdNota
{
	PMString ID_Elemento_padre;		//Id_Elemento_padre
	PMString Id_Elem_Cont_Note;		//Id_Elemento que contiene el contenido de la nota
	PMString Id_Elem_Tit_Note;		//Id
	PMString Id_Elem_PieFoto_Note;
	PMString Id_Elem_Balazo_Note;
	
	int32 UIDFrTituloNote;				//UID Frame en donde fue fluido el titulo de la nota
	int32 UIDFrPieFotoNote;
	int32 UIDFrBalazoNote;
	int32 UIDFrContentNote;
	
	PMString LastCheckInNote;
	
	PMString DSNNote;
	bool16 EsArticulo;
};



/**
	Encapsulates detail of working with the list-box API. 
	@author Ian Paterson
*/
class IUpdateStorysAndDBUtils:public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IN2PsqlUpdateStorysAndDBUtilities };
		
	virtual bool16 GetDefaultPreferencesConnection(PreferencesConnection &Connection,bool16 refreshprefernces)=0;

	virtual void MakeUpdate(IDocument* document, const bool16& RefreshListaNotas)=0;

	virtual void ActualizaNotasDesdeBD(IDocument* document)=0;
	
	/*
	virtual void RefresStatusNotas()=0;
		//Se elimino
	*/
	virtual bool16 GetPreferencesConnectionForNameConection(PMString NameConnection, PreferencesConnection &Connection)=0;
	
	virtual bool16 GuardarPreferencesConnectionForNameConection(PMString NameConnection, PreferencesConnection &Connection)=0;
	
	
	
	virtual ErrorCode CambiaTipoDEstadoDElementoNota(PMString UIDModelString,PMString EstadoNota,bool16 Visisble)=0;

	virtual ErrorCode CambiaModoDeEscrituraDElementoNota(PMString UIDModelString,bool16 setLock)=0;
	
	virtual void ShowFrameEdgesDocument(IDocument* document,bool16 Show)=0;
	
	virtual void PlaceUserNameLogedOnPalette(PMString NameUser)=0;
	
	//Funciones sobre notas
	virtual bool16 ImportarNota()=0;
	
	virtual bool16 CheckOutNota()=0;
	
	virtual bool16 NotaCmdDepositarCheckIn()=0;
	
	virtual bool16 CheckInNotasEnEdicion(bool16 IsUpdate)=0;
	
	virtual bool16 EstadoDeNotasCompleto(PMString EstatusEnCompleto)=0;
	
	virtual bool16 SeRequiereActualizacionDeNotasOnDocuments()=0;
	
	virtual bool16 SeRequiereActualizacionDeNotasDeDocument(IDocument* document)=0;
	
	virtual void CambiaEstadoLapizUnLock(PMString UIDModelString, int32 lockLapiz,bool16 Visible)=0;

	virtual bool16  ObtenerIDNotasPUpdateDesign(K2Vector<int32>& Notas)=0;
	
	virtual bool16 ActualizaTodasLNotasYDisenoPagina(const PMString& accionSolicitada)=0;
	
	virtual void LlenarVectorElemDNotasFluidas(StructIdFramesElementosXIdNota* VectorNota,int32 &contNotasInVector, IDocument* Document)=0;
	
	
	virtual bool16 DoMuestraNotasConFrameOverset()=0;
	
	
    virtual bool16 Fotos_UpdateCoordenadasEnBaseD(IDocument* document)=0;
    
   	 virtual bool16 Fotos_ActualizaLinks(IDocument* document)=0;
   	 
   	virtual bool16 Fotos_SeRequiereActualizacionPregunta(IDocument* Doc)=0;
   	
   	virtual bool16 DetachNota()=0;
   	
   	virtual bool16 DoCrearNuevaNota()=0;
	
	virtual bool16 CancelaNuevaNota()=0;
	
	virtual bool16 DoSubirABDNuevaNota()=0;
	
	virtual bool16 DoCrearPaseNota()=0;
	
	
	virtual bool16 EstableceParametrosToCheckInElementoNota(PMString IDElementoPadre,
														PMString IDElemento,
														int32 UIDElementoNota, 
														PMString& FechaSalida, 
														PMString Estado,
														PMString RoutedTo,
														PMString IDUsuario,
														PMString PublicacionID,
														PMString SeccionID,
														bool16 VieneDeCrearPase,
														IDocument* document, PMString& SA, bool16 IsUpdate,
														PMString GuiaNotaToCheckIn)=0;
														
														
	virtual	bool16 ObtenUsuarioLogeadoActualmente(PMString& UsuarioLogeado)=0;
	
	virtual bool16 GetElementosNotaDXMP_Of_UIDTextModel(int32 UIDTextModelOfTextFrame,StructIdFramesElementosXIdNota* StructNota_IDFrames,IDocument* document)=0;

	virtual bool16 CanDoCheckInNota(int32 UIDFrame)=0;
	
	virtual bool16 GuardarQuery( PMString Query)=0;
	
	virtual bool16 AlgunElementoSeEncuentraCheckauteado()=0;
	
	virtual bool16 Fotos_DesligarDePagina()=0;
	
	virtual bool16 GuardarContenidoNotasEnEdicion()=0;
	
	virtual bool16 GuardarElementoDNotaEnBDSinCheckIN(PMString IDElementoPadre,PMString IDElemento,int32 UIDElementoNota, PMString& LastCheckinNotaToXMP,PMString Estado,PMString RoutedTo,PMString IDUsuario,PMString PublicacionID,PMString SeccionID, bool16 VieneDeCrearPase , 
		IDocument* document,
		PMString& TextoExportado)=0;
		
	virtual ErrorCode  SetShortCutToMenuFittingActions()=0;
	
	virtual bool16 ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(PMString Id_Elemento_PadreDElemNotaAColocar,bool16 buscarSuPadre)=0;
	
	virtual void ExportarNotasATablaWEB(IDocument* document, const bool16& RefreshListaNotas)=0;
	
	
	//virtual void DoLigarImagenANota()=0;
	
	/*
		Metodo para crear un registro sobre la base de datos.
		de la o las fotos que se tienen seleccionadas sobre la pagina.
	*/
	virtual void Fotos_DoAceptarLigasDImagenANota()=0;
	
	/*
		Crea o actualiza registros de las fotos que se encuentran sobre la pagina en la tabla NotasWEB
	*/
	virtual bool16 ExportarNomFotosEnPagATablaWEB(IDocument* document)=0;
	
	/*
		Actualiza la guia de las Fotos.
	*/
	virtual bool16 Fotos_ActualizaGuiaDNota(IDocument* document,PMString GuiaNota, PMString Id_NotaPadreDFoto)=0;
	
	
	/*
		Desliga todos los elementos pertenecientes al Sistema editorial de la pagina
	*/
	virtual void DesligarNotasYFotosDPagina(IDocument* document)=0;
	
	/*
		Desliga un elemento de la pagina del sistema Editorial
	*/
	virtual bool16 DesligarElementoDEstrucPorNum(StructIdFramesElementosXIdNota* StructNota_IDFrames,int32 numReg)=0;
	
	/*
		Examina que el Id del documento exista sobre la base de datos
	*/
	virtual bool16 ValidaExistePaginaSobreBD(IDocument* document)=0;
	
	/*
		Desliga Pagina del Sistema Editorial junto con todas las refrencias a notas y paginas
	*/
	virtual bool16 DesligarPagina(IDocument* document)=0;
	
	
	virtual bool16 LimpiarOLlenarListaDeNotas()=0;
	
	
	virtual bool16 CambiaEstadoYLapizDeElementoNOTA(int32 UIDFrElementoNota, bool16 setLock,PMString EstadoNuevo,bool16 ChangeStatus)=0;
	
	virtual bool16 UpdateCoordenadasDeUIDElemEnBaseD(PMString IDElementoDB,int32 ElementoUID,bool16 EsFoto,bool16 MandarCeros,IDocument* document)=0;
	
	virtual bool16 Verifica_BorradoDCaja_D_News2Page(const UIDList& ListFrameToDelete)=0;
	
	virtual bool16 ValidaSiUsuario_P_CheckInEnBaseDDatos(const PMString& Id_Nota,const PMString& Id_Usuario)=0;
	
	virtual bool16 CreaPieDeFotoFunction()=0;
	
	virtual void ExportarNotasN2P_A_N2PJoomla(IDocument* document, const bool16& RefreshListaNotas)=0;
	
	virtual bool16 ExportarNomFotosEnPagA_N2P_Joomla_Multimedia(IDocument* document)=0;
	
	virtual bool16 ExportarFotosNoFluidas_DeNotasEnPagina_A_N2P_Joomla_Multimedia(IDocument* document)=0;
	
	virtual void RestaurarVersionNota()=0;
	
	
	virtual void ExportarPaginaYNotasABDHemeroteca(IDocument* document,PMString RutaPDF,PMString Rutajpg,PMString SPCallPaginnaChickIn,PreferencesConnection Connection)=0;
};


#endif // __N2PSQLListBoxHelper_h__

