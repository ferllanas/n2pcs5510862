//======================================================================================== 
// 
// 
// ADOBE CONFIDENTIAL 
// 
// Copyright 2002 Adobe Systems Incorporated. All Rights Reserved. 
// 
// NOTICE: All information contained herein is, and remains 
// the property of Adobe Systems Incorporated and its suppliers, 
// if any. The intellectual and technical concepts contained 
// herein are proprietary to Adobe Systems Incorporated and its 
// suppliers and may be covered by U.S. and Foreign Patents, 
// patents in process, and are protected by trade secret or copyright law. 
// Dissemination of this information or reproduction of this material 
// is strictly forbidden unless prior written permission is obtained 
// from Adobe Systems Incorporated. 
// 
// 
// MACINTOSH event handler for password widget 
// 
//======================================================================================== 

//#include "VCPlugInHeaders.h" 
#include "MEditBoxEventHandler.h" 

//interfaces 

//includes 
#include "TextChar.h" 
#include "N2PsqlID.h" 

class N2PSQLNonBrokenPasswordEventHandler : public MEditBoxEventHandler 
{ 
	public: 
		N2PSQLNonBrokenPasswordEventHandler(IPMUnknown *boss); 
		~N2PSQLNonBrokenPasswordEventHandler(); 

		virtual bool16 KeyDown(IEvent* e); 
}; 

CREATE_PMINTERFACE(N2PSQLNonBrokenPasswordEventHandler, kN2PSQLNonBrokenPasswordEventHandlerImpl) 

N2PSQLNonBrokenPasswordEventHandler::N2PSQLNonBrokenPasswordEventHandler(IPMUnknown *boss) : 
MEditBoxEventHandler(boss) 
{ 

} 

N2PSQLNonBrokenPasswordEventHandler::~N2PSQLNonBrokenPasswordEventHandler() 
{ 

} 

bool16 N2PSQLNonBrokenPasswordEventHandler::KeyDown(IEvent* e) 
{ 
	TXNObject theTextObject = GetMacControl(); 
	if ( theTextObject != NULL ) 
	{ 
		// we don't actually need to call this for each key stroke, but this is the easiest way 
		TXNEchoMode(theTextObject, kTextChar_Bullet, kTextEncodingUnicodeDefault, true); 
	} 

	return MEditBoxEventHandler::KeyDown(e); 
} 

//And then we go and use it like this (in your dialog .fr): 

/*type kN2PNonBrokenPasswordEditBoxWidgetBoss(kViewRsrcType) 
: TextEditBoxWidget(ClassID = kN2PNonBrokenPasswordEditBoxWidgetBoss) {}; 

NonBrokenPasswordEditBoxWidget 
( 
kLoginPasswordEditBoxWidgetId, kSysEditBoxPMRsrcId, // WidgetId, RsrcId 
kBindNone, 
Frame(105, 100, 280, 120), // Frame 
kTrue, kTrue, // Visible, Enabled 
0, // widget id of nudge button 
0, 0, // small/large nudge amount 
32, // max num chars( 0 = no limit) 
kFalse, // is read only 
kFalse, // notify on every keystroke 
kFalse, // range checking enabled 
kTrue, // blank entry allowed 
0, 0, // upper/lower bounds 
"" // initial text 
),*/