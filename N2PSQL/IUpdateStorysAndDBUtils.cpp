/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "LinksID.h"
#include "IRangeData.h"
#include "IApplication.h"
#include "ITextModel.h"
#include "ITextModelCmds.h"
#include "ICommand.h"
#include "IComposeScanner.h"
#include "IDocument.h"
#include "IDataBase.h"
#include "IFrameList.h"
#include "ILayoutUIUtils.h"
#include "IStyleNameTable.h"
#include "IWindow.h"
#include "IWindowUtils.h"
#include "IWorkspace.h"
#include "IHierarchy.h"
#include "IMultiColumnTextFrame.h"
#include "IGeometry.h"
#include "IPageItemTypeUtils.h"
#include "ILayoutControlData.h"
#include "ILayerList.h"
#include "ILayoutCmdData.h"
#include "IBoolData.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "IGraphicFrameData.h"
#include "ILockPosition.h"
#include "IIntData.h"
#include "IItemLockData.h"
#include "IFrameEdgePrefsCmdData.h"
#include "IInCopyDocUtils.h"
#include "IInCopyStoryList.h"
#include "IDataLinkHelper.h"
#include "ILinksManager.h"
#include "IDataLink.h"
#include "IDataLinkReference.h"
#include "IUpdateLinkService.h"
#include "ILinkObjectReference.h"
#include "IRestoreLinkCmdData.h"
#include "IUpdateLink.h"
#include "ISelectionUtils.h"
#include "ITextTarget.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDocumentList.h"
#include "ITextFrameColumn.h"
#include "IScrapItem.h"
//#include "IImportFileCmdData.h"
//#include "IUpdateLinkCmdData.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ErrorUtils.h"
#include "TextEditorID.h"
#include "KBSCModifierDefs.h"
#include "VirtualKey.h"
#include "IShortcutManager.h"
#include "IShortcutContext.h"
#include "IShortcutUtils.h"
#include "IActionManager.h"
#include "IDocumentLayer.h"
#include "CAlert.h"
#include "SDKLayoutHelper.h"
#include "StreamUtil.h"
#include "IFileList.h"
#include "IDocumentUtils.h"
#include "ProgressBar.h"
#include "SDKFileHelper.h"
#include "SDKUtilities.h"
#include "FileUtils.h"
#include "IFrmLblData.h"
#include "IDocFileHandler.h"
#include "SDKDef.h"
#include "TaggedTextFiltersID.h"
#include "NAMEINFO.H"
#include "ILinkFacade.H"
#include "stdio.h"
#include "ILinkUtils.h"
#include "IURIUtils.h"
#include "IPlugInList.h"


//#include "SnpXMLResolutionHelper.cpp"
//#include "FrmLblDataFacade.h"
//#include "FrmLblID.h"

#ifdef MACINTOSH
	#include <sys/stat.h>
	
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"
	#include "IN2PNotesUtils.h"
	
	#include "InterlasaUtilities.h"
	

	#include "IN2PCTUtilities.h"

	#include "ICheckInOutSuite.h"

	#include "IN2PAdIssueUtils.h"
	#include "N2PAdIssueID.h"

	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"


	#include "N2PwsdlcltID.h"
	#include "N2PWSDLCltUtils.h"
#endif

#ifdef WINDOWS
	#include <sys/stat.h>
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"
	#include "IN2PNotesUtils.h"
	
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
	#include "..\N2PCheckInOut\ICheckInOutSuite.h"

	#include "..\N2PAdornmentIssue\IN2PAdIssueUtils.h"
	#include "..\N2PAdornmentIssue\N2PAdIssueID.h"

	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"

	#include "..\InterlasaUltraProKeys\InterUltraProKyID.h"
	#include "..\InterlasaUltraProKeys\IInterUltraProKy.h"
	#include "..\InterlasaUltraProKeys\IInterUltraObjectProKy.h"
#endif


/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/


PreferencesConnection PreferenciasEstaticas;


class N2PsqlUpdateStorysAndDBUtils : public CPMUnknown<IUpdateStorysAndDBUtils>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	N2PsqlUpdateStorysAndDBUtils (IPMUnknown *boss);

	/*	Hace un Update de las cambios que se hallan realizado a la base de datos
	*/
	void MakeUpdate(IDocument* document, const bool16& RefreshListaNotas);
	
	/*
		-Actualiza des de la base de datos los elementos de3 cada una de las notas fluidos sobre el documento abierto.
		-Actualiza la lista de la busqueda de notas.
		-Actualiza los datos de la nota ultima nota seleccionada sobre la paleta.
	*/
	void ActualizaNotasDesdeBD(IDocument* document);
	
	/*	Refresca el estado de las notas sobre el Documento
	
	void RefresStatusNotas();
		//Se elimino
	*/
	
	/*	Obtiene las preferencias para la conexion a la base de datos Globales
	*/
	bool16 GetDefaultPreferencesConnection(PreferencesConnection &Connection,bool16 refrespreferencias);
	
	
	/*	Obtiene las preferencias para la conexion a la base de datos del documento de preferencias
	*/
	bool16 GetPreferencesConnectionForNameConection(PMString NameConnection, PreferencesConnection &Connection);
	
	/*Guarda las preferencias para la conexion a la base de datos en documento de preferencias
	*/
	bool16 GuardarPreferencesConnectionForNameConection(PMString NameConnection, PreferencesConnection &Connection);
	
	/*	InDica si todas las notas del Documento se encuentra en estado(Status) Copleto
	*/
	bool16 EstadoDeNotasCompleto(PMString EstatusEnCompleto);
	
	/*	Cambia el estado(Status) de una Nota (Mensaje que muestra el estado de una nota sobre el Frame de cada elemento de la nota)
	*/
	ErrorCode CambiaTipoDEstadoDElementoNota(PMString UIDModelString,PMString EstadoNota,bool16 Visivle);

	/*	Coloca el estado de un frame (Lock/Unlock) ademas de cambiar la forma del dibujo del lapiz
	*/
	ErrorCode CambiaModoDeEscrituraDElementoNota(PMString UIDModelString,bool16 setLock);
	
	/*	Muestra/Oculta los Dibujos del Frame(Marco, Lapiz, EstadoNota...etc)
	*/
	void ShowFrameEdgesDocument(IDocument* document,bool16 Show);
	
	/*	Coloca el nombre que se ha logeado sobre la paleta del N2PSQL
	*/
	void PlaceUserNameLogedOnPalette(PMString NameUser);
	
	/*	Hace CheckOut de nota a partir del elemento(TextFrame) que se encuentre seleccionado de ella
	*/
	virtual bool16 CheckOutNota();
	
	/*	Hace Checkin de nota a partir del elemento(TextFrame) que se encuentre seleccionado de ella
	*/
	virtual bool16 NotaCmdDepositarCheckIn();
	
	/*	Checa las notas que se estan editando actualmente 
		Guarda el contenido de cada uno de los elementos de la nota en la BD
		y en un archivo po si acaso se esta haciendo un Update Design en InCopy
	*/
	virtual bool16 CheckInNotasEnEdicion(bool16 IsUpdate);
	
	
	virtual bool16 SeRequiereActualizacionDeNotasOnDocuments();
	
	/*	Revisa si se debe hacer una actualizacion de las notas desde la base dedatos
	*/
	virtual bool16 SeRequiereActualizacionDeNotasDeDocument(IDocument* document);
	
	/*	Cambia el estado del Lapiz 
	*/
	virtual void CambiaEstadoLapizUnLock(PMString UIDModelString, int32 lockLapiz,bool16 Visible);
	
	/*	Obtiene los ID de los frames que componen una nota cuando se hace un Update design para volver a 
		poner estos frame como editando
	*/
	virtual bool16  ObtenerIDNotasPUpdateDesign(K2Vector<int32>& Notas);
	
	/*
		Funcion que hace la Actualizacion de Notas desde y hacia la base de datos asi como
		 la actualizacion del diseÒo de pagina si se requiere
	*/
	virtual bool16 ActualizaTodasLNotasYDisenoPagina(const PMString& ActionSolicitada);
	
	/*
		Llenado de las estructura de las notas que han sido fluidas sobre el Documento, obteniendolas desde el XMP.
			StructIdFramesElementosXIdNota* VectorNota, Vector que guarda los datos de cada elemento de las notas fluidas sobre el Documento.
			int32 &contNotasInVector,	cantidad de elementos de las notas fluidas sobre el documento.
	*/
	void LlenarVectorElemDNotasFluidas(StructIdFramesElementosXIdNota* VectorNota,int32 &contNotasInVector, IDocument* Document);
	
	
	
	/*
		Muestra un textframe adicional montranto el texto overset.
	*/
	
	 virtual  bool16 DoMuestraNotasConFrameOverset();
	 
	 
	 /*
	 */
	virtual bool16 ImportarNota();
	 
	 
	virtual bool16 Fotos_UpdateCoordenadasEnBaseD(IDocument* document);
	 
	virtual bool16 Fotos_ActualizaLinks(IDocument* document);
	 
	 
	virtual bool16 Fotos_SeRequiereActualizacionPregunta(IDocument* Document);
	 
	 
	virtual bool16 DetachNota();
	 
	virtual bool16 DoCrearNuevaNota();
	
	virtual bool16 CancelaNuevaNota();
	
	virtual bool16 DoSubirABDNuevaNota();
	
	virtual bool16 EstableceParametrosToCheckInElementoNota(PMString IDElementoPadre,
														PMString IDElemento,
														int32 UIDElementoNota, 
														PMString & FechaSalida, 
														PMString Estado,
														PMString RoutedTo,
														PMString IDUsuario,
														PMString PublicacionID,
														PMString SeccionID,
														bool16 VieneDeCrearPase, IDocument* document, PMString& TextoExportado,bool16 isUpdate,
														PMString GuiaNotaToCheckIn);
														
	virtual bool16 GetElementosNotaDXMP_Of_UIDTextModel(int32 UIDTextModelOfTextFrame,StructIdFramesElementosXIdNota* StructNota_IDFrames, IDocument* DocFront);
	
	virtual bool16 ObtenUsuarioLogeadoActualmente(PMString& UsuarioLogeado);
	
	/*
		Revisa si el frame se encuentra en estado de edicion para poder hacer un check in a todos los elementos de nota a que corresponde este frame.
			int32 UIDFrame, Id del textFrame.
	*/
	virtual bool16 CanDoCheckInNota(int32 UIDFrame);
	
	
	virtual bool16 GuardarQuery( PMString Query);
	
	virtual bool16 AlgunElementoSeEncuentraCheckauteado();
	
	virtual bool16 Fotos_DesligarDePagina();
	
	
	virtual bool16 GuardarContenidoNotasEnEdicion();
	
	virtual bool16 GuardarElementoDNotaEnBDSinCheckIN(PMString IDElementoPadre,PMString IDElemento,int32 UIDElementoNota, PMString& LastCheckinNotaToXMP,PMString Estado,PMString RoutedTo,PMString IDUsuario,PMString PublicacionID,PMString SeccionID, bool16 VieneDeCrearPase , 
		IDocument* document,
		PMString& TextoExportado);
		
	virtual ErrorCode  SetShortCutToMenuFittingActions();
	
	virtual bool16 ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(PMString Id_Elemento_PadreDElemNotaAColocar,bool16 buscarSuPadre);

	virtual void ExportarNotasATablaWEB(IDocument* document, const bool16& RefreshListaNotas);
	
	
	//virtual void DoLigarImagenANota();
	
	/*
		Metodo para crear un registro sobre la base de datos.
		de la o las fotos que se tienen seleccionadas sobre la pagina.
	*/
	virtual void Fotos_DoAceptarLigasDImagenANota();
	
	/*
		Crea o actualiza registros de las fotos que se encuentran sobre la pagina en la tabla NotasWEB
	*/
	virtual bool16 ExportarNomFotosEnPagATablaWEB(IDocument* document);
	
	/*
		Actualiza la guia de las Fotos.
	*/
	virtual bool16 Fotos_ActualizaGuiaDNota(IDocument* document,PMString GuiaNota, PMString Id_NotaPadreDFoto);
	
	/*
		Desliga todos los elementos pertenecientes al Sistema editorial de la pagina
	*/
	void DesligarNotasYFotosDPagina(IDocument* document);
	
	/*
		Desliga un elemento de la pagina del sistema Editorial
	*/
	bool16 DesligarElementoDEstrucPorNum(StructIdFramesElementosXIdNota* StructNota_IDFrames,int32 numReg);
	
	/*
		Examina que el Id del documento exista sobre la base de datos
	*/
	virtual bool16 ValidaExistePaginaSobreBD(IDocument* document);

	/*
		Desliga Pagina del Sistema Editorial junto con todas las refrencias a notas y paginas
	*/
	virtual bool16 DesligarPagina(IDocument* document);
	
	
	/*
		Para limpiar o llenar con el documento de enfrente la lista de notas
	*/
	virtual bool16 LimpiarOLlenarListaDeNotas();
	
	
	virtual bool16 CambiaEstadoYLapizDeElementoNOTA(int32 UIDFrElementoNota, bool16 setLock,PMString EstadoNuevo,bool16 ChangeStatus);
	
	
	virtual bool16 UpdateCoordenadasDeUIDElemEnBaseD(PMString IDElementoDB,int32 ElementoUID,bool16 EsFoto,bool16 MandarCeros,IDocument* document);
	
	virtual bool16 Verifica_BorradoDCaja_D_News2Page(const UIDList& ListFrameToDelete);
	
	
	virtual bool16 ValidaSiUsuario_P_CheckInEnBaseDDatos(const PMString& Id_Nota,const PMString& Id_Usuario);
	
	/*
		Crea pie de Fotografia para utilizar esta funcion se debe de tener seleccionado al mismo tiempo una caja 
		de texto que no se aun parte de sistema editorial N2P y una caja con Fotografia que est sim sea parte del sistema editorial N2P.
		- Esta funcion liga la caja de texto seleccionada a la imagen como un elemento hijo en la base de datos.
	 */
	virtual bool16 CreaPieDeFotoFunction();
	
	/*
		Funcion que exporta las notas de que se encuentra en la pagina a la tabla N2P_Joomla_Content el texto de la notas es enviado
		sin tag de indesign.
	 */
	virtual void ExportarNotasN2P_A_N2PJoomla(IDocument* document, const bool16& RefreshListaNotas);
	
	/*
		Funcion que crea registros sobre la tabla N2P_Joomla_Multimedia de las fotografias provenientes o incluidas en el sistema 
		Editorial N2P y han sido colovcadas sobre el documento actual.
	 */
	virtual bool16 ExportarNomFotosEnPagA_N2P_Joomla_Multimedia(IDocument* document);
	/*
		Funcion que exporta las fotografias a la tabla N2P_Joomla_Multimedia que no fueron colocadas sobre la pagina actual.
	 */
	virtual bool16 ExportarFotosNoFluidas_DeNotasEnPagina_A_N2P_Joomla_Multimedia(IDocument* document);
	
	
	virtual void RestaurarVersionNota();
	
	
	
	virtual void ExportarPaginaYNotasABDHemeroteca(IDocument* document,
												PMString RutaPDF,
												PMString Rutajpg,
												PMString SPCallPaginnaChickIn,
												PreferencesConnection Connection);
	
private:

	
	/*	
		Actualiza elemento de nota desde la BD.
			PMString UIDModelString, UID del TextFrame del elemento de la nota.
			PMString ItemToSearchString, Tipo o nombre del elemento de la nota ejemp: Titulo, Balazo,,,etc
			PMString Text,	Texto que debera colocarse sobre el TextFrame
	*/
	void ActualizaElementoNotaDsdDB(PMString UIDModelString,PMString ItemToSearchString, PMString Text,  bool16 isCheckOutNota, IDocument* document,  const K2Vector<N2PNotesStorage*>& fBNotesStore);

	
	/*
		Coloca texto sobre un textframe asi como cambia el tipo de estilo de este textframe dependiendo el tipo de elemento que se este colocando.
			iTextModel iTextModel, TextModel del Frame donde se va acolora el texto.
			PMString NomStyle, Nombre del elemeto que se va a colocar por lo tanto el tipo de estilo que se va a plicar.
			PMString Texto, text que se colocara sobre el textFrame.
		
	*/
	int32 ColocaTextYAplyStyleATextFrame(InterfacePtr<ITextModel> iTextModel,PMString NomStyle, PMString Texto);

	

	/*
	int32 CantidadDNotasFluidas();
	*/
	


	
	/*
		Actualiza los elementos de una determinada nota sobre la base de Datos
			StructIdFramesElementosXIdNota* VectorNota, Vector que contiene los elementos de las notas que se han fluidop sobre el documento.
			int32 numReg,	numero de registro que apunta al vector ClassRegEnXMP el cual contien los datos para hacer la actualizacion sobre la BD.
	*/
	void ActualizaNotaDesdeBDPorIDNota(StructIdFramesElementosXIdNota* VectorNota,int32 numReg, bool16 IsCheckOut, IDocument* document);
	
	/*
		Funcion que Abre el Dialogo para hacer que checkIn a una nota.
			PMString& Estado, Para obtener el Estado con que se va a guardar la nota en la Base de datos.
			PMString& RoutedTo, Para obtener a quien va dirigido los cambios que se han realizado sobre la nota seleccionada.
	*/
	bool16 OpenDlgCheckInNota(PMString& Status,PMString& Dirigido, PMString &PublicacionID, PMString &SeccionID,PMString &GuiaNota);

	/*
		Funcion que aplica un column break cuando encuentra un salto de linea sobre un textframe.
			PMString UIDModelString, UID del Textframe o text model que se debe aplicar un ColumnBreak;
	*/
	bool16 ApplicaColumnBreakSiNecesita(PMString UIDModelString);
	
	/*
		Inserta el ColimnBreak sobre el story en la posicion determinada.
			const UIDRef storyUIDRef, UIDRef del story donde se aplicara el ColumnBreak.
			const TextIndex finishIndex, posicion nsobre el texto del story donde se aplicara el ColumnBreak.
		 
	*/
	bool16 InsertBreakColumnCmd(const UIDRef storyUIDRef, const TextIndex finishIndex );

	
	/*
		Guarda los ID de los frames que componen una nota cuando se hace un Update design para volver a 
		poner estos frame como editando.
			K2Vector<int32> Notas, vector donde guardara los ID de los textFrames que se encontraban etidando antes de hacer UpdateDesign
	*/
	void  guardarIDNotasPUpdateDesign(K2Vector<int32> Notas);
	

	
	
	/*
		-Actualiza a BD el contenido de los elementos de la nota,
		-Cambia el estado de escritura(Lapiz),
		-Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota, 
		-Libera la nota en la BD para ser modificad por cualquier otro usuario
			int32 IDNota, entero que indica el ID de la nota en la BD
	*/
	bool16 CheckInNotaDBAndPag(StructIdFramesElementosXIdNota* VectorNota,PMString Estado,PMString RoutedTo,PMString Id_Empleado, PMString PublicacionID,PMString SeccionID, bool16 ProvieneD_CrearPase, IDocument* document,PMString GuiaNota);
	
														
																
	bool16 CanDoCheckOutNota(int32 UIDTextModelOfTextFrame);
	
	
	
	

	
	bool16 CambiaEstadoYLapizDeNota(StructIdFramesElementosXIdNota* VectorNota,bool16 setLock,PMString EstadoNuevo,bool16 ChangeStatus,const int32 numNotaEnEstruct=0);
	
	
	PMString ARMA_UPDATE_NOTA_QUERY(StructIdFramesElementosXIdNota* VectorNota,int32 numreg, bool16 IsCheckOut);
	
	
	
	bool16 OcultaYMuestraWidgetsPNNota(bool16 Muestra);
	
	
	bool16 DoCrearPaseNota();
	
	
	
	
	bool16 CheckInElementoNota(PMString& Params, bool16 NotaEnModoCheckOut,K2Vector<PMString>& VectorQuery);
	
	
														
	
	
	
	bool16 Insert_IN_Bitacora_Elemento(PMString Fecha_Edicion,PMString ID_Elemento_padre,PMString IDUsuario, PMString Estado, const bool16& GuardarRevisionElemento);
	
	
	bool16 GetElementosNotaDXMP_Of_IDElementoPadre(PMString UIDTextModelOfTextFrame,
							StructIdFramesElementosXIdNota* StructNota_IDFrames,
							IDocument* document);

	bool16 SubeRespaldoDeNota(PMString IdElemento,//Id elemento Nota
											PMString tituloC,//Contenido del titulo
											PMString summaryC,//Contenido del Balazo
											PMString photocaptionC,//Contenido del pir de foto
											PMString storyC,//Contenido de la nota
											PMString fechaC,//Fecha de creacion
											PMString nombreP,//Nombre de Publiucacion
											PMString nombreS,//Nombre de seccion
											PMString creator,//Nombre del usuario Creador
											PMString& Ruta_Elemento);//Ruta Default o que ya se encuentra en la base de datos
											
	bool16 BuscaRutaDeElementoYSubeRespaldoNota(PMString IdElemento,
											PMString Conte_Titulo,
											PMString Conte_balazo,
											PMString Conte_PieFoto,
											PMString Conte_Contenido,
											PMString FechCreacion,
											PMString Publicacion,
											PMString Seccion,
											PMString Creador);
											
	 bool16 Fotos_ReestablecerLinkDFoto(IDFile FileList,IDataLink* datalink,UIDRef linkRef,IDataBase* database);
											
	bool16 Fotos_TraerPathDeFotoEnBD(PMString IDElementoFoto,PMString DSNName, PMString& PathReturn );
	
	
	bool16 HacerRevisionPaDocNuevo();

	bool16 OpenDlgDetachElemento();
	
	
	bool16 EnviarPorLotesContenidoDElemento_De_Nota(PMString Conte_Contenido,PMString IdElemento_Contenido);
	
	
	bool16 OpenDlgCrearNotaHija();
	
	bool16 PreguntaSiSeEstaEditandoElementoDNota(PMString UIDModelString,IDocument* document);
	
	bool16 CheckInNotaDBAndPag_CompletesElements(StructIdFramesElementosXIdNota* VectorNota,
						PMString Estado,
						PMString RoutedTo,
						PMString IDUsuario,
						PMString PublicacionID,
						PMString SeccionID, 
						bool16 ProvieneDeCrearPase, 
						IDocument* document, 
						PMString GuiaNota);


	PMString RegresaParametrosParaCheckInElementoNota(
		PMString IDElemento,int32 UIDElementoNota, 
		IDocument* document,
		PMString& TextoExportado);
		
	//Metodo que envia texto plano a versiones de elemento las notas que esten editandose
	bool16 GuardarContenidoNotasEnEdicionEn_TVersionesDElemento_ComoTextPlain();
	
	//Metodo que envia por lotes el contenido de las notas o elemento de las notas a Tabla Versiones de Elementos
	bool16 EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(PMString Conte_Contenido,PMString IdDVersionElemento,PMString Id_TipoElem);
	
	
	
	
	//Medoto que abre el dialogo para la restauracion de las notas
	bool16 DoDialogVersionesNota();	
	
	/*
		Envia version de texto plano a tabla versiones de elemeto
	 */
	void EnviaAVersionDNota(PMString ID_Elemento_padre,PMString IDUsuario,PMString Estado,PMString ID_Evento,PMString IdBitacoraElemento,PMString Fecha_Edicion);

	
	//1073
	
	PMString ObtenerContenidoPlanoDElementoDesdeBaseDDatos(const PMString& Id_Nota,const int32& tipoElemento);	
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PsqlUpdateStorysAndDBUtils, kN2PsqlUpdateStorysAndDBUtilisImpl)


/* HelloWorld Constructor
*/
N2PsqlUpdateStorysAndDBUtils::N2PsqlUpdateStorysAndDBUtils(IPMUnknown* boss) 
: CPMUnknown<IUpdateStorysAndDBUtils>(boss)
{
}

/*
		Update TextFrame To DB
*/
void N2PsqlUpdateStorysAndDBUtils::MakeUpdate(IDocument* document, const bool16& RefreshListaNotas)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::MakeUpdate ini");
	//CAlert::InformationAlert("Make Update");
	do
	{	
		PMString ItemToSearchString="";
		PMString UIDModelString="";
		PMString IDNotaString="";
		PMString Busqueda="";
		PMString IDUsuario="";	
		PMString Aplicacion="";	
		PMString GuiaNota="";	
		
		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_Nota);
		
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
			(
				kN2PCTUtilitiesBoss,	// Object boss/class
				IN2PCTUtilities::kDefaultIID
			)));
		
		if(N2PCTUtils==nil)
			break;
		
			
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);

		if(contNotasInVector>0)
		{
							
			
			
			PreferencesConnection PrefConections;
		
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			//
			N2PCTUtils->OcultaNotasConFrameOverset();	
			
			//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
					K2Vector<PMString> Params;
						
						
					
					PMString IdNotaDB = VectorNota[numNota].ID_Elemento_padre;
					
					if(PrefConections.NameConnection!=VectorNota[numNota].DSNNote)
					{
						//Como esta nota proviene de otro servidor se la salta y prosigue con la siguiente
						continue;
					}
					
					PMString Conte_Titulo="";
					PMString Conte_balazo="";//Contenido del Balazo
					PMString Conte_PieFoto="";//Contenido del pir de foto
					PMString Conte_Contenido="";//Contenido de la nota
						
					if( Aplicacion.Contains("InCopy")  )
					{	
						
						//Es InCopy solo modifica Texto
						
							
							
							PMString LastCheckinNotaToXMP;//=Params[15];
							PMString Fecha_Edicion="0";
							LastCheckinNotaToXMP=Fecha_Edicion;
							Fecha_Edicion="0";
							
			
							LastCheckinNotaToXMP=Fecha_Edicion;
							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrTituloNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Tit_Note, VectorNota[numNota].UIDFrTituloNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Titulo,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
								
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;

							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrPieFotoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_PieFoto_Note, VectorNota[numNota].UIDFrPieFotoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_PieFoto,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;

							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrBalazoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Balazo_Note, VectorNota[numNota].UIDFrBalazoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_balazo,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;
							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrContentNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Cont_Note, VectorNota[numNota].UIDFrContentNote, Fecha_Edicion , "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Contenido,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
							
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;
							
							//CAlert::InformationAlert("MAMAMAMA"+LastCheckinNotaToXMP);
							this->Insert_IN_Bitacora_Elemento(LastCheckinNotaToXMP,VectorNota[numNota].ID_Elemento_padre,IDUsuario,"0", kFalse);
							
							
							Fecha_Edicion="0";
						
							//CAlert::InformationAlert(LastCheckinNotaToQuery);
							if(LastCheckinNotaToXMP.NumUTF16TextChars()>0)
							{
								//LastCheckinNotaToXMP="0";
								N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[numNota].ID_Elemento_padre,LastCheckinNotaToXMP, document);
							}
							
							
						
						
					}
					else
					{	
							
							PMString LastCheckinNotaToXMP;//=Params[15];
							PMString Fecha_Edicion="0";
							LastCheckinNotaToXMP=Fecha_Edicion;
							Fecha_Edicion="0";
							Fecha_Edicion=MySQLFecha;
							if(VectorNota[numNota].UIDFrContentNote>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Cont_Note, VectorNota[numNota].UIDFrContentNote, Fecha_Edicion , "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Contenido,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
			
							LastCheckinNotaToXMP=Fecha_Edicion;
							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrTituloNote>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Tit_Note, VectorNota[numNota].UIDFrTituloNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Titulo,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
							
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;

							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrPieFotoNote>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_PieFoto_Note, VectorNota[numNota].UIDFrPieFotoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_PieFoto,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
							
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;

							Fecha_Edicion=MySQLFecha;
							
							if(VectorNota[numNota].UIDFrBalazoNote>0)
								if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Balazo_Note, VectorNota[numNota].UIDFrBalazoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_balazo,kFalse,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
							
							
							
							if(Fecha_Edicion!=MySQLFecha)
								LastCheckinNotaToXMP=Fecha_Edicion;

							//CAlert::InformationAlert("APPAPAAPPA"+LastCheckinNotaToXMP);
							this->Insert_IN_Bitacora_Elemento(LastCheckinNotaToXMP,VectorNota[numNota].ID_Elemento_padre,IDUsuario,"0",kFalse);
							
							if(LastCheckinNotaToXMP.NumUTF16TextChars()>0)
							{
								
								N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[numNota].ID_Elemento_padre,LastCheckinNotaToXMP, document);
							}
						
							
						
					}
					
					if(VectorNota[numNota].EsArticulo)
						this->BuscaRutaDeElementoYSubeRespaldoNota(VectorNota[numNota].ID_Elemento_padre, Conte_Titulo, Conte_balazo, Conte_PieFoto, Conte_Contenido,"","","","");
					
		
					
							
			}	
		
				
			Busqueda=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlBusquedaTextWidgetID);
			if(Busqueda.NumUTF16TextChars()>0 && RefreshListaNotas==kTrue)
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
			else
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
			
			
			

			PMString IDNotaStri=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
			
			if(IDNotaStri.NumUTF16TextChars()>0)
			{
				
	 			if(N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID))
	 			{
	 				//hacer busquedas para pases
	 				//N2PSQLUtilities::LlenarElementosDePasesEnVentanaN2PSQL();
	 			}
	 			else
	 			{
	 				//PMString Busqueda= "CALL QueryElementdNotas2 (" + IDNotaStri +")";
	 				//N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kFalse);
	 			}
	 			
				
			}
			
			if( N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowTxtOversetEditWidgetID))
				this->DoMuestraNotasConFrameOverset();
			//CAlert::InformationAlert(kN2PsqlUpdateFinishedStringKey);
		}
		else
		{
			ASSERT_FAIL(kN2PSQLNoExistenNotasSobreDocumentoStringKey);
		}
	
		//delete VectorNota;
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::MakeUpdate Fin");
}




/*
	-Actualiza des de la base de datos los elementos de3 cada una de las notas fluidos sobre el documento abierto.
	-Actualiza la lista de la busqueda de notas.
	-Actualiza los datos de la nota ultima nota seleccionada sobre la paleta.
*/

void N2PsqlUpdateStorysAndDBUtils::ActualizaNotasDesdeBD(IDocument* document)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaNotasDesdeBD ini");
	do
	{
	
		N2PSQLUtilities::SetRootStyleADefaultCharStyle();
		
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
		
			break;
		}
		
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		
		if(contNotasInVector>0)
		{
				
				
			//Actualiza des de la base de datos los elementos de3 cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
				
				ActualizaNotaDesdeBDPorIDNota(VectorNota,numNota, kFalse, document);
				
			}
				
			//Obtiene el ultmo query utilizado para buscar notas y llenar la lista de notas en la paleta
			PMString Busqueda=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlBusquedaTextWidgetID);
				
			//Obtiene el ID de la ultima nota seleccionada sobre la lista en la paleta.
			PMString IDNotaStri=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
				
			//Actualiza la lista de la busqueda de notas.
			if(Busqueda.NumUTF16TextChars()>0)
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple",kFalse);
			else
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple",kFalse);
			
			//Actualiza los datos de la nota ultima nota seleccionada sobre la paleta.
			if(IDNotaStri.NumUTF16TextChars()>0)
			{
				if(N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID))
	 			{	//hacer busquedas para pases
	 				//N2PSQLUtilities::LlenarElementosDePasesEnVentanaN2PSQL();
	 			}
	 			else
	 			{
					Busqueda= "CALL QueryElementdNotas2 (" + IDNotaStri +")";
	 				N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kFalse);
	 			}
			}	
		}
		else
		{
			ASSERT_FAIL(kN2PSQLNoExistenNotasSobreDocumentoStringKey);
		}
	
		//delete VectorNota;
		
	}while(false);
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaNotasDesdeBD fin");
}

void N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota(StructIdFramesElementosXIdNota* VectorNota,int32 numReg,  bool16 IsCheckOut, IDocument* document)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota ini");
	int32 index=0;
	PMString texto;
	PMString UIDModelString="";
	PMString IDNotaString="";
	PMString ItemToSearchString="";
	PMString Consulta="";
	int32 UIDModel=0;
	int32 IDNota=0;
	PMString NameDoc="";
	int32 IndexString=0;
	bool16 HacerUpdate_D_Nota=kTrue;
	
	bool16 TraesElementosConPases=kFalse;
	if(IsCheckOut==kTrue)
		TraesElementosConPases=kTrue;
	else
		TraesElementosConPases=kFalse;
	
	do
	{
				
		Consulta = this->ARMA_UPDATE_NOTA_QUERY( VectorNota, numReg, IsCheckOut);
	
		if(Consulta.NumUTF16TextChars()<=0)
		{
			break;
		}
		//CAlert::InformationAlert(Consulta);
		////// Obtiene Preferencias para conexion a partir del nombre de donde prvieve/////////////
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota No Preferences");
			break;
		}
		
		//Para poner por default el root de caracter de estilos como Default
		N2PSQLUtilities::SetRootStyleADefaultCharStyle();
				
				
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
				
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
						
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		PMString Estatus_Note="";
		PMString UltimoCheckIn ="";
		PMString DirigidoA ="";
		PMString GuiaDNota ="";
		PMString Label ="";
		for(int32 j=0;j<QueryVector.Length() && HacerUpdate_D_Nota==kTrue;j++)
		{
			
					
			
			//Hacer comparacion entre las fechas para verificar si se ha modificado la nota desde la ultima vez que fue actualizada
			UltimoCheckIn = SQLInterface->ReturnItemContentbyNameColumn("Fecha_Ult_ModOUT",QueryVector[j]);
			
			//CAlert::InformationAlert(UltimoCheckIn+"=="+VectorNota[numReg].LastCheckInNote+"\n"+VectorNota[numReg].Id_Elem_Cont_Note);
			//Para ver si cambiio si no cambio contunua con el ciclo
			if(UltimoCheckIn==VectorNota[numReg].LastCheckInNote)//si llas fechas son iguales no hay que hacer update de nota
				HacerUpdate_D_Nota=kTrue; //Simpre va a traerse los cambios y actualizar la nota
			
			
			DirigidoA = SQLInterface->ReturnItemContentbyNameColumn("DirigidoAOUT",QueryVector[j]);
			
			Estatus_Note  = SQLInterface->ReturnItemContentbyNameColumn("Nom_EstatusOUT",QueryVector[j]);
			
			GuiaDNota  = SQLInterface->ReturnItemContentbyNameColumn("GuiaNota",QueryVector[j]);
			
			
			if(DirigidoA.NumUTF16TextChars()>0)
				Label=DirigidoA+"/"+Estatus_Note;
			else
				Label=Estatus_Note;
		}
		
		if( HacerUpdate_D_Nota==kFalse)
		{
			break;
		}
		
		
		/////Para Elemento Titulo de la Nota
		UIDModelString="";
		texto ="";
		
		if(VectorNota[numReg].UIDFrTituloNote>0)
		{ 
			//CAlert::InformationAlert("A "+VectorNota[numReg].Id_Elem_Tit_Note);
			UIDModelString.AppendNumber(VectorNota[numReg].UIDFrTituloNote);
			
			if(PreguntaSiSeEstaEditandoElementoDNota(UIDModelString,document) && IsCheckOut==kFalse)//si regresa verdadero quiere decir que se esta editando la nota
			{					//Esto es para evitar hacer consulta a la base de datos y traernos el contenido de una nota
				//CAlert::InformationAlert("Salio por que se esta editando la nota o por que no existe la caja ligada anteriormente");
				break;
			}

			
			//CAlert::InformationAlert("A1");
			texto =   N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, VectorNota[numReg].Id_Elem_Tit_Note, TraesElementosConPases);// SQLInterface->ReturnItemContentbyNameColumn("Titulo",QueryVector[j]);
		
			//CAlert::InformationAlert("A2");
			
			if(UIDModelString.NumUTF16TextChars()>0)
			{
				if(texto.NumUTF16TextChars()>0)
				{
					
					//CAlert::InformationAlert("A3");
					if (document == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota document is invalid");
						break;
					}
					//CAlert::InformationAlert("A4");
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota db is invalid");	
						break;
					}
					//CAlert::InformationAlert("A5");
					UID uidStory=UIDModelString.GetAsNumber();
					///////////////////Obtener texto de Texframe
					//CAlert::InformationAlert("A6");
					UIDRef graphicFrame(db,uidStory);
					
					//CAlert::InformationAlert("A7");
					K2Vector<N2PNotesStorage*> fBNotesStore;
					//CAlert::InformationAlert(texto);
					UIDRef itemToPlace = N2PSQLUtilities::ImportTaggedTextOfPMString(texto, fBNotesStore);
					
					//CAlert::InformationAlert("A8");
					if(itemToPlace == UIDRef::gNull)
					{
						/////////////////////////////////////////////
						//Para cuando viene en formato de texto plano
						/////////////////////////////////////////////
						//CAlert::InformationAlert("Para cuando viene en formato de texto plano");
						//CAlert::InformationAlert("A9");
						this->ActualizaElementoNotaDsdDB(UIDModelString,"Titulo",texto, IsCheckOut ,document, fBNotesStore);
						//CAlert::InformationAlert("A10");
					}
					else
					{
						//CAlert::InformationAlert("si se pudo");
						//convierte el texto tageado a texto plano
						//CAlert::InformationAlert("A11");
						
						
						bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
						//if(sa)
						//{
							//Como de todas formas no se actualizo el elemento de la nota de todas formas eliminamos el 
							//item que se deseaba reemplazar para no sobrepasar memoria
							InterfacePtr<IScrapItem> frameScrap(itemToPlace, IID_ISCRAPITEM);
							// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
							InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
							// Process the DeleteFrameCmd:
							if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
							{	
								ASSERT_FAIL(" No pudo reemplazar ");
							}
						//}
						//CAlert::InformationAlert("A12");
		
						
						
						
						//CAlert::InformationAlert("A13");
					}
					
					//CAlert::InformationAlert("A14");
					
				}
				//CAlert::InformationAlert("A15");
			}
			
			//CAlert::InformationAlert("A16");
			//CAlert::InformationAlert("hizo bien todod");
			this->CambiaTipoDEstadoDElementoNota(UIDModelString,Label,kTrue);
			
			
			//CAlert::InformationAlert("A16");
		}
			
		
		
		/////Para Elemento Balazo de la Nota	
		if(VectorNota[numReg].UIDFrBalazoNote>0)
		{	
			
			UIDModelString="";
			UIDModelString.AppendNumber(VectorNota[numReg].UIDFrBalazoNote);
			
			if(PreguntaSiSeEstaEditandoElementoDNota(UIDModelString,document) && IsCheckOut==kFalse)//si regresa verdadero quiere decir que se esta editando la nota
			{					//Esto es para evitar hacer consulta a la base de datos y traernos el contenido de una nota
				break;
			}
			
			
			texto = N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, VectorNota[numReg].Id_Elem_Balazo_Note, TraesElementosConPases);//SQLInterface->ReturnItemContentbyNameColumn("Balazo",QueryVector[j]);
			if(UIDModelString.NumUTF16TextChars()>0)
			{
				if(texto.NumUTF16TextChars()>0)
				{
					
					if (document == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota document is invalid");
						break;
					}
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota db is invalid");	
						break;
					}
					
					UID uidStory=UIDModelString.GetAsNumber();
					///////////////////Obtener texto de Texframe
					UIDRef graphicFrame(db,uidStory);
					
					K2Vector<N2PNotesStorage*> fBNotesStoretexto;
					UIDRef itemToPlace = N2PSQLUtilities::ImportTaggedTextOfPMString(texto, fBNotesStoretexto);
					if(itemToPlace == UIDRef::gNull)
					{
						/////////////////////////////////////////////
						//Para cuando viene en formato de texto plano
						/////////////////////////////////////////////
						this->ActualizaElementoNotaDsdDB(UIDModelString,"Titulo",texto, IsCheckOut, document , fBNotesStoretexto);
						
					}
					else
					{
						bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
						//if(sa)
						//{
							//Como de todas formas no se actualizo el elemento de la nota de todas formas eliminamos el 
							//item que se deseaba reemplazar para no sobrepasar memoria
							InterfacePtr<IScrapItem> frameScrap(itemToPlace, IID_ISCRAPITEM);
							// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
							InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
							// Process the DeleteFrameCmd:
							if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
							{	
								ASSERT_FAIL(" No pudo reemplazar ");
							}
						//}
						
						
					}
					
				}
				//this->ApplicaColumnBreakSiNecesita(UIDModelString);
			
			}
			this->CambiaTipoDEstadoDElementoNota(UIDModelString,Label,kTrue);
		}
		
	
		/////Para Elemento PieFoto de la Nota
		if(VectorNota[numReg].UIDFrPieFotoNote>0)
		{
			//CAlert::InformationAlert("C");
			UIDModelString="";
			UIDModelString.AppendNumber(VectorNota[numReg].UIDFrPieFotoNote);
			
			if(PreguntaSiSeEstaEditandoElementoDNota(UIDModelString,document) && IsCheckOut==kFalse)//si regresa verdadero quiere decir que se esta editando la nota
			{					//Esto es para evitar hacer consulta a la base de datos y traernos el contenido de una nota
				break;
			}
			
			
			texto = N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, VectorNota[numReg].Id_Elem_PieFoto_Note, TraesElementosConPases);//=SQLInterface->ReturnItemContentbyNameColumn("PieFoto",QueryVector[j]);
			if(UIDModelString.NumUTF16TextChars()>0)
			{
				if(texto.NumUTF16TextChars()>0)
					{
					
					if (document == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota document is invalid");
						break;
					}
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota db is invalid");	
						break;
					}
					
					UID uidStory=UIDModelString.GetAsNumber();
					///////////////////Obtener texto de Texframe
					UIDRef graphicFrame(db,uidStory);
					
					K2Vector<N2PNotesStorage*> fBNotesStoretexto;
					UIDRef itemToPlace = N2PSQLUtilities::ImportTaggedTextOfPMString(texto, fBNotesStoretexto);
					if(itemToPlace == UIDRef::gNull)
					{
						/////////////////////////////////////////////
						//Para cuando viene en formato de texto plano
						/////////////////////////////////////////////
						this->ActualizaElementoNotaDsdDB(UIDModelString,"Titulo",texto, IsCheckOut , document, fBNotesStoretexto);
						
					}
					else
					{
						bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
						//if(sa)
						//{
							//Como de todas formas no se actualizo el elemento de la nota de todas formas eliminamos el 
							//item que se deseaba reemplazar para no sobrepasar memoria
							InterfacePtr<IScrapItem> frameScrap(itemToPlace, IID_ISCRAPITEM);
							// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
							InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
							// Process the DeleteFrameCmd:
							if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
							{	
								ASSERT_FAIL(" No pudo reemplazar ");
							}
						//}
						
					}
					
				}
				//this->ApplicaColumnBreakSiNecesita(UIDModelString);
		
			}
			this->CambiaTipoDEstadoDElementoNota(UIDModelString,Label,kTrue);
		}
		
		
		/////Para Elemento Contenido de la Nota
		if(VectorNota[numReg].UIDFrContentNote>0)
		{
			//CAlert::InformationAlert("D");
			UIDModelString="";
			UIDModelString.AppendNumber(VectorNota[numReg].UIDFrContentNote);//UIDModelString=SQLInterface->ReturnItemContentbyNameColumn("IDTextModelDeContenidoNota",QueryVector[j]);
			
			if(PreguntaSiSeEstaEditandoElementoDNota(UIDModelString,document) && IsCheckOut==kFalse)//si regresa verdadero quiere decir que se esta editando la nota
			{					//Esto es para evitar hacer consulta a la base de datos y traernos el contenido de una nota
				break;
			}


			//CAlert::InformationAlert("D 1");
			texto = N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, VectorNota[numReg].Id_Elem_Cont_Note, TraesElementosConPases);//= SQLInterface->ReturnItemContentbyNameColumn("ContentNota",QueryVector[j]);
			
			//CAlert::InformationAlert("Texto"+texto);
			if(UIDModelString.NumUTF16TextChars()>0)
			{	
				if(texto.NumUTF16TextChars()>0)
				{
					
					if (document == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota document is invalid");
						break;
					}
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
			
						ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota db is invalid");	
						break;
					}
					
					UID uidStory=UIDModelString.GetAsNumber();
					///////////////////Obtener texto de Texframe
					UIDRef graphicFrame(db,uidStory);
					
					K2Vector<N2PNotesStorage*> fBNotesStoretexto;
					//CAlert::InformationAlert("D 3");
					UIDRef itemToPlace = N2PSQLUtilities::ImportTaggedTextOfPMString(texto, fBNotesStoretexto);
					
					//this->GuardarQuery("Chingadamadre con esto :: "+texto);
					//CAlert::InformationAlert("D 4");
					if(itemToPlace == UIDRef::gNull)
					{
						//CAlert::InformationAlert("D 15");
						
						/////////////////////////////////////////////
						//Para cuando viene en formato de texto plano
						/////////////////////////////////////////////
						this->ActualizaElementoNotaDsdDB(UIDModelString,"Titulo",texto, IsCheckOut , document, fBNotesStoretexto);
						
					}
					else
					{
						bool16 sa = N2PSQLUtilities::ProcessCopyStoryCmd(itemToPlace, graphicFrame, document);
						//if(sa)
						//{
							//Como de todas formas no se actualizo el elemento de la nota de todas formas eliminamos el 
							//item que se deseaba reemplazar para no sobrepasar memoria
							InterfacePtr<IScrapItem> frameScrap(itemToPlace, IID_ISCRAPITEM);
							// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
							InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
							// Process the DeleteFrameCmd:
							if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
							{	
								ASSERT_FAIL(" No pudo reemplazar ");
							}
						//}
					}
					
				}
					
				
			}
			this->CambiaTipoDEstadoDElementoNota(UIDModelString,Label,kTrue);	
		}
		
	
		
		//
		this->Fotos_ActualizaGuiaDNota(document,GuiaDNota, VectorNota[numReg].ID_Elemento_padre);
		
		//Reemplaza el ultimo ckeckin en la nota
		if(UltimoCheckIn.NumUTF16TextChars()>0)
		{
			//CAlert::InformationAlert(VectorNota[numReg].ID_Elemento_padre+",   "+UltimoCheckIn);
			N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[numReg].ID_Elemento_padre,UltimoCheckIn, document);
		}
			
	}while(false);	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota fin");
}


bool16 N2PsqlUpdateStorysAndDBUtils::InsertBreakColumnCmd(const UIDRef storyUIDRef, const TextIndex finishIndex )
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::InsertBreakColumnCmd ini");
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<ICommand> breakCommand(CmdUtils::CreateCommand(kInsertBreakCharacterCmdBoss));
   		ASSERT(breakCommand);
     	if(breakCommand == nil) 
         {
            break;
         }
        
         breakCommand->SetItemList(UIDList(storyUIDRef));
         									
         InterfacePtr<IRangeData> rangeData(breakCommand, UseDefaultIID());
         ASSERT(rangeData);
         if(rangeData == nil) 
         {
             break;
         }
         rangeData->Set(finishIndex-1,finishIndex-1);
         									
         InterfacePtr<IIntData> intData(breakCommand,UseDefaultIID());
         ASSERT(intData);
         if(intData == nil) 
         {
             break;
         }
         									
         intData->Set(Text::kAtColumn);
         CmdUtils::ProcessCommand(breakCommand);
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::InsertBreakColumnCmd Fin");
	return(retval);
}

void N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB(PMString UIDModelString,PMString ItemToSearchString,PMString Text, bool16 isCheckOutNota, IDocument* document, const K2Vector<N2PNotesStorage*>& fBNotesStore)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB Ini");

	do
	{
	
		
		
				
		PMString Consulta="";
	
		//////obtener Uid del TextModel
		int32 intthis=UIDModelString.GetAsNumber();
		UID UIDTextModel=intthis;
				
		/////////////////////Obtener Base de taso para el Textmodel
		
		if (document == nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB document is invalid");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB db is invalid");	
			break;
		}
		///////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB mytextmodel is invalid");	
			break;
		}
		
		///
		InterfacePtr<IItemLockData> textLoctData(TextModelUIDRef, UseDefaultIID()); 
		if(textLoctData==nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB textLoctData is invalid");	
			break;
		}
		
		
		bool16 VeniaBlockeado=kFalse;
		if(textLoctData->GetInsertLock())
		{
		
			UIDList ListFramer=UIDList(TextModelUIDRef);
			// Create a SetLockPositionCmd:
			InterfacePtr<ICommand>	setLPCmd(CmdUtils::CreateCommand(kSetItemLockDataCmdBoss));
			if(setLPCmd==nil)
			{
				break;
			}
		
			// Set the SetLockPositionCmd's ItemList:
			setLPCmd->SetItemList(ListFramer);
			// Get an IIntData Interface for the SetLockPositionCmd:
			InterfacePtr<IBoolData> boolData(setLPCmd, IID_IBOOLDATA);
			if(boolData==nil)
			{
				break;
			}
			// Set the IIntData Interface's data:
			boolData->Set(kFalse);
			// Process the SetLockPositionCmd:
			if (CmdUtils::ProcessCommand(setLPCmd) != kSuccess)
			{
				//ASSERT_FAIL("Can't process SetLockPositionCmd");
			}
			VeniaBlockeado=kTrue;
			
		}
		else
		{
			//si No se encuentra bloqueda este elemento de la nota no puede
			//hacer update pues se perderia los datos que se han realizado sobre el antes de realizar el update.
			if(isCheckOutNota==kFalse)// a menos que se haya habilitado la nota para dar checkout
			{
				VeniaBlockeado=kFalse;
				//CAlert::InformationAlert("Venia bloqueado");
				//break;
			}
			
		}
			
		TextIndex index=0;
		
			
		/////////Delete Text
		 if(mytextmodel->TotalLength()>0)
		 {
		 	InterfacePtr<ITextModelCmds> textModelCmds(mytextmodel, UseDefaultIID());
			ASSERT(textModelCmds);
			if (!textModelCmds) 
			{
				
				ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB textModelCmds is invalid");	
				break;
			}
			InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(index, mytextmodel->TotalLength()-1));
			ASSERT(deleteCmd);
			if (deleteCmd) 
			{
				CmdUtils::ProcessCommand(deleteCmd);
			}
			else
			{
				
				ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB deleteCmd is invalid");
			}	
		 }
		 
		 
		 
		 //Coloca texto sobre textFrame
		this->ColocaTextYAplyStyleATextFrame(mytextmodel,ItemToSearchString,Text);
		///////////////
		//Adiciona texto de la nota y la convierte en Nota
		Utils<IN2PNotesUtils>()->AddNotesToElementArticle(mytextmodel, fBNotesStore);
		
		
		if(VeniaBlockeado==kTrue)
		{	
			//si el usuario no estaba editando la nota se trae el contenido de la nota.
			
			
			UIDList ListFramer=UIDList(TextModelUIDRef);
			// Create a SetLockPositionCmd:
			InterfacePtr<ICommand>	setLPCmd(CmdUtils::CreateCommand(kSetItemLockDataCmdBoss));
			if(setLPCmd==nil)
			{
				break;
			}
			// Set the SetLockPositionCmd's ItemList:
			setLPCmd->SetItemList(ListFramer);
			// Get an IIntData Interface for the SetLockPositionCmd:
			InterfacePtr<IBoolData> boolData(setLPCmd, IID_IBOOLDATA);
			if(boolData==nil)
			{
				break;
			}
			// Set the IIntData Interface's data:
			boolData->Set(kTrue);
			// Process the SetLockPositionCmd:
			if (CmdUtils::ProcessCommand(setLPCmd) != kSuccess)
			{
				ASSERT_FAIL("Can't process SetLockPositionCmd");
			}
		}
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB fin");
}









bool16 N2PsqlUpdateStorysAndDBUtils::EstadoDeNotasCompleto(PMString EstatusEnCompleto)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB ini");
	
	bool16 retval=kTrue;
	int32 IndexString=-1;
	bool16 visibility=kFalse;
	do
	{	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
			
		
		int32 contUpdate=0;
		PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		if(CadenaCompleta.NumUTF16TextChars()>0)
		{

			contUpdate=N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
				
			ClassRegEnXMP *ListUpdate= new ClassRegEnXMP[contUpdate];
			N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
				
			for(int32 i=0;i<contUpdate;i++)
			{
					
				int32 intthis=ListUpdate[i].IDFrame;

				UID UIDTextModel=intthis;
								
				//////////////////////Obtener Base de taso para el Textmodel
				IDataBase* db = ::GetDataBase(document);
				if (db == nil)
				{
					ASSERT_FAIL("db is invalid");
					break;
				}
				////////////////////Obtener texto de Texframe
				UIDRef TextModelUIDRef(db,UIDTextModel);
				InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
				if(mytextmodel==nil)
				{
					continue;
				}
							
				IFrameList* frameList= mytextmodel->QueryFrameList();
				
				 
				UIDRef refFrame;
				for(int32 Framecount=0 ; Framecount<frameList->GetFrameCount() ; Framecount++)
				{
					
					InterfacePtr<ITextFrameColumn> textFrame(frameList->QueryNthFrame(Framecount), UseDefaultIID());
					if(textFrame==nil)
					{
						ASSERT_FAIL("textFrame pointer nil");
						continue;
					}
					
					
					SDKLayoutHelper layoutHelper;
					UIDRef sourceFrameRef = layoutHelper.GetGraphicFrameRef(textFrame); 
					
					
					
					InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
					(
						kN2PAdIssueUtilsBoss,	// Object boss/class
						IN2PAdIssueUtils::kDefaultIID
					)));
					
					if(N2PAdIssueUtil==nil)
					{
						
						break;
					}
					
					UIDList ListFrame=UIDList(sourceFrameRef);
					PMString theLabel="";
					PMString EstatusEnLabel="";
					const PMString strintofind="/";
					N2PAdIssueUtil->GetFrameLabelAndVisibilityUIDListLabelfor(ListFrame, theLabel, visibility);
					
					
					
					if(theLabel.IndexOfString(strintofind,0)>0)
					{
					
						EstatusEnLabel=theLabel.GetItem(strintofind,2)->GrabCString();
					}
					else
					{
						EstatusEnLabel=theLabel;
					}
					
					
					PMString EstadoEnMayuscyula=EstatusEnLabel;
					EstadoEnMayuscyula.ToUpper();
					EstatusEnCompleto.ToUpper();
					//CAlert::InformationAlert(EstatusEnLabel+", "+EstadoEnMayuscyula);//EstatusEnLabel!=EstatusEnCompleto &&
					if(  EstadoEnMayuscyula!=EstatusEnCompleto)
					{
						retval=kFalse;
						i=contUpdate;
						break;
					}
				}
			}
			//delete ListUpdate;
		}
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB fin");
	return(retval);
}








int32 N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame(InterfacePtr<ITextModel> iTextModel,PMString NomStyle, PMString Texto)
{
	int32 UIDTexModelOfMyFrame=0;
	
	do
	{

		/*IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame document");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame db");
			break;
		}*/

		WideString * inserthis= new WideString(Texto);
		
		
	/////////////////////////
		//Obtiene el ID del TextModel
		PMString f="";
		IFrameList* Listffame=iTextModel->QueryFrameList();
		UID UIDTextModel=Listffame->GetTextModelUID();
		UIDTexModelOfMyFrame=UIDTextModel.Get();
		
	////////////////////////
		if(iTextModel->Insert( 0, inserthis)!=kSuccess)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame Insert");
			break;
		}
		
		////////////////////////////
		//PARA IMPORTAR TEXTO AGEADO
		/////////////////////////////
		
		
		/////////////////////////
	/*	InterfacePtr<IDocument> document(documentUIDRef, UseDefaultIID()); 
		UIDRef textModelRef = ::GetUIDRef(textModel); 
		InterfacePtr<ITextTarget> targetboss ((ITextTarget*)::CreateObject(kTextSuiteBoss, IID_ITEXTTARGET)); 
		targetboss->SetTextUnmanaged(textModelRef, RangeData(Start, Ende)); 
		InterfacePtr<IK2ServiceRegistry> registry(GetExecutionContextSession(), UseDefaultIID()); 
		InterfacePtr<IK2ServiceProvider> service(registry->QueryServiceProviderByClassID(kExportProviderService, kTaggedTextExportFilterBoss)); 
		InterfacePtr<IExportProvider> export(service, UseDefaultIID()); 
		IRXferBytes irXferBytes; 
		InterfacePtr<IPMStream> stream(StreamUtil::CreateMemoryStreamWrite(&irXferBytes)); 
		stream->Open(); 
		export->ExportToStream(stream, document, targetboss, "InDesign Tagged Text", kSuppressUI); 
		stream->Flush(); 
		PMString taggedText = ""; 
		irXferBytes.Seek(0, kSeekFromStart); 
		PMString taggedTemp; 
		char temp[256]; 
		bool crlf = false; 
		while (irXferBytes.GetStreamState() == kStreamStateGood) 
		{ 
		   uint32 anzahl = irXferBytes.Read(temp, 255); 
		   temp[anzahl] = 0; 
		   taggedTemp.SetCString(temp, PMString::kNoTranslate); 
		   taggedText.Append(taggedTemp); 
		} 
		stream->Close();
		
		//////////////////////////
		InterfacePtr<IExportProvider> iptrExportProvider (::CreateObject(kTaggedTextExportFilterBoss) ,IID_IEXPORTPROVIDER); 
		if (iptrExportProvider == nil) 
break; 
if (iptrExportProvider->CanExportToFile () == kTrue) 
iptrExportProvider->ExportToFile (sysFile,iptrDocument,iptrFromTextModel,"",kSuppressUI); 
		///////////////////////// */
		
		
		


		
	/*	InterfacePtr<ITextModelCmds> iTextModelCmd(iTextModel,UseDefaultIID());
		if(iTextModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}
		

		///insertar texto
		const bool16 copyString=kFalse;
		
	
		//aplicar Estilo
		InterfacePtr<IWorkspace> theWS(::QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}

	
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);
		if(ParaStyleNameTable==nil)
		{
			break;
		}
	
		UID myStyleUID=ParaStyleNameTable->FindByName(NomStyle);
		if(myStyleUID==nil)
		{
			break;
		}

		InterfacePtr<ICommand> applyCmd(iTextModelCmd->ApplyStyleCmd(0,iTextModel->TotalLength(), myStyleUID,kParaAttrStrandBoss));
		if(applyCmd==nil)
		{
			break;
		}

		if(CmdUtils::ProcessCommand(applyCmd)!=kSuccess)
		{
			break;
		}*/
	}while(false);
	return(UIDTexModelOfMyFrame);
}


bool16 N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita(PMString UIDModelString)
{
	
	
	do
	{
		PMString Consulta="";
		//////obtener Uid del TextModel
		int32 intthis=UIDModelString.GetAsNumber();
		UID UIDTextModel=intthis;
				
		/////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita document is invalid");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita db is invalid");	
			break;
		}
		///////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		InterfacePtr<ITextModel> iTextModel(TextModelUIDRef, UseDefaultIID()); 
		if(iTextModel==nil)
		{
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita mytextmodel is invalid");	
			break;
		}
		
		///
		InterfacePtr<IItemLockData> textLoctData(TextModelUIDRef, UseDefaultIID()); 
		if(textLoctData==nil)
		{
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita textLoctData is invalid");	
			break;
		}
		
		
		bool16 VeniaBlockeado=kFalse;
		if(textLoctData->GetAttributeLock())
		{
		
			UIDList ListFramer=UIDList(TextModelUIDRef);
			// Create a SetLockPositionCmd:
			InterfacePtr<ICommand>	setLPCmd(CmdUtils::CreateCommand(kSetItemLockDataCmdBoss));
			if(setLPCmd==nil)
			{
				break;
			}
			// Set the SetLockPositionCmd's ItemList:
			setLPCmd->SetItemList(ListFramer);
			// Get an IIntData Interface for the SetLockPositionCmd:
			InterfacePtr<IBoolData> boolData(setLPCmd, IID_IBOOLDATA);
			if(boolData==nil)
			{
				break;
			}
			// Set the IIntData Interface's data:
			boolData->Set(kFalse);
			// Process the SetLockPositionCmd:
			if (CmdUtils::ProcessCommand(setLPCmd) != kSuccess)
			{
				ASSERT_FAIL("Can't process SetLockPositionCmd");
			}
			VeniaBlockeado=kTrue;
			
		}
		else
		{
			//si No se encuentra bloqueda este elemento de la nota no puede
			//hacer update pues perdedia los datos que se han realizado sobre el antes de realizar el update.
			VeniaBlockeado=kFalse;
			break;
		}
		
		
		
		InterfacePtr<IComposeScanner> cs(iTextModel, UseDefaultIID()); 
			// Setup a variable to retrieve the length of the text 
			if(cs==nil)
			{
				break;
			}
			// Setup a variable to retrieve the length of the text 
			int32 length = iTextModel->TotalLength();//obtengo el total de caracteres de la cadena en el textframe
			
			WideString Temp;
			cs->CopyText(0, length-1, &Temp);
			// Setup a PMString to hold the text (depending on what you want to do with the text, the textchar may be ok) 
			// Load the text into the buf variable 
			//buf.SetXString(buffer, length); 
			PMString texto="";
			texto.Append(Temp);
			
			TextIndex index = texto.LastIndexOfWChar(13);
			
			while(index>0)
			{	
				PMString numindex="";
				numindex.AppendNumber(index);
				
				
				InterfacePtr<ITextModelCmds> textModelCmds(iTextModel, UseDefaultIID());
				ASSERT(textModelCmds);
				if (!textModelCmds) 
				{
					
					ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita textModelCmds is invalid");	
					break;
				}
				InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(index,2));
				ASSERT(deleteCmd);
				if (!deleteCmd) 
				{
					
					ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ApplicaColumnBreakSiNecesita deleteCmd is invalid");
					break;
				}
				CmdUtils::ProcessCommand(deleteCmd);
				
				texto.Remove(index,1);
			
				UIDRef storyUIDRef(::GetUIDRef(iTextModel));		
				InsertBreakColumnCmd(storyUIDRef, index );
				
				index = texto.LastIndexOfWChar(13);
			}
			
		
		///////////////

		if(VeniaBlockeado==kTrue)
		{	
			//si el usuario no estaba editando la nota se trae el contenido de la nota.
			
			
			UIDList ListFramer=UIDList(TextModelUIDRef);
			// Create a SetLockPositionCmd:
			InterfacePtr<ICommand>	setLPCmd(CmdUtils::CreateCommand(kSetItemLockDataCmdBoss));
			if(setLPCmd==nil)
			{
				break;
			}
			// Set the SetLockPositionCmd's ItemList:
			setLPCmd->SetItemList(ListFramer);
			// Get an IIntData Interface for the SetLockPositionCmd:
			InterfacePtr<IBoolData> boolData(setLPCmd, IID_IBOOLDATA);
			if(boolData==nil)
			{
				break;
			}
			// Set the IIntData Interface's data:
			boolData->Set(kTrue);
			// Process the SetLockPositionCmd:
			if (CmdUtils::ProcessCommand(setLPCmd) != kSuccess)
			{
				ASSERT_FAIL("Can't process SetLockPositionCmd");
			}
		}
	
	}while(false);
	
	return(kTrue);
}








bool16 N2PsqlUpdateStorysAndDBUtils::GetDefaultPreferencesConnection(PreferencesConnection &Connection,bool16 RefreshPreferencias)
{
	do
	{
		
		if(RefreshPreferencias==kTrue)
		{
			if(!this->GetPreferencesConnectionForNameConection("Default", Connection))
				return kFalse;
		}
		else
		{
			if(PreferenciasEstaticas.DSNNameConnection.NumUTF16TextChars()<=0)
			{
				//CAlert::InformationAlert("AAA");
				if(!this->GetPreferencesConnectionForNameConection("Default", Connection))
					return kFalse;
			}
			else
			{
				//CAlert::InformationAlert("BBB");
				//Connection=PreferenciasEstaticas;
				//Connection.TypeConnection=PreferenciasEstaticas.TypeConnection;
				//Connection.IPServerConnection=PreferenciasEstaticas.IPServerConnection;
				Connection.NameConnection=PreferenciasEstaticas.NameConnection;
				Connection.DSNNameConnection=PreferenciasEstaticas.DSNNameConnection;
				Connection.NameUserDBConnection=PreferenciasEstaticas.NameUserDBConnection;
				Connection.PwdUserDBConnection=PreferenciasEstaticas.PwdUserDBConnection;
				Connection.PathOfServerFile=PreferenciasEstaticas.PathOfServerFile;
				Connection.PathOfServerFileImages=PreferenciasEstaticas.PathOfServerFileImages;
				Connection.ImagCarpInServFImages=PreferenciasEstaticas.ImagCarpInServFImages;
				
				Connection.RutaDServidorRespNotas=PreferenciasEstaticas.RutaDServidorRespNotas;
				Connection.URLWebServices=PreferenciasEstaticas.URLWebServices;
				Connection.TimeToCheckUpdateElements=PreferenciasEstaticas.TimeToCheckUpdateElements;
				Connection.N2PResizeHeight=PreferenciasEstaticas.N2PResizeHeight;
				Connection.N2PResizeWidth=PreferenciasEstaticas.N2PResizeWidth;
				
				
				Connection.LlenarTablaWEB=PreferenciasEstaticas.LlenarTablaWEB;
				Connection.EstadoParaExportarATWEB=PreferenciasEstaticas.EstadoParaExportarATWEB;
				Connection.NombreEstatusParaNotasCompletas=PreferenciasEstaticas.NombreEstatusParaNotasCompletas;
				Connection.ExportarPaginaComoPDF=PreferenciasEstaticas.ExportarPaginaComoPDF;
				Connection.RutaDePaginaComoPDF=PreferenciasEstaticas.RutaDePaginaComoPDF;
				Connection.CreateBackUpPagina=PreferenciasEstaticas.CreateBackUpPagina;
				Connection.EsCampoGuiaObligatorio=PreferenciasEstaticas.EsCampoGuiaObligatorio;
				Connection.EsEnviarNotasHijasAWEB=PreferenciasEstaticas.EsEnviarNotasHijasAWEB;
				Connection.EnviaAWEBEnAltasPieFoto=PreferenciasEstaticas.EnviaAWEBEnAltasPieFoto;
				Connection.EnviaAWEBSinRetornosTitulo=PreferenciasEstaticas.EnviaAWEBSinRetornosTitulo;
				Connection.ExportPDFPresets=PreferenciasEstaticas.ExportPDFPresets;
				Connection.ExportPDFPresetsEditWEB=PreferenciasEstaticas.ExportPDFPresetsEditWEB;
				Connection.PresetTOPDFEditorial=PreferenciasEstaticas.PresetTOPDFEditorial;
				Connection.PresetTOPDFEditorialWEB=PreferenciasEstaticas.PresetTOPDFEditorialWEB;
				Connection.RutaDePaginaComoPDFWEB=PreferenciasEstaticas.RutaDePaginaComoPDFWEB;
				Connection.nameParaStyleCredito=PreferenciasEstaticas.nameParaStyleCredito;
				
				Connection.N2PHmteSendToHmca=PreferenciasEstaticas.N2PHmteSendToHmca;
				Connection.N2PHmeDSNName=PreferenciasEstaticas.N2PHmeDSNName;
				Connection.N2PHmeDSNUserLogin=PreferenciasEstaticas.N2PHmeDSNUserLogin;
				Connection.N2PHmeDSNPwdLogin=PreferenciasEstaticas.N2PHmeDSNPwdLogin;
				Connection.N2PHmeFTPServer=PreferenciasEstaticas.N2PHmeFTPServer;
				Connection.N2PHmeFTPUser=PreferenciasEstaticas.N2PHmeFTPUser;
				Connection.N2PHmeFTPPwd=PreferenciasEstaticas.N2PHmeFTPPwd;
				Connection.N2PHmeFTPFolder=PreferenciasEstaticas.N2PHmeFTPFolder;
				
				Connection.N2PResizePreview=PreferenciasEstaticas.N2PResizePreview;
				Connection.N2PUtilizarGuias=PreferenciasEstaticas.N2PUtilizarGuias;
				Connection.URLWebServices=PreferenciasEstaticas.URLWebServices;
			}
		}
		
		
	}while(false);
	return kTrue;
}





bool16 N2PsqlUpdateStorysAndDBUtils::GetPreferencesConnectionForNameConection(PMString NameConnection, PreferencesConnection &Preferencia)
{
	bool16 retval=kTrue;
	FILE *ArchivoPref;
	//CAlert::InformationAlert("antes de crear las preferencias");
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	PathFile.Append(NameConnection + ".pfa");
	PMString PMSLinea="";
	PMString TMP="";
	PMString Copia="";
	char Linea[1000];
	int i=0;
	
	
	do
	{
		//CAlert::InformationAlert(PathFile);
		if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"r"))==NULL)//if((ArchivoPref=fopen(PathFile.GrabCString(),"r"))==NULL)
		{
			//CAlert::InformationAlert("no no");
			//Preferencia.TypeConnection="";
			//CAlert::InformationAlert("1");
			//Preferencia.IPServerConnection="";
			//CAlert::InformationAlert("2");
			Preferencia.NameConnection="";
			//CAlert::InformationAlert("3");
			Preferencia.DSNNameConnection="";
			//CAlert::InformationAlert("4");
			Preferencia.NameUserDBConnection="";
			//CAlert::InformationAlert("5");
			Preferencia.PwdUserDBConnection="";
			//CAlert::InformationAlert("6");
			Preferencia.PathOfServerFile="";
			//CAlert::InformationAlert("7");
			Preferencia.PathOfServerFileImages="";
			
			Preferencia.ImagCarpInServFImages="";
			Preferencia.URLWebServices="";
			
			
			//CAlert::InformationAlert("8");
			Preferencia.RutaDServidorRespNotas="";
			//CAlert::InformationAlert("9");
			Preferencia.TimeToCheckUpdateElements=0;
			Preferencia.N2PResizeHeight=0;
			Preferencia.N2PResizeWidth=0;
			///CAlert::InformationAlert("10");
			Preferencia.LlenarTablaWEB=0;
			//CAlert::InformationAlert("11");
			Preferencia.EstadoParaExportarATWEB="";
			//CAlert::InformationAlert("12");
			Preferencia.NombreEstatusParaNotasCompletas="";
			//CAlert::InformationAlert("13");
			Preferencia.ExportarPaginaComoPDF=0;
			//CAlert::InformationAlert("14");
			Preferencia.RutaDePaginaComoPDF="";
			//CAlert::InformationAlert("15");
			Preferencia.CreateBackUpPagina=0;
			//CAlert::InformationAlert("16");
			Preferencia.EsCampoGuiaObligatorio=0;
			//CAlert::InformationAlert("17");
			Preferencia.EsEnviarNotasHijasAWEB=0;
			//CAlert::InformationAlert("18");
			Preferencia.EnviaAWEBEnAltasPieFoto=0;
			//CAlert::InformationAlert("19");
			Preferencia.EnviaAWEBSinRetornosTitulo=0;
			//CAlert::InformationAlert("20");
			Preferencia.ExportPDFPresets=0;
			//			//T>CAlert::InformationAlert("21");
			Preferencia.ExportPDFPresetsEditWEB=0;
			//			//T>CAlert::InformationAlert("22");
			Preferencia.PresetTOPDFEditorialWEB="";
			//			//T>CAlert::InformationAlert("23");
			Preferencia.PresetTOPDFEditorial="";
			//			//T>CAlert::InformationAlert("24");
			Preferencia.nameParaStyleCredito="";
			
			Preferencia.N2PHmteSendToHmca=0;
			
			Preferencia.N2PResizePreview=0;
			Preferencia.N2PUtilizarGuias=0;
			//CAlert::InformationAlert("si si");
			retval=kFalse;
			break;
		}
		else
		{
			//CAlert::InformationAlert("si entro aqui");
			Preferencia.TimeToCheckUpdateElements=0;
			Preferencia.N2PResizeHeight=0;
			Preferencia.N2PResizeWidth=0;
			Preferencia.LlenarTablaWEB=0;
			Preferencia.ExportarPaginaComoPDF=0;
			Preferencia.CreateBackUpPagina=0;
			Preferencia.EsCampoGuiaObligatorio=0;
			Preferencia.EsEnviarNotasHijasAWEB=0;
			Preferencia.EnviaAWEBEnAltasPieFoto=0;
			Preferencia.EnviaAWEBSinRetornosTitulo=0;
			Preferencia.ExportPDFPresets=0;
			Preferencia.ExportPDFPresetsEditWEB=0;
			Preferencia.N2PHmteSendToHmca=0;
			Preferencia.N2PResizePreview=0;
			Preferencia.N2PUtilizarGuias=0;
			
			while(fgets(Linea,200,ArchivoPref)!=NULL)
			{
			
				PMSLinea=InterlasaUtilities::Converter(Linea);
				
				PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
				
				//CAlert::InformationAlert(PMSLinea+"!");
				//Copia=PMSLinea;
				if(PMSLinea.IndexOfWChar(59)==-1)
				{
					continue;
				}
				
				int32 carborrarOS=0;
#ifdef MACINTOSH
				if(PMSLinea.NumUTF16TextChars()==PMSLinea.IndexOfWChar(59))
				{
					continue;
				}
				
				carborrarOS=2;
				
#endif
				//PMString XSQW=PMSLinea;
				Copia =PMSLinea.Substring(0,PMSLinea.IndexOfWChar(59)+1) ->GrabCString();
				
				/*if(Copia=="TypeConnection;")
				 {
				 //CAlert::InformationAlert("TypeConnection");
				 PMSLinea.Remove(0,15);
				 PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
				 Preferencia.TypeConnection=PMSLinea;
				 }*/
				
				
				//Copia=PMSLinea;
				////Copia.Remove(15,Copia.NumUTF16TextChars());
				if(Copia=="NameConnection;")
				{
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.NameConnection=PMString(PMSLinea.GetItem(";",2)->GrabCString());
					continue;
				}
				
				//Copia=PMSLinea;
				////Copia.Remove(18,Copia.NumUTF16TextChars());
				if(Copia=="DSNNameConnection;")
				{
					Preferencia.DSNNameConnection=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				//Copia=PMSLinea;
				////Copia.Remove(21,Copia.NumUTF16TextChars());
				if(Copia=="NameUserDBConnection;")
				{
					Preferencia.NameUserDBConnection=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				//Copia=PMSLinea;
				////Copia.Remove(20,Copia.NumUTF16TextChars());
				if(Copia=="PwdUserDBConnection;")
				{//(PMSLinea.NumUTF16TextChars()-(PMSLinea.IndexOfWChar(59)+1) )- carborrarOS
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.PwdUserDBConnection=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				
				
				
				
				//Copia=PMSLinea;
				//Copia.Remove(17,Copia.NumUTF16TextChars());
				if(Copia=="PathOfServerFile;")
				{
					//CAlert::InformationAlert("PathOfServerFile");
					//PMSLinea.Remove(0,17);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					//if(PMSLinea.NumUTF16TextChars()-1==PMSLinea.LastIndexOfWChar(59))
					//	PMSLinea.Remove(PMSLinea.LastIndexOfWChar(59),1);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.PathOfServerFile=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea;
					
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(23,Copia.NumUTF16TextChars());
				if(Copia=="PathOfServerFileImages;")
				{
					//CAlert::InformationAlert("PathOfServerFileImages");
					//PMSLinea.Remove(0,23);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					//if(PMSLinea.NumUTF16TextChars()-1==PMSLinea.LastIndexOfWChar(59))
					//	PMSLinea.Remove(PMSLinea.LastIndexOfWChar(59),1);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.PathOfServerFileImages=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea;
					continue;
				}
				
				
				
				//Copia=PMSLinea;
				//Copia.Remove(23,Copia.NumUTF16TextChars());
				if(Copia=="RutaDServidorRespNotas;")
				{
					//CAlert::InformationAlert("RutaDServidorRespNotas");
					//PMSLinea.Remove(0,23);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					//if(PMSLinea.NumUTF16TextChars()-1==PMSLinea.LastIndexOfWChar(59))
					//	PMSLinea.Remove(PMSLinea.LastIndexOfWChar(59),1);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.RutaDServidorRespNotas=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea;
					continue;
				}
				
				
				//Copia=PMSLinea;
				//Copia.Remove(26,Copia.NumUTF16TextChars());
				if(Copia=="TimeToCheckUpdateElements;")
				{
					//CAlert::InformationAlert("TimeToCheckUpdateElements");
					//PMSLinea.Remove(0,26);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.TimeToCheckUpdateElements=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsDouble();//PMSLinea.GetAsDouble();
					continue;
				}
				
				if(Copia=="N2PResizeHeight;")
				{
					//CAlert::InformationAlert("N2PResizeHeight");
					//PMSLinea.Remove(0,16);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PResizeHeight=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsDouble();//PMSLinea.GetAsDouble();
					continue;
				}
				
				if(Copia=="N2PResizeWidth;")
				{
					//CAlert::InformationAlert("N2PResizeWidth");
					//PMSLinea.Remove(0,15);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PResizeWidth=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsDouble();//PMSLinea.GetAsDouble();
					continue;
				}
				
				
				//Copia=PMSLinea;
				//Copia.Remove(15,Copia.NumUTF16TextChars());
				if(Copia=="LlenarTablaWEB;")
				{
					//CAlert::InformationAlert("LlenarTablaWEB");
					//PMSLinea.Remove(0,15);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.LlenarTablaWEB=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(24,Copia.NumUTF16TextChars());
				if(Copia=="EstadoParaExportarATWEB;")
				{
					//CAlert::InformationAlert("EstadoParaExportarATWEB");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.EstadoParaExportarATWEB=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				
				//Copia=PMSLinea;
				//Copia.Remove(22,Copia.NumUTF16TextChars());
				if(Copia=="ExportarPaginaComoPDF;")
				{
					//CAlert::InformationAlert("ExportarPaginaComoPDF");
					//PMSLinea.Remove(0,22);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.ExportarPaginaComoPDF=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				
				//Copia=PMSLinea;
				//Copia.Remove(19,Copia.NumUTF16TextChars());
				if(Copia=="CreateBackUpPagina;")
				{
					//CAlert::InformationAlert("CreateBackUpPagina");
					//PMSLinea.Remove(0,19);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.CreateBackUpPagina=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				
				//Copia=PMSLinea;
				//Copia.Remove(23,Copia.NumUTF16TextChars());
				if(Copia=="EsCampoGuiaObligatorio;")
				{
					//CAlert::InformationAlert("EsCampoGuiaObligatorio");
					//PMSLinea.Remove(0,23);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.EsCampoGuiaObligatorio=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//MSLinea.GetAsNumber();
					continue;
				}
				//Copia=PMSLinea;
				//Copia.Remove(23,Copia.NumUTF16TextChars());
				if(Copia=="EsEnviarNotasHijasAWEB;")
				{
					//CAlert::InformationAlert("EsEnviarNotasHijasAWEB");
					//PMSLinea.Remove(0,23);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.EsEnviarNotasHijasAWEB=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(27,Copia.NumUTF16TextChars());
				if(Copia=="EnviaAWEBSinRetornosTitulo;")
				{
					//CAlert::InformationAlert("EnviaAWEBSinRetornosTitulo");
					//PMSLinea.Remove(0,27);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.EnviaAWEBSinRetornosTitulo=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(24,Copia.NumUTF16TextChars());
				if(Copia=="EnviaAWEBEnAltasPieFoto;")
				{
					//CAlert::InformationAlert("EnviaAWEBEnAltasPieFoto");
					//PMSLinea.Remove(0,24);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.EnviaAWEBEnAltasPieFoto=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				
				
				//Copia=PMSLinea;
				//Copia.Remove(24,Copia.NumUTF16TextChars());
				if(Copia=="ExportPDFPresetsEditWEB;")
				{
					//CAlert::InformationAlert("ExportPDFPresetsEditWEB");
					//PMSLinea.Remove(0,24);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.ExportPDFPresetsEditWEB=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				
				if(Copia=="N2PHmteSendToHmca;")
				{
					//CAlert::InformationAlert("N2PHmteSendToHmca");
					//PMSLinea.Remove(0,18);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmteSendToHmca=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				
				
				if(Copia=="N2PResizePreview;")
				{
					//CAlert::InformationAlert("N2PResizePreview");
					//PMSLinea.Remove(0,17);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PResizePreview=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				
				
				if(Copia=="N2PUtilizarGuias;")
				{
					/*PMSLinea.Remove(0,17);
					if (PMSLinea.IsEmpty()) 
					{
						Preferencia.N2PUtilizarGuias = 0;
					}
					else 
					{
						PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
						if (PMSLinea.IsEmpty()) {
							Preferencia.N2PUtilizarGuias = 0;
						}
						else {
							if (!PMSLinea.IsEmpty()) 
							{
								Preferencia.N2PUtilizarGuias=PMSLinea.GetAsNumber();
							}
							else
								Preferencia.N2PUtilizarGuias = 0;
						}
						
					}*/
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PUtilizarGuias=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//
					continue;
				}
				
				
				
				
				//Copia=PMSLinea;
				//Copia.Remove(17,Copia.NumUTF16TextChars());
				if(Copia=="ExportPDFPresets;")
				{
					//CAlert::InformationAlert("ExportPDFPresets");
					//PMSLinea.Remove(0,17);
					//PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					
					////CAlert::InformationAlert(PMSLinea);
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.ExportPDFPresets=PMString(PMSLinea.GetItem(";",2)->GrabCString()).GetAsNumber();//PMSLinea.GetAsNumber();
					continue;
				}
				//Copia=PMSLinea;
				//Copia.Remove(32,Copia.NumUTF16TextChars());
				if(Copia=="NombreEstatusParaNotasCompletas;")
				{
					//CAlert::InformationAlert("NombreEstatusParaNotasCompletas");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.NombreEstatusParaNotasCompletas=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(20,Copia.NumUTF16TextChars());
				if(Copia=="RutaDePaginaComoPDF;")
				{
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.RutaDePaginaComoPDF=PMString(PMSLinea.GetItem(";",2)->GrabCString());//
					//CAlert::InformationAlert("XX:"+PMSLinea+"XX");
					/*if(PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59)))!=nil)
					{
						
						Preferencia.RutaDePaginaComoPDF=PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					}
					else {
						Preferencia.RutaDePaginaComoPDF="";
					}*/
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(23,Copia.NumUTF16TextChars());
				if(Copia=="RutaDePaginaComoPDFWEB;")
				{
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.RutaDePaginaComoPDFWEB =PMString(PMSLinea.GetItem(";",2)->GrabCString());
					//CAlert::InformationAlert("Pta madreZ:"+PMSLinea+"Z");
					/*if(PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59)))!=nil)
					{
						Preferencia.RutaDePaginaComoPDFWEB = PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					}
					else {
						Preferencia.RutaDePaginaComoPDFWEB="";
					}*/
					continue;
				}
				
				if(Copia=="N2PHmeDSNName;")
				{
					//CAlert::InformationAlert("N2PHmeDSNName");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeDSNName=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				if(Copia=="N2PHmeDSNUserLogin;")
				{
					//CAlert::InformationAlert("N2PHmeDSNUserLogin");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeDSNUserLogin=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				if(Copia=="N2PHmeDSNPwdLogin;")
				{
					//CAlert::InformationAlert("N2PHmeDSNPwdLogin");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeDSNPwdLogin=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				if(Copia=="N2PHmeFTPServer;")
				{
					//CAlert::InformationAlert("N2PHmeFTPServer");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeFTPServer=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				if(Copia=="N2PHmeFTPUser;")
				{
					//CAlert::InformationAlert("N2PHmeFTPUser");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeFTPUser=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				if(Copia=="N2PHmeFTPPwd;")
				{
					//CAlert::InformationAlert("N2PHmeFTPPwd");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeFTPPwd=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				if(Copia=="N2PHmeFTPFolder;")
				{
					//CAlert::InformationAlert("N2PHmeFTPFolder");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.N2PHmeFTPFolder=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				
				
				//Copia=PMSLinea;
				//Copia.Remove(21,Copia.NumUTF16TextChars());
				if(Copia=="N2PParaStyleCredito;")
				{
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.nameParaStyleCredito =PMString(PMSLinea.GetItem(";",2)->GrabCString());
					/*CAlert::InformationAlert("MM:"+PMSLinea+"MM");
					if(PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59)))!=nil)
					{
						//CAlert::InformationAlert("Trae algo");
						Preferencia.nameParaStyleCredito = PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					}else
						Preferencia.nameParaStyleCredito ="";*/
					//CAlert::InformationAlert("nameParaStyleCredito");
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(21,Copia.NumUTF16TextChars());
				if(Copia=="PresetTOPDFEditorial;")
				{
					//CAlert::InformationAlert("PresetTOPDFEditorial");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.PresetTOPDFEditorial=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				//Copia=PMSLinea;
				//Copia.Remove(24,Copia.NumUTF16TextChars());
				if(Copia=="PresetTOPDFEditorialWEB;")
				{
					//CAlert::InformationAlert("PresetTOPDFEditorialWEB");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.PresetTOPDFEditorialWEB=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					continue;
				}
				
				//Copia=PMSLinea;
				
				if(Copia=="ImagCarpInServFImages;")
				{
					//CAlert::InformationAlert("ImagCarpInServFImages");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.ImagCarpInServFImages=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					
					/*PMSLinea.Remove(0,22);
					 PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					 if(PMSLinea.NumUTF16TextChars()-1==PMSLinea.LastIndexOfWChar(59))
					 PMSLinea.Remove(PMSLinea.LastIndexOfWChar(59),1);
					 Preferencia.ImagCarpInServFImages=PMSLinea;*/
					continue;
				}
				
				if(Copia=="URLWebServices;")
				{
					//CAlert::InformationAlert("URLWebServices");
					if(PMSLinea.GetItem(";",2)!=nil)
						Preferencia.URLWebServices=PMString(PMSLinea.GetItem(";",2)->GrabCString());//PMSLinea.Substring( PMSLinea.IndexOfWChar(59)+1, (PMSLinea.NumUTF16TextChars()-PMSLinea.IndexOfWChar(59))- carborrarOS)->GrabCString();
					//CAlert::InformationAlert("RutaDServidorRespNotas");
					/*PMSLinea.Remove(0,15);
					 PMSLinea.Remove(PMSLinea.NumUTF16TextChars()-1,1);
					 
					 if(PMSLinea.NumUTF16TextChars()-1==PMSLinea.LastIndexOfWChar(59))
					 PMSLinea.Remove(PMSLinea.LastIndexOfWChar(59),1);
					 Preferencia.URLWebServices=PMSLinea;*/
					continue;
				}
				
				
			}
			
			fclose(ArchivoPref);
			
			retval=kTrue;
		}
	}while(false);
	
	//CAlert::InformationAlert("se supone que ase copiabn");
	//PreferenciasEstaticas.TypeConnection=		Preferencia.TypeConnection;
	//PreferenciasEstaticas.IPServerConnection=	Preferencia.IPServerConnection;
	PreferenciasEstaticas.NameConnection=		Preferencia.NameConnection;
	PreferenciasEstaticas.DSNNameConnection=	Preferencia.DSNNameConnection;
	PreferenciasEstaticas.NameUserDBConnection=	Preferencia.NameUserDBConnection;
	PreferenciasEstaticas.PwdUserDBConnection=	Preferencia.PwdUserDBConnection;
	PreferenciasEstaticas.PathOfServerFile=		Preferencia.PathOfServerFile;
	PreferenciasEstaticas.PathOfServerFileImages=	Preferencia.PathOfServerFileImages;
	
	PreferenciasEstaticas.ImagCarpInServFImages=	Preferencia.ImagCarpInServFImages;
	PreferenciasEstaticas.URLWebServices=	Preferencia.URLWebServices;
	
	PreferenciasEstaticas.RutaDServidorRespNotas=	Preferencia.RutaDServidorRespNotas;
	PreferenciasEstaticas.TimeToCheckUpdateElements=	Preferencia.TimeToCheckUpdateElements;
	PreferenciasEstaticas.N2PResizeHeight=	Preferencia.N2PResizeHeight;
	PreferenciasEstaticas.N2PResizeWidth=	Preferencia.N2PResizeWidth;
	
	PreferenciasEstaticas.LlenarTablaWEB=	Preferencia.LlenarTablaWEB;
	PreferenciasEstaticas.EstadoParaExportarATWEB=	Preferencia.EstadoParaExportarATWEB;
	PreferenciasEstaticas.NombreEstatusParaNotasCompletas=Preferencia.NombreEstatusParaNotasCompletas;
	PreferenciasEstaticas.ExportarPaginaComoPDF=	Preferencia.ExportarPaginaComoPDF;
	PreferenciasEstaticas.RutaDePaginaComoPDF=		Preferencia.RutaDePaginaComoPDF;
	PreferenciasEstaticas.CreateBackUpPagina=	Preferencia.CreateBackUpPagina;
	PreferenciasEstaticas.EsCampoGuiaObligatorio=Preferencia.EsCampoGuiaObligatorio;
	PreferenciasEstaticas.EsEnviarNotasHijasAWEB=Preferencia.EsEnviarNotasHijasAWEB;
	PreferenciasEstaticas.EnviaAWEBEnAltasPieFoto=Preferencia.EnviaAWEBEnAltasPieFoto;
	PreferenciasEstaticas.EnviaAWEBSinRetornosTitulo=Preferencia.EnviaAWEBSinRetornosTitulo;
	PreferenciasEstaticas.ExportPDFPresets=Preferencia.ExportPDFPresets;
	PreferenciasEstaticas.ExportPDFPresetsEditWEB=Preferencia.ExportPDFPresetsEditWEB;
	PreferenciasEstaticas.PresetTOPDFEditorialWEB=Preferencia.PresetTOPDFEditorialWEB;
	PreferenciasEstaticas.PresetTOPDFEditorial=Preferencia.PresetTOPDFEditorial;
	PreferenciasEstaticas.nameParaStyleCredito=Preferencia.nameParaStyleCredito;
	PreferenciasEstaticas.RutaDePaginaComoPDFWEB=Preferencia.RutaDePaginaComoPDFWEB;
	
	PreferenciasEstaticas.N2PHmteSendToHmca=Preferencia.N2PHmteSendToHmca;
	PreferenciasEstaticas.N2PHmeDSNName=Preferencia.N2PHmeDSNName;
	PreferenciasEstaticas.N2PHmeDSNUserLogin=Preferencia.N2PHmeDSNUserLogin;
	PreferenciasEstaticas.N2PHmeDSNPwdLogin=Preferencia.N2PHmeDSNPwdLogin;
	PreferenciasEstaticas.N2PHmeFTPServer=Preferencia.N2PHmeFTPServer;
	PreferenciasEstaticas.N2PHmeFTPUser=Preferencia.N2PHmeFTPUser;
	PreferenciasEstaticas.N2PHmeFTPPwd=Preferencia.N2PHmeFTPPwd;
	PreferenciasEstaticas.N2PHmeFTPFolder=Preferencia.N2PHmeFTPFolder;
	
	PreferenciasEstaticas.N2PResizePreview=Preferencia.N2PResizePreview;
	PreferenciasEstaticas.N2PUtilizarGuias=Preferencia.N2PUtilizarGuias;
	
	
	
	//CAlert::InformationAlert("sale");
	return(retval);
}

bool16 N2PsqlUpdateStorysAndDBUtils::GuardarPreferencesConnectionForNameConection(PMString NameConnection, PreferencesConnection &Connection)
{
	bool16 retval=kFalse;
	FILE *ArchivoPref;
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	PathFile.Append(NameConnection+".pfa");
	
	if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"w"))!=NULL)
	{
		fprintf(ArchivoPref,"%s\n","Preferencias News2Page SQL ");
		//fprintf(ArchivoPref,"TypeConnection;%s\n",Connection.TypeConnection.GrabCString());
		//fprintf(ArchivoPref,"IPServerConnection;%s\n",Connection.IPServerConnection.GrabCString());
		
		fprintf(ArchivoPref,"NameConnection;%s\n",Connection.NameConnection.GrabCString());
		fprintf(ArchivoPref,"DSNNameConnection;%s\n",Connection.DSNNameConnection.GrabCString());
		fprintf(ArchivoPref,"NameUserDBConnection;%s\n",Connection.NameUserDBConnection.GrabCString());
		fprintf(ArchivoPref,"PwdUserDBConnection;%s\n",Connection.PwdUserDBConnection.GrabCString());
		
		fprintf(ArchivoPref,"PathOfServerFile;%s\n",Connection.PathOfServerFile.GrabCString());
		fprintf(ArchivoPref,"PathOfServerFileImages;%s\n",Connection.PathOfServerFileImages.GrabCString());
		fprintf(ArchivoPref,"ImagCarpInServFImages;%s\n",Connection.ImagCarpInServFImages.GrabCString());
		
		fprintf(ArchivoPref,"RutaDServidorRespNotas;%s\n",Connection.RutaDServidorRespNotas.GrabCString());
		fprintf(ArchivoPref,"URLWebServices;%s\n",Connection.URLWebServices.GrabCString());
		fprintf(ArchivoPref,"EstadoParaExportarATWEB;%s\n",Connection.EstadoParaExportarATWEB.GrabCString());
		
		
		PMString ASX="";
		ASX.AppendNumber(Connection.TimeToCheckUpdateElements);
		//CAlert::InformationAlert("Antes de Guardar valor:"+ASX);
		fprintf(ArchivoPref,"TimeToCheckUpdateElements;%s\n", ASX.GrabCString());
		
		ASX="";
		ASX.AppendNumber(Connection.N2PResizeHeight);
		//CAlert::InformationAlert("Antes de Guardar valor:"+ASX);
		fprintf(ArchivoPref,"N2PResizeHeight;%s\n", ASX.GrabCString());
		
		ASX="";
		ASX.AppendNumber(Connection.N2PResizeWidth);
		//CAlert::InformationAlert("Antes de Guardar valor:"+ASX);
		fprintf(ArchivoPref,"N2PResizeWidth;%s\n", ASX.GrabCString());
		
		 ASX="";
		ASX.AppendNumber(Connection.LlenarTablaWEB);
		fprintf(ArchivoPref,"LlenarTablaWEB;%s\n", ASX.GrabCString());
		
		ASX="";
		ASX.AppendNumber(Connection.ExportarPaginaComoPDF);
		fprintf(ArchivoPref,"ExportarPaginaComoPDF;%s\n", ASX.GrabCString());
		
		
		ASX="";
		ASX.AppendNumber(Connection.CreateBackUpPagina);
		fprintf(ArchivoPref,"CreateBackUpPagina;%s\n", ASX.GrabCString());
		
		
		ASX="";
		ASX.AppendNumber(Connection.EsCampoGuiaObligatorio);
		fprintf(ArchivoPref,"EsCampoGuiaObligatorio;%s\n", ASX.GrabCString());
		
		
			ASX="";
		ASX.AppendNumber(Connection.EsEnviarNotasHijasAWEB);
		fprintf(ArchivoPref,"EsEnviarNotasHijasAWEB;%s\n", ASX.GrabCString());
		
			ASX="";
		ASX.AppendNumber(Connection.EnviaAWEBSinRetornosTitulo);
		fprintf(ArchivoPref,"EnviaAWEBSinRetornosTitulo;%s\n", ASX.GrabCString());
		
			ASX="";
		ASX.AppendNumber(Connection.EnviaAWEBEnAltasPieFoto);
		fprintf(ArchivoPref,"EnviaAWEBEnAltasPieFoto;%s\n", ASX.GrabCString());
		
		
			ASX="";
		ASX.AppendNumber(Connection.ExportPDFPresets);
		fprintf(ArchivoPref,"ExportPDFPresets;%s\n", ASX.GrabCString());
		
			ASX="";
		ASX.AppendNumber(Connection.ExportPDFPresetsEditWEB);
		fprintf(ArchivoPref,"ExportPDFPresetsEditWEB;%s\n", ASX.GrabCString());
		
		ASX="";
		ASX.AppendNumber(Connection.N2PHmteSendToHmca);
		fprintf(ArchivoPref,"N2PHmteSendToHmca;%s\n", ASX.GrabCString());
		
		ASX="";
		ASX.AppendNumber(Connection.N2PResizePreview);
		fprintf(ArchivoPref,"N2PResizePreview;%s\n", ASX.GrabCString());
		ASX="";
		ASX.AppendNumber(Connection.N2PUtilizarGuias);
		fprintf(ArchivoPref,"N2PUtilizarGuias;%s\n", ASX.GrabCString());
		
		fprintf(ArchivoPref,"RutaDePaginaComoPDF;%s\n",Connection.RutaDePaginaComoPDF.GrabCString());
		fprintf(ArchivoPref,"NombreEstatusParaNotasCompletas;%s\n",Connection.NombreEstatusParaNotasCompletas.GrabCString());
		fprintf(ArchivoPref,"RutaDePaginaComoPDFWEB;%s\n",Connection.RutaDePaginaComoPDFWEB.GrabCString());
		fprintf(ArchivoPref,"N2PParaStyleCredito;%s\n",Connection.nameParaStyleCredito.GrabCString());
		fprintf(ArchivoPref,"PresetTOPDFEditorial;%s\n",Connection.PresetTOPDFEditorial.GrabCString());
		fprintf(ArchivoPref,"PresetTOPDFEditorialWEB;%s\n",Connection.PresetTOPDFEditorialWEB.GrabCString());
		
		fprintf(ArchivoPref,"N2PHmeDSNName;%s\n",Connection.N2PHmeDSNName.GrabCString());
		fprintf(ArchivoPref,"N2PHmeDSNUserLogin;%s\n",Connection.N2PHmeDSNUserLogin.GrabCString());
		fprintf(ArchivoPref,"N2PHmeDSNPwdLogin;%s\n",Connection.N2PHmeDSNPwdLogin.GrabCString());
		fprintf(ArchivoPref,"N2PHmeFTPServer;%s\n",Connection.N2PHmeFTPServer.GrabCString());
		fprintf(ArchivoPref,"N2PHmeFTPUser;%s\n",Connection.N2PHmeFTPUser.GrabCString());
		fprintf(ArchivoPref,"N2PHmeFTPPwd;%s\n",Connection.N2PHmeFTPPwd.GrabCString());
		fprintf(ArchivoPref,"N2PHmeFTPFolder;%s\n",Connection.N2PHmeFTPFolder.GrabCString());
		
		fclose(ArchivoPref);
#ifdef MACINTOSH
		chmod(InterlasaUtilities::MacToUnix(PathFile).GrabCString(),0777);
#endif
		retval=kTrue;
	}
	else
	{
		ASSERT_FAIL("GuardarPreferencesConnectionForNameConection Functiuon No pudo Abrir Archivo para guardar preferencia");
	}
	return(retval);
	
}	


bool16 N2PsqlUpdateStorysAndDBUtils::GuardarQuery( PMString Query)
{
	bool16 retval=kFalse;
	FILE *ArchivoPref;
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	PathFile.Append("QueryCSA.query");
	
	if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"w"))!=NULL)
	{
		
		fprintf(ArchivoPref,"Processs: %s\n",Query.GrabCString());
		
		
		fclose(ArchivoPref);
		retval=kTrue;
	}
	else
	{
		ASSERT_FAIL("GuardarPreferencesConnectionForNameConection Functiuon No pudo Abrir Archivo para guardar preferencia");
	}
	return(retval);
	
}	

void N2PsqlUpdateStorysAndDBUtils::ShowFrameEdgesDocument(IDocument* document, bool16 Show)
{
	do
	{
		InterfacePtr<ICommand> setFrameEdgePrefsCmd(::CmdUtils::CreateCommand(kSetFrameEdgePrefsCmdBoss)); 

		if (setFrameEdgePrefsCmd == nil) 
		{
			break;
		} 

		InterfacePtr<IFrameEdgePrefsCmdData> frameEdgePrefs(setFrameEdgePrefsCmd, IID_IFRAMEEDGEPREFSCMDDATA); 
		if (frameEdgePrefs == nil)
		{ 
			break;
		} 

		frameEdgePrefs->Set(::GetUIDRef(document), Show); 
		if ( CmdUtils::ProcessCommand(setFrameEdgePrefsCmd) != kSuccess) 
		{
		 	break;
		} 

	}while(false);
}


void N2PsqlUpdateStorysAndDBUtils::PlaceUserNameLogedOnPalette(PMString NameUser)
{
	N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PSQLUsernameLogedWidgetID,NameUser);
}



//Funcion que hace el Update desde base de datos y hacia base de datos de notas
bool16 N2PsqlUpdateStorysAndDBUtils::ActualizaTodasLNotasYDisenoPagina(const PMString& ActionSolicitada)
{
	N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaTodasLNotasYDisenoPagina ini");
	RangeProgressBar bar("Update/Save All", 1, 100, kTrue);
	//CAlert::InformationAlert("0");
	bool16 retval=kFalse;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			CAlert::InformationAlert("salio DELETE_Pinches_HyperlinksYBookMarks");
			break;	
		}
		
		if(!this->ValidaExistePaginaSobreBD(document))
		{
			CAlert::InformationAlert(kN2PsqlErrorAlertPaginaBorradaEnBDString);
			break;
		}
		
		bar.SetPosition(10);
		//CAlert::InformationAlert("1");
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			CAlert::InformationAlert("salio app");
			break;
		} 
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
			
		

		bar.SetPosition(20);

		PMString FolioPagina=N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);//Obtiene el folio de la pagina
		PMString N2PSQL_ID_Pag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);//Obtiene ID de la pagina sobre la base de datos
		
		//Validar si esta pagina o documento existe sobre la base de datos
		
		
		
		
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
			continue;
			
		//Sise oprimio save o save as
		if(Aplicacion.Contains("InDesign") && (ActionSolicitada=="Save" || ActionSolicitada=="Save As")) 
		{
			//CAlert::InformationAlert("InDesign Save Save As");
			
			/*SDKLayoutHelper fersdklayout;
			UIDRef docUIDRefFER = ::GetUIDRef(document);
			if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER))
				CAlert::InformationAlert("FerCanSaveDocumentAs 1");*/
			//Mostrar Dialogo de Save revision
			if(FolioPagina.NumUTF16TextChars()<=0 || FolioPagina=="")
			{
				N2PsqlUpdateStorysAndDBUtils::HacerRevisionPaDocNuevo();
				break;
			}
			/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER))
				CAlert::InformationAlert("FerCanSaveDocumentAs 2");*/
			//Guardar notas si se estan editando sin hacer check in
			this->GuardarContenidoNotasEnEdicion();
			/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER))
				CAlert::InformationAlert("FerCanSaveDocumentAs 3");*/
			//Guardar Revision	
			checkIn->AutomaticSaveRevi(document);
			/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER))
				CAlert::InformationAlert("FerCanSaveDocumentAs 4");*/
			retval=kTrue;
			break;
		}
		bar.SetPosition(50);
		if(Aplicacion.Contains("InDesign") && ActionSolicitada=="N2PActualizacion") 
		{
			
			//CAlert::InformationAlert("InDesign N2PActualizacion");
			//No hace nada si no es una pagina de indesign
			if(FolioPagina.NumUTF16TextChars()<=0 || FolioPagina=="")
			{
				break;
			}
			
			
			//Guardar notas si se estan editando sin hacer check in
			this->ActualizaNotasDesdeBD(document);
			this->Fotos_ActualizaLinks(document);
			retval=kTrue;
			break;
		}
		
		
		
		
		bar.SetPosition(60);
		
		
		if(Aplicacion.Contains("InCopy") && (ActionSolicitada=="Save" || ActionSolicitada=="Save As")) 
		{
			if(N2PSQLUtilities::VerificarSiExisteDocumentoSobrePathInicial(Utils<ILayoutUIUtils>()->GetFrontDocument()))
			{
				FolioPagina=N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
				//Mostrar Dialogo de Save revision
				if(FolioPagina.NumUTF16TextChars()<=0 || FolioPagina=="")
				{
					break;
				}
				//Guardar notas si se estan editando sin hacer check in
				this->GuardarContenidoNotasEnEdicion();
				//this->GuardarContenidoNotasEnEdicionEn_TVersionesDElemento_ComoTextPlain();
				//Guardar Revision	
				checkIn->AutomaticSaveRevi(document);
			}
			else
			{
			
				CAlert::InformationAlert(kN2PHacerUpdateAntesDContinuarStringKey);
				
			}
			
			break;
		}
		
		
		bar.SetPosition(70);
		
		if(Aplicacion.Contains("InCopy") && ActionSolicitada=="N2PActualizacion") 
		{
		//	InterfacePtr<IDocFileHandler>  DocFileHandler(Utils<IInCopyDocUtils>()->QueryDocFileHandler());
		//	if(DocFileHandler)
		//	{
				
			//	IDFile FileActual;
			//	bool16 CDS=kFalse;
			//	DocFileHandler->GetCopyDefaultName(::GetUIDRef(document), &FileActual, CDS) ;
				
			//	PMString PathActual=SDKUtilities::SysFileToPMString(&FileActual);//RUTA DE DOCUMENTO ABIERTO
				
				
				PreferencesConnection PrefConectionsOfServerRemote;
		
				if( !this->GetDefaultPreferencesConnection( PrefConectionsOfServerRemote,kFalse ) )
				{
					break;
				}


				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
				(
					kN2PSQLUtilsBoss,	// Object boss/class
					IN2PSQLUtils::kDefaultIID
				)));
				if(SQLInterface==nil)
				{
					break;
				}
				
				PMString StringConection="";
				StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);


				K2Vector<PMString> QueryVector;	
				PMString RutaOnBD="";
				PMString Busqueda="SELECT P.Ruta_Elemento FROM  Pagina P";
				Busqueda.Append("  WHERE  P.ID = ");
				Busqueda.Append(InterlasaUtilities::GetXMPVar("N2PSQLIDPagina",document));
				Busqueda.Append("");
				
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
			
				if(QueryVector.Length()>0)
				{
					//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
					RutaOnBD =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
				}
				else
				{
					CAlert::InformationAlert("No se encontro La pagina sobre la base de datos");
					break;
				}
				
				
				/////////////////////////////////////////////////
				//PARA OBTENER LA RUTA DEL DOCUMENTO ACTUAL EN BD
				/////////////////////////////////////////////////
				
				PMString PathActual=InterlasaUtilities::GetXMPVar("N2PPaginaPath",document);
				//CAlert::InformationAlert("!"+RutaOnBD+"=="+PathActual+"!");
				////////////////////////////////////////////////
				if(RutaOnBD!=PathActual)
				{
					//CAlert::InformationAlert("A chingado se vino por aca");
					//Si las rutas son diferentes hay que depositar la notas y traernos el documento de la nueva ruta
				
					/*PMString IDPaginaXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina",document);
				
					if(IDPaginaXMP.NumUTF16TextChars()<=0)
					{
						CAlert::InformationAlert("Dont Found Page ID");
						break;
					}
				
					//Consulta a la base de datos la nueva ruta de la pagina
					PreferencesConnection PrefConectionsOfServerRemote;
		
					if( !this->GetDefaultPreferencesConnection( PrefConectionsOfServerRemote,kFalse ) )
					{
						break;
					}
			
					PMString PaginaNuevaPath="";
					PMString Consulta=" SELECT Ruta_Elemento FROM Pagina WHERE ID=" +IDPaginaXMP ;
			
			
					PMString StringConection="";
					K2Vector<PMString> QueryVector;
			
					//Id de los integrantes de la Familia del Articulo o nota
					
					StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					if(SQLInterface==nil)
					{
						break;
					}
		
			
					PMString idSeccionDElemento="";
					if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
					{
						CAlert::InformationAlert("Error en Consulta");
						break;
					}
					else
					{
						PaginaNuevaPath = SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[0]);
						if(PaginaNuevaPath=="" || PaginaNuevaPath.NumUTF16TextChars()<=0)
						{
							//CAlert::InformationAlert("AAA");
							CAlert::InformationAlert("No se encontro Path de Archivo en Base de datos");
							break;
						}
					}
					*/
					//Actualizacion de contenido de elementos de una nota hacia la base de datos
					this->CheckInNotasEnEdicion(kTrue);
				
					UIDRef docUIDRef = ::GetUIDRef(document);
				
					SDKLayoutHelper helperLayout;
					helperLayout.CloseDocument(docUIDRef,kFalse,kSuppressUI,kFalse,IDocFileHandler::kSchedule);
				
				
					IDFile targetFile;
					FileUtils::PMStringToIDFile(RutaOnBD,targetFile);
				
					Utils<IInCopyDocUtils>()->DoOpen(targetFile,IOpenFileCmdData::kOpenCopy);
				
					document = Utils<ILayoutUIUtils>()->GetFrontDocument();
					if (document == nil)
					{
						continue;
					}
					
					//Guarda la ruta de donde se abrio la pagina esto con el motivo de verificar la actualizacion 
					//de Documento si es que no fue renombrado por el diseñador
					InterlasaUtilities::SaveXMPVar("N2PPaginaPath",RutaOnBD,document);
					
					//CAlert::InformationAlert("F2");
					//Hace un update de las notas
					if(this->SeRequiereActualizacionDeNotasDeDocument(document))
					{
						//Actualizacion de contenido de elementos de una nota desde la base de datos
						this->ActualizaNotasDesdeBD(document);
					}
					//CAlert::InformationAlert("F3");
					
					//Guarda revision de una pagina
					checkIn->AutomaticSaveRevi(document);
					
					
					//CAlert::InformationAlert("F4");
					//Pone todos los elementos en estado de no edicion
					PMString CadenaCompleta = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
					if(CadenaCompleta.NumUTF16TextChars()>0)
					{
										
							
						UIDRef ur(GetUIDRef(document));
			
						IDataBase* db = ::GetDataBase(document);
						if (db == nil)
						{
							ASSERT_FAIL("db is invalid");
							continue;
						}
			
		
						int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
						ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
						N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
									
						for(int32 numElem=0;numElem<contUpdate;numElem++)
						{
						
							PMString IDFrameEleNota="";
							IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
						
						
							ErrorCode error=this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
							if (error != kSuccess)
							{
								continue;
							}
						
							UID UIDStory=UID(ListUpdate[numElem].IDFrame);
							UIDRef UIDRefStory=UIDRef(db,UIDStory);
								
							InterfacePtr<IInCopyStoryList> icStoryList(document,UseDefaultIID());
							if(icStoryList==nil)
								continue;
						
						
		
							icStoryList->ProcessAddStory(UIDRefStory.GetUID());
						
						
						}
					}
					//CAlert::InformationAlert("F5");
					
					K2Vector<int32> Notas;
					if(this->ObtenerIDNotasPUpdateDesign(Notas))
					{
							
						//Cambia modo de bloquedo de cada una de los textframe que se encontraban editando
						for(int32 i=0;i<Notas.Length();i++)
						{
							PMString IDFrameEleNota="";
						
							IDFrameEleNota.AppendNumber(Notas[i]);
							//CAlert::InformationAlert(IDFrameEleNota);
							this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kFalse);
						}
					}
				
					this->ShowFrameEdgesDocument( document, kTrue);	
					
					/////////////////////////////
				}
				else
				{
					//CAlert::InformationAlert("FFFFF");
					if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(document)))
					{
					
						//CAlert::InformationAlert("F0");
				
						//Actualizacion de contenido de elementos de una nota hacia la base de datos
						this->CheckInNotasEnEdicion(kTrue);
						
						//CAlert::InformationAlert("F1");
				
						
						///antes que nada hay que guardar el id de las 
						//notas que se estan editando para que no se pierda el hido de ellas
						Utils<IInCopyDocUtils>()->DoUpdateDesign(::GetUIDRef(document));
						
						document = Utils<ILayoutUIUtils>()->GetFrontDocument();
						if (document == nil)
						{
							continue;
						}
						
						//Guarda la ruta de donde se abrio la pagina esto con el motivo de verificar la actualizacion 
						//de Documento si es que no fue renombrado por el diseñador
						InterlasaUtilities::SaveXMPVar("N2PPaginaPath",RutaOnBD,document);
					
						//Hace un update de las notas
						if(this->SeRequiereActualizacionDeNotasDeDocument(document))
						{
							//Actualizacion de contenido de elementos de una nota desde la base de datos
							this->ActualizaNotasDesdeBD(document);
						}
						//CAlert::InformationAlert("F3");
					
						//Guarda revision de una pagina
						checkIn->AutomaticSaveRevi(document);
					
					
						//CAlert::InformationAlert("F4");
						//Pone todos los elementos en estado de no edicion
						PMString CadenaCompleta = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
						if(CadenaCompleta.NumUTF16TextChars()>0)
						{
										
							
							UIDRef ur(GetUIDRef(document));
			
							IDataBase* db = ::GetDataBase(document);
							if (db == nil)
							{
								ASSERT_FAIL("db is invalid");
								continue;
							}
			
		
							int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
							ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
							N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
									
							for(int32 numElem=0;numElem<contUpdate;numElem++)
							{
						
								PMString IDFrameEleNota="";
								IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
						
						
								ErrorCode error=this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
								if (error != kSuccess)
								{
									continue;
								}
						
								UID UIDStory=UID(ListUpdate[numElem].IDFrame);
								UIDRef UIDRefStory=UIDRef(db,UIDStory);
								
								InterfacePtr<IInCopyStoryList> icStoryList(document,UseDefaultIID());
								if(icStoryList==nil)
									continue;
							
						
		
								icStoryList->ProcessAddStory(UIDRefStory.GetUID());
						
						
							}
						}
						//CAlert::InformationAlert("F5");
					
						K2Vector<int32> Notas;
						if(this->ObtenerIDNotasPUpdateDesign(Notas))
						{
							
							//Cambia modo de bloquedo de cada una de los textframe que se encontraban editando
							for(int32 i=0;i<Notas.Length();i++)
							{
								PMString IDFrameEleNota="";
						
								IDFrameEleNota.AppendNumber(Notas[i]);
								//CAlert::InformationAlert(IDFrameEleNota);
								this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kFalse);
							}
						}
						this->ShowFrameEdgesDocument( document, kTrue);
					}
					else
					{
						//CAlert::InformationAlert("Y luego por acaya");
						if(this->SeRequiereActualizacionDeNotasDeDocument(document))
						{
							//Actualizacion de contenido de elementos de una nota desde la base de datos
							this->ActualizaNotasDesdeBD(document);
					
						}
						//Guarda revision de una pagina
						checkIn->AutomaticSaveRevi(document);
						retval=kTrue;
						
					}	
				}

				/*if(N2PSQLUtilities::VerificarSiExisteDocumentoSobrePathInicial( document))
				{
				if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(document)))
				{
					
				
				
					//Actualizacion de contenido de elementos de una nota hacia la base de datos
					this->CheckInNotasEnEdicion(kTrue);
						
					//CAlert::InformationAlert("F1");
				
						
					///antes que nada hay que guardar el id de las 
					//notas que se estan editando para que no se pierda el hido de ellas
					Utils<IInCopyDocUtils>()->DoUpdateDesign(::GetUIDRef(document));
						
					document = Utils<ILayoutUIUtils>()->GetFrontDocument();
					if (document == nil)
					{
						continue;
					}
						
					//CAlert::InformationAlert("F2");
					//Hace un update de las notas
					if(this->SeRequiereActualizacionDeNotasDeDocument(document))
					{
						//Actualizacion de contenido de elementos de una nota desde la base de datos
						this->ActualizaNotasDesdeBD(document);
					}
					//CAlert::InformationAlert("F3");
					
					//Guarda revision de una pagina
					checkIn->AutomaticSaveRevi(document);
					
					
					//CAlert::InformationAlert("F4");
					//Pone todos los elementos en estado de no edicion
					PMString CadenaCompleta = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
					if(CadenaCompleta.NumUTF16TextChars()>0)
					{
										
							
						UIDRef ur(GetUIDRef(document));
			
						IDataBase* db = ::GetDataBase(document);
						if (db == nil)
						{
							ASSERT_FAIL("db is invalid");
							continue;
						}
			
		
						int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
						ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
						N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
									
						for(int32 numElem=0;numElem<contUpdate;numElem++)
						{
						
							PMString IDFrameEleNota="";
							IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
						
						
							ErrorCode error=this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
							if (error != kSuccess)
							{
								continue;
							}
						
							UID UIDStory=UID(ListUpdate[numElem].IDFrame);
							UIDRef UIDRefStory=UIDRef(db,UIDStory);
								
							InterfacePtr<IInCopyStoryList> icStoryList(document,UseDefaultIID());
							if(icStoryList==nil)
								continue;
						
						
		
							icStoryList->ProcessAddStory(UIDRefStory.GetUID());
						
						
						}
					}
					//CAlert::InformationAlert("F5");
					
					K2Vector<int32> Notas;
					if(this->ObtenerIDNotasPUpdateDesign(Notas))
					{
							
						//Cambia modo de bloquedo de cada una de los textframe que se encontraban editando
						for(int32 i=0;i<Notas.Length();i++)
						{
							PMString IDFrameEleNota="";
						
							IDFrameEleNota.AppendNumber(Notas[i]);
							//CAlert::InformationAlert(IDFrameEleNota);
							this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kFalse);
						}
					}
				
					this->ShowFrameEdgesDocument( document, kTrue);	
				
				}
				else
				{
				
			
					//CAlert::InformationAlert("E1");
					if(this->SeRequiereActualizacionDeNotasDeDocument(document))
					{
						//CAlert::InformationAlert("E21");
						//Actualizacion de contenido de elementos de una nota desde la base de datos
						this->ActualizaNotasDesdeBD(document);
					
					}
				
					//CAlert::InformationAlert("E4");
			
					//Actualizacion de contenido de elementos de una nota hacia la base de datos
					
							
					//Guarda revision de una pagina
					checkIn->AutomaticSaveRevi(document);
						
					//CAlert::InformationAlert("E5");
					retval=kTrue;
						
				}	
			}
				else
				{
				//Si no existe hay que depositar la notas y traenos el documento de la nueva ruta
				
				PMString IDPaginaXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina",document);
				
				if(IDPaginaXMP.NumUTF16TextChars()<=0)
				{
					CAlert::InformationAlert("Dont Found Page ID");
					break;
				}
				
				//Consulta a la base de datos la nueva ruta de la pagina
				PreferencesConnection PrefConectionsOfServerRemote;
		
				if( !this->GetDefaultPreferencesConnection( PrefConectionsOfServerRemote,kFalse ) )
				{
					break;
				}
			
				PMString PaginaNuevaPath="";
				PMString Consulta=" SELECT Ruta_Elemento FROM Pagina WHERE ID=" +IDPaginaXMP ;
			
			
				PMString StringConection="";
				K2Vector<PMString> QueryVector;
			
				 //Id de los integrantes de la Familia del Articulo o nota
					
				StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
				(
					kN2PSQLUtilsBoss,	// Object boss/class
					IN2PSQLUtils::kDefaultIID
				)));
				if(SQLInterface==nil)
				{
					break;
				}
		
			
				PMString idSeccionDElemento="";
				if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
				{
					CAlert::InformationAlert("Error en Consulta");
					break;
				}
				else
				{
					PaginaNuevaPath = SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[0]);
					if(PaginaNuevaPath=="" || PaginaNuevaPath.NumUTF16TextChars()<=0)
					{
						//CAlert::InformationAlert("AAA");
						CAlert::InformationAlert("No se encontro Path de Archivo en Base de datos");
						break;
					}
				}
			
				//Actualizacion de contenido de elementos de una nota hacia la base de datos
				this->CheckInNotasEnEdicion(kTrue);
				
				UIDRef docUIDRef = ::GetUIDRef(document);
				
				SDKLayoutHelper helperLayout;
				helperLayout.CloseDocument(docUIDRef,kFalse,kSuppressUI,kFalse,IDocFileHandler::kSchedule);
				
				
				IDFile targetFile;
				FileUtils::PMStringToIDFile(PaginaNuevaPath,targetFile);
				
				Utils<IInCopyDocUtils>()->DoOpen(targetFile,IOpenFileCmdData::kOpenCopy);
				
				document = Utils<ILayoutUIUtils>()->GetFrontDocument();
					if (document == nil)
					{
						continue;
					}
						
					//CAlert::InformationAlert("F2");
					//Hace un update de las notas
					if(this->SeRequiereActualizacionDeNotasDeDocument(document))
					{
						//Actualizacion de contenido de elementos de una nota desde la base de datos
						this->ActualizaNotasDesdeBD(document);
					}
					//CAlert::InformationAlert("F3");
					
					//Guarda revision de una pagina
					checkIn->AutomaticSaveRevi(document);
					
					
					//CAlert::InformationAlert("F4");
					//Pone todos los elementos en estado de no edicion
					PMString CadenaCompleta = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
					if(CadenaCompleta.NumUTF16TextChars()>0)
					{
										
							
						UIDRef ur(GetUIDRef(document));
			
						IDataBase* db = ::GetDataBase(document);
						if (db == nil)
						{
							ASSERT_FAIL("db is invalid");
							continue;
						}
			
		
						int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
						ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
						N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
									
						for(int32 numElem=0;numElem<contUpdate;numElem++)
						{
						
							PMString IDFrameEleNota="";
							IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
						
						
							ErrorCode error=this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
							if (error != kSuccess)
							{
								continue;
							}
						
							UID UIDStory=UID(ListUpdate[numElem].IDFrame);
							UIDRef UIDRefStory=UIDRef(db,UIDStory);
								
							InterfacePtr<IInCopyStoryList> icStoryList(document,UseDefaultIID());
							if(icStoryList==nil)
								continue;
						
						
		
							icStoryList->ProcessAddStory(UIDRefStory.GetUID());
						
						
						}
					}
					//CAlert::InformationAlert("F5");
					
					K2Vector<int32> Notas;
					if(this->ObtenerIDNotasPUpdateDesign(Notas))
					{
							
						//Cambia modo de bloquedo de cada una de los textframe que se encontraban editando
						for(int32 i=0;i<Notas.Length();i++)
						{
							PMString IDFrameEleNota="";
						
							IDFrameEleNota.AppendNumber(Notas[i]);
							//CAlert::InformationAlert(IDFrameEleNota);
							this->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kFalse);
						}
					}
				
					this->ShowFrameEdgesDocument( document, kTrue);	
			}
				 */
			//}
			
			//CAlert::InformationAlert("Salio");
			break;
		}
		
		bar.SetPosition(80);
		
		
	}while(false);
	
	
	N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID,kFalse);
	N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID, kN2PsqlStcTextActualizaPagsWidgetID,"");
	
	//CAlert::InformationAlert("FIN");
	bar.SetPosition(100);
	
	N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ActualizaTodasLNotasYDisenoPagina fin");
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::HacerRevisionPaDocNuevo()
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::HacerRevisionPaDocNuevo ini");
	bool16 retval=kFalse;
	do
	{
		//CAlert::InformationAlert("1");
		PMString NameDocumentActual="";
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		//CAlert::InformationAlert("2");
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
			break;
		}
		
		//Verifica que no sea un documento nuevo
			document->GetName(NameDocumentActual);
			
		//CAlert::InformationAlert("3");
		//Para Gurdar Revision
			ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			
		
		//CAlert::InformationAlert("4");
			if(NameDocumentActual.Contains("Untitled") || NameDocumentActual.Contains("Sin tÌtulo-") || NameDocumentActual.Contains(".indt"))
			{
				//Hace revision
				if(checkIn->SaveRevi(kTrue))
				{
					retval=kTrue;
				}
				break;
			}
			
			
		///Obtiene las Ultimas Preferencias
			PrefParamDialogInOutPage PreferencesPara;
		//CAlert::InformationAlert("5");
			checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
			//checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
		
		//Hay que validar que ya se ha realizadon un chech in o save revition
		// y que la pagina se llame igual a nombre de pagina actual
			if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars() == 0)
			{
				//CAlert::InformationAlert(PreferencesPara.Pagina_To_SavePage);
				
				//si no se ha realizado un previo save as o  save se habre el diago save as 
				if(checkIn->SaveRevi(kTrue))
				{
					retval=kTrue;
				}
				
				
				
				PreferencesConnection PrefConections;
				
				
				if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
				{
					break;
				}
				
				if(PrefConections.N2PUtilizarGuias==1)
				{
					N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kTrue);
					N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kFalse);
					PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
					if(Id_Seccion.NumUTF16TextChars()>0 )
						N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
					
				}
				else
				{
					//CAlert::InformationAlert("BBB");
					N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kFalse );
					N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kTrue );
					
				}
				
				break;
				
				
			}
		//CAlert::InformationAlert("6");
			PMString NameDocUltimoSaveAs=PreferencesPara.Pagina_To_SavePage;
			NameDocUltimoSaveAs.Append(".indd");
		
			
			///Para ver si el documento de enfrente se llama igual que el ultimo salvado
			if(NameDocumentActual!=NameDocUltimoSaveAs)
			{
			
				if(checkIn->SaveRevi(kTrue))
				{
					retval=kTrue;
				}
				break;
			}
		
		PreferencesConnection PrefConections;
		
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		if(PrefConections.N2PUtilizarGuias==1)
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kTrue);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kFalse);
			PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
			if(Id_Seccion.NumUTF16TextChars()>0 )
				N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
			
		}
		else
		{
			//CAlert::InformationAlert("BBB");
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kFalse );
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kTrue );
			
		}
		//CAlert::InformationAlert("8");
		retval=kTrue;
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::HacerRevisionPaDocNuevo fin");
	return(retval);
}

bool16  N2PsqlUpdateStorysAndDBUtils::CheckOutNota()
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckOutNota ini");
	//Hace update de notas y DiseÒo si se require
	bool16 retval=kFalse;
	int32 numeroPaginaActual=0;
	//UID del TextModel del Frame que se encuentra seleccionado
	int32 UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
	PMString IDNota = "";
	PMString DSN_Nota_Seleted="";
	K2Vector<PMString> PramVector;
	
	RangeProgressBar bar("Check out progress", 1, 100, kTrue);
	do
	{
		
		PMString IdUsuarioLogeaqdo = "";
		if(!this->ObtenUsuarioLogeadoActualmente(IdUsuarioLogeaqdo))
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		
		bar.SetPosition(30);
		if(UIDTextModelOfTextFrame>0)
		{
		
			PreferencesConnection PrefConections;
			
			PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
			
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}

			
			InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																						  (
																						   kN2PwsdlcltUtilsBoss,	// Object boss/class
																						   IN2PWSDLCltUtils::kDefaultIID
																						   )));
			
			if(!	N2PWSDLCltUtil)
			{
				CAlert::InformationAlert("salio");
				break;
			}
			PMString ResultString="";
			retval=N2PWSDLCltUtil->realizaConneccion(IdUsuarioLogeaqdo,
															"192.6.2.2", 
															"",
															Aplicacion, 
															PrefConections.URLWebServices);
			if(!retval)
			{
				retval=kFalse;
			}
			/*	DONGLE VERDE RED			
			//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckOutNota Sentinel check");
			InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
			(
				kInterUltraObjetProKyBoss,	// Object boss/class
				IInterUltraObjectProKy::kDefaultIID
			)));
			
			if(UltraProCheck==nil)
			{
				 retval=kFalse;
				break;
			}
		
			PMString ResultString="";
			retval=UltraProCheck->RedValueN2PPlugin(ResultString);
			if(!retval)
			{

				CAlert::ErrorAlert(ResultString);
				 retval=kFalse;
				break;
			} */
			//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckOutNota Sentinel check fin");

			/********* DONGLE ROJO LOCAL ************
					InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*> (CreateObject
					(
						kInterUltraProKyBoss,	// Object boss/class
						IInterUltraProKy::kDefaultIID
					)));

					if(UltraProCheck==nil)
					{
						
						retval=kFalse;
						break;
					}
					PMString ResultString="";
					retval=UltraProCheck->CheckDongleN2PPlugins(ResultString);
					if(!retval)
					{
						CAlert::ErrorAlert(ResultString);
						retval=kFalse;
						break;
					}
			
			**/
			
			StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
			//Obtiene el ID_elemento_padre apartir del textframe seleccionado
			retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
			//SI NO ES ELMENTO DE UNA NOTA
			if(retval==kFalse)
			{
				CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
				break;
			}
			
			///////////////////////////////////
			//Para saber si se puede fluir la nota
			PreferencesConnection PrefConectionsOfServerRemote;
		
			if( !this->GetDefaultPreferencesConnection( PrefConectionsOfServerRemote,kFalse ) )
			{
				break;
			}
		
			PMString Consulta=" SELECT Id_Seccion FROM Elemento WHERE Id_Elemento=" +StructNota_IDFrames[0].ID_Elemento_padre ;
			
			
			PMString StringConection="";
			K2Vector<PMString> QueryVector;
			
			 //Id de los integrantes de la Familia del Articulo o nota
					
			StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			if(SQLInterface==nil)
			{
				break;
			}
		
			
			PMString idSeccionDElemento="";
			if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
			{
				break;
			}
			else
			{
				idSeccionDElemento = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
				if(idSeccionDElemento=="0")
				{
					//CAlert::InformationAlert("AAA");
					CAlert::InformationAlert(kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey);
					break;
				}
			}
			
			
			
			bar.SetPosition(45);
			
			//solo si la nota proviene del servidor de Preferencia
			
			
			QueryVector.clear();
			Consulta="";
			if(Aplicacion.Contains("InDesign"))
			{
				Consulta.Append(" CALL HaveUserPrivilegioForEvent ('" + IdUsuarioLogeaqdo + "' , "+idSeccionDElemento+" , 4, @X)" );
			}
			else
			{
				Consulta.Append(" CALL HaveUserPrivilegioForEvent ('" + IdUsuarioLogeaqdo + "' , "+idSeccionDElemento+" , 12, @X)" );
			}
			
			if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
			{
				break;
			}
			else
			{
				PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
				if(RESULT=="0")
				{
					
					CAlert::InformationAlert(kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey);
					break;
				}
			}
			bar.SetPosition(50);		
			///////////////////////////////////
		
			
			
			
			//Para verificar que no se haya hech un chec out anteriormente de esta nota sobre la misma pagina
			if(!this->CanDoCheckOutNota(UIDTextModelOfTextFrame))
			{
				break;
			}
			
						
			
			
			//Id de usuario logeado
					
			//Cadena de error
			PMString Cadena(kInfoAlertNotaEditPorOtroUsuarioStringKey);
			Cadena.Translate();
 			Cadena.SetTranslatable(kTrue);
 			
 			
 			PMString Cadena2(kInfoAlertNotaASidoDirigidoAUsuarioStringKey);
			Cadena2.Translate();
 			Cadena2.SetTranslatable(kTrue);
 	
 			bar.SetPosition(60);
 		 	PMString id_NotaOPieDFoto="";
			if(StructNota_IDFrames[0].Id_Elem_PieFoto_Note.NumUTF16TextChars()<=0)
			{
				id_NotaOPieDFoto=StructNota_IDFrames[0].ID_Elemento_padre;//Nota
			}
			else
			{
				id_NotaOPieDFoto=StructNota_IDFrames[0].Id_Elem_PieFoto_Note;//Pie de foto
			}
 		 	PMString QueryString="CALL NOTAN2PCHECKOUT2("+id_NotaOPieDFoto+",'"+IdUsuarioLogeaqdo+"',@a,@x,";
 			
 			/*PramVector.Append(StructNota_IDFrames[0].ID_Elemento_padre);
 			PramVector.Append(IdUsuarioLogeaqdo);
 			PramVector.Append(Cadena);
 			PramVector.Append("");*/
 			if(Aplicacion.Contains("InDesign"))
 				QueryString.Append("4)");
 			else
 				QueryString.Append("12)");
 			
 			////// Obtiene Preferencias para conexion /////////////
			
		
			/*PreferencesConnection PrefConections;
		
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}*/
			
			//Verifica que el servidor de donde provien la nota sea el local o en el que actualmente esta trabajando
			/*if(StructNota_IDFrames[0].DSNNote!=PrefConections.DSNNameConnection)
			{
				//Esta nota pertenece a un servidor remoto 
				
				
				PMString msgNotaDeOtroServidor(kCantCheckOutserverRemoteStringKey);
				msgNotaDeOtroServidor.Translate();
  				msgNotaDeOtroServidor.SetTranslatable(kTrue);
  
				msgNotaDeOtroServidor.Append(StructNota_IDFrames[0].DSNNote);
				
  				
				PMString msgServidorLocal(kN2PsqlServidorLocalStringKey);
				msgServidorLocal.Translate();
  				msgServidorLocal.SetTranslatable(kTrue);
  				
				msgServidorLocal.Append(PrefConections.DSNNameConnection);
				CAlert::InformationAlert(msgNotaDeOtroServidor + msgServidorLocal);
				break;
			}*/
		
			
			bar.SetPosition(70);
			/*SQLInterface->StoreProcedureCHECKOUTNOTA(StringConection,PramVector);*/
			SQLInterface->SQLQueryDataBase(StringConection,QueryString,QueryVector);
			
			PMString ResultadoX="";
			PMString UltimoCheckOut="";
			for(int32 j=0;j<QueryVector.Length() ;j++)
			{
					
					
				//Hacer comparacion entre las fechas para verificar si se ha modificado la nota desde la ultima vez que fue actualizada
				ResultadoX = SQLInterface->ReturnItemContentbyNameColumn("CadenaOUT",QueryVector[j]);
				UltimoCheckOut=SQLInterface->ReturnItemContentbyNameColumn("Fecha_CheckOut",QueryVector[j]);
				
			}
			bar.SetPosition(90);
			if(ResultadoX=="OK")
			{
				retval=kTrue;
			}
			else
			{
				if(ResultadoX=="Error 4" || ResultadoX=="Error 6" || ResultadoX=="Error 5" || ResultadoX==Cadena)
				{
					if(ResultadoX==Cadena)
					{
						//CAlert::InformationAlert("Salio");
					}
					else
					{
						if(ResultadoX=="Error 4")
						{
							//CAlert::InformationAlert("Error 4");
						}
						else
						{
							if(ResultadoX=="Error 6")
							{
								ResultadoX="";
								//Busqueda del usuario a quien va dirigida la nota
								QueryVector.clear();
								SQLInterface->SQLQueryDataBase(StringConection,"SELECT Dirigido_a FROM Elemento WHERE Id_Elemento="+StructNota_IDFrames[0].ID_Elemento_padre,QueryVector);
								for(int32 j=0;j<QueryVector.Length() ;j++)
								{
									//Hacer comparacion entre las fechas para verificar si se ha modificado la nota desde la ultima vez que fue actualizada
									ResultadoX = SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[j]);
					
								}
								
								if(ResultadoX.NumUTF16TextChars()<=0)
									CAlert::InformationAlert(kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey);
								else
									CAlert::InformationAlert(Cadena2 + ResultadoX);
								
							}
							else
							{
								if(ResultadoX=="Error 5")
								{
									//CAlert::InformationAlert("DDD");
									CAlert::InformationAlert(kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey);
								}
								else
								{
									//CAlert::InformationAlert(PramVector[2]);
									PMString msgErrorChechOutNota(kN2PSqlNotaPerteneceaOtroGrupoStringKey);
									msgErrorChechOutNota.Translate();
									msgErrorChechOutNota.SetTranslatable(kTrue);
									CAlert::InformationAlert(msgErrorChechOutNota);
								}
									
							}
						
						}
						
					}
				}
				else
				{
					CAlert::InformationAlert(Cadena + ResultadoX);
				}
				
				retval=kFalse;
				break;
			}
			bar.SetPosition(95);
			
 			if(!CambiaEstadoYLapizDeNota(StructNota_IDFrames,kFalse,"",kFalse))
			{
				//CAlert::InformationAlert("CambiaEstadoYLapizDeNota");
				break;
			}
				
			//Actualiza Elementos de nota desde la base de datos
			this->ActualizaNotaDesdeBDPorIDNota(StructNota_IDFrames,0, kTrue, document);
			
			///////////////////////////////////////////
			//CheckOut demas Familiares sobre la pagina
			///////////////////////////////////////////
			bar.SetPosition(96);
			PMString ConsultaFamily="SELECT Id_Elemento FROM Elemento WHERE Id_Elemento_padre="+StructNota_IDFrames[0].ID_Elemento_padre+ " AND id_tipo_ele IN(10,4) ";
			K2Vector<PMString> IdFamilyArticle;
			if(!SQLInterface->SQLQueryDataBase(StringConection,ConsultaFamily,IdFamilyArticle))
			{
				break;
			}
			
			if(IdFamilyArticle.Length()>0)
			{
				for(int32 j=0;j<IdFamilyArticle.Length();j++)
				{
					PMString ID_Elemento_PadreAA = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",IdFamilyArticle[j]);
					if(GetElementosNotaDXMP_Of_IDElementoPadre(ID_Elemento_PadreAA,	StructNota_IDFrames, document))
					{
						this->CambiaEstadoYLapizDeNota(StructNota_IDFrames,kFalse,"",kFalse);
									
						//Actualiza Elementos de nota desde la base de datos
						this->ActualizaNotaDesdeBDPorIDNota(StructNota_IDFrames,0, kTrue, document);
						
					}
				}
					 
			}
			bar.SetPosition(97);
		}
		else
		{
			CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
		}
	}while(false);	
	bar.SetPosition(100);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckOutNota fin");
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::CanDoCheckOutNota(int32 UIDFrame)
{
	bool16 retval=kFalse;
	do
	{
	
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			break;	
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		
		UID UIDTextModel=UIDFrame;
					
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("document is invalid");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		////////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			ASSERT_FAIL("mytextmodel is invalid");
			break;
		}
		
		InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
		if(frameList==nil)
		{
			ASSERT_FAIL("frameList");
			break;
		}
		
		InterfacePtr<IItemLockData> textLockData(mytextmodel,UseDefaultIID());
		if(textLockData==nil)
		{
			ASSERT_FAIL("textLockSDta");
			break;
		}
		
		if(Aplicacion.Contains("InDesign"))
		{
			if(textLockData->GetAttributeLock())
			{
				 retval=kTrue;
				 
			}
		}
		else
		{
			if(textLockData->GetInsertLock())
			{
				 retval=kTrue;
			}
		}
		
	}while(false);
	return(retval);
}

//Para Preguntar si puede hacer checkIn a una nota
bool16 N2PsqlUpdateStorysAndDBUtils::CanDoCheckInNota(int32 UIDFrame)
{
	bool16 retval=kFalse;
	do
	{
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			break;	
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		UID UIDTextModel=UIDFrame;
					
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CanDoCheckInNota document == nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CanDoCheckInNota db == nil");
			break;
		}
		
		////////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			ASSERT_FAIL("CanDoCheckInNota mytextmodel == nil");
			break;
		}
		
		InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
		if(frameList==nil)
		{
			ASSERT_FAIL("CanDoCheckInNota frameList == nil");
			break;
		}
		
		InterfacePtr<IItemLockData> textLockData(mytextmodel,UseDefaultIID());
		if(textLockData==nil)
		{
			ASSERT_FAIL("CanDoCheckInNota textLockData == nil");
			break;
		}
		
		if(Aplicacion.Contains("InDesign"))
		{
			if(!textLockData->GetAttributeLock()) //!textLockData->GetAttributeLock()
			{
				
				 retval=kTrue;
			}
		}
		else
		{
			if(!textLockData->GetInsertLock())
			{
				
				 retval=kTrue;
			}
		}
	}while(false);
	return(retval);
}



bool16 N2PsqlUpdateStorysAndDBUtils::NotaCmdDepositarCheckIn()
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInNota ini");
	//CAlert::InformationAlert("Test1");
	bool16 retval=kFalse;
	int32 numeroPaginaActual=0;
	PMString IDNota = "";
	int32 UIDTextModelOfTextFrame =0;
	
	
	GlobalTime TimeExpGuardaYMAndaABaseDDatos;
	GlobalTime Time1;	//Inicial
	//ActualTime.CurrentTime();

	RangeProgressBar bar("Check in progress", 1, 100, kTrue);

	do
	{
		
		
		PMString ID_Usuario="";
		
		
		//Ve si el existe usuario logeado
		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
		{
			break;
		}
		
		
		//CAlert::InformationAlert("Test2");
		
		//Ve si existe documento en frente o si se esta trabajando con algun documento
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
			
		

		//Obtiene el ID text model del Frame que se esta seleccionando
		UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
		//SI NO SE ENCUENTRA SELECCIONADO UN TEXTFRAME
		if(UIDTextModelOfTextFrame<1)
		{
			CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
			break;
		}
		
		//CAlert::InformationAlert("Test3");
		//
		StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
		//Obtiene el ID_elemento_padre apartir del textframe seleccionado
		retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
		//SI NO ES ELMENTO DE UNA NOTA
		if(retval==kFalse)
		{
			CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
			break;
		}
		
		//CAlert::InformationAlert("Test4");
		//PARA AVERIGUAR SI SE PUEDE HACER UN CHECKIN A ESTA NOTA
		if(!CanDoCheckInNota(UIDTextModelOfTextFrame))
		{	
			//CAlert::InformationAlert("NO PUDO HACER CHECKIN");
			break;
		}
		
		//Verifica que el ultimo usuario que retiro la nota sea el mismo que esta depositandola
		//Validsacion Guarachin
		/*	if(!ValidaSiUsuario_P_CheckInEnBaseDDatos(StructNota_IDFrames[0].ID_Elemento_padre,ID_Usuario))
			{
				//Significa que alguien mas retiro la nota por lo tanto no puede modificar nada sobre la base de datos.
				//Debe de notificar al usuario que no puede depositar la nota y que esta sera deshabilitada para no generar 
				//Errores en la versiones de las nota
				CAlert::InformationAlert(kN2PSQLNotaRetiradaPorOtroUsuarioDepositoVisualStringKey);
							
				StructIdFramesElementosXIdNota* VectorCheckInNoteToChangeEstatus=new StructIdFramesElementosXIdNota[1];	
							
				VectorCheckInNoteToChangeEstatus[0]=StructNota_IDFrames[0];
						
				if(!CambiaEstadoYLapizDeNota(VectorCheckInNoteToChangeEstatus,kTrue,"",kFalse))
				{
					continue;
				}
							
				continue;
			}
		*/				
		//CAlert::InformationAlert("Test5");
		
		PMString Estado="";
		PMString RoutedTo="";
		PMString PublicacionID="";
		 PMString SeccionID="";
		 PMString GuiaNota="";
		//Obtiene las preferencias para dar checkIn a la nota
		if(!this->OpenDlgCheckInNota(Estado,RoutedTo, PublicacionID, SeccionID,GuiaNota))
		{
			//CAlert::InformationAlert("NO PUDO ABRER DIALOGO");
			break;
		}
		
		//CAlert::InformationAlert("Test6");
		
		//Para Ocultar los text frames con overset
		bool16 isTextFrameWithOverstShow = N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowTxtOversetEditWidgetID);
		if(isTextFrameWithOverstShow)
		{
			InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
			(
				kN2PCTUtilitiesBoss,	// Object boss/class
				IN2PCTUtilities::kDefaultIID
			)));
			
			if(N2PCTUtils==nil)
			{
				break;
			}
				
			N2PCTUtils->OcultaNotasConFrameOverset();		
			//CAlert::InformationAlert("Oculta Frames whit overset");
		}
		
		
		//CAlert::InformationAlert("Test7");
		//Actualiza a BD el contenido de los elementos de la nota,
		//Cambia el estado de escritura(Lapiz),
		//Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota, 
		//Libera la nota en la BD para ser modificad por cualquier otro usuario
		bar.SetPosition(50);
		
		Time1.CurrentTime();

		this->CheckInNotaDBAndPag(StructNota_IDFrames,Estado,RoutedTo,ID_Usuario,  PublicacionID, SeccionID, kFalse, document,GuiaNota);//V1034
		//this->CheckInNotaDBAndPag_CompletesElements(StructNota_IDFrames, Estado, RoutedTo, ID_Usuario,PublicacionID, SeccionID, kFalse,  document,  GuiaNota);
	
		
		
		bar.SetPosition(75);
		///////////////////////////////////////////
		//CheckOut demas Familiares sobre la pagina
		///////////////////////////////////////////
		
			PreferencesConnection PrefConections;
			
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument No Preferences");
				break;
			}
		
							
		//CAlert::InformationAlert("Test8");	
			PMString StringConection="";
		
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			if(SQLInterface==nil)
			{
				break;
			} 
			K2Vector<PMString> IdFamilyArticle;	//Id_Elemento="+StructNota_IDFrames[0].ID_Elemento_padre+"  OR
			PMString ConsultaFamily="SELECT Id_Elemento FROM Elemento WHERE Id_Elemento_padre="+StructNota_IDFrames[0].ID_Elemento_padre+ " AND id_tipo_ele IN(10,4) ";//UNION SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre IN(SELECT ID_Elemento FROM Elemento WHERE Id_Elemento_padre="+StructNota_IDFrames[0].ID_Elemento_padre+" AND id_tipo_Ele=10)";

			PMString NotaPrincipal=StructNota_IDFrames[0].ID_Elemento_padre;
			//CAlert::InformationAlert(ConsultaFamily);
			if(!SQLInterface->SQLQueryDataBase(StringConection,ConsultaFamily,IdFamilyArticle))
			{
				break;
			}
			
			if(IdFamilyArticle.Length()>0)
			{
				for(int32 j=0;j<IdFamilyArticle.Length();j++)
				{
					PMString ID_Elemento_PadreAA = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",IdFamilyArticle[j]);
					
					//PMString jString="";
					//jString.AppendNumber(j);
					//jString.Append(" , ");
					//jString.AppendNumber(IdFamilyArticle.Length());
					//CAlert::InformationAlert(ID_Elemento_PadreAA+"=="+StructNota_IDFrames[0].ID_Elemento_padre+"     "+jString);
					
					//PMString ID_Elemento_PadreAA = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",IdFamilyArticle[j]);
					//if(GetElementosNotaDXMP_Of_IDElementoPadre(ID_Elemento_PadreAA,	StructNota_IDFrames, document))
					
					if(GetElementosNotaDXMP_Of_IDElementoPadre(ID_Elemento_PadreAA,	StructNota_IDFrames, document) && NotaPrincipal!=StructNota_IDFrames[0].ID_Elemento_padre)
					{	//Check in de notas y actualiza Base de datos desde InDesign Con contenidos de notas y demas
						GuiaNota="";
						
						this->CheckInNotaDBAndPag(StructNota_IDFrames,Estado,RoutedTo,ID_Usuario,  PublicacionID, SeccionID, kFalse, document,GuiaNota);//V1034
						//this->CheckInNotaDBAndPag_CompletesElements(StructNota_IDFrames, Estado, RoutedTo, ID_Usuario,PublicacionID, SeccionID, kFalse,  document,  GuiaNota);
						//CAlert::InformationAlert("Pasa por aqui");
					}
					else
					{
						//Check in de Notassolo estado, seccion publicacion etc no contenidos
						//CAlert::InformationAlert("ASSXDCXX");
						//this->Insert_IN_Bitacora_Elemento("0000-00-00",ID_Elemento_PadreAA,ID_Usuario,  Estado,kFalse);
					}
				}
					 
			}
			
			bar.SetPosition(80);
		///////////////////////////////////////////
		
		//CAlert::InformationAlert("Test9");
		
		if(isTextFrameWithOverstShow)
		{			
			this->DoMuestraNotasConFrameOverset();
			//CAlert::InformationAlert("Muestra Frames whit overset");
		}
		
		//CAlert::InformationAlert("Test9");
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
			break;
		
		//CAlert::InformationAlert("Test10");
		//OBTIENE LA ULTIMA CONSULTA PARA EL LLENADO DE LA LISTA DE NOTAS EN LA PALETA
		PMString Busqueda = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlBusquedaTextWidgetID);
		
		if(Busqueda.NumUTF16TextChars()>0)
			N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);			
		else
			N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
		
		
		//CAlert::InformationAlert("Tes11");
		//OBTIENE EL ID DE LA ULTIMA NOTA QUE FUE SELECCIONADA PARA MOSTRARLAEN LA PALETA
		PMString IDNotaStri = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
		
		if(IDNotaStri.NumUTF16TextChars()>0)
		{
				
			
	 		if(N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID))
	 		{	//hacer busquedas para pases
	 				//N2PSQLUtilities::LlenarElementosDePasesEnVentanaN2PSQL();
	 		}
	 		else
	 		{
				Busqueda= "CALL QueryElementdNotas2 (" + IDNotaStri +")";
	 			N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kFalse);
	 		}
	 	
		}
		
		bar.SetPosition(90);
		//CAlert::InformationAlert("Test12");
		checkIn->AutomaticSaveRevi(document);
		
		//CAlert::InformationAlert("Test13");
		retval = kTrue;
		
		TimeExpGuardaYMAndaABaseDDatos.CurrentTime();

		uint64 seonds=(TimeExpGuardaYMAndaABaseDDatos.GetTime() - Time1.GetTime());///10000000
		

		PMString ASAQ="";
		ASAQ.AppendNumber(seonds);
		ASAQ.Append(" , ");
		ASAQ.AppendNumber(seonds,10);
		//CAlert::InformationAlert(ASAQ);
		
		this->Fotos_ActualizaGuiaDNota( document,GuiaNota,StructNota_IDFrames[0].ID_Elemento_padre);

	}while(false);
	bar.SetPosition(100);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInNota fin");
	return(retval);
}




////////////
/*
		-Actualiza a BD el contenido de los elementos de la nota,
		-Cambia el estado de escritura(Lapiz),
		-Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota, 
		-Libera la nota en la BD para ser modificad por cualquier otro usuario
			int32 IDNota, entero que indica el ID de la nota en la BD
*/
bool16 N2PsqlUpdateStorysAndDBUtils::CheckInNotaDBAndPag(StructIdFramesElementosXIdNota* VectorNota,PMString Estado,PMString RoutedTo,PMString IDUsuario,PMString PublicacionID,PMString SeccionID, bool16 ProvieneDeCrearPase, IDocument* document, PMString GuiaNota)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInNotaDBAndPag ini");
	bool16 retval=kFalse;
	do
	{	
		VectorNota[0].EsArticulo=kTrue;
		
		PMString Busqueda="";
		
		PMString Conte_Titulo="";
		PMString Conte_Balazo="";
		PMString Conte_PieFoto="";
		PMString Conte_Contenido="";
		
		PMString LastCheckinNotaToXMP;//=Params[15];
		PMString Fecha_Edicion="0";
		LastCheckinNotaToXMP=Fecha_Edicion;
		
		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_Nota);
				
		//Es InCopy solo modifica Texto
		PMString CAl="";
		CAl.AppendNumber(VectorNota[0].UIDFrContentNote);
		
		Fecha_Edicion=MySQLFecha;
		if(VectorNota[0].UIDFrPieFotoNote>0)
		{
			if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[0].  ID_Elemento_padre,  VectorNota[0].Id_Elem_PieFoto_Note, VectorNota[0].UIDFrPieFotoNote, Fecha_Edicion , Estado, RoutedTo, IDUsuario, PublicacionID, SeccionID, ProvieneDeCrearPase, document,Conte_PieFoto,kTrue,GuiaNota))			
			{
				//CAlert::InformationAlert(" CheckInNotaDBAndPag UIDFrPieFotoNote XXX= " + CAl);
				ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
			}
			VectorNota[0].EsArticulo=kFalse;
		}
		
		
		if(Fecha_Edicion!=MySQLFecha)
			LastCheckinNotaToXMP=Fecha_Edicion;

		Fecha_Edicion=MySQLFecha;
		
		CAl="";
		CAl.AppendNumber(VectorNota[0].UIDFrTituloNote);
		if(VectorNota[0].UIDFrTituloNote>0)
			if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[0].ID_Elemento_padre,  VectorNota[0].Id_Elem_Tit_Note, VectorNota[0].UIDFrTituloNote, Fecha_Edicion , Estado, RoutedTo, IDUsuario, PublicacionID, SeccionID, ProvieneDeCrearPase, document,Conte_Titulo,kTrue,GuiaNota))			
			{
				//CAlert::InformationAlert(" CheckInNotaDBAndPag UIDFrTituloNote XXX= " + CAl);
				ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
			}
			
		CAl="";
		CAl.AppendNumber(VectorNota[0].UIDFrPieFotoNote);
		
		if(Fecha_Edicion!=MySQLFecha)
			LastCheckinNotaToXMP=Fecha_Edicion;

		Fecha_Edicion=MySQLFecha;
		
		if(VectorNota[0].UIDFrContentNote>0)//UIDFrContentNote
		{
			if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[0].ID_Elemento_padre,  VectorNota[0].Id_Elem_Cont_Note, VectorNota[0].UIDFrContentNote, Fecha_Edicion , Estado, RoutedTo, IDUsuario, PublicacionID, SeccionID, ProvieneDeCrearPase, document,Conte_PieFoto,kTrue,GuiaNota))			
			{
				//CAlert::InformationAlert(" CheckInNotaDBAndPag UIDFrPieFotoNote XXX= " + CAl);
				ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
			}
		}
			
		//*****
		if(Fecha_Edicion!=MySQLFecha)
			LastCheckinNotaToXMP=Fecha_Edicion;

		Fecha_Edicion=MySQLFecha;
		
		CAl="";
		CAl.AppendNumber(VectorNota[0].UIDFrBalazoNote);
		if(VectorNota[0].UIDFrBalazoNote>0)
			if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[0].ID_Elemento_padre,  VectorNota[0].Id_Elem_Balazo_Note, VectorNota[0].UIDFrBalazoNote, Fecha_Edicion , Estado, RoutedTo, IDUsuario, PublicacionID, SeccionID, ProvieneDeCrearPase, document,Conte_Balazo,kTrue,GuiaNota))			
			{
				//CAlert::InformationAlert(" CheckInNotaDBAndPag UIDFrBalazoNote XXX= " + CAl);
				ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
			}
		
		
		
		if(Fecha_Edicion!=MySQLFecha)
			LastCheckinNotaToXMP=Fecha_Edicion;
		
		//CAlert::InformationAlert("ZZZZZZZ"+Estado);
		this->Insert_IN_Bitacora_Elemento(LastCheckinNotaToXMP,VectorNota[0].ID_Elemento_padre,IDUsuario,Estado,kFalse);
		
		Fecha_Edicion="0";
				
		if(LastCheckinNotaToXMP.NumUTF16TextChars()<=0)
		{
			break;
		}		 
				
		N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[0].ID_Elemento_padre,LastCheckinNotaToXMP, document);
		
		PMString Label="";
		if(RoutedTo.NumUTF16TextChars()>0)
			Label= RoutedTo+ "/" +Estado;
		else
			Label= Estado;
		
		if(!CambiaEstadoYLapizDeNota(VectorNota,kTrue,Label,kTrue))
		{
			break;
		}			
		
		
		if(VectorNota[0].EsArticulo)
			this->BuscaRutaDeElementoYSubeRespaldoNota(VectorNota[0].ID_Elemento_padre, Conte_Titulo, Conte_Balazo, Conte_PieFoto, Conte_Contenido,"","","","");		
		
		
		retval=kTrue;
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInNotaDBAndPag fin");
	return(retval);
}







bool16 N2PsqlUpdateStorysAndDBUtils::BuscaRutaDeElementoYSubeRespaldoNota(PMString IdElemento,
											PMString Conte_Titulo,
											PMString Conte_balazo,
											PMString Conte_PieFoto,
											PMString Conte_Contenido,
											PMString FechCreacion,
											PMString Publicacion,
											PMString Seccion,
											PMString Creador)
{
	bool16 retval=kFalse;
	do
	{
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
			
		//Arma la cadena para la conexion ODBC
		PMString StringConection="";
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
	
					
		PMString RutaElemento="";
		K2Vector<PMString> VectorRutaElem;
 		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		
		if(SQLInterface->SQLQueryDataBase(StringConection , "SELECT Ruta_Elemento FROM Elemento WHERE Id_Elemento="+IdElemento, VectorRutaElem))
		{
			for(int32 j=0;j<VectorRutaElem.Length();j++)
			{		
				RutaElemento=SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",VectorRutaElem[j]);					
			}
		
			this->SubeRespaldoDeNota(IdElemento,//Id elemento Nota
											Conte_Titulo,//Contenido del titulo
											Conte_balazo,//Contenido del Balazo
											Conte_PieFoto,//Contenido del pir de foto
											Conte_Contenido,//Contenido de la nota
											FechCreacion,//Fecha de creacion
											Publicacion,//Nombre de Publiucacion
											Seccion,//Nombre de seccion
											Creador,//Nombre del usuario Creador
											RutaElemento);
											
									
			SQLInterface->SQLSetInDataBase(StringConection,"UPDATE Elemento SET Ruta_Elemento='"+RutaElemento+"' WHERE Id_Elemento="+IdElemento);
				
		}
	}while(false);
	return retval;
}			
					

bool16 N2PsqlUpdateStorysAndDBUtils::SubeRespaldoDeNota(PMString IdElemento,//Id elemento Nota
											PMString tituloC,//Contenido del titulo
											PMString summaryC,//Contenido del Balazo
											PMString photocaptionC,//Contenido del pir de foto
											PMString storyC,//Contenido de la nota
											PMString fechaC,//Fecha de creacion
											PMString nombreP,//Nombre de Publiucacion
											PMString nombreS,//Nombre de seccion
											PMString creator,//Nombre del usuario Creador
											PMString& Ruta_Elemento)//Ruta Default o que ya se encuentra en la base de datos
{
	bool16 retval=kFalse;
	do
	{
		FILE *ArchivoPref;
		
		//Obtiene las Preferencias de coneccion  a BD
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
			
		//Arma la cadena para la conexion ODBC
		PMString StringConection="";
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		PMString string=tituloC+"\n"+summaryC+"\n"+photocaptionC+"\n"+storyC;
		
		PMString PathFieStr="";
		if(Ruta_Elemento.NumUTF16TextChars()<=0)
		{
			
			PathFieStr= N2PSQLUtilities::CrearFolderRespaldoNotas(PrefConections.RutaDServidorRespNotas,
															fechaC,
															nombreP,
															nombreS,
															creator);			
			SDKUtilities::AppendPathSeparator(PathFieStr);
			PathFieStr.Append(IdElemento+".txt");		
			
			
		}
		else
		{
			PathFieStr = PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(PathFieStr);
			PathFieStr.Append("respaldo_notas");
			//SDKUtilities::AppendPathSeparator(PathFieStr);
			PMString XASDR=Ruta_Elemento;
			XASDR.Remove(0,1);
#ifdef MACINTOSH			
			SDKUtilities::convertToMacPath(XASDR);
#endif			
			
			SDKUtilities::AppendPathSeparator(PathFieStr);
			PathFieStr.Append(XASDR);
			//PathFieStr=InterlasaUtilities::UnixToMac(PathFieStr);
			
			
			
		}
		PMString AS="";
		AS=PrefConections.PathOfServerFile;
		SDKUtilities::AppendPathSeparator(AS);
		AS.Append("respaldo_notas");
		SDKUtilities::AppendPathSeparator(AS);
		
		
		
		Ruta_Elemento=PathFieStr;
		Ruta_Elemento.Remove(0,AS.NumUTF16TextChars()-1);
		Ruta_Elemento=InterlasaUtilities::MacToUnix(Ruta_Elemento);
		
		//CAlert::InformationAlert("C : "+PathFieStr);
		//CAlert::InformationAlert("D : "+Ruta_Elemento);
		if((ArchivoPref=FileUtils::OpenFile(PathFieStr.GrabCString(),"w"))!=NULL)
		{
		
			fprintf(ArchivoPref,"%s\n",string.GrabCString());
		
			fclose(ArchivoPref);
			
			#ifdef MACINTOSH
				chmod(InterlasaUtilities::MacToUnix(PathFieStr).GrabCString(),0777);
			#endif
			
			retval=kTrue;
		}
		else
		{
			//CAlert::InformationAlert("ParametrosDeDialogosN2PSQL.pfa Functiuon No pudo Abrir Archivo para guardar preferencia");
			retval=kFalse;
		}
		
	}while(false);
	return retval;
}


		

/*******************/

bool16 N2PsqlUpdateStorysAndDBUtils::OpenDlgCheckInNota(PMString& Status,PMString& Dirigido, PMString &PublicacionID, PMString &SeccionID,PMString &GuiaNota)
{
	bool16 CheckInDlg=kFalse;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		
		
		
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}
		
		
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PsqlPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kN2PsqlCheckInArticleDlgResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);
		
		
		

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		
		
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
		
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		
		dialog->WaitForDialog();
		//Consulta Parametros
		
		PMString IsCancel=dialogController->GetTextControlData(kN2PsqlDlgChekInNotaCancelWidgetID);
		//CAlert::InformationAlert(IsCancel);
		if(IsCancel=="Cancel" || IsCancel=="Cancelar")
		{
			CheckInDlg=kFalse;
			break;
		}
		
		Dirigido = dialogController->GetTextControlData(kN2PsqlComboRoutedToWidgetID);
		Status = dialogController->GetTextControlData(kN2PsqlTextStatusWidgetID);
		PublicacionID = dialogController->GetTextControlData(kN2PsqlComboPubliWidgetID);
		SeccionID = dialogController->GetTextControlData(kN2PsqlComboSeccionWidgetID);
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument No Preferences");
			break;
		}
		
		if(PrefConections.N2PUtilizarGuias==1)
			GuiaNota=dialogController->GetTextControlData(kN2PsqlComboguiaWidgetID);
		else
			GuiaNota=dialogController->GetTextControlData(kN2PsqlTextEnGuiaWidgetID);
		
		CheckInDlg=kTrue;
	} while (false);			
	return(CheckInDlg);

}




bool16 N2PsqlUpdateStorysAndDBUtils::OpenDlgDetachElemento()
{
	bool16 CheckInDlg=kFalse;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		
		
		
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}
		
		
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PsqlPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kN2PsqlDetachArticleDlgResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);
		
		
		

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		
		
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
		
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		
		dialog->WaitForDialog();
		//Consulta Parametros
		
		PMString IsCancel=dialogController->GetTextControlData(kN2PsqlDlgChekInNotaCancelWidgetID);
		//CAlert::InformationAlert(IsCancel);
		if(IsCancel=="Cancel" || IsCancel=="Cancelar")
		{
			CheckInDlg=kFalse;
			break;
		}
			
		CheckInDlg=kTrue;
	} while (false);			
	return(CheckInDlg);

}

//Checa las notas que se estan editando actualmente 
//Guarda el contenido de cada uno de los elementos de la nota en la BD
bool16 N2PsqlUpdateStorysAndDBUtils::CheckInNotasEnEdicion(bool16 IsUpdate)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInNotasEnEdicion ini");
	
	bool16 retval=kFalse;
	bool16 existenNotasEditandose=kFalse;
	PMString ItemToSearchString="";
	PMString UIDModelString="";
	PMString IDNotaString="";
	PMString Busqueda="";
	PMString IDUsuario="";	
	PMString Aplicacion="";	
	PMString GuiaNota="";
	do
	{
		//CAlert::InformationAlert("Haber antes de aqui para tratar de borrar el Update lista");
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		PMString Fecha_Edicion_NotaXX =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_NotaXX);
	
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		Aplicacion = app->GetApplicationName();
		
		Aplicacion.SetTranslatable(kFalse);
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}
	
	
		
		
		
		
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		K2Vector<int32> Notas;	
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		if(contNotasInVector>0)
		{
			//CAlert::InformationAlert("");
				//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
				for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
				{	
					
					if(!this->CanDoCheckInNota(VectorNota[numNota].UIDFrContentNote) && 
						!this->CanDoCheckInNota(VectorNota[numNota].UIDFrTituloNote) &&
						!this->CanDoCheckInNota(VectorNota[numNota].UIDFrPieFotoNote) &&
						!this->CanDoCheckInNota(VectorNota[numNota].UIDFrBalazoNote))
					{	//
						//CAlert::InformationAlert("SE supone que no hixzo Update");
						continue;
					}
					else
					{
						//CAlert::InformationAlert("Pues se supone	ue si hubo update ");
						existenNotasEditandose=kTrue;
						//Verifica que el ultimo usuario que retiro la nota sea el mismo que esta depositandola
						//Validsacion Guarachin
						/*if(!ValidaSiUsuario_P_CheckInEnBaseDDatos(VectorNota[numNota].ID_Elemento_padre,IDUsuario))
						{
							//Significa que alguien mas retiro la nota por lo tanto no puede modificar nada sobre la base de datos.
							//Debe de notificar al usuario que no puede depositar la nota y que esta sera deshabilitada para no generar 
							//Errores en la versiones de las nota
							CAlert::InformationAlert(kN2PSQLNotaRetiradaPorOtroUsuarioDepositoVisualStringKey);
							
							StructIdFramesElementosXIdNota* VectorCheckInNoteToChangeEstatus=new StructIdFramesElementosXIdNota[1];	
							
							VectorCheckInNoteToChangeEstatus[0]=VectorNota[numNota];
								
							if(!CambiaEstadoYLapizDeNota(VectorCheckInNoteToChangeEstatus,kTrue,"",kFalse))
							{
								continue;
							}
							
							continue;
						}*/
						
						
						
						PMString Conte_Titulo="";
						PMString Conte_balazo="";//Contenido del Balazo
						PMString Conte_PieFoto="";//Contenido del pir de foto
						PMString Conte_Contenido="";//Contenido de la nota
					
						//CAlert::InformationAlert("SE supone que no hixzo Update");
						PMString LastCheckinNotaToQuery="";
						PMString Fecha_Edicion="0";
						LastCheckinNotaToQuery=Fecha_Edicion;
						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrContentNote>0)
							if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Cont_Note, VectorNota[numNota].UIDFrContentNote, Fecha_Edicion , "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Contenido,IsUpdate,GuiaNota))			
								ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
			
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;

						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrTituloNote>0)
							if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Tit_Note, VectorNota[numNota].UIDFrTituloNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Titulo,IsUpdate,GuiaNota))			
								ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
						
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;

						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrPieFotoNote>0)
							if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_PieFoto_Note, VectorNota[numNota].UIDFrPieFotoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_PieFoto,IsUpdate,GuiaNota))			
								ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
						
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;

						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrBalazoNote>0)
							if(!this->EstableceParametrosToCheckInElementoNota(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Balazo_Note, VectorNota[numNota].UIDFrBalazoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_balazo,IsUpdate,GuiaNota))			
									ASSERT_FAIL("Ocurrio un error al intentar mandar contenido a la base de datos");
						
						
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;

						Fecha_Edicion=MySQLFecha;
						//CAlert::InformationAlert("YYYYYY"+LastCheckinNotaToQuery);
						this->Insert_IN_Bitacora_Elemento(LastCheckinNotaToQuery,VectorNota[numNota].ID_Elemento_padre,IDUsuario,"0",IsUpdate);
						if(LastCheckinNotaToQuery.NumUTF16TextChars()>0)
						{
								StructIdFramesElementosXIdNota* VectorCheckInNoteToChangeEstatus=new StructIdFramesElementosXIdNota[1];	
								VectorCheckInNoteToChangeEstatus[0]=VectorNota[numNota];
							
								if(!CambiaEstadoYLapizDeNota(VectorCheckInNoteToChangeEstatus,kTrue,"",kFalse))
								{
									//CAlert::InformationAlert("A chinga se sale po aca cabron no se por que");
									break;
								}
							
							
									
								if(LastCheckinNotaToQuery.NumUTF16TextChars()>0)
								{
									N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[numNota].ID_Elemento_padre,LastCheckinNotaToQuery, document);
								}
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrTituloNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrTituloNote);
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrPieFotoNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrPieFotoNote);
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrBalazoNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrBalazoNote);
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrContentNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrContentNote);
								
					}
				}
							
			}
				
				this->guardarIDNotasPUpdateDesign(Notas);
		}
		
		//CAlert::InformationAlert("awebo que tiene que pasat pot aca cabron");
		//Para Borrara ek Archivo UpdateDesign.pfd pues se quedaba con notas anteriores
		if(!existenNotasEditandose)
		{
			//Debe borrar el archivo UpdateDesign.pfd para que no se habran notas cuando ya habian sido depositadas
			PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
			PathFile.Append("UpdateDesign.pfd");
			
			if ( remove(InterlasaUtilities::MacToUnix(PathFile).GrabCString()) != 0 )
				ASSERT_FAIL("Error al intentar borrar archivo UpdateDesign.pfd");
			
			//unlink(PathFile.GrabCString());
			
			K2Vector<int32> NotasVacias;
			NotasVacias.push_back(0);
			this->guardarIDNotasPUpdateDesign(NotasVacias);
			//CAlert::InformationAlert(PathFile);
		}

		retval=kTrue;
		//delete VectorNota;
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInNotasEnEdicion fin");
	return(retval);
}


//Guarda los ID de los frames que componen una nota cuando se hace un Update design para volver a 
//poner estos frame como editando
void  N2PsqlUpdateStorysAndDBUtils::guardarIDNotasPUpdateDesign(K2Vector<int32> Notas)
{
	bool16 retval=kFalse;
	FILE *ArchivoPref;
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	PathFile.Append("UpdateDesign.pfd");
	if(Notas.Length()>0)
	{
		if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"wb"))!=NULL && Notas.Length()>0)
		{
			for(int32 i=0;i<Notas.Length();i++)
			{
				int32 NotaID=Notas[i];
				PMString ss="";
				ss.AppendNumber(NotaID);
				fwrite((char*)&NotaID,sizeof(int32),1,ArchivoPref);
				
			}	
		
			fclose(ArchivoPref);
			
			chmod(InterlasaUtilities::MacToUnix(PathFile).GrabCString(),0777);
		}
		else
		{
			CAlert::InformationAlert("UpdateDesign.pfd Functiuon No pudo Abrir Archivo para guardar preferencia");
		}
	}
	
}

//Obtiene los ID de los frames que componen una nota cuando se hace un Update design para volver a 
//poner estos frame como editando
bool16  N2PsqlUpdateStorysAndDBUtils::ObtenerIDNotasPUpdateDesign(K2Vector<int32>& Notas)
{
	bool16 retval=kFalse;
	FILE *ArchivoPref;
	int32 Cantidad=0;
	int32 IDNota=0;
	 
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	
	PathFile.Append("UpdateDesign.pfd");
	
	if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"rb"))!=NULL)
	{
		
		fread((char*)&IDNota,sizeof(int32),1,ArchivoPref);
		Notas.push_back(IDNota);
		Cantidad++;
		while(/*!ferror(FileToClasidesFluidos) && */!feof(ArchivoPref))
		{	
			fread((char*)&IDNota,sizeof(int32),1,ArchivoPref);
				
			Notas.push_back(IDNota);
			Cantidad++;
		}	
		fclose(ArchivoPref);
		retval=kTrue;
	}
	if(retval==kTrue)
	{
		
		remove(InterlasaUtilities::MacToUnix(PathFile).GrabCString());
	}
		
	
	return(retval);
}



bool16 N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasOnDocuments()
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasOnDocuments ini");
	bool16 actualiza=kFalse;
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		} 
		
		//InterfacePtr<IDocumentList> DocList(app->QueryDocumentList());
		//if(DocList==nil)
		//{
		//	ASSERT_FAIL("DocList");
		//	break;
		//}
		
		//for(int32 contDoc=0; contDoc<DocList->GetDocCount() && actualiza==kFalse;contDoc++)
		//{
			
			//IDocument* document = DocList->GetNthDoc(contDoc);
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
		
			actualiza=SeRequiereActualizacionDeNotasDeDocument( document);
			
			PMString NameOfDocument="";
			if(actualiza)
			{
				document->GetName(NameOfDocument); 
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID, kN2PsqlStcTextActualizaPagsWidgetID,NameOfDocument);
			}
			else
			{
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID, kN2PsqlStcTextActualizaPagsWidgetID,NameOfDocument);
			}
			
		///}
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasOnDocuments fin");

	return(actualiza);
}





/*
*/
bool16 N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument(IDocument* document)
{
	//N2PSQLUtilities::ImprimeMensaje("ini SeRequiereActualizacionDeNotasDeDocument se necesita actualizacion de fotos");
	
	bool16 actualiza=kFalse;
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		} 
		
		
		
		
		
			PMString Aplicacion=app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);
			
			//CAlert::InformationAlert("pasa por aca");
			if(this->Fotos_SeRequiereActualizacionPregunta(document)==kTrue && Aplicacion.Contains("InCopy")==kFalse)
			{	
					//CAlert::InformationAlert("Actualiza potor por fa");
					//N2PSQLUtilities::ImprimeMensaje("Fotos_SeRequiereActualizacionPregunta se necesita actualizacion de fotos");
					actualiza=kTrue;
					break;
			}	
			//CAlert::InformationAlert("pasa por aca tambien");
			int32 numReg=0;
		
			int32 contNotasInVector=30;
		
			StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
			
			VectorNota[0].DSNNote="";
		
			this->LlenarVectorElemDNotasFluidas(VectorNota,contNotasInVector, document);
		
		
			if(contNotasInVector>0)
			{
		
				for(numReg=0;numReg<contNotasInVector  && actualiza==kFalse;numReg++)
				{	
					
					if(VectorNota[numReg].DSNNote.NumUTF16TextChars()<=0)
					{
						continue;
					}

					int32 index=0;
					PMString texto;
					PMString UIDModelString="";
					PMString IDNotaString="";
					PMString ItemToSearchString="";
					int32 UIDModel=0;
					int32 IDNota=0;
					PMString NameDoc="";
					int32 IndexString=0;
			
					
					//PMString Consulta="SELECT CONVERT(varchar,Fecha_ult_mod,121) AS Fecha_ult_mod FROM Elemento Where Id_Elemento='";
					PMString Consulta="";
					if(VectorNota[numReg].Id_Elem_PieFoto_Note.NumUTF16TextChars()<=0 || VectorNota[numReg].Id_Elem_PieFoto_Note=="" || VectorNota[numReg].Id_Elem_PieFoto_Note=="0")
					{
						Consulta="SELECT DATE_FORMAT(Fecha_ult_mod,'%Y-%m-%d %H:%i:%s.%f') AS Fecha_ult_mod FROM Elemento Where Id_Elemento=";
						Consulta.Append(VectorNota[numReg].ID_Elemento_padre);
					}
					else
					{
						Consulta="SELECT DATE_FORMAT(Fecha_ult_mod,'%Y-%m-%d %H:%i:%s.%f') AS Fecha_ult_mod FROM Elemento Where Id_Elemento=";
						Consulta.Append(VectorNota[numReg].Id_Elem_PieFoto_Note);					
					}
					
				
				
				
				
							
				
					PreferencesConnection PrefConections;
			
					if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
					{
						ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument No Preferences");
						break;
					}
		
							
				
					PMString StringConection="";
		
					StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
						
				
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					if(SQLInterface==nil)
					{
						break;
					} 
					K2Vector<PMString> QueryVector;
					
					//CAlert::InformationAlert(StringConection);
					//CAlert::InformationAlert(Consulta);
					SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
			
				
					for(int32 j=0;j<QueryVector.Length() && actualiza==kFalse;j++)
					{
					
					
						//Hacer comparacion entre las fechas para verificar si se ha modificado la nota desde la ultima vez que fue actualizada
						PMString UltimoCheckIn = SQLInterface->ReturnItemContentbyNameColumn("Fecha_ult_mod",QueryVector[j]);
					
						//
						if(VectorNota[numReg].LastCheckInNote!=UltimoCheckIn)
						{
							//CAlert::InformationAlert(VectorNota[numReg].LastCheckInNote + " != " + UltimoCheckIn+"\nNotaID:"+VectorNota[numReg].ID_Elemento_padre);
							actualiza=kTrue;
						}
					}
				 
				}
				
				if(Aplicacion.Contains("InCopy"))
				{
					//Para InDicar que hubo un cambio de DiseÒo
					if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(document)))
					{
						//CAlert::InformationAlert("CanDoUpdateDesign 1");
						actualiza=kTrue;
					}
						
				}
			
			}	
			else
			{
				if(Aplicacion.Contains("InCopy"))
				{	
					//Para InDicar que hubo un cambio de DiseÒo
					if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(document)))
					{
						//CAlert::InformationAlert("CanDoUpdateDesign 2");
						actualiza=kTrue;
					}
						
				}	
			}
			//delete VectorNota;
			
		
	}while(false);
	
	
	return(actualiza);
}





/*
*/

PMString N2PsqlUpdateStorysAndDBUtils::ARMA_UPDATE_NOTA_QUERY(StructIdFramesElementosXIdNota* VectorNota,int32 numReg, bool16 IsCheckOut)
{
	PMString Consulta="";
	//CAlert::InformationAlert("!"+VectorNota[numReg].Id_Elem_PieFoto_Note+"¡");
	if(VectorNota[numReg].Id_Elem_PieFoto_Note.NumUTF16TextChars()>0)
	{//Para cuando es pie de foto
		Consulta="CALL GetEstatusYFechaUltModDElem("+VectorNota[numReg].Id_Elem_PieFoto_Note+",@A,@X,@Y);";
	}
	else
	{
		if(VectorNota[numReg].ID_Elemento_padre.NumUTF16TextChars()>0)
			Consulta="CALL GetEstatusYFechaUltModDElem("+VectorNota[numReg].ID_Elemento_padre+",@A,@X,@Y);";
	}
	
	/*Consulta.Append(" DECLARE @Fecha_Ult_Mod AS VARCHAR(30)");
	 Consulta.Append(" DECLARE @Id_Estatus AS VARCHAR(30)");
	 
	 
	 Consulta.Append(" SET @Fecha_Ult_Mod =(SELECT CONVERT ( VARCHAR, Fecha_Ult_Mod, 121) FROM Elemento WHERE Id_Elemento ='" +VectorNota[numReg].ID_Elemento_padre +"' )");
	 Consulta.Append(" SET @Id_Estatus = (SELECT TOP 1 Id_Estatus FROM Bitacora_Elemento WHERE  Id_Elemento ='" + VectorNota[numReg].ID_Elemento_padre+ "' ORDER BY Fecha DESC)");
	 Consulta.Append(" SELECT @Id_Estatus AS Id_Estatus,");
	 Consulta.Append(" @Fecha_Ult_Mod AS  Fecha_Ult_Mod");*/
	return(Consulta);
}


/*
	Llenado de las estructura de las notas que han sido fluidas sobre el Documento, obteniendolas desde el XMP.
	StructIdFramesElementosXIdNota* VectorNota, Vector que guarda los datos de cada elemento de las notas fluidas sobre el Documento.
	int32 &contNotasInVector,	cantidad de elementos de las notas fluidas sobre el documento.
*/
void N2PsqlUpdateStorysAndDBUtils::LlenarVectorElemDNotasFluidas(StructIdFramesElementosXIdNota* VectorNota,int32 &contNotasInVector, IDocument* Document)
{
	PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", Document);
	if(CadenaCompleta.NumUTF16TextChars()>0)
	{
		int32 numReg=0;
		
		//Obtiene cantidad de elelementos en XMP
		int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
		
		//Crea estructura para registros en XMP
		ClassRegEnXMP *ListUpdate= new ClassRegEnXMP[contUpdate];
		
		//Limpia Estructura de Registros de elementos de nota en XMP
		for(numReg=0;numReg<contUpdate;numReg++)
		{
			ListUpdate[numReg].IDElemPadre="";
			ListUpdate[numReg].IDElemento="";
			ListUpdate[numReg].IDFrame=-1;
			ListUpdate[numReg].TipoDato="";
			ListUpdate[numReg].NameDSN="";
			ListUpdate[numReg].LastCheckInNote="";
		}
		
		//Llena estrcutura de registros de XMP
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
		
		//Limpiar Estructura de Notas
		for(numReg=0;numReg<contNotasInVector;numReg++)
		{
			VectorNota[numReg].ID_Elemento_padre = "";		//Id_Elemento_padre
			VectorNota[numReg].Id_Elem_Cont_Note= "";	//Id_Elemento que contiene el contenido de la nota
			VectorNota[numReg].Id_Elem_Tit_Note= "";//Id
			VectorNota[numReg].Id_Elem_PieFoto_Note= "";
			VectorNota[numReg].Id_Elem_Balazo_Note= "";
	
			VectorNota[numReg].UIDFrTituloNote=-1;				//UID Frame en donde fue fluido el titulo de la nota
			VectorNota[numReg].UIDFrPieFotoNote=-1;
			VectorNota[numReg].UIDFrBalazoNote=-1;
			VectorNota[numReg].UIDFrContentNote=-1;
	
			VectorNota[numReg].LastCheckInNote= "";
	
			VectorNota[numReg].DSNNote= "";
		}
		
	
	/////////////////////////////////////////////////////////////////////////
		//Crea estructura temporal de Nota
		StructIdFramesElementosXIdNota Nota;
		
		Nota.ID_Elemento_padre = "";		//Id_Elemento_padre
		Nota.Id_Elem_Cont_Note= "";	//Id_Elemento que contiene el contenido de la nota
		Nota.Id_Elem_Tit_Note= "";//Id
		Nota.Id_Elem_PieFoto_Note= "";
		Nota.Id_Elem_Balazo_Note= "";
	
		Nota.UIDFrTituloNote=-1;				//UID Frame en donde fue fluido el titulo de la nota
		Nota.UIDFrPieFotoNote=-1;
		Nota.UIDFrBalazoNote=-1;
		Nota.UIDFrContentNote=-1;
		Nota.LastCheckInNote= "";
	
		Nota.DSNNote= "";
		Nota.EsArticulo=kTrue;
		
		//Obtiene el Id de la nota y el DNS siempre icuando el tipo de elemento sea diferente a una foto
		for(numReg=0;numReg<contUpdate;numReg++)
		{
			if(ListUpdate[numReg].TipoDato!="Foto" && ListUpdate[numReg].TipoDato!="PieFoto")
			{
				Nota.ID_Elemento_padre=ListUpdate[0].IDElemPadre;
				Nota.DSNNote=ListUpdate[0].NameDSN;
				Nota.LastCheckInNote=ListUpdate[0].LastCheckInNote;
				numReg = contUpdate;
			}
			else
			{
				continue;
			}
		}
		
		
		
		for(numReg=0;numReg<contUpdate;numReg++)
		{
			if(Nota.ID_Elemento_padre==ListUpdate[numReg].IDElemPadre && Nota.DSNNote==ListUpdate[numReg].NameDSN)
			{
				if(ListUpdate[numReg].TipoDato=="Titulo")
				{
					
					Nota.UIDFrTituloNote=ListUpdate[numReg].IDFrame;
					Nota.Id_Elem_Tit_Note=ListUpdate[numReg].IDElemento;
				}
				else
				{
					if(ListUpdate[numReg].TipoDato=="Balazo")
					{
							
							Nota.UIDFrBalazoNote=ListUpdate[numReg].IDFrame;
							Nota.Id_Elem_Balazo_Note=ListUpdate[numReg].IDElemento;
					}
					else
					{
						if(ListUpdate[numReg].TipoDato=="PieFoto")
						{
							
							Nota.UIDFrPieFotoNote=ListUpdate[numReg].IDFrame;
							Nota.Id_Elem_PieFoto_Note=ListUpdate[numReg].IDElemento;
							Nota.EsArticulo=kFalse;
						}
						else
						{
							if(ListUpdate[numReg].TipoDato=="ContenidoNota")
							{
								Nota.UIDFrContentNote=ListUpdate[numReg].IDFrame;
								Nota.Id_Elem_Cont_Note=ListUpdate[numReg].IDElemento;
							}
						}
					}
				}
			}
		}
			
			
			VectorNota[0].UIDFrBalazoNote=Nota.UIDFrBalazoNote;
			
			VectorNota[0].UIDFrContentNote=Nota.UIDFrContentNote;
			
			VectorNota[0].DSNNote=Nota.DSNNote;
			VectorNota[0].ID_Elemento_padre=Nota.ID_Elemento_padre;
			VectorNota[0].UIDFrPieFotoNote=Nota.UIDFrPieFotoNote;
			VectorNota[0].UIDFrTituloNote=Nota.UIDFrTituloNote;
			VectorNota[0].LastCheckInNote =Nota.LastCheckInNote;
			
			VectorNota[0].Id_Elem_Tit_Note = Nota.Id_Elem_Tit_Note;
			VectorNota[0].Id_Elem_Balazo_Note = Nota.Id_Elem_Balazo_Note;
			VectorNota[0].Id_Elem_PieFoto_Note = Nota.Id_Elem_PieFoto_Note;
			VectorNota[0].Id_Elem_Cont_Note = Nota.Id_Elem_Cont_Note;
			VectorNota[0].EsArticulo = Nota.EsArticulo;
		
			contNotasInVector=1;
			int32 numNota=0;
			
			for(numReg=0;numReg<contUpdate;numReg++)
			{
				
				bool16 se_Encontro=kFalse;
				for(numNota=0;numNota<contNotasInVector && se_Encontro==kFalse;numNota++)
				{
					
					if(VectorNota[numNota].ID_Elemento_padre==ListUpdate[numReg].IDElemPadre && 
					   VectorNota[numNota].DSNNote==ListUpdate[numReg].NameDSN
					  )
					{
						//ya se encuentra los Datos de esta Nota
						se_Encontro=kTrue;
					}
					else
					{
						se_Encontro=kFalse;	
					}
				}

				if(se_Encontro==kFalse)
				{
					
					if(ListUpdate[numReg].TipoDato!="Foto")
					{
						VectorNota[contNotasInVector].ID_Elemento_padre=ListUpdate[numReg].IDElemPadre;
						VectorNota[contNotasInVector].DSNNote=ListUpdate[numReg].NameDSN;
						VectorNota[contNotasInVector].LastCheckInNote=ListUpdate[numReg].LastCheckInNote;
						
						for(int32 numRegCop=0;numRegCop<contUpdate;numRegCop++)
						{
							if( VectorNota[contNotasInVector].ID_Elemento_padre==ListUpdate[numRegCop].IDElemPadre && 
								VectorNota[contNotasInVector].DSNNote==ListUpdate[numRegCop].NameDSN
							  )
							{
								if(ListUpdate[numRegCop].TipoDato=="Titulo")
								{
									VectorNota[contNotasInVector].UIDFrTituloNote = ListUpdate[numRegCop].IDFrame;
									VectorNota[contNotasInVector].Id_Elem_Tit_Note = ListUpdate[numRegCop].IDElemento;
								}
								else
								{
									if(ListUpdate[numRegCop].TipoDato=="Balazo")
									{
										VectorNota[contNotasInVector].UIDFrBalazoNote=ListUpdate[numRegCop].IDFrame;
										VectorNota[contNotasInVector].Id_Elem_Balazo_Note = ListUpdate[numRegCop].IDElemento;
									}
									else
									{
										if(ListUpdate[numRegCop].TipoDato=="PieFoto")
										{
											VectorNota[contNotasInVector].UIDFrPieFotoNote=ListUpdate[numRegCop].IDFrame;
											VectorNota[contNotasInVector].Id_Elem_PieFoto_Note = ListUpdate[numRegCop].IDElemento;
											VectorNota[contNotasInVector].EsArticulo = kFalse;
										}
										else
										{
											if(ListUpdate[numRegCop].TipoDato=="ContenidoNota")
											{
												VectorNota[contNotasInVector].UIDFrContentNote=ListUpdate[numRegCop].IDFrame;
												VectorNota[contNotasInVector].Id_Elem_Cont_Note = ListUpdate[numRegCop].IDElemento;
											}
										}
									}
								}
							}
						}	
						contNotasInVector++;
					}
					
				}
				
			}
		//delete ListUpdate;
	}
	else
	{
		contNotasInVector=0;
	}
	
}


/*
	Cantidad de notas fluidas

int32 N2PsqlUpdateStorysAndDBUtils::CantidadDNotasFluidas()
{	
	int32 contNotasInVector=0;
	StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[100];
	
	PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate");
	if(CadenaCompleta.NumUTF16TextChars()>0)
	{
		int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
		
		
		ClassRegEnXMP *ListUpdate= new ClassRegEnXMP[contUpdate];

		N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
	
		StructIdFramesElementosXIdNota Nota;

		Nota.ID_Elemento_padre=ListUpdate[0].IDElemPadre;
		
		
			
		Nota.DSNNote=ListUpdate[0].NameDSN;
		
		Nota.LastCheckInNote=ListUpdate[0].LastCheckInNote;
		
		int32 numReg=0;
		for(numReg=0;numReg<contUpdate;numReg++)
		{
			if(Nota.ID_Elemento_padre==ListUpdate[numReg].IDElemPadre && Nota.DSNNote==ListUpdate[numReg].NameDSN)
			{
				if(ListUpdate[numReg].TipoDato=="Titulo")
				{
					
					Nota.UIDFrTituloNote=ListUpdate[numReg].IDFrame;
					
				}
				else
				{
					if(ListUpdate[numReg].TipoDato=="Balazo")
					{
							
							Nota.UIDFrBalazoNote=ListUpdate[numReg].IDFrame;
							
						}
						else
						{
							if(ListUpdate[numReg].TipoDato=="PieFoto")
							{
								
								Nota.UIDFrPieFotoNote=ListUpdate[numReg].IDFrame;
								
							}
							else
							{
								if(ListUpdate[numReg].TipoDato=="ContenidoNota")
								{
									
									Nota.UIDFrContentNote=ListUpdate[numReg].IDFrame;
									
								}
							}
						}
					}
				}
			}
			
			
			VectorNota[0].UIDFrBalazoNote=Nota.UIDFrBalazoNote;
			
			VectorNota[0].UIDFrContentNote=Nota.UIDFrContentNote;
			
			VectorNota[0].DSNNote=Nota.DSNNote;
			VectorNota[0].ID_Elemento_padre=Nota.ID_Elemento_padre;
			VectorNota[0].UIDFrPieFotoNote=Nota.UIDFrPieFotoNote;
			VectorNota[0].UIDFrTituloNote=Nota.UIDFrTituloNote;
			VectorNota[0].LastCheckInNote =Nota.LastCheckInNote;
			
			contNotasInVector=1;
			int32 numNota=0;
			
			for(numReg=0;numReg<contUpdate;numReg++)
			{
				
				bool16 se_Encontro=kFalse;
				for(numNota=0;numNota<contNotasInVector && se_Encontro==kFalse;numNota++)
				{
					
					if(VectorNota[numNota].ID_Elemento_padre==ListUpdate[numReg].IDElemPadre && 
					   VectorNota[numNota].DSNNote==ListUpdate[numReg].NameDSN
					  )
					{
						//ya se encuentra los Datos de esta Nota
						se_Encontro=kTrue;
					}
					else
					{
						se_Encontro=kFalse;	
					}
				}

				if(se_Encontro==kFalse)
				{
						VectorNota[contNotasInVector].ID_Elemento_padre=ListUpdate[numReg].IDElemPadre;
						VectorNota[contNotasInVector].DSNNote=ListUpdate[numReg].NameDSN;
						VectorNota[contNotasInVector].LastCheckInNote=ListUpdate[numReg].LastCheckInNote;
						
						for(int32 numRegCop=0;numRegCop<contUpdate;numRegCop++)
						{
							if( VectorNota[contNotasInVector].ID_Elemento_padre==ListUpdate[numRegCop].IDElemPadre && 
								VectorNota[contNotasInVector].DSNNote==ListUpdate[numRegCop].NameDSN
							  )
							{
								if(ListUpdate[numRegCop].TipoDato=="Titulo")
								{
									VectorNota[contNotasInVector].UIDFrTituloNote=ListUpdate[numRegCop].IDFrame;
								}
								else
								{
									if(ListUpdate[numRegCop].TipoDato=="Balazo")
									{
										VectorNota[contNotasInVector].UIDFrBalazoNote=ListUpdate[numRegCop].IDFrame;
									}
									else
									{
										if(ListUpdate[numRegCop].TipoDato=="PieFoto")
										{
											VectorNota[contNotasInVector].UIDFrPieFotoNote=ListUpdate[numRegCop].IDFrame;
										}
										else
										{
											if(ListUpdate[numRegCop].TipoDato=="ContenidoNota")
											{
												VectorNota[contNotasInVector].UIDFrContentNote=ListUpdate[numRegCop].IDFrame;
											}
										}
									}
								}
							}
						}
						
						contNotasInVector++;
				}
			}
		//delete ListUpdate;
	}
	//delete	VectorNota;
	return(contNotasInVector);
}
*/








/**********************************************************************/

/**********************************************************************/

bool16 N2PsqlUpdateStorysAndDBUtils::DoMuestraNotasConFrameOverset()
{
	bool16 retval=kFalse;
	do
	{
		
		PMString ItemToSearchString="";
		PMString UIDModelString="";
		PMString IDNotaString="";
		PMString Busqueda="";
		PMString IDUsuario="";	
		PMString Aplicacion="";	
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);

		if(contNotasInVector>0)
		{
		
		
			InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
					(
						kN2PCTUtilitiesBoss,	// Object boss/class
						IN2PCTUtilities::kDefaultIID
					)));
			
			if(N2PCTUtils==nil)
			{
				break;
			}
				
			N2PCTUtils->MuestraNotasConFrameOverset(VectorNota, contNotasInVector);
				
		}
		else
		{
			//CAlert::InformationAlert(kN2PSQLNoExistenNotasSobreDocumentoStringKey);
		}
	
		//delete VectorNota;
		
	}while(false);
	return(retval);
}






bool16 N2PsqlUpdateStorysAndDBUtils::ImportarNota()
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ImportarNota ini");


	bool16 retval=kFalse;

		
	PreferencesConnection PrefConections;  	//Preferencias para la conexion a la base de datos Actual
	PreferencesConnection PrefConectionsRemota;	//Preferencias para la conexion a la base de datos Romota
	
	int32 numeroPaginaActual=0;
	PMString Estado="";						//Estado de la nota
	PMString NombreNota = "";
	PMString Busqueda="";					//Cadena para establecer el query a la base de datos
	int32 UIDTextModelOfTextFrame = 0;		//UID del TextModel del Frame seleccionado
	PMString StringConection="";			//Cadena para conexion a la base de datos
	
	PMString Servidor_Actual="";
	
	PMString FechaDCracionNota="";			//Fecha de la creacion de la nota en el servidor remoto
	PMString ID_UsuarioActual="";			//ID de usuario que se encuentra actualmente logeado
	PMString PublicacionID="";
	PMString SeccionID="";
	PMString DirigidoA="";					//Aq uien va dirigida la nota
	PMString ProveniendeD="";				//quien deposuita la nota
	PMString ID_Fuente_Nota="";				//Id de la fuente que proviene la nota
	PMString ID_Color_Nota="";				//Id_ del color que fue asignada la nota
	int32 Calificacion_Nota=0;				//Calificacion que fue asignada a la nota
	
	PMString GuiaNota="";
	
	PMString IdNotaDB = "";					//Id_Elemento_padre de la nota
	
	PMString CadenaTitulo="";				//Contenido de caracteres del titulo de la nota
	PMString CadenaBalazo="";				//Contenido de caracteres del balazo de la nota
	PMString CadenaPieFoto="";				//Contenido de caracteres del Pie de foto de la nota
	PMString CadenaContentNote= "";			//Contenido de caracteres del contenido de la nota
								
	PMRect CoorTitulo;						//PMRect de las coordenadas del titilo en la pagina colocada
	int32	LineFaltTitulo=0;				//Lineas que faltan para completar el copy fit al contenido del titulo
	int32 	LineRestTitulo=0;				//Lineas que sobran al frame del contenido del titulo
	int32	CarRestTitulo=0;				//Caracteres que sobran al frame del contenido del titulo
	int32	CarFaltTitulo=0;				//Caracteres falktantes para completar el copy fit en el frame del titulo
	int32	CarTotTitulo=0;					//Cantidad de caracteres del titulo de la nota
						
	PMRect CoorBalazo;
	int32	LineFaltBalazo=0;
	int32 	LineRestBalazo=0;
	int32	CarRestBalazo=0;
	int32	CarFaltBalazo=0;
	int32	CarTotBalazo=0;
					
	PMRect CoorPieFoto;
	int32	LineFaltPieFoto=0;
	int32 	LineRestPieFoto=0;
	int32	CarRestPieFoto=0;
	int32	CarFaltPieFoto=0;
	int32	CarTotPieFoto=0;
						
	PMRect CoorContenido;
	int32	LineFaltPieContenido=0;
	int32 	LineRestPieContenido=0;
	int32	CarRestPieContenido=0;
	int32	CarFaltPieContenido=0;
	int32	CarTotPieContenido=0;
	PMString N2PSQLFolioPag = "";
	
	K2Vector<PMString> ResultQueryVector;
	K2Vector<PMString> Params;
	PMString sss="";
	
	do
	{
			
		UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
		
		if(UIDTextModelOfTextFrame>0)
		{
		
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
		
			//PARA AVERIGUAR SI Frame seleccionado es un elemento de alguna nota 
			StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
			//Obtiene el ID_elemento_padre apartir del textframe seleccionado
			retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
			
			//SI NO ES ELMENTO DE UNA NOTA
			if(retval==kFalse)
			{
				ASSERT_FAIL(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
				break;
			}
	
		
 			
			//Obtenemos las preferencias del Servidor actual o por default		
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			
			//Si el nombre de las conexiones son iguales entonces no es una nota de servidor remoto
			//por lo tanto no puede importarse
			if(PrefConections.NameConnection==StructNota_IDFrames[0].DSNNote)
			{
				break;
			}
			
			//Obtiene las preferencias para dar checkIn a la nota
			if(!this->OpenDlgCheckInNota(Estado,DirigidoA, PublicacionID, SeccionID,GuiaNota))
			{
				break;
			}
			
			//Entonces obtiene las preferencias para la conexion para la base de datos remota
			if(!this->GetDefaultPreferencesConnection(PrefConectionsRemota,kFalse))
			{
				break;
			}
			
			//Obtener datos de la nota del servidor remoto
			StringConection.Append("DSN=" + PrefConectionsRemota.DSNNameConnection + ";UID=" + PrefConectionsRemota.NameUserDBConnection + ";PWD=" + PrefConectionsRemota.PwdUserDBConnection);
		
			
			//Inteface para query a la base de datos
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
			if(SQLInterface==nil)
			{
				break;
			} 
			
			Busqueda="SELECT Id_Fuente, Nombre, Id_Color, Calificacion, DATE_FORMAT(Fecha_CreaciÛn,'%Y-%m-%d %H:%i:%s.%f' ) AS Fecha_Creacion FROM Elemento Where Id_Elemento='" + StructNota_IDFrames[0].ID_Elemento_padre + "'";
			
			if(!SQLInterface->SQLQueryDataBase(StringConection, Busqueda, ResultQueryVector))
			{
				break;
			}
			
			
			
			if(ResultQueryVector.Length()>0)
			{
				ID_Fuente_Nota = SQLInterface->ReturnItemContentbyNameColumn("Id_Fuente",ResultQueryVector[0]);
				ID_Color_Nota = SQLInterface->ReturnItemContentbyNameColumn("Id_Color",ResultQueryVector[0]);
				Calificacion_Nota = SQLInterface->ReturnItemContentbyNameColumn("Calificacion",ResultQueryVector[0]).GetAsNumber();
				FechaDCracionNota = SQLInterface->ReturnItemContentbyNameColumn("Fecha_Creacion",ResultQueryVector[0]);
				NombreNota = SQLInterface->ReturnItemContentbyNameColumn("Nombre",ResultQueryVector[0]);
				
			}
			else
			{
				break;
			}
			
			
			
			// Obtiniene el usuario actual y la aplicacion actual
			if(!this->ObtenUsuarioLogeadoActualmente(ID_UsuarioActual))
			{
				break;
			}
			
			
			//
			InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
				(
					kN2PCTUtilitiesBoss,	// Object boss/class
					IN2PCTUtilities::kDefaultIID
				)));
			
			if(N2PCTUtils==nil)
			{
				break;
			}
 						
 			
 			////////	Obitne los datos de la nota(lineas, coordenadas y caracteres)			
						N2PSQLFolioPag = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
					
						
						
						if(StructNota_IDFrames[0].UIDFrTituloNote>0)
						{
							CadenaTitulo= N2PSQLUtilities::GetPMStringOfTextFrame(StructNota_IDFrames[0].UIDFrTituloNote);
							N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(StructNota_IDFrames[0].UIDFrTituloNote),CoorTitulo, LineFaltTitulo,
						 											LineRestTitulo,
																	CarFaltTitulo,
																	CarRestTitulo,
																	CarTotTitulo); 
						}
							
					
						
						if(StructNota_IDFrames[0].UIDFrBalazoNote>0)
						{
							CadenaBalazo = N2PSQLUtilities::GetPMStringOfTextFrame(StructNota_IDFrames[0].UIDFrBalazoNote);
							N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(StructNota_IDFrames[0].UIDFrBalazoNote),CoorBalazo,
									LineFaltBalazo,
									LineRestBalazo,
									CarFaltBalazo,
									CarRestBalazo,
									CarTotBalazo); 
						}
							
					 
						
						if(StructNota_IDFrames[0].UIDFrPieFotoNote>0)
						{
							CadenaPieFoto = N2PSQLUtilities::GetPMStringOfTextFrame(StructNota_IDFrames[0].UIDFrPieFotoNote);
							N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(StructNota_IDFrames[0].UIDFrPieFotoNote),CoorPieFoto ,
														LineFaltPieFoto,
														LineRestPieFoto,
														CarFaltPieFoto,
														CarRestPieFoto,
														CarTotPieFoto);
						}
					
						
						if(StructNota_IDFrames[0].UIDFrContentNote>0)
						{
							CadenaContentNote = N2PSQLUtilities::GetPMStringOfTextFrame(StructNota_IDFrames[0].UIDFrContentNote);
							N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(StructNota_IDFrames[0].UIDFrContentNote),CoorContenido,
																	LineFaltPieContenido,
																	LineRestPieContenido,
																	CarFaltPieContenido,
																	CarRestPieContenido,
																	CarTotPieContenido); 
						}
						
						
						
/*	CREATE PROCEDURE NOTA_IMPORTAR_N2P
@IdElemento VARCHAR(50) OUTPUT,
@NomElemento VARCHAR(50),
@IDFuente  VARCHAR(50),
@IDColor VARCHAR(50),
@Servidor VARCHAR(50),
@Id_USUARIO VARCHAR(50), 
@DigidoA VARCHAR(50),
@ESTATUS_ELE VARCHAR(30),
@Calificacion INTEGER,
@IdElemento_Contenido VARCHAR(30) OUTPUT ,
@IdElemento_Titulo VARCHAR(30)  OUTPUT ,
@IdElemento_PieFoto VARCHAR(30) OUTPUT,
@IdElemento_balazo VARCHAR(30) OUTPUT,

@Conte_Contenido VARCHAR(8000),
@TopContNota FLOAT,
@LeftContNota FLOAT,
@BottomContNota FLOAT,
@RigthContNota FLOAT,  
@LineFaltantesContNota INTEGER,
@LineRestantesContNota INTEGER,
@CarFaltantesContNota INTEGER,
@CarRestantesContNota INTEGER,
@CarContNota INTEGER,

@Conte_Titulo VARCHAR(8000),
@TopContTitulo FLOAT,
@LeftContTitulo FLOAT,
@BottomContTitulo FLOAT,
@RigthContTitulo FLOAT,
@LineFaltantesContTitulo INTEGER,
@LineRestantesContTitulo INTEGER,
@CarFaltantesContTitulo INTEGER,
@CarRestantesContTitulo INTEGER,
@CarContTitulo INTEGER,

@Conte_balazo VARCHAR(8000),
@TopContBalazo FLOAT,
@LeftContBalazo FLOAT,
@BottomContBalazo FLOAT,
@RigthContBalazo FLOAT,
@LineFaltantesContBalazo  INTEGER,
@LineRestantesContBalazo INTEGER,
@CarFaltantesContBalazo  INTEGER,
@CarRestantesContBalazo INTEGER,
@CarContBalazo  INTEGER,

@Conte_PieFoto  VARCHAR(8000),
@TopContPieFoto FLOAT,
@LeftContPieFoto FLOAT,
@BottomContPieFoto FLOAT,
@RigthContPieFoto FLOAT,
@LineFaltantesContPieFoto  INTEGER,
@LineRestantesContPieFoto INTEGER,
@CarFaltantesContPieFoto  INTEGER,
@CarRestantesContPieFoto INTEGER,
@CarContPieFoto INTEGER,

@FechaCheckInParamOUT VARCHAR(50) OUTPUT,
@N2PSQLFolioPag VARCHAR (50) 

*/	
						
							
							
						Params.push_back("");										//@IdElemento VARCHAR(50) OUTPUT,
						Params.push_back(NombreNota);								//@NomElemento VARCHAR(50),
						Params.push_back(ID_Fuente_Nota);							//@IDFuente  VARCHAR(50),			
						Params.push_back(ID_Color_Nota);							//@IDColor VARCHAR(50),
						Params.push_back(PrefConections.DSNNameConnection);		//@Servidor VARCHAR(50),
						Params.push_back(ID_UsuarioActual);						//@Id_USUARIO VARCHAR(50),
						Params.push_back(DirigidoA);								//@DigidoA VARCHAR(50),
						Params.push_back(Estado);									//@ESTATUS_ELE VARCHAR(30),
						
						sss.AppendNumber(Calificacion_Nota);	
						Params.push_back(sss);									//@Calificacion INTEGER,					
						Params.push_back("");									//@IdElemento_Contenido VARCHAR(30) OUTPUT ,
						Params.push_back("");									//@IdElemento_Titulo VARCHAR(30)  OUTPUT ,
						Params.push_back("");									//@IdElemento_PieFoto VARCHAR(30) OUTPUT,
						Params.push_back("");									//@IdElemento_balazo VARCHAR(30) OUTPUT,
						
							
						
						
						Params.push_back(CadenaContentNote);						//@Conte_Contenido VARCHAR(8000),
						sss="";
						sss.AppendNumber(CoorContenido.Top());
						
						
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorContenido.Left());
						
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorContenido.Bottom() );
						
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorContenido.Right() );
						
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineFaltPieContenido);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineRestPieContenido);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarFaltPieContenido);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarRestPieContenido);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarTotPieContenido);
						Params.push_back(sss);
						
						
						Params.push_back(CadenaTitulo);
						sss="";
						sss.AppendNumber(CoorTitulo.Top() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorTitulo.Left() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorTitulo.Bottom());
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorTitulo.Right());
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineFaltTitulo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineRestTitulo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarFaltTitulo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarRestTitulo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarTotTitulo);
						Params.push_back(sss);
						
						
						
						Params.push_back(CadenaBalazo);
						sss="";
						sss.AppendNumber(CoorBalazo.Top() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorBalazo.Left());
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorBalazo.Bottom() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorBalazo.Right() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineFaltBalazo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineRestBalazo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarFaltBalazo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarRestBalazo);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarTotBalazo);
						Params.push_back(sss);
						
						
						Params.push_back(CadenaPieFoto);
						sss="";
						sss.AppendNumber(CoorPieFoto.Top());
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorPieFoto.Left() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorPieFoto.Bottom() );
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CoorPieFoto.Right());
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineFaltPieFoto);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(LineRestPieFoto);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarFaltPieFoto);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarRestPieFoto);
						Params.push_back(sss);
						sss="";
						sss.AppendNumber(CarTotPieFoto);
						Params.push_back(sss);									//@CarContPieFoto INTEGER,
						
						
						Params.push_back("");									//@FechaCheckInParamOUT VARCHAR(50) OUTPUT,
						
						
						
						Params.push_back(N2PSQLFolioPag);						//@N2PSQLFolioPag VARCHAR (50) 
						
						Params.push_back(SeccionID);
						
						Params.push_back(PublicacionID);
					
					
				
					//arma la cadena para la conexion a la base de datos local
					StringConection="";
					StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
					
				
					//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
					/*if(!SQLInterface->StoreProcedureIMPORTARNOTA(StringConection , Params))
					{
						break;
					}*/
				
				
					if((Params[0]).NumUTF16TextChars()<=0)
					{
						//Por alguna razon no se pudo crear o generar la nueva nota
						break;
					}
					
					
					//Reemplaza el anterior Id de la base de datos remota por el nuevo ID de la Nota creada en la base de datos local
					N2PSQLUtilities::Busca_y_Reemplaza_En_XMP_IDlementosNotas( StructNota_IDFrames[0].ID_Elemento_padre , Params[0],
												Params[10], Params[11], 
												  Params[12], Params[9],
												  Params[53],  PrefConections.DSNNameConnection,
												  Params[53]);
												  
					
					PMString ss="";
					ss.AppendNumber(StructNota_IDFrames[0].UIDFrTituloNote); 
					this->CambiaTipoDEstadoDElementoNota(ss ,Estado,kTrue);					
					ss="";
					ss.AppendNumber(StructNota_IDFrames[0].UIDFrPieFotoNote); 
					this->CambiaTipoDEstadoDElementoNota(ss,Estado,kTrue);			
					ss="";
					ss.AppendNumber(StructNota_IDFrames[0].UIDFrBalazoNote); 
					this->CambiaTipoDEstadoDElementoNota(ss,Estado,kTrue);			
					ss="";
					ss.AppendNumber(StructNota_IDFrames[0].UIDFrContentNote); 
					this->CambiaTipoDEstadoDElementoNota(ss,Estado,kTrue);			
				
					
					//
					this->Fotos_ActualizaGuiaDNota(document,Estado, StructNota_IDFrames[0].ID_Elemento_padre);
				
				
				
				
		}
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ImportarNota fin");
	return(retval);
}





///
 bool16 N2PsqlUpdateStorysAndDBUtils::DetachNota()
 {
 	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DetachNota ini");
	bool16 retval=kFalse;
	int32 numeroPaginaActual=0;
	PMString IDNota = "";
	PMString IDUsuario="";
	PMString Aplicacion="";
	
	int32 UIDTextModelOfTextFrame =0;
	do
	{
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		if( Aplicacion.Contains("InCopy")  )
		{
			break;
		}
		
		UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
		//SI NO SE ENCUENTRA SELECCIONADO UN TEXTFRAME
		if(UIDTextModelOfTextFrame<1)
		{
			CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
			break;
		}
		
		
		StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
		//Obtiene el ID_elemento_padre apartir del textframe seleccionado
		retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
		//SI NO ES ELMENTO DE UNA NOTA
		if(retval==kFalse)
		{
			CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
			break;
		}
		
		if(!this->OpenDlgDetachElemento())
		{
			break;	
		}
		
		/*const int ActionAgreedValue=1;
						int32 result=-1;
						result=CAlert::ModalAlert(kN2PDeseasDesligarEstaNotaStringKey,
							kYesString, 
							kCancelString,
							kNullString,
							ActionAgreedValue,CAlert::eQuestionIcon);
						if(result!=1)
						{
							//Name the command sequence:
						
						
							break;
						}
		*/				
		
			///////////////////////////////////
					//Para saber si se puede fluir la nota
					PreferencesConnection PrefConectionsOfServerRemote;
		
					if( !this->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
					{
						break;
					}
					PMString idSeccionDElemento="";
					PMString Consulta=""; 
					PMString StringConection="";
					K2Vector<PMString> QueryVector;
					
					StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
					InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
					if(SQLInterface==nil)
						{
							break;
						} 
						
						
					
					
					
					Consulta="SELECT Id_Seccion FROM Elemento WHERE Id_Elemento=" +StructNota_IDFrames[0].ID_Elemento_padre +"";
					if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
					{
						break;
					}
					else
					{
						idSeccionDElemento = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[0]);
						if(idSeccionDElemento=="0")
						{
							CAlert::InformationAlert(kN2PSQLUserCantDesligarNotaAPagStringKey);
							break;
						}
						
					}
					
					QueryVector.clear();
					Consulta=" CALL HaveUserPrivilegioForEvent ('" + IDUsuario + "' ,"+idSeccionDElemento+" , 22, @X)";
					//solo si la nota proviene del servidor de Preferencia
					if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
					{
						break;
					}
					else
					{
						PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
						if(RESULT=="0")
						{
							CAlert::InformationAlert(kN2PSQLUserCantDesligarNotaAPagStringKey);
							break;
						}
						
					}
		
		
		///////////////////////////////////
		//Aqui lo cambie
		//PARA AVERIGUAR SI SE PUEDE HACER UN CHECKIN A ESTA NOTA quiero decir para saber si se esta editando la noa en este momento
		if(CanDoCheckInNota(UIDTextModelOfTextFrame))
		{
			PMString Estado="0";
			PMString RoutedTo="0";
			PMString PublicacionID="0";
			PMString SeccionID="0";
			//Obtiene las preferencias para dar checkIn a la nota
			//if(!this->OpenDlgCheckInNota(Estado,RoutedTo, PublicacionID, SeccionID))
			//{
			//	break;
			//}
		
			//Actualiza a BD el contenido de los elementos de la nota,
			//Cambia el estado de escritura(Lapiz),
			//Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota, 
			//Libera la nota en la BD para ser modificad por cualquier otro usuario
			//XXXXX this->CheckInNotaDBAndPag(StructNota_IDFrames,Estado,RoutedTo,IDUsuario,  PublicacionID, SeccionID, kFalse, document);
			
				
			///////////////////////////////////////////
			//CheckOut demas Familiares sobre la pagina
			///////////////////////////////////////////
			
			
			PreferencesConnection PrefConections;
			
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument No Preferences");
				break;
			}
		
					
			
			StringConection="";
		
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
			/*InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			if(SQLInterface==nil)
			{
				break;
			} */
			K2Vector<PMString> IdFamilyArticle;	
			PMString ConsultaFamily="SELECT Id_Elemento FROM Elemento WHERE Id_Elemento_padre="+StructNota_IDFrames[0].ID_Elemento_padre+
			" AND id_tipo_ele!=4 UNION SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre IN(SELECT ID_Elemento FROM Elemento WHERE Id_Elemento_padre="+StructNota_IDFrames[0].ID_Elemento_padre+
			" AND id_tipo_Ele=4) ";			
			if(!SQLInterface->SQLQueryDataBase(StringConection,ConsultaFamily,IdFamilyArticle))
			{
				break;
			}
			
			
			if(IdFamilyArticle.Length()>1)
			{
				for(int32 j=0;j<IdFamilyArticle.Length();j++)
				{
					PMString ID_Elemento_PadreAA = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",IdFamilyArticle[j]);
					if(GetElementosNotaDXMP_Of_IDElementoPadre(ID_Elemento_PadreAA,	StructNota_IDFrames, document))
					{	//Check in de notas y actualiza Base de datos desde InDesign Con contenidos de notas y demas
						//this->CheckInNotaDBAndPag(StructNota_IDFrames,Estado,RoutedTo,IDUsuario,  PublicacionID, SeccionID, kFalse, document);
					}
					else
					{
						//Check in de Notassolo estado, seccion publicacion etc no contenidos
						//this->Insert_IN_Bitacora_Elemento("0",ID_Elemento_PadreAA,IDUsuario,  Estado,kFalse);
					}
				}
					 
			}
			
			
			///////////////////////////////////////////
			
		}
		else
		{
			CAlert::InformationAlert(kN2PSQLDebeSerChecauteadaStringKey);
			break;
		}
		
		
		//Quita los adornaments
		if(!CambiaEstadoYLapizDeNota(StructNota_IDFrames,kFalse,"",kTrue))
		{
				break;
		}
		
		
		//Cambia en base de datos
		PMString QueryNotaDetach="CALL NOTA_DETACH_D_PAGINA (" + StructNota_IDFrames[0].ID_Elemento_padre + " , '" + N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document) + "' , '" + IDUsuario + "')";
		
		
		
		
		//Elimina de XMP
		N2PSQLUtilities::Busca_y_Elimina_En_XMP_IDlementosNotas(StructNota_IDFrames[0].ID_Elemento_padre);
		
		
		
		
		K2Vector<PMString> Params;
		
		

			
		
		
							
		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		if(!SQLInterface->SQLQueryDataBase(StringConection , QueryNotaDetach, Params))
		{
			break;
		}
		
		//Se borra de notas WEB
		SQLInterface->SQLSetInDataBase(StringConection,"CALL  DELETE_NOTAFOT_D_DOTASWEB( "+StructNota_IDFrames[0].ID_Elemento_padre+","+"0"+")");

		this->ActualizaTodasLNotasYDisenoPagina("N2PActualizacion");
		
		QueryNotaDetach = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlBusquedaTextWidgetID);
		if(QueryNotaDetach.NumUTF16TextChars()>0)
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
		else
			N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
		
		retval=kTrue;
	}while(false);
	
	 //N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::ImportarNota fin");
	return(retval);
 }
 
 
 
 bool16 N2PsqlUpdateStorysAndDBUtils::DoCrearNuevaNota()
 {
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DoCrearNuevaNota ini");
 	bool16 retval=kFalse;
 	PMString ID_Usuario="";
 	do
 	{
 		
 		//CAlert::InformationAlert("WWW 08");
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		} 
		//CAlert::InformationAlert("WWW 09");
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);

		if( Aplicacion.Contains("InCopy")  )
		{
			break;
		}
 		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			break;	
		}
		//CAlert::InformationAlert("WWW 10");
 		PreferencesConnection PrefConections;
 		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}

 		//Si existen usuarios Logeados
		if(!N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		//CAlert::InformationAlert("WWW 11");
		
		PMString N2PSQLFolioPag = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
 		PMString Id_Publicacion =  N2PSQLUtilities::GetXMPVar("N2PPublicacion", document);
 		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString ID_Estatus_Nota="";
		PMString ID_DirigidoA_Nota="";
		//Si es una pagina de N2PSQL
		if(N2PSQLFolioPag.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert(kN2PSQLYouCouldSaveThisPageString);
			break;
		}
		//CAlert::InformationAlert("WWW 12");
		InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																					  (
																					   kN2PwsdlcltUtilsBoss,	// Object boss/class
																					   IN2PWSDLCltUtils::kDefaultIID
																					   )));
		
		if(!	N2PWSDLCltUtil)
		{
			CAlert::InformationAlert("salio");
			break;
		}
		/*CAlert::InformationAlert("WWW 13"+ID_Usuario+",192.6.2.2,,"+
								 Aplicacion+","+ 
								 PrefConections.URLWebServices);*/
		PMString ResultString="";
		retval=N2PWSDLCltUtil->realizaConneccion(ID_Usuario,
												 "192.6.2.2", 
												 "",
												 Aplicacion, 
												 PrefConections.URLWebServices);
		if(!retval)
		{
			retval=kFalse;
		}
		/*	DONGLE VERDE RED
		//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DoCrearNuevaNota() Sentinel check");
		InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
			(
				kInterUltraObjetProKyBoss,	// Object boss/class
				IInterUltraObjectProKy::kDefaultIID
			)));
			
			if(UltraProCheck==nil)
			{
				 retval=kFalse;
				break;
			}
		
			PMString ResultString="";
			retval=UltraProCheck->RedValueN2PPlugin(ResultString);
			if(!retval)
			{

				CAlert::ErrorAlert(ResultString);
				 retval=kFalse;
				break;
			}*/
			//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DoCrearNuevaNota() Sentinel check fin");
			/********* DONGLE ROJO LOCAL *************
					InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*> (CreateObject
					(
						kInterUltraProKyBoss,	// Object boss/class
						IInterUltraProKy::kDefaultIID
					)));

					if(UltraProCheck==nil)
					{
						
						retval=kFalse;
						break;
					}
					PMString ResultString="";
					retval=UltraProCheck->CheckDongleN2PPlugins(ResultString);
					if(!retval)
					{
						CAlert::ErrorAlert(ResultString);
						retval=kFalse;
						break;
					}
				*/	
			
		//Para obtener el nombre de la seccion y de la publicacion de la pagina
		//CAlert::InformationAlert("WWW 14");
		
				
		//Crea la conexion a la base de datos	
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
				
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
		//CAlert::InformationAlert("WWW 15");
		K2Vector<PMString> QueryVector;
		PMString Privilegio="";				
		PMString Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "', " + Id_Seccion + ", 24 , @A) ";
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		for(int32 j=0;j<QueryVector.Length();j++)
		{
			Privilegio = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[j]);
		}
		
		if(Privilegio=="0")
		{
			CAlert::InformationAlert(kN2PSQLCantCrearNotaInDStringKey);
			break;
		}
		//CAlert::InformationAlert("WWW 16");
		Consulta="";
		
		Consulta.Append(" SELECT (SELECT Nombre_Publicacion FROM Publicacion WHERE Id_Publicacion=" + Id_Publicacion + ") AS Publicacion");	
		Consulta.Append(" ,(SELECT Nombre_de_Seccion FROM Seccion WHERE Id_Seccion=" + Id_Seccion + " AND Id_Publicacion=" + Id_Publicacion + ") as Seccion");
		
		QueryVector.clear();
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		//CAlert::InformationAlert("WWW 17");
		for(int32 j=0;j<QueryVector.Length();j++)
		{	
			
			
			Id_Publicacion=SQLInterface->ReturnItemContentbyNameColumn("Publicacion",QueryVector[j]);
			Id_Seccion=SQLInterface->ReturnItemContentbyNameColumn("Seccion",QueryVector[j]);

			ID_Estatus_Nota="";
			ID_DirigidoA_Nota="";
						
		}
		//CAlert::InformationAlert("WWW 18");
		
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextTituloWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextBalazoWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPieWidgetID,"");
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPageWidgetID,"");
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextEnGuiaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextFechaWidgetID,Fecha_Edicion_Nota);
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlStTextPubliWidgetID,Id_Publicacion);
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlSeccionLabelWidgetID,Id_Seccion);
 		N2PSQLUtilities::LlenarComboUsuarioSobrePaletaN2P();
 		N2PSQLUtilities::LlenarComboEstatusSobrePaletaN2P();
 		this->OcultaYMuestraWidgetsPNNota(kTrue);
 		
		//CAlert::InformationAlert("WWW 19");
 		/////////////////////////////////////////
 		// En caso de que se tenga seleccionado una caja de texto se debe obtener el Id de la nota
 		// de Este elemento de nota para indicar que se desea generar una nota hija
 		int32 numeroPaginaActual=0;
 		int32 UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
		//SI NO SE ENCUENTRA SELECCIONADO UN TEXTFRAME
		if(UIDTextModelOfTextFrame > 1)
		{
			
			//Crear como nota hija
			StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
			//Obtiene el ID_elemento_padre apartir del textframe seleccionado
			retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
			//SI NO ES ELMENTO DE UNA NOTA
			if(retval==kTrue)
			{
				if(!this->OpenDlgCrearNotaHija())//Preguntar si dsea crearla como nota hija
				{
					//Crear como simple nota
					N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,"0");
				}
				else
				{
					
					
					if(!this->ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(StructNota_IDFrames[0].ID_Elemento_padre,kFalse))
					{
						CAlert::InformationAlert(kN2PSQLDebeDepositarNotaPadreStringKey);
						break;
					}
					
					N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,StructNota_IDFrames[0].ID_Elemento_padre);
					
					//Colocar Dirigido A y Estatus inhabilitar si es nota hijas
					//#ifdef WINDOWS
						
						Consulta="";
						Consulta.Append(" SELECT (SELECT Nombre_Estatus FROM Estatus_Elemento WHERE Id_Estatus=(SELECT LastIdEstatusNota FROM Elemento WHERE Id_Elemento=" + StructNota_IDFrames[0].ID_Elemento_padre + ")) AS Estatus");	
						Consulta.Append(" ,(SELECT Dirigido_A FROM Elemento WHERE Id_Elemento=" + StructNota_IDFrames[0].ID_Elemento_padre + ") as DirigidoA");
						//CAlert::InformationAlert(Consulta);
						QueryVector.clear();
						
						SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
						
						for(int32 j=0;j<QueryVector.Length();j++)
						{	
							
							
							ID_Estatus_Nota=SQLInterface->ReturnItemContentbyNameColumn("Estatus",QueryVector[j]);
							ID_DirigidoA_Nota=SQLInterface->ReturnItemContentbyNameColumn("DirigidoA",QueryVector[j]);	
							//CAlert::InformationAlert(QueryVector[j]);
						}
						
						//CAlert::InformationAlert(ID_Estatus_Nota);
						N2PSQLUtilities::SetInComboSobrePaletaN2P(kN2PsqlTextStatusComboWidgetID,ID_Estatus_Nota);
						//CAlert::InformationAlert(ID_DirigidoA_Nota);
						N2PSQLUtilities::SetInComboSobrePaletaN2P(kN2PsqlComboRoutedToWidgetID,ID_DirigidoA_Nota);
						N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta(kN2PsqlComboRoutedToWidgetID,kFalse);
						N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta(kN2PsqlTextStatusComboWidgetID,kFalse);
						//CAlert::InformationAlert(ID_Estatus_Nota);
						//N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,ID_Estatus_Nota);
						//N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,ID_DirigidoA_Nota);
					//#endif
					
				}
			}
			else
			{
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,"0");
			}
			
		}
		else
		{
			//Crear como simple nota
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,"0");
		}
		
		
		
 		
 		retval=kTrue;
 		
 		//CAlert::InformationAlert("WWW 20");
 		
 	}while(false);
	 
	 //N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DoCrearNuevaNota fin");
 	return(retval);
 }
 
 
/* void N2PsqlUpdateStorysAndDBUtils::DoLigarImagenANota()
 {
 	bool16 retval=kFalse;
 	PMString ID_Usuario="";
 	do
 	{
 		
 		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		} 
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);

		if( Aplicacion.Contains("InCopy")  )
		{
			break;
		}
 		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			break;	
		}
		
 		PreferencesConnection PrefConections;
 		
 		//Si existen usuarios Logeados
		if(!N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		PMString N2PSQLFolioPag = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
 		PMString Id_Publicacion =  N2PSQLUtilities::GetXMPVar("N2PPublicacion", document);
 		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		
		//Si es una pagina de N2PSQL
		if(N2PSQLFolioPag.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert(kN2PSQLYouCouldSaveThisPageString);
			break;
		}
		
		
		//Para obtener el nombre de la seccion y de la publicacion de la pagina
		
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		//Crea la conexion a la base de datos	
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
				
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
		
		K2Vector<PMString> QueryVector;
		PMString Privilegio="";				
		PMString Consulta=" CALL HaveUserPrivilegioForEvent ('" + ID_Usuario + "', " + Id_Seccion + ", 24 , @A) ";//podria se 28 importar foto
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		for(int32 j=0;j<QueryVector.Length();j++)
		{
			Privilegio = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[j]);
		}
		
		if(Privilegio=="0")
		{
			CAlert::InformationAlert(kN2PSQLCantCrearNotaInDStringKey);
			break;
		}
		
		/*Consulta="";
		
		Consulta.Append(" SELECT (SELECT Nombre_Publicacion FROM Publicacion WHERE Id_Publicacion=" + Id_Publicacion + ") AS Publicacion");	
		Consulta.Append(" ,(SELECT Nombre_de_Seccion FROM Seccion WHERE Id_Seccion=" + Id_Seccion + " AND Id_Publicacion=" + Id_Publicacion + ") as Seccion");
		
		QueryVector.clear();
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		
		for(int32 j=0;j<QueryVector.Length();j++)
		{	
			
			
			Id_Publicacion=SQLInterface->ReturnItemContentbyNameColumn("Publicacion",QueryVector[j]);
			Id_Seccion=SQLInterface->ReturnItemContentbyNameColumn("Seccion",QueryVector[j]);
						
		}
		
		
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextTituloWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextBalazoWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPieWidgetID,"");
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPageWidgetID,"");
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextEnGuiaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextFechaWidgetID,Fecha_Edicion_Nota);
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlStTextPubliWidgetID,Id_Publicacion);
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlSeccionLabelWidgetID,Id_Seccion);
 		N2PSQLUtilities::LlenarComboUsuarioSobrePaletaN2P();
 		N2PSQLUtilities::LlenarComboEstatusSobrePaletaN2P();
 		this->OcultaYMuestraWidgetsPNNota(kTrue);
 		
 		/////////////////////////////////////////
 		// En caso de que se tenga seleccionado una caja de texto se debe obtener el Id de la nota
 		// de Este elemento de nota para indicar que se desea generar una nota hija
 		
 		int32 numeroPaginaActual=0;
 		int32 UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
		//SI NO SE ENCUENTRA SELECCIONADO UN TEXTFRAME
		if(UIDTextModelOfTextFrame > 1)
		{
			
			//Crear como nota hija
			StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
			//Obtiene el ID_elemento_padre apartir del textframe seleccionado
			retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
			//SI NO ES ELMENTO DE UNA NOTA
			if(retval==kTrue)
			{
				retval=kTrue;
				//CrearLiga a imagene
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,StructNota_IDFrames[0].ID_Elemento_padre);
			
			}
			else
			{
				retval=kFalse;
			}
			
		}
		else
		{
			//
			retval=kFalse;
		}	
 		//CAlert::InformationAlert("OK");
 		
 	}while(false);
 	
 }
 */

 
bool16 N2PsqlUpdateStorysAndDBUtils::CancelaNuevaNota()
 {
 	bool16 retval=kFalse;
 	do
 	{ 
 		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextTituloWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextBalazoWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPieWidgetID,"");
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPageWidgetID,"");
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextEnGuiaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextFechaWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboPubliWidgetID,"");
  		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboSeccionWidgetID,"");
  		N2PSQLUtilities::RemoveXMPVar("UIDTextModelTitulo", document);	
		N2PSQLUtilities::RemoveXMPVar("UIDTextModelBalazo", document);	
		N2PSQLUtilities::RemoveXMPVar("UIDTextModelPieFoto", document);
		N2PSQLUtilities::RemoveXMPVar("UIDTextModelConteNota", document);	
 		this->OcultaYMuestraWidgetsPNNota(kFalse);
 		retval=kTrue;
 	}while(false);
 	return(retval);
 }	
 

 
 
 
 
 
bool16 N2PsqlUpdateStorysAndDBUtils::DoSubirABDNuevaNota()
 {
	 
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DoSubirABDNuevaNota ini");
 	bool16 retval=kFalse;
 	
 	PMString ID_Usuario="";
 	do
 	{
 		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
		/////
 		PreferencesConnection PrefConections;
 		
 		///////////////
 		
 		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		
 		
		
 		///////////////
		
		
			
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
 		///////////////
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
 		if(SQLInterface==nil)
		{
			break;
		} 
						
 		bool16 process=kFalse;
 		
 		if(N2PSQLUtilities::GetXMPVar("UIDTextModelTitulo", document).NumUTF16TextChars()>0)
 		{
 			 process=kTrue;
 		}
 		else
 		{
 				if(N2PSQLUtilities::GetXMPVar("UIDTextModelBalazo",document).NumUTF16TextChars()>0)
 				{
 					process=kTrue;
 				}
			else
				{
					if(N2PSQLUtilities::GetXMPVar("UIDTextModelPieFoto", document).NumUTF16TextChars()>0)
					{
						process=kTrue;
					}
					else
					{
						if(N2PSQLUtilities::GetXMPVar("UIDTextModelConteNota", document).NumUTF16TextChars()>0)
						{
							process=kTrue;
						}
						
					}
				}
			
 		}
 		
 		if(process==kFalse)
 		{
 			CAlert::InformationAlert("You must draged a article element ");
 			break;
 		}
 		///////////////
 		PMString IdElemento="";
 		PMString NomElemento="";
 		PMString EsNotaDuplicada="0";
 		PMString Id_elem_padre_De_nota_Duplicada="0";
 		
 		PMString DigidoA = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlComboRoutedToWidgetID);
 		/*if(DigidoA.NumUTF16TextChars()<=0)
 		{
 			/CAlert::InformationAlert("Please a user");
 			break;
 		}*/
 		DigidoA.Translate();
 		DigidoA.SetTranslatable(kTrue);

 		PMString IDFuente="1";
 		PMString IDColor="1";
 		PMString Servidor= PrefConections.DSNNameConnection ;
 		PMString IdElemento_titulo="";
 		
 		///////////////////
 		UID uidRef=kInvalidUID;
 		UIDRef textModelRef = UIDRef::gNull;
 		PMString Conte_Titulo="";
 		PMString Conte_balazo = "";
 		PMString Conte_PieFoto = "";
 		PMString Conte_Contenido = "";
 		
 		PMString IdElemento_balazo="";
 		PMString IdElemento_PieFoto="";
 		PMString IdElemento_Contenido = "";
 		
 		
 		if(N2PSQLUtilities::GetXMPVar("UIDTextModelTitulo", document).NumUTF16TextChars()>0)
 		{
 			uidRef = N2PSQLUtilities::GetXMPVar("UIDTextModelTitulo", document).GetAsNumber();
 			textModelRef = UIDRef(db, uidRef);
 			Conte_Titulo =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef);//PMString Conte_Titulo =  N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextTituloWidgetID);//
 			
 				//N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(UIDElementoNota),CoorTitulo, LineFaltTitulo,
				//		LineRestTitulo,
				//		CarFaltTitulo,
				//		CarRestTitulo,
				//			CarTotTitulo); 
 		}
		if(N2PSQLUtilities::GetXMPVar("UIDTextModelBalazo", document).NumUTF16TextChars()>0)
 		{
 			uidRef = N2PSQLUtilities::GetXMPVar("UIDTextModelBalazo", document).GetAsNumber();
 			textModelRef = UIDRef(db, uidRef);
 			Conte_balazo =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef); //Conte_balazo= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextBalazoWidgetID);//
 			
 				//N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(UIDElementoNota),CoorTitulo, LineFaltTitulo,
				//		LineRestTitulo,
				//		CarFaltTitulo,
				//		CarRestTitulo,
				//			CarTotTitulo); 
 		}
		
		
		if(N2PSQLUtilities::GetXMPVar("UIDTextModelPieFoto", document).NumUTF16TextChars()>0)
 		{
 			uidRef = N2PSQLUtilities::GetXMPVar("UIDTextModelPieFoto", document).GetAsNumber();
 			textModelRef = UIDRef(db, uidRef);
 			Conte_PieFoto =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef); // Conte_PieFoto= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextPieWidgetID);//
 			
 				//N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(UIDElementoNota),CoorTitulo, LineFaltTitulo,
				//		LineRestTitulo,
				//		CarFaltTitulo,
				//		CarRestTitulo,
				//			CarTotTitulo); 
 		}
 		
 		if(N2PSQLUtilities::GetXMPVar("UIDTextModelConteNota", document).NumUTF16TextChars()>0)
 		{
 			uidRef = N2PSQLUtilities::GetXMPVar("UIDTextModelConteNota", document).GetAsNumber();
 			textModelRef = UIDRef(db, uidRef);
 			Conte_Contenido =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef); //Conte_Contenido = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextNotaWidgetID);//
 			
 				//N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(UIDElementoNota),CoorTitulo, LineFaltTitulo,
				//		LineRestTitulo,
				//		CarFaltTitulo,
				//		CarRestTitulo,
				//			CarTotTitulo); 
 		}
 		
 		
 		 
 		
 		PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
 		PMString Id_Publicacion =  N2PSQLUtilities::GetXMPVar("N2PPublicacion", document);
 		PMString Id_Estatus = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextStatusComboWidgetID);
 		
		if(PrefConections.N2PUtilizarGuias==1)
		NomElemento=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID);
		else
			NomElemento=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID); 		
 		
 		

 		if(PrefConections.EsCampoGuiaObligatorio>0)
 		{
 			if(NomElemento.NumUTF16TextChars()<=0)
 			{
 				CAlert::InformationAlert(kN2PSQLGuiaNoVaciaStringKey);
 				break;
 			}
 			
 		}
 		


 		if(NomElemento.Contains("'") || NomElemento.IndexOfWChar(34) >=0)
 		{
 			CAlert::InformationAlert(kN2PSQLValidaComilasYDComillasStringKey);
	 		break;
 		}
 		

 		
 		Id_elem_padre_De_nota_Duplicada = N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
 		


 		if(Id_Estatus.NumUTF16TextChars()<=0)
 		{
 			//CAlert::InformationAlert("Please select the article status");
 			break;
 		}
 		Id_Estatus.Translate();
 		Id_Estatus.SetTranslatable(kTrue);
 		

 		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_Nota);
 		PMString FechaYMD=InterlasaUtilities::ChangeInDesignDateStringToDateStringYMD(Fecha_Edicion_Nota);
 		


 		PMString FECHASALIDA="";
 		PMString N2PSQL_IDPag=N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
 		
 		PMString StringConection="";
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		PMString QueryNotaCreateID=""; /*"DECLARE @IdElemento VARCHAR(50) "; 
		QueryNotaCreateID.Append("DECLARE @IdElemento_titulo VARCHAR(50) ") ;
		QueryNotaCreateID.Append("DECLARE @IdElemento_balazo VARCHAR(50) ") ;
		QueryNotaCreateID.Append("DECLARE @IdElemento_PieFoto VARCHAR(50) ") ;
		QueryNotaCreateID.Append("DECLARE @IdElemento_Contenido VARCHAR(50) ") ;
		QueryNotaCreateID.Append("DECLARE @FECHASALIDA VARCHAR(50) SET @IdElemento ='' SET @IdElemento_titulo ='' SET @IdElemento_Balazo ='' SET @IdElemento_PieFoto ='' SET @IdElemento_Contenido =''  SET @FECHASALIDA =''") ;
		*/

		PMString FirstParte="";
		if(Conte_Contenido.NumUTF16TextChars()>30000)
		{
			FirstParte=Conte_Contenido.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte=Conte_Contenido;
		}
		


		PMString FirstParte_Conte_Titulo="";
		if(Conte_Titulo.NumUTF16TextChars()>30000)
		{
			FirstParte_Conte_Titulo=Conte_Titulo.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte_Conte_Titulo=Conte_Titulo;
		}
		
		PMString FirstParte_Conte_balazo="";
		if(Conte_balazo.NumUTF16TextChars()>30000)
		{
			FirstParte_Conte_balazo=Conte_balazo.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte_Conte_balazo=Conte_balazo;
		}
		
		PMString FirstParte_Conte_PieFoto="";
		if(Conte_PieFoto.NumUTF16TextChars()>30000)
		{
			FirstParte_Conte_PieFoto=Conte_PieFoto.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte_Conte_PieFoto=Conte_PieFoto;
		}
		


		QueryNotaCreateID.Append(" CALL W_NOTA_CREAR_Sin_ID_N2P2(@Elemento ,'");
		QueryNotaCreateID.Append(NomElemento);
		QueryNotaCreateID.Append("' , ");
		QueryNotaCreateID.Append(EsNotaDuplicada);
		QueryNotaCreateID.Append(" , '");
		QueryNotaCreateID.Append(Id_elem_padre_De_nota_Duplicada);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(ID_Usuario);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(DigidoA);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(IDFuente);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(IDColor);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(Servidor);
		QueryNotaCreateID.Append("' , ");
		QueryNotaCreateID.Append("@IdElemento_titulo ");
		QueryNotaCreateID.Append(" , '");
		QueryNotaCreateID.Append(FirstParte_Conte_Titulo);
		
		QueryNotaCreateID.Append("' , ");
		QueryNotaCreateID.Append("@IdElemento_balazo ");
		QueryNotaCreateID.Append(" , '");
		QueryNotaCreateID.Append(FirstParte_Conte_balazo);
		
		QueryNotaCreateID.Append("' , ");
		QueryNotaCreateID.Append("@IdElemento_PieFoto ");
		QueryNotaCreateID.Append(" , '");
		QueryNotaCreateID.Append(FirstParte_Conte_PieFoto);
		
		QueryNotaCreateID.Append("' , ");
		QueryNotaCreateID.Append("@IdElemento_Contenido ");
		QueryNotaCreateID.Append(" , '");
		QueryNotaCreateID.Append(FirstParte);
		
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(Id_Seccion);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(Id_Publicacion);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(Id_Estatus);
		QueryNotaCreateID.Append("' , '");
		QueryNotaCreateID.Append(MySQLFecha);
		
		QueryNotaCreateID.Append("' , ");
		QueryNotaCreateID.Append("@FECHASALIDA ");
		
		QueryNotaCreateID.Append(" , ");
		QueryNotaCreateID.Append(N2PSQL_IDPag);
		QueryNotaCreateID.Append(",24)");
		
		

		
		
		//QueryNotaCreateID.Append(" SELECT @IdElemento AS IdElemento, @IdElemento_titulo AS IdElemento_titulo, @IdElemento_balazo AS IdElemento_balazo, @IdElemento_PieFoto AS IdElemento_PieFoto, @IdElemento_Contenido  AS IdElemento_Contenido  ");
		PMString Tamano="";
		Tamano.AppendNumber(Conte_Contenido.NumUTF16TextChars());
		
		//CAlert::InformationAlert(QueryNotaCreateID);
 		//CAlert::InformationAlert("Antes de enviar   "+Tamano);
 		this->GuardarQuery(QueryNotaCreateID);
 		
 		K2Vector<PMString> QueryVector;
 		
 		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		if(!SQLInterface->SQLQueryDataBase(StringConection , QueryNotaCreateID, QueryVector))
		{
			break;
		}
		

		
		for(int32 j=0;j<QueryVector.Length();j++)
		{		
			IdElemento=SQLInterface->ReturnItemContentbyNameColumn("IdElementoOUT",QueryVector[j]);
			IdElemento_titulo=SQLInterface->ReturnItemContentbyNameColumn("IdElemento_tituloOUT",QueryVector[j]);
			IdElemento_balazo=SQLInterface->ReturnItemContentbyNameColumn("IdElemento_balazoOUT",QueryVector[j]);
			IdElemento_PieFoto=SQLInterface->ReturnItemContentbyNameColumn("IdElemento_PieFotoOUT",QueryVector[j]);	
			IdElemento_Contenido=SQLInterface->ReturnItemContentbyNameColumn("IdElemento_ContenidoOUT",QueryVector[j]);
			FECHASALIDA=SQLInterface->ReturnItemContentbyNameColumn("FECHASALIDAOUT",QueryVector[j]);					
		}
		

		//CAlert::InformationAlert("X 0");
		this->EnviarPorLotesContenidoDElemento_De_Nota(Conte_Titulo,IdElemento_titulo);
		this->EnviarPorLotesContenidoDElemento_De_Nota(Conte_balazo,IdElemento_balazo);
		this->EnviarPorLotesContenidoDElemento_De_Nota(Conte_PieFoto,IdElemento_PieFoto);
		this->EnviarPorLotesContenidoDElemento_De_Nota(Conte_Contenido,IdElemento_Contenido);
		//CAlert::InformationAlert("X1");
		
		
		//Guarda en XMP
		bool16 FueReemplazado=kFalse;
		if(N2PSQLUtilities::GetXMPVar("UIDTextModelTitulo", document).NumUTF16TextChars()>0)
		{
			FueReemplazado = N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",IdElemento,IdElemento_titulo,N2PSQLUtilities::GetXMPVar("UIDTextModelTitulo", document),"Titulo",Servidor,Id_Estatus,kFalse,FECHASALIDA, kFalse /*No es elemento foto*/);
			this->CambiaModoDeEscrituraDElementoNota(N2PSQLUtilities::GetXMPVar("UIDTextModelTitulo", document),kTrue);
			N2PSQLUtilities::RemoveXMPVar("UIDTextModelTitulo", document);	
		}
		

		if(N2PSQLUtilities::GetXMPVar("UIDTextModelBalazo", document).NumUTF16TextChars()>0)
		{
			FueReemplazado = N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",IdElemento,IdElemento_balazo,N2PSQLUtilities::GetXMPVar("UIDTextModelBalazo", document),"Balazo",Servidor,Id_Estatus,kFalse,FECHASALIDA, kFalse /*No es elemento foto*/);
			this->CambiaModoDeEscrituraDElementoNota(N2PSQLUtilities::GetXMPVar("UIDTextModelBalazo", document),kTrue);
			N2PSQLUtilities::RemoveXMPVar("UIDTextModelBalazo", document);	
		}
		
		
		if(N2PSQLUtilities::GetXMPVar("UIDTextModelPieFoto", document).NumUTF16TextChars()>0)
		{
			FueReemplazado = N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",IdElemento,IdElemento_PieFoto,N2PSQLUtilities::GetXMPVar("UIDTextModelPieFoto", document),"PieFoto",Servidor,Id_Estatus,kFalse,FECHASALIDA, kFalse /*No es elemento foto*/);
			this->CambiaModoDeEscrituraDElementoNota(N2PSQLUtilities::GetXMPVar("UIDTextModelPieFoto", document),kTrue);
			N2PSQLUtilities::RemoveXMPVar("UIDTextModelPieFoto", document);	
		}
		

		
		if(N2PSQLUtilities::GetXMPVar("UIDTextModelConteNota", document).NumUTF16TextChars()>0)
		{
			FueReemplazado = N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",IdElemento,IdElemento_Contenido,N2PSQLUtilities::GetXMPVar("UIDTextModelConteNota", document),"ContenidoNota",Servidor,Id_Estatus,kFalse,FECHASALIDA, kFalse /*No es elemento foto*/);
			this->CambiaModoDeEscrituraDElementoNota(N2PSQLUtilities::GetXMPVar("UIDTextModelConteNota", document),kTrue);
			N2PSQLUtilities::RemoveXMPVar("UIDTextModelConteNota", document);	
		}
		

		////////////////////////////////////////
		//////////////
		PMString nombreS="";
		QueryVector.clear();
 		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		if(!SQLInterface->SQLQueryDataBase(StringConection , "SELECT Nombre_de_Seccion FROM Seccion WHERE Id_Seccion="+Id_Seccion, QueryVector))
		{
			break;
		}
		

		
		for(int32 j=0;j<QueryVector.Length();j++)
		{		
			nombreS=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[j]);					
		}
		
		PMString nombreP="";
		QueryVector.clear();
 		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		if(!SQLInterface->SQLQueryDataBase(StringConection , "SELECT Nombre_Publicacion FROM Publicacion WHERE Id_Publicacion="+Id_Publicacion, QueryVector))
		{
			break;
		}
		
		for(int32 j=0;j<QueryVector.Length();j++)
		{		
			nombreP=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[j]);					
		}
		

		
		this->BuscaRutaDeElementoYSubeRespaldoNota(IdElemento, Conte_Titulo, Conte_balazo, Conte_PieFoto, Conte_Contenido,FechaYMD,//Fecha de creacion
											nombreP,//Nombre de Publiucacion
											nombreS,//Nombre de seccion
											ID_Usuario);//Nombre del usuario Creador
											
		
		
 		this->OcultaYMuestraWidgetsPNNota(kFalse);
 		
		this->ActualizaTodasLNotasYDisenoPagina("N2PActualizacion");
 		
		
 		this->Fotos_UpdateCoordenadasEnBaseD(document);
 		
		

 		retval=kTrue;
 	}while(false);
	 
	 //N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::DoSubirABDNuevaNota fin");
 	return(retval);
 }	
 
 
 //Funcion que adiciona por lotes el contenido totalde un elemento de la nota siempre y cuando este exeda al 30,000 caracteres
bool16 N2PsqlUpdateStorysAndDBUtils::EnviarPorLotesContenidoDElemento_De_Nota(PMString Conte_Contenido,PMString IdElemento_Contenido)
 {
	 //N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::EnviarPorLotesContenidoDElemento_De_Nota ini");
 	do
 	{
 	
 	
 		PreferencesConnection PrefConections;		
			
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
 		///////////////
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
 		if(SQLInterface==nil)
		{
			break;
		} 
		
		
		
 		/********CONTENIDO DE ELEMENTO********/
		
		//CAlert::InformationAlert("FERNANDO LLANAS RDZ");
		
		PMString FirstParte="";
		int32 inicio=29999;
		int32 length=29999;
		
		
		
		
		
		
		PMString Buscar="";
		K2Vector<PMString> QueryContenido;
		bool16 pasarDNuevo=kTrue;
		PMString Contenido="";
		int32 CantidadDeCracteres=Conte_Contenido.NumUTF16TextChars();
		PMString AZS="";
		
		if(CantidadDeCracteres>0 && CantidadDeCracteres>30000)
		{
			if(CantidadDeCracteres > (inicio+length))
			{
				while(pasarDNuevo==kTrue)
				{
			
					FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
				
					Buscar = "CALL Append_Contenido_De_Id_ElementoSUBString(" + IdElemento_Contenido + ",'";
					Buscar.Append(FirstParte);
					Buscar.Append("')");
				
				
				
		
					//GuardarQuery(AZS+ ","+Buscar);
				
				
					QueryContenido.clear();
					SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
					}
					
					inicio = inicio + length;
				
				
				
					//CAlert::InformationAlert(AZS);
				 
					if((inicio+length)>CantidadDeCracteres)
					{
						length=(CantidadDeCracteres-inicio)-1;
						pasarDNuevo=kFalse;
						
					
						FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
				
						Buscar = "CALL Append_Contenido_De_Id_ElementoSUBString(" + IdElemento_Contenido + ",'";
						Buscar.Append(FirstParte);
						Buscar.Append("')");
					
						QueryContenido.clear();
						SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
						for(int32 j=0;j<QueryContenido.Length();j++)
						{
							Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
						}
					
						//CAlert::InformationAlert("DEBE SALIR");
					}
				
					
				}
			}
			else
			{
				
				length=(CantidadDeCracteres-inicio)-1;
			
				
				FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
				
				
				Buscar = "CALL Append_Contenido_De_Id_ElementoSUBString(" + IdElemento_Contenido + ",'";
				Buscar.Append(FirstParte);
				Buscar.Append("')");
				
				QueryContenido.clear();
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
				}
				
			}
		}
		
		
		
		
	
		/****************/
 	}while(false);
	 
	 //N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::EnviarPorLotesContenidoDElemento_De_Nota fin");
 	return(kTrue);
 }
 
 
 
 
 
bool16 N2PsqlUpdateStorysAndDBUtils::OcultaYMuestraWidgetsPNNota(bool16 Muestra)
{
	bool16 retval=kTrue;
	
	if(Muestra)
	{
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlTextStatusComboWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlBotonCancelNewNoteWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlBotonSubirNewNoteWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlComboRoutedToWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlStTextRoutedToWidgetID , kTrue);
		
		N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta( kN2PsqlTextEnGuiaWidgetID , kTrue);
		N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta( kN2PsqlComboguiaWidgetID , kTrue);
		
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlTextPageWidgetID , kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlStcTextPageWidgetID , kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlTextStatusWidgetID , kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlBotonBuscarWidgetID , kFalse);
		
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextPieWidgetID,kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextNotaWidgetID,kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextBalazoWidgetID,kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextTituloWidgetID,kFalse);
		
		N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta(kN2PsqlTextStatusComboWidgetID,kTrue);
	}
	else
	{
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlTextStatusComboWidgetID , kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlBotonCancelNewNoteWidgetID , kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlBotonSubirNewNoteWidgetID , kFalse);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlComboRoutedToWidgetID , kFalse );
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlStTextRoutedToWidgetID , kFalse);
		
		N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta( kN2PsqlTextEnGuiaWidgetID , kFalse);
		N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta( kN2PsqlComboguiaWidgetID , kFalse);
		
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlTextPageWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlStcTextPageWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlTextStatusWidgetID , kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta( kN2PsqlBotonBuscarWidgetID ,kTrue );
		
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextPieWidgetID,kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextNotaWidgetID,kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextBalazoWidgetID,kTrue);
		N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlStTextTituloWidgetID,kTrue);
		
	}
	
	
	
	
	
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::DoCrearPaseNota()
{
	bool16 retval=kTrue;
	IAbortableCmdSeq* seq = nil;
	ErrorCode result = kFailure;
	
	int32 numeroPaginaActual=0;
	
	//UID del TextModel del Frame que se encuentra seleccionado
	int32 UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
	PMString IDNota = "";
	PMString DSN_Nota_Seleted="";
	PMString ID_Usuario="";
	PMString GuiaNota="";
	do
	{
		seq = CmdUtils::BeginAbortableCmdSeq();
		
		if(UIDTextModelOfTextFrame<=0)
		{
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
		
		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		
		
		StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
		//Obtiene el ID_elemento_padre apartir del textframe seleccionado
		retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
		//SI NO ES ELMENTO DE UNA NOTA
		if(retval==kFalse)
		{
			CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
			break;
		}
		
		if(!CanDoCheckInNota(UIDTextModelOfTextFrame))
		{
			
			break;
		}
		
		
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}

		
		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}

	
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
		if(pTextTarget==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect pTextTarget pointer nil");
			break;
		}
			
		InterfacePtr<ITextModel> iTextModel(pTextTarget->QueryTextModel());
		if(iTextModel==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iTextModel pointer nil");
			break;
		}
		
		int32 length = iTextModel->TotalLength();//obtengo el total de caracteres de la cadena en el textframe
		
		RangeData range = pTextTarget->GetRange();
		
		//Validaciones
		if(range.Length()==0)
		{
			CAlert::InformationAlert("*You most select the text to page jump.");
			break;
		}
		
		if(range.End()!=length-1)
		{
			CAlert::InformationAlert("*You most select the story last character.");
			break;
		}
		
		
		
		InterfacePtr<IComposeScanner> cs(iTextModel, UseDefaultIID()); 
		// Setup a variable to retrieve the length of the text 
		if(cs==nil)
		{
			break;
		}
		// Setup a variable to retrieve the length of the text 
		
			
		WideString Temp;
		cs->CopyText(range, &Temp);
		// Setup a PMString to hold the text (depending on what you want to do with the text, the textchar may be ok) 
		// Load the text into the buf variable 
		//buf.SetXString(buffer, length); 
		PMString TextoDePase="";
		PMString TextoQueSeQueda="";
		TextoDePase.Append(Temp);
		
		RangeData Primari(0,range.Start(nil));
		
		cs->CopyText(Primari, &Temp);
		TextoQueSeQueda.Append(Temp);
		
		if(length-1 > range.End())
		{
			RangeData Second(range.End() , length-1);
			cs->CopyText(Second, &Temp);
			TextoQueSeQueda.Append(Temp);
		}
		
		//CAlert::InformationAlert(TextoDePase);
		//CAlert::InformationAlert(TextoQueSeQueda);
		
		
		
		
		
		
		
		PMString StringProcedure="";
		
		StringProcedure="DECLARE @Retval VARCHAR(50) EXEC CrearPaseDeElementoNota '";
					
		StringProcedure.Append(StructNota_IDFrames[0].Id_Elem_Cont_Note );
		StringProcedure.Append("','");
		StringProcedure.Append(TextoQueSeQueda);
		StringProcedure.Append("','");
		StringProcedure.Append(TextoDePase);
		StringProcedure.Append("',");
		StringProcedure.Append(" @Retval OUTPUT  SELECT @Retval AS SALIDA");
					
					
		//arma la cadena para la conexion a la base de datos local
			
		
		PMString StringConection="";
		K2Vector<PMString> Params;
		
		PreferencesConnection PrefConectionsOfServerRemote;
		
		if( !this->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
		{
			break;
		}
		
		StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
			
		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		if(!SQLInterface->SQLQueryDataBase(StringConection , StringProcedure, Params))
		{
			//CAlert::InformationAlert("Que paso.");
			break;
		}
		else
		{
			PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("SALIDA",Params[0]);
			if(RESULT=="0")
			{
				CAlert::InformationAlert(kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey);
				break;
			}
			else
			{
				if(RESULT=="YaExiste")
				{
					CAlert::InformationAlert("Ya existe un pase de este elemento de nota ");
					break;
				}
			}
				
		}
		
		//Borra el texto seleccionado
		
		result  = N2PSQLUtilities::DeleteText( iTextModel ,range.Start(nil) , range.Length());
		
		if(result==kFailure)
		{
			//CAlert::InformationAlert("No pudo barra el texto.");
			break;
		}
		
		//CAlert::InformationAlert("Desoues de crear pase");
					
		//PARA AVERIGUAR SI SE PUEDE HACER UN CHECKIN A ESTA NOTA quiero decir para saber si se esta editando la noa en este momento
		if(CanDoCheckInNota(UIDTextModelOfTextFrame))
		{
			PMString Estado="0";
			PMString RoutedTo="0";
			PMString PublicacionID="0";
			PMString SeccionID="0";
		
			//Actualiza a BD el contenido de los elementos de la nota,
			//Cambia el estado de escritura(Lapiz),
			//Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota, 
			//Libera la nota en la BD para ser modificad por cualquier otro usuario
			this->CheckInNotaDBAndPag(StructNota_IDFrames,Estado,RoutedTo,ID_Usuario,  PublicacionID, SeccionID, kTrue,document,GuiaNota);
			//CAlert::InformationAlert("Despues de Dar CheckIn nota");
			
			
		}
		else
		{
			break;
		}
		
		retval=kTrue;
		
		
			
	}while(false);	
	
	if(seq!=nil)
	{
		if(retval==kTrue)
		{
			CmdUtils::EndCommandSequence(seq);
			//Llenado de Lista y Datos de Elemento
			PMString Busqueda=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlBusquedaTextWidgetID);
			if(Busqueda.NumUTF16TextChars()>0)
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
			else
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
			
			
			

			PMString IDNotaStri=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
			if(IDNotaStri.NumUTF16TextChars()>0)
			{
				
	 			if(N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID))
	 			{
	 				//hacer busquedas para pases
	 				//N2PSQLUtilities::LlenarElementosDePasesEnVentanaN2PSQL();
	 			}
	 			else
	 			{
					Busqueda= "CALL QueryElementdNotas2 (" + IDNotaStri +")";
	 				N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kFalse);
	 			}
	 			
				
			}
		}
		else
		{
			CmdUtils::RollBackCommandSequence(seq);
			CmdUtils::AbortCommandSequence(seq);
		}
			
		seq=nil;
	}
	
		
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::ObtenUsuarioLogeadoActualmente(PMString& UsuarioLogeado)
{
	bool16 retval = kFalse;
	do
	{
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}		
		
		UsuarioLogeado = wrkSpcPrefs->GetIDUserLogedString();
		if(UsuarioLogeado.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert(kN2PSQLYouCouldSaveThisPageString);
			break;
		}
		
		retval = kTrue;
	}while(false);
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::CheckInElementoNota(PMString& Params, bool16 NotaEnModoCheckOut,K2Vector<PMString>& VectorRes)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInElementoNota ini");
	bool16 retval = kFalse;
	

	do
	{
		PMString StringConection="";
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{	
			break;
		}
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
		K2Vector<PMString> VectorQuery;					
						
		if(!SQLInterface->SQLQueryDataBase(StringConection , Params , VectorQuery))
		{
			
			retval=kFalse;
		}	
		else
		{
			VectorRes.push_back(SQLInterface->ReturnItemContentbyNameColumn("ID_Elem_Cont_NoteIN",VectorQuery[0]));
			VectorRes.push_back(SQLInterface->ReturnItemContentbyNameColumn("FechaCheckInParamOUT",VectorQuery[0]));
			//PMString MASA = SQLInterface->ReturnItemContentbyNameColumn("ESTATUS_ELEIN",VectorQuery[0]);
			//CAlert::InformationAlert(MASA);
			retval=kTrue;
		}
		
	}while(false);
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::CheckInElementoNota fin");
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::EstableceParametrosToCheckInElementoNota(PMString IDElementoPadre,
		PMString IDElemento,int32 UIDElementoNota, 
		PMString& LastCheckinNotaToXMP,PMString Estado,
		PMString RoutedTo,PMString IDUsuario,
		PMString PublicacionID,PMString SeccionID, 
		bool16 VieneDeCrearPase , IDocument* document,
		PMString& TextoExportado, bool16 isUpdate,
		PMString GuiaNotaToCheckIn)
{
	bool16 retval=kFalse;
	PMString Params;
	
	PMString CadenaTitulo="";
	PMString CadenaBalazo="";
	PMString CadenaPieFoto="";
	PMString CadenaContentNote= "";
	PMString sss="";
							
	PMRect CoorTitulo;
	int32	LineFaltTitulo=0;
	int32 	LineRestTitulo=0;
	int32	CarRestTitulo=0;
	int32	CarFaltTitulo=0;
	int32	CarTotTitulo=0;
	do
	{
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock db");
			break;
		}
			
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		
		CadenaTitulo.AppendNumber(UIDElementoNota);
		
		
		UID uidRef=UIDElementoNota;
		UIDRef textModelRef(db, uidRef);
		
		
		//////
		
		/*UIDRef ur(GetUIDRef(document));
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CanDoCheckInNota db == nil");
			break;
		}*/
		
		////////////////////Obtener texto de Texframe
		//UIDRef TextModelUIDRef(db,textModelRef);
		
		InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			ASSERT_FAIL("CanDoCheckInNota mytextmodel == nil");
			break;
		}
		
		//////
		//Para TextoTageado
		CadenaTitulo =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef);

		//Sin Texto tageado
		//PMString CadenaTitulo= N2PSQLUtilities::GetPMStringOfTextFrame(UIDElementoNota);
		
		//CAlert::InformationAlert("antes");
		N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(UIDElementoNota),CoorTitulo, LineFaltTitulo,
				LineRestTitulo,
				CarFaltTitulo,
				CarRestTitulo,
				CarTotTitulo); 
		//CAlert::InformationAlert("despues");
				
		bool16 NotaEsCheckOut=kFalse; //
		
		if(LineFaltTitulo<0)
		{
			LineFaltTitulo=0;
		}
		
		if(LineRestTitulo<0)
		{
			LineRestTitulo=0;
		}
		
		if(CarTotTitulo<0)
		{
			CarTotTitulo=0;
		}
		
			if(this->CanDoCheckInNota(UIDElementoNota)==kTrue)	//Si se puede dar checkIn
			{	
				NotaEsCheckOut=kTrue;
			
				if(VieneDeCrearPase==kTrue)
				{
					NotaEsCheckOut=kFalse;
				}
			}
			else
			{
				//CAlert::InformationAlert("Salio Cuando Intentaba dar check in o update de texto");
				NotaEsCheckOut=kFalse;
			
			}		
		
		PMString FirstParte="";
		if(CadenaTitulo.NumUTF16TextChars()>30000)
		{
			FirstParte=CadenaTitulo.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte=CadenaTitulo;
		}
			
		if(NotaEsCheckOut)
			Params="CALL NOTAELEMENTON2PCHECKINMejorWithPases2(";
		else
			Params="CALL NOTAELEMENTON2PCHECKINMejor2(";
		
		TextoExportado=CadenaTitulo;	
		sss="";
		Params.Append(IDElementoPadre);
		//CAlert::InformationAlert("EstableceParametrosToCheckInElementoNota IDUsuario= " + IDUsuario);
		Params.Append(",'");
		Params.Append(IDUsuario);
		Params.Append("','");
		Params.Append(RoutedTo);
		Params.Append("','");
		Params.Append(Estado);
		Params.Append("',");
		Params.Append(IDElemento);
		Params.Append(",'");					
		Params.Append(FirstParte);
		Params.Append("',");		
		sss="";
		sss.AppendNumber(CoorTitulo.Top() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Left());
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Bottom() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Right() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(LineFaltTitulo);
		
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(LineRestTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarFaltTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarRestTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarTotTitulo);
		
		Params.Append(sss);
		Params.Append(",");
								
		Params.Append("@X");
		Params.Append(",");
		
		if(isUpdate==kFalse)
		{
			if(Aplicacion.Contains("InDesign"))
				Params.Append("3");
			else
				Params.Append("16");
		}
		else
		{
			if(Aplicacion.Contains("InDesign"))
				Params.Append("9");
			else
				Params.Append("17");
		}
		Params.Append(",'");
						
		Params.Append(PublicacionID); //Publicacion
		Params.Append("','");
		Params.Append(SeccionID);	//Seccion
		Params.Append("','");
		//CAlert::InformationAlert(LastCheckinNotaToXMP);
		Params.Append(LastCheckinNotaToXMP);	//Fecha de Edicion*/
		Params.Append("','");
		Params.Append(GuiaNotaToCheckIn);
		Params.Append("')");
		
	
		
		//CAlert::InformationAlert(Params);
		
		K2Vector<PMString> VectorResults;
		if(!this->CheckInElementoNota( Params, NotaEsCheckOut,VectorResults))
		{
			
			break;
		}
						
		
		/****************/
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
			
			
			/****************/
		
		//CAlert::InformationAlert("FERNANDO LLANAS RDZ");
		
		FirstParte="";
		int32 inicio=29999;
		int32 length=29999;
		PMString Buscar="";
		K2Vector<PMString> QueryContenido;
		int32 vecespasadas=0;
		PMString Contenido="";
		int32 CantidadDeCracteres=CadenaTitulo.NumUTF16TextChars();
		
		/*
		*/
		
		if(CantidadDeCracteres>30000)
		{
			do
			{
				/*PMString AZS="";
				AZS.AppendNumber(CantidadDeCracteres);
				AZS.Append(", INICIO=");
				AZS.AppendNumber(inicio);
				AZS.Append(", TAMAÑO=");
				AZS.AppendNumber(length);
				CAlert::InformationAlert("Este es nuevo texto y su cantidad es de "+AZS);
				*/

				FirstParte=CadenaTitulo.Substring(inicio,length)->GrabCString();
				
				Buscar = "CALL Append_Contenido_De_Id_ElementoSUBString(" + IDElemento + ",'";
				Buscar.Append(FirstParte);
				Buscar.Append("')");
					
				//CAlert::InformationAlert(Buscar);
				
				//AZS.Append(", VECES PASADAS=");
				//AZS.AppendNumber(vecespasadas);
		
				//GuardarQuery(AZS+ ","+Buscar);
				//CAlert::InformationAlert("MAMAMAMAMAM "+AZS+ ","+Buscar);
				
				QueryContenido.clear();
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
				}
					
				inicio = inicio + length;
				
				//AZS="Sandunga ===";
				//AZS.AppendNumber(inicio);
				//AZS.Append(">");
				//AZS.AppendNumber(CantidadDeCracteres);
				
				//CAlert::InformationAlert(AZS);
				 
				if((inicio+length)>CantidadDeCracteres)
				{
					length=(CantidadDeCracteres-inicio)-1;
					vecespasadas++;
				}
				
				//AZS="";
				//AZS.Append(", VECES PASADAS=");
				//AZS.AppendNumber(vecespasadas);
				//CAlert::InformationAlert("Pasa por aca ZXXAASSA "+Contenido+" , " + AZS);
			}while(vecespasadas<2 && inicio< CantidadDeCracteres);
		}
		/****************/
		
		
		//CAlert::InformationAlert("Para Borra Exeso de Texto=" + Temp);
		if(VectorResults[0]=="1" && CarRestTitulo>0)
		{
			
																					
			////////obtener Uid del TextModel	
								
			UID UIDTextModel=UIDElementoNota;
				
			
		
			IDataBase* dbDoc = ::GetDataBase(document);
			if (dbDoc == nil)
			{
				
				break;
			}
			
				
			UIDRef TextModelUIDRef( dbDoc , UIDTextModel );
				
			InterfacePtr<ITextModel> mytextmodel1(TextModelUIDRef, UseDefaultIID()); 
			if(mytextmodel==nil)
			{
				
				break;
			}
					
			
																	
			ErrorCode result  = N2PSQLUtilities::DeleteText( mytextmodel1 , CarTotTitulo - CarRestTitulo ,CarRestTitulo -1);
		
			if(result==kFailure)
			{
			
				break;
			}
		}
					
		LastCheckinNotaToXMP=VectorResults[1];
		//CAlert::InformationAlert("ASSDX="+LastCheckinNotaToXMP);	
		retval=kTrue;
	}while(false);
	return(retval);
						
}



/////////////////////////////////////////////////////////////////////////////////////
//////			CAMBIA ESTADO y LOCK O UNLOCK DE LA NOTA					////////
////////////////////////////////////////////////////////////////////////////////////
bool16 N2PsqlUpdateStorysAndDBUtils::CambiaEstadoYLapizDeNota(StructIdFramesElementosXIdNota* VectorNota,bool16 setLock,PMString EstadoNuevo,bool16 ChangeStatus,int32 numNotaEnEstruct)	
{
	bool16 retval=kTrue;
	do
	{
	
		if(VectorNota[numNotaEnEstruct].UIDFrContentNote>0)
		{
			retval = this->CambiaEstadoYLapizDeElementoNOTA(VectorNota[numNotaEnEstruct].UIDFrContentNote, setLock, EstadoNuevo, ChangeStatus);
			if(!retval)
				retval=kFalse;
		}
		
		if(VectorNota[numNotaEnEstruct].UIDFrTituloNote>0)
		{
			retval = this->CambiaEstadoYLapizDeElementoNOTA(VectorNota[numNotaEnEstruct].UIDFrTituloNote, setLock, EstadoNuevo, ChangeStatus);
			if(!retval)
				retval=kFalse;
		}
		
		if(VectorNota[numNotaEnEstruct].UIDFrPieFotoNote>0)
		{
			retval = this->CambiaEstadoYLapizDeElementoNOTA(VectorNota[numNotaEnEstruct].UIDFrPieFotoNote, setLock, EstadoNuevo, ChangeStatus);
			if(!retval)
				retval=kFalse;
		}
		
		if(VectorNota[numNotaEnEstruct].UIDFrBalazoNote>0)
		{
			retval = this->CambiaEstadoYLapizDeElementoNOTA(VectorNota[numNotaEnEstruct].UIDFrBalazoNote, setLock, EstadoNuevo, ChangeStatus);
			if(!retval)
				retval=kFalse;
		}
		
	}while(false);
	return(retval);
}

////////////////////////////////////////////////////////////////////////////////////
//////			CAMBIA ESTADO y LOCK UNLOCK DEL ELEMENTO DE LA NOTA			////////
////////////////////////////////////////////////////////////////////////////////////
bool16 N2PsqlUpdateStorysAndDBUtils::CambiaEstadoYLapizDeElementoNOTA(int32 UIDFrElementoNota, bool16 setLock,PMString EstadoNuevo,bool16 ChangeStatus)
{
	bool16 retval=kFalse;
		
		PMString UIDString="";
		
		if(UIDFrElementoNota>0)
		{
			
			UIDString="";
			UIDString.AppendNumber(UIDFrElementoNota);
				
			//Cambia el estado de escritura(Lapiz),
			this->CambiaModoDeEscrituraDElementoNota(UIDString,setLock);
			
			if(ChangeStatus==kTrue)
			{
				//Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota,
				if(EstadoNuevo!="0")
				{
					this->CambiaTipoDEstadoDElementoNota(UIDString,EstadoNuevo,kTrue);
				}
				
			}
				
				retval=kTrue;
		}
	return(retval);
}



/////////////////////////////////////////////////////////////////////////////////////
//////			CAMBIA EL TIPO DE ESTADO DE UN ELEMENTO DE LA NOTA 	(frame)	 ////////
////////////////////////////////////////////////////////////////////////////////////
ErrorCode N2PsqlUpdateStorysAndDBUtils::CambiaTipoDEstadoDElementoNota(PMString UIDModelString,PMString EstadoNota,bool16 Visible)
{	
	ErrorCode error=kFailure;
	do
	{
		PMString Consulta="";
		////////obtener Uid del TextModel
		int32 intthis=UIDModelString.GetAsNumber();
		UID UIDTextModel=intthis;
					
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaTipoDEstadoDElementoNota document");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaTipoDEstadoDElementoNota db");
			break;
		}
		////////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaTipoDEstadoDElementoNota mytextmodel");
			break;
		}
		
		//Obtiene la lista de Frames que contiene al TextModel Actual
		IFrameList* frameList= mytextmodel->QueryFrameList();
		
		 
		UIDRef refFrame;
		//Ciclo para cambiar el adornament de cada uno de los frames que contiene el Teextmodel Actual.
		for(int32 Framecount=0 ; Framecount<frameList->GetFrameCount() ; Framecount++)
		{
			
			InterfacePtr<ITextFrameColumn > textFrame(frameList->QueryNthFrame(Framecount), UseDefaultIID());
			if(textFrame==nil)
			{
				ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaTipoDEstadoDElementoNota textFrame pointer nil");
				continue;
			}
			
			  
			InterfacePtr<IMultiColumnTextFrame> MultiColumnTextFrame(textFrame->QueryMultiColumnTextFrame (), UseDefaultIID());
			if(MultiColumnTextFrame==nil)
			{
				ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaTipoDEstadoDElementoNota textFrame pointer nil");
				continue;
			}
			
			
			//UIDRef refFrame = N2PSQLUtilities::GetGraphicFrameRef(MultiColumnTextFrame, kTrue);
			
			 SDKLayoutHelper layoutHelper;
             UIDRef sourceFrameRef = layoutHelper.GetGraphicFrameRef(textFrame); 
			
			 UIDList selectUIDList=UIDList(sourceFrameRef);
			
			
			
			//Interfaz para cambiar el adornament(adorno) que muestra el tipo Estado del elemento de la nota
			InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
			(
				kN2PAdIssueUtilsBoss,	// Object boss/class
				IN2PAdIssueUtils::kDefaultIID
			)));
		
			if(N2PAdIssueUtil==nil)
			{
				break;
			}
			
			UIDList ListFrame=UIDList(sourceFrameRef);
			//CAlert::InformationAlert("A chingado: "+EstadoNota);
			error = N2PAdIssueUtil->UpdateUIDListLabelfor( ListFrame,EstadoNota, Visible);
		}		
	}while(false);
	return(error);
}




//SetLockPositionCmd
ErrorCode N2PsqlUpdateStorysAndDBUtils::CambiaModoDeEscrituraDElementoNota(PMString UIDModelString,bool16 setLock)	
{

	ErrorCode error=kFailure;
	do
	{
	//	PMString Consulta="";
		////////obtener Uid del TextModel
		int32 intthis=UIDModelString.GetAsNumber();
		UID UIDTextModel=intthis;
					
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("document is invalid");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		////////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			
			ASSERT_FAIL("mytextmodel is invalid");
			break;
		}
		
		InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
		if(frameList==nil)
		{
			ASSERT_FAIL("frameList");
			break;
		}
		
		InterfacePtr<IItemLockData> textLockData(mytextmodel,UseDefaultIID());
		if(textLockData==nil)
		{
			ASSERT_FAIL("textLockSDta");
			break;
		}

		
		UIDList ListFramer=UIDList(TextModelUIDRef);
			
		
		
		
		///// Proceso Cambia Modo Lock Unlock para escritura de 
		InterfacePtr<ICommand>	setLPCmd(CmdUtils::CreateCommand(kSetItemLockDataCmdBoss));
		if(setLPCmd==nil)
		{
			break;
		}
			
		// Set the SetLockPositionCmd's ItemList:
		setLPCmd->SetItemList(ListFramer);
		// Get an IIntData Interface for the SetLockPositionCmd:
		InterfacePtr<IBoolData> boolData(setLPCmd, IID_IBOOLDATA);
		if(boolData==nil)
		{
			ASSERT_FAIL("boolData is invalid");
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		PMString nameApp=app->GetApplicationName();
		nameApp.SetTranslatable(kFalse);
		
		// Set the IIntData Interface's data:
		if(setLock==kTrue)
		{
			
			boolData->Set(kTrue);
			
		}
		else
		{
			boolData->Set(kFalse);
			
		}
		
		
		error = CmdUtils::ProcessCommand(setLPCmd);
		// Process the SetLockPositionCmd:
		if (error != kSuccess)
		{
			break;
			ASSERT_FAIL("Can't process SetLockPositionCmd");
		}
		////////////////
			
	if(setLock==kTrue)
		{
			
			
			if(nameApp.Contains("InDesign"))
			{
				//falta instruccion para manejar que no se pueda insertar texto
				this->CambiaEstadoLapizUnLock(UIDModelString, 0,kTrue);
			}
			else
			{
				//this->CambiaEstadoLapizUnLock(UIDModelString, 0,kFalse);
				this->CambiaEstadoLapizUnLock(UIDModelString, 0,kTrue);
				textLockData->SetAttributeLock(kFalse);
			}
          
		}
		else
		{
			
			//this->CambiaEstadoLapizUnLock(UIDModelString, 1,kTrue);	
			if(nameApp.Contains("InDesign"))
			{
				//falta instruccion para manejar que no se pueda insertar texto
				this->CambiaEstadoLapizUnLock(UIDModelString, 0,kFalse);
			}
			else
			{
				
				//Para que no pueda modificar el Tipo y tamaÒo de texto(font)
				textLockData->SetAttributeLock(kTrue);
				this->CambiaEstadoLapizUnLock(UIDModelString, 0,kFalse);
			}
			
		}
	/*	if(nameApp.Contains("InDesign"))
		{
			//falta instruccion para manejar que no se pueda insertar texto
			this->CambiaEstadoLapizUnLock(UIDModelString, 0,setLock);
		}
		else
		{
			//this->CambiaEstadoLapizUnLock(UIDModelString, 0,kFalse);
			this->CambiaEstadoLapizUnLock(UIDModelString, 0,setLock);
			textLockData->SetAttributeLock(!setLock);
		}*/
		error = kSuccess;
	}while(false);
	return(error);
}



void N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock(PMString UIDModelString, int32 lockLapiz, bool16 Visible)
{	
	do
	{
		PMString Consulta="";
		////////obtener Uid del TextModel
		int32 intthis=UIDModelString.GetAsNumber();
		UID UIDTextModel=intthis;
					
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock document");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock db");
			break;
		}
		////////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock mytextmodel");
			break;
		}
					
		IFrameList* frameList= mytextmodel->QueryFrameList();
		
		 
		UIDRef refFrame;
		for(int32 Framecount=0 ; Framecount<frameList->GetFrameCount() ; Framecount++)
		{
			
			InterfacePtr<ITextFrameColumn > textFrame(frameList->QueryNthFrame(Framecount), UseDefaultIID());
			if(textFrame==nil)
			{
				
				ASSERT_FAIL("textFrame pointer nil");
			}
			
			
			 SDKLayoutHelper layoutHelper;
             UIDRef sourceFrameRef = layoutHelper.GetGraphicFrameRef(textFrame); 
			
			
			
			InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
			(
				kN2PAdIssueUtilsBoss,	// Object boss/class
				IN2PAdIssueUtils::kDefaultIID
			)));
		
			if(N2PAdIssueUtil==nil)
			{
				
				
				break;
			}
			
			UIDList ListFrame=UIDList(sourceFrameRef);
			
			
			N2PAdIssueUtil->UpdateUIDListLabelforPenUnLock(ListFrame, lockLapiz, Visible);
			
		}		
	}while(false);

}

bool16 N2PsqlUpdateStorysAndDBUtils::Insert_IN_Bitacora_Elemento(PMString Fecha_Edicion,PMString ID_Elemento_padre,PMString IDUsuario, PMString Estado,const bool16& SaveRevisionElemento)
{
	bool16 retval=kTrue;
	do
	{
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		PMString Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		PMString ID_Evento="";
		
		if(!SaveRevisionElemento)
		{
			if(Aplicacion.Contains("InDesign"))
				ID_Evento.Append("3");
			else
				ID_Evento.Append("16");
		}
		else
		{
			if(Aplicacion.Contains("InDesign"))
				ID_Evento.Append("9");
			else
				ID_Evento.Append("17");
		}
			
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		PMString Consulta="CALL n2pV1055_insert_en_Bitacora_elemento ('"+Fecha_Edicion+"',"+ID_Elemento_padre+",'"+IDUsuario+"','"+Estado+"',"+ID_Evento+")";
		
		//CAlert::InformationAlert(Estado);
		
		K2Vector<PMString> QueryVector;
		
		if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
		{
			break;
		}
		else
		{
			PMString RESULT = SQLInterface->ReturnItemContentbyNameColumn("RESULTADOOUT",QueryVector[0]);
			
			if(RESULT=="0")
			{
				CAlert::InformationAlert(kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey);
				break;
			}
			
			PMString IdBitacoraElemento = SQLInterface->ReturnItemContentbyNameColumn("Bitacora_Elemento_ID",QueryVector[0]);
			
			this->EnviaAVersionDNota(ID_Elemento_padre,IDUsuario,Estado,ID_Evento,IdBitacoraElemento,Fecha_Edicion);
		}	
	}while(false);
	return(retval);
}




bool16 N2PsqlUpdateStorysAndDBUtils::GetElementosNotaDXMP_Of_UIDTextModel(int32 UIDTextModelOfTextFrame,StructIdFramesElementosXIdNota* StructNota_IDFrames,IDocument* document)
{
	bool16 encontro=kFalse;
	int32 contNotasInVector=100;
	
	//llena la estructura de notas ya fluidas
	StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[100];
	
	this->LlenarVectorElemDNotasFluidas( VectorNota,contNotasInVector, document);
	
	if(contNotasInVector==0)//quiere decir que no existe alguna nota fluida sobre el documento
	{
		return encontro;
	}
	
	for(int32 i=0;i<contNotasInVector && encontro==kFalse;i++)
	{
		if(UIDTextModelOfTextFrame==VectorNota[i].UIDFrContentNote)
		{
			UIDTextModelOfTextFrame=i;
			encontro=kTrue;
		}
		else
		{
			if(UIDTextModelOfTextFrame==VectorNota[i].UIDFrTituloNote)
			{
				UIDTextModelOfTextFrame=i;
				encontro=kTrue;
			}
			else
			{
				if(UIDTextModelOfTextFrame==VectorNota[i].UIDFrBalazoNote)
				{
					UIDTextModelOfTextFrame=i;
					encontro=kTrue;
				}
				else
				{
					if(UIDTextModelOfTextFrame==VectorNota[i].UIDFrPieFotoNote)
					{
						UIDTextModelOfTextFrame=i;
						encontro=kTrue;
					}
				}
			}
		}
	}
	
	if(encontro==kTrue)
	{
		StructNota_IDFrames[0].ID_Elemento_padre	= 	VectorNota[UIDTextModelOfTextFrame].ID_Elemento_padre;
		StructNota_IDFrames[0].DSNNote				=	VectorNota[UIDTextModelOfTextFrame].DSNNote;
		StructNota_IDFrames[0].UIDFrTituloNote		=	VectorNota[UIDTextModelOfTextFrame].UIDFrTituloNote;
		StructNota_IDFrames[0].UIDFrPieFotoNote		=	VectorNota[UIDTextModelOfTextFrame].UIDFrPieFotoNote;
		StructNota_IDFrames[0].UIDFrBalazoNote		=	VectorNota[UIDTextModelOfTextFrame].UIDFrBalazoNote;
		StructNota_IDFrames[0].UIDFrContentNote		=	VectorNota[UIDTextModelOfTextFrame].UIDFrContentNote;
		StructNota_IDFrames[0].LastCheckInNote		=	VectorNota[UIDTextModelOfTextFrame].LastCheckInNote;
		
		StructNota_IDFrames[0].Id_Elem_Cont_Note	=	VectorNota[UIDTextModelOfTextFrame].Id_Elem_Cont_Note;
		StructNota_IDFrames[0].Id_Elem_Tit_Note		=	VectorNota[UIDTextModelOfTextFrame].Id_Elem_Tit_Note;
		StructNota_IDFrames[0].Id_Elem_PieFoto_Note	=	VectorNota[UIDTextModelOfTextFrame].Id_Elem_PieFoto_Note;
		StructNota_IDFrames[0].Id_Elem_Balazo_Note	=	VectorNota[UIDTextModelOfTextFrame].Id_Elem_Balazo_Note;
	}
	
	return(encontro);
	
}

bool16 N2PsqlUpdateStorysAndDBUtils::GetElementosNotaDXMP_Of_IDElementoPadre(PMString IdElementoPadre,StructIdFramesElementosXIdNota* StructNota_IDFrames,IDocument* document)
{
	bool16 encontro=kFalse;
	int32 contNotasInVector=100;
	
	//llena la estructura de notas ya fluidas
	StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[100];
	
	this->LlenarVectorElemDNotasFluidas( VectorNota,contNotasInVector, document);
	
	if(contNotasInVector==0)//quiere decir que no existe alguna nota fluida sobre el documento
	{
		return encontro;
	}
	
	int32 numberNote=-1;
	for(int32 i=0;i<contNotasInVector && encontro==kFalse;i++)
	{
		//CAlert::InformationAlert(IdElementoPadre+"=="+VectorNota[i].ID_Elemento_padre);
		if(IdElementoPadre==VectorNota[i].ID_Elemento_padre)
		{
			numberNote=i;
			encontro=kTrue;
		}
	}
	
	if(encontro==kTrue)
	{
		StructNota_IDFrames[0].ID_Elemento_padre	= 	VectorNota[numberNote].ID_Elemento_padre;
		StructNota_IDFrames[0].DSNNote				=	VectorNota[numberNote].DSNNote;
		StructNota_IDFrames[0].UIDFrTituloNote		=	VectorNota[numberNote].UIDFrTituloNote;
		StructNota_IDFrames[0].UIDFrPieFotoNote		=	VectorNota[numberNote].UIDFrPieFotoNote;
		StructNota_IDFrames[0].UIDFrBalazoNote		=	VectorNota[numberNote].UIDFrBalazoNote;
		StructNota_IDFrames[0].UIDFrContentNote		=	VectorNota[numberNote].UIDFrContentNote;
		StructNota_IDFrames[0].LastCheckInNote		=	VectorNota[numberNote].LastCheckInNote;
		
		StructNota_IDFrames[0].Id_Elem_Cont_Note	=	VectorNota[numberNote].Id_Elem_Cont_Note;
		StructNota_IDFrames[0].Id_Elem_Tit_Note		=	VectorNota[numberNote].Id_Elem_Tit_Note;
		StructNota_IDFrames[0].Id_Elem_PieFoto_Note	=	VectorNota[numberNote].Id_Elem_PieFoto_Note;
		StructNota_IDFrames[0].Id_Elem_Balazo_Note	=	VectorNota[numberNote].Id_Elem_Balazo_Note;
		
		if(StructNota_IDFrames[0].Id_Elem_PieFoto_Note.NumUTF16TextChars()>0)
		{
			StructNota_IDFrames[0].EsArticulo=kFalse;
		}
		else
			StructNota_IDFrames[0].EsArticulo=kTrue;
	}
	
	return(encontro);
	
}



bool16 N2PsqlUpdateStorysAndDBUtils::AlgunElementoSeEncuentraCheckauteado()
{
	bool16 retval=kFalse;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			ASSERT_FAIL("FilActActionComponent::DoSave: document invalid");
			break;
		}
		
		PMString N2PID_Pagina = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
			
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
						
		PMString CadenaAMostrar="";
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		PMString Consulta;
		Consulta="CALL AlgunElementoDPaginaEstaRetirado("+ N2PID_Pagina +")";
		K2Vector<PMString> QueryVector;
		if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
		{
				break;
		}
		else
		{
			if(QueryVector.Length()>0)
			{
				for(int32 j=0;j<QueryVector.Length();j++)
				{
					PMString EncontroNotaCkOut = SQLInterface->ReturnItemContentbyNameColumn("EncontroNota",QueryVector[j]);
					if(EncontroNotaCkOut=="1")
					{
						PMString Contenido = SQLInterface->ReturnItemContentbyNameColumn("ContenidoNota",QueryVector[0]);
						PMString Id_Usuario	 = SQLInterface->ReturnItemContentbyNameColumn("ID_UsuarioElementoChecauteado",QueryVector[0]);
						PMString Id_ElementoNota	 = SQLInterface->ReturnItemContentbyNameColumn("ID_ElementoEnPaginaChecauteado",QueryVector[0]);
						
						K2Vector<N2PNotesStorage*> fBNotesStore;
						UIDRef itemToPlace = N2PSQLUtilities::ImportTaggedTextOfPMString(Contenido, fBNotesStore);
					
						if(itemToPlace != UIDRef::gNull)
						{	//convierte el texto tageado a texto plano
							Contenido = N2PSQLUtilities::GetTextPlainOfUIDRefImportTextTagged(itemToPlace);
						}
					
					
						CadenaAMostrar=Id_ElementoNota+","+Id_Usuario+","+Contenido+",\n";
					}
				}
								
			}
		}
		
		if(CadenaAMostrar.NumUTF16TextChars()>0)
		{
			PMString AlertMsg(kN2PSQLElementosMostradosEstanEnUsoStringKey);
			AlertMsg.Translate();
			CAlert::InformationAlert(AlertMsg+"\n"+CadenaAMostrar);
			retval=kTrue;
		}
	}while(false);
	return(retval);
}









//Checa las notas que se estan editando actualmente 
//Guarda el contenido de cada uno de los elementos de la nota en la BD
bool16 N2PsqlUpdateStorysAndDBUtils::GuardarContenidoNotasEnEdicion()
{
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::GuardarContenidoNotasEnEdicion ini");
	
	bool16 retval=kFalse;
	PMString ItemToSearchString="";
	PMString UIDModelString="";
	PMString IDNotaString="";
	PMString Busqueda="";
	PMString IDUsuario="";	
	PMString Aplicacion="";	
		
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		PMString Fecha_Edicion_NotaXXX =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_NotaXXX);
 		
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			break;	
		}
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		Aplicacion = app->GetApplicationName();
		
		Aplicacion.SetTranslatable(kFalse);
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}
	
	
		
		
		
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		K2Vector<int32> Notas;	
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		if(contNotasInVector>0)
		{
			
				//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
				for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
				{	
					
					
					if(!this->CanDoCheckInNota(VectorNota[numNota].UIDFrContentNote) && 
						!this->CanDoCheckInNota(VectorNota[numNota].UIDFrTituloNote) &&
						!this->CanDoCheckInNota(VectorNota[numNota].UIDFrPieFotoNote) &&
						!this->CanDoCheckInNota(VectorNota[numNota].UIDFrBalazoNote))
					{	//CAlert::InformationAlert("SE supone que no hixzo Update");
						continue;
					}
					else
					{
						
						//Verifica que el ultimo usuario que retiro la nota sea el mismo que esta depositandola
						//Validsacion Guarachin
						/*if(!ValidaSiUsuario_P_CheckInEnBaseDDatos(VectorNota[numNota].ID_Elemento_padre,IDUsuario))
						{
							//Significa que alguien mas retiro la nota por lo tanto no puede modificar nada sobre la base de datos.
							//Debe de notificar al usuario que no puede depositar la nota y que esta sera deshabilitada para no generar 
							//Errores en la versiones de las nota
							CAlert::InformationAlert(kN2PSQLNotaRetiradaPorOtroUsuarioDepositoVisualStringKey);
							
							StructIdFramesElementosXIdNota* VectorCheckInNoteToChangeEstatus=new StructIdFramesElementosXIdNota[1];	
							
							VectorCheckInNoteToChangeEstatus[0]=VectorNota[numNota];
								
							if(!CambiaEstadoYLapizDeNota(VectorCheckInNoteToChangeEstatus,kTrue,"",kFalse))
							{
								continue;
							}
							
							continue;
						}
						*/
						
						PMString Conte_Titulo="";
						PMString Conte_balazo="";//Contenido del Balazo
						PMString Conte_PieFoto="";//Contenido del pir de foto
						PMString Conte_Contenido="";//Contenido de la nota
					
						//CAlert::InformationAlert("SE supone que no hixzo Update");
						
						PMString LastCheckinNotaToQuery="";
						PMString Fecha_Edicion="0";
						LastCheckinNotaToQuery=Fecha_Edicion;
						Fecha_Edicion=MySQLFecha;
						
						
						if(VectorNota[numNota].UIDFrContentNote>0)
							if(!this->GuardarElementoDNotaEnBDSinCheckIN(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Cont_Note, VectorNota[numNota].UIDFrContentNote, Fecha_Edicion , "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Contenido))			
								break;
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;
						
						
						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrTituloNote>0)
							if(!this->GuardarElementoDNotaEnBDSinCheckIN(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Tit_Note, VectorNota[numNota].UIDFrTituloNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_Titulo))			
								break;
						
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;
						
						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrPieFotoNote>0)
							if(!this->GuardarElementoDNotaEnBDSinCheckIN(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_PieFoto_Note, VectorNota[numNota].UIDFrPieFotoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_PieFoto))			
								break;
						
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;

						Fecha_Edicion=MySQLFecha;
						if(VectorNota[numNota].UIDFrBalazoNote>0)
							if(!this->GuardarElementoDNotaEnBDSinCheckIN(VectorNota[numNota].ID_Elemento_padre,  VectorNota[numNota].Id_Elem_Balazo_Note, VectorNota[numNota].UIDFrBalazoNote, Fecha_Edicion ,  "0",  "0",  IDUsuario,  "0",  "0", kFalse, document,Conte_balazo))			
									break;
						
						
						
						if(Fecha_Edicion!=MySQLFecha)
							LastCheckinNotaToQuery=Fecha_Edicion;
						//Fecha_Edicion="0";
						//CAlert::InformationAlert("YYYYYY"+LastCheckinNotaToQuery);
						this->Insert_IN_Bitacora_Elemento(LastCheckinNotaToQuery,VectorNota[numNota].ID_Elemento_padre,IDUsuario,"0",kTrue);
						if(LastCheckinNotaToQuery.NumUTF16TextChars()>0)
						{
								StructIdFramesElementosXIdNota* VectorCheckInNoteToChangeEstatus=new StructIdFramesElementosXIdNota[1];	
								VectorCheckInNoteToChangeEstatus[0]=VectorNota[numNota];
							
								//if(!CambiaEstadoYLapizDeNota(VectorCheckInNoteToChangeEstatus,kTrue,"",kFalse))
								//{
								//	break;
								//}
							
							
									
								if(LastCheckinNotaToQuery.NumUTF16TextChars()>0)
								{
									N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[numNota].ID_Elemento_padre,LastCheckinNotaToQuery, document);
								}
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrTituloNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrTituloNote);
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrPieFotoNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrPieFotoNote);
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrBalazoNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrBalazoNote);
							
								if(VectorCheckInNoteToChangeEstatus[0].UIDFrContentNote>0)
									Notas.push_back(VectorCheckInNoteToChangeEstatus[0].UIDFrContentNote);
								
						}
						
					}		
			}
				
				//this->guardarIDNotasPUpdateDesign(Notas);
		}
		retval=kTrue;
		//delete VectorNota;
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::GuardarContenidoNotasEnEdicion fin");
	return(retval);
}





bool16 N2PsqlUpdateStorysAndDBUtils::GuardarElementoDNotaEnBDSinCheckIN(PMString IDElementoPadre,PMString IDElemento,int32 UIDElementoNota, PMString& LastCheckinNotaToXMP,PMString Estado,PMString RoutedTo,PMString IDUsuario,PMString PublicacionID,PMString SeccionID, bool16 VieneDeCrearPase , 
		IDocument* document,
		PMString& TextoExportado)
{
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::GuardarElementoDNotaEnBDSinCheckIN ini");
	bool16 retval=kFalse;
	PMString Params;
	
	PMString CadenaTitulo="";
	PMString CadenaBalazo="";
	PMString CadenaPieFoto="";
	PMString CadenaContentNote= "";
	PMString sss="";
							
	PMRect CoorTitulo;
	int32	LineFaltTitulo=0;
	int32 	LineRestTitulo=0;
	int32	CarRestTitulo=0;
	int32	CarFaltTitulo=0;
	int32	CarTotTitulo=0;
	do
	{
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock db");
			break;
		}
			
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		
		CadenaTitulo.AppendNumber(UIDElementoNota);
		
		
		UID uidRef=UIDElementoNota;
		UIDRef textModelRef(db, uidRef);
		
		
		//Para TextoTageado
		CadenaTitulo =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef);

		
		//Sin Texto tageado
		//PMString CadenaTitulo= N2PSQLUtilities::GetPMStringOfTextFrame(UIDElementoNota);
		
		N2PSQLUtilities::GETCoordenadasDePrimerFrameOfTextModel(UID(UIDElementoNota),CoorTitulo, CarTotTitulo); 
				
		bool16 NotaEsCheckOut=kFalse; //
		
		if(LineFaltTitulo<0)
		{
			LineFaltTitulo=0;
		}
		
		if(LineRestTitulo<0)
		{
			LineRestTitulo=0;
		}
		
		if(CarTotTitulo<0)
		{
			CarTotTitulo=0;
		}
		
		if(this->CanDoCheckInNota(UIDElementoNota)==kTrue)	//Si se puede dar checkIn
		{	
			NotaEsCheckOut=kTrue;
		
			if(VieneDeCrearPase==kTrue)
			{
				NotaEsCheckOut=kFalse;
			}
		}
		else
		{
			//CAlert::InformationAlert("Salio Cuando Intentaba dar check in o update de texto");
			NotaEsCheckOut=kFalse;
			
		}	
		
		PMString FirstParte_Conte_balazo="";
		if(CadenaTitulo.NumUTF16TextChars()>30000)
		{
			FirstParte_Conte_balazo=CadenaTitulo.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte_Conte_balazo=CadenaTitulo;
		}
		
		
		/*if(CadenaTitulo.NumUTF16TextChars()<=0)
		{
			Params="CALL NOTAELEMENTON2PSaveContentAndCoordenadas(";
		}
		else
		{
			if(NotaEsCheckOut)
			{
				
			}
				
		
		}*/
		Params="CALL NOTAELEMENTON2PSaveContentAndCoordenadas(";
		
		
		TextoExportado=CadenaTitulo;	
		sss="";
		Params.Append(IDElementoPadre);
		//CAlert::InformationAlert("EstableceParametrosToCheckInElementoNota IDUsuario= " + IDUsuario);
		Params.Append(",'");
		Params.Append(IDUsuario);
		Params.Append("','");
		Params.Append(RoutedTo);
		Params.Append("','");
		Params.Append(Estado);
		Params.Append("',");
		Params.Append(IDElemento);
		Params.Append(",'");					
		Params.Append(FirstParte_Conte_balazo);
		Params.Append("',");		
		sss="";
		sss.AppendNumber(CoorTitulo.Top() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Left());
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Bottom() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Right() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(LineFaltTitulo);
		
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(LineRestTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarFaltTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarRestTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarTotTitulo);
		
		Params.Append(sss);
		Params.Append(",");
								
		Params.Append("@X");
		Params.Append(",");
					
		if(Aplicacion.Contains("InDesign"))
			Params.Append("9");
		else
			Params.Append("17");
		Params.Append(",'");
						
		Params.Append(PublicacionID); //Publicacion
		Params.Append("','");
		Params.Append(SeccionID);	//Seccion
		Params.Append("','");
		//CAlert::InformationAlert(LastCheckinNotaToXMP);
		Params.Append(LastCheckinNotaToXMP);	//Fecha de Edicion*/
		Params.Append("')");
		
		
		
		//CAlert::InformationAlert(Params);
		K2Vector<PMString> VectorResults;
		if(!this->CheckInElementoNota( Params, NotaEsCheckOut,VectorResults))
		{
			
			break;
		}
		
		this->EnviarPorLotesContenidoDElemento_De_Nota(TextoExportado,IDElemento);
						
			
		LastCheckinNotaToXMP=VectorResults[1];
		//CAlert::InformationAlert("ASSDX="+LastCheckinNotaToXMP);	
		retval=kTrue;
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::GuardarElementoDNotaEnBDSinCheckIN fin");
	return(retval);
						
}

ErrorCode  N2PsqlUpdateStorysAndDBUtils::SetShortCutToMenuFittingActions()
{
	ErrorCode error=kSuccess;
	do
	{
		/*
			int16 mods = kShiftOption; 
			VirtualKey key_S; 
			PMString ctrshiftS="";

				
			PMString IdiomaActual=PMString(kN2PsqlIdiamaActualStringKey);
			IdiomaActual.Translate();
			IdiomaActual.SetTranslatable(kFalse);
			//IdiomaActual.SetTranslatable(kTrue);

			//CAlert::InformationAlert(IdiomaActual);

			if(IdiomaActual=="English")
			{
				
#ifdef MACINTOSH
				
			ctrshiftS =PMString("Opt+Shift+U"); 
#endif
#ifdef WINDOWS
			
			ctrshiftS =PMString("Shift+Alt+U" );//"Alt+May˙s+U"
#endif
			}
			else
			{
				
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+U"); 
#endif
#ifdef WINDOWS
			
			ctrshiftS = PMString("Alt+May˙s+0");//
#endif
			}
			ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			//CAlert::InformationAlert(ctrshiftS);

			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication()); 
			InterfacePtr<IActionManager> actionMgr(app->QueryActionManager()); 
			InterfacePtr<IShortcutManager> shortcutMgr(actionMgr, UseDefaultIID()); 
			InterfacePtr<IShortcutContext> context(shortcutMgr->QueryShortcutContextByName(kDefaultContext)); 
			
			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			PMString short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PsqlRetirarPaginaActionID, kFalse); 
			bool16 assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PsqlRetirarPaginaActionID, kDefaultContext, key_S, mods); 
			}
			
			if(IdiomaActual=="English")
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+N"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Shift+Alt+N"); 
#endif
			}
			else
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+N"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Alt+May˙s+P");//
#endif
			}

			
			//ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			///CAlert::InformationAlert(ctrshiftS);

			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PsqlDepositarPaginaActionID, kFalse); 
			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PsqlDepositarPaginaActionID, kDefaultContext, key_S, mods); 
			}

			if(IdiomaActual=="English")
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+O"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Shift+Alt+O"); 
#endif
			}
			else
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+O"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Alt+May˙s+R");//
#endif
			}

	
			//ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			//CAlert::InformationAlert(ctrshiftS);

			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PSQLCheckOutNotaActionID, kFalse); 
			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PSQLCheckOutNotaActionID, kDefaultContext, key_S, mods); 
			}

			if(IdiomaActual=="English")
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+I"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Shift+Alt+I"); 
#endif
			}
			else
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+I"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Alt+May˙s+D");//
#endif
			}


			
			//ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			//CAlert::InformationAlert(ctrshiftS);

			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PSQLCheckInNotaActionID, kFalse); 
			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PSQLCheckInNotaActionID, kDefaultContext, key_S, mods); 
			}

			if(IdiomaActual=="English")
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+D"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Shift+Alt+D"); 
#endif
			}
			else
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+D"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("");//
#endif
			}

			
			//ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			//CAlert::InformationAlert(ctrshiftS);

			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PSQLDetachNotaActionID, kFalse); 
			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PSQLDetachNotaActionID, kDefaultContext, key_S, mods); 
			}

			if(IdiomaActual=="English")
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+C"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Shift+Alt+C"); 
#endif
			}
			else
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+C"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Alt+May˙s+C");//
#endif
			}

			
			//ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			//CAlert::InformationAlert(ctrshiftS);

			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PSQLCrearSpaceAndNotaActionID, kFalse); 
			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PSQLCrearSpaceAndNotaActionID, kDefaultContext, key_S, mods); 
			}
			
//#ifdef MACINTOSH
//			ctrshiftS =PMString("Opt+Shift+M"); 
//#endif
//#ifdef WINDOWS
//			ctrshiftS =PMString("Shift+Alt+M"); 
//#endif
			
			//ctrshiftS.Translate();
//			ctrshiftS.SetTranslatable(kFalse);

//			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
//			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PsqlLogInActionID, kFalse); 
//			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
//			if(!assigned && short_S.IsEmpty()) 
//			{
//				shortcutMgr->AddShortcut(kN2PsqlLogInActionID, kDefaultContext, key_S, mods); 
//			}
			
//#ifdef MACINTOSH
//			ctrshiftS =PMString("Opt+Shift+N"); 
//#endif
//#ifdef WINDOWS
//			ctrshiftS =PMString("Shift+Alt+N"); 
//#endif
			//ctrshiftS.Translate();
//			ctrshiftS.SetTranslatable(kFalse);

//			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
//			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PsqlLogOutActionID, kFalse); 
//			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
//			if(!assigned && short_S.IsEmpty()) 
//			{
//				shortcutMgr->AddShortcut(kN2PsqlLogOutActionID, kDefaultContext, key_S, mods); 
//			}
//#ifdef MACINTOSH
//			ctrshiftS =PMString("Opt+Shift+P"); 
//#endif
//#ifdef WINDOWS
//			ctrshiftS =PMString("Shift+Alt+P"); 
//#endif
			//ctrshiftS.Translate();
//			ctrshiftS.SetTranslatable(kFalse);

//			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
//			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PSQLPreferenciasActionID, kFalse); 
//			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
//			if(!assigned && short_S.IsEmpty()) 
//			{
//				shortcutMgr->AddShortcut(kN2PSQLPreferenciasActionID, kDefaultContext, key_S, mods); 
//			}


			if(IdiomaActual=="English")
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+P"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("Shift+Alt+P"); 
#endif
			}
			else
			{
#ifdef MACINTOSH
			ctrshiftS =PMString("Opt+Shift+P"); 
#endif
#ifdef WINDOWS
			ctrshiftS =PMString("");//
#endif
			}


			//ctrshiftS.Translate();
			ctrshiftS.SetTranslatable(kFalse);
			//CAlert::InformationAlert(ctrshiftS);

			Utils<IShortcutUtils>()->ParseShortcutString(ctrshiftS, &key_S, &mods); 
			short_S = Utils<IShortcutUtils>()->GetShortcutString(kN2PsqlDetachPhotoActionID, kFalse); 
			assigned = shortcutMgr->IsEnabledShortcut(context, key_S, mods); 
			if(!assigned && short_S.IsEmpty()) 
			{
				shortcutMgr->AddShortcut(kN2PsqlDetachPhotoActionID, kDefaultContext, key_S, mods); 
			}
			

	*/	
			
	}while(false);
	return error;
}



bool16 N2PsqlUpdateStorysAndDBUtils::OpenDlgCrearNotaHija()
{
	bool16 CheckInDlg=kFalse;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		
		
		
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}
		
		
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PsqlPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kN2PsqlCrearNotaHijaDlgResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);
		
		
		

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		
		
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
		
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		
		dialog->WaitForDialog();
		//Consulta Parametros
		
		PMString IsCancel=dialogController->GetTextControlData(kN2PsqlDlgChekInNotaCancelWidgetID);
		//CAlert::InformationAlert(IsCancel);
		if(IsCancel=="Cancel" || IsCancel=="Cancelar")
		{
			CheckInDlg=kFalse;
			break;
		}
			
		CheckInDlg=kTrue;
	} while (false);			
	return(CheckInDlg);

}


///Metodo para verificar si un Elemento de nota esta siendo editado
bool16 N2PsqlUpdateStorysAndDBUtils::PreguntaSiSeEstaEditandoElementoDNota(PMString UIDModelString,IDocument* document)
{
	bool16 retval=kFalse;
	do
	{
		int32 intthis=UIDModelString.GetAsNumber();
		UID UIDTextModel=intthis;
		
		
		if (document == nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB document is invalid");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB db is invalid");	
			break;
		}
		
		UIDRef TextModelUIDRef(db,UIDTextModel);
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB mytextmodel is invalid");	
			break;
		}
		

		InterfacePtr<IItemLockData> textLoctData(TextModelUIDRef, UseDefaultIID()); 
		if(textLoctData==nil)
		{
			
			ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNotaDsdDB textLoctData is invalid");	
			break;
		}
	
		if(textLoctData->GetInsertLock())
		{
		
			retval=kFalse;
			
		}
		else
		{
			//si No se encuentra bloqueda este elemento de la nota no puede
			//hacer update pues se perderia los datos que se han realizado sobre el antes de realizar el update.
			retval=kTrue;
			
		}
	}while(false);
	return retval;
}




////////////////

/*
		-Actualiza a BD el contenido de los elementos de la nota,
		-Cambia el estado de escritura(Lapiz),
		-Actualiza el Estado del edicion(Adornament status Nota) los elementos de nota, 
		-Libera la nota en la BD para ser modificad por cualquier otro usuario
			int32 IDNota, entero que indica el ID de la nota en la BD
*/
bool16 N2PsqlUpdateStorysAndDBUtils::CheckInNotaDBAndPag_CompletesElements(StructIdFramesElementosXIdNota* VectorNota,PMString Estado,PMString RoutedTo,PMString IDUsuario,PMString PublicacionID,PMString SeccionID, bool16 ProvieneDeCrearPase, IDocument* document, PMString GuiaNota)
{
	bool16 retval=kFalse;
	bool16 isUpdate=kFalse;	//Indica que la nota esta siendfo editada
	do
	{	
	
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		PMString Aplicacion=app->GetApplicationName();
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota No Preferences");
			break;
		}
		
		//Para poner por default el root de caracter de estilos como Default
		N2PSQLUtilities::SetRootStyleADefaultCharStyle();
				
				
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		PMString Busqueda="";
		
		PMString Conte_Titulo="";
		PMString Conte_Balazo="";
		PMString Conte_PieFoto="";
		PMString Conte_Contenido="";
		
		PMString LastCheckinNotaToXMP;//=Params[15];
		PMString Fecha_Edicion="0";
		LastCheckinNotaToXMP=Fecha_Edicion;
		
		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_Nota);
		
		
		PMString Params="";
		
		
		Params="CALL NotaN2PTodosLosElementos(";
		
		
		//TextoExportado=CadenaTitulo;	
		
		Params.Append(VectorNota[0].ID_Elemento_padre);
		//CAlert::InformationAlert("EstableceParametrosToCheckInElementoNota IDUsuario= " + IDUsuario);
		Params.Append(",'");
		Params.Append(IDUsuario);
		Params.Append("','");
		Params.Append(RoutedTo);
		Params.Append("','");
		Params.Append(Estado);
		Params.Append("'");
		
		
		//Es InCopy solo modifica Texto
		PMString CAl="";
		CAl.AppendNumber(VectorNota[0].UIDFrContentNote);
		
		
		Params.Append(RegresaParametrosParaCheckInElementoNota(VectorNota[0].Id_Elem_Cont_Note,
																VectorNota[0].UIDFrContentNote, 
																document,
																Conte_Contenido));
		
		
		Params.Append(RegresaParametrosParaCheckInElementoNota( VectorNota[0].Id_Elem_Tit_Note,
																VectorNota[0].UIDFrTituloNote, 
																document,
																Conte_Titulo));
																
			
		Params.Append(RegresaParametrosParaCheckInElementoNota( VectorNota[0].Id_Elem_PieFoto_Note,
																VectorNota[0].UIDFrPieFotoNote, 
																document,
																Conte_PieFoto));
		
		Params.Append(RegresaParametrosParaCheckInElementoNota( VectorNota[0].Id_Elem_Balazo_Note,
																VectorNota[0].UIDFrBalazoNote, 
																document,
																Conte_Balazo));
		
		Params.Append(",");
								
		Params.Append("@X");
		Params.Append(",");
		
		if(isUpdate==kFalse)
		{
			if(Aplicacion.Contains("InDesign"))
				Params.Append("3");
			else
				Params.Append("16");
		}
		else
		{
			if(Aplicacion.Contains("InDesign"))
				Params.Append("9");
			else
				Params.Append("17");
		}
		Params.Append(",'");
						
		Params.Append(PublicacionID); //Publicacion
		Params.Append("','");
		Params.Append(SeccionID);	//Seccion
		Params.Append("','");
		//CAlert::InformationAlert(LastCheckinNotaToXMP);
		Params.Append(MySQLFecha);	//Fecha de Edicion*/
		Params.Append("','");
		Params.Append(GuiaNota);
		Params.Append("')");
		
		
		K2Vector<PMString> VectorResults;
		if(!this->CheckInElementoNota( Params, kFalse,VectorResults))
		{
			
			break;
		}
		
		//////////////////////
		PMString FirstParte="";
		int32 inicio=29999;
		int32 length=29999;
		PMString Buscar="";
		K2Vector<PMString> QueryContenido;
		int32 vecespasadas=0;
		PMString Contenido="";
		int32 CantidadDeCracteres=Conte_Contenido.NumUTF16TextChars();
		
		
		
		if(CantidadDeCracteres>30000)
		{
			do
			{

				FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
				
				Buscar = "CALL Append_Contenido_De_Id_ElementoSUBString(" + VectorNota[0].Id_Elem_Cont_Note + ",'";
				Buscar.Append(FirstParte);
				Buscar.Append("')");
					
				
				
				QueryContenido.clear();
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
				}
					
				inicio = inicio + length;
				
				
				 
				if((inicio+length)>CantidadDeCracteres)
				{
					length=(CantidadDeCracteres-inicio)-1;
					vecespasadas++;
				}
				
				
			}while(vecespasadas<2 && inicio< CantidadDeCracteres);
		}
		
		/////////////////////////
		
		LastCheckinNotaToXMP=VectorResults[1];
		
		if(Fecha_Edicion!=MySQLFecha)
			LastCheckinNotaToXMP=Fecha_Edicion;
		
		//CAlert::InformationAlert("ZZZZZZZ"+Estado);
		//this->Insert_IN_Bitacora_Elemento(LastCheckinNotaToXMP,VectorNota[0].ID_Elemento_padre,IDUsuario,Estado,kFalse);
		
		Fecha_Edicion="0";
				
		if(LastCheckinNotaToXMP.NumUTF16TextChars()<=0)
		{
			break;
		}		 
				
		N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(VectorNota[0].ID_Elemento_padre,LastCheckinNotaToXMP, document);
		
		PMString Label="";
		if(RoutedTo.NumUTF16TextChars()>0)
			Label= RoutedTo+ "/" +Estado;
		else
			Label= Estado;
		
		if(!CambiaEstadoYLapizDeNota(VectorNota,kTrue,Estado,kTrue))
		{
			break;
		}			
		
		
		if(VectorNota[0].EsArticulo)
			this->BuscaRutaDeElementoYSubeRespaldoNota(VectorNota[0].ID_Elemento_padre, Conte_Titulo, Conte_Balazo, Conte_PieFoto, Conte_Contenido,"","","","");
		
		
		
		retval=kTrue;
	}while(false);
	
	return(retval);
}


PMString N2PsqlUpdateStorysAndDBUtils::RegresaParametrosParaCheckInElementoNota(
		PMString IDElemento,int32 UIDElementoNota, 
		IDocument* document,
		PMString& TextoExportado)
{
	PMString retval="";
	PMString Params;
	
	PMString CadenaTitulo="";
	PMString CadenaBalazo="";
	PMString CadenaPieFoto="";
	PMString CadenaContentNote= "";
	PMString sss="";
							
	PMRect CoorTitulo;
	int32	LineFaltTitulo=0;
	int32 	LineRestTitulo=0;
	int32	CarRestTitulo=0;
	int32	CarFaltTitulo=0;
	int32	CarTotTitulo=0;
	bool16 VieneDeCrearPase=0;
	do
	{
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::CambiaEstadoLapizUnLock db");
			break;
		}
			
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		PMString CAl="";
		CAl.AppendNumber(UIDElementoNota);
		if(UIDElementoNota<=0)
			return ",0,'',0,0,0,0,0,0,0,0,0";
		
		CadenaTitulo.AppendNumber(UIDElementoNota);
		
		
		UID uidRef=UIDElementoNota;
		UIDRef textModelRef(db, uidRef);
		
		
		
		
		//Para TextoTageado
		CadenaTitulo =N2PSQLUtilities::ExportTaggedTextOfPMString(textModelRef);

		//Sin Texto tageado
		//PMString CadenaTitulo= N2PSQLUtilities::GetPMStringOfTextFrame(UIDElementoNota);
		
		N2PSQLUtilities::GETStoryParams_OfUIDTextModel(UID(UIDElementoNota),CoorTitulo, LineFaltTitulo,
				LineRestTitulo,
				CarFaltTitulo,
				CarRestTitulo,
				CarTotTitulo); 
				
		bool16 NotaEsCheckOut=kFalse; //
		
		if(LineFaltTitulo<0)
		{
			LineFaltTitulo=0;
		}
		
		if(LineRestTitulo<0)
		{
			LineRestTitulo=0;
		}
		
		if(CarTotTitulo<0)
		{
			CarTotTitulo=0;
		}
		
			if(this->CanDoCheckInNota(UIDElementoNota)==kTrue)	//Si se puede dar checkIn
			{	
				NotaEsCheckOut=kTrue;
			
				if(VieneDeCrearPase==kTrue)
				{
					NotaEsCheckOut=kFalse;
				}
			}
			else
			{
				//CAlert::InformationAlert("Salio Cuando Intentaba dar check in o update de texto");
				NotaEsCheckOut=kFalse;
			
			}		
		
		PMString FirstParte="";
		if(CadenaTitulo.NumUTF16TextChars()>30000)
		{
			FirstParte=CadenaTitulo.Substring(0,29999)->GrabCString();
		}
		else
		{
			FirstParte=CadenaTitulo;
		}
		
		
		Params.Append(",");
		Params.Append(IDElemento);
		Params.Append(",'");					
		Params.Append(FirstParte);
		Params.Append("',");		
		sss="";
		sss.AppendNumber(CoorTitulo.Top() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Left());
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Bottom() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(CoorTitulo.Right() );
		Params.Append(sss);
		Params.Append(",");
		sss="";
		sss.AppendNumber(LineFaltTitulo);
		
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(LineRestTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarFaltTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarRestTitulo);
		
		Params.Append(sss);
		Params.Append(",");
		sss="";
		
		sss.AppendNumber(CarTotTitulo);
		
		Params.Append(sss);
		
			
		
		
		//CAlert::InformationAlert("FERNANDO LLANAS RDZ");
		
		/*FirstParte="";
		int32 inicio=29999;
		int32 length=29999;
		PMString Buscar="";
		K2Vector<PMString> QueryContenido;
		int32 vecespasadas=0;
		PMString Contenido="";
		int32 CantidadDeCracteres=CadenaTitulo.NumUTF16TextChars();
		
		
		
		if(CantidadDeCracteres>30000)
		{
			do
			{

				FirstParte=CadenaTitulo.Substring(inicio,length)->GrabCString();
				
				Buscar = "CALL Append_Contenido_De_Id_ElementoSUBString(" + IDElemento + ",'";
				Buscar.Append(FirstParte);
				Buscar.Append("')");
					
				
				
				QueryContenido.clear();
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
				}
					
				inicio = inicio + length;
				
				
				 
				if((inicio+length)>CantidadDeCracteres)
				{
					length=(CantidadDeCracteres-inicio)-1;
					vecespasadas++;
				}
				
				
			}while(vecespasadas<2 && inicio< CantidadDeCracteres);
		}*/
		
		TextoExportado=CadenaTitulo;

		retval=Params;
	}while(false);
	return(retval);
						
}




bool16 N2PsqlUpdateStorysAndDBUtils::ValidacionSiEsNotaHija_NoDebeColocarOCrearNotaCuando_El_Padre_EstaEn_EnChecOut(PMString Id_Elemento_PadreDElemNotaAColocar,bool16 buscarSuPadre)
{
	bool16 retval=kTrue;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		PMString Id_Elemento_PadreNota ="";
		if(buscarSuPadre)
		{
			PMString Consulta="";
			PMString StringConection="";
		
			K2Vector<PMString> QueryVector;
			PreferencesConnection PrefConectionsOfServerRemote;
			if( !this->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
			{
				break;
			}
					
					
			//
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
			StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
		
			Consulta="SELECT id_Elemento_Padre FROM Elemento WHERE Id_Elemento="+Id_Elemento_PadreDElemNotaAColocar;
			if(!SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector))
			{
			
				break;
			
			}
		
		
			if(QueryVector.Length()>0)
			{
				Id_Elemento_PadreNota = SQLInterface->ReturnItemContentbyNameColumn("id_Elemento_Padre",QueryVector[0]);
			}
			else
			{
				break;
			}
		
			if(Id_Elemento_PadreNota=="0")
			{
				break;		
			}
		}
		else
		{
			Id_Elemento_PadreNota=Id_Elemento_PadreDElemNotaAColocar;
		}
		
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[1];
		if(this->GetElementosNotaDXMP_Of_IDElementoPadre(Id_Elemento_PadreNota,	VectorNota, document))
		{
			PMString UIDModelStringtitulo="";
			UIDModelStringtitulo.AppendNumber(VectorNota[0].UIDFrTituloNote);
			
			PMString UIDModelStringPieFoto="";
			UIDModelStringPieFoto.AppendNumber(VectorNota[0].UIDFrPieFotoNote);
			
			PMString UIDModelStringBalazo="";
			UIDModelStringBalazo.AppendNumber(VectorNota[0].UIDFrBalazoNote);
			
			PMString UIDModelStringContent="";
			UIDModelStringContent.AppendNumber(VectorNota[0].UIDFrContentNote);
			
			if(this->PreguntaSiSeEstaEditandoElementoDNota(UIDModelStringtitulo, document) 		|| 
				this->PreguntaSiSeEstaEditandoElementoDNota(UIDModelStringPieFoto, document) ||
				this->PreguntaSiSeEstaEditandoElementoDNota(UIDModelStringBalazo, document) 	||
				this->PreguntaSiSeEstaEditandoElementoDNota(UIDModelStringContent, document))
			{
				retval=kFalse;
			}
			else
			{
				break;
			}
					
		}
		else
		{
			break;
		}
					
	}while(false);
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::DesligarPagina(IDocument* document)
{
	bool16 retvak=kFalse;
	do
	{
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame db");
			break;
		}
		
		
		const int ActionAgreedValue=1;
		int32 result=-1;
		result=CAlert::ModalAlert(kN2PDeseasDesligarPaginaStringKey,
						kYesString,                           // OK button
						kNoString,                       // Cancel button
						kNullString,
						ActionAgreedValue,
						CAlert::eQuestionIcon);
					if(result!=1)
					{	
							break;
					}
		
		this->DesligarNotasYFotosDPagina(document);
		
		
		//Borrar XMP de News2Page
		N2PSQLUtilities::RemoveXMPVar("N2PDate", document);
		N2PSQLUtilities::RemoveXMPVar("N2PPublicacion", document);
		N2PSQLUtilities::RemoveXMPVar("N2PAplicacion", document);
		N2PSQLUtilities::RemoveXMPVar("N2PEstatus", document);
		N2PSQLUtilities::RemoveXMPVar("N2PDirigidoA", document);
		N2PSQLUtilities::RemoveXMPVar("N2PSeccion", document);
		N2PSQLUtilities::RemoveXMPVar("N2PPagina", document);
		N2PSQLUtilities::RemoveXMPVar("N2PIssue", document);
		N2PSQLUtilities::RemoveXMPVar("N2PFechaCreacionPath", document);
		N2PSQLUtilities::RemoveXMPVar("N2PUsuarioCreador", document);
		N2PSQLUtilities::RemoveXMPVar("N2PUsuarioCredor", document);
		N2PSQLUtilities::RemoveXMPVar("N2PNPubliCreacionPath", document);
		N2PSQLUtilities::RemoveXMPVar("N2PNSecCreacionPath", document);
		N2PSQLUtilities::RemoveXMPVar("N2PSQLFolioPagina", document);
		N2PSQLUtilities::RemoveXMPVar("N2PSQLIDPagina", document);
		N2PSQLUtilities::RemoveXMPVar("UIDTextModelTitulo", document);
		N2PSQLUtilities::RemoveXMPVar("UIDTextModelConteNota", document);
		N2PSQLUtilities::RemoveXMPVar("N2P_ListaUpdate", document);
		
		CAlert::InformationAlert(kN2PPaginaDesligadaStringKey);
		retvak=kTrue;
	}while(false);
	return(retvak);
}

void N2PsqlUpdateStorysAndDBUtils::DesligarNotasYFotosDPagina(IDocument* document)
{
	do
	{
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame db");
			break;
		}
		
		InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
		(
			kN2PAdIssueUtilsBoss,	// Object boss/class
			IN2PAdIssueUtils::kDefaultIID
		)));
		
		if(N2PAdIssueUtil==nil)
		{
			break;
		}
		
		N2PSQLUtilities::SetRootStyleADefaultCharStyle();
		
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
		
			break;
		}
		
		
		
		
		
		////Desligar Fotos
		
		
		
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					
					UIDRef pageItemUIDRef(db,UIDFrameFoto);
					
					InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
					ASSERT(itemHierarchy);
					if(!itemHierarchy) 
					{
						
					}
					
					IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
					UIDList children(dbPageItem);

					

				/*	itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
					for (int32 c = 0; c < children.Length(); c++)
					{
						UIDRef datachildRef1(GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(0));
						InterfacePtr<IScrapItem> frameScrap(datachildRef1, IID_ISCRAPITEM);
						if(frameScrap!=nil)
						{
								// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
								InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
								// Process the DeleteFrameCmd:
								if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
								{		
									break;
								}
								
							}
					}
				*/	
					PMString SAS="";
					SAS.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
					//CAlert::InformationAlert(SAS);
					N2PSQLUtilities::BuscaYElimina_IDFrameFotoOnXMP("N2P_ListaUpdate", IDElementoFoto, SAS);
					
					//SQLInterface->SQLSetInDataBase(StringConection,"UPDATE Elemento SET Estatus_Colocado=0 WHERE Id_Elemento="+IDElementoFoto);
					//SQLInterface->SQLSetInDataBase(StringConection,"DELETE QUICK FROM Elemento_por_Pagina  WHERE Id_Elemento="+IDElementoFoto);
					
					
					
					/*PMString IdElemPadre=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
					if(IdElemPadre.NumUTF16TextChars()>0)
					{
						PMString Busqueda="CALL QueryFotoFromElemPadre (" + IdElemPadre + ")" ;
			 			N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto(Busqueda);
					}*/
						
					UIDList ListFrame=UIDList(pageItemUIDRef);
					N2PAdIssueUtil->UpdateUIDListLabelfor( ListFrame,"", kFalse);
					
			}
		}
		
		///Desligar Notas
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		
		if(contNotasInVector>0)
		{
				
				
			//Actualiza des de la base de datos los elementos de3 cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
				
				DesligarElementoDEstrucPorNum(VectorNota,numNota);
				
			}
				
			
		}
	
		//delete VectorNota;
		
	}while(false);
	
}


///
 bool16 N2PsqlUpdateStorysAndDBUtils::DesligarElementoDEstrucPorNum(StructIdFramesElementosXIdNota* StructNota_IDFrames,int32 numReg)
 {
 	
	bool16 retval=kFalse;
	int32 numeroPaginaActual=0;
	PMString IDNota = "";
	PMString IDUsuario="";
	PMString Aplicacion="";
	
	int32 UIDTextModelOfTextFrame =0;
	do
	{
	
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
			
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		
				
		
		
		///////////////////////////////////
		//Para saber si se puede fluir la nota
		PreferencesConnection PrefConectionsOfServerRemote;
		if( !this->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
		{
			break;
		}
		PMString idSeccionDElemento="";
		PMString Consulta=""; 
		PMString StringConection="";
		K2Vector<PMString> QueryVector;
					
		StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
						
			
		
		
		PMString Estado="0";
		PMString RoutedTo="0";
		PMString PublicacionID="0";
		PMString SeccionID="0";
					
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument No Preferences");
			break;
		}
		
		
			
				
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
	
		///////////////////////////////////////////
			
			
		
		//Quita los adornaments
		if(!CambiaEstadoYLapizDeNota(StructNota_IDFrames,kFalse,"",kTrue,numReg))
		{
				break;
		}
		
		
		//Cambia en base de datos
		PMString QueryNotaDetach="CALL NOTA_DETACH_D_PAGINA (" + StructNota_IDFrames[numReg].ID_Elemento_padre + " , '" + N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document) + "' , '" + IDUsuario + "')";
		
		
		
		//Elimina de XMP
		N2PSQLUtilities::Busca_y_Elimina_En_XMP_IDlementosNotas(StructNota_IDFrames[numReg].ID_Elemento_padre);
		
		
		
		
		K2Vector<PMString> Params;
	
							
		//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
		if(!SQLInterface->SQLQueryDataBase(StringConection , QueryNotaDetach, Params))
		{
			break;
		}
		
		
		retval=kTrue;
	}while(false);
	
	return(retval);
 }
/*
		Update TextFrame To DB
*/
void N2PsqlUpdateStorysAndDBUtils::ExportarNotasATablaWEB(IDocument* document, const bool16& RefreshListaNotas)
{
	//CAlert::InformationAlert("Make Update");
	do
	{	
		//IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame document");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame db");
			break;
		}
		
		
		
		PMString ItemToSearchString="";
		PMString UIDModelString="";
		PMString IDNotaString="";
		PMString Busqueda="";
		PMString IDUsuario="";	
		PMString Aplicacion="";	
		PMString GuiaNota="";	
		
		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_Nota);
		
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
			(
				kN2PCTUtilitiesBoss,	// Object boss/class
				IN2PCTUtilities::kDefaultIID
			)));
		
		if(N2PCTUtils==nil)
			break;
		
			
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		/////////////
		
		PreferencesConnection PrefConections;
		
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			PMString StringConection="";
		
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		////////////
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		int32 contNotasInVector=100;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[100];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		K2Vector<PMString> IDNotaEnPagina;
		
		if(contNotasInVector>0)
		{
							
			
			
			
			
			//
			N2PCTUtils->OcultaNotasConFrameOverset();	
			
			
			
			//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
				
				bool16 AlmenosUnElemntoValido=kFalse;
				K2Vector<PMString> Params;
						
						
					
				PMString IdNotaDB = VectorNota[numNota].ID_Elemento_padre;
				
				
					
				PMString Conte_Titulo="";
				PMString Conte_balazo="";//Contenido del Balazo
				PMString Conte_PieFoto="";//Contenido del pir de foto
				PMString Conte_Contenido="";//Contenido de la nota
						
					
					
							
							
				if(VectorNota[numNota].UIDFrTituloNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrTituloNote;
					UIDRef textModelRef(db, uidRef);
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_Titulo=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrTituloNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						AlmenosUnElemntoValido=kTrue;
					}
								
				}
							
				if(VectorNota[numNota].UIDFrPieFotoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrPieFotoNote;
					UIDRef textModelRef(db, uidRef);
		
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_PieFoto= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrPieFotoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						if(!AlmenosUnElemntoValido)
							AlmenosUnElemntoValido=kTrue;
					}
								
				}
							
				if(VectorNota[numNota].UIDFrBalazoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrBalazoNote;
					UIDRef textModelRef(db, uidRef);
		
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_balazo= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrBalazoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						if(!AlmenosUnElemntoValido)
							AlmenosUnElemntoValido=kTrue;
					}
								
				}	
							
				if(VectorNota[numNota].UIDFrContentNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrContentNote;
					UIDRef textModelRef(db, uidRef);
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_Contenido=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrContentNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						if(!AlmenosUnElemntoValido)
							AlmenosUnElemntoValido=kTrue;
					}
								
				}	
				
				
				
				if(AlmenosUnElemntoValido)
				{
						
					if(PrefConections.EsEnviarNotasHijasAWEB==0)
					{
						PMString ConsultaFamily="SELECT Id_Elemento_padre FROM Elemento  WHERE (Id_Elemento="+VectorNota[numNota].ID_Elemento_padre+" ) AND Id_tipo_ele =10";
			
						K2Vector<PMString> QueryVector;
						//CAlert::InformationAlert(ConsultaFamily);
						if(SQLInterface->SQLQueryDataBase(StringConection,ConsultaFamily,QueryVector))
						{
							if(QueryVector.Length()>0)
							{
								PMString Resultado = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento_padre",QueryVector[0]);
								if(Resultado!="0")
								{
									continue;//Se brinca por que es una nota hija
								}
							}
						}
					}
					
					
					//Convierte comas en comillas de WEB
					if(PrefConections.EnviaAWEBSinRetornosTitulo>0)
					{
						
						PMString replace=" ";
						N2PSQLUtilities::ReplaceRetornBy(Conte_Titulo,replace);
						
					}
					
					if(PrefConections.EnviaAWEBEnAltasPieFoto>0)
					{
						Conte_PieFoto.ToUpper();
						PMString target="á";
						PMString remplace= "Á";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="é";
						remplace= "É";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="í";
						remplace= "Í";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="ó";
						remplace= "Ó";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="ú";
						remplace= "Ú";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="ñ";
						remplace= "Ñ";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
					}

					//SDKUtilities::Replace(Conte_Titulo, "'", "&quot;");
					//SDKUtilities::Replace(Conte_Titulo, "/n", "<br>;");
					//SDKUtils::Replace(Conte_Titulo, "/n", "<br>;");
					//SDKUtilities::Replace(Conte_balazo, "'", "&quot;");
					//SDKUtilities::Replace(Conte_balazo , "/n", "<br>;");
				
					//SDKUtilities::Replace(Conte_Contenido, "'", "&quot;");
					//SDKUtilities::Replace(Conte_Contenido , "/n", "<br>;");
				
					//SDKUtilities::Replace(Conte_PieFoto, "'", "&quot;");
					//SDKUtilities::Replace(Conte_PieFoto , "/n", "<br>;");
				
					K2Vector<PMString> QueryVector;
				
					Busqueda="CALL SubeNotasATablaWEB("+VectorNota[numNota].ID_Elemento_padre+",'"+Conte_Titulo+"','"+Conte_balazo+"','"+Conte_Contenido+"','"+Conte_PieFoto+"')";
					SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
					
					PMString Resultado ="";
					for(int32 i=0;i<QueryVector.Length();i++)
					{
				
						Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
					//CAlert::InformationAlert(Resultado);
					}
				
				
					IDNotaEnPagina.push_back(VectorNota[numNota].ID_Elemento_padre);
				}
			}	
		}
		else
		{
			ASSERT_FAIL(kN2PSQLNoExistenNotasSobreDocumentoStringKey);
		}
	
	
	
	
	
		//////////////////////////////////
		//para obtener las notas que deben ser borradas sobre la tabla web
		//delete VectorNota;
		K2Vector<PMString> NotasABorrarDTablaWEB;//Notas que deben ser borradas de la natbla WEB
		
		//Debe borrar de la tabla Notaweb las notas que no se encuentren sobre la pagina y que estan en la tabla ElemntoporPagina
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		K2Vector<PMString> VectorIDNotasEnPagina;
				
		Busqueda="SELECT Id_Elemento FROM elemento_por_pagina WHERE Pagina_ID="+IDPaginaENXMP;
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,VectorIDNotasEnPagina);
					
		PMString IDElementoEnPagina ="";
		for(int32 i=0;i<VectorIDNotasEnPagina.Length();i++)//ciclo de elementos en pagina desde tabla Elemento_Por_Pagina
		{
				
			IDElementoEnPagina = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",VectorIDNotasEnPagina[i]);
			
			bool16 ExisteEnPagina=kFalse;
			for(int32 j=0;j<IDNotaEnPagina.Length() && ExisteEnPagina==kFalse;j++)//Ciclo de Elementos Sobre pagina real
			{
				if(IDNotaEnPagina[j]==IDElementoEnPagina)
					ExisteEnPagina=kTrue;
			}
			
			if(ExisteEnPagina==kFalse)
			{
				NotasABorrarDTablaWEB.push_back(IDElementoEnPagina);
			}
		}
		
		for(int32 i=0 ; i<NotasABorrarDTablaWEB.Length() ; i++)
		{
			SQLInterface->SQLSetInDataBase(StringConection,"CALL  DELETE_NOTAFOT_D_DOTASWEB( "+NotasABorrarDTablaWEB[i]+","+"0"+")");

		}
		
		
	}while(false);
}




bool16 N2PsqlUpdateStorysAndDBUtils::ExportarNomFotosEnPagATablaWEB(IDocument* document)
{
	bool16 retval=kFalse;
	
	do
	{
		
		
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
			
		///////
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota No Preferences");
			break;
		}
		
								
				
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
				
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
			
		K2Vector<PMString> QueryVector;
						
		PMString Busqueda="";
						
					
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		K2Vector<PMString>  IDs_FotosOnPage;
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		PMString DSNWherElem="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				//Si la foto proviene del servidor local
					
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					DSNWherElem= RegistrosEnXMP[NumReg].NameDSN;
					
					PMString RutaElemEnBD="";
							
					
					UIDRef pageItemUIDRef(db,UIDFrameFoto);
					
					
					InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
					ASSERT(itemHierarchy);
					if(!itemHierarchy) 
					{
						IDs_FotosOnPage.push_back(IDElementoFoto);
						continue;
					}
					
					IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
					UIDList children(dbPageItem);

					
					
					itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
					for (int32 c = 0; c < children.Length(); c++)
					{
						
					
						UIDRef datachildRef1(::GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(c));
						
						
						InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
						if(graphicFrameHierarchy1==nil)
						{
							
							continue;
						} 
						
						
						IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);
						
						InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID());
						ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
						ILinkManager::QueryResult linkQueryResult;
				
						PMString FullNameOfLinkRef="";
					
						if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult))
						{//Existen ligas para este frame
							ASSERT_MSG(linkQueryResult.size()==1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
							ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
							InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
							if (iLink!=nil)
							{//Se encontro Link
								InterfacePtr<ILinkResource> iLinkResource(iLinkManager->QueryResourceByUID(iLink->GetResource()));
								ASSERT_MSG(iLinkResource,"CHMLFiltHelper::addGraphicFrameDescription - Link with no associated asset?");
								if(iLinkResource!=nil)
								{//Esite Resource
									PMString ("UNDEFINED");
									PMString FullNameOfLinkRef = iLinkResource->GetLongName(true);
									PMString FullNameOfLink = iLinkResource->GetShortName(true);
									//CAlert::InformationAlert(datalinkPath);
									if(FullNameOfLinkRef.CharCount()>0) 
									{
										
										
										
										
										
										
										PMString Separator="";
										SDKUtilities::AppendPathSeparator(Separator);
										
										
										
										PMString NameFile=FullNameOfLink;
										int32 LastSeparator=FullNameOfLink.LastIndexOfCharacter(Separator.GetChar(0));
										NameFile.Remove(0 , LastSeparator+1);
										
										
										QueryVector.clear();
										Busqueda="CALL SubeFotosATablaWEB("+IDElementoFoto+",'"+NameFile+"')";
										SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
										
										PMString Resultado ="";
										for(int32 i=0;i<QueryVector.Length();i++)
										{
											
											Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
											//CAlert::InformationAlert(Resultado);
										}
									}
								}
								
							}
						}
					}
				}
			}

		
		
		
		//Remove de Tabla NotasWEB Fotos que no esten sobre la pagina 
		
		for(int32 i=0; i< IDs_FotosOnPage.Length();i++)
		{
			//CAlert::InformationAlert("XX CALL  DELETE_NOTAFOT_D_DOTASWEB( "+IDs_FotosOnPage[i]+","+"1"+")");
			SQLInterface->SQLSetInDataBase(StringConection,"CALL  DELETE_NOTAFOT_D_DOTASWEB( "+IDs_FotosOnPage[i]+","+"1"+")");
			
		}
			
		
		
		
	}while(false);

	
	return retval;
}




bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_UpdateCoordenadasEnBaseD(IDocument* document)
{
	bool16 retval=kFalse;
	
	
	PMString IDUsuario="";
	PMString Aplicacion="";
	
	do
	{
		
		int32 NumLinesFaltantes;
 		int32 NumLinesRestantes;
 		int32 NumCarFaltantes;
 		int32 NumCarRestantes;
 		int32 NumCarOfTextModel;
 												
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		
		//CAlert::InformationAlert("macorina");
		
		
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
		
		
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		if(Aplicacion.Contains("InCopy"))
		{
			break;
		}
		
		
		
			PreferencesConnection PrefConections;
		
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			PMString StringConection="";
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		//CAlert::InformationAlert("ponme ");
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
	
	
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		//CAlert::InformationAlert("la  ");
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				
				//Si la foto proviene del servidor local
				
				//if(PrefConections.DSNNameConnection ==  RegistrosEnXMP[NumReg].NameDSN)
//				{
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					
					UIDRef UIDRefOfElementoNota(db,UIDFrameFoto);
					CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_Reales(UIDRefOfElementoNota);
					//CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_On_Spread(UIDRefOfElementoNota);
					
					StringProcedure="CALL SET_COORDENADAS_ELEMENTO (";
					
					StringProcedure.Append(IDElementoFoto);
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Top());
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Left());
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Bottom());
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Right());
					StringProcedure.Append(", @X )");
					
					
					//arma la cadena para la conexion a la base de datos local
					
					//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
					if(!SQLInterface->SQLQueryDataBase(StringConection , StringProcedure, Params))
					{
						break;
					}
//				}

				
					
			}
			else
			{
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					
					//UIDRef UIDRefOfElementoNota(db,UIDFrameFoto);
					//CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_Reales(UIDRefOfElementoNota);
					//CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_On_Spread(UIDRefOfElementoNota);
					
					//CAlert::InformationAlert("aqui 1 ");
					N2PSQLUtilities::GETStoryParams_OfUIDTextModel( UIDFrameFoto,
 												 CoordFrameFoto,
 												 NumLinesFaltantes,
 												 NumLinesRestantes,
 												 NumCarFaltantes,
 												 NumCarRestantes,
 												 NumCarOfTextModel);
					//CAlert::InformationAlert("aqui 2 ");
					StringProcedure="CALL SET_COORDENADAS_ELEMENTO (";
					
					StringProcedure.Append(IDElementoFoto);
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Top());
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Left());
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Bottom());
					StringProcedure.Append(",");
					StringProcedure.AppendNumber(CoordFrameFoto.Right());
					StringProcedure.Append(", @X )");
					
					
					//arma la cadena para la conexion a la base de datos local
					
					//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
					if(!SQLInterface->SQLQueryDataBase(StringConection , StringProcedure, Params))
					{
						break;
					}
					
					//CAlert::InformationAlert("aqui 3 ");
			}
		}
		
		//CAlert::InformationAlert("la mano aqui ");
	}while(false);

	
	return retval;
}



bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_SeRequiereActualizacionPregunta(IDocument* document)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::Fotos_SeRequiereActualizacionPregunta ini");
	bool16 retval=kFalse;
	
	do
	{
		
		
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
			
	
					
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		PMString DSNWherElem="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				//Si la foto proviene del servidor local
					
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					DSNWherElem= RegistrosEnXMP[NumReg].NameDSN;
					
					PMString RutaElemEnBD="";
					//this->Fotos_TraerPathDeFotoEnBD(IDElementoFoto,DSNWherElem,RutaElemEnBD);				
					
					
					UIDRef pageItemUIDRef(db,UIDFrameFoto);
					
					
					InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
					ASSERT(itemHierarchy);
					if(!itemHierarchy) 
					{
						continue;
					}
					
					IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
					UIDList children(dbPageItem);
					
					itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
					for (int32 c = 0; c < children.Length(); c++)
					{
						
					
						UIDRef datachildRef1(::GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(c));
						
						
						InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
						if(graphicFrameHierarchy1==nil)
						{
							
							continue;
						} 
						
						
						IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);
						
						InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID());
						ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
						ILinkManager::QueryResult linkQueryResult;
				
						PMString FullNameOfLinkRef="";
					
						if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult)>0)
						{//Existen ligas para este frame
							ASSERT_MSG(linkQueryResult.size()==1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
							ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
							InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
							if (iLink!=nil)
							{//Se encontro Link
								InterfacePtr<ILinkResource> iLinkResource(iLinkManager->QueryResourceByUID(iLink->GetResource()));
								ASSERT_MSG(iLinkResource,"CHMLFiltHelper::addGraphicFrameDescription - Link with no associated asset?");
								if(iLinkResource!=nil)
								{//Esite Resource
									PMString ("UNDEFINED");
									PMString datalinkPath = iLinkResource->GetLongName(true);
									//CAlert::InformationAlert(datalinkPath);
									if(datalinkPath.CharCount()>0) 
									{
										if(datalinkPath.Contains("@"))
										{//Es una imagen trabajada
											if (iLink->GetResourceModificationState() == ILink::kResourceModified )
											{
												
												retval=kTrue;
												break;
											}
										}
										else
										{//No es una imagen trabajada
									
											//Aqui deberia preguntarse si se debe de hacer un reelink para ver si se encuentra la misma imagen trabajada "@"
										
										
											//Si no existe la imagen trabaja pues se debe de actualizar la imagen
											if (iLink->GetResourceModificationState() == ILink::kResourceModified )
											{
												
												retval=kTrue;
												break;
											}
											
											/*bool16 resultado=Utils<ILinkUtils>()->IsLinkMissingOrOutOfDate(datachildRef1);
											{//Se perdio el link o esta fuera de actualizacion
												CAlert::InformationAlert("AAAAAA");
												retval=kTrue;
												break;
											}*/
										}
									
									FullNameOfLinkRef=datalinkPath;
								}
							}
						}
					}
    				
				}
					/*{
						
						UIDRef datachildRef1(GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(0));
    					InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
    					if(graphicFrameHierarchy1==nil)
						{
							break;
						} 
						
						
    					UIDRef datachildRef(GetDataBase(graphicFrameHierarchy1), itemHierarchy->GetChildUID(0));
    				    				
    					IDataBase* theDb;
    					UID thePageItem;
    			 
						
						InterfacePtr<IDataLinkHelper> helper1(::CreateObject2<IDataLinkHelper>(kDataLinkHelperBoss));
    					if(helper1==nil)
						{
							break;
						} 
						
   						theDb = datachildRef.GetDataBase();
    					InterfacePtr<IDataLinkReference> theDataLinkRef(datachildRef, UseDefaultIID());
    					if( theDataLinkRef == nil )
    					{
     					   continue;
    					}

						
    					InterfacePtr<IDataLink> theDataLink(theDb,theDataLinkRef->GetUID(), IID_IDATALINK);
    					if( theDataLink == nil )
   						{
   							continue;
    					}
    					
    					//Para la administracion de fotos
    					PMString* FullBaseOfLinkRef=theDataLink->GetBaseName();
    					if(FullBaseOfLinkRef->Contains("@"))
    					{
    						if(theDataLink->GetCurrentState(nil,nil,nil,nil,nil)!=IDataLink::kLinkNormal)// || theDataLink->GetCurrentState(nil,nil,nil,nil,nil)==IDataLink::kLinkMissing)
							{
								
								retval=kTrue;
								break;
							}
    					}

    					
    					PMString* FullNameOfLinkRef=theDataLink->GetFullName();
						PMString FullNameOfLink=FullNameOfLinkRef->GrabCString();
						
						

						//Relink Foto desde BD
						if(RutaElemEnBD!=FullNameOfLink)
						{
							//retval=kTrue;
							//break;
						}
						PMString Separator="";
#ifdef WINDOWS
						Separator="\\";
#endif
#ifdef MACINTOSH
						SDKUtilities::AppendPathSeparator(Separator);
#endif
						

						int32 LastSeparator=FullNameOfLink.LastIndexOfCharacter(Separator.GetChar(0));
						PMString numebre="";
						numebre.AppendNumber(LastSeparator);
						
						
						
						if(LastSeparator>0)
						{
							PMString FullNameOfLinkListo=FullNameOfLink;
							FullNameOfLinkListo.Insert("@",1,LastSeparator+1);
							
							
							IDFile FileList;
							FileUtils::PMStringToIDFile(FullNameOfLinkListo,FileList);
							
							SDKFileHelper fileHelper(FileList);
							if(fileHelper.IsExisting())
							{
								//CAlert::InformationAlert("Si se requiere update de fotos 1");
								retval=kTrue;
								break;
							}
						}
						
  						if(theDataLink->GetCurrentState(nil,nil,nil,nil,nil)!=IDataLink::kLinkNormal)// || theDataLink->GetCurrentState(nil,nil,nil,nil,nil)==IDataLink::kLinkMissing)
						{
							//CAlert::InformationAlert("Si se requiere update de fotos");
							retval=kTrue;
							break;
						}
						
						
					} // loop over kids with IDataLinkReference interface	*/
			}
		}
	}while(false);

	//N2PSQLUtilities::ImprimeMensaje("N2PsqlUpdateStorysAndDBUtils::Fotos_SeRequiereActualizacionPregunta fin");
	return retval;
}


/*
*/
bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_ActualizaLinks(IDocument* document)
{
	bool16 retval=kFalse;
	//CAlert::InformationAlert("##");
	PMString IDUsuario="";
	PMString Aplicacion="";
	do
	{
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			
			break;
		}
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		if(Aplicacion.Contains("InCopy"))
		{
			
			break;
		}
		
		//ErrorCode retval = kFailure;
		ErrorCode result = kFailure;
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			
			break;
		}
			
	
					
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		PMString DSNWherElem="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				
				//Si la foto proviene del servidor local
				
				IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
				IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
				UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
				DSNWherElem= RegistrosEnXMP[NumReg].NameDSN;
					
				PMString RutaElemEnBD="";
				//this->Fotos_TraerPathDeFotoEnBD(IDElementoFoto,DSNWherElem,RutaElemEnBD);	
					
				UIDRef pageItemUIDRef(db,UIDFrameFoto);
					
				InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
				ASSERT(itemHierarchy);
				if(!itemHierarchy) 
				{
					
					continue;
				}
					

				IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
				UIDList children(dbPageItem);


				itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
				
				UIDList uidsToUpdate(dbPageItem);
				InterlasaUtilities::GetListaDLinksParaActualizar( itemHierarchy, uidsToUpdate);
				if ( !uidsToUpdate.IsEmpty() )
				{
					Utils<Facade::ILinkFacade>()->UpdateLinks(uidsToUpdate, true, kMinimalUI, false);
				}
				/*for (int32 c = 0; c < children.Length(); c++)
				{
						
					
					UIDRef datachildRef1(::GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(c));
						
						
    				InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
    				if(graphicFrameHierarchy1==nil)
					{
							
						continue;
					} 
						
						
					IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);
						
					InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID());
					ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
					ILinkManager::QueryResult linkQueryResult;
				
					PMString FullNameOfLinkRef="";
					
					if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult))
					{//Existen ligas para este frame
						ASSERT_MSG(linkQueryResult.size()==1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
						ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
						InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
						if (iLink!=nil)
						{//Se encontro Link
							InterfacePtr<ILinkResource> iLinkResource(iLinkManager->QueryResourceByUID(iLink->GetResource()));
							ASSERT_MSG(iLinkResource,"CHMLFiltHelper::addGraphicFrameDescription - Link with no associated asset?");
							if(iLinkResource!=nil)
							{//Esite Resource
								PMString ("UNDEFINED");
								PMString datalinkPath = iLinkResource->GetLongName();
								//CAlert::InformationAlert(datalinkPath);
								if(datalinkPath.CharCount()>0) 
								{
									if(datalinkPath.Contains("@"))
									{//Es una imagen trabajada
										if (iLink->GetResourceModificationState() == ILink::kResourceModified )
										{//Se perdio el link o esta fuera de actualizacion
											UID newsLink;
											
										
											iLink->Update(kTrue, kMinimalUI, newsLink);//Actualiza link
											continue;
										}
									}
									else
									{//No es una imagen trabajada
										
										if (iLink->GetResourceModificationState() != ILink::kResourceModified )
										{
											continue;
										}
										//GetFileFromLink (ILink *link, IDFile *file) 
										IDFile file;
										if(Utils<ILinkUtils>()->GetFileFromLink(iLink,&file))
										{
											for(int32 i=0;i<itemHierarchy->GetChildCount();i++)
											{
												UID Child=itemHierarchy->GetChildUID(i);
												UIDRef UIDRefChild=UIDRef(db,Child);
												
												InterfacePtr<IScrapItem> frameScrap(UIDRefChild, IID_ISCRAPITEM);
												if(frameScrap!=nil)
												{
													
													// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
													InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
													// Process the DeleteFrameCmd:
													if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
													{		
														break;
													}
												}
											}
											
											//Coloca de nuevo el link
											
											UIDRef docUIDRef = ::GetUIDRef(document);
											
											UIDRef UIDRefImagenAImportar = N2PSQLUtilities::PreparaImportImageAndLoadPlaceGun(docUIDRef, datalinkPath);
											if(UIDRefImagenAImportar!=UIDRef::gNull)
											{
											N2PSQLUtilities::CreateAndProcessPlaceItemInGraphicFrameCmd(docUIDRef,UIDRefImagenAImportar, pageItemUIDRef);
											}
											
											continue;
										}
									}
									
									FullNameOfLinkRef=datalinkPath;
								}
							}
						}
					}
    				

				} // loop over kids with IDataLinkReference interface	*/
			}
		}
		
	}while(false);

	//CAlert::InformationAlert("##11");
	return retval;
}




///
 bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_DesligarDePagina()
 {
 	
	bool16 retval=kFalse;
	int32 numeroPaginaActual=0;
	PMString IDNota = "";
	PMString IDUsuario="";
	PMString Aplicacion="";
	
	int32 UIDTextModelOfTextFrame =0;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
		
		InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
		(
			kN2PAdIssueUtilsBoss,	// Object boss/class
			IN2PAdIssueUtils::kDefaultIID
		)));
		
		if(N2PAdIssueUtil==nil)
		{
			break;
		}
			
		PreferencesConnection PrefConectionsOfServerRemote;
		
		if( !this->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
		{
			break;
		}
		
		PMString StringConection="DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection;
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
						
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			
			break;
		}
			
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		
			
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
			
		UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfGraphicFrameSelect();
		
		
		if(UIDTextModelOfTextFrame<1)
		{
			CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
			break;
		}
		
		
		
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				if(RegistrosEnXMP[NumReg].IDFrame==UIDTextModelOfTextFrame)
				{
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					
					UIDRef pageItemUIDRef(db,UIDFrameFoto);
					
					InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
					ASSERT(itemHierarchy);
					if(!itemHierarchy) 
					{
						
					}
					
					IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
					UIDList children(dbPageItem);

					

					itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
					for (int32 c = 0; c < children.Length(); c++)
					{
						UIDRef datachildRef1(GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(0));
						InterfacePtr<IScrapItem> frameScrap(datachildRef1, IID_ISCRAPITEM);
						if(frameScrap!=nil)
						{
								// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
								InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
								// Process the DeleteFrameCmd:
								if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
								{		
									break;
								}
								
							}
					}
					
					PMString SAS="";
					SAS.AppendNumber(UIDTextModelOfTextFrame);
					//CAlert::InformationAlert(SAS);
					N2PSQLUtilities::BuscaYElimina_IDFrameFotoOnXMP("N2P_ListaUpdate", IDElementoFoto, SAS);
					
					SQLInterface->SQLSetInDataBase(StringConection,"UPDATE Elemento SET Estatus_Colocado=0 WHERE Id_Elemento="+IDElementoFoto);
					SQLInterface->SQLSetInDataBase(StringConection,"DELETE QUICK FROM Elemento_por_Pagina  WHERE Id_Elemento="+IDElementoFoto);
					
					//Se borra de notas WEB
					SQLInterface->SQLSetInDataBase(StringConection,"CALL  DELETE_NOTAFOT_D_DOTASWEB( "+IDElementoFoto+","+"1"+")");
					
					PMString IdElemPadre=N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlText_ID_Elem_PadreNotaWidgetID);
					if(IdElemPadre.NumUTF16TextChars()>0)
					{
						PMString Busqueda="CALL N2PV1055_QueryFotoFromElemPadre (" + IdElemPadre + ")" ;
			 			N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto(Busqueda);
					}
						
					UIDList ListFrame=UIDList(pageItemUIDRef);
					N2PAdIssueUtil->UpdateUIDListLabelfor( ListFrame,"", kFalse);
				}	
			}
		}
		
	

		
		
		retval=kTrue;
	}while(false);
	
	return(retval);
 }
 
 
 /*
 	Funcion que Hace un Relink de la Imagen de la foto 
 */
 
 bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_ReestablecerLinkDFoto(IDFile FileList, IDataLink* dataLink,UIDRef linkRef, IDataBase* database)
 {
 	bool16 retval=kFalse;
 	do
 	{
 		/*InterfacePtr<ILinkObjectReference> dataLinkReference(dataLink, UseDefaultIID());
		if (dataLinkReference == nil)
		      break;
		
		UID pageItemUID = dataLinkReference->GetUID();
		InterfacePtr<IHierarchy> hierarchy(database, pageItemUID, UseDefaultIID());
		if (hierarchy == nil)
	      break;
		
   		pageItemUID = hierarchy->GetParentUID(); 
   
 		InterfacePtr<IDataLinkHelper> helper(CreateObject2<IDataLinkHelper>(kDataLinkHelperBoss));
   		if (helper == nil)
	      break;
   		
   		InterfacePtr<IDataLink> dataLinkNew(helper->CreateDataLink(FileList));
		if (dataLinkNew == nil)
	      break; 
		
		NameInfo name;
   		PMString type;
   		uint32 filetype;
   		dataLinkNew->GetNameInfo(&name, &type, &filetype); 
   		
   		InterfacePtr<ICommand> relinkCmd(CmdUtils::CreateCommand(kRestoreLinkCmdBoss));
  		if (!relinkCmd)
		   break;
  		
		InterfacePtr<IRestoreLinkCmdData> relinkCmdData(relinkCmd, IID_IRESTORELINKCMDDATA);
		if (!relinkCmdData)
	      break;
		
		relinkCmdData->Set(database, linkRef.GetUID(), &name, &type, filetype, IDataLink::kLinkNormal);
		if (CmdUtils::ProcessCommand(relinkCmd) != kSuccess)
	      break;
		
		uint64 iSizeCurrent, iTimeCurrent;
   		uint32 iFileTypeCurrent;
   		dataLink->GetCurrentState(0, &iSizeCurrent, &iTimeCurrent, &iFileTypeCurrent, 0);
   		if (dataLink->SetStoredState(&iSizeCurrent, &iTimeCurrent, IDataLink::kLinkOutOfDate) != 0)
      		break;
   		
   		InterfacePtr<ICommand> reimportCmd(CmdUtils::CreateCommand(kReimportCmdBoss));
   		if (!reimportCmd)
      		break;
   		
   		InterfacePtr<IImportFileCmdData> reimportCmdData(reimportCmd, IID_IIMPORTFILECMDDATA);
   		if (!reimportCmdData)
      		break;
   		
   		reimportCmdData->Set(database, dataLinkNew, kSuppressUI, pageItemUID);
   		if (CmdUtils::ProcessCommand(reimportCmd) != kSuccess)
      		break; 
		*/
		CAlert::InformationAlert("Verificar este metodo N2PsqlUpdateStorysAndDBUtils::Fotos_ReestablecerLinkDFoto");
		retval=kTrue;
 	}while(false);
 	return retval;
 }
 
 /*
 	Esta funcion hace una consulta a la base de datos por el Campo Ruta_Elemento para verid¡ficar si ha cambiado la imagen o el path de la imagen.
 */
bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_TraerPathDeFotoEnBD(PMString IDElementoFoto,PMString DSNNombre ,PMString& PathReturn )
{
	bool16 retval=kFalse;
	do
	{
		if(IDElementoFoto.NumUTF16TextChars()>0)
		{
			break;
		}
		
		PMString Path="";
		PreferencesConnection PrefConections;
			
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::SeRequiereActualizacionDeNotasDeDocument No Preferences");
			break;
		}
		
							
				
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
		
		K2Vector<PMString> IdFamilyArticle;	
		PMString ConsultaFamily="SELECT Ruta_Elemento FROM Elemento  WHERE Id_Elemento="+IDElementoFoto;
			
		if(!SQLInterface->SQLQueryDataBase(StringConection,ConsultaFamily,IdFamilyArticle))
		{
			break;
		}
			
		if(IdFamilyArticle.Length()>1)
		{
			Path = SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",IdFamilyArticle[0]);
			
		}
		
		if(Path.NumUTF16TextChars()>0)
		{
			SDKUtilities::RemoveFirstElement(Path);
#ifdef MACINTOSH
			SDKUtilities::convertToMacPath(Path);
#endif
			PMString CopyaRuta=PrefConections.PathOfServerFileImages;
			SDKUtilities::AppendPathSeparator(CopyaRuta);
			if(CopyaRuta.NumUTF16TextChars()<=1)
			{
				Path=Path;
			}
			else
			{
				Path = CopyaRuta +""+ Path;
			}
		}
		PathReturn	=Path;	
	}while(false);
 	return retval;
}



void N2PsqlUpdateStorysAndDBUtils::Fotos_DoAceptarLigasDImagenANota()
{
	ErrorCode stat = kFailure;
	IDFile file;
	
	IAbortableCmdSeq* sequence = CmdUtils::BeginAbortableCmdSeq();
	if (sequence == nil)
	{
		ASSERT_FAIL("Cannot create command sequence?");
	}
	else
	{
		do
  		{
			PreferencesConnection PrefConections;
			PMString StringConection="";
			PMString Busqueda="";
				
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				continue;
			}
			
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
			
			////
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																				(
																				 kN2PSQLUtilsBoss,	// Object boss/class
																				 IN2PSQLUtils::kDefaultIID
																				 )));
			if(SQLInterface==nil)
			{
				continue;
			} 	
  			//Interfaz para cambiar el adornament(adorno) que muestra el tipo Estado del elemento de la nota
			InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
			(
				kN2PAdIssueUtilsBoss,	// Object boss/class
				IN2PAdIssueUtils::kDefaultIID
			)));
		
			if(N2PAdIssueUtil==nil)
			{
				break;
			}
			
					
			
			
			
		  	////////////////////////
			sequence->SetName(PMString("LigarFotoANota",  PMString::kDontTranslateDuringCall));
	
  			PMString ID_Usuario="";
  		
  			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
			
				ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
				break;
			}
		
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			if(app==nil)
			{
				break;
			} 
		
			PMString Aplicacion=app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);

			if( Aplicacion.Contains("InCopy")  )
			{
				break;
			}
			
			//CAlert::InformationAlert("X7");	
	 		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
			{
				ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
				break;
			}
		
  			UIDRef UidRefTextFrame=UIDRef::gNull;	//
  			UID UIDTextModel = kInvalidUID;			//
	  		UIDList ListFrameOfArticles=N2PSQLUtilities::GetUIDListOfGraphicFramesSelected(UidRefTextFrame);
  		
  		
  			if(UidRefTextFrame ==UIDRef::gNull)
  			{
				CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
  				break;
	  		}
  		
  			UIDTextModel = N2PSQLUtilities::GetUIDTextModelFromUIDRefTextFrame(UidRefTextFrame);
  		
  			//Validacion para revisar si existe como nota de news2page
			StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
			//Obtiene el ID_elemento_padre apartir del textframe seleccionado
			bool16 retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModel.Get(),StructNota_IDFrames, document);
			//SI NO ES ELMENTO DE UNA NOTA
			if(retval==kFalse)
			{
				CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
				break;
			}
		
			
  	
  			if(ListFrameOfArticles.Length()<=0)
			{
  				CAlert::InformationAlert(kN2PsqlNoexistenCajasDImagenSelecStringKey);
	  			break;
  			}
  		
  		
		
		
		
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
				break;
			}

  		
	  		UID selectedItem = kInvalidUID;
  		
  			for(int32 NumFrameImage=0;NumFrameImage<ListFrameOfArticles.Length();NumFrameImage++)
  			{
  					
	  			selectedItem = kInvalidUID;
  				selectedItem = ListFrameOfArticles.At(NumFrameImage);//UID de Frame
  						
  				if(selectedItem==kInvalidUID)//si no existe
	  			{
  				
  					continue;
  				}
  					
  				UIDRef pageItemUIDRef(db,selectedItem);
			
				PMString UIDOfFramePlaced="";
					
				UID UIdParentOfItemPlaced = pageItemUIDRef.GetUID();
				
				UIDOfFramePlaced.AppendNumber(UIdParentOfItemPlaced.Get());		
			
				if( N2PSQLUtilities::Existe_IDFrameFotoOnXMP("N2P_ListaUpdate", UIDOfFramePlaced) )//evisa si ya se encuentra linkeado en al pagina
				{
					//CAlert::WarningAlert("Ya es un Frame de una nota");
					continue;
				}	
					
				InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
				ASSERT(itemHierarchy);
				{if(!itemHierarchy) 
				{
				
					continue;
				}
				IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
				UIDList children(dbPageItem);

				itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
				for (int32 c = 0; c < children.Length(); c++)
				{
					UIDRef datachildRef1(GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(c));
    				InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
    				if(graphicFrameHierarchy1==nil)
					{
						 continue;
					} 
					
					
					IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);

					InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID());
					ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
					ILinkManager::QueryResult linkQueryResult;
					
					PMString FullNameOfLinkRef="";
					PMString NombreFile="";
					PMString extension="";
					PMString dststringfile="";
					PMString publicacion="";
					PMString seccion="";
					PMString fecha="";
					
					if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult))
					{
						ASSERT_MSG(linkQueryResult.size()==1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
						ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
						
						UIDRef linkUIDRef = UIDRef(iDataBase, *iter);
						InterfacePtr<IDataLink> dataLnk(iDataBase, *iter,UseDefaultIID());
						if(dataLnk==nil)
						{
							PMString ("no es por aqui dataLnk");
						}
						
						InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
						if (iLink!=nil)
						{
							 InterfacePtr<ILinkResource> iLinkResource(iLinkManager->QueryResourceByUID(iLink->GetResource()));
							 ASSERT_MSG(iLinkResource,"CHMLFiltHelper::addGraphicFrameDescription - Link with no associated asset?");
							 if(iLinkResource!=nil)
							 {
								 PMString ("UNDEFINED");
								 PMString datalinkPath = iLinkResource->GetLongName(true);
								 //CAlert::InformationAlert(datalinkPath);
								 if(datalinkPath.CharCount()>0) 
								 {
									if(datalinkPath.Contains("@"))
									{
										UIDList uidsToUpdate(db);
										InterlasaUtilities::GetListaDLinksParaActualizar( graphicFrameHierarchy1, uidsToUpdate);
									
										if ( !uidsToUpdate.IsEmpty() )
										{
											Utils<Facade::ILinkFacade>()->UpdateLinks(uidsToUpdate, true, kMinimalUI, false);
											continue;
											//ImportImage.AplicarAjusteRecImagen(frameUIDRef);
										}
									}
									
									
									if(Utils<ILinkUtils>()->GetFileFromLink(iLink,&file))
									{
										if(!FileUtils::DoesFileExist(file))
										{
											continue;
										}
										FileUtils::IDFileToPMString(file, datalinkPath);
									}
									 
									// CAlert::InformationAlert("BBB:"+datalinkPath);
									FullNameOfLinkRef = datalinkPath;
								 }
							 }
						}
						
						/**Relink **/
						
						
						
						FileUtils::GetBaseFileName(file, NombreFile);
						FileUtils::	GetExtension(file, extension);
						
#ifdef MACINTOSH
						Busqueda="SELECT (SELECT Nombre_de_Seccion FROM Seccion WHERE id_Seccion=e.Id_Seccion) as seccion, (SELECT Nombre_Publicacion FROM Publicacion WHERE id_Publicacion=e.Id_Publicacion) as publicacion,  DATE_FORMAT(NOW(),'%Y%m%d') as fecha FROM Elemento e WHERE e.Id_Elemento="+StructNota_IDFrames[0].ID_Elemento_padre;
						//Obtener los datos de la ruta estos son publicacion, seccion, 
						K2Vector<PMString> QueryContenido;
						SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryContenido);
						for(int32 j=0;j<QueryContenido.Length();j++)
						{
							publicacion = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryContenido[j]);
							seccion= SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryContenido[j]);
							fecha= SQLInterface->ReturnItemContentbyNameColumn("fecha",QueryContenido[j]);
						}
						
						if(PrefConections.PathOfServerFileImages.NumUTF16TextChars()>0)
						{	
							dststringfile.Append(PrefConections.PathOfServerFileImages);
							SDKUtilities::AppendPathSeparator(dststringfile);
						}
						
						if(PrefConections.ImagCarpInServFImages.NumUTF16TextChars()>0)
						{
							dststringfile.Append(PrefConections.ImagCarpInServFImages);
							SDKUtilities::AppendPathSeparator(dststringfile);
						}
						if(publicacion.NumUTF16TextChars()>0)
						{
							dststringfile.Append(publicacion);
							SDKUtilities::AppendPathSeparator(dststringfile);
						}
						if(seccion.NumUTF16TextChars()>0)
						{
							dststringfile.Append(seccion);
							SDKUtilities::AppendPathSeparator(dststringfile);
						}
						if(fecha.NumUTF16TextChars()>0)
						{
							dststringfile.Append(fecha);
							SDKUtilities::AppendPathSeparator(dststringfile);
						}
						
						//Crea las carpetas si no existen
						IDFile Folder=FileUtils::PMStringToSysFile (dststringfile);
						FileUtils::CreateFolderIfNeeded( Folder,kTrue );
						
						//adiciona el nombre del archivo
						dststringfile.Append( NombreFile+"."+extension );
						IDFile dstFile = FileUtils::PMStringToSysFile (dststringfile);
						
						if(FileUtils::CopyFile(file,dstFile))
						{
							//hay que hacer reelink
							URI tmpURI;
							Utils<IURIUtils>()->IDFileToURI(dstFile, tmpURI);
							
							UID newLinkUID;
							if(Utils<Facade::ILinkFacade>()->RelinkLink(linkUIDRef, tmpURI, kMinimalUI, newLinkUID)!=kSuccess)
							{
								
							}
							else
							{
								//SDKUtilities::RemoveFirstElement(dststringfile);
								PMString target=":";
								PMString remplace= "/";
								N2PSQLUtilities::ReplaceAllOcurrencias(dststringfile,target,remplace);
								//CAlert::InformationAlert("AAA:"+dststringfile);
								FullNameOfLinkRef = dststringfile;
							}
						}
#endif
					}
							
					
					PMString FullNameOfLink = FullNameOfLinkRef;//FullNameOfLinkRef->GrabCString();

					
					PMString NameFile=NombreFile+"."+extension;
					
					//CAlert::InformationAlert( FullNameOfLink+" , "+ NameFile);
				/*	PMString Separator="";
					SDKUtilities::AppendPathSeparator(Separator);



					PMString NameFile=FullNameOfLink;
					int32 LastSeparator=FullNameOfLink.LastIndexOfCharacter(Separator.GetChar(0));
					NameFile.Remove(0 , LastSeparator+1);
				*/
				
					PMString N2PSQL_ID_Pag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
				
					//CALL Foto_CrearRegElementoFotoYLigaANotaDsdID( StructNota_IDFrames[0].ID_Elemento_padre , FullNameOfLink , NameFile, N2PSQL_ID_Pag );
					Busqueda="CALL Foto_CrearRegElementoFotoYLigaANotaDsdID (";
					Busqueda.Append(StructNota_IDFrames[0].ID_Elemento_padre);
					Busqueda.Append(",'");
					Busqueda.Append(FullNameOfLink);
					Busqueda.Append("','");
					Busqueda.Append(NameFile);
					Busqueda.Append("',");
					Busqueda.Append(N2PSQL_ID_Pag);
					Busqueda.Append(",'");
					Busqueda.Append(ID_Usuario);
					Busqueda.Append("','");
					Busqueda.Append(ID_Usuario);
					Busqueda.Append("',@A)");
					
					
		
				
	 				///////////////
					
				
					PMString IdElePhoto="";
					PMString StatusPhoto="";
					PMString GuiaNota="";
					K2Vector<PMString> QueryContenido;
					SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryContenido);
					
					if(QueryContenido.Length()<=0)
						continue;
					
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						IdElePhoto = SQLInterface->ReturnItemContentbyNameColumn("IdElemento_FotoOUT",QueryContenido[j]);
						StatusPhoto= SQLInterface->ReturnItemContentbyNameColumn("IdEstatusFotoOUT",QueryContenido[j]);
						GuiaNota= SQLInterface->ReturnItemContentbyNameColumn("GuiaNotaOUT",QueryContenido[j]);
					}
				
					if(N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",StructNota_IDFrames[0].ID_Elemento_padre, IdElePhoto,
												 UIDOfFramePlaced, "Foto",
												 PrefConections.DSNNameConnection, StatusPhoto, kFalse, "0",kTrue) ==kTrue)
					{
						stat=kSuccess;
					}
					
					if (stat == kSuccess) 
					{
						UIDList ListFrame=UIDList(pageItemUIDRef);
						N2PAdIssueUtil->UpdateUIDListLabelfor( ListFrame,GuiaNota, kTrue);
					}
				}
  				}
	  		}
	  		
  		}while(false);
	}
  	
  	if (stat == kSuccess) 
	{
		// Everything completed so end the command sequence
		CmdUtils::EndCommandSequence(sequence);
		CAlert::InformationAlert(kN2PsqlImegenesLigadasANotasStringKey);
		
	} else
	{
		// Abort the sequence and roll back the changes.
		CmdUtils::AbortCommandSequence(sequence);
	
	}
}



bool16 N2PsqlUpdateStorysAndDBUtils::Fotos_ActualizaGuiaDNota(IDocument* document,PMString GuiaNota, PMString Id_NotaPadreDFoto)
{
	bool16 retval=kFalse;
	
	do
	{

		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
			
		//Interfaz para cambiar el adornament(adorno) que muestra el tipo Estado del elemento de la nota
			InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
			(
				kN2PAdIssueUtilsBoss,	// Object boss/class
				IN2PAdIssueUtils::kDefaultIID
			)));
		
			if(N2PAdIssueUtil==nil)
			{
				break;
			}
					
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		PMString DSNWherElem="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto" && RegistrosEnXMP[NumReg].IDElemPadre==Id_NotaPadreDFoto)
			{	
				//Si la foto proviene del servidor local
					
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					DSNWherElem= RegistrosEnXMP[NumReg].NameDSN;
					
					PMString RutaElemEnBD="";
					//this->Fotos_TraerPathDeFotoEnBD(IDElementoFoto,DSNWherElem,RutaElemEnBD);				
					
					
					UIDRef pageItemUIDRef(db,UIDFrameFoto);
					
					
					InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
					ASSERT(itemHierarchy);
					if(!itemHierarchy) 
					{
						continue;
					}
					
					UIDList ListFrame=UIDList(pageItemUIDRef);
					N2PAdIssueUtil->UpdateUIDListLabelfor( ListFrame,GuiaNota, kTrue);
			}
		}
	}while(false);

	
	return retval;
}



bool16 N2PsqlUpdateStorysAndDBUtils::ValidaExistePaginaSobreBD(IDocument* document)
{
	bool16 retval=kFalse;
	do
	{
		if(document==nil)
		{
			CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
			break;
		}
		
		PMString NameDocumentActual="";
			document->GetName(NameDocumentActual);
			NameDocumentActual.Remove(NameDocumentActual.NumUTF16TextChars()-5,5);
			//Obtierne el Nombre del Documento actual
				
			PMString N2PSQLFolioPag  = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
				
			PMString N2PSQL_ID_Pag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
			
			if(N2PSQL_ID_Pag.NumUTF16TextChars()<=0)
			{
				retval=kTrue;
				break;
			}
			//Arma la cadena para buscar los datos de la pagina de enfrente
			PMString Busqueda="SELECT P.Nombre_Archivo, P.Id_Seccion, (SELECT Nombre_Estatus FROM Estatus_Elemento WHERE Id_Estatus=P.Id_Estatus) as Id_Estatus, P.Id_Publicacion, DATE_FORMAT(P.Fecha_Edicion,'%Y-%m-%d') ";
			Busqueda.Append(" AS SoloDia, P.Dirigido_a,  DATE_FORMAT(P.Fecha_Ult_Mod,'%H:%i:%s' )"); 
			Busqueda.Append(" AS HoraIn, DATE_FORMAT(P.Fecha_Ult_Mod,'%m/%d/%Y') AS FechaIn FROM  Pagina P");
			Busqueda.Append("  WHERE  P.ID = ");
			Busqueda.Append(N2PSQL_ID_Pag);
			Busqueda.Append("");	
		
			//Obtiene las Preferencias de coneccion  a BD
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
		
		
			PreferencesConnection PrefConections;
			if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			//Arma la cadena para la conexion ODBC
			PMString StringConection="";
		
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
			//Para obtener las funciones que realizan las conexiones a la BD y hace la conexion  y la consulta
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
			
			K2Vector<PMString> QueryVector;
				
				
				
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
			//CAlert::InformationAlert(Busqueda);
			
			if(QueryVector.Length()>0)
			{
				retval=kTrue;
			}	
			else
			{
				retval=kFalse;
			}
	}while(false);
	return retval;
}



bool16 N2PsqlUpdateStorysAndDBUtils::LimpiarOLlenarListaDeNotas()
{
	
	N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
	
	return(kTrue);
}





bool16 N2PsqlUpdateStorysAndDBUtils::UpdateCoordenadasDeUIDElemEnBaseD(PMString IDElementoDB,int32 ElementoUID,bool16 EsFoto,bool16 MandarCeros,IDocument* document)
{
	bool16 retval=kFalse;
	
	
	PMString IDUsuario="";
	PMString Aplicacion="";
	
	do
	{
		
		int32 NumLinesFaltantes;
 		int32 NumLinesRestantes;
 		int32 NumCarFaltantes;
 		int32 NumCarRestantes;
 		int32 NumCarOfTextModel;
 												
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		
		
		
		
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
		
		
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		if(Aplicacion.Contains("InCopy"))
		{
			break;
		}
		
		
		
			PreferencesConnection PrefConections;
		
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			PMString StringConection="";
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		} 
	
	
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		//for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		//{
			if(EsFoto)
			{	
				
				//Si la foto proviene del servidor local
				
				//if(PrefConections.DSNNameConnection ==  RegistrosEnXMP[NumReg].NameDSN)
//				{
					//IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					//IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto =ElementoUID;
					
					UIDRef UIDRefOfElementoNota(db,UIDFrameFoto);
					CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_Reales(UIDRefOfElementoNota);
					//CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_On_Spread(UIDRefOfElementoNota);
					
					StringProcedure="CALL SET_COORDENADAS_ELEMENTO (";
					
					StringProcedure.Append(IDElementoDB);
					StringProcedure.Append(",");

					if(!MandarCeros)
					{
						StringProcedure.AppendNumber(CoordFrameFoto.Top());
						StringProcedure.Append(",");
						StringProcedure.AppendNumber(CoordFrameFoto.Left());
						StringProcedure.Append(",");
						StringProcedure.AppendNumber(CoordFrameFoto.Bottom());
						StringProcedure.Append(",");
						StringProcedure.AppendNumber(CoordFrameFoto.Right());
						}
					else
					{
						StringProcedure.Append("0");
						StringProcedure.Append(",");
						StringProcedure.Append("0");
						StringProcedure.Append(",");
						StringProcedure.Append("0");
						StringProcedure.Append(",");
						StringProcedure.Append("0");
					}
					StringProcedure.Append(", @X )");
					
					
					//arma la cadena para la conexion a la base de datos local
					
					//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
					if(!SQLInterface->SQLQueryDataBase(StringConection , StringProcedure, Params))
					{
						break;
					}
//				}

				
					
			}
			else
			{
					//IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					//IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = ElementoUID;
					
					//UIDRef UIDRefOfElementoNota(db,UIDFrameFoto);
					//CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_Reales(UIDRefOfElementoNota);
					//CoordFrameFoto = N2PSQLUtilities::Get_Coordinates_On_Spread(UIDRefOfElementoNota);
					N2PSQLUtilities::GETStoryParams_OfUIDTextModel( UIDFrameFoto,
 												 CoordFrameFoto,
 												 NumLinesFaltantes,
 												 NumLinesRestantes,
 												 NumCarFaltantes,
 												 NumCarRestantes,
 												 NumCarOfTextModel);
					
					StringProcedure="CALL SET_COORDENADAS_ELEMENTO (";
					
					StringProcedure.Append(IDElementoDB);
					StringProcedure.Append(",");
					if(!MandarCeros)
					{
						StringProcedure.AppendNumber(CoordFrameFoto.Top());
						StringProcedure.Append(",");
						StringProcedure.AppendNumber(CoordFrameFoto.Left());
						StringProcedure.Append(",");
						StringProcedure.AppendNumber(CoordFrameFoto.Bottom());
						StringProcedure.Append(",");
						StringProcedure.AppendNumber(CoordFrameFoto.Right());
						}
					else
					{
						StringProcedure.Append("0");
						StringProcedure.Append(",");
						StringProcedure.Append("0");
						StringProcedure.Append(",");
						StringProcedure.Append("0");
						StringProcedure.Append(",");
						StringProcedure.Append("0");
					}
					StringProcedure.Append(", @X )");
					
					
					//arma la cadena para la conexion a la base de datos local
					
					//Executa el Store procedure enviando la cadena para la conexion a la base de deatso y los para,metros del Store procedure
					if(!SQLInterface->SQLQueryDataBase(StringConection , StringProcedure, Params))
					{
						break;
					}
			}
		//}
	}while(false);

	
	return retval;
}



bool16 N2PsqlUpdateStorysAndDBUtils::Verifica_BorradoDCaja_D_News2Page(const UIDList& ListFrameToDelete)
{
	bool16 retval=kFalse;
	do
	{
		//CAlert::InformationAlert("1");
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(!document)
			break;
		
		//CAlert::InformationAlert("2");
		IDataBase* db = ::GetDataBase(document);
		if(!db)
			break;
	
		//CAlert::InformationAlert("3");
		for(int32 i=0;i<ListFrameToDelete.Length();i++)
		{
			//CAlert::InformationAlert("4");
			UIDRef pageItemUIDRef(db,ListFrameToDelete[i]);
					
			//CAlert::InformationAlert("5");		
			InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
			ASSERT(itemHierarchy);
			if(!itemHierarchy) 
			{
				//CAlert::InformationAlert("itemHierarchy");
				continue;
			}
			else
			{
				//CAlert::InformationAlert("6");
				InterfacePtr<IGraphicFrameData> graphicFrameData(pageItemUIDRef, UseDefaultIID());
				ASSERT(graphicFrameData);
				if (!graphicFrameData) 
				{
					//CAlert::InformationAlert("graphicFrameData");
					break;
				}
				
				InterfacePtr<IMultiColumnTextFrame> MultiColumnText(graphicFrameData->QueryMCTextFrame ());
				if(!MultiColumnText)
				{
					break;
				}
					
				//CAlert::InformationAlert("10");
				UID UIDTextModelFrame=MultiColumnText->GetTextModelUID();
				int32 Int32UIDTextModel=UIDTextModelFrame.Get();
				StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
				//Obtiene el ID_elemento_padre apartir del textframe seleccionado
				retval=this->GetElementosNotaDXMP_Of_UIDTextModel(Int32UIDTextModel,StructNota_IDFrames, document);
				//SI NO ES ELMENTO DE UNA NOTA
				if(retval==kTrue)
				{
				
					CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoConNotaN2PDesligarNotaStringKey);
					/*//CAlert::InformationAlert("8");
					InterfacePtr<IFrameList> ListaDFrameQueContinStory(MultiColumnText->QueryFrameList());
					if(!ListaDFrameQueContinStory)
					{
						
						break;
					}
				
					//CAlert::InformationAlert("9");
					if(ListaDFrameQueContinStory->GetFrameCount()>1)
					{
				
						//PMString ASSS="";
						//ASSS.AppendNumber(ListaDFrameQueContinStory->GetFrameCount());
						//CAlert::InformationAlert(ASSS);
						//CAlert::InformationAlert("Se debe de mandar mensaje que debe desligar caja");
						retval=kTrue;
						break;
						//for(int32 XFrame=0;XFrame<ListaDFrameQueContinStory->Length();XFrame++)
						//{
						//	if(ListFrameToDelete[i]=ListaDFrameQueContinStory->GetNthFrameUID(XFrame))
						//	{
						//	
						//	}
						//}
					}
					//kN2PSQLAlertCajaDeTextoConNotaN2PDesligarNotaStringKey);*/
					break;
				}
				//CAlert::InformationAlert("Salir de Interceptor");

				
			}
			
		}
	}while(false);
	
	//CAlert::InformationAlert("Salio");
	return(retval);
}

/*
	Verifica sobre la base de datos quien fue el utilmo usuario que retiro la pagina
	si el usuario que se encuentra en la BD es la mima persona que intenta guardar cambios sobre esta regresa un verdadero
*/
bool16 N2PsqlUpdateStorysAndDBUtils::ValidaSiUsuario_P_CheckInEnBaseDDatos(const PMString& Id_Nota,const PMString& Id_Usuario)
{

	bool16 retval=kFalse;
	//Significa que alguien mas retiro la nota por lo tanto no puede modificar nada sobre la base de datos.
	//Debe de notificar al usuario que no puede depositar la nota y que esta sera deshabilitada para no generar 
	//Errores en la versiones de las nota
	
	do
	{
	
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
			
		PMString Consulta= "Select Id_Usuario FROM Bitacora_Elemento WHERE Id_Elemento="+Id_Nota+" AND Id_Evento=12 OR Id_Evento=4 OR Id_Evento=26 ORDER BY ID DESC LIMIT 1";
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
				
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		if(SQLInterface==nil)
		{
			break;
		}
						
		K2Vector<PMString> QueryVector;
		
		
		SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVector);
		
		for(int32 j=0;j<QueryVector.Length() ;j++)
		{
			
			PMString UserConUltimoCheckOutDNota = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[j]);
			
			//CAlert::InformationAlert("!"+UserConUltimoCheckOutDNota+"=="+Id_Usuario+"!");
			if(UserConUltimoCheckOutDNota==Id_Usuario)
				retval=kTrue;
			else
				retval=kFalse;
		}


	}while(false);
	return retval;
}



/*
 Liga Pie de foto a Foto
 */
bool16 N2PsqlUpdateStorysAndDBUtils::CreaPieDeFotoFunction()
{
	ErrorCode stat = kFailure;
	
	
	IAbortableCmdSeq* sequence = CmdUtils::BeginAbortableCmdSeq();
	if (sequence == nil)
	{
		ASSERT_FAIL("Cannot create command sequence?");
	}
	else
	{
		do
  		{
			//Para querys a base de datos
			PreferencesConnection PrefConections;
			PMString StringConection="";
			
			
			if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				continue;
			}
			
			StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
			
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																				(
																				 kN2PSQLUtilsBoss,	// Object boss/class
																				 IN2PSQLUtils::kDefaultIID
																				 )));
			if(SQLInterface==nil)
			{
				continue;
			} 	
			
			K2Vector<PMString> QueryContenido;
			PMString Busqueda="";
			
			///////////////
			
			
			
  			//Interfaz para cambiar el adornament(adorno) que muestra el tipo Estado del elemento de la nota
			InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
																						  (
																						   kN2PAdIssueUtilsBoss,	// Object boss/class
																						   IN2PAdIssueUtils::kDefaultIID
																						   )));
			
			if(N2PAdIssueUtil==nil)
			{
				break;
			}
			
			
			
			
			
		  	////////////////////////
			sequence->SetName(PMString("LigarPieDFotoAFoto",  PMString::kDontTranslateDuringCall));
			
  			PMString ID_Usuario="";
			
  			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				
				ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
				break;
			}
			
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			if(app==nil)
			{
				break;
			} 
			
			PMString Aplicacion=app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);
			
			if( Aplicacion.Contains("InCopy")  )
			{
				break;
			}
	 		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
			{
				ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
				break;
			}
			
  			UIDRef UidRefTextFrame=UIDRef::gNull;	//
  			UID UIDTextModel = kInvalidUID;			//
	  		UIDList ListFrameOfArticles=N2PSQLUtilities::GetUIDListOfGraphicFramesSelected(UidRefTextFrame);
			
			//Verifica que se encuentre seleccionada una caja de texto
  			if(UidRefTextFrame ==UIDRef::gNull)
  			{
				CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
  				break;
	  		}
			
			//Obtiene el UID del TextModel a partir del textframe seleccionado
  			UIDTextModel = N2PSQLUtilities::GetUIDTextModelFromUIDRefTextFrame(UidRefTextFrame);
			
  			//Validacion para revisar si existe como nota de news2page
			StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
			//Obtiene el ID_elemento_padre apartir del textframe seleccionado
			bool16 retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModel.Get(),StructNota_IDFrames, document);
			//SI regresa verdadero signifoca que ya es un elemento de nota
			if(retval==kTrue)
			{
				CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoYaEsElemNotaN2PStringKey);
				break;
			}
			
			
			//Verifica que existan cajas de imagen seleccionadas
  			if(ListFrameOfArticles.Length()<=0)
			{
  				CAlert::InformationAlert(kN2PsqlNoexistenCajasDImagenSelecStringKey);
	  			break;
  			}
			//Valida que solo exista un frame de imagen seleccionado 
			if(ListFrameOfArticles.Length()>1)
			{
  				CAlert::InformationAlert(kN2PsqlSoloSelect1CajaDImagenSelecStringKey);
	  			break;
  			}
			
			
			
			
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
				break;
			}
			
			
	  		UID selectedItem = kInvalidUID;
			
  			for(int32 NumFrameImage=0;NumFrameImage<ListFrameOfArticles.Length();NumFrameImage++)
  			{
				
	  			selectedItem = kInvalidUID;
  				selectedItem = ListFrameOfArticles.At(NumFrameImage);//UID de Frame
				
				
  				if(selectedItem==kInvalidUID)//si no existe
	  			{
					
  					continue;
  				}
				
  				UIDRef pageItemUIDRef(db,selectedItem);
				
				PMString UIDOfFramePlaced="";
				
				UID UIdParentOfItemPlaced = pageItemUIDRef.GetUID();
				
				UIDOfFramePlaced.AppendNumber(UIdParentOfItemPlaced.Get());		
				
				if( !N2PSQLUtilities::Existe_IDFrameFotoOnXMP("N2P_ListaUpdate", UIDOfFramePlaced) )//evisa si ya se encuentra linkeado en al pagina
				{
					CAlert::WarningAlert(kN2Psql_NoEsFotoLigadaAPagina_LigarPrimeroSelecStringKey);
					break;
					//continue;
				}	
				
				PMString ID_ElementoFoto="";
				N2PSQLUtilities::Busca_IDFrameFotoOnXMP("N2P_ListaUpdate", ID_ElementoFoto, UIDOfFramePlaced);
				
				/////////////////////////////////
				//Validar que la foto no contenga un pie de foto ya ligado.
				Busqueda="SELECT * FROM Elemento WHERE id_Elemento_padre=";
				Busqueda.Append(ID_ElementoFoto);
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryContenido);
				
				if(QueryContenido.Length()>0)
				{	
					CAlert::WarningAlert(kN2Psql_Yatiene_UnPiedFotoLigado_FluirSelecStringKey);
					break;
				}
				//
				/////////////////////////////////
				//bueno, a continuacion se debe de crear el nuevo elemento en XMP 
				//y crear en base de datos el nuevo elemento pie de foto ligado a la imagen
				Busqueda="CALL N2Pv1055_CreateElementHijo (";
				Busqueda.Append(ID_ElementoFoto);
				Busqueda.Append(",");
				Busqueda.Append("6");
				Busqueda.Append(",'");
				Busqueda.Append(ID_Usuario);
				Busqueda.Append("',@A)");
				
				PMString IdElePieDFoto="";
				//PMString StatusPhoto="";
				//PMString GuiaNota="";
				//QueryContenido;
				if(QueryContenido.Length()>0)
					QueryContenido.clear();
				
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryContenido);
				
				if(QueryContenido.Length()<=0)
					break;//Sin rsultados
				
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					IdElePieDFoto = SQLInterface->ReturnItemContentbyNameColumn("newElement",QueryContenido[j]);
				}
				
				if(IdElePieDFoto=="0")
				{
					//No se creo el nuevo elemento
					break;
				}
				else
				{
					//this->EnviarPorLotesContenidoDElemento_De_Nota(PMString Conte_Contenido,PMString IdElemento_Contenido);
					PMString ContenidoPieDFoto="";
					/*GuardarElementoDNotaEnBDSinCheckIN(PMString IDElementoPadre,PMString IDElemento,int32 UIDElementoNota, 
					 PMString& LastCheckinNotaToXMP,PMString Estado,PMString RoutedTo,PMString IDUsuario,PMString PublicacionID,PMString SeccionID, bool16 VieneDeCrearPase , 
					 IDocument* document,
					 PMString& TextoExportado)*/
					PMString LastCheckinNotaToXMP="";
					this->GuardarElementoDNotaEnBDSinCheckIN(ID_ElementoFoto,IdElePieDFoto,UIDTextModel.Get(),
															 LastCheckinNotaToXMP,
															 "0",
															 "0",
															 ID_Usuario,
															 "0",
															 "0", 
															 kFalse , 
															 document,
															 ContenidoPieDFoto);
					
					PMString UIDStringIDTextmodelPieFoto="";
					UIDStringIDTextmodelPieFoto.AppendNumber(UIDTextModel.Get());
					
					
					/////////Para obtener etiqueta de nota padre
					// En la siguiente parte hay que buscar el si se encuentra en checkout la nota a la que pertenece la foto
					//para poner en checkout el pie de foto
					//CAlert::InformationAlert("B0");
					int32 idFrameElemenNota=0;
					PMString EtiquetaNota="";
					
					StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[1];
					
					VectorNota[0].ID_Elemento_padre=ID_ElementoFoto;
					VectorNota[0].Id_Elem_PieFoto_Note=IdElePieDFoto;
					VectorNota[0].EsArticulo=kFalse;
					
					PMString Consulta = this->ARMA_UPDATE_NOTA_QUERY( VectorNota, 0, kFalse);
					QueryContenido.clear();
					SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryContenido);
					
					if(QueryContenido.Length()>0)
					{
						PMString DirigidoA = SQLInterface->ReturnItemContentbyNameColumn("DirigidoAOUT",QueryContenido[0]);
					
						PMString Estatus_Note  = SQLInterface->ReturnItemContentbyNameColumn("Nom_EstatusOUT",QueryContenido[0]);
					
						PMString GuiaDNota  = SQLInterface->ReturnItemContentbyNameColumn("GuiaNota",QueryContenido[0]);
						
						if(DirigidoA.NumUTF16TextChars()>0)
							EtiquetaNota=DirigidoA+"/"+Estatus_Note;
						else
							EtiquetaNota=Estatus_Note;
					}
					
					if(N2PSQLUtilities::GetIdFrameDElementoNotaANDEtiquetaDNota("N2P_ListaUpdate",idFrameElemenNota,EtiquetaNota,ID_ElementoFoto))
					{
						//CAlert::InformationAlert("B1");
						/////////////////////////
						bool16 UnLockStory=kFalse;
						if(this->CanDoCheckOutNota(idFrameElemenNota))
							UnLockStory=kTrue;
						
						if(N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",ID_ElementoFoto, IdElePieDFoto,
																				UIDStringIDTextmodelPieFoto, "PieFoto",
																				PrefConections.DSNNameConnection, EtiquetaNota, UnLockStory, LastCheckinNotaToXMP,kFalse) ==kTrue)
						{
							stat=kSuccess;
						}
					}
				}
	  		}
  		}while(false);
	}
  	
  	if (stat == kSuccess) 
	{
		// Everything completed so end the command sequence
		CmdUtils::EndCommandSequence(sequence);
		CAlert::InformationAlert(kN2PsqlPieDFotoCreadoStringKey);
		
	} else
	{
		// Abort the sequence and roll back the changes.
		CmdUtils::AbortCommandSequence(sequence);
		
	}
	return(kTrue);
}


/*
 Update TextFrame To DB
 */
void N2PsqlUpdateStorysAndDBUtils::ExportarNotasN2P_A_N2PJoomla(IDocument* document, const bool16& RefreshListaNotas)
{
	//CAlert::InformationAlert("Make Update");
	do
	{	
		//IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame document");
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ColocaTextYAplyStyleATextFrame db");
			break;
		}
		
		
		
		PMString ItemToSearchString="";
		PMString UIDModelString="";
		PMString IDNotaString="";
		PMString Busqueda="";
		PMString IDUsuario="";	
		PMString Aplicacion="";	
		PMString GuiaNota="";	
		
		PMString Fecha_Edicion_Nota =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_Nota);
		
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
																				(
																				 kN2PCTUtilitiesBoss,	// Object boss/class
																				 IN2PCTUtilities::kDefaultIID
																				 )));
		
		if(N2PCTUtils==nil)
			break;
		
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			ASSERT_FAIL("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		/////////////
		
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		////////////
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		int32 contNotasInVector=100;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[100];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		K2Vector<PMString> IDNotaEnPagina;
		
		if(contNotasInVector>0)
		{
			
			
			
			
			
			//
			N2PCTUtils->OcultaNotasConFrameOverset();	
			
			//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
				
				bool16 AlmenosUnElemntoValido=kFalse;
				K2Vector<PMString> Params;
				
				
				
				PMString IdNotaDB = VectorNota[numNota].ID_Elemento_padre;
				
				
				
				PMString Conte_Titulo="";
				PMString Conte_balazo="";//Contenido del Balazo
				PMString Conte_PieFoto="";//Contenido del pir de foto
				PMString Conte_Contenido="";//Contenido de la nota
				
				
				
				
				
				if(VectorNota[numNota].UIDFrTituloNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrTituloNote;
					UIDRef textModelRef(db, uidRef);
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_Titulo=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrTituloNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						AlmenosUnElemntoValido=kTrue;
					}
					
				}
				else
				{
					Conte_Titulo= this->ObtenerContenidoPlanoDElementoDesdeBaseDDatos(VectorNota[numNota].ID_Elemento_padre,8);//1073
				}
				
				
				if(VectorNota[numNota].UIDFrBalazoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrBalazoNote;
					UIDRef textModelRef(db, uidRef);
					
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_balazo= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrBalazoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						if(!AlmenosUnElemntoValido)
							AlmenosUnElemntoValido=kTrue;
					}
					
				}	
				else
				{
					//Hay que obtener el contenido del texto de elemento balazo sobre la base de datos de N2P y enviarlo tambien a tabla content_joomla
					Conte_balazo= this->ObtenerContenidoPlanoDElementoDesdeBaseDDatos(VectorNota[numNota].ID_Elemento_padre,7);//1073
					
				}
				
				if(VectorNota[numNota].UIDFrContentNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				{
					UID uidRef=VectorNota[numNota].UIDFrContentNote;
					UIDRef textModelRef(db, uidRef);
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{	
						RangeData myrange= 	mytextmodel->GetStoryThreadRange (0); 
						InDesign::TextRange range(mytextmodel, myrange.Start(nil), myrange.End() - myrange.Start(nil));
						
						//PMString nameParaStyle="9.4Neigh3Texto";
						TextIndex indexPara=0;
						int32 lengtPara=0;
						
						
						
						Conte_Contenido=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrContentNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
						//PMString dds="";
						//dds.AppendNumber(indexPara);
						//dds.Append(",  "+PrefConections.nameParaStyleCredito+": ");
						//dds.AppendNumber(PrefConections.nameParaStyleCredito.NumUTF16TextChars());
						//CAlert::InformationAlert(dds);
						if(PrefConections.nameParaStyleCredito.NumUTF16TextChars()>0)
						{	if(N2PSQLUtilities::FindParaStyleOnStory(range,PrefConections.nameParaStyleCredito,indexPara,lengtPara )==kSuccess)
							{
								//CAlert::InformationAlert("jajaja");
								Conte_Contenido.Insert("</Credito>", 10,indexPara+lengtPara);
								Conte_Contenido.Insert("<Credito>", 9,indexPara);
							}
						}
						
						if(!AlmenosUnElemntoValido)
							AlmenosUnElemntoValido=kTrue;
					}
					
					
				}	
				else
				{
					Conte_Contenido= this->ObtenerContenidoPlanoDElementoDesdeBaseDDatos(VectorNota[numNota].ID_Elemento_padre,5);//1073
				}
				
				/*bool16 EsPiedFoto=kFalse;
				 
				 if(VectorNota[numNota].UIDFrPieFotoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
				 {
				 UID uidRef=VectorNota[numNota].UIDFrPieFotoNote;
				 UIDRef textModelRef(db, uidRef);
				 
				 InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
				 if(mytextmodel!=nil)
				 {
				 Conte_PieFoto= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrPieFotoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
				 if(!AlmenosUnElemntoValido)
				 {
				 AlmenosUnElemntoValido=kTrue;
				 EsPiedFoto=kFalse;
				 }
				 }
				 
				 }
				 */
				if(AlmenosUnElemntoValido)
				{
					
					/*Al parecer evita que se envien las notas hijas hay que preguntar si estas deben de enviarse por lo pronto voy a enviar todas las notas
					 if(PrefConections.EsEnviarNotasHijasAWEB==0)
					 {
					 PMString ConsultaFamily="SELECT Id_Elemento_padre FROM Elemento  WHERE (Id_Elemento="+VectorNota[numNota].ID_Elemento_padre+" ) AND Id_tipo_ele =10";
					 
					 K2Vector<PMString> QueryVector;
					 //CAlert::InformationAlert(ConsultaFamily);
					 if(SQLInterface->SQLQueryDataBase(StringConection,ConsultaFamily,QueryVector))
					 {
					 if(QueryVector.Length()>0)
					 {
					 PMString Resultado = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento_padre",QueryVector[0]);
					 if(Resultado!="0")
					 {
					 continue;//Se brinca por que es una nota hija
					 }
					 }
					 }
					 }*/
					
					
					//Convierte comas en comillas de WEB
					if(PrefConections.EnviaAWEBSinRetornosTitulo>0)
					{
						PMString replace=" ";
						N2PSQLUtilities::ReplaceRetornBy(Conte_Titulo,replace);
						
					}
					
					if(PrefConections.EnviaAWEBEnAltasPieFoto>0)
					{
						Conte_PieFoto.ToUpper();
						PMString target="á";
						PMString remplace= "Á";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="é";
						remplace= "É";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="í";
						remplace= "Í";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="ó";
						remplace= "Ó";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="ú";
						remplace= "Ú";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
						target="ñ";
						remplace= "Ñ";
						N2PSQLUtilities::ReplaceAllOcurrencias(Conte_PieFoto,target,remplace);
					}
					
					//SDKUtilities::Replace(Conte_Titulo, "'", "&quot;");
					//SDKUtilities::Replace(Conte_Titulo, "/n", "<br>;");
					//SDKUtils::Replace(Conte_Titulo, "/n", "<br>;");
					//SDKUtilities::Replace(Conte_balazo, "'", "&quot;");
					//SDKUtilities::Replace(Conte_balazo , "/n", "<br>;");
					
					//SDKUtilities::Replace(Conte_Contenido, "'", "&quot;");
					//SDKUtilities::Replace(Conte_Contenido , "/n", "<br>;");
					
					//SDKUtilities::Replace(Conte_PieFoto, "'", "&quot;");
					//SDKUtilities::Replace(Conte_PieFoto , "/n", "<br>;");
					
					K2Vector<PMString> QueryVector;
					
					Busqueda="CALL N2PV1055_SubeNotas_A_N2PJoomla("+VectorNota[numNota].ID_Elemento_padre+",'"+Conte_Titulo+"','<p>"+Conte_balazo+"</p></br>','"+Conte_Contenido+"')";
					SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
					
					PMString Resultado ="";
					for(int32 i=0;i<QueryVector.Length();i++)
					{
						
						Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
						//CAlert::InformationAlert(Resultado);
					}
					
					
					IDNotaEnPagina.push_back(VectorNota[numNota].ID_Elemento_padre);
				}
			}	
		}
		else
		{
			ASSERT_FAIL(kN2PSQLNoExistenNotasSobreDocumentoStringKey);
		}
		
		
		
		
		
		//////////////////////////////////
		//para obtener las notas que deben ser borradas sobre la tabla web
		//delete VectorNota;
		/*K2Vector<PMString> NotasABorrarDTablaWEB;//Notas que deben ser borradas de la natbla WEB
		 
		 //Debe borrar de la tabla Notaweb las notas que no se encuentren sobre la pagina y que estan en la tabla ElemntoporPagina
		 PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		 
		 K2Vector<PMString> VectorIDNotasEnPagina;
		 
		 Busqueda="SELECT Id_Elemento FROM elemento_por_pagina WHERE Pagina_ID="+IDPaginaENXMP;
		 SQLInterface->SQLQueryDataBase(StringConection,Busqueda,VectorIDNotasEnPagina);
		 
		 PMString IDElementoEnPagina ="";
		 for(int32 i=0;i<VectorIDNotasEnPagina.Length();i++)//ciclo de elementos en pagina desde tabla Elemento_Por_Pagina
		 {
		 
		 IDElementoEnPagina = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",VectorIDNotasEnPagina[i]);
		 
		 bool16 ExisteEnPagina=kFalse;
		 for(int32 j=0;j<IDNotaEnPagina.Length() && ExisteEnPagina==kFalse;j++)//Ciclo de Elementos Sobre pagina real
		 {
		 if(IDNotaEnPagina[j]==IDElementoEnPagina)
		 ExisteEnPagina=kTrue;
		 }
		 
		 if(ExisteEnPagina==kFalse)
		 {
		 NotasABorrarDTablaWEB.push_back(IDElementoEnPagina);
		 }
		 }
		 
		 for(int32 i=0 ; i<NotasABorrarDTablaWEB.Length() ; i++)
		 {
		 SQLInterface->SQLSetInDataBase(StringConection,"CALL  DELETE_NOTAFOT_D_DOTASWEB( "+NotasABorrarDTablaWEB[i]+","+"0"+")");
		 
		 }*/
		
		
	}while(false);
}


bool16 N2PsqlUpdateStorysAndDBUtils::ExportarNomFotosEnPagA_N2P_Joomla_Multimedia(IDocument* document)
{
	bool16 retval=kFalse;
	IDFile file;
	do
	{
		
		
		//CAlert::InformationAlert("Pasa 1");
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		///////
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota No Preferences");
			break;
		}
		
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		if(SQLInterface==nil)
		{
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
		PMString Busqueda="";
		
		
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		/*
		 calse
		 struct ClassRegEnXMP
		 {
		 PMString IDElemPadre;
		 PMString IDElemento;
		 int32 IDFrame;
		 PMString TipoDato;
		 PMString NameDSN;
		 PMString LastCheckInNote;
		 };
		 */
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		K2Vector<PMString>  IDs_FotosOnPage;
		int32 NumReg=0;
		PMRect CoordFrameFoto;
		PMString IDElemPadreFoto;
		PMString IDElementoFoto;
		UID UIDFrameFoto;
		PMString StringProcedure="";
		PMString DSNWherElem="";
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
			{	
				//
				int32 IDFramePieFoto=0;
				PMString IDElementoPieFoto="0";
				PMString Conte_PieFoto="";
				for(int32 NumRegPPies	= 0 ; NumRegPPies < CantDatosEnXMP && IDFramePieFoto==0; NumRegPPies++)
				{
					if(RegistrosEnXMP[NumReg].IDElemento==RegistrosEnXMP[NumRegPPies].IDElemPadre)
					{
						//Se encotro y se encuentra fluido en la pagina el pie
						IDFramePieFoto=RegistrosEnXMP[NumRegPPies].IDFrame;
						IDElementoPieFoto=RegistrosEnXMP[NumRegPPies].IDElemento;
					}
				}
				//Si la foto proviene del servidor local
				
				IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
				IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
				UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
				DSNWherElem= RegistrosEnXMP[NumReg].NameDSN;
				
				
				if(IDFramePieFoto==0)
				{
					//Buscar Pie de Foto en Base de datos
					/*	QueryVector.clear();
					 Busqueda="SELECT Id_Elemento FROM Elemento WHERE id_elemento_padre="+IDElementoFoto+"";
					 SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
					 
					 if(QueryVector.Length()>0)
					 {
					 */		QueryVector.clear();
					Busqueda="SELECT Contenido FROM Contenido WHERE id_Elemento=(SELECT Id_Elemento FROM Elemento WHERE id_elemento_padre="+IDElementoFoto+")";
					SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
					
					
					for(int32 i=0;i<QueryVector.Length();i++)
					{
						
						Conte_PieFoto = SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryVector[i]);
						
						K2Vector<N2PNotesStorage*> fBNotesStore;
						UIDRef RefTagetTextImported = N2PSQLUtilities::ImportTaggedTextOfPMString(Conte_PieFoto, fBNotesStore);
						Conte_PieFoto = N2PSQLUtilities::GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
						
					}
					//	}
					
				}
				{
					//Obtener Contenido de Pie de Foto 
					UID uidRef=IDFramePieFoto;
					UIDRef textModelRef(db, uidRef);
					
					InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
					if(mytextmodel!=nil)
					{
						Conte_PieFoto= N2PSQLUtilities::GetPMStringOfTextFrame(IDFramePieFoto);
					}
					
				}
				
				
				
				PMString RutaElemEnBD="";
				
				
				UIDRef pageItemUIDRef(db,UIDFrameFoto);
				
				
				InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
				ASSERT(itemHierarchy);
				if(!itemHierarchy) 
				{
					IDs_FotosOnPage.push_back(IDElementoFoto);
					continue;
				}
				
				IDataBase* dbPageItem = pageItemUIDRef.GetDataBase();
				UIDList children(dbPageItem);
				
				
				
				itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
				for (int32 c = 0; c < children.Length(); c++)
				{
					UIDRef datachildRef1(GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(c));
    				InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
    				if(graphicFrameHierarchy1==nil)
					{
						continue;
					} 
					
					
					IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);
					
					InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID());
					ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
					ILinkManager::QueryResult linkQueryResult;
					
					PMString FullNameOfLinkRef="";
					PMString NombreFile="";
					PMString extension="";
					PMString dststringfile="";
					PMString publicacion="";
					PMString seccion="";
					PMString fecha="";
					PMString nombreBDMulti="";
					
					if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult))
					{
						ASSERT_MSG(linkQueryResult.size()==1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
						ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
						
						UIDRef linkUIDRef = UIDRef(iDataBase, *iter);
						InterfacePtr<IDataLink> dataLnk(iDataBase, *iter,UseDefaultIID());
						if(dataLnk==nil)
						{
							PMString ("no es por aqui dataLnk");
						}
						
						InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
						if (iLink!=nil)
						{
							InterfacePtr<ILinkResource> iLinkResource(iLinkManager->QueryResourceByUID(iLink->GetResource()));
							ASSERT_MSG(iLinkResource,"CHMLFiltHelper::addGraphicFrameDescription - Link with no associated asset?");
							if(iLinkResource!=nil)
							{
								PMString ("UNDEFINED");
								PMString datalinkPath = iLinkResource->GetLongName(true);
								//CAlert::InformationAlert(datalinkPath);
								if(datalinkPath.CharCount()>0) 
								{
									if(datalinkPath.Contains("@"))
									{
										UIDList uidsToUpdate(db);
										InterlasaUtilities::GetListaDLinksParaActualizar( graphicFrameHierarchy1, uidsToUpdate);
										
										if ( !uidsToUpdate.IsEmpty() )
										{
											Utils<Facade::ILinkFacade>()->UpdateLinks(uidsToUpdate, true, kMinimalUI, false);
											continue;
											//ImportImage.AplicarAjusteRecImagen(frameUIDRef);
										}
									}
									
									
									if(Utils<ILinkUtils>()->GetFileFromLink(iLink,&file))
									{
										if(!FileUtils::DoesFileExist(file))
										{
											continue;
										}
										FileUtils::IDFileToPMString(file, datalinkPath);
									}
									
									FullNameOfLinkRef=datalinkPath;
								}
							}
						}
						
						/**Relink **/
						
						
					
						FileUtils::GetBaseFileName(file, NombreFile);
						FileUtils::	GetExtension(file, extension);
						
#ifdef MACINTOSH
						
						
						Busqueda="SELECT Nombre FROM Elemento WHERE Id_Elemento="+RegistrosEnXMP[NumReg].IDElemento;
						//Obtener los datos de la ruta estos son publicacion, seccion, 
						K2Vector<PMString> QueryContenidof;
						SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryContenidof);
						nombreBDMulti="";
						for(int32 j=0;j<QueryContenidof.Length();j++)
						{
							nombreBDMulti = SQLInterface->ReturnItemContentbyNameColumn("Nombre",QueryContenidof[j]);
						}
						
						if(nombreBDMulti!=NombreFile+"."+extension)
						{
							Busqueda="SELECT (SELECT Nombre_de_Seccion FROM Seccion WHERE id_Seccion=e.Id_Seccion) as seccion, (SELECT Nombre_Publicacion FROM Publicacion WHERE id_Publicacion=e.Id_Publicacion) as publicacion,  DATE_FORMAT(NOW(),'%Y%m%d') as fecha FROM Elemento e WHERE e.Id_Elemento="+RegistrosEnXMP[NumReg].IDElemento;
							//Obtener los datos de la ruta estos son publicacion, seccion, 
							K2Vector<PMString> QueryContenido;
							SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryContenido);
							for(int32 j=0;j<QueryContenido.Length();j++)
							{
								publicacion = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryContenido[j]);
								seccion= SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryContenido[j]);
								fecha= SQLInterface->ReturnItemContentbyNameColumn("fecha",QueryContenido[j]);
							}
							if(PrefConections.PathOfServerFileImages.NumUTF16TextChars()>0);
							{	
								dststringfile.Append(PrefConections.PathOfServerFileImages);
								SDKUtilities::AppendPathSeparator(dststringfile);
							}
						
							if(PrefConections.ImagCarpInServFImages.NumUTF16TextChars()>0)
							{
								dststringfile.Append(PrefConections.ImagCarpInServFImages);
								SDKUtilities::AppendPathSeparator(dststringfile);
							}
							if(publicacion.NumUTF16TextChars()>0)
							{
								dststringfile.Append(publicacion);
								SDKUtilities::AppendPathSeparator(dststringfile);
							}
							if(seccion.NumUTF16TextChars()>0)
							{
								dststringfile.Append(seccion);
								SDKUtilities::AppendPathSeparator(dststringfile);
							}
							if(fecha.NumUTF16TextChars()>0)
							{
								dststringfile.Append(fecha);
								SDKUtilities::AppendPathSeparator(dststringfile);
							}
						
							//Crea las carpetas si no existen
							IDFile Folder=FileUtils::PMStringToSysFile (dststringfile);
							FileUtils::CreateFolderIfNeeded( Folder,kTrue );
						
							//adiciona el nombre del archivo
							dststringfile.Append( NombreFile+"."+extension );
							IDFile dstFile = FileUtils::PMStringToSysFile (dststringfile);
						
							if(FileUtils::CopyFile(file,dstFile))
							{
								//CAlert::InformationAlert("Exito de copia");
								//hay que hacer reelink
								URI tmpURI;
								Utils<IURIUtils>()->IDFileToURI(dstFile, tmpURI);
							
								UID newLinkUID;
								if(Utils<Facade::ILinkFacade>()->RelinkLink(linkUIDRef, tmpURI, kMinimalUI, newLinkUID)!=kSuccess)
								{
								
								}
								else
								{
									SDKUtilities::RemoveFirstElement(dststringfile);
									PMString target=":";
									PMString remplace= "/";
									N2PSQLUtilities::ReplaceAllOcurrencias(dststringfile,target,remplace);
									FullNameOfLinkRef=dststringfile;
									
									
									//Reemplaza el nuevo path sobre la tabla Elemento
									Busqueda="Update Elemento SET Nombre='"+NombreFile+"."+extension+"' , Ruta_Elemento='"+FullNameOfLinkRef+"', Nombre_Archivo='"+NombreFile+"."+extension+"' WHERE id_Elemento="+RegistrosEnXMP[NumReg].IDElemento;
									SQLInterface->SQLSetInDataBase(StringConection,Busqueda);
								}
								
								
							}
							
						}
						else
						{
							PMString target=":";
							PMString remplace= "/";
							N2PSQLUtilities::ReplaceAllOcurrencias(dststringfile,target,remplace);
							FullNameOfLinkRef=dststringfile;// no envia nada y el nombre se mantiene
						}
#endif
					}
					
					PMString FullNameOfLink = FullNameOfLinkRef;//FullNameOfLinkRef->GrabCString();
					
					
					
					PMString Separator="";
					SDKUtilities::AppendPathSeparator(Separator);
					
					
					
					PMString NameFile=NombreFile+"."+extension;
					
					
					QueryVector.clear();
					Busqueda="CALL N2PV1055_subeMultimedia_A_N2P_joss_multimedia ("+
					IDElemPadreFoto+","+
					IDElementoFoto+",'"+
					NameFile+"',"+
					"'"+FullNameOfLink+"','"+
					Conte_PieFoto+"',"+
					"'',"+
					"'',"+
					"1)";
					SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
					
					PMString Resultado ="";
					for(int32 i=0;i<QueryVector.Length();i++)
					{
						
						Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
						//CAlert::InformationAlert(Resultado);
					}
					
					//CAlert::InformationAlert("hablo a script");
					//system("sh /Developer/InDesign\\ SDK\\ CS3/devtools/sdktools/dollyxs/DollyXs.sh");					
  				}// loop over kids with IDataLinkReference interface	
			}
		}
	}while(false);
	
	
	return retval;
}

bool16 N2PsqlUpdateStorysAndDBUtils::ExportarFotosNoFluidas_DeNotasEnPagina_A_N2P_Joomla_Multimedia(IDocument* document)
{
	bool16 retval=kFalse;
	
	do
	{
		
		
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		///////
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota No Preferences");
			break;
		}
		
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		if(SQLInterface==nil)
		{
			break;
		}
		
		
		/*
		 struct StructIdFramesElementosXIdNota
		 {
		 PMString ID_Elemento_padre;		//Id_Elemento_padre
		 PMString Id_Elem_Cont_Note;		//Id_Elemento que contiene el contenido de la nota
		 PMString Id_Elem_Tit_Note;		//Id
		 PMString Id_Elem_PieFoto_Note;
		 PMString Id_Elem_Balazo_Note;
		 
		 int32 UIDFrTituloNote;				//UID Frame en donde fue fluido el titulo de la nota
		 int32 UIDFrPieFotoNote;
		 int32 UIDFrBalazoNote;
		 int32 UIDFrContentNote;
		 
		 PMString LastCheckInNote;
		 
		 PMString DSNNote;
		 
		 bool16  EsArticulo;			//Para indicar que es un articu,o completo pues puede ser otro tipo osea una foto para version v1.0.70
		 };
		 */
		int32 contNotasInVector=100;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[100];
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		
		for(int32 numArticle=0;numArticle < contNotasInVector;numArticle++)
		{
			K2Vector<PMString> QueryVector;
			PMString Busqueda="CALL N2PV1055_subeMultimedia_IF_NOT_EXISTS_inTN2PJoomlaContent ("+
			VectorNota[numArticle].ID_Elemento_padre+")";
			
			//CAlert::InformationAlert(Busqueda);
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
			
			PMString Resultado ="";
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				
				Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
				//CAlert::InformationAlert(Resultado);
			}
		}
		
	}while(false);
	
	
	return retval;
}



//Checa las notas que se estan editando actualmente 
//Guarda el contenido de cada uno de los elementos de la nota en la BD
bool16 N2PsqlUpdateStorysAndDBUtils::GuardarContenidoNotasEnEdicionEn_TVersionesDElemento_ComoTextPlain()
{
	bool16 retval=kFalse;
	
	
	do
	{	
		PMString IDUsuario="";
		PMString IDEvento="";
		PMString Aplicacion="";
		PMString ItemToSearchString="";
		PMString UIDModelString="";
		PMString IDNotaString="";
		PMString Busqueda="";
		PMString StringConection="";
		PMString Buscar="";
		K2Vector<PMString> QueryContenido;
		
		IDocument* document =  Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
			break;
		
		IDataBase* db = ::GetDataBase(document);
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		PMString Fecha_Edicion_NotaXXX =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_NotaXXX);
 		
		if(N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks())
		{
			break;	
		}
		
		if(!this->ObtenUsuarioLogeadoActualmente(IDUsuario))
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		
		
		if( Aplicacion.Contains("InCopy")  )
			IDEvento="33";
		else
			IDEvento="34";
		
		Aplicacion = app->GetApplicationName();
		
		Aplicacion.SetTranslatable(kFalse);
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
																				(
																				 kN2PCTUtilitiesBoss,	// Object boss/class
																				 IN2PCTUtilities::kDefaultIID
																				 )));
		
		if(N2PCTUtils==nil)
		{
			break;
		}
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
 		if(SQLInterface==nil)
		{
			break;
		} 
		
		
		
		
		bool16 AlmenosUnElemntoValido=kFalse;
		
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		K2Vector<int32> Notas;	
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		if(contNotasInVector>0)
		{
			
			//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
				
				
				if(!this->CanDoCheckInNota(VectorNota[numNota].UIDFrContentNote) && 
				   !this->CanDoCheckInNota(VectorNota[numNota].UIDFrTituloNote) &&
				   !this->CanDoCheckInNota(VectorNota[numNota].UIDFrPieFotoNote) &&
				   !this->CanDoCheckInNota(VectorNota[numNota].UIDFrBalazoNote))
				{	//CAlert::InformationAlert("SE supone que no hixzo Update");
					continue;
				}
				else
				{
					AlmenosUnElemntoValido=kFalse;
					
					PMString Conte_Titulo="";
					PMString Conte_balazo="";//Contenido del Balazo
					PMString Conte_PieFoto="";//Contenido del pir de foto
					PMString Conte_Contenido="";//Contenido de la nota
					
					//CAlert::InformationAlert("SE supone que no hixzo Update");
					
					PMString LastCheckinNotaToQuery="";
					PMString Fecha_Edicion="0";
					LastCheckinNotaToQuery=Fecha_Edicion;
					Fecha_Edicion=MySQLFecha;
					
					
					//Obtiene contenido en texto plano de Titulo de la nota
					if(VectorNota[numNota].UIDFrTituloNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrTituloNote;
						UIDRef textModelRef(db, uidRef);
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_Titulo=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrTituloNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							AlmenosUnElemntoValido=kTrue;
						}
						
					}
					
					//Obtiene contenido en texto plano de Pie de foto de la nota
					if(VectorNota[numNota].UIDFrPieFotoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrPieFotoNote;
						UIDRef textModelRef(db, uidRef);
						
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_PieFoto= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrPieFotoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							if(!AlmenosUnElemntoValido)
								AlmenosUnElemntoValido=kTrue;
						}
						
					}
					
					//Obtiene contenido en texto plano de balazo de la nota
					if(VectorNota[numNota].UIDFrBalazoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrBalazoNote;
						UIDRef textModelRef(db, uidRef);
						
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_balazo= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrBalazoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							if(!AlmenosUnElemntoValido)
								AlmenosUnElemntoValido=kTrue;
						}
						
					}	
					
					//Obtiene contenido en texto plano de Contenido de la nota
					if(VectorNota[numNota].UIDFrContentNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrContentNote;
						UIDRef textModelRef(db, uidRef);
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_Contenido=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrContentNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							if(!AlmenosUnElemntoValido)
								AlmenosUnElemntoValido=kTrue;
						}
						
					}	
					
					
					//ingresar contenidos por lotes
					
					PMString FirstParteConte_Titulo="";
					bool16 isConte_TituloMayTreitamil=kFalse;
					if(Conte_Titulo.NumUTF16TextChars()>30000)
					{
						FirstParteConte_Titulo=Conte_Titulo.Substring(0,29999)->GrabCString();
						isConte_TituloMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_Titulo=Conte_Titulo;
					}
					
					PMString FirstParteConte_balazo="";
					bool16 isConte_balazoMayTreitamil=kFalse;
					if(Conte_balazo.NumUTF16TextChars()>30000)
					{
						FirstParteConte_balazo=Conte_balazo.Substring(0,29999)->GrabCString();
						isConte_balazoMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_balazo=Conte_balazo;
					}
					
					PMString FirstParteConte_PieFoto="";
					bool16 isConte_PieFotoMayTreitamil=kFalse;
					if(Conte_PieFoto.NumUTF16TextChars()>30000)
					{
						FirstParteConte_PieFoto=Conte_PieFoto.Substring(0,29999)->GrabCString();
						isConte_PieFotoMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_PieFoto=Conte_PieFoto;
					}
					
					
					PMString FirstParteConte_Contenido="";
					bool16 isConte_ContenidoMayTreitamil=kFalse;
					if(Conte_Contenido.NumUTF16TextChars()>30000)
					{
						FirstParteConte_Contenido=Conte_Contenido.Substring(0,29999)->GrabCString();
						isConte_ContenidoMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_Contenido=Conte_Contenido;
					}
					
					
					////////////
					PreferencesConnection PrefConections;		
					
					
					if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
					{
						break;
					}
					
					
					StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
					
					
					Buscar = "CALL INSERT_IN_VERSIONES_ELEM(" + VectorNota[numNota].ID_Elemento_padre + ",'";
					Buscar.Append(IDUsuario);
					Buscar.Append("',");
					Buscar.Append(IDEvento);//Id Evento
					Buscar.Append(",");
					Buscar.Append("'0'");//Id Estatus si manda nulo el procedimiento almacenado obtiene el ultimo Estatus del nota
					Buscar.Append(",'");
					Buscar.Append(FirstParteConte_Contenido); //Contenido de nota
					Buscar.Append("','");
					Buscar.Append(FirstParteConte_Titulo);//Contenido de Titulo
					Buscar.Append("','");
					Buscar.Append(FirstParteConte_balazo);//Contenido de Balazo
					Buscar.Append("','");
					Buscar.Append(FirstParteConte_PieFoto);//Contenido de Pie de foto
					Buscar.Append("')");
					//CAlert::InformationAlert(Buscar);
					
					
					
					if(QueryContenido.Length()>0)
						QueryContenido.clear();
					
					if(!SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido))
					{
						break;
					}
					
					
					PMString IdDVersionElemento="";
					
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						IdDVersionElemento= SQLInterface->ReturnItemContentbyNameColumn("ID_VersionElemnto",QueryContenido[j]);
					}
					
					
					////////////
					//Id de Version de Elemento generado en Base de datos
					
					
					
					
					//CAlert::InformationAlert("NUM NUEVO DE VERSIONES ELEMENTO="+IdDVersionElemento);
					//Enviando Lotes sobrantes 
					if(isConte_TituloMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_Titulo,IdDVersionElemento,"8");
					}
					
					if(isConte_balazoMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_balazo,IdDVersionElemento,"7");
					}
					
					if(isConte_PieFotoMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_PieFoto,IdDVersionElemento,"6");
					}
					
					if(isConte_ContenidoMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_Contenido,IdDVersionElemento,"5");
					}
				}		
			}
			
			//this->guardarIDNotasPUpdateDesign(Notas);
		}
		retval=kTrue;
		//delete VectorNota;
	}while(false);
	
	return(retval);
}


bool16 N2PsqlUpdateStorysAndDBUtils::EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(PMString Conte_Contenido,PMString IdDVersionElemento,PMString Id_TipoElem)
{
 	do
 	{
		
		
 		PreferencesConnection PrefConections;		
		
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
 		///////////////
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
 		if(SQLInterface==nil)
		{
			break;
		} 
		
		
		
 		/********CONTENIDO DE ELEMENTO********/
		
		//CAlert::InformationAlert("FERNANDO LLANAS RDZ");
		
		PMString FirstParte="";
		int32 inicio=29999;
		int32 length=29999;
		
		
		
		
		
		
		PMString Buscar="";
		K2Vector<PMString> QueryContenido;
		bool16 pasarDNuevo=kTrue;
		PMString Contenido="";
		int32 CantidadDeCracteres=Conte_Contenido.NumUTF16TextChars();
		PMString AZS="";
		
		if(CantidadDeCracteres>0 && CantidadDeCracteres>30000)
		{
			if(CantidadDeCracteres > (inicio+length))
			{
				while(pasarDNuevo==kTrue)
				{
					
					FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
					
					Buscar = "CALL Append_Contenido_De_ElementoAVersioneElem(" + IdDVersionElemento + ",'";
					Buscar.Append(FirstParte);
					Buscar.Append("',"+Id_TipoElem+")");
					
					//CAlert::InformationAlert(Buscar);
					
					//AZS.Append("inicio=");
					//AZS.AppendNumber(inicio);
					//GuardarQuery(AZS+ ","+Buscar);
					
					
					QueryContenido.clear();
					if(!SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido))
						break;
					
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
					}
					
					inicio = inicio + length;
					
					
					
					
					
					if((inicio+length)>CantidadDeCracteres)
					{
						length=(CantidadDeCracteres-inicio)-1;
						pasarDNuevo=kFalse;
						
						
						FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
						
						Buscar = "CALL Append_Contenido_De_ElementoAVersioneElem(" + IdDVersionElemento + ",'";
						Buscar.Append(FirstParte);
						Buscar.Append("',"+Id_TipoElem+")");
						
						
						//AZS.Append("inicio=");
						//AZS.AppendNumber(inicio);
						//GuardarQuery(AZS+ ","+Buscar);
						
						QueryContenido.clear();
						SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
						for(int32 j=0;j<QueryContenido.Length();j++)
						{
							Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
						}
						
						//CAlert::InformationAlert("DEBE SALIR");
					}
					
					
				}
			}
			else
			{
				
				length=(CantidadDeCracteres-inicio)-1;
				
				
				FirstParte=Conte_Contenido.Substring(inicio,length)->GrabCString();
				
				
				Buscar = "CALL Append_Contenido_De_ElementoAVersioneElem(" + IdDVersionElemento + ",'";
				Buscar.Append(FirstParte);
				Buscar.Append("',"+Id_TipoElem+")");
				//CAlert::InformationAlert(Buscar);
				
				//AZS.Append("inicio=");
				//AZS.AppendNumber(inicio);
				//GuardarQuery(AZS+ ","+Buscar);
				
				QueryContenido.clear();
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					Contenido= SQLInterface->ReturnItemContentbyNameColumn("resultado",QueryContenido[j]);
				}
				
			}
		}
		/****************/
 	}while(false);
 	return(kTrue);
}


void N2PsqlUpdateStorysAndDBUtils::RestaurarVersionNota()
{
 	do
	{
		PMString ID_Usuario="";
		int32 UIDTextModelOfTextFrame =0;
		int32 numeroPaginaActual=0;
		bool16 retval=kFalse;
		//Ve si el existe usuario logeado
		if(!this->ObtenUsuarioLogeadoActualmente(ID_Usuario))
		{
			break;
		}
		
		
		
		
		//Ve si existe documento en frente o si se esta trabajando con algun documento
		IDocument* document =  Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		
		//Obtiene el ID text model del Frame que se esta seleccionando
		UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
		//SI NO SE ENCUENTRA SELECCIONADO UN TEXTFRAME
		if(UIDTextModelOfTextFrame<1)
		{
			CAlert::InformationAlert(kN2PSQLAlertSeleccionarCajaDeTextoStringKey);
			break;
		}
		
		
		StructIdFramesElementosXIdNota* StructNota_IDFrames=new StructIdFramesElementosXIdNota[1];
		//Obtiene el ID_elemento_padre apartir del textframe seleccionado
		retval=this->GetElementosNotaDXMP_Of_UIDTextModel(UIDTextModelOfTextFrame,StructNota_IDFrames, document);
		
		//SI NO ES ELMENTO DE UNA NOTA
		if(retval==kFalse)
		{
			CAlert::InformationAlert(kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey);
			break;
		}
		
		this->DoDialogVersionesNota();
	}while(false);
}


bool16 N2PsqlUpdateStorysAndDBUtils::DoDialogVersionesNota()
{
	bool16 retval=kFalse;
	do
	{
		/*// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("BscDlgActionComponent::DoDialog: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("BscDlgActionComponent::DoDialog: dialogMgr invalid"); 
			break;
		}
		
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
		 nLocale,					// Locale index from PMLocaleIDs.h. 
		 kN2PsqlPluginID,			// Our Plug-in ID from BasicDialogID.h. 
		 kViewRsrcType,				// This is the kViewRsrcType.
		 kN2PsqlRestoreNotaDlgResourceID,	// Resource ID for our dialog.
		 kTrue						// Initially visible.
		 );
		
		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("BscDlgActionComponent::DoDialog: can't create dialog"); 
			break;
		}
		
		// Open the dialog.
		dialog->Open(); 
		*/
	} while (false);
	
	return(retval);			
}



void N2PsqlUpdateStorysAndDBUtils::EnviaAVersionDNota(PMString ID_Elemento_padre,PMString IDUsuario,PMString Estado,PMString ID_Evento,PMString IdBitacoraElemento,PMString Fecha_Edicion)
{
	bool16 retval=kFalse;
	do
	{
		IDocument* document =  Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
			break;
		
		IDataBase* db = ::GetDataBase(document);
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
 		if(SQLInterface==nil)
		{
			break;
		} 
		
		
		PMString Fecha_Edicion_NotaXXX =  N2PSQLUtilities::GetXMPVar("N2PDate", document);
 		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha_Edicion_NotaXXX);
		
		bool16 AlmenosUnElemntoValido=kFalse;
		
		
		int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
		StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
		K2Vector<int32> Notas;	
		
		this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		if(contNotasInVector>0)
		{
			
			//Actualiza desde la base de datos los elementos de cada una de las notas fluidos sobre el documento abierto
			for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
			{	
				
				
				if(VectorNota[numNota].ID_Elemento_padre==ID_Elemento_padre)
				{	
					AlmenosUnElemntoValido=kFalse;
					
					PMString Conte_Titulo="";
					PMString Conte_balazo="";//Contenido del Balazo
					PMString Conte_PieFoto="";//Contenido del pir de foto
					PMString Conte_Contenido="";//Contenido de la nota
					
					//CAlert::InformationAlert("SE supone que no hixzo Update");
					
					PMString LastCheckinNotaToQuery="";
					Fecha_Edicion="0";
					LastCheckinNotaToQuery=Fecha_Edicion;
					Fecha_Edicion=MySQLFecha;
					
					//Obtiene contenido en texto plano de Titulo de la nota
					if(VectorNota[numNota].UIDFrTituloNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrTituloNote;
						UIDRef textModelRef(db, uidRef);
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_Titulo=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrTituloNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							AlmenosUnElemntoValido=kTrue;
						}
						
					}
					
					//Obtiene contenido en texto plano de Pie de foto de la nota
					if(VectorNota[numNota].UIDFrPieFotoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrPieFotoNote;
						UIDRef textModelRef(db, uidRef);
						
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_PieFoto= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrPieFotoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							if(!AlmenosUnElemntoValido)
								AlmenosUnElemntoValido=kTrue;
						}
						
					}
					
					//Obtiene contenido en texto plano de balazo de la nota
					if(VectorNota[numNota].UIDFrBalazoNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrBalazoNote;
						UIDRef textModelRef(db, uidRef);
						
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_balazo= N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrBalazoNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							if(!AlmenosUnElemntoValido)
								AlmenosUnElemntoValido=kTrue;
						}
						
					}	
					
					//Obtiene contenido en texto plano de Contenido de la nota
					if(VectorNota[numNota].UIDFrContentNote>0 && VectorNota[numNota].ID_Elemento_padre.NumUTF16TextChars()>0)
					{
						UID uidRef=VectorNota[numNota].UIDFrContentNote;
						UIDRef textModelRef(db, uidRef);
						InterfacePtr<ITextModel> mytextmodel(textModelRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							Conte_Contenido=N2PSQLUtilities::GetPMStringOfTextFrame(VectorNota[numNota].UIDFrContentNote);//N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(textModelRef);
							if(!AlmenosUnElemntoValido)
								AlmenosUnElemntoValido=kTrue;
						}
						
					}	
					
					
					//ingresar contenidos por lotes
					
					PMString FirstParteConte_Titulo="";
					bool16 isConte_TituloMayTreitamil=kFalse;
					if(Conte_Titulo.NumUTF16TextChars()>30000)
					{
						FirstParteConte_Titulo=Conte_Titulo.Substring(0,29999)->GrabCString();
						isConte_TituloMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_Titulo=Conte_Titulo;
					}
					
					PMString FirstParteConte_balazo="";
					bool16 isConte_balazoMayTreitamil=kFalse;
					if(Conte_balazo.NumUTF16TextChars()>30000)
					{
						FirstParteConte_balazo=Conte_balazo.Substring(0,29999)->GrabCString();
						isConte_balazoMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_balazo=Conte_balazo;
					}
					
					PMString FirstParteConte_PieFoto="";
					bool16 isConte_PieFotoMayTreitamil=kFalse;
					if(Conte_PieFoto.NumUTF16TextChars()>30000)
					{
						FirstParteConte_PieFoto=Conte_PieFoto.Substring(0,29999)->GrabCString();
						isConte_PieFotoMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_PieFoto=Conte_PieFoto;
					}
					
					
					PMString FirstParteConte_Contenido="";
					bool16 isConte_ContenidoMayTreitamil=kFalse;
					if(Conte_Contenido.NumUTF16TextChars()>30000)
					{
						FirstParteConte_Contenido=Conte_Contenido.Substring(0,29999)->GrabCString();
						isConte_ContenidoMayTreitamil=kTrue;//Indica que el condenido del titulo es mayor a treitamil caracteres
					}
					else
					{
						FirstParteConte_Contenido=Conte_Contenido;
					}
					
					
					////////////
					PreferencesConnection PrefConections;		
					
					
					if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
					{
						break;
					}
					
					PMString StringConection="";
					PMString Buscar="";
					StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
					
					
					Buscar = "CALL INSERT_IN_VERSIONES_ELEM(" + VectorNota[numNota].ID_Elemento_padre + ",'";
					Buscar.Append(IDUsuario);
					Buscar.Append("',");
					Buscar.Append(ID_Evento);//Id Evento
					Buscar.Append(",");
					Buscar.Append("'0'");//Id Estatus si manda nulo el procedimiento almacenado obtiene el ultimo Estatus del nota
					Buscar.Append(",'");
					Buscar.Append(FirstParteConte_Contenido); //Contenido de nota
					Buscar.Append("','");
					Buscar.Append(FirstParteConte_Titulo);//Contenido de Titulo
					Buscar.Append("','");
					Buscar.Append(FirstParteConte_balazo);//Contenido de Balazo
					Buscar.Append("','");
					Buscar.Append(FirstParteConte_PieFoto);//Contenido de Pie de foto
					Buscar.Append("',");
					Buscar.Append(IdBitacoraElemento);//Contenido de Pie de foto
					Buscar.Append(")");
					//CAlert::InformationAlert(Buscar);
					
					
					
					K2Vector<PMString> QueryContenido;
					if(!SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido))
					{
						break;
					}
					
					
					PMString IdDVersionElemento="";
					
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						IdDVersionElemento= SQLInterface->ReturnItemContentbyNameColumn("ID_VersionElemnto",QueryContenido[j]);
					}
					
					
					////////////
					//Id de Version de Elemento generado en Base de datos
					
					
					
					
					//CAlert::InformationAlert("NUM NUEVO DE VERSIONES ELEMENTO="+IdDVersionElemento);
					//Enviando Lotes sobrantes 
					if(isConte_TituloMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_Titulo,IdDVersionElemento,"8");
					}
					
					if(isConte_balazoMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_balazo,IdDVersionElemento,"7");
					}
					
					if(isConte_PieFotoMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_PieFoto,IdDVersionElemento,"6");
					}
					
					if(isConte_ContenidoMayTreitamil)
					{
						this->EnviarPorLotesContenidoDElemento_De_Nota_AVersionesElemento(Conte_Contenido,IdDVersionElemento,"5");
					}
				}		
			}
			
			//this->guardarIDNotasPUpdateDesign(Notas);
		}
		retval=kTrue;
		//delete VectorNota;
		
	}while(false);
}

//1073
PMString N2PsqlUpdateStorysAndDBUtils::ObtenerContenidoPlanoDElementoDesdeBaseDDatos(const PMString& Id_Nota,const int32& tipoElemento)
{
	PMString contenido="";
	do
	{
		PreferencesConnection PrefConections;
		
		if(!this->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL("N2PsqlUpdateStorysAndDBUtils::ActualizaNotaDesdeBDPorIDNota No Preferences");
			break;
		}
		
		//Para poner por default el root de caracter de estilos como Default
		N2PSQLUtilities::SetRootStyleADefaultCharStyle();
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		if(SQLInterface==nil)
		{
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
		PMString Busqueda="SELECT Contenido FROM Contenido WHERE id_Elemento=(SELECT Id_Elemento FROM Elemento WHERE id_elemento_padre="+Id_Nota+" AND id_tipo_ele=";
		Busqueda.AppendNumber(tipoElemento);
		Busqueda.Append(");");
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			contenido = SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryVector[i]);
			
			K2Vector<N2PNotesStorage*> fBNotesStore;
			UIDRef RefTagetTextImported = N2PSQLUtilities::ImportTaggedTextOfPMString(contenido, fBNotesStore);
			contenido = N2PSQLUtilities::GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
			
		}
		
	}while(false);
	return contenido;
}


void N2PsqlUpdateStorysAndDBUtils::ExportarPaginaYNotasABDHemeroteca(IDocument* document,
																	 PMString RutaPDF,
																	 PMString Rutajpg,
																	 PMString SPCallPaginnaChickIn,
																	 PreferencesConnection Connection)
{
	do
	{
		
		bool16 ExisteArchivo=kFalse;
		IDFile myFile=SDKUtilities::PMStringToSysFile(&RutaPDF);
		
		if(SDKUtilities::FileExistsForRead(RutaPDF)==kSuccess)
		{
			 ExisteArchivo=kTrue;
		}
		if(FileUtils::IsNetworkDrive (myFile))
		{
			//CAlert::InformationAlert("OK ");
			RutaPDF="z:Volumes:"+RutaPDF;
			Rutajpg="z:Volumes:"+Connection.PathOfServerFile+":apaginas:"+Rutajpg;
		}
		
		//Obtiene el id de la pagina sobre la base e datos de N2P
		PMString N2PID_Pagina = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		//Obtiene path del shell
		InterfacePtr <IPlugInList> iPlugInList(GetExecutionContextSession(),IID_IPLUGINLIST);
		if(iPlugInList==nil)
		{
			break;
		}
		
		
		IDFile	myIDFilePlugin;
		bool16 retva=iPlugInList->GetPathName(kN2PsqlPluginID, &myIDFilePlugin);
		if(!retva)
		{
			break;
		}
		PMString Path=SDKUtilities::SysFileToPMString(&myIDFilePlugin);
		SDKUtilities::AppendPathSeparator(Path);
		Path.Append("Resources");
		SDKUtilities::AppendPathSeparator(Path);
		PMString shpathMkDirFTP=Path;
		shpathMkDirFTP.Append("makefolder.sh");
		Path.Append("n2pftpconnect.sh");
		PMString ShellScriptPathUnix=InterlasaUtilities::MacToUnix(Path);
		//Hay que seleccionar obtener los datos de la base de datos.
		//Arma cadena de coxion ODBC a base de datos de N2P
		PMString StringConection="";
		StringConection.Append("DSN=" + Connection.DSNNameConnection + ";UID=" + Connection.NameUserDBConnection + ";PWD=" + Connection.PwdUserDBConnection);
		
		//Interface de utilidad para la conexion a la base de datos
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		if(SQLInterface==nil)//Si no se obtuvo la interface para la conexion a la base de datos
		{
			break;
		}
		//Vector que almacena resultados del query
		K2Vector<PMString> PaginaV;
		
		//Arma query pregunta por los datos e la pagina
		PMString Busqueda="SELECT * FROM Pagina WHERE id="+N2PID_Pagina;
		
		//Ejecuta consulta a la base de datos
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,PaginaV);
		N2PSQLUtilities::InicializaDebugeo();
		if(PaginaV.Length()>0)
		{
			//Envia por FTP jpg de pagina
			PMString serverftp= Connection.N2PHmeFTPServer;//"ftp.publish88.com";
			PMString userftp=   Connection.N2PHmeFTPUser;//"n2pfile@publish88.com";
			PMString pwdftp=    Connection.N2PHmeFTPPwd;//"n2pfile";
			
			PMString idPageInSQL = SQLInterface->ReturnItemContentbyNameColumn("ID",PaginaV[0]);
			
			PMString namePDFftpserver = "";
			PMString namejpgftpserver = "";
			PMString Nombre_Archivo = "";//Nombre de archivo asignado por el usuario diseñador
			
			Nombre_Archivo=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",PaginaV[0]);
			//Obtiene nombre de carpeta Edicion
			PMString id_Publicacio=SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",PaginaV[0]);
			PMString nombrePublicacion="";
			Busqueda="SELECT Nombre_Publicacion FROM publicacion WHERE Id_Publicacion="+id_Publicacio;
			K2Vector<PMString> PublicacionV;
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,PublicacionV);
			if(PublicacionV.Length()>0)
			{
				nombrePublicacion=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",PublicacionV[0]);
			}
			
			//Obtiene nombre de carpeta fecha
			//CAlert::InformationAlert(SQLInterface->ReturnItemContentbyNameColumn("Fecha_Edicion",PaginaV[0]));
			PMString fehcacompeta=SQLInterface->ReturnItemContentbyNameColumn("Fecha_Edicion",PaginaV[0]).GetItem(" ",1)->GrabCString();
			
			PMString fechaPublicacion = fehcacompeta;//InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(fehcacompeta);
			
			namePDFftpserver=idPageInSQL+"PageIDLast.pdf";
			
			PMString rutaftppdf="'/"+nombrePublicacion+"'";
			PMString shpathMkDirFTPUnix=InterlasaUtilities::MacToUnix(shpathMkDirFTP);
			PMString CommandStr="sh "+shpathMkDirFTPUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftppdf;
			N2PSQLUtilities::ImprimeMensaje(CommandStr);
			system(CommandStr.GrabCString());
			
			rutaftppdf="'/"+nombrePublicacion+"/"+fechaPublicacion+"'";
			CommandStr="sh "+shpathMkDirFTPUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftppdf;
			N2PSQLUtilities::ImprimeMensaje(CommandStr);
			system(CommandStr.GrabCString());
			
			rutaftppdf="'/"+nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"'";
			CommandStr="sh "+shpathMkDirFTPUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftppdf;
			N2PSQLUtilities::ImprimeMensaje(CommandStr);
			system(CommandStr.GrabCString());
			
			rutaftppdf="'/"+nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"/"+namePDFftpserver+"'";
			
			if(ExisteArchivo)
			{
				N2PSQLUtilities::ImprimeMensaje(RutaPDF);
				PMString RutaPDFUnix=InterlasaUtilities::MacToUnix(RutaPDF);
				N2PSQLUtilities::ImprimeMensaje(RutaPDFUnix);
				CommandStr="sh "+ShellScriptPathUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+RutaPDFUnix+" "+rutaftppdf;
								
				N2PSQLUtilities::ImprimeMensaje(CommandStr);
				
				system(CommandStr.GrabCString());
				//CAlert::InformationAlert("Se envio aftp");
			}
			//CAlert::InformationAlert("111");
			namejpgftpserver=idPageInSQL+"PageIDLast.jpg";
			
			//CAlert::InformationAlert("AAA "+RutaPDF);
			//CAlert::InformationAlert("222");
			PMString rutaftpjpg="'/"+nombrePublicacion+"'";
			PMString shpathMkDirJPGUnix=InterlasaUtilities::MacToUnix(shpathMkDirFTP);
			CommandStr="sh "+shpathMkDirJPGUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftpjpg;
			N2PSQLUtilities::ImprimeMensaje(CommandStr);
			system(CommandStr.GrabCString());
			//CAlert::InformationAlert("333");
			rutaftpjpg="'/"+nombrePublicacion+"/"+fechaPublicacion+"'";
			CommandStr="sh "+shpathMkDirJPGUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftpjpg;
			N2PSQLUtilities::ImprimeMensaje(CommandStr);
			system(CommandStr.GrabCString());
			//CAlert::InformationAlert("444");
			rutaftpjpg="'/"+nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"'";
			CommandStr="sh "+shpathMkDirJPGUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftpjpg;
			N2PSQLUtilities::ImprimeMensaje(CommandStr);
			system(CommandStr.GrabCString());
			//CAlert::InformationAlert("555");
			rutaftpjpg="'/"+nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"/"+namejpgftpserver+"'";
			//CAlert::InformationAlert("666");
			if(ExisteArchivo)
			{
				
				
				N2PSQLUtilities::ImprimeMensaje(Rutajpg);
				PMString RutajpgUnix=InterlasaUtilities::MacToUnix(Rutajpg);
				N2PSQLUtilities::ImprimeMensaje(RutajpgUnix);
				CommandStr="sh "+ShellScriptPathUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+RutajpgUnix+" "+rutaftpjpg;
				
				N2PSQLUtilities::ImprimeMensaje(CommandStr);
				
				system(CommandStr.GrabCString());
				//CAlert::InformationAlert("Se envio el jpg aftp");
			}
			//CAlert::InformationAlert("777");
						//Envia por FTP pdf de pagina
			PMString StringConectionHemeroteca="";
			StringConectionHemeroteca.Append("DSN="+Connection.N2PHmeDSNName+";UID="+Connection.N2PHmeDSNUserLogin+";PWD="+Connection.N2PHmeDSNPwdLogin+"");
			
			PMString salto="";
			//Llama a metodo del webservice para agregar o actualizar registro sobre la base de datos
			PMString InsertReplacePagina="REPLACE INTO Pagina(ID, Id_tipo_ele, Folio_pagina,"+ salto+
																"Id_Estatus, Id_Color, Id_Publicacion, Fecha_Edicion,"+salto+
																"Id_Seccion, Id_Tiro, Servidor, Ruta_Elemento, Pliego, Nombre_Archivo,"+salto+
																"Par_impar, Vista_Previa, Fecha_Creacion, Fecha_Ult_Mod, Dirigido_a,"+salto+
																"Webable, Proveniente_de, Calificacion, Cameo, Ruta_Preview_Pagina) "+salto+
														"VALUES("+SQLInterface->ReturnItemContentbyNameColumn("ID",PaginaV[0])+
																","+SQLInterface->ReturnItemContentbyNameColumn("Id_tipo_ele",PaginaV[0])+",'"+
																SQLInterface->ReturnItemContentbyNameColumn("Folio_pagina",PaginaV[0])+"',"+
																SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",PaginaV[0])+","+
																SQLInterface->ReturnItemContentbyNameColumn("Id_Color",PaginaV[0])+","+
			SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",PaginaV[0])+",'"+
			SQLInterface->ReturnItemContentbyNameColumn("Fecha_Edicion",PaginaV[0])+"',"+
			SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",PaginaV[0])+",0,'"+
			SQLInterface->ReturnItemContentbyNameColumn("Servidor",PaginaV[0])+"',"+
			rutaftpjpg+","+
			SQLInterface->ReturnItemContentbyNameColumn("Pliego",PaginaV[0])+",'"+
			SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",PaginaV[0])+"',0,'"+
			SQLInterface->ReturnItemContentbyNameColumn("Vista_Previa",PaginaV[0])+"','"+
			SQLInterface->ReturnItemContentbyNameColumn("Fecha_Creacion",PaginaV[0])+"','"+
			SQLInterface->ReturnItemContentbyNameColumn("Fecha_Ult_Mod",PaginaV[0])+"','"+
			SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",PaginaV[0])+"',0,'"+
			SQLInterface->ReturnItemContentbyNameColumn("Proveniente_de",PaginaV[0])+"',"+
			SQLInterface->ReturnItemContentbyNameColumn("Calificacion",PaginaV[0])+",'"+
			SQLInterface->ReturnItemContentbyNameColumn("Cameo",PaginaV[0])+"',"+
			rutaftpjpg+")";
			//CAlert::InformationAlert(InsertReplacePagina);
			SQLInterface->SQLSetInDataBase(StringConectionHemeroteca,InsertReplacePagina);
			N2PSQLUtilities::ImprimeMensaje(InsertReplacePagina);
			
			Busqueda="SELECT * FROM elemento_por_pagina WHERE Pagina_ID="+N2PID_Pagina;
			K2Vector<PMString> ElementoEnPagina;
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,ElementoEnPagina);
			if(ElementoEnPagina.Length()>0)
			{
				PMString InsertReplaceElemEnPaginas="";
				for(int32 i=0;i<ElementoEnPagina.Length();i++)
				{
					
					InsertReplaceElemEnPaginas.Append("REPLACE INTO elemento_por_pagina(");
					if(SQLInterface->ReturnItemContentbyNameColumn("Pagina_ID",ElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElemEnPaginas.Append("Pagina_ID,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",ElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElemEnPaginas.Append("Id_Elemento,");
					}
					
					InsertReplaceElemEnPaginas.Remove(InsertReplaceElemEnPaginas.NumUTF16TextChars()-1, 1);
					/////////////////////////
					//////////////////////////
					//////////////////////////
					InsertReplaceElemEnPaginas.Append(") VALUES(");
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Pagina_ID",ElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElemEnPaginas.Append(SQLInterface->ReturnItemContentbyNameColumn("Pagina_ID",ElementoEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",ElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElemEnPaginas.Append(SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",ElementoEnPagina[i])+",");
					}
					
					
					InsertReplaceElemEnPaginas.Remove(InsertReplaceElemEnPaginas.NumUTF16TextChars()-1, 1);
					
					InsertReplaceElemEnPaginas.Append(");");
				}
				//CAlert::InformationAlert(InsertReplaceElemEnPaginas);
				SQLInterface->SQLSetInDataBase(StringConectionHemeroteca,InsertReplaceElemEnPaginas);
				//CAlert::InformationAlert("Despues de la insercion");
			}
			
			
			
			Busqueda="SELECT DISTINCT ee.id_elemento,cc.contenido, cc.ID FROM elemento e INNER JOIN elemento_por_pagina p on p.id_elemento=e.id_elemento INNER JOIN elemento ee on ee.id_elemento_padre=e.id_elemento INNER JOIN contenido cc on ee.id_Elemento=cc.id_elemento AND p.Pagina_ID="+N2PID_Pagina;
			K2Vector<PMString> ContenidoDElementoEnPagina;
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,ContenidoDElementoEnPagina);
			if(ContenidoDElementoEnPagina.Length()>0)
			{
				PMString InsertReplaceContenidos="";
				for(int32 i=0;i<ContenidoDElementoEnPagina.Length();i++)
				{
					InsertReplaceContenidos.Append("REPLACE INTO Contenido(");
					if(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ContenidoDElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceContenidos.Append("id_elemento,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("ID",ContenidoDElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceContenidos.Append("ID,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("contenido",ContenidoDElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceContenidos.Append("contenido,");
					}
					
					InsertReplaceContenidos.Remove(InsertReplaceContenidos.NumUTF16TextChars()-1, 1);
					/////////////////////////
					//////////////////////////
					//////////////////////////
					InsertReplaceContenidos.Append(") VALUES(");
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ContenidoDElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceContenidos.Append(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ContenidoDElementoEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("ID",ContenidoDElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceContenidos.Append(SQLInterface->ReturnItemContentbyNameColumn("ID",ContenidoDElementoEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("contenido",ContenidoDElementoEnPagina[i]).NumUTF16TextChars()>0)
					{
						PMString Conte_PieFoto = SQLInterface->ReturnItemContentbyNameColumn("contenido",ContenidoDElementoEnPagina[i]);
						
						K2Vector<N2PNotesStorage*> fBNotesStore;
						UIDRef RefTagetTextImported = N2PSQLUtilities::ImportTaggedTextOfPMString(Conte_PieFoto, fBNotesStore);
						Conte_PieFoto = N2PSQLUtilities::GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
						
						InsertReplaceContenidos.Append("'"+Conte_PieFoto+"',");
					}
					
					InsertReplaceContenidos.Remove(InsertReplaceContenidos.NumUTF16TextChars()-1, 1);
					
					InsertReplaceContenidos.Append(");");
					
				}
				
				SQLInterface->SQLSetInDataBase(StringConectionHemeroteca,InsertReplaceContenidos);
			}
			
			//Agrega notas a BD de Hemeroteca
			Busqueda="(SELECT ee.id_elemento, ee.id_color, ee.id_Publicacion, ee.Id_Seccion, ee.id_tipo_ele, ee.Id_elemento_padre,ee.Id_Fuente, ee.Nombre, ee.Servidor, ee.Ruta_Elemento, ee.Nombre_Archivo, ee.Fecha_Creacion, ee.Fecha_ult_mod, ee.Dirigido_a, ee.Proveniente_de, ee.Estatus_Colocado, ee.Calificacion, ee.Redimensionamiento, ee.Coor_top, ee.Coor_left, ee.Coor_right, ee.Coor_bottom, ee.Lineas_faltantes, ee.Lineas_restantes, ee.Caracteres_faltantes, ee.Caracteres_restantes, ee.Caracteres, ee.PaginaColocada, ee.Mostrar_Indesign, ee.Fecha_de_Edicion, ee.Creado_Por, ee.LastIdEstatusNota FROM elemento ee INNER JOIN  elemento_por_pagina p on p.id_elemento=ee.id_elemento AND p.Pagina_ID="+N2PID_Pagina+") UNION"+
			salto+"(SELECT DISTINCT ee.id_elemento, ee.id_color, ee.id_Publicacion, ee.Id_Seccion, ee.id_tipo_ele, ee.Id_elemento_padre,ee.Id_Fuente, ee.Nombre, ee.Servidor, ee.Ruta_Elemento, ee.Nombre_Archivo, ee.Fecha_Creacion, ee.Fecha_ult_mod, ee.Dirigido_a, ee.Proveniente_de, ee.Estatus_Colocado, ee.Calificacion, ee.Redimensionamiento, ee.Coor_top, ee.Coor_left, ee.Coor_right, ee.Coor_bottom, ee.Lineas_faltantes, ee.Lineas_restantes, ee.Caracteres_faltantes, ee.Caracteres_restantes, ee.Caracteres, ee.PaginaColocada, ee.Mostrar_Indesign, ee.Fecha_de_Edicion, ee.Creado_Por, ee.LastIdEstatusNota FROM elemento e INNER JOIN elemento_por_pagina p on p.id_elemento=e.id_elemento INNER JOIN elemento ee on ee.id_elemento_padre=e.id_elemento AND p.Pagina_ID="+N2PID_Pagina+") ORDER BY id_elemento ASC";
			
			K2Vector<PMString> ElementosEnPagina;
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,ElementosEnPagina);
			if(ElementosEnPagina.Length()>0)
			{
				
				for(int32 i=0;i<ElementosEnPagina.Length();i++)
				{
					PMString InsertReplaceElementos="";
					InsertReplaceElementos.Append("REPLACE INTO elemento(");
					if(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("id_elemento,");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_color",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("id_color,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_Publicacion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("id_Publicacion,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Id_Seccion,");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_tipo_ele",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("id_tipo_ele,");
					}
					PMString Imagenrutaftpjpg="";
					//Envio FTP Imagen de Nota
					if(SQLInterface->ReturnItemContentbyNameColumn("id_tipo_ele",ElementosEnPagina[i])=="4")
					{
						//CAlert::InformationAlert(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ElementosEnPagina[i])+","+SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",ElementosEnPagina[i]));
						if(SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
						{
							
							PMString pathserverunix=InterlasaUtilities::MacToUnix("C:"+Connection.PathOfServerFileImages);
							
							Rutajpg="/Volumes"+pathserverunix+""+SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",ElementosEnPagina[i]);
							
							PMString target= " "; 
							PMString replaced="\\ ";
							N2PSQLUtilities::ReplaceAllOcurrencias(Rutajpg,target, replaced);

							//CAlert::InformationAlert(Rutajpg);
							rutaftpjpg="'/"+nombrePublicacion+"'";
							shpathMkDirJPGUnix=InterlasaUtilities::MacToUnix(shpathMkDirFTP);
							CommandStr="sh "+shpathMkDirJPGUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftpjpg;
							N2PSQLUtilities::ImprimeMensaje(CommandStr);
							system(CommandStr.GrabCString());
							//CAlert::InformationAlert("333");
							rutaftpjpg="'/"+nombrePublicacion+"/"+fechaPublicacion+"'";
							CommandStr="sh "+shpathMkDirJPGUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftpjpg;
							N2PSQLUtilities::ImprimeMensaje(CommandStr);
							system(CommandStr.GrabCString());
							//CAlert::InformationAlert("444");
							rutaftpjpg="'/"+nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"'";
							CommandStr="sh "+shpathMkDirJPGUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+rutaftpjpg;
							N2PSQLUtilities::ImprimeMensaje(CommandStr);
							system(CommandStr.GrabCString());
							//CAlert::InformationAlert("555");
							rutaftpjpg="'/"+nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"/"+SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ElementosEnPagina[i])+".jpg"+"'";
							
							Imagenrutaftpjpg=nombrePublicacion+"/"+fechaPublicacion+"/"+Nombre_Archivo+"/"+SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ElementosEnPagina[i])+".jpg";
							//CAlert::InformationAlert("666");
							if(ExisteArchivo)
							{
							
							
								N2PSQLUtilities::ImprimeMensaje(Rutajpg);
								PMString RutajpgUnix=Rutajpg;///InterlasaUtilities::MacToUnix(Rutajpg);
								N2PSQLUtilities::ImprimeMensaje(RutajpgUnix);
								CommandStr="sh "+ShellScriptPathUnix+" "+serverftp+" "+userftp+" "+pwdftp+" "+RutajpgUnix+" "+rutaftpjpg;
							
								N2PSQLUtilities::ImprimeMensaje(CommandStr);
							
								system(CommandStr.GrabCString());
								//CAlert::InformationAlert("Se envio el jpg aftp");
							}
						}
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_elemento_padre",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Id_elemento_padre,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_Fuente",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Id_Fuente,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Nombre",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Nombre,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Servidor",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Servidor,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Ruta_Elemento,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Nombre_Archivo,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Fecha_Creacion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Fecha_Creacion,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Fecha_ult_mod",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Fecha_ult_mod,");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Dirigido_a,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Proveniente_de",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Proveniente_de,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Estatus_Colocado,");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Calificacion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Calificacion,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Redimensionamiento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Redimensionamiento,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_top",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Coor_top,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_left",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Coor_left,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_right",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Coor_right,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_bottom",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Coor_bottom,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Lineas_faltantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Lineas_faltantes,");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Lineas_restantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Lineas_restantes,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Caracteres_faltantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Caracteres_faltantes,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Caracteres_restantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Caracteres_restantes,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Caracteres",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Caracteres,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("PaginaColocada",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("PaginaColocada,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Mostrar_Indesign",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Mostrar_Indesign,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Fecha_de_Edicion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Fecha_de_Edicion,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Creado_Por",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("Creado_Por,");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("LastIdEstatusNota",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("LastIdEstatusNota,");
					}
					
					InsertReplaceElementos.Remove(InsertReplaceElementos.NumUTF16TextChars()-1, 1);
					/////////////////////////
					//////////////////////////
					//////////////////////////
					InsertReplaceElementos.Append(") VALUES(");
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("id_elemento",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_color",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("id_color",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_Publicacion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("id_Publicacion",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("id_tipo_ele",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("id_tipo_ele",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_elemento_padre",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Id_elemento_padre",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Id_Fuente",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Id_Fuente",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Nombre",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Nombre",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Servidor",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Servidor",ElementosEnPagina[i])+"',");
					}
					
					//CAlert::InformationAlert("Ruta A "+SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",ElementosEnPagina[i]));
					if(SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						//CAlert::InformationAlert("Ruta XXXX "+Imagenrutaftpjpg);
						InsertReplaceElementos.Append("'"+Imagenrutaftpjpg+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Fecha_Creacion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Fecha_Creacion",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Fecha_ult_mod",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Fecha_ult_mod",ElementosEnPagina[i])+"',");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Proveniente_de",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Proveniente_de",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",ElementosEnPagina[i])+",");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Calificacion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Calificacion",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Redimensionamiento",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Redimensionamiento",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_top",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Coor_top",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_left",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Coor_left",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_right",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Coor_right",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Coor_bottom",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Coor_bottom",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Lineas_faltantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Lineas_faltantes",ElementosEnPagina[i])+",");
					}
					
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Lineas_restantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Lineas_restantes",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Caracteres_faltantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Caracteres_faltantes",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Caracteres_restantes",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Caracteres_restantes",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Caracteres",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Caracteres",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("PaginaColocada",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("PaginaColocada",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Mostrar_Indesign",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("Mostrar_Indesign",ElementosEnPagina[i])+",");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Fecha_de_Edicion",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Fecha_de_Edicion",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("Creado_Por",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append("'"+SQLInterface->ReturnItemContentbyNameColumn("Creado_Por",ElementosEnPagina[i])+"',");
					}
					
					if(SQLInterface->ReturnItemContentbyNameColumn("LastIdEstatusNota",ElementosEnPagina[i]).NumUTF16TextChars()>0)
					{
						InsertReplaceElementos.Append(SQLInterface->ReturnItemContentbyNameColumn("LastIdEstatusNota",ElementosEnPagina[i])+",");
					}
					
					InsertReplaceElementos.Remove(InsertReplaceElementos.NumUTF16TextChars()-1, 1);
					InsertReplaceElementos.Append(" );");
					N2PSQLUtilities::ImprimeMensaje(InsertReplaceElementos);
					SQLInterface->SQLSetInDataBase(StringConectionHemeroteca,InsertReplaceElementos);
				}
				
				//CAlert::InformationAlert(InsertReplaceElementos); 
			}
			
			
			N2PSQLUtilities::FinalizaDebugeo();

			//Agrega multimedias a BD de Hemeroteca
		}
		
	}while(false);
}



