/*
 *  N2PHemerotecaPanelCreator.cpp
 *  N2PSQL
 *
 *  Created by Fernando  Llanas on 13/09/10.
 *  Copyright 2010 Interlasa. All rights reserved.
 *
 */

/*
 *  N2PN2PHemerotecaPanelCreator.cpp
 *  N2PSQL
 *
 *  Created by Fernando  Llanas on 12/09/10.
 *  Copyright 2010 Interlasa. All rights reserved.
 *
 */

//========================================================================================
//  
//  $File: //depot/indesign_6.0/gm/source/sdksamples/basicselectabledialog/N2PHemerotecaPanelCreator.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2008/08/18 16:29:43 $
//  
//  $Revision: #1 $
//  
//  $Change: 643585 $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "cpanelcreator.h"

// Project includes:
#include "N2PsqlID.h"

/** We override CPanelCreator::GetPanelRsrcID() so we 
 can provide a specific service ID and resource ID for our panel.
 This is one of the key components that allows the switching mechanism.
 This implements IPanelCreator, and uses the partial implementation CPanelCreator.
 @see GetPanelRsrcID
 @ingroup basicselectabledialog
 
 */
class N2PHemerotecaPanelCreator : public CPanelCreator
	{
	public:
		/**	Constructor.
		 @param boss IN interface ptr from boss object on which this interface is aggregated.
		 */
		N2PHemerotecaPanelCreator(IPMUnknown* boss) : CPanelCreator(boss) {}
		
		/** Destructor.
		 */
		virtual ~N2PHemerotecaPanelCreator() {}
		
		/** Returns the resource ID of the panel that is used in finding the
		 service IDs resource and the panel view resource.
		 Called by CPanelCreator::GetServiceIDs and CPanelCreator::GetPanelViewRsrcID.
		 @return resource ID containing service IDs resource and panel view resource.
		 */
		virtual RsrcID GetPanelRsrcID() const;
	};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
 */
CREATE_PMINTERFACE(N2PHemerotecaPanelCreator, kN2PHemerotecaPanelCreatorImpl)

/* GetPanelRsrcID
 */
RsrcID N2PHemerotecaPanelCreator::GetPanelRsrcID() const
{
	return kN2PHemerotecaPanelCreatorResourceID;
}

// End, N2PHemerotecaPanelCreator.cpp



