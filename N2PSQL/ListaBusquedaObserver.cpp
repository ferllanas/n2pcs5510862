/*
//	File:	N2PsqlListaBusquedaObserver.cpp
//
//	Date:	5-Mar-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
*/
#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "IApplication.h"

// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"

#include "N2PsqlID.h"
#include "N2PSQLListBoxHelper.h"
#include "N2PSQLUtilities.h"


/**

	Implements IObserver. The intent of this class is to provide handling for the AutoAttach (sent when shown), AutoDetach (hidden)
	and Update (when listbox hit by end-user, for instance) message
	
	 The class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class implements the IObserver interface using the CObserver helper class,
	and is listening along the IID_ILISTCONTROLDATA protocol for changes in the list-data model.

	@author Ian Paterson
	@version API Level 1.5
*/
class N2PsqlListaBusquedaObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	N2PsqlListaBusquedaObserver(IPMUnknown *boss);

	/**
		Destructor for N2PsqlListaBusquedaObserver class - 
		performs cleanup 
	*/	
	~N2PsqlListaBusquedaObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 

		This class is interested in changes along IID_ILISTCONTROLDATA protocol with classID of
		kListSelectionChangedByUserMessage. This message is sent when a user clicks on an element
		in the list-box.

	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	void updateListBox(bool16 isAttaching);
	
	
	
	
};

CREATE_PMINTERFACE(N2PsqlListaBusquedaObserver, kN2PsqlListaResultadoBoxObserverImpl)


N2PsqlListaBusquedaObserver::N2PsqlListaBusquedaObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


N2PsqlListaBusquedaObserver::~N2PsqlListaBusquedaObserver()
{
	
}


void N2PsqlListaBusquedaObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());// IID_ISUBJECT);
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
	//updateListBox(kTrue);
}


void N2PsqlListaBusquedaObserver::AutoDetach()
{
	//updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());//IID_ISUBJECT);
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}


void N2PsqlListaBusquedaObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) {
		do {
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			ASSERT(app);
			if (app == nil) {	
				break;
			}
		
			PMString nameApp = app->GetApplicationName();
		
			N2PSQLListBoxHelper listHelper(this, kN2PsqlPluginID,kN2PsqlNotasListBoxWidgetID);
			IControlView * listBox = listHelper.FindCurrentListBox();
			if(listBox == nil) {
				break;
			}
			InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER); // kDefaultIID not def'd
			ASSERT_MSG(listCntl != nil, "listCntl nil");
			if(listCntl == nil) {
				break;
			}
			//  Get the first item in the selection (should be only one if the list box
			// has been set up correctly in the framework resource file)
			PMString IdElemPadre="";
			PMString Seccion="";
			PMString Estado="";
			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;
			const int kSelectionLength =  multipleSelection.Length() ;
			if (kSelectionLength> 0 )
			{
				
			int indexSelected = multipleSelection[0];
			listHelper.ObtenerTextoDeSeleccionActual(kN2PsqlIDNotaLabelWidgetID,IdElemPadre,kN2PsqlSeccionLabelWidgetID,Seccion,indexSelected);
			
			
			PMString Busqueda= "CALL QueryElementdNotas2(" + IdElemPadre +")";
			N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kFalse);
			
			//CAlert::InformationAlert("AAA");
			//Para ver si existen 
			Busqueda=" SELECT Id_Elemento  ";
 			Busqueda.Append(" FROM Elemento  Where Id_elemento_padre=");
 			Busqueda.Append(IdElemPadre);
 			Busqueda.Append(" AND Id_tipo_ele=4");
 			if(nameApp.Contains("InDesign"))
 			{
 				Busqueda.Append(" AND Mostrar_Indesign=1");
 			}
 			N2PSQLUtilities::MuestraIconodeCamaraSiNotaTieneFotos(Busqueda);
 			//CAlert::InformationAlert("BBB");
 			
 			Busqueda="CALL N2PV1055_QueryFotoFromElemPadre (" + IdElemPadre + ")" ;
 			N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto(Busqueda);
 			//CAlert::InformationAlert("CCC");
/*	
			PMString Busqueda="DECLARE @ID_Elemento_Titulo varchar(30)";
 			Busqueda.Append("DECLARE @ID_Elemento_Contenido varchar(30)");
 			Busqueda.Append("DECLARE @ID_Elemento_Balazo varchar(30)");
			Busqueda.Append("DECLARE @ID_Elemento_PieFoto varchar(30)");
			
			Busqueda.Append("DECLARE @ID_Seccion varchar(30)");
			Busqueda.Append("DECLARE @ID_Publicacion varchar(30)");
			Busqueda.Append("DECLARE @ID_Fecha_Edicion varchar(30)");
			Busqueda.Append("DECLARE @ID_Nombre_Archivo varchar(30)");
			Busqueda.Append("DECLARE @ID_Id_Estatus varchar(30)");
			Busqueda.Append("DECLARE @Folio_Pagina varchar(30)");
			
			//De tabla elemento
			Busqueda.Append("SELECT @ID_Elemento_Titulo =(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='" + IdElemPadre + "' AND Id_tipo_ele='IdElemPadre')");
			Busqueda.Append("SELECT @ID_Elemento_Contenido =(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='" + IdElemPadre + "' AND Id_tipo_ele='ContentNota')");
			Busqueda.Append("SELECT @ID_Elemento_PieFoto =(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='" + IdElemPadre + "' AND Id_tipo_ele='PieFoto')");
			Busqueda.Append("SELECT @ID_Elemento_Balazo =(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='" + IdElemPadre + "' AND Id_tipo_ele='Balazo')");
			//De tabla Elemento_por_pagina
			Busqueda.Append("SELECT @Folio_Pagina = (SELECT TOP 1 Folio_pagina FROM Elemento_por_p�gina WHERE Id_Elemento='" + IdElemPadre + "' )");
			Busqueda.Append("SELECT @ID_Seccion = (SELECT TOP 1 Id_Seccion FROM Elemento_por_p�gina WHERE Id_Elemento='" + IdElemPadre + "' )");
			Busqueda.Append("SELECT @ID_Publicacion = (SELECT TOP 1 Id_Publicacion FROM Elemento_por_p�gina WHERE Id_Elemento='" + IdElemPadre + "' )");
			Busqueda.Append("SELECT @ID_Fecha_Edicion = (SELECT TOP 1 Fecha_Edicion FROM Elemento_por_p�gina WHERE Id_Elemento='" + IdElemPadre + "' )");
			//De tablaPagina
			Busqueda.Append("SELECT @ID_Nombre_Archivo = (SELECT  TOP 1 Nombre_Archivo FROM Pagina WHERE Id_Elemento=Folio_Pagina ORDER BY Fecha DESC)");
			//De tablaPagina
			Busqueda.Append("SELECT @ID_Id_Estatus = (SELECT TOP 1 Id_Estatus FROM Bitacora_Elemento WHERE Id_Elemento='" + IdElemPadre + "' ORDER BY Fecha DESC)");
			
			
			Busqueda.Append("SELECT @ID_Id_Estatus AS Id_Estatus, @ID_Nombre_Archivo AS Id_Estatus, @ID_Fecha_Edicion AS Fecha_Edicion, @ID_Publicacion As Id_Publicacion, @ID_Seccion AS Id_Seccion, Contenido.Id_Elemento,Contenido.Contenido, Elemento.Id_tipo_ele,Elemento.Fecha_ult_mod FROM Contenido,Elemento WHERE (Contenido.Id_Elemento=@ID_Elemento_Titulo AND Contenido.Id_Elemento=Elemento.Id_Elemento) ");
			Busqueda.Append("OR (Contenido.Id_Elemento=@ID_Elemento_Contenido AND Contenido.Id_Elemento=Elemento.Id_Elemento) ");
			Busqueda.Append("OR (Contenido.Id_Elemento=@ID_Elemento_PieFoto AND Contenido.Id_Elemento=Elemento.Id_Elemento) ");
			Busqueda.Append("OR (Contenido.Id_Elemento=@ID_Elemento_Balazo AND Contenido.Id_Elemento=Elemento.Id_Elemento)");
*/
			/*PMString Busqueda=" IF EXISTS(SELECT * FROM Elemento_por_p�gina WHERE Id_Elemento='";
			Busqueda.Append(IdElemPadre);
			Busqueda.Append("')");
			Busqueda.Append(" begin ");
				Busqueda.Append(" if EXISTS(SELECT * FROM Bitacora_Elemento WHERE Id_Elemento='");
				Busqueda.Append(IdElemPadre);
				Busqueda.Append("')");
				Busqueda.Append(" begin ");
					
					Busqueda.Append(" SELECT   Contenido.Id_Elemento, Elemento.Id_elemento_padre, Elemento_por_p�gina.Fecha_Edicion,"); 
					Busqueda.Append(" Elemento_por_p�gina.Id_Publicacion, Elemento_por_p�gina.Id_Seccion,");
					Busqueda.Append(" CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod,Elemento.Id_tipo_ele, ");
 					Busqueda.Append(" Pagina.Nombre_Archivo, Bitacora_Elemento.Id_Estatus,Contenido.Contenido ");

 					Busqueda.Append(" FROM Elemento,Pagina,Bitacora_Elemento, Elemento_por_p�gina, Contenido");

					Busqueda.Append(" WHERE Elemento.Id_Elemento ");
 					Busqueda.Append(" IN(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='");
					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
 					Busqueda.Append(" AND Elemento.Id_elemento_padre=Bitacora_Elemento.Id_Elemento AND Elemento.Id_Elemento=Contenido.Id_Elemento");
 					Busqueda.Append(" AND Pagina.Folio_Pagina=Elemento_por_p�gina.Folio_Pagina");
					Busqueda.Append(" AND Elemento_por_p�gina.Id_Elemento=Elemento.Id_elemento_padre");
				Busqueda.Append(" end ");
 				Busqueda.Append(" ELSE ");
 				Busqueda.Append(" begin ");
					
 					Busqueda.Append(" SELECT  Contenido.Id_Elemento, Elemento.Id_elemento_padre, Elemento_por_p�gina.Fecha_Edicion, ");
					Busqueda.Append(" Elemento_por_p�gina.Id_Publicacion, Elemento_por_p�gina.Id_Seccion,");
					Busqueda.Append("CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod,Elemento.Id_tipo_ele, ");
 					Busqueda.Append(" Pagina.Nombre_Archivo,  Contenido.Contenido");
					Busqueda.Append(" FROM Elemento, Pagina, Elemento_por_p�gina,Contenido");
					Busqueda.Append(" WHERE Elemento.Id_Elemento ");
					Busqueda.Append(" IN(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='");
					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
					Busqueda.Append(" AND Elemento.Id_Elemento=Contenido.Id_Elemento And Pagina.Folio_Pagina=Elemento_por_p�gina.Folio_Pagina");
					Busqueda.Append(" AND Elemento_por_p�gina.Id_Elemento=Elemento.Id_elemento_padre");
				Busqueda.Append(" end ");
			Busqueda.Append(" end ");	
			Busqueda.Append(" ELSE ");
			Busqueda.Append(" begin ");
				Busqueda.Append(" if EXISTS(SELECT * FROM Bitacora_Elemento WHERE Id_Elemento='");
				Busqueda.Append(IdElemPadre);
				Busqueda.Append("')");
				Busqueda.Append(" begin ");
 					
					Busqueda.Append(" SELECT   Contenido.Id_Elemento, Elemento.Id_elemento_padre, CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod, Elemento.Id_tipo_ele,");
 					Busqueda.Append(" Contenido.Contenido, Bitacora_Elemento.Id_Estatus");
					Busqueda.Append(" FROM Elemento,Bitacora_Elemento, Contenido");
					Busqueda.Append(" WHERE Elemento.Id_Elemento IN(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='");
					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
					Busqueda.Append("AND Elemento.Id_elemento_padre=Bitacora_Elemento.Id_Elemento AND Elemento.Id_Elemento=Contenido.Id_Elemento");
 			
				Busqueda.Append(" end ");
				Busqueda.Append(" else ");
				Busqueda.Append(" begin ");
					Busqueda.Append(" SELECT   Contenido.Id_Elemento, Elemento.Id_elemento_padre,CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod, Elemento.Id_tipo_ele,");
 					Busqueda.Append(" Contenido.Contenido");
 					Busqueda.Append(" FROM Elemento, Contenido");
 					Busqueda.Append(" WHERE Elemento.Id_Elemento IN(SELECT Id_Elemento FROM Elemento WHERE");
 					Busqueda.Append(" Id_elemento_padre='");
 					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
 					Busqueda.Append(" AND Elemento.Id_Elemento=Contenido.Id_Elemento");
 				Busqueda.Append(" end ");
 			Busqueda.Append(" end ");*/
			
			
			////////////////////
		/*	PMString Busqueda=" IF EXISTS(SELECT * FROM Elemento_por_p�gina WHERE Id_Elemento='";
			Busqueda.Append(IdElemPadre);
			Busqueda.Append("')");
			Busqueda.Append(" begin ");
				Busqueda.Append(" if EXISTS(SELECT * FROM Bitacora_Elemento WHERE Id_Elemento='");
				Busqueda.Append(IdElemPadre);
				Busqueda.Append("')");
				Busqueda.Append(" begin ");
					
					Busqueda.Append(" SELECT   Contenido.Id_Elemento, Elemento.Id_elemento_padre, Elemento_por_p�gina.Fecha_Edicion,"); 
					Busqueda.Append(" Elemento_por_p�gina.Id_Publicacion, Elemento_por_p�gina.Id_Seccion,");
					Busqueda.Append(" CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod,Elemento.Id_tipo_ele, ");
 					Busqueda.Append(" Pagina.Nombre_Archivo, Bitacora_Elemento.Id_Estatus,Contenido.Contenido ");

 					Busqueda.Append(" FROM Elemento,Pagina,Bitacora_Elemento, Elemento_por_p�gina, Contenido");

					Busqueda.Append(" WHERE Elemento.Id_Elemento ");
 					Busqueda.Append(" IN(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='");
					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
 					Busqueda.Append(" AND Elemento.Id_elemento_padre=Bitacora_Elemento.Id_Elemento AND Elemento.Id_Elemento=Contenido.Id_Elemento");
 					Busqueda.Append(" AND Pagina.Folio_Pagina=Elemento_por_p�gina.Folio_Pagina");
					Busqueda.Append(" AND Elemento_por_p�gina.Id_Elemento=Elemento.Id_elemento_padre");
				Busqueda.Append(" end ");
 				Busqueda.Append(" ELSE ");
 				Busqueda.Append(" begin ");
					
 					Busqueda.Append(" SELECT  Contenido.Id_Elemento, Elemento.Id_elemento_padre, Elemento_por_p�gina.Fecha_Edicion, ");
					Busqueda.Append(" Elemento_por_p�gina.Id_Publicacion, Elemento_por_p�gina.Id_Seccion,");
					Busqueda.Append("CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod,Elemento.Id_tipo_ele, ");
 					Busqueda.Append(" Pagina.Nombre_Archivo,  Contenido.Contenido");
					Busqueda.Append(" FROM Elemento, Pagina, Elemento_por_p�gina,Contenido");
					Busqueda.Append(" WHERE Elemento.Id_Elemento ");
					Busqueda.Append(" IN(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='");
					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
					Busqueda.Append(" AND Elemento.Id_Elemento=Contenido.Id_Elemento And Pagina.Folio_Pagina=Elemento_por_p�gina.Folio_Pagina");
					Busqueda.Append(" AND Elemento_por_p�gina.Id_Elemento=Elemento.Id_elemento_padre");
				Busqueda.Append(" end ");
			Busqueda.Append(" end ");	
			Busqueda.Append(" ELSE ");
			Busqueda.Append(" begin ");
				Busqueda.Append(" if EXISTS(SELECT * FROM Bitacora_Elemento WHERE Id_Elemento='");
				Busqueda.Append(IdElemPadre);
				Busqueda.Append("')");
				Busqueda.Append(" begin ");
 					
					Busqueda.Append(" SELECT   Contenido.Id_Elemento, Elemento.Id_elemento_padre, CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod, Elemento.Id_tipo_ele,");
 					Busqueda.Append(" Contenido.Contenido, Bitacora_Elemento.Id_Estatus");
					Busqueda.Append(" FROM Elemento,Bitacora_Elemento, Contenido");
					Busqueda.Append(" WHERE Elemento.Id_Elemento IN(SELECT Id_Elemento FROM Elemento WHERE Id_elemento_padre='");
					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
					Busqueda.Append("AND Elemento.Id_elemento_padre=Bitacora_Elemento.Id_Elemento AND Elemento.Id_Elemento=Contenido.Id_Elemento");
 			
				Busqueda.Append(" end ");
				Busqueda.Append(" else ");
				Busqueda.Append(" begin ");
					Busqueda.Append(" SELECT   Contenido.Id_Elemento, Elemento.Id_elemento_padre,CONVERT(varchar,Elemento.Fecha_ult_mod,121) AS Fecha_ult_mod, Elemento.Id_tipo_ele,");
 					Busqueda.Append(" Contenido.Contenido");
 					Busqueda.Append(" FROM Elemento, Contenido");
 					Busqueda.Append(" WHERE Elemento.Id_Elemento IN(SELECT Id_Elemento FROM Elemento WHERE");
 					Busqueda.Append(" Id_elemento_padre='");
 					Busqueda.Append(IdElemPadre);
					Busqueda.Append("')");
 					Busqueda.Append(" AND Elemento.Id_Elemento=Contenido.Id_Elemento");
 				Busqueda.Append(" end ");
 			Busqueda.Append(" end ");*/
 			
 				
			}

		} while(0);
	}
}


 
void N2PsqlListaBusquedaObserver::updateListBox(bool16 isAttaching)
{
	N2PSQLListBoxHelper listHelper(this, kN2PsqlPluginID,kN2PsqlNotasListBoxWidgetID);
	listHelper.EmptyCurrentListBox();
	// See if we can find the  listbox
	// If we can then populate the listbox in brain-dead way
	if(isAttaching) {
		do {
				const int targetDisplayWidgetId = kN2PsqlNotaLabelWidgetID;
				const int kNumberItems = 12;
				for(int i=0; i < kNumberItems; i++) {
					PMString name("mio");
					name.AppendNumber(i+1);
					listHelper.AddElement(name, targetDisplayWidgetId);
				}
		   } while(0);
	}
}




