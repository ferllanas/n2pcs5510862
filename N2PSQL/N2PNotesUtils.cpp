//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/browsenotes/N2PNotesUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: tomdonov $
//  
//  $DateTime: 2005/03/18 15:32:27 $
//  
//  $Revision: #2 $
//  
//  $Change: 326839 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPanelControlData.h"
#include "ITextModel.h"
#include "INoteUtils.h"
#include "INoteData.h"
#include "INoteDataUtils.h"
#include "INoteSuiteUtils.h"
#include "IStoryList.h"
#include "IControlView.h"
#include "IDocument.h"
#include "ISelectionUtils.h"
#include "IActiveContext.h"
#include "IListBoxController.h"
#include "IListControlDataOf.h"
#include "ICreateNoteCmdData.h"
#include "IIntData.h"
#include "NoteID.h"
#include "LayoutUIID.h"
// Implementation includes:
#include "IPalettePanelUtils.h"

// Project includes:
#include "CAlert.h"
#include "N2PNotesStorage.h"
#include "N2PsqlID.h"
#include "IN2PNotesUtils.h"
#include "N2PSQLUtilities.h"

/**
	Utility for updating info in the BrowseNotes panel and
	navigating to selected note in story.
	
	@ingroup browsenotes
	@ingroup sdk_incopy
*/

class N2PNotesUtils : public CPMUnknown<IN2PNotesUtils>
{
public:

	/** Constructor.
	*/
	N2PNotesUtils(IPMUnknown* boss);

	/** Destructor.
	*/
	~N2PNotesUtils();

	/**
		Helper to change state of entire listbox.
		@param listHelper (in) specifies reference to listbox helper.
		@param doc (in) specifies document to be parsed for notes.
		@param delStoryList specifies list of stories to be excluded from listbox.
		@param noteAnchorPos specifies anchor index of a note that has changed.
		
	virtual void UpdateListBox(
		SDKListBoxHelper& listHelper, 
		IDocument* doc,
		UIDList delStoryList, 
		TextIndex noteAnchorPos);
*/	
	/**
		Helper to ease process of manipulating a listbox. 
		@param widgetID (in) specifies desired widget.
		@return ???
	*/
	virtual IControlView* GetAssociatedWidget(WidgetID widgetID);

	/**
		Helper to find note in text that corresponds to selected listbox element. 
		@param iNote (in) specifies index of selected listbox element.
	*/
	virtual void GoToSpecifiedNote(int32 iNote);

	/**
		Helper to set note sort criterion. 
		@param sortBy (in) specifies sort criterion.
	*/
	virtual void SetSortBy(int32 sortBy) { fSortBy = sortBy; }

	/**
		Helper to get note sort criterion. 
		@return sort criterion enum.
	*/
	virtual int32 GetSortBy() { return fSortBy; }
	
	/*
	*/
	virtual void GetNotesOnElementArticle(K2Vector<N2PNotesStorage*>& fN2PNotesStore,const UIDRef& StoryElementOfArticle);
	
	virtual ErrorCode AddNotesToElementArticle(ITextModel* mytextmodel, const  K2Vector<N2PNotesStorage*>& fBNotesStore);

private:

	/**
		Helper to iterate story for Note info.
	*/
	void GetNoteInfo(int32& noteCount, UIDRef& docRef);

	/** Notes storage structure defined in N2PNotesStorage.h.	*/
	K2Vector<N2PNotesStorage*> fBNotesStore;

	/** Stores sort preference.	*/
	int32 fSortBy;
};

CREATE_PMINTERFACE(N2PNotesUtils, kN2PNotesUtilsImpl)


/** Used to sort notes list by note author.
	
	@ingroup browsenotes
	@ingroup sdk_incopy
 */
struct CompareAuthors
{
	/** Compares the authors of two N2PNotesStorage objects.
	 * 	@param first IN The first (or LHS) N2PNotesStorage in the comparison.
	 * 	@param second IN The second (or RHS) N2PNotesStorage in the comparison.
	 * 	@return kTrue if first is later than second. kFalse otherwise.
	 */
	inline bool operator()(N2PNotesStorage* first, N2PNotesStorage* second)
	{
		if (first->fauthor < second->fauthor)
			return true;
		else
			return false;
	}
};


/** Used to sort notes list by note last modified.
	
	@ingroup browsenotes
	@ingroup sdk_incopy
 */
struct CompareDateTime
{
	/** Compares the date and time of two N2PNotesStorage objects.
	 * 	@param first IN The first (or LHS) N2PNotesStorage in the comparison.
	 * 	@param second IN The second (or RHS) N2PNotesStorage in the comparison.
	 * 	@return kTrue if first is later than second.  kFalse otherwise.
	 */
	inline bool operator()(N2PNotesStorage* first, N2PNotesStorage* second)
	{
		if (first->flastmodified < second->flastmodified)
			return false;
		else
			return true;
	}
};


/** Constructor.
*/
N2PNotesUtils::N2PNotesUtils(IPMUnknown* boss) :
	CPMUnknown<IN2PNotesUtils>(boss)
{
	fSortBy = 0;
}

	
/** Destructor.
*/
N2PNotesUtils::~N2PNotesUtils()
{
	for (int i = 0; i < fBNotesStore.size(); i++)
	{
		if (fBNotesStore[i] != nil)
		{
			delete fBNotesStore[i];
			fBNotesStore[i] = nil;
		}
	}
}


/* UpdateListBox

void N2PNotesUtils::UpdateListBox(
	SDKListBoxHelper& listHelper, 
	IDocument* doc,
	UIDList delStoryList, 
	TextIndex noteAnchorPos)
{
	int32 nStories = 0;
	int lbIndex = -1;

	do
	{
		// Empty all listbox entries.
		listHelper.EmptyCurrentListBox();

		// Does this doc have any stories? If not, fail gracefully.
		InterfacePtr<IStoryList> storyList(doc, IID_ISTORYLIST);
		if(storyList == nil)
		{
			break;
		}

		// Release custom notes storage memory.
		for (int n = 0; n < fBNotesStore.size(); n++)
		{
			if (fBNotesStore[n] != nil)
			{
				delete fBNotesStore[n];
				fBNotesStore[n] = nil;
			}
		}

		// Clear the storage list.
		fBNotesStore.Clear();

		// Iterate the story list
		nStories = storyList->GetUserAccessibleStoryCount();
		for (int i=0; i < nStories; i++)
		{
			bool16 bFound = kFalse;
			int32 nNotes = 0;

			// Get ith storyRef.
			UIDRef storyRef = storyList->GetNthUserAccessibleStoryUID(i);
			// See whether ith storyRef is in list of deleted storyRefs...
			for(int j=0; j<delStoryList.Length(); j++)
			{
				// ...if it is found, do nothing...
				if(storyRef == delStoryList.GetRef(j))
				{
					// mark as found
					bFound = kTrue;
					// fast-forward
					j = delStoryList.Length();
				}
			}

			// ...else, add its note info to our custom storage.
			if(bFound == kFalse)
			{
				Utils<INoteDataUtils>()->GetStoryNoteCount(storyRef, nNotes);

				// Parse ith story for note content and load storage.
				this->GetNoteInfo(nNotes, storyRef);
			}
		}  // note info storage is now complete.

		// if kByUserName, sort note storage based on note author order.
		if (this->GetSortBy() == kByUserName)
		{
			std::sort(fBNotesStore.begin(), fBNotesStore.end(), CompareAuthors());
		}

		// If kByDateTime, sort note storage based on note date/time last-modified order.
		else if (this->GetSortBy() == kByDateTime)
		{
			std::sort(fBNotesStore.begin(), fBNotesStore.end(), CompareDateTime());
		}

		// Populate listbox widget elements with respective note text.
		for (int j=0; j < fBNotesStore.size(); j++)
		{
			// If a listbox element changed due to a note edit, find its index
			// so we can restore it to view after the entire listbox refreshes.
			if(fBNotesStore[j]->findex == noteAnchorPos)
			{
				lbIndex = j;
			}
			listHelper.AddElement(fBNotesStore[j]->ftext, kBNotesTextWidgetID, j);
		}

		// If update was not triggered by an edit within a note, we can break.
		if(lbIndex != -1)
		{
			break;
		}

		// Otherwise, scroll changed listbox element back into view.
		IControlView* listBox = listHelper.FindCurrentListBox();
		if(listBox == nil)
		{
			break;
		}
		InterfacePtr< IListControlDataOf<IControlView*> > 
			lcData(listBox, UseDefaultIID());
		if(lcData == nil)
		{ 
			break;
		}
		InterfacePtr<IListBoxController> 
			controller(lcData, IID_ILISTBOXCONTROLLER);
		if (controller == nil)
		{ 
			break;
		}
		controller->ScrollItemIntoView(lbIndex);

	} while(kFalse);
}
*/

/* GetNoteInfo
*/
void N2PNotesUtils::GetNoteInfo(int32& noteCount, UIDRef& storyRef)
{
	do
	{
		InterfacePtr<ITextModel> textModel(storyRef, UseDefaultIID());
		if (textModel == nil)
		{
			// No story, so we leave.
			break;
		}

		int32 storyLength = textModel->GetPrimaryStoryThreadSpan() - 1;
	
		// If story is not empty...
		if (storyLength >= 1)
		{
			OwnedItemDataList ownedItemList;
			
			Utils<ITextUtils> textUtils;
			ASSERT(textUtils);
			if (!textUtils) {
				break;
			}
			
			textUtils->CollectOwnedItems(textModel, 0, storyLength, &ownedItemList);
			//Text::CollectOwnedItems(textModel, 0, storyLength, &ownedItemList);

			// Iterate the Notes
			for (int32 i = 0; i < noteCount; ++i)
			{
				UID noteUID = ownedItemList[i].fUID;
				UIDRef noteUIDRef(storyRef.GetDataBase(), noteUID);
				InterfacePtr<INoteData> noteData(noteUIDRef, UseDefaultIID());

				// Create an instance of the storage class.
				N2PNotesStorage* store = new N2PNotesStorage();;

				// Get text index
				store->findex = ownedItemList[i].fAt;

				// Get story ref
				store->fstoryref = storyRef;

				// Get note UID
				store->fnoteUID = noteUID;

				// Get user name
				noteData->GetAuthor().GetSystemString(&store->fauthor);
				store->fauthor.SetTranslatable(kFalse);

				// Get creation date as a string
				GlobalTime gCreationTime = noteData->GetCreationTime();
				PMString creationTimeStr;
				PMString dateStr;
				gCreationTime.DateToString(dateStr, kTrue);
				PMString timeStr;
				gCreationTime.TimeToString(timeStr);
				creationTimeStr.Append(dateStr);
				creationTimeStr.Append(" at ");
				creationTimeStr.Append(timeStr);

				store->fcreated = creationTimeStr;
				store->fcreated.SetTranslatable(kFalse);

				// Get modified time as a string
				GlobalTime gModifiedTime = noteData->GetModifiedTime();
				PMString modifiedTimeStr;
				PMString dateStr1;
				gModifiedTime.DateToString(dateStr1, kTrue);
				PMString timeStr1;
				gModifiedTime.TimeToString(timeStr1);
				modifiedTimeStr.Append(dateStr1);
				modifiedTimeStr.Append(" at ");
				modifiedTimeStr.Append(timeStr1);

				store->flastmodified = modifiedTimeStr;
				store->flastmodified.SetTranslatable(kFalse);

				// Get note text 
				WideString noteText(Utils<INoteUtils>()->GetNoteContent(
					textModel, noteUIDRef));
				noteText.GetSystemString(&store->ftext);
				store->ftext.SetTranslatable(kFalse);

				// Append storage instance to storage structure
				fBNotesStore.push_back(store);
			}
		}
	} while(kFalse);
}


/* GetAssociatedWidget
*/
IControlView* N2PNotesUtils::GetAssociatedWidget(WidgetID widgetID)
{
	IControlView* listControlView = nil;
	/*do
	{
		InterfacePtr<IPanelControlData> 
			panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kBNotesPanelWidgetID));	
		if(panelData == nil)
		{
			break;
		}

		listControlView = panelData->FindWidget(widgetID);
		if(listControlView == nil)
		{
			ASSERT_MSG(listControlView != nil, "listControlView nil");
			break;
		}
	} while(kFalse);*/
	
	return listControlView;
}


/* GoToSpecifiedNote
*/
void N2PNotesUtils::GoToSpecifiedNote(int32 iNote)
{
	do
	{
		TextIndex start = kInvalidTextIndex;
		UIDRef sref(nil, kInvalidUID);

		// Get related story ref for selected listbox widget.
		sref = fBNotesStore[iNote]->fstoryref;

		// Get related index into text for selected listbox widget.
 		start = fBNotesStore[iNote]->findex;

		IControlView* docView = Utils<INoteSuiteUtils>()->GetDocControlView();
		if (docView == nil)
		{
			ASSERT_MSG(docView != nil, "docView nil");
			break;
		}
		IActiveContext* ac = Utils<ISelectionUtils>()->ActivateView(docView);
		ISelectionManager* viewSelMgr = ac->GetContextSelection();

		Utils<INoteSuiteUtils>()->NavigateNote(
			viewSelMgr, docView, start, sref, INoteSuite::kNextNote);

	} while(kFalse);
}

void N2PNotesUtils::GetNotesOnElementArticle(K2Vector<N2PNotesStorage*>& fN2PNotesStore,const UIDRef& StoryElementOfArticle)
{
	do
	{
		CAlert::InformationAlert("21");
		InterfacePtr<ITextModel> textModel(StoryElementOfArticle, UseDefaultIID());
		if (textModel == nil)
		{
			// No story, so we leave.
			break;
		}
		CAlert::InformationAlert("22");

		int32 storyLength = textModel->GetPrimaryStoryThreadSpan() - 1;
	
		CAlert::InformationAlert("23");

		// If story is not empty...
		if (storyLength >= 1)
		{
			CAlert::InformationAlert("24");

			OwnedItemDataList ownedItemList;
			
			CAlert::InformationAlert("25");

			//Para CS4
			Utils<ITextUtils> textUtils;
			ASSERT(textUtils);
			if (!textUtils) {
				break;
			}
			CAlert::InformationAlert("26");

			textUtils->CollectOwnedItems(textModel, 0, storyLength, &ownedItemList);
			//Text::CollectOwnedItems(textModel, 0, storyLength, &ownedItemList);
			
			CAlert::InformationAlert("27");

			// Iterate the Notes
			for (int32 i = 0; i < ownedItemList.Length(); ++i)
			{
				UID noteUID = ownedItemList[i].fUID;
				UIDRef noteUIDRef(StoryElementOfArticle.GetDataBase(), noteUID);
				InterfacePtr<INoteData> noteData(noteUIDRef, UseDefaultIID());
				if(noteData==nil)
					continue;
				// Create an instance of the storage class.
				N2PNotesStorage* store = new N2PNotesStorage();

				// Get text index
				store->findex = ownedItemList[i].fAt;

				// Get story ref
				store->fstoryref = StoryElementOfArticle;

				// Get note UID
				store->fnoteUID = noteUID;

				// Get user name
				noteData->GetAuthor().GetSystemString(&store->fauthor);
				store->fauthor.SetTranslatable(kFalse);

				// Get creation date as a string
				GlobalTime gCreationTime = noteData->GetCreationTime();
				PMString creationTimeStr;
				PMString dateStr;
				gCreationTime.DateToString(dateStr, kTrue);
				PMString timeStr;
				gCreationTime.TimeToString(timeStr);
				creationTimeStr.Append(dateStr);
				creationTimeStr.Append(" at ");
				creationTimeStr.Append(timeStr);

				store->fcreated = creationTimeStr;
				store->fcreated.SetTranslatable(kFalse);

				// Get modified time as a string
				GlobalTime gModifiedTime = noteData->GetModifiedTime();
				PMString modifiedTimeStr;
				PMString dateStr1;
				gModifiedTime.DateToString(dateStr1, kTrue);
				PMString timeStr1;
				gModifiedTime.TimeToString(timeStr1);
				modifiedTimeStr.Append(dateStr1);
				modifiedTimeStr.Append(" at ");
				modifiedTimeStr.Append(timeStr1);

				store->flastmodified = modifiedTimeStr;
				store->flastmodified.SetTranslatable(kFalse);

				// Get note text 
				WideString noteText(Utils<INoteUtils>()->GetNoteContent(
					textModel, noteUIDRef));
				noteText.GetSystemString(&store->ftext);
				store->ftext.SetTranslatable(kFalse);

				// Append storage instance to storage structure
				fN2PNotesStore.push_back(store);
			}
			CAlert::InformationAlert("28");

		}
	} while(kFalse);
}


ErrorCode N2PNotesUtils::AddNotesToElementArticle(ITextModel* mytextmodel, const  K2Vector<N2PNotesStorage*>& fBNotesStore)
{
	ErrorCode error=kFailure;
	do
	{
		
		for(int32 i=0;i<fBNotesStore.Length();i++)
		{
			PMString Texto = fBNotesStore[i]->ftext;
			PMString Autor = fBNotesStore[i]->ftext;
			TextIndex position = fBNotesStore[i]->findex;
			
			WideString mytextW(Texto);
			//K2::shared_ptr<WideString>	mytext(new WideString(Texto));
			boost::shared_ptr<WideString>	mytext(new WideString(Texto));//const boost::shared_ptr<WideString>	autorr=Autor;
			
			N2PSQLUtilities::InsertTextMiee(mytextmodel, position, mytext);
			
			InterfacePtr<ICommand>	ConvertToNotCmd(CmdUtils::CreateCommand(kConvertToNoteCmdBoss));
			if(ConvertToNotCmd==nil)
			{
				break;
			}
			
			///
			InterfacePtr<IIntData> intData(ConvertToNotCmd, IID_IINTDATA);
			if(intData==nil)
			{
				break;
			}
			
			intData->Set(position);
			
			//
			InterfacePtr<ICreateNoteCmdData> CreateNoteData(ConvertToNotCmd, IID_ICREATENOTECMDDATA);
			if(CreateNoteData==nil)
			{
				break;
			}
			CreateNoteData->Set(mytextW,kTrue);
			CreateNoteData->SetNoteContentRange(position,position+Texto.NumUTF16TextChars());
			
			ConvertToNotCmd->SetItemList(::GetUIDRef(mytextmodel));
			
			
			if (CmdUtils::ProcessCommand(ConvertToNotCmd) != kSuccess)
			{
				//ASSERT_FAIL("Can't process SetLockPositionCmd");
			}
			error=kSuccess;
		}
		
		
	}while(false);
	return(error);
}
//	End N2PNotesUtils.cpp
