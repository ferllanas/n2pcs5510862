/*
//	File:	N2PsalBusquedaDialogController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "N2PSQLListBoxHelper.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "IApplication.h"



#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
//#include "DocumentID.h"
#include "OpenPlaceID.h"


// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "SDKUtilities.h"
#include "CAlert.h"
#include "SDKUtilities.h"

#include <stdlib.h>
#include <stdio.h>


#ifdef MACINTOSH

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"
	#include "IN2PSQLUtils.h"
	
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PCheckInOutID.h"
	#include "InterlasaUtilities.h"
#endif

#ifdef WINDOWS
	#include <sys/stat.h>
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"
	
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PCheckInOutID.h"
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif




struct DatosUltimaBusquedaNotas
{
	PMString Guia_Nota_Busc;
	PMString Fecha_Nota_Busc;
	PMString Estatus_Nota_Busc;
	PMString Seccion_Nota_Busc;
	PMString Publicacion_Nota_Busc;
	PMString DirigidoA_Nota_Busc;
	PMString Paina_Nota_Busc;
};

DatosUltimaBusquedaNotas DatosBuscquedaNotas;


/** N2PsalBusquedaDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class N2PsalBusquedaDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PsalBusquedaDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~N2PsalBusquedaDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		virtual void UserCancelled();
	private:

		void LLenar_Combo_DirigidoA();
		
		void LLenar_Combo_EstatusNota(PMString& Id_EstatusActual);
		
		void LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		void LLenar_Combo_Seccion(PMString Id_PublicacionText);
		
		void SeleccionarCadenaEnComboBox(PMString Cadena, WidgetID widget);
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
		
		PMString Seccion ;
		PMString Publicidad ;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(N2PsalBusquedaDialogController, kN2PsqlBusquedaDialogControllerImpl)

/* ApplyDialogFields
*/
void N2PsalBusquedaDialogController::InitializeDialogFields(IActiveContext* context) 
{

	
	
	//Vamos a crear un archivo para guardar los parametros de la ultima busqueda y leerlos
	bool16 retval=kTrue;
	
	//CAlert::InformationAlert("A");
	
	PMString Date = InterlasaUtilities::FormatFechaYHoraManana("%m/%d/%Y");
	
	this->SetTextControlData(kN2PsqlTextEnGuiaWidgetID,DatosBuscquedaNotas.Guia_Nota_Busc);
	this->SetTextControlData(kN2PsqlTextPageWidgetID,DatosBuscquedaNotas.Paina_Nota_Busc);
	if(DatosBuscquedaNotas.Fecha_Nota_Busc.NumUTF16TextChars()<=0)
		this->SetTextControlData(kN2PsqlTextFechaWidgetID,Date);
	else
		this->SetTextControlData(kN2PsqlTextFechaWidgetID,DatosBuscquedaNotas.Fecha_Nota_Busc);
	
	this->LLenar_Combo_DirigidoA();
	this->LLenar_Combo_EstatusNota(DatosBuscquedaNotas.Estatus_Nota_Busc);
	this->LLenar_Combo_Publicacion(DatosBuscquedaNotas.Publicacion_Nota_Busc);
	this->LLenar_Combo_Seccion(DatosBuscquedaNotas.Seccion_Nota_Busc );
	this->SeleccionarCadenaEnComboBox(DatosBuscquedaNotas.DirigidoA_Nota_Busc, kN2PsqlComboRoutedToWidgetID);
	//CAlert::InformationAlert("B");
}

/*
*/
void N2PsalBusquedaDialogController::UserCancelled()
{
	this->SetTextControlData(kN2PsqlTextPageWidgetID,"Cancel");
}

/* ValidateDialogFields
*/
WidgetID N2PsalBusquedaDialogController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result = kNoInvalidWidgets;
	
	do
	{
		PMString Date = this->GetTextControlData(kN2PsqlTextFechaWidgetID);

		// Put code to validate widget values here.
		if(Date.NumUTF16TextChars()>0)
		{
					Date.Append("/");
					PMString *MM=Date.GetItem("/",1);
					if(MM==nil)
					{
						PMString NoNotas=PMString(kN2PSQLNoSeFechaValidaKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						result=kN2PsqlTextFechaWidgetID;
						break;
					}
					
					/*if(MM->GetAsNumber()>0 && MM->GetAsNumber()<13)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaNoExisteStrKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}*/
					
					PMString *DD=Date.GetItem("/",2);
					
					if(DD==nil)
					{
						PMString NoNotas=PMString(kN2PSQLNoSeFechaValidaKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						result=kN2PsqlTextFechaWidgetID;
						break;
					}
					
					/*if(DD->GetAsNumber()>0 && DD->GetAsNumber()<32)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaNoExisteStrKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}*/
					
					PMString *Anno=Date.GetItem("/",3);
					
					if(Anno==nil)
					{
						PMString NoNotas=PMString(kN2PSQLNoSeFechaValidaKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						result=kN2PsqlTextFechaWidgetID;
						break;
					}
					
					/*if(Anno->GetAsNumber()>0)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaNoExisteStrKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}
					
					Date="";
					if(Anno->NumUTF16TextChars()>0)
					{	
						Date.Append(Anno->GrabCString());
						
						Date.Append("-");
					}
					if(MM->NumUTF16TextChars()>0)
					{
						
						Date.Append(MM->GrabCString());
						Date.Append("-");
					}
					if(DD->NumUTF16TextChars()>0)
					{
						Date.Append(DD->GrabCString());
					}
					*/
					
				}
	}while(false);
	return result;
}

/* ApplyDialogFields
*/
void N2PsalBusquedaDialogController::ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId) 
{
	// Replace with code that gathers widget values and applies them.

	do
	{
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		if(SQLInterface==nil)
			break;
		
		PMString GuiaNota = this->GetTextControlData(kN2PsqlTextEnGuiaWidgetID);
		PMString NomPagina = this->GetTextControlData(kN2PsqlTextPageWidgetID);
		PMString Date = this->GetTextControlData(kN2PsqlTextFechaWidgetID);
		PMString Status = this->GetTextControlData(kN2PsqlTextStatusWidgetID);
		PMString DirigidoA = this->GetTextControlData(kN2PsqlComboRoutedToWidgetID);
		
		PMString NomPublicidad = this->GetTextControlData(kN2PsqlComboPubliWidgetID);
		PMString NomSeccion = this->GetTextControlData(kN2PsqlComboSeccionWidgetID);
		
		
			
		DatosBuscquedaNotas.Guia_Nota_Busc=GuiaNota;
		DatosBuscquedaNotas.Paina_Nota_Busc=NomPagina;
		DatosBuscquedaNotas.Fecha_Nota_Busc=Date;
		DatosBuscquedaNotas.Estatus_Nota_Busc=Status;
		DatosBuscquedaNotas.DirigidoA_Nota_Busc=DirigidoA;
		DatosBuscquedaNotas.Publicacion_Nota_Busc=NomPublicidad;
		DatosBuscquedaNotas.Seccion_Nota_Busc=NomSeccion;
		
		
	
		
		
	}while(false);
	
	SystemBeep();  
}


void N2PsalBusquedaDialogController::LLenar_Combo_DirigidoA()
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::LLenar_Combo_DirigidoA panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboRoutedToWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::LLenar_Combo_DirigidoA ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::LLenar_Combo_DirigidoA dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Id_Usuario From Usuario ORDER By Id_Usuario ASC ";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::LLenar_Combo_DirigidoA IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		} 
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}

		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		PMString DirigidoA="";

		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		} 
		
		PMString nameApp=app->GetApplicationName();
		nameApp.SetTranslatable(kFalse);
			
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in N2PsalBusquedaDialogController::LLenar_Combo_DirigidoA()");
			break;
		}
		
		DirigidoA.AppendNumber(wrkSpcPrefs->GetID_Usuario());
		
		Busqueda="SELECT Id_Usuario From Usuario Where Id_Empleado=";
		Busqueda.Append(DirigidoA);
		PMString Cadena="";
		
			
		K2Vector<PMString> QueryVector2;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector2);
		
		for(int32 i=0;i<QueryVector2.Length();i++)
		{
			Cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector2[i]);
		}
		
		Cadena.SetTranslatable(kFalse);
		DirigidoA=Cadena;
		
		DirigidoA.SetTranslatable(kFalse);
		int32 Index=dropListData->GetIndex(DirigidoA);
		IDDLDrComboBoxSelecPrefer->Select(Index);
	}while(false);
}


void N2PsalBusquedaDialogController::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::SeleccionarCadenaEnComboBox panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::SeleccionarCadenaEnComboBoxComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::SeleccionarCadenaEnComboBoxdropListData");
			
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController::SeleccionarCadenaEnComboBoxIDDLDrComboBoxSelecPrefer");
			break;
		}

		
		int32 Index=dropListData->GetIndex(CadenaASeleccionar);
		
		if(Index >= 0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		
		
	}while(false);
}


void N2PsalBusquedaDialogController::LLenar_Combo_EstatusNota(PMString& Id_EstatusActual)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController:: LLenar_Combo_EstatusNota panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlTextStatusWidgetID);
		if(ComboCView==nil)
		{
			CAlert::ErrorAlert("ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController:: LLenar_Combo_EstatusNota dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Id_Estatus, Nombre_Estatus From Estatus_Elemento WHERE Id_tipo_ele=10 ORDER BY Nombre_Estatus ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsalBusquedaDialogController:: LLenar_Combo_EstatusNota IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		PMString DSNName= "Default";//N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		
		if(SQLInterface==nil)
		{
			break;
		}
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}

		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		int32 Index=dropListData->GetIndex(Id_EstatusActual);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		/*PMString DirigidoA="";

		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString nameApp=app->GetApplicationName();

		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in N2PPrfDialogController::ApplyFields()");
			break;
		}
		
		DirigidoA.AppendNumber(wrkSpcPrefs->GetID_Usuario());
		
		Busqueda="SELECT Id_Usuario From Usuario Where Id_Empleado=";
		Busqueda.Append(DirigidoA);
		PMString Cadena="";
		
			
		K2Vector<PMString> QueryVector2;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector2);
		
		for(int32 i=0;i<QueryVector2.Length();i++)
		{
			Cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector2[i]);
		}
		DirigidoA=Cadena;
	
		int32 Index=dropListData->GetIndex(DirigidoA);
		IDDLDrComboBoxSelecPrefer->Select(Index);*/
	}while(false);
}

void N2PsalBusquedaDialogController::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboPubliWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT SQL_CACHE  Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
					
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		int32 Index=dropListData->GetIndex(Id_PublicacionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}

		
			
	}while(false);
	
}



void N2PsalBusquedaDialogController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
	
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
			
		K2Vector<PMString> QueryVector;
		
		
		PMString NomPublicidad = this->GetTextControlData(kN2PsqlComboPubliWidgetID);
		
		if(NomPublicidad.NumUTF16TextChars()<=0)
			break;

		//Consulta
		PMString Busqueda="SELECT SQL_CACHE  Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion IN(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + NomPublicidad + "') ORDER BY Nombre_de_Seccion ASC";
		//CAlert::InformationAlert(Busqueda);
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		int32 Index=dropListData->GetIndex(Id_SeccionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
			
	}while(false);
	
}

int32 N2PsalBusquedaDialogController::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}

//  Generated by Dolly build 17: template "Dialog".
// End, N2PsalBusquedaDialogController.cpp.

