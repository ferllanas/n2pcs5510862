#!/bin/sh
REMOTE="$1"
USER="$2"
PASSWORD="$3"
FTPLOG='/tmp/ftplog'

ftp -n $REMOTE <<_FTP>>$FTPLOG
quote USER $USER
quote PASS $PASSWORD
bin
cd /n2pfile
put "$4" "$5"
quit
_FTP
# n2pftpconnect.sh
# N2PSQL
#
# Created by Fernando  Llanas on 07/09/10.
# Copyright 2010 Interlasa. All rights reserved.


