//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/persistentlist/N2PSQLDocServiceProvider.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #5 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// Implementation includes:
#include "CServiceProvider.h"
#include "K2Vector.h"
#include "DocumentID.h"
#include "N2PsqlID.h"


/** 
	registers as providing the service of responding to a group of document
	file action signals.  See the constructor code for a list of the
	signals this service responds.

	N2PSQLDocServiceProvider implements IK2ServiceProvider based on
	the partial implementation CServiceProvider.


	@ingroup persistentlist
	
*/
class N2PSQLDocServiceProvider : public CServiceProvider
{
	public:

		/**
			Constructor initializes a list of service IDs, one for each file action signal that DocWchResponder will handle.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLDocServiceProvider(IPMUnknown* boss);
		
		/**
			Destructor.  
		*/
		virtual	~N2PSQLDocServiceProvider();

		/**
			GetName initializes the name of the service.
			@param pName Ptr to PMString to receive the name.
		*/
		virtual void GetName(PMString* pName);

		/**
			GetServiceID returns a single service ID.  This is required, even though
			GetServiceIDs() will return the complete list initialized in the constructor.
			This method just returns the first service ID in the list.
		*/
		virtual ServiceID GetServiceID();

		/**
			IsDefaultServiceProvider tells the application this service is not the default service.
		*/
		virtual bool16 IsDefaultServiceProvider();
		
		/**
			GetInstantiationPolicy returns a InstancePerX value to indicate that only
			one instance per session is needed.
		*/
		virtual InstancePerX GetInstantiationPolicy();

		/**
			HasMultipleIDs returns kTrue in order to force a call to GetServiceIDs().
		*/
		virtual bool16 HasMultipleIDs() const;

		/**
			GetServiceIDs returns a list of services provided.
			@param serviceIDs List of IDs describing the services that N2PSQLDocServiceProvider registers to handle.
		*/
		virtual void GetServiceIDs(K2Vector<ServiceID>& serviceIDs);

	private:

		K2Vector<ServiceID> fSupportedServiceIDs;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLDocServiceProvider, kN2PSQLDocServiceProviderImpl)


/* N2PSQLDocServiceProvider Constructor
*/
N2PSQLDocServiceProvider::N2PSQLDocServiceProvider(IPMUnknown* boss)
	: CServiceProvider(boss)
{
	// Add the service IDs we want the associated responder to handle.
	//  (See DocumentID.h)
	fSupportedServiceIDs.clear();
	
	//	NewDoc
	fSupportedServiceIDs.push_back(kAfterNewDocSignalResponderService);

	//	OpenDoc
	fSupportedServiceIDs.push_back(kDuringOpenDocSignalResponderService);

	//	CloseDoc
	fSupportedServiceIDs.push_back(kBeforeCloseDocSignalResponderService);

	fSupportedServiceIDs.push_back(kAfterOpenDocSignalResponderService);

	if (fSupportedServiceIDs.size()<=0)
	{
		//ASSERT_FAIL("N2PSQLDocServiceProvider must support at least 1 service ID");
		fSupportedServiceIDs.push_back(kInvalidService);
	}

}

/* N2PSQLDocServiceProvider Dtor
*/
N2PSQLDocServiceProvider::~N2PSQLDocServiceProvider()
{
}

/* N2PSQLDocServiceProvider::GetName
*/
void N2PSQLDocServiceProvider::GetName(PMString* pName)
{
	pName->SetCString("A2PPro Responder Service");
}

/* N2PSQLDocServiceProvider::GetServiceID
*/
ServiceID N2PSQLDocServiceProvider::GetServiceID() 
{
	// Should never be called given that HasMultipleIDs() returns kTrue.
	return fSupportedServiceIDs[0];
}

/* N2PSQLDocServiceProvider::IsDefaultServiceProvider
*/
bool16 N2PSQLDocServiceProvider::IsDefaultServiceProvider()
{
	return kFalse;
}

/* N2PSQLDocServiceProvider::GetInstantiationPolicy
*/
IK2ServiceProvider::InstancePerX N2PSQLDocServiceProvider::GetInstantiationPolicy()
{
	return IK2ServiceProvider::kInstancePerSession;
}

/* N2PSQLDocServiceProvider::HasMultipleIDs
*/
bool16 N2PSQLDocServiceProvider::HasMultipleIDs() const
{
	return kTrue;
}

/* N2PSQLDocServiceProvider::GetServiceIDs
*/
void N2PSQLDocServiceProvider::GetServiceIDs(K2Vector<ServiceID>& serviceIDs)
{
	// Append a service IDs for each service provided. 
	for (int32 i = 0; i<fSupportedServiceIDs.size(); i++)
		serviceIDs.push_back(fSupportedServiceIDs[i]);

}


// End, N2PSQLDocServiceProvider.cpp.



