/*
//	File:	N2PSQLPreferDlgObserver.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IPDFExptStyleListMgr.h"
#include "IWorkspace.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "SDKFileHelper.h"
#include "CAlert.h"
#include "PlatformFileSystemIterator.h"
#include "FileUtils.h"
#include "ITriStateControlData.h"

// Project includes:



#ifdef MACINTOSH
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "InterlasaUtilities.h"

	#include "IN2PSQLUtils.h"
#endif

#ifdef WINDOWS
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\IN2PSQLUtils.h"
#endif

//#include <sys/io.h>
#include <stdio.h>
//#include <unistd.h>
/** N2PSQLPreferDlgObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Juan Fernando Llanas Rdz
*/
class N2PSQLPreferDlgObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLPreferDlgObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~N2PSQLPreferDlgObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
	private:
		
		void SetTextValue(WidgetID widget, PMReal value);
		PMReal GetTextValue(WidgetID widget);
		void DoNewConecction();
		void HideShowWidgets(WidgetID widget,bool16 muestra);
		void EnableDisableWidgetsParaGuardarConection(bool16 muestra);
		void EnableDisableWidgets(WidgetID widget,bool16 muestra);
		void DoCancelConection();
		void DoGuardarConnection();
		PMString GetStringOfWidget(WidgetID widget);
		void LlenarComboPreferencias(PMString NomPref);
		void DoBorrarPreferencia();
		void BorrarPreferenciaActual();
		void DoEditarConnection();
		void SetStringOfWidget(WidgetID widget, PMString string);
		void LimpiarWidgets();
		void DoSetDefaultConection();
		bool16 ValidaDatosConexion();
		bool16 DoTestConection(const int32& testtype);
		void DoChagedConectionSelection();
		void SelectPathFileServer();
		
		void SetTriStateControlData(WidgetID widget,bool16 status);
		bool16 GetTriStateControlData(WidgetID widget);
		
		void LlenarPDFComboBox(PMString PresetToPDFeditorial,PMString PresetToPDFeditorialWEB);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(N2PSQLPreferDlgObserver, kN2PSQLPreferenciasDialogObserverImpl)

/* AutoAttach
*/
void N2PSQLPreferDlgObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PSQLPreferDlgObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		AttachToWidget(kN2PsqlNewDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kN2PsqlDeleteDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kN2PsqlSetLocalDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kN2PsqlTestConectionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kN2PsqlCancelarDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kN2PsqlGuardarDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kN2PsqlEditDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);	
		AttachToWidget(kN2PsqlSelectPathFileServerButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);	
			
		AttachToWidget(kN2PsqlDataBaseComboWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void N2PSQLPreferDlgObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PSQLPreferDlgObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from N2PCheckInOut's info button widget.
		DetachFromWidget(kN2PsqlNewDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlDeleteDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlSetLocalDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlTestConectionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlCancelarDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlGuardarDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlEditDBConexionButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kN2PsqlSelectPathFileServerButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);	
		DetachFromWidget(kN2PsqlDataBaseComboWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void N2PSQLPreferDlgObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("N2PSQLPreferDlgObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		//Click sobre boton "Nueva Coneccion"
		if (theSelectedWidget == kN2PsqlNewDBConexionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			this->DoNewConecction();
			
		}
		
		//Click sobre boton "Borrar Coneccion"
		if (theSelectedWidget == kN2PsqlDeleteDBConexionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			this->DoBorrarPreferencia();
		}
		
		//Click sobre boton "Predeterminar coneccion"
		if (theSelectedWidget == kN2PsqlSetLocalDBConexionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			
			this->DoSetDefaultConection();
		}

		if (theSelectedWidget == kN2PsqlDataBaseComboWidgetID && theChange == kPopupChangeStateMessage)
		{
			// Bring up the About box.
			this->DoChagedConectionSelection();
		}
		
		//Click sobre boton "Probar coneccion"
		if (theSelectedWidget == kN2PsqlTestConectionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			if(this->DoTestConection(0))
				this->EnableDisableWidgets(kN2PsqlGuardarDBConexionButtonWidgetID,kTrue);
		}
		
		//Click sobre boton "Cancelar coneccion"
		if (theSelectedWidget == kN2PsqlCancelarDBConexionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			this->DoCancelConection();
		}
		
		//Click sobre boton "Gurdar coneccion"
		if (theSelectedWidget == kN2PsqlGuardarDBConexionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			this->DoGuardarConnection();
		}
		
		//Click sobre boton "Gurdar coneccion"
		if (theSelectedWidget == kN2PsqlEditDBConexionButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			this->DoEditarConnection();
		}
		
		//Click sobre boton "Gurdar coneccion"
		if (theSelectedWidget == kN2PsqlSelectPathFileServerButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			this->SelectPathFileServer();
		}
		
	} while (false);
}




void N2PSQLPreferDlgObserver::DoChagedConectionSelection()
{
	do
	{
			//CAlert::InformationAlert("Cambio");
			PreferencesConnection PrefConections;
		
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				break;
			}

			InterfacePtr<IControlView>		CVComboBoxSelecPrefer(panel->FindWidget(kN2PsqlDataBaseComboWidgetID), UseDefaultIID() );
			if(CVComboBoxSelecPrefer==nil)
			{
				break;
			}
			
			InterfacePtr<IStringListControlData> dropListData(CVComboBoxSelecPrefer, UseDefaultIID());
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				break;
			}
			
			int32 indexSelected=IDDLDrComboBoxSelecPrefer->GetSelected();
			
			PMString NamePrefSelected = dropListData->GetString(indexSelected);
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
				break;
			
			if(UpdateStorys->GetPreferencesConnectionForNameConection(NamePrefSelected,PrefConections))//UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				//this->SetStringOfWidget(kN2PSQLIPServerOfDBEditWidgetID,PrefConections.IPServerConnection);
				this->SetStringOfWidget(kN2PSQLNameConectionDBWidgetID,PrefConections.NameConnection);
				this->SetStringOfWidget(kN2PSQLDSNNameEditWidgetID,PrefConections.DSNNameConnection);
				this->SetStringOfWidget(kN2PSQLUsuarioOfDBEditWidgetID,PrefConections.NameUserDBConnection);
				this->SetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID,PrefConections.PathOfServerFile);
				this->SetStringOfWidget(kN2PSQLPathOfServerFileImagenesEditWidgetID,PrefConections.PathOfServerFileImages);
				this->SetStringOfWidget(kN2PSQLRutaDServidorRespNotasEditWidgetID,PrefConections.RutaDServidorRespNotas);
				this->SetStringOfWidget(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,PrefConections.ImagCarpInServFImages);
				this->SetStringOfWidget(kN2PSQLURLWebServicesEditWidgetID,PrefConections.URLWebServices);
				
				
				this->SetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID, PrefConections.TimeToCheckUpdateElements);
				this->SetTextValue(kN2PResizeHeightWidgetID, PrefConections.N2PResizeHeight);
				this->SetTextValue(kN2PResizeWidthWidgetID, PrefConections.N2PResizeWidth);
				
				if(PrefConections.LlenarTablaWEB==1)
					this->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,kTrue);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,kFalse);
				 
				 this->SetStringOfWidget(KN2PSQLEstatusParaExportarATWEBWidgetID,PrefConections.EstadoParaExportarATWEB);
				 
				 
				 
				
				
				if(PrefConections.EnviaAWEBSinRetornosTitulo==1)
					this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,kTrue);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,kFalse);
				

				if(PrefConections.EsEnviarNotasHijasAWEB==1)
					this->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,kTrue);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,kFalse);

				 if(PrefConections.EsCampoGuiaObligatorio==1)
					this->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,kTrue);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,kFalse);
				
				 if(PrefConections.CreateBackUpPagina==1)
					this->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,kTrue);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,kFalse);
				
				if(PrefConections.ExportarPaginaComoPDF==1)
					this->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,kTrue);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,kFalse);
		
				this->SetStringOfWidget(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,PrefConections.NombreEstatusParaNotasCompletas);
				
				
				this->SetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWidgetID,PrefConections.RutaDePaginaComoPDF);
				this->SetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,PrefConections.RutaDePaginaComoPDFWEB);
				this->SetStringOfWidget(kN2PSQLnameParaStyleCreditoWidgetID, PrefConections.nameParaStyleCredito);
				this->SetStringOfWidget(kN2PHmeDSNNameWidgetID, PrefConections.N2PHmeDSNName);
				this->SetStringOfWidget(kN2PHmeDSNUserLoginWidgetID, PrefConections.N2PHmeDSNUserLogin);
				this->SetStringOfWidget(kN2PHmeDSNPwdLoginWidgetID, PrefConections.N2PHmeDSNPwdLogin);
				this->SetStringOfWidget(kN2PHmeFTPServerWidgetID, PrefConections.N2PHmeFTPServer);
				this->SetStringOfWidget(kN2PHmeFTPUserWidgetID, PrefConections.N2PHmeFTPUser);
				this->SetStringOfWidget(kN2PHmeFTPPwdWidgetID, PrefConections.N2PHmeFTPPwd);
				this->SetStringOfWidget(kN2PHmeFTPFolderWidgetID, PrefConections.N2PHmeFTPFolder);
				
				if(PrefConections.EnviaAWEBEnAltasPieFoto==1)
					this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kSelected);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kUnselected);
		
				
				
				 if(PrefConections.ExportPDFPresets==1)
					this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kSelected);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kUnselected);
				
				 if(PrefConections.ExportPDFPresetsEditWEB==1)
					this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kSelected);
				else
					this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kUnselected);
				
				if(PrefConections.N2PHmteSendToHmca==1)
				{
					//CAlert::InformationAlert("B select");
					this->SetTriStateControlData(kN2PHmteSendToHmcaWidgetID,ITriStateControlData::kSelected);
				}
				else
				{
					//CAlert::InformationAlert("B kUnselected");
					this->SetTriStateControlData(kN2PHmteSendToHmcaWidgetID,ITriStateControlData::kUnselected);
				}
				 //CAlert::InformationAlert("AAAAA");
				//this->SetStringOfWidget(kN2PSQLPassswordOfDBWidgetID,"");
				
				if(PrefConections.N2PResizePreview==1)
				{
					//CAlert::InformationAlert("B select");
					this->SetTriStateControlData(kN2PResizePreviewViewWidgetID,ITriStateControlData::kSelected);
				}
				else
				{
					//CAlert::InformationAlert("B kUnselected");
					this->SetTriStateControlData(kN2PResizePreviewViewWidgetID,ITriStateControlData::kUnselected);
				}
				
				if(PrefConections.N2PUtilizarGuias==1)
				{
					//CAlert::InformationAlert("B select");
					this->SetTriStateControlData(kN2PUsarGuiasWidgetID,ITriStateControlData::kSelected);
				}
				else
				{
					//CAlert::InformationAlert("B kUnselected");
					this->SetTriStateControlData(kN2PUsarGuiasWidgetID,ITriStateControlData::kUnselected);
				}
			}	
			
	}while(false);
}



void N2PSQLPreferDlgObserver::DoNewConecction()
{
	this->EnableDisableWidgetsParaGuardarConection(kTrue);
	this->LimpiarWidgets();
	this->EnableDisableWidgets(kN2PsqlGuardarDBConexionButtonWidgetID,kFalse);
}

void N2PSQLPreferDlgObserver::LimpiarWidgets()
{
	//this->SetStringOfWidget(kN2PSQLIPServerOfDBEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLNameConectionDBWidgetID,"");
	this->SetStringOfWidget(kN2PSQLDSNNameEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLUsuarioOfDBEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLPassswordOfDBWidgetID,"");
	this->SetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLPathOfServerFileImagenesEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLURLWebServicesEditWidgetID,"");
	
	this->SetStringOfWidget(kN2PSQLRutaDServidorRespNotasEditWidgetID,"");
	
	this->SetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID, 0);
	this->SetTextValue(kN2PResizeHeightWidgetID, 0);
	this->SetTextValue(kN2PResizeWidthWidgetID, 0);
	
	this->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,ITriStateControlData::kUnselected);
	
	this->SetStringOfWidget(KN2PSQLEstatusParaExportarATWEBWidgetID,"");
	
	
	this->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kUnselected);
	
	
	
	this->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,ITriStateControlData::kUnselected);
	this->SetStringOfWidget(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,"");
	this->SetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWidgetID,"");
	this->SetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,"");
	this->SetStringOfWidget(kN2PSQLnameParaStyleCreditoWidgetID,"");
	
}

void N2PSQLPreferDlgObserver::DoEditarConnection()
{
	
	this->EnableDisableWidgetsParaGuardarConection(kTrue);
	this->EnableDisableWidgets(kN2PSQLNameConectionDBWidgetID,kFalse);
	this->EnableDisableWidgets(kN2PsqlGuardarDBConexionButtonWidgetID,kFalse);
}


void N2PSQLPreferDlgObserver::DoSetDefaultConection()
{

	do
	{
		PreferencesConnection PrefConections;
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
		
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				break;
			}
		
			InterfacePtr<IControlView>		CVComboBoxSelecPrefer(panel->FindWidget(kN2PsqlDataBaseComboWidgetID), UseDefaultIID() );
			if(CVComboBoxSelecPrefer==nil)
			{
				break;
			}
		
			
			InterfacePtr<IStringListControlData> dropListData(CVComboBoxSelecPrefer, UseDefaultIID());
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
		
			
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				break;
			}
		
			
			int32 indexSelected=IDDLDrComboBoxSelecPrefer->GetSelected();
			
			PMString NamePrefSelected = dropListData->GetString(indexSelected);
			
	
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
	
		if(UpdateStorys==nil)
		{
			break;
		}
		
		
		//if(!UpdateStorys->GetDefaultPreferencesConnection( PrefConections,kFalse))
		if(!UpdateStorys->GetPreferencesConnectionForNameConection(NamePrefSelected, PrefConections))
			break;	
		
		UpdateStorys->GuardarPreferencesConnectionForNameConection("Default",PrefConections);
		UpdateStorys->GuardarPreferencesConnectionForNameConection(PrefConections.NameConnection,PrefConections);
		
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kTrue);
		//CAlert::InformationAlert("FERR");
	}while(false);
	
}

bool16 N2PSQLPreferDlgObserver::DoTestConection(const int32& Testnormal)
{
	bool16 retval=kFalse;
	PreferencesConnection PrefConections;
	
	//PrefConections.TypeConnection="SQL SERVER";
	//PrefConections.IPServerConnection=this->GetStringOfWidget(kN2PSQLIPServerOfDBEditWidgetID);
	PrefConections.NameConnection=this->GetStringOfWidget(kN2PSQLNameConectionDBWidgetID);
	PrefConections.DSNNameConnection=this->GetStringOfWidget(kN2PSQLDSNNameEditWidgetID);
	PrefConections.NameUserDBConnection=this->GetStringOfWidget(kN2PSQLUsuarioOfDBEditWidgetID);
	PrefConections.PwdUserDBConnection=this->GetStringOfWidget(kN2PSQLPassswordOfDBWidgetID);
	PrefConections.PathOfServerFile=this->GetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID);
	PrefConections.PathOfServerFileImages=this->GetStringOfWidget(kN2PSQLPathOfServerFileImagenesEditWidgetID);
	PrefConections.RutaDServidorRespNotas=this->GetStringOfWidget(kN2PSQLRutaDServidorRespNotasEditWidgetID);
	PrefConections.ImagCarpInServFImages=this->GetStringOfWidget(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID);
	PrefConections.URLWebServices=this->GetStringOfWidget(kN2PSQLURLWebServicesEditWidgetID);
	
	
	PrefConections.PresetTOPDFEditorial=this->GetStringOfWidget(kN2PsqlExportPDFPresetsDropDownWidgetID);
	PrefConections.PresetTOPDFEditorialWEB=this->GetStringOfWidget(kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID);
	
	PrefConections.TimeToCheckUpdateElements = this->GetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID);
	PrefConections.N2PResizeWidth = this->GetTextValue(kN2PResizeWidthWidgetID);
	PrefConections.N2PResizeHeight = this->GetTextValue(kN2PResizeHeightWidgetID);
	
	if(this->GetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID)==kTrue)
			PrefConections.LlenarTablaWEB=1;
		else
			PrefConections.LlenarTablaWEB=0;
	
	PrefConections.EstadoParaExportarATWEB=this->GetStringOfWidget(KN2PSQLEstatusParaExportarATWEBWidgetID);
	
	
	PrefConections.NombreEstatusParaNotasCompletas=this->GetStringOfWidget(KN2PSQLNombreEstatusParaNotasCompletasWidgetID);
	PrefConections.RutaDePaginaComoPDF=this->GetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWidgetID);
	PrefConections.RutaDePaginaComoPDFWEB=this->GetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWEBWidgetID);
	PrefConections.nameParaStyleCredito=this->GetStringOfWidget(kN2PSQLnameParaStyleCreditoWidgetID);
	PrefConections.N2PHmeDSNName=this->GetStringOfWidget(kN2PHmeDSNNameWidgetID);
	PrefConections.N2PHmeDSNUserLogin=this->GetStringOfWidget(kN2PHmeDSNUserLoginWidgetID);
	PrefConections.N2PHmeDSNPwdLogin=this->GetStringOfWidget(kN2PHmeDSNPwdLoginWidgetID);
	PrefConections.N2PHmeFTPServer=this->GetStringOfWidget(kN2PHmeFTPServerWidgetID);
	PrefConections.N2PHmeFTPUser=this->GetStringOfWidget(kN2PHmeFTPUserWidgetID);
	PrefConections.N2PHmeFTPPwd=this->GetStringOfWidget(kN2PHmeFTPPwdWidgetID);
	PrefConections.N2PHmeFTPFolder=this->GetStringOfWidget(kN2PHmeFTPFolderWidgetID);
	
	if(this->GetTriStateControlData(kN2PHmteSendToHmcaWidgetID)==ITriStateControlData::kSelected)
	{
		PrefConections.N2PHmteSendToHmca=1;
	}
	else
		PrefConections.N2PHmteSendToHmca=0;
	
	if(this->GetTriStateControlData(kN2PResizePreviewViewWidgetID)==kTrue)
	{
		PrefConections.N2PResizePreview=1;
	}
	else
		PrefConections.N2PResizePreview=0;
	
	if(this->GetTriStateControlData(kN2PUsarGuiasWidgetID)==kTrue)
	{
		PrefConections.N2PUtilizarGuias=1;
	}
	else
		PrefConections.N2PUtilizarGuias=0;
	
	if(this->GetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID)==kTrue)
			PrefConections.ExportPDFPresetsEditWEB=1;
		else
			PrefConections.ExportPDFPresetsEditWEB=0;
		
	if(this->GetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID)==kTrue)
			PrefConections.ExportPDFPresets=1;
		else
			PrefConections.ExportPDFPresets=0;
		
	if(this->GetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID)==kTrue)
			PrefConections.EnviaAWEBEnAltasPieFoto=1;
		else
			PrefConections.EnviaAWEBEnAltasPieFoto=0;
		
		
	if(this->GetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID)==kTrue)
		PrefConections.EnviaAWEBSinRetornosTitulo=1;
	else
		PrefConections.EnviaAWEBSinRetornosTitulo=0;
		
		if(this->GetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID)==kTrue)
			PrefConections.EsEnviarNotasHijasAWEB=1;
		else
			PrefConections.EsEnviarNotasHijasAWEB=0;

	if(this->GetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID)==kTrue)
			PrefConections.EsCampoGuiaObligatorio=1;
		else
			PrefConections.EsCampoGuiaObligatorio=0;
		
	if(this->GetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID)==kTrue)
			PrefConections.CreateBackUpPagina=1;
		else
			PrefConections.CreateBackUpPagina=0;
		
	if(this->GetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID)==kTrue)
			PrefConections.ExportarPaginaComoPDF=1;
		else
			PrefConections.ExportarPaginaComoPDF=0;
		
	do
	{
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			CAlert::ErrorAlert("N2PSQLPreferDlgObserver::DoTestConection(),Invalid SQLInterface");
			break;
		}
		
		PMString stringConection="";
		stringConection.Append("DSN=" + PrefConections.DSNNameConnection +";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		if(SQLInterface->TestConnection(stringConection))
		{
			//CAlert::InformationAlert(stringConection);
			if(Testnormal==0)
			{
				CAlert::InformationAlert(kN2PSQLOkConecctionDSNStringKey);
			}
			
			retval=kTrue;
		}
		else
		{
			//CAlert::InformationAlert(stringConection);
			if(Testnormal==0)
			{
				CAlert::InformationAlert(kN2PSQLErrorConecctionDSNStringKey);
			}
			retval=kFalse;
		}
	}while(false);
	
return(retval);	
}



void N2PSQLPreferDlgObserver::DoGuardarConnection()
{

	do
	{
		if(this->DoTestConection(1))
		{
			PreferencesConnection PrefConections;
			PMString NameDefaultPrefenceConnection="";
	
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
	
			if(UpdateStorys==nil)
			{
				break;			
			}
	
			if(UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				NameDefaultPrefenceConnection=PrefConections.NameConnection;
	
			//PrefConections.TypeConnection="SQL SERVER";
			//PrefConections.IPServerConnection=this->GetStringOfWidget(kN2PSQLIPServerOfDBEditWidgetID);
			PrefConections.NameConnection=this->GetStringOfWidget(kN2PSQLNameConectionDBWidgetID);
			PrefConections.DSNNameConnection=this->GetStringOfWidget(kN2PSQLDSNNameEditWidgetID);
			PrefConections.NameUserDBConnection=this->GetStringOfWidget(kN2PSQLUsuarioOfDBEditWidgetID);
			PrefConections.PwdUserDBConnection=this->GetStringOfWidget(kN2PSQLPassswordOfDBWidgetID);
			
			PrefConections.PathOfServerFile=this->GetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID);
			PrefConections.PathOfServerFileImages=this->GetStringOfWidget(kN2PSQLPathOfServerFileImagenesEditWidgetID);
			PrefConections.RutaDServidorRespNotas=this->GetStringOfWidget(kN2PSQLRutaDServidorRespNotasEditWidgetID);
			PrefConections.ImagCarpInServFImages=this->GetStringOfWidget(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID);
			PrefConections.URLWebServices=this->GetStringOfWidget(kN2PSQLURLWebServicesEditWidgetID);
	
			PrefConections.TimeToCheckUpdateElements = this->GetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID);
			PrefConections.N2PResizeHeight = this->GetTextValue(kN2PResizeHeightWidgetID);
			PrefConections.N2PResizeWidth = this->GetTextValue(kN2PResizeWidthWidgetID);
			
			PrefConections.EstadoParaExportarATWEB=this->GetStringOfWidget(KN2PSQLEstatusParaExportarATWEBWidgetID);
			
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID) == kTrue)
			{
				
				PrefConections.LlenarTablaWEB=1;
			}
			else
			{
				
				PrefConections.LlenarTablaWEB=0;
			}
				
			PrefConections.NombreEstatusParaNotasCompletas=this->GetStringOfWidget(KN2PSQLNombreEstatusParaNotasCompletasWidgetID);
			PrefConections.RutaDePaginaComoPDF=this->GetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWidgetID);
			PrefConections.RutaDePaginaComoPDFWEB=this->GetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWEBWidgetID);
			PrefConections.nameParaStyleCredito=this->GetStringOfWidget(kN2PSQLnameParaStyleCreditoWidgetID);
			
			PrefConections.N2PHmeDSNName=this->GetStringOfWidget(kN2PHmeDSNNameWidgetID);
			PrefConections.N2PHmeDSNUserLogin=this->GetStringOfWidget(kN2PHmeDSNUserLoginWidgetID);
			PrefConections.N2PHmeDSNPwdLogin=this->GetStringOfWidget(kN2PHmeDSNPwdLoginWidgetID);
			PrefConections.N2PHmeFTPServer=this->GetStringOfWidget(kN2PHmeFTPServerWidgetID);
			PrefConections.N2PHmeFTPUser=this->GetStringOfWidget(kN2PHmeFTPUserWidgetID);
			PrefConections.N2PHmeFTPPwd=this->GetStringOfWidget(kN2PHmeFTPPwdWidgetID);
			PrefConections.N2PHmeFTPFolder=this->GetStringOfWidget(kN2PHmeFTPFolderWidgetID);
			
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID)==kTrue)
				PrefConections.EnviaAWEBEnAltasPieFoto=1;
			else
				PrefConections.EnviaAWEBEnAltasPieFoto=0;
			
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID)==kTrue)
				PrefConections.EnviaAWEBSinRetornosTitulo=1;
			else
				PrefConections.EnviaAWEBSinRetornosTitulo=0;
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID)==kTrue)
				PrefConections.EsEnviarNotasHijasAWEB=1;
			else
				PrefConections.EsEnviarNotasHijasAWEB=0;

			if(this->GetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID)==kTrue)
				PrefConections.EsCampoGuiaObligatorio=1;
			else
				PrefConections.EsCampoGuiaObligatorio=0;
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID)==kTrue)
				PrefConections.CreateBackUpPagina=1;
			else
				PrefConections.CreateBackUpPagina=0;
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID)==kTrue)
				PrefConections.ExportarPaginaComoPDF=1;
			else
				PrefConections.ExportarPaginaComoPDF=0;
			
			
			
			PrefConections.PresetTOPDFEditorial=this->GetStringOfWidget(kN2PsqlExportPDFPresetsDropDownWidgetID);
			PrefConections.PresetTOPDFEditorialWEB=this->GetStringOfWidget(kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID);
			
			if(this->GetTriStateControlData(kN2PResizePreviewViewWidgetID)==kTrue)
			{
				PrefConections.N2PResizePreview=1;
			}
			else
				PrefConections.N2PResizePreview=0;
			
			if(this->GetTriStateControlData(kN2PUsarGuiasWidgetID)==kTrue)
			{
				PrefConections.N2PUtilizarGuias=1;
			}
			else
				PrefConections.N2PUtilizarGuias=0;
			
			if(this->GetTriStateControlData(kN2PHmteSendToHmcaWidgetID)==ITriStateControlData::kSelected)
			{
				CAlert::InformationAlert("C select");
				PrefConections.N2PHmteSendToHmca=1;
			}
			else
			{
				CAlert::InformationAlert("C select");
				PrefConections.N2PHmteSendToHmca=0;
			}
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID)==kTrue)
				PrefConections.ExportPDFPresetsEditWEB=1;
			else
				PrefConections.ExportPDFPresetsEditWEB=0;
			
			
			if(this->GetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID)==kTrue)
				PrefConections.ExportPDFPresets=1;
			else
				PrefConections.ExportPDFPresets=0;
			
			
			if(UpdateStorys->GuardarPreferencesConnectionForNameConection(PrefConections.NameConnection,PrefConections)==kFalse)
			{
				CAlert::InformationAlert(kN2PSQLFailedWhenSavePrefStringKey);
				break;
			}
		
	
			this->EnableDisableWidgetsParaGuardarConection(kFalse);
			//this->LlenarComboPreferencias(PrefConections.NameConnection);
			//this->LlenarPDFComboBox(PrefConections.PresetTOPDFEditorial,PrefConections.PresetTOPDFEditorialWEB);
			
			if(NameDefaultPrefenceConnection.NumUTF16TextChars()>0 && NameDefaultPrefenceConnection==PrefConections.NameConnection)
			{
				UpdateStorys->GuardarPreferencesConnectionForNameConection("Default",PrefConections);
			}
			
		}
		else
		{
			this->EnableDisableWidgets(kN2PsqlGuardarDBConexionButtonWidgetID,kFalse);
		}
	}while(false);
}



void N2PSQLPreferDlgObserver::DoBorrarPreferencia()
{
	
	
	this->BorrarPreferenciaActual();
	//this->SetStringOfWidget(kN2PSQLIPServerOfDBEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLNameConectionDBWidgetID,"");
	this->SetStringOfWidget(kN2PSQLDSNNameEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLUsuarioOfDBEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLPassswordOfDBWidgetID,"");
	this->SetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLPathOfServerFileImagenesEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,"");
	this->SetStringOfWidget(kN2PSQLURLWebServicesEditWidgetID,"");
	
	this->SetStringOfWidget(kN2PSQLRutaDServidorRespNotasEditWidgetID,"");
	
	this->SetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID,0);
	this->SetTextValue(kN2PResizeHeightWidgetID,0);
	this->SetTextValue(kN2PResizeWidthWidgetID,0);
	
	this->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,ITriStateControlData::kUnselected);
	
	this->SetStringOfWidget(KN2PSQLEstatusParaExportarATWEBWidgetID,"");
	
	this->LlenarComboPreferencias("");
	
	this->LlenarPDFComboBox("","");
	this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kUnselected);
	
	this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kUnselected);
	
	this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kUnselected);
	
	this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,ITriStateControlData::kUnselected);

	this->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,ITriStateControlData::kUnselected);
	this->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,ITriStateControlData::kUnselected);
	this->SetStringOfWidget(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,"");
	this->SetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWidgetID,"");
	this->SetStringOfWidget(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,"");
	this->SetStringOfWidget(kN2PSQLnameParaStyleCreditoWidgetID,"");
	
}
			
			
void N2PSQLPreferDlgObserver::DoCancelConection()
{
	this->EnableDisableWidgetsParaGuardarConection(kFalse);
	this->LlenarComboPreferencias("");
	this->LlenarPDFComboBox("","");
}

void N2PSQLPreferDlgObserver::EnableDisableWidgetsParaGuardarConection(bool16 muestra)
{
	if(muestra==kTrue)
	{
		this->HideShowWidgets(kN2PsqlNewDBConexionButtonWidgetID,kFalse);
		this->HideShowWidgets(kN2PsqlDeleteDBConexionButtonWidgetID,kFalse);
		this->HideShowWidgets(kN2PsqlSetLocalDBConexionButtonWidgetID,kFalse);
		this->HideShowWidgets(kN2PsqlDataBaseComboWidgetID,kFalse);
		
		
		this->HideShowWidgets(kN2PsqlTestConectionButtonWidgetID,kTrue);
		this->HideShowWidgets(kN2PsqlCancelarDBConexionButtonWidgetID,kTrue);
		this->HideShowWidgets(kN2PsqlGuardarDBConexionButtonWidgetID,kTrue);
		
		
		//this->EnableDisableWidgets(kN2PSQLIPServerOfDBEditWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLNameConectionDBWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLDSNNameEditWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLUsuarioOfDBEditWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLPassswordOfDBWidgetID,kTrue);
		this->EnableDisableWidgets(kOKButtonWidgetID,kFalse);
		this->EnableDisableWidgets(kCancelButton_WidgetID,kFalse);
		
		this->EnableDisableWidgets(kN2PSQLPathOfServerFileEditWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLPathOfServerFileImagenesEditWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLURLWebServicesEditWidgetID,kTrue);
		
		this->EnableDisableWidgets(kN2PSQLRutaDServidorRespNotasEditWidgetID,kTrue);
		this->EnableDisableWidgets(KN2PSQLEstatusParaExportarATWEBWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,kTrue);
		this->EnableDisableWidgets(KN2PSQLTimeToCheckUpdateElemsWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PResizeWidthWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PResizeHeightWidgetID,kTrue);
		
		this->EnableDisableWidgets(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,kTrue);
		this->EnableDisableWidgets(KN2PSQLRutaDePaginaComoPDFWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,kTrue);
		this->EnableDisableWidgets(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,kTrue);
		this->EnableDisableWidgets(kN2PSQLnameParaStyleCreditoWidgetID,kTrue);
		
		
		
		
	}
	else
	{
		this->HideShowWidgets(kN2PsqlNewDBConexionButtonWidgetID,kTrue);
		this->HideShowWidgets(kN2PsqlDeleteDBConexionButtonWidgetID,kTrue);
		this->HideShowWidgets(kN2PsqlSetLocalDBConexionButtonWidgetID,kTrue);
		this->HideShowWidgets(kN2PsqlDataBaseComboWidgetID,kTrue);
		
		this->HideShowWidgets(kN2PsqlTestConectionButtonWidgetID,kFalse);
		this->HideShowWidgets(kN2PsqlCancelarDBConexionButtonWidgetID,kFalse);
		this->HideShowWidgets(kN2PsqlGuardarDBConexionButtonWidgetID,kFalse);
	
	
	
		//this->EnableDisableWidgets(kN2PSQLIPServerOfDBEditWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLNameConectionDBWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLDSNNameEditWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLUsuarioOfDBEditWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLPassswordOfDBWidgetID,kFalse);
		this->EnableDisableWidgets(kOKButtonWidgetID,kTrue);
		this->EnableDisableWidgets(kCancelButton_WidgetID,kTrue);
		
		this->EnableDisableWidgets(kN2PSQLPathOfServerFileEditWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLPathOfServerFileImagenesEditWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLURLWebServicesEditWidgetID,kFalse);
		
		this->EnableDisableWidgets(kN2PSQLRutaDServidorRespNotasEditWidgetID,kFalse);
		this->EnableDisableWidgets(KN2PSQLEstatusParaExportarATWEBWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,kFalse);
		this->EnableDisableWidgets(KN2PSQLTimeToCheckUpdateElemsWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PResizeWidthWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PResizeHeightWidgetID,kFalse);
		
		this->EnableDisableWidgets(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,kFalse);
		this->EnableDisableWidgets(KN2PSQLRutaDePaginaComoPDFWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,kFalse);
		this->EnableDisableWidgets(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,kFalse);
		this->EnableDisableWidgets(kN2PSQLnameParaStyleCreditoWidgetID,kFalse);
		
	}
}


void N2PSQLPreferDlgObserver::HideShowWidgets(WidgetID widget,bool16 muestra)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		if(muestra==kTrue)
		{
			controlView->Show(kTrue);
		}
		else
		{
			controlView->Hide();
		}
		
	}while(false);
}

void N2PSQLPreferDlgObserver::EnableDisableWidgets(WidgetID widget,bool16 muestra)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		if(muestra==kTrue)
		{
			controlView->Enable(kTrue);
		}
		else
		{
			controlView->Disable(kTrue);
		}
		
	}while(false);
}



PMString N2PSQLPreferDlgObserver::GetStringOfWidget(WidgetID widget)
{
	PMString Data="";
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		
		InterfacePtr<ITextControlData>		TextControlData( controlView, UseDefaultIID() );
		if(TextControlData==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets TextControlData");
			break;
		}
		
		Data=TextControlData->GetString();
	}while(false);
	return(Data);
}



void N2PSQLPreferDlgObserver::SetStringOfWidget(WidgetID widget, PMString string)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		
		InterfacePtr<ITextControlData>		TextControlData( controlView, UseDefaultIID() );
		if(TextControlData==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets TextControlData");
			break;
		}
		string.SetTranslatable(kFalse);
		TextControlData->SetString(string);
	}while(false);
}

void N2PSQLPreferDlgObserver::LlenarComboPreferencias(PMString NomPref)
{
	PMString Archivo;
	PMString PathPMS;
	
	PMString Arc;
	int posicion;
		do
		{
			
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				break;
			}

			
			
			InterfacePtr<IControlView>		CVComboBoxSelecPrefer(panel->FindWidget(kN2PsqlDataBaseComboWidgetID), UseDefaultIID() );
			if(CVComboBoxSelecPrefer==nil)
			{
				break;
			}
			
			
			InterfacePtr<IStringListControlData> dropListData(CVComboBoxSelecPrefer, UseDefaultIID());
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			///borrado de la lista al inicializar el combo
			
			dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////COMENTARIO ES MUY EXTRAÑO QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   
			//remoeve los ultimos dos puntos
		    PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   
  			
  			if(PathPMS.IsEmpty() == kTrue)
			{
				break;
			}
			SDKFileHelper rootFileHelper(PathPMS);
			IDFile rootSysFile = rootFileHelper.GetIDFile();
			PlatformFileSystemIterator iter;
		
			
			iter.SetStartingPath(rootSysFile);
			
			PMString TypeFile="//*.*";
			IDFile sysFile;
			bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
			while(hasNext)
			{
				SDKFileHelper fileHelper(sysFile);
				PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
				
				if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
				{
					truncP=N2PSQLUtilities::TruncateExtencion(truncP);
					dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				}
				hasNext= iter.FindNextFile(sysFile);
			}
			
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				
				break;
			}
			
			if(dropListData->Length()>0)
			{
				posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
				if(posicion!=-1)
					dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
				posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
				if(posicion!=-1)
					dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
				posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
				if(posicion<0)
				{
					IDDLDrComboBoxSelecPrefer->Select(0);
				}
				else
				{
					IDDLDrComboBoxSelecPrefer->Select(posicion);
				}
			}
			
			
			
		}while(false);}

void N2PSQLPreferDlgObserver::BorrarPreferenciaActual()
{
	PMString Archivo;
	PMString PathPMS;
	CString Path="";
	PMString Arc;
		do
		{
			PreferencesConnection PrefConections;
			PMString NameDefaultPrefenceConnection="";
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
	
			if(UpdateStorys==nil)
			{
				break;			
			}
	
			if(UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				NameDefaultPrefenceConnection = PrefConections.NameConnection;
		
		
		
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
			
				break;
			}

			//obtengo una interfaz del la caja de vista previa
			InterfacePtr<IControlView>		editBoxView( panel->FindWidget(kN2PsqlDataBaseComboWidgetID), UseDefaultIID() );
			if(editBoxView==nil)
			{
			 
				break;
			}

			

			//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
			InterfacePtr<ITextControlData>	TextoDeDireccion( editBoxView, UseDefaultIID());
			if(TextoDeDireccion==nil)
			{
			
				break;
			}

			//Extraigo el texto del Widget
			Archivo=TextoDeDireccion->GetString();
			
			
			if(Archivo.NumUTF16TextChars()==0)
			{
				CAlert::ErrorAlert(kN2PSQLAlmenosUnaSeleccionStringKey);
				break;
			}
			PathPMS= N2PSQLUtilities::CrearFolderPreferencias();
			PathPMS.Append(Archivo+".pfa");//"C:Interlasa:News2Page 1.0:Preferencias:"+;
			//Path=PathPMS.GrabCString();
			const int ActionAgreedValue=1;
			int32 result=-1;
			if(NameDefaultPrefenceConnection==Archivo)
			{
				result=CAlert::ModalAlert(kN2PSQLSeguroBorrarPrefereDefaultStringKey,
						kYesString, 
						kCancelString,
						kNullString,
						ActionAgreedValue,CAlert::eQuestionIcon);
			}
			else
			{
				result=CAlert::ModalAlert(kN2PSQLSeguroBorrarPrefereStringKey,
						kYesString, 
						kCancelString,
						kNullString,
						ActionAgreedValue,CAlert::eQuestionIcon);
			}
			 
			if(ActionAgreedValue==result)
			{
				if(remove(InterlasaUtilities::MacToUnix(PathPMS.GrabCString()).GrabCString())==-1)
					CAlert::InformationAlert(kN2PSQLErrorDuranteBorradoStringKey);
				else
				{
					if(NameDefaultPrefenceConnection==Archivo)
					{
						PathPMS= N2PSQLUtilities::CrearFolderPreferencias();
						PathPMS.Append("Default.pfa");//"C:Interlasa:News2Page 1.0:Preferencias:"+;
						//Path=PathPMS.GrabCString();
						remove(InterlasaUtilities::MacToUnix(PathPMS.GrabCString()).GrabCString());
					}
					
					CAlert::InformationAlert(kN2PSQLPreferenciaBorradaStringKey);
				}
					
			}
		}while(false);
}

bool16 N2PSQLPreferDlgObserver::ValidaDatosConexion()
{
	bool16 retval=kFalse;
	PreferencesConnection PrefConections;
	
	//PrefConections.TypeConnection="SQL SERVER";
	//PrefConections.IPServerConnection=this->GetStringOfWidget(kN2PSQLIPServerOfDBEditWidgetID);
	PrefConections.NameConnection=this->GetStringOfWidget(kN2PSQLNameConectionDBWidgetID);
	PrefConections.DSNNameConnection=this->GetStringOfWidget(kN2PSQLDSNNameEditWidgetID);
	PrefConections.NameUserDBConnection=this->GetStringOfWidget(kN2PSQLUsuarioOfDBEditWidgetID);
	PrefConections.PwdUserDBConnection=this->GetStringOfWidget(kN2PSQLPassswordOfDBWidgetID);
	PrefConections.PathOfServerFile=this->GetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID);
	PrefConections.PathOfServerFileImages=this->GetStringOfWidget(kN2PSQLPathOfServerFileImagenesEditWidgetID);
	PrefConections.RutaDServidorRespNotas=this->GetStringOfWidget(kN2PSQLRutaDServidorRespNotasEditWidgetID);
	PrefConections.TimeToCheckUpdateElements = this->GetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID);
	PrefConections.N2PResizeHeight = this->GetTextValue(kN2PResizeHeightWidgetID);
	PrefConections.N2PResizeWidth = this->GetTextValue(kN2PResizeWidthWidgetID);
	
	PrefConections.ImagCarpInServFImages = this->GetStringOfWidget(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID);
	PrefConections.URLWebServices = this->GetStringOfWidget(kN2PSQLURLWebServicesEditWidgetID);
	
	PrefConections.PresetTOPDFEditorial=this->GetStringOfWidget(kN2PsqlExportPDFPresetsDropDownWidgetID);
	PrefConections.PresetTOPDFEditorialWEB=this->GetStringOfWidget(kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID);
	
	PrefConections.EstadoParaExportarATWEB=this->GetStringOfWidget(KN2PSQLEstatusParaExportarATWEBWidgetID);
	
	if(this->GetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID)==kTrue)
		PrefConections.LlenarTablaWEB=1;
	else
		PrefConections.LlenarTablaWEB=0;
			
	//if(PrefConections.IPServerConnection.NumUTF16TextChars()>1)
	//{
		if(PrefConections.NameConnection.NumUTF16TextChars()>1)
		{
			if(PrefConections.DSNNameConnection.NumUTF16TextChars()>1)
			{
				if(PrefConections.NameUserDBConnection.NumUTF16TextChars()>1)
				{
					if(PrefConections.PwdUserDBConnection.NumUTF16TextChars()>1)
					{
					
						if(PrefConections.PathOfServerFile.NumUTF16TextChars()>1 )
						{
							retval=kTrue;
						}
						else
						{
							CAlert::ErrorAlert("5");
						}
					}
					else
					{
						CAlert::ErrorAlert("4");
					}
				}
				else
				{
					CAlert::ErrorAlert("3");
				}
			}
			else
			{
				CAlert::ErrorAlert("2");
			}
		}
		else
		{
			CAlert::ErrorAlert("2");
		}
//	}
	//else
	///{
	//	CAlert::ErrorAlert("1");
	//}
	return(retval);
}

void N2PSQLPreferDlgObserver::SelectPathFileServer()
{
	do
	{
		const int32 cCountOfOptions = 1;
		K2Vector<PMString> optionsVec(cCountOfOptions);
		PMString assetPathTitle(kN2PsqlSelectPathStringKey);
		assetPathTitle.Translate();
		assetPathTitle.SetTranslatable(kFalse);
		SDKFolderChooser folderChooser;
		folderChooser.SetTitle(assetPathTitle);
		folderChooser.ShowDialog();
		if(!folderChooser.IsChosen())
		{
			break;
		}
		IDFile FileId = folderChooser.GetIDFile();
		
		PMString folder;
		FileUtils::IDFileToPMString(FileId, folder);
		if(folder.IsEmpty() == kTrue)
		{
			break;
		}	
		
		
		CAlert::InformationAlert(FileId.GetFileName());
		CAlert::InformationAlert(FileId.GetFileName());
		SetStringOfWidget(kN2PSQLPathOfServerFileEditWidgetID, FileId.GetFileName());
	}while(false);
	
}


void N2PSQLPreferDlgObserver::SetTextValue(WidgetID widget, PMReal value)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		
		InterfacePtr<ITextControlData>		TextControlData( controlView, UseDefaultIID() );
		if(TextControlData==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets TextControlData");
			break;
		}
		PMString string="";
		string.AppendNumber(value);
		string.SetTranslatable(kFalse);
		TextControlData->SetString(string);
	}while(false);
}

PMReal N2PSQLPreferDlgObserver::GetTextValue(WidgetID widget)
{
	PMReal value=0;
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		
		InterfacePtr<ITextControlData>		TextControlData( controlView, UseDefaultIID() );
		if(TextControlData==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets TextControlData");
			break;
		}
		PMString string="";
		string=TextControlData->GetString();
		value=string.GetAsNumber();
	}while(false);
	return(value);
}



void N2PSQLPreferDlgObserver::SetTriStateControlData(WidgetID widget,bool16 status)
{
	
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		
		InterfacePtr<ITriStateControlData>		TextControlData( controlView, UseDefaultIID() );
		if(TextControlData==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets TextControlData");
			break;
		}
		
		if(status==kTrue)
			TextControlData->SetState(ITriStateControlData::kSelected);
		else
			TextControlData->SetState(ITriStateControlData::kUnselected);
			
	}while(false);
}

bool16 N2PSQLPreferDlgObserver::GetTriStateControlData(WidgetID widget)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		
		InterfacePtr<ITriStateControlData>		TextControlData( controlView, UseDefaultIID() );
		if(TextControlData==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets TextControlData");
			break;
		}
		
		if(TextControlData->GetState()==ITriStateControlData::kSelected)
			retval=kTrue;
		else
			retval=kFalse;
			
	}while(false);
	return(retval);
}


void N2PSQLPreferDlgObserver::LlenarPDFComboBox(PMString PresetToPDFeditorial,PMString PresetToPDFeditorialWEB)
{
	
	do
	{
		
	
	
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panelData==nil)
			{
				break;
			}	
		
		IControlView* ComboCView = panelData->FindWidget(kN2PsqlExportPDFPresetsDropDownWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
		
		IControlView* ComboCViewEditWEB = panelData->FindWidget(kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID);
		ASSERT(ComboCViewEditWEB);
		if(ComboCViewEditWEB == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCViewEditWEB");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataWEB(ComboCViewEditWEB,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataWEB == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListDataWEB*");
	
			break;
		}
		
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPreferWEB( ComboCViewEditWEB, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPreferWEB==nil)
		{
			CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPreferWEB*");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListData*");
			
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
			break;
		}
		
		dropListDataWEB->Clear(kFalse, kFalse);
		dropListData->Clear(kFalse, kFalse);
		
		InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		if(piWorkspace==nil)
		{
			CAlert::InformationAlert("No pudo Obtener piWorkspace*");
			break;
		}
			
		InterfacePtr<IPDFExptStyleListMgr> piPDFExptStyleListMgr((IPDFExptStyleListMgr *) piWorkspace->QueryInterface(IID_IPDFEXPORTSTYLELISTMGR));
		if(piPDFExptStyleListMgr==nil)
		{
			CAlert::InformationAlert("No pudo Obtener piPDFExptStyleListMgr*");
			break;
		}
		
		int32 indexFromEditorial=0;
		int32 indexFromEditorialWEB=0;
		for(int32 i=0;i<piPDFExptStyleListMgr->GetNumStyles(); i++)
		{
			PMString pName="";
			ErrorCode er=piPDFExptStyleListMgr->GetNthStyleName (i, &pName);
		 	if(er==kSuccess)
		 	{
		 		dropListData->AddString(pName, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				dropListDataWEB->AddString(pName, IStringListControlData::kEnd, kFalse, kFalse);
		 	}
			
			if(pName==PresetToPDFeditorial)
			{
				indexFromEditorial=i;
			}
			
			if(pName==PresetToPDFeditorialWEB)
			{
				indexFromEditorialWEB=i;
			}
		}
		
		if(dropListData->Length()>0)//si se lleno con almenos un path el combo
		{
			if(indexFromEditorial>=0)
				IDDLDrComboBoxSelecPrefer->Select(indexFromEditorial);
			else
				IDDLDrComboBoxSelecPrefer->Select(0);
		}
		
		
		
		if(dropListDataWEB->Length()>0)//si se lleno con almenos un path el combo
		{
			if(indexFromEditorial>=0)
				IDDLDrComboBoxSelecPreferWEB->Select(indexFromEditorial);
			else
				IDDLDrComboBoxSelecPreferWEB->Select(0);
		}
	}while(false);
	
}
//  Generated by Dolly build 17: template "Dialog".
// End, N2PSQLPreferDlgObserver.cpp.


