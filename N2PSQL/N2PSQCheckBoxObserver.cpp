//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/pictureicon/PicIcoSuiteButtonObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #6 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"
#include "ITextControlData.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"

#include "IDropDownListController.h"
#include "IStringListControlData.h"

// Interface includes
#include "ISubject.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// Implem includes

#include "CAlert.h"
#include "CObserver.h"

#ifdef MACINTOSH
	#include "N2PsqlID.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"

	#include "N2PCheckInOutID.h"
	#include "ICheckInOutSuite.h"

	#include "IN2PAdIssueUtils.h"

	#include"IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "N2PsqlID.h"
	#include "N2PSQLUtilities.h"

	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"

	#include "..\N2PCheckInOut\N2PCheckInOutID.h"
	#include "..\N2PCheckInOut\ICheckInOutSuite.h"

	#include "..\N2PAdornmentIssue\IN2PAdIssueUtils.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif
/**
	Adding an IObserver interface to a widget boss class creates a button
	whose presses can be registered as Update messages to the client code. 

	@ingroup pictureicon
	
*/
class N2PSQCheckBoxObserver : public CObserver
{
public:
	/**
		Constructor 
	*/		
	N2PSQCheckBoxObserver(IPMUnknown *boss);
	/**
		Destructor
	*/
	~N2PSQCheckBoxObserver();
	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called when the 
		widget boss object is shown.
	*/		
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when the widget is hidden.
	*/		
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
	
private:
	/*
	*/
	void updateRegister(const bool16& valor);
};

CREATE_PMINTERFACE(N2PSQCheckBoxObserver, kN2PSQCheckBoxObserverImpl)


N2PSQCheckBoxObserver::N2PSQCheckBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


N2PSQCheckBoxObserver::~N2PSQCheckBoxObserver()
{
	
}


void N2PSQCheckBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}


void N2PSQCheckBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}



void N2PSQCheckBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if(theChange == kTrueStateMessage || theChange == kFalseStateMessage) 
	{
		if(theChange==kTrueStateMessage)
		{
			this->updateRegister(1);
		}
		else
		{
			this->updateRegister(0);
		}
		
	}	
}

void N2PSQCheckBoxObserver::updateRegister(const bool16& valor)
{
	do
	{
		PMString Id_Elemento="";		//Id del Elemento o de la foto
		PMString DSNName ="";			//Nombre del DSN seleccionado
		PMString StringConection="";	//Cadena de la conexion
		K2Vector<PMString> QueryVector;	//Resultado para la consulta
		//Si existen usuarios logeado
		if(!N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			break;
				
		///
		InterfacePtr<IWidgetParent> widgetParent( this, UseDefaultIID());
		if(widgetParent==nil)
		{
			break;
		}
			
		InterfacePtr<IPanelControlData>	iPanelControlData( (IPanelControlData*)widgetParent->QueryParentFor(IID_IPANELCONTROLDATA) );
		if(iPanelControlData==nil)
		{
			break;
		}
				
		IControlView* FechaControlView = iPanelControlData->FindWidget(kN2PsqlIdElePhotoLabelWidgetID);
		ASSERT(FechaControlView);
		if(!FechaControlView)
		{
			break;
		}
				
		InterfacePtr<ITextControlData> TextFechaEditFechaControlView(FechaControlView, UseDefaultIID());
		if(TextFechaEditFechaControlView==nil)
		{
			break;
		}
			
		//Coloca la fecha seleccionada
		Id_Elemento=TextFechaEditFechaControlView->GetString();
		
		
		
		PMString QuerySatring="UPDATE Elemento SET Mostrar_Indesign=";
		QuerySatring.AppendNumber(valor);
		QuerySatring.Append(" Where Id_Elemento=");
		QuerySatring.Append(Id_Elemento);
		//QuerySatring.Append("' SELECT Top 1 * FROM Elemento");
		
		///////////
		PreferencesConnection PrefConectionsOfServerRemote;
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		//Obtiene el DSN Seleccionado o nombre del servidor
		DSNName = "Default";//N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		//Obtiene preferencias
		if( !UpdateStorys->GetDefaultPreferencesConnection(PrefConectionsOfServerRemote,kFalse) )
		{
			break;
		}
		
		//Arma la cadena para la conexion
		StringConection.Append("DSN=" + PrefConectionsOfServerRemote.DSNNameConnection + ";UID=" + PrefConectionsOfServerRemote.NameUserDBConnection + ";PWD=" + PrefConectionsOfServerRemote.PwdUserDBConnection);
		
		//Interface para la conexion
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
					
		ASSERT(SQLInterface);
		if (SQLInterface == nil) {	
			break;
		}
		
		SQLInterface->SQLSetInDataBase(StringConection,QuerySatring);
		//if()	//!SQLInterface->SQLQueryDataBase(StringConection,QuerySatring,QueryVector)	
		//{
		//	ASSERT_FAIL("Conexion fallo o update tuve Errores");
		//	break;
		//}
		
	}while(false);
}