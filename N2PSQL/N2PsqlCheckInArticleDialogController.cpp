/*
//	File:	N2PsqlCheckInArticleDialogController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "N2PSQLListBoxHelper.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "IApplication.h"



#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
//#include "DocumentID.h"
#include "OpenPlaceID.h"


// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "SDKUtilities.h"
#include "CAlert.h"
#include "SDKUtilities.h"

#include <stdlib.h>
#include <stdio.h>




// Project includes:
#include "N2PsqlID.h"
#include "UpdateStorysAndDBUtilis.h"
#include "N2PSQLUtilities.h"

#ifdef WINDOWS
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"

	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
#endif

#ifdef MACINTOSH
	#include "N2PCheckInOutID.h"

	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
#endif

////////////////



/** N2PsqlCheckInArticleDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class N2PsqlCheckInArticleDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PsqlCheckInArticleDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~N2PsqlCheckInArticleDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		virtual void UserCancelled();
	private:

		void LLenadoDeCombos(PMString IDNota);
		
		bool16 LLenar_Combo_EstatusNota(PMString& ID_EstatusNota);
		
		void SeleccionarCadenaEnComboBox(PMString Cadena, WidgetID widget);
		
		void LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		void LLenar_Combo_Seccion(PMString Id_PublicacionText);
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
		
		int32 EnableDisableWidget(WidgetID widget,bool16 Enabled);
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
		
		PMString Seccion ;
		PMString Publicidad ;
	
	
		bool16 IsEnableWidget(WidgetID widget);
	
	void Llenar_Combo_guias(PMString Seccion, PMString guiaSelected);
	
	void HideShowWidgets(WidgetID widget,bool16 muestra);

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(N2PsqlCheckInArticleDialogController, kN2PsqlCheckInArticleDialogControllerImpl)

/* ApplyDialogFields
*/
void N2PsqlCheckInArticleDialogController::InitializeDialogFields(IActiveContext* context) 
{


	
	int32 numeroPaginaActual=0;
	bool16 retval=kFalse;
	int32 UIDTextModelOfTextFrame=0;
	PMString IDNota="";
	UIDTextModelOfTextFrame = N2PSQLUtilities::GetUIDOfTextFrameSelect(numeroPaginaActual);
	
	/*InterfacePtr<IApplication> app(gSession->QueryApplication());
	PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
	NomAplicacionActual.SetTranslatable(kFalse);
	if(NomAplicacionActual.Contains("InCopy"))
	{
		this->EnableDisableWidget(kN2PsqlTextStatusWidgetID,kFalse);
		this->EnableDisableWidget(kN2PsqlComboRoutedToWidgetID,kFalse);
	}*/
	//this->LLenar_Combo_EstatusNota();
	
	
	//Selecciona la seccion Anterior
	this->LLenar_Combo_Publicacion(Publicidad);
	
	
	//Selecciona la seccion Anterior
	this->LLenar_Combo_Seccion(Seccion);	
	

		
	
		
	//SI NO SE ENCUENTRA SELECCIONADO UN TEXTFRAME
	if(UIDTextModelOfTextFrame>0)
	{
		bool16 PudeModificarGuia=kFalse;
		retval= N2PSQLUtilities::GetIDNota_ConIDTextFrame_SobreXMPNotasFluidas(UIDTextModelOfTextFrame,IDNota,PudeModificarGuia);
		if(retval==kTrue)
		{
			
			//CAlert::InformationAlert("1");
			this->LLenadoDeCombos(IDNota);
			if(!PudeModificarGuia)
				this->EnableDisableWidget(kN2PsqlTextEnGuiaWidgetID, PudeModificarGuia);//  kN2PsqlTextEnGuiaWidgetID
		}
		else
		{
			//
			//Cerra Dialogo
		}
		
	}
	else
	{
		//CAlert::InformationAlert("4");
		///Carra Dialogo
	}
	
	PMString CadenaOk="OK";
	CadenaOk.SetTranslatable(kFalse);
	this->SetTextControlData(kN2PsqlDlgChekInNotaCancelWidgetID,CadenaOk);
	
	
	
}

void N2PsqlCheckInArticleDialogController::UserCancelled()
{
	PMString CadenaCancel="Cancel";
	CadenaCancel.SetTranslatable(kFalse);
	this->SetTextControlData(kN2PsqlDlgChekInNotaCancelWidgetID,CadenaCancel);
}


/* ValidateDialogFields
*/
WidgetID N2PsqlCheckInArticleDialogController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result = kNoInvalidWidgets;
	// Put code to validate widget values here.
	do
	{
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			
			
		if(UpdateStorys==nil)
		{
			
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			
			break;
		}
		
		PMString Publicacion =  this->GetTextControlData(kN2PsqlComboPubliWidgetID);
		if(Publicacion.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Seccion =this->GetTextControlData(kN2PsqlComboSeccionWidgetID);
		if(Seccion.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Estatus =this->GetTextControlData(kN2PsqlTextStatusWidgetID);
		if(Estatus.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlTextStatusWidgetID;
			break;
		}
		
		
		PMString GuiaS ="";
		if(PrefConections.N2PUtilizarGuias==1)
			GuiaS=this->GetTextControlData(kN2PsqlComboguiaWidgetID);
		else
			GuiaS=this->GetTextControlData(kN2PsqlTextEnGuiaWidgetID);
		
		//CAlert::InformationAlert(GuiaS);
		if(GuiaS.Contains("'") || GuiaS.IndexOfWChar(34) >=0)
		{
			CAlert::InformationAlert(kN2PSQLValidaComilasYDComillasStringKey);
			result = kN2PsqlTextEnGuiaWidgetID;
			break;
		}
		
		if(PrefConections.EsCampoGuiaObligatorio>0)
 		{
			if(IsEnableWidget(kN2PsqlTextEnGuiaWidgetID))
			{
				if(GuiaS.NumUTF16TextChars()<=0)
				{
					CAlert::InformationAlert(kN2PSQLGuiaNoVaciaStringKey);
					result = kN2PsqlTextEnGuiaWidgetID;
					break;
				}
			}
 			
 		}
 		
 		
		
	}while(false);


	return result;
}

/* ApplyDialogFields
*/
void N2PsqlCheckInArticleDialogController::ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId) 
{

	SystemBeep();  
}




bool16 N2PsqlCheckInArticleDialogController::LLenar_Combo_EstatusNota(PMString& ID_EstatusNota)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlTextStatusWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			
			
		if(UpdateStorys==nil)
		{
			
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT SQL_CACHE  Id_Estatus,Nombre_Estatus From Estatus_Elemento WHERE Id_tipo_ele='10' ORDER BY Nombre_Estatus ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		PMString cadena="";
		PMString NombreEstatus="";
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			NombreEstatus=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			if(ID_EstatusNota==cadena)
			{
				ID_EstatusNota=NombreEstatus;
			}
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(NombreEstatus, IStringListControlData::kEnd, kFalse, kFalse);
			
		}

		cadena = "";
		cadena.SetTranslatable(kFalse);
		dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		

		
		
		int32 Index=dropListData->GetIndex(ID_EstatusNota);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		
	}while(false);
	return(kTrue);
}

void N2PsqlCheckInArticleDialogController::LLenadoDeCombos(PMString IDNoda)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboRoutedToWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataDirigidoa(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataDirigidoa == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos dropListDataDirigidoa");
			break;
		}
		dropListDataDirigidoa->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Id_Usuario From Usuario ORDER By Id_Usuario ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		
		//llenado del combo Dirigido A
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListDataDirigidoa->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		cadena = "";
		cadena.SetTranslatable(kFalse);
		dropListDataDirigidoa->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);

		
		//Para traeme los parametros del ultimo checkIn de la nota
	
		Busqueda="";
		Busqueda.Append("CALL QueryDatosParaCheckInNota2(" + IDNoda + ")");
		
		PMString Estatus="";
		PMString Asignado="";
		PMString Id_Publicacion="";
		PMString Id_Seccion="";
		PMString NombreOGuia="";
		//CAlert::InformationAlert(Busqueda);
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		
		//llenado del combo Dirigido A
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			Estatus=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			Asignado=SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[i]);
			Id_Publicacion=SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
			Id_Seccion=SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
			NombreOGuia=SQLInterface->ReturnItemContentbyNameColumn("Nombre",QueryVector[i]);
		}
		
		//CAlert::InformationAlert(Estatus);
		//SELECCIONA EL USUARIO ASIGNADO EN EL COMBO	
		SeleccionarCadenaEnComboBox(Asignado, kN2PsqlComboRoutedToWidgetID);
		
	this->LLenar_Combo_EstatusNota(Estatus);
		//SeleccionarCadenaEnComboBox(Estatus, kN2PsqlTextStatusWidgetID);
		
		//Selecciona la seccion Anterior
	
	this->LLenar_Combo_Publicacion(Id_Publicacion);

	
	//Selecciona la seccion Anterior
	this->LLenar_Combo_Seccion(Id_Seccion);	
	
		if(PrefConections.N2PUtilizarGuias==1)
		{
			this->HideShowWidgets(kN2PsqlTextEnGuiaWidgetID,kFalse);
			this->HideShowWidgets(kN2PsqlComboguiaWidgetID,kTrue);
			this->Llenar_Combo_guias(Id_Seccion,NombreOGuia);
		}
		else
		{
			this->HideShowWidgets(kN2PsqlTextEnGuiaWidgetID, kTrue);
			this->HideShowWidgets(kN2PsqlComboguiaWidgetID,kFalse);
			this->SetTextControlData(kN2PsqlTextEnGuiaWidgetID,NombreOGuia);
		}	
	
	
	//////////////////////
	Busqueda="";
	Busqueda.Append("SELECT Id_Usuario FROM Bitacora_Elemento WHERE Id_Elemento=" + IDNoda +" AND Id_Evento IN(3,16,27)  ORDER BY ID DESC LIMIT 1");
	
	QueryVector.clear();
	SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);

	Busqueda="";
	//llenado del combo Dirigido A
	for(int32 i=0;i<QueryVector.Length();i++)
	{
			
		Busqueda=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
		
	}
	
	if(Busqueda.NumUTF16TextChars()>0)
		this->SetTextControlData(kN2PsqlUltimoUsuarioQModNotaWidgetID,Busqueda);
	
	//	SeleccionarCadenaEnComboBox(Id_Publicacion, kN2PsqlComboPubliWidgetID);
	//	SeleccionarCadenaEnComboBox(Id_Seccion, kN2PsqlComboSeccionWidgetID);
		
	}while(false);
}


void N2PsqlCheckInArticleDialogController::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL(" N2PsqlCheckInArticleDialogController::SeleccionarCadenaEnComboBox panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			ASSERT_FAIL(" N2PsqlCheckInArticleDialogController::SeleccionarCadenaEnComboBox ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataDirigidoa(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataDirigidoa == nil)
		{
			ASSERT_FAIL(" N2PsqlCheckInArticleDialogController::SeleccionarCadenaEnComboBox dropListDataDirigidoa");
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL(" N2PsqlCheckInArticleDialogController::SeleccionarCadenaEnComboBox IDDLDrComboBoxSelecPrefer");
			break;
		}

		
		int32 Index=dropListDataDirigidoa->GetIndex(CadenaASeleccionar);
		
		if(Index < 0)
		{
			Index=dropListDataDirigidoa->Length();
		}
		
		IDDLDrComboBoxSelecPrefer->Select(Index);
		
	}while(false);
}





void N2PsqlCheckInArticleDialogController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
	
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
	
		PMString NomPublicidad = this->GetTextControlData(kN2PsqlComboPubliWidgetID);
		
		
		int32 indexOfIssue = this->GetIdexSelectedOfComboBoxWidgetID(kN2PsqlComboPubliWidgetID, NomPublicidad);
		if(VectorIdPublicacion.Length()>0  )
		{
			
			if(indexOfIssue>=0 && indexOfIssue < VectorIdPublicacion.Length())
			{
				
				Publicidad = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",VectorIdPublicacion[indexOfIssue]);
				
			}
		}
			
		
		
		//Consulta
		PMString Busqueda="SELECT  SQL_CACHE  Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion='" + Publicidad + "' ORDER BY Nombre_de_Seccion ASC";
	
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		PMString cadena="";
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			//CAlert::InformationAlert(Id_SeccionText + " , " + SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]));
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}

		cadena="";
		cadena.SetTranslatable(kFalse);
		dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
		
			
	}while(false);
	
}




void N2PsqlCheckInArticleDialogController::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboPubliWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT SQL_CACHE  Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		PMString cadena="";
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}

		cadena="";
		cadena.SetTranslatable(kFalse);
		dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		

		//Selecciona el item deseado o la Publicacion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
			
	}while(false);
	
}


int32 N2PsqlCheckInArticleDialogController::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}


int32 N2PsqlCheckInArticleDialogController::EnableDisableWidget(WidgetID widget,bool16 Enabled)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		if(Enabled)
		{
			ComboCView->Enable(kTrue,kTrue);
		}
		else
		{
			ComboCView->Disable(kTrue);
		}
		
	}while(false);
	return(retval);
}


bool16 N2PsqlCheckInArticleDialogController::IsEnableWidget(WidgetID widget)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		retval=ComboCView->IsEnabled();
		
		
	}while(false);
	return(retval);
}

void N2PsqlCheckInArticleDialogController::Llenar_Combo_guias(PMString Seccion, PMString guiaSelected)
{
	
	do
	{
		
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboguiaWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);
		
		
		///borrado de la lista al inicializar el combo
		
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		if(SQLInterface==nil)
		{
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
		
		PMString NomPublicidad = this->GetTextControlData(kN2PsqlComboPubliWidgetID);
		
		
		int32 indexOfIssue = this->GetIdexSelectedOfComboBoxWidgetID(kN2PsqlComboPubliWidgetID, NomPublicidad);
		if(VectorIdPublicacion.Length()>0  )
		{
			
			if(indexOfIssue>=0 && indexOfIssue < VectorIdPublicacion.Length())
			{
				
				Publicidad = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",VectorIdPublicacion[indexOfIssue]);
				
			}
		}
		
		
		
		//Consulta
		PMString Busqueda="SELECT SQL_CACHE a.id, a.nombre FROM dguias a INNER JOIN mguias b ON a.id_mguias=b.id WHERE b.id_seccion="+Seccion+"  ORDER by a.nombre";
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		PMString cadena="";
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			//CAlert::InformationAlert(Id_SeccionText + " , " + SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]));
			if(guiaSelected==SQLInterface->ReturnItemContentbyNameColumn("nombre",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			cadena=SQLInterface->ReturnItemContentbyNameColumn("nombre",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}
		
		cadena="";
		cadena.SetTranslatable(kFalse);
		dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
		
		
	}while(false);
	
}


void N2PsqlCheckInArticleDialogController::HideShowWidgets(WidgetID widget,bool16 muestra)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets myParent");
			break;
		}
		
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets panel");
			break;
		}
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		controlView( panel->FindWidget(widget), UseDefaultIID() );
		if(controlView==nil)
		{
			ASSERT_FAIL("EnableDisableWidgets controlView");
			break;
		}
		if(muestra==kTrue)
		{
			controlView->Show(kTrue);
		}
		else
		{
			controlView->Hide();
		}
		
	}while(false);
}



//  Generated by Dolly build 17: template "Dialog".
// End, N2PsqlCheckInArticleDialogController.cpp.

