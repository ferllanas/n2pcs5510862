
//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/N2PSQLInspeccionaNotasTask.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #6 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// interface includes
#include "IApplication.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IIdleTaskMgr.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "CAlert.h"
#include "N2PSQLUtilities.h"
#include "Utils.h"
#include "IInCopyDocUtils.h"
#include "ILayoutUIUtils.h"
// General includes:

#include "CIdleTask.h"

// Project includes
#include "N2PsqlID.h"
#include "UpdateStorysAndDBUtilis.h"

const static int kkN2PSQLInspeccionaNotasExecInterval = 300*1000;	//10*500; // Refresh every 5 minutes in case we add new images
// milliseconds between checks

/** Implements IIdleTask, to refresh the view onto the file system.
	Just lets us keep up to date when people add new assets.
	This implementation isn't too respectful; it just clears the tree and calls ChangeRoot.

	
	@ingroup paneltreeview
*/

class N2PSQLInspeccionaNotasTask : public CIdleTask
{
public:

	/**	Constructor
		@param boss boss object on which this interface is aggregated
	*/
	N2PSQLInspeccionaNotasTask(IPMUnknown* boss);

	/**	Destructor
	*/
	virtual ~N2PSQLInspeccionaNotasTask() {}


	/**	Called by the idle task manager in the app core when the task is running
		@param appFlags [IN] specifies set of flags which task can check to see if it should do something
			this time round
		@param timeCheck [IN] specifies how many milliseconds before the next active task becomes overdue.
		@return uint32 giving the interval (msec) that should elapse before calling this back
	 */
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
protected:

	/**	Update the treeview.
	 */
	void refresh();
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PSQLInspeccionaNotasTask, kN2PSQLInspeccionaNotasTaskImpl)

/* Constructor
*/
N2PSQLInspeccionaNotasTask::N2PSQLInspeccionaNotasTask(IPMUnknown *boss)
	:CIdleTask(boss)
{
}


/* RunTask
*/
uint32 N2PSQLInspeccionaNotasTask::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{
	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		return kOnFlagChange;
	}
	
	this->refresh();
	// Has FS changed?
	PreferencesConnection PrefConections;
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			return kNextEventCycle;
		}
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			return kNextEventCycle;
		}
		
	if(PrefConections.TimeToCheckUpdateElements<=0)
	{
		this->UninstallTask();
		return kkN2PSQLInspeccionaNotasExecInterval;
	}
	
	PMString AAS="";
	AAS.AppendNumber( PrefConections.TimeToCheckUpdateElements);
	int32  kN2PSQLLogOffExecInterval2 =   (AAS.GetAsNumber() * 60) * 1000;//1 por 60 segundo = 1 minuto
	//CAlert::InformationAlert("Inspect UPDATE");
	// Has FS changed?
	return kN2PSQLLogOffExecInterval2;
}


/* TaskName
*/
const char* N2PSQLInspeccionaNotasTask::TaskName()
{
	return kN2PSQLInspeccionaNotasTaskKey;
}


/* refresh
*/
void N2PSQLInspeccionaNotasTask::refresh()
{
	do
	{
		
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
		if(UpdateStorys==nil)
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID, kFalse);
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID, kFalse);
			break;
		}
		PMString Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID, kFalse);
			break;
		}
		
		PMString NameOfDocument="";
		document->GetName(NameOfDocument); 

		if(Aplicacion.Contains("InCopy"))
		{
			if(N2PSQLUtilities::VerificarSiExisteDocumentoSobrePathInicial(Utils<ILayoutUIUtils>()->GetFrontDocument()))
			{
				//Para InDicar que hubo un cambio de DiseÒo
				if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument())))
				{
					N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID,kTrue);
					
					N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID, kN2PsqlStcTextActualizaPagsWidgetID,NameOfDocument);
				}
				else
				{
					N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID, UpdateStorys->SeRequiereActualizacionDeNotasOnDocuments());
				}
			}
			else
			{
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID,kTrue);
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID, kN2PsqlStcTextActualizaPagsWidgetID,NameOfDocument);
			}
			
		}
		else
		{
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlAlertActualizarNotasWidgetID, UpdateStorys->SeRequiereActualizacionDeNotasOnDocuments());
		}
		
		/*if(UpdateStorys->SeRequiereActualizacionDeNotasDeDocument())
		{
			
			//UpdateStorys->Update();
			//UpdateStorys->MakeUpdate();
			//UpdateStorys->Fotos_ActualizaLinks();
		}*/
			
	} while(kFalse);
}

//	end, File:	N2PSQLInspeccionaNotasTask.cpp
