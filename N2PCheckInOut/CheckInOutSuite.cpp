/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "SDKUtilities.h"
// Interface includes:
#include "ISelectionManager.h" // required by selection templates.
#include "ICommandSequence.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IDialog.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ISelectableDialogSwitcher.h"
#include "IDocument.h"
#include "IDocumentUtils.h"
#include "IHierarchy.h"
#include "IPageList.h"
#include "IDialogController.h"
#include "IPageSetupPrefs.h"
#include "PreferenceUtils.h"
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "SnapshotUtils.h"
#include "SDKLayoutHelper.h"
#include "StreamUtil.h"
#include "ISpreadList.h"
#include "FileUtility.h"

// General includes:
#include "CPMUnknown.h"
#include "CmdUtils.h"	// so that the thing will compile (selectionasbtemplates.tpp)
#include "SelectionASBTemplates.tpp"
#include "ILayoutUIUtils.h"
#include "CAlert.h"
#include "FileUtils.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"
#include "IInCopyDocUtils.h"



#ifdef WINDOWS
	#include <sys/types.h> 
	#include <sys/stat.h> 
	
	#include "IN2PXMPUtilities.h"

	#include "ICheckInOutSuite.h"
	#include "N2PCheckInOutID.h"
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"

	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"

	#include "..\InterlasaUltraProKeys\InterUltraProKyID.h"
	#include "..\InterlasaUltraProKeys\IInterUltraProKy.h"
	#include "..\InterlasaUltraProKeys\IInterUltraObjectProKy.h"
#endif

#ifdef MACINTOSH
	#include <types.h> 
	#include <sys/stat.h> 
	
	#include "IN2PXMPUtilities.h"

	#include "ICheckInOutSuite.h"
	#include "N2PCheckInOutID.h"
	#include "InterlasaUtilities.h"

	#include "N2PsqlLogInLogOutUtilities.h"

	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"


	#include "N2PwsdlcltID.h"
	#include "N2PWSDLCltUtils.h"
#endif

ClassDialogCheckOutPaginaSets SetsCheckOutPagina;
/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class CheckInOutSuite : public CPMUnknown<ICheckInOutSuite>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	CheckInOutSuite (IPMUnknown *boss);

	/** Destructor.
	 */
//	virtual ~CheckInOutSuite (void);
	/*
	*/
	bool16 CheckIn(void);

	/**

	*/
	bool16 CheckOut(void);

	/**

	*/
	bool16 SaveRevi(bool16 ToCheckInOnDB);
	
	bool16 AutomaticSaveRevi(IDocument* document, const bool16 DuplicandoPagina);
	
	
	bool16 SetPreferencesParametros(PrefParamDialogInOutPage *PreferencesPara);
	
	bool16 GetPreferencesParametrosOfFile(PrefParamDialogInOutPage *PreferencesPara);
	
	//bool16 TomarFotosDeDocumento(PMString& FotoVersionPagina,PMString& FotoLastPagina,PMString NamePage,const bool16 isResize=kFalse, const PMReal& with=1);
	bool16 TomarFotosDeDocumento(PMString& FotoVersionPagina,
									PMString& FotoLastPagina,
								 PMString NamePage,
								 const bool16 isResize=kFalse,
								 const PMReal& alto=1, 
								 const PMReal& with=1);
	
	bool16 ActualizaFotosDeDocumento(PMString& RutaToSaveFile,PMString& RutaTOJpg,PMString NamePage);
	

	PMString FechaYHora();
	
	PMString FechaYHoraActual();
	
	bool16 InsertEnMovimientos(PMString &Busqueda);
	
	bool16 GetPreferencesParametrosOfDB(PrefParamDialogInOutPage *PreferencesPara);
	
	bool16 StoreProcCheckInPage(K2Vector<PMString>& QueryVector);
	
	bool16 StoreProcSaveRevPage(K2Vector<PMString>& QueryVector);
	
	bool16 StoreProcCheckOutPage(K2Vector<PMString>& QueryVector);

	bool16 ObtenerDatosDeCheckOutDPaginas(ClassDialogCheckOutPaginaSets& CheckOutPaginaSets);

	bool16 GuardaDatosDeCheckOutDPaginas(ClassDialogCheckOutPaginaSets CheckOutPaginaSets);
	
	bool16 TomarFotosDeSpread(PMString &RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage);
	
	bool16 DuplicarPagina();
	
	/*
		0 cancelar
		1 no 
		2 si
	*/
	int32 OpenDialogoQuestion(PMString Question);
	
private:

	void DoDialogCheckIn();

	void DoDialogCheckOut();

	bool16 DoDialogGuardaRev(bool16 ToCheckInOnDB);
	
	bool16 CreateFolder(PMString path,PMString foldername);
	
	int Contador_Archivos_Carpeta(PMString Dir);
	
	PMString ID_Evento;
	
	
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CheckInOutSuite, kCheckInOutSuiteImpl)


/* HelloWorld Constructor
*/
CheckInOutSuite::CheckInOutSuite(IPMUnknown* boss) 
: CPMUnknown<ICheckInOutSuite>(boss)
{
}

/* CheckInOutSuite Destructor

CheckInOutSuite::~CheckInOutSuite(void)
{
}
*/
/**
*/


bool16 CheckInOutSuite::CheckIn()
{
	bool16 retval=kFalse;
	do
	{
	
	
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		
		
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
		
			IDocument* myDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(myDoc==nil)
			{
				CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
			
			IDFile FileDD; 
			FileUtils::PMStringToIDFile(PrefConections.PathOfServerFile, FileDD);
		
			if(!FileUtils::IsDirectory(FileDD))
			{
				CAlert::InformationAlert(kN2PCkInOutAlertDontServerFileConectionStringKey);
				break;
			}
		
			SDKFileHelper fileHelper(PrefConections.PathOfServerFile + ":apaginas");
			ASSERT(fileHelper.IsExisting());
			if(!FileUtils::DoesFileExist(FileDD))
			{
				CAlert::InformationAlert(kN2PCkInOutAlertDontServerFileConectionStringKey);
				break;
			}
			
			//Para check In Paginas
			this->DoDialogCheckIn();
			retval=kTrue;
		}
		else
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
		}	
	}while(false);
	
	return(retval);
}

/**
*/
bool16 CheckInOutSuite::CheckOut()
{
	bool16 retval=kFalse;
	do
	{
	
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
		
			IDFile FileDD; 
			FileUtils::PMStringToIDFile(PrefConections.PathOfServerFile, FileDD);
		
			if(!FileUtils::IsDirectory(FileDD))
			{
				CAlert::InformationAlert(kN2PCkInOutAlertDontServerFileConectionStringKey);
				break;
			}
		
			SDKFileHelper fileHelper(PrefConections.PathOfServerFile + ":apaginas");
			ASSERT(fileHelper.IsExisting());
			if(!FileUtils::DoesFileExist(FileDD))
			{
				//CAlert::InformationAlert("No puede conectar al servidor.");
				break;
			}
			
			
			InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				break;
			}	
			
			PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
			
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			ASSERT(app);
			if (app == nil) {	
				break;
			}
			
			PMString Aplicacion=app->GetApplicationName();
			
			InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																						  (
																						   kN2PwsdlcltUtilsBoss,	// Object boss/class
																						   IN2PWSDLCltUtils::kDefaultIID
																						   )));
			
			if(!	N2PWSDLCltUtil)
			{
				CAlert::InformationAlert("salio");
				break;
			}
			PMString ResultString="";
			retval=N2PWSDLCltUtil->realizaConneccion(ID_Usuario,
													 "192.6.2.2", 
													 "",
													 Aplicacion, 
													 PrefConections.URLWebServices);
			if(!retval)
			{
				retval=kFalse;
			}
			/******DONGLE VERDE RED
			//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::CheckOut() Sentinel check");
			InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
			(
				kInterUltraObjetProKyBoss,	// Object boss/class
				IInterUltraObjectProKy::kDefaultIID
			)));
			
			if(UltraProCheck==nil)
			{
				 retval=kFalse;
				break;
			}
		
			PMString ResultString="";
			retval=UltraProCheck->RedValueN2PPlugin(ResultString);
			if(!retval)
			{

				CAlert::ErrorAlert(ResultString);
				 retval=kFalse;
				break;
			}
			*/
			//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::CheckOut() Sentinel check fin");

			/********* DONGLE ROJO LOCAL *************
					InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*> (CreateObject
					(
						kInterUltraProKyBoss,	// Object boss/class
						IInterUltraProKy::kDefaultIID
					)));

					if(UltraProCheck==nil)
					{
						
						retval=kFalse;
						break;
					}
					PMString ResultString="";
					retval=UltraProCheck->CheckDongleN2PPlugins(ResultString);
					if(!retval)
					{
						CAlert::ErrorAlert(ResultString);
						retval=kFalse;
						break;
					}
			*/
			this->DoDialogCheckOut();
			
			retval=kTrue;
		}
		else
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
		}
		
	}while(false);
	return(retval);
}

/**
*/
bool16 CheckInOutSuite::SaveRevi(bool16 ToCheckInOnDB)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::SaveRevi ini");
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
	
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			IDFile FileDD; 
			FileUtils::PMStringToIDFile(PrefConections.PathOfServerFile, FileDD);
		
			if(!FileUtils::IsDirectory(FileDD))
			{
				CAlert::InformationAlert(kN2PCkInOutAlertDontServerFileConectionStringKey);
				break;
			}
		
			SDKFileHelper fileHelper(PrefConections.PathOfServerFile + ":apaginas");
			ASSERT(fileHelper.IsExisting());
			if(!FileUtils::DoesFileExist(FileDD))
			{
				//CAlert::InformationAlert("No puede conectar al servidor.");
				break;
			}
			
			if(this->DoDialogGuardaRev(ToCheckInOnDB))
			{
				retval=kTrue;
			}
			
		}
		else
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
		}
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::SaveRevi fin");
	return(retval);
}

/**
*/
bool16 CheckInOutSuite::AutomaticSaveRevi(IDocument* document,const bool16 DuplicandoPagina)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::AutomaticSaveRevi ini");

	bool16 retval=kFalse;
	PMString NameDocumentActual="";
	PMString RutaPaInCopy="";
	do
	{	
			
			
			
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				
				/*SDKLayoutHelper fersdklayout;
				UIDRef docUIDRefFER = ::GetUIDRef(document);
				if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 6");
			*/
				InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
					(
						kN2PXMPUtilitiesBoss,	// Object boss/class
						IN2PXMPUtilities::kDefaultIID
					)));
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 7");
				*/
				//	Estructira que guarda los datos de la persona logeada
					PrefParamDialogInOutPage PreferencesPara;
				///Obtiene las Ultimas Preferencias del ultimo dialogo abierto para el save revision
				//this->GetPreferencesParametrosOfFile(&PreferencesPara);
				
				
				if(document==nil)
				{
					CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
					break;
				}
				document->GetName(NameDocumentActual);
				NameDocumentActual.Remove(NameDocumentActual.NumUTF16TextChars()-5,5);
				//Obtierne el Nombre del Documento actual
				
				PMString N2PSQLFolioPag  = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
				
				PMString N2PSQL_ID_Pag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
				
				//Arma la cadena para buscar los datos de la pagina de enfrente
				PMString Busqueda="SELECT P.Ruta_Elemento, P.Nombre_Archivo, P.Id_Seccion, (SELECT Nombre_Estatus FROM Estatus_Elemento WHERE Id_Estatus=P.Id_Estatus) as Id_Estatus, P.Id_Publicacion, DATE_FORMAT(P.Fecha_Edicion,'%Y-%m-%d') ";
				Busqueda.Append(" AS SoloDia, P.Dirigido_a,  DATE_FORMAT(P.Fecha_Ult_Mod,'%H:%i:%s' )"); 
				Busqueda.Append(" AS HoraIn, DATE_FORMAT(P.Fecha_Ult_Mod,'%m/%d/%Y') AS FechaIn FROM  Pagina P");
				Busqueda.Append("  WHERE  P.ID = ");
				Busqueda.Append(N2PSQL_ID_Pag);
				Busqueda.Append("");	
		
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 9");
				*/
				//Obtiene las Preferencias de coneccion  a BD
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
			
				if(UpdateStorys==nil)
				{
					break;
				}
				
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 10");
		*/
				PreferencesConnection PrefConections;
		
				if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				{
					break;
				}
			
				//Arma la cadena para la conexion ODBC
				PMString StringConection="";
		
				StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
				//Para obtener las funciones que realizan las conexiones a la BD y hace la conexion  y la consulta
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
			
				K2Vector<PMString> QueryVector;
				
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 11");
				*/
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
				//CAlert::InformationAlert(Busqueda);
			
				if(QueryVector.Length()>0)
				{
					//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
					PreferencesPara.Pagina_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",QueryVector[QueryVector.Length()-1]);
					PreferencesPara.Seccion_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[QueryVector.Length()-1]);
					PreferencesPara.Estado_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[QueryVector.Length()-1]);
					PreferencesPara.Issue_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[QueryVector.Length()-1]);
					PreferencesPara.Date_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("SoloDia",QueryVector[QueryVector.Length()-1]);
					PreferencesPara.DirididoA_To_SavePage =  SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[QueryVector.Length()-1]);
					RutaPaInCopy =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
				}
				else
				{
					CAlert::InformationAlert("Error 101 no se encontraron datos para el id de pagina especificado");
					break;
				}
				
					
			
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 12");
				*/
				
				//Para Obtener los datos de la persona que se logeo
				InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
				if (wrkSpcPrefs == nil)
				{	
					break;
				}		
		
				PMString ID_Usuario="";	
				PMString Applicacion="";	
				ID_Usuario.AppendNumber(wrkSpcPrefs->GetID_Usuario());
				Applicacion.Append(wrkSpcPrefs->GetNameApp());
		
				if(Applicacion.NumUTF16TextChars()<1)
				{
					CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
					retval=kFalse;
					break;
				}
				
				//////////
				
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 13");
				*/
				PMString FotoVersionPagina="";
				PMString FotoLastPagina="";
				PMString fecha_Guion="";
				PMString File="";
				if(N2PSQLFolioPag.NumUTF16TextChars()<3)
				{
					///
				}
				else
				{
				
					fecha_Guion= N2PSQLUtilities::GetXMPVar("N2PDate", document);
					SDKUtilities::Replace(fecha_Guion,"/","-");
					SDKUtilities::Replace(fecha_Guion,"/","-");
				
					
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 14");
					*/
					//Arma la ruta para os ducumento de Versiones y fotos o previews
					//Arma la Ruta para el Documento InDesign
					File= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(File);
					File.Append("apaginas");
					SDKUtilities::AppendPathSeparator(File);
					File.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(File);
					File.Append(N2PSQLUtilities::GetXMPVar("N2PFechaCreacionPath", document));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( N2PSQLUtilities::GetXMPVar("N2PNPubliCreacionPath", document));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( N2PSQLUtilities::GetXMPVar("N2PNSecCreacionPath", document) );
					SDKUtilities::AppendPathSeparator(File);
					File.Append( N2PSQLUtilities::GetXMPVar("N2PUsuarioCredor", document));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( PreferencesPara.Pagina_To_SavePage );
					File.Append(".indd");
					

					
					FotoVersionPagina= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(N2PSQLUtilities::GetXMPVar("N2PFechaCreacionPath", document));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append( N2PSQLUtilities::GetXMPVar("N2PNPubliCreacionPath", document));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append( N2PSQLUtilities::GetXMPVar("N2PNSecCreacionPath", document) );
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append( N2PSQLUtilities::GetXMPVar("N2PUsuarioCredor", document));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					
					FotoLastPagina= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(N2PSQLUtilities::GetXMPVar("N2PFechaCreacionPath", document));
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append( N2PSQLUtilities::GetXMPVar("N2PNPubliCreacionPath", document));
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append( N2PSQLUtilities::GetXMPVar("N2PNSecCreacionPath", document) );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append( N2PSQLUtilities::GetXMPVar("N2PUsuarioCredor", document));
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					
					
					FotoVersionPagina.Append(N2PSQLUtilities::GetXMPVar("N2PPagina", document));
					FotoLastPagina.Append(N2PSQLUtilities::GetXMPVar("N2PPagina", document));
				}
				
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 15");
				*/
				PMString Aplicacion="";
				PMString IDUsuario="";
				PMString HoraIn="";
				PMString FechaIn="";

				PMString fechaCrea=this->FechaYHora();
				PMString* Hora=fechaCrea.Substring(0,fechaCrea.IndexOfWChar(94));
				fechaCrea.Remove(0,fechaCrea.IndexOfWChar(94)+2);
				PMString horaCrea=Hora->GrabCString();
		
				InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
				if(app==nil)
				{
					break;
				}
					
				//nombre de la aplicacion en que se esta trabajando
				PMString nameApp=app->GetApplicationName();
				Aplicacion.Append(wrkSpcPrefs->GetNameApp());
				Aplicacion.SetTranslatable(kFalse);
				nameApp.SetTranslatable(kFalse);
				
				IDUsuario = wrkSpcPrefs->GetIDUserLogedString();
				HoraIn.Append(wrkSpcPrefs->GetHoraIn());
				FechaIn.Append(wrkSpcPrefs->GetFechaIn());
				
				PMString ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
					CAlert::InformationAlert("FerCanSaveDocumentAs 16");
				*/
				if(Applicacion.Contains("InCopy"))
				{
					ID_Evento="15";
					
					
					PMString SPCallPaginnaChickIn="CALL PaginaCheckInStoreProc(";
					SPCallPaginnaChickIn.Append(ID_Pagina);
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append(PreferencesPara.Issue_To_SavePage);
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Date_To_SavePage);
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Seccion_To_SavePage);
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(N2PSQLFolioPag);
					SPCallPaginnaChickIn.Append("',");
					SPCallPaginnaChickIn.Append("1");	//PLIEGO
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("0");	//PAR/IMPAR
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("1");	//ID-Color
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append(PreferencesPara.DirididoA_To_SavePage);	
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(IDUsuario);	//Proveniente_de
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("SQLServer");	//Servidor
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(RutaPaInCopy);	//Ruta_Elemento 
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Pagina_To_SavePage);	//Nombre_Archivo 
					SPCallPaginnaChickIn.Append("',");
					SPCallPaginnaChickIn.Append("@A");	//Fecha_Creacion
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("@X");	//Fecha_Ult_Mod
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("0");	//Webable
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("10");	//Calificacion
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append("Pagina");	//Id_TipoElemento
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Estado_To_SavePage);	//Id_Estatus
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("No se que es Cameo");	//Cameo
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(IDUsuario);	//Id_Empleado
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("0");	//Fech_Evento
					SPCallPaginnaChickIn.Append("',");
				
					SPCallPaginnaChickIn.Append(ID_Evento);	//Id_Evento
				
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append(FotoVersionPagina);	//Ruta_Previo
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("Check In");	//Descripcion_Evento
					SPCallPaginnaChickIn.Append("','");
				
					SPCallPaginnaChickIn.Append(RutaPaInCopy);	//Descripcion_Evento
					SPCallPaginnaChickIn.Append("');");
					
					//CAlert::InformationAlert(SPCallPaginnaChickIn);
					PMString StringConection="";
		
					StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
					if(!SQLInterface->SQLQueryDataBase(StringConection,SPCallPaginnaChickIn,QueryVector))
					{
						break;
					}
					
					
					
				
				}
				else
				{
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 17");
					*/
					if(DuplicandoPagina)
						ID_Evento="5";
					else
						ID_Evento="7";
					
					PMString NameDocUltimoSaveAs=PreferencesPara.Pagina_To_SavePage;
					
					
					//Hay que validar que ya se ha realizadon un chech in o save revition
					// y que la pagina se llame igual a nombre de pagina actual
					if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars() == 0)
					{
						
						//si no se ha realizado un previo save as o  save se habre el diago save as 
						this->SaveRevi(kTrue);
						break;
					}
					
					if(NameDocumentActual.Contains("Untitled") || NameDocumentActual.Contains("Sin tÌtulo-") || NameDocumentActual.Contains(".indt"))
					{
						
							this->SaveRevi(kTrue);
							break;
					}
					
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 18");
					*/
					
					///Para ver si el documento de enfrente se llama igual que el ultimo salvado
					/*if(NameDocumentActual!=NameDocUltimoSaveAs)
					{
						
						this->SaveRevi(kTrue);
						break;
					}*/
				
					
					
					//Guardar el Documento	
					UIDRef docUIDRef = ::GetUIDRef(document);	//UIDRef del Documento	
					IDFile sysFile=FileUtils::PMStringToSysFile(File);;			//Para guardar el Documento de InDesign
					
					//Guarda el Documento
					//SDKLayoutHelper helperLayout;
					//helperLayout.SaveDocumentAs(docUIDRef,sysFile,kFullUI);
					
					//Para hacer unicamente save no hacer saveas
					Utils<IDocumentUtils>()->DoSave(document); //docUtils((IDocumentUtils*)GetExecutionContextSession()->QueryInterface(IID_DOCUMENTUTILS));
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 19");
					*/
					//Oculta 
					InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
					(
						kN2PCTUtilitiesBoss,	// Object boss/class
						IN2PCTUtilities::kDefaultIID
					)));
			
					if(N2PCTUtils==nil)
					{
						break;
					}
				
					N2PCTUtils->OcultaNotasConFrameOverset();					
			
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 20");
				*/
					
					this->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));	
					//CAlert::InformationAlert("TomarFotosDeDocumento:"+FotoVersionPagina+"\n"+FotoLastPagina);
					//this->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));	
					
					//CAlert::InformationAlert("TomarFotosDeDocumento:"+FotoVersionPagina+"\n"+FotoLastPagina);
					
					
					
					PMString SPCallPaginnaChickIn="CALL PaginaCheckInStoreProc(";
					SPCallPaginnaChickIn.Append(ID_Pagina);
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append(PreferencesPara.Issue_To_SavePage);
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Date_To_SavePage);
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Seccion_To_SavePage);
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(N2PSQLFolioPag);
					SPCallPaginnaChickIn.Append("',");
					SPCallPaginnaChickIn.Append("1");	//PLIEGO
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("0");	//PAR/IMPAR
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("1");	//ID-Color
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append(PreferencesPara.DirididoA_To_SavePage);	
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(IDUsuario);	//Proveniente_de
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("SQLServer");	//Servidor
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(File);	//Ruta_Elemento 
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Pagina_To_SavePage);	//Nombre_Archivo 
					SPCallPaginnaChickIn.Append("',");
					SPCallPaginnaChickIn.Append("@A");	//Fecha_Creacion
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("@X");	//Fecha_Ult_Mod
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("0");	//Webable
					SPCallPaginnaChickIn.Append(",");
					SPCallPaginnaChickIn.Append("10");	//Calificacion
					SPCallPaginnaChickIn.Append(",'");
					SPCallPaginnaChickIn.Append("Pagina");	//Id_TipoElemento
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(PreferencesPara.Estado_To_SavePage);	//Id_Estatus
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("No se que es Cameo");	//Cameo
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append(IDUsuario);	//Id_Empleado
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("0");	//Fech_Evento
					SPCallPaginnaChickIn.Append("',");
				
					SPCallPaginnaChickIn.Append(ID_Evento);	//Id_Evento
				
					SPCallPaginnaChickIn.Append(",'");
					
					PMString CadenaSinDosRutas=FotoVersionPagina;
					
					
					//Remueve todo hasta a paginas
					if(CadenaSinDosRutas.NumUTF16TextChars()>0)
					{
						int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
						if(indexapaginas>0)
							CadenaSinDosRutas.Remove(0, indexapaginas+9);
					}
					//CadenaSinDosRutas.Remove(0, CadenaSinDosRutas.IndexOfWChar(58)+1);
					//CadenaSinDosRutas.Remove(0, CadenaSinDosRutas.IndexOfWChar(58)+1);
					SPCallPaginnaChickIn.Append(CadenaSinDosRutas);	//Ruta_Previo
					SPCallPaginnaChickIn.Append("','");
					SPCallPaginnaChickIn.Append("Save Revision");	//Descripcion_Evento
					SPCallPaginnaChickIn.Append("','");
					
					PMString CopiaToLast=FotoLastPagina;
					CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
					CopiaToLast.Append("_last.jpg");
					
					SPCallPaginnaChickIn.Append(CopiaToLast);	//Descripcion_Evento
					SPCallPaginnaChickIn.Append("');");
					
					//CAlert::InformationAlert(SPCallPaginnaChickIn);
					PMString StringConection="";
		
					StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
					if(!SQLInterface->SQLQueryDataBase(StringConection,SPCallPaginnaChickIn,QueryVector))
					{
						break;
					}
			
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 21");
					*/
					//CAlert::InformationAlert("1");
					
					this->SetPreferencesParametros(&PreferencesPara);

					//CAlert::InformationAlert("2");
					//Muestra
					InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
					(
						kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
						IUpdateStorysAndDBUtils::kDefaultIID
					)));
					
				//	CAlert::InformationAlert("3");
					if(UpdateStorys==nil)
					{
						break;
					}
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 22");
					*/
					//CAlert::InformationAlert("4");
		
					if( N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowTxtOversetEditWidgetID))
						UpdateStorys->DoMuestraNotasConFrameOverset();
					
					//CAlert::InformationAlert("5");
					/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
						CAlert::InformationAlert("FerCanSaveDocumentAs 23");
					 */
				}
		
				retval=kTrue;
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::AutomaticSaveRevi fin");
	return(retval);
}






int CheckInOutSuite::Contador_Archivos_Carpeta(PMString Dir)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::Contador_Archivos_Carpeta ini");
	int contador=0;
	
	PMString PathPMS = Dir;
	
	do
	{
				  
  		if(PathPMS.IsEmpty() == kTrue)
		{
			
			break;
		}
	
		SDKFileHelper rootFileHelper(PathPMS);
		IDFile rootIDFile = rootFileHelper.GetIDFile();
		PlatformFileSystemIterator iter;
		if(!iter.IsDirectory(rootIDFile))
		{
			break;
		}

		/*#ifdef WINDOWS
			// Windows dir iteration a little diff to Mac
			rootIDFile.Append(fFilter);
		#endif
		*/

		iter.SetStartingPath(rootIDFile);
		
		PMString TypeFile;
#ifdef MACINTOSH 
		TypeFile="*.jpg";
#endif
#ifdef WINDOWS 
		TypeFile="\\*.*";
#endif
		IDFile IDFile;
		bool16 hasNext= iter.FindFirstFile(IDFile,TypeFile);
		while(hasNext)
		{
			SDKFileHelper fileHelper(IDFile);
			PMString truncP = fileHelper.GetPath();
			if(truncP.Contains(".jpg") && truncP.NumUTF16TextChars()>=4)
			{
				 contador++;
			}
			hasNext= iter.FindNextFile(IDFile);
		}
	}while(false);
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::Contador_Archivos_Carpeta fin");
	return(contador);////retorna la posicion del laultima preferencia con que se trabajo
}
/* DoDialog
*/
void CheckInOutSuite::DoDialogCheckIn()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PCheckInOutPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kCheckInDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}
		
		// Open the dialog.
		dialog->Open(); 
	
		
	} while (false);			
}

void CheckInOutSuite::DoDialogCheckOut()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PCheckInOutPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kCheckOutDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);
}

/* DoDialog
*/
bool16 CheckInOutSuite::DoDialogGuardaRev(bool16 ToCheckInOnDB)
{
	bool16  retval=kFalse;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PCheckInOutPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kGuardarRevDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}
		
		// Open the dialog.
		dialog->Open(); 
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
				
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		//Para indicar que se debe hacer un ChecjIn Pero solo a la base de datos
		if(ToCheckInOnDB==kTrue)
			dialogController->SetTextControlData(kToCheckInOnDBWidgetID,"1");
		else
			dialogController->SetTextControlData(kToCheckInOnDBWidgetID,"0");
		
		dialog->WaitForDialog();
		
		PMString DialogwasCanceled =  dialogController->GetTextControlData(kToCheckInOnDBWidgetID);
		
		if(DialogwasCanceled!="Cancel" && DialogwasCanceled!="Cancelar")
		{
			retval=kTrue;
		}
	} while (false);	
	return(retval);		
}


bool16 CheckInOutSuite::GetPreferencesParametrosOfFile(PrefParamDialogInOutPage *PreferencesPara)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::GetPreferencesParametrosOfFile ini");
	bool16 retval=kTrue;
	FILE *ArchivoPref;
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	PathFile.Append("ParametrosDeDialogosN2PSQL.pfd");
	PMString PMSLinea="";
	PMString TMP="";
	char Linea[250];
	
	PMString PreferenciasStrings="";
	
	if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"r"))!=NULL)//FileUtils::OpenFile(PathFile.GrabCString(),"r"))!=NULL)//
	{
		while(fgets(Linea,80,ArchivoPref)!=NULL)
		{
			PMSLinea=Linea;
			PreferenciasStrings.Append(PMSLinea);
			retval=kTrue;	
		}
	
	}
	
		
		int32 index=PreferenciasStrings.IndexOfString("Estado_To_PageIn:");
		int32 LengthStrPref=-1;
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
			if(LengthStrPref>0)
				PreferencesPara->Estado_To_PageIn=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Estado_To_PageIn="";
		}
		
		
		
		
		index=PreferenciasStrings.IndexOfString("Seccion_To_PageIn:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
			if(LengthStrPref>0)
				PreferencesPara->Seccion_To_PageIn=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Seccion_To_PageIn="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("Pagina_To_PageIn:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
			if(LengthStrPref>0)
				PreferencesPara->Pagina_To_PageIn=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Pagina_To_PageIn="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("Issue_To_PageIn:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+16)-(index+16));
			if(LengthStrPref>0)
				PreferencesPara->Issue_To_PageIn=(PreferenciasStrings.Substring(index+16,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Issue_To_PageIn="";
		
		}
		
		index=PreferenciasStrings.IndexOfString("Date_To_PageIn:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+15)-(index+15));
			if(LengthStrPref>0)
				PreferencesPara->Date_To_PageIn=(PreferenciasStrings.Substring(index+15,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Date_To_PageIn="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("DirididoA_To_PageIn:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+20)-(index+20));
			if(LengthStrPref>0)
				PreferencesPara->DirididoA_To_PageIn=(PreferenciasStrings.Substring(index+20,LengthStrPref))->GrabCString();
			else
				PreferencesPara->DirididoA_To_PageIn="";
		}
		
		
		
		index=PreferenciasStrings.IndexOfString("Estado_To_SavePage:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+19)-(index+19));
			if(LengthStrPref>0)
				PreferencesPara->Estado_To_SavePage=(PreferenciasStrings.Substring(index+19,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Estado_To_SavePage="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("Seccion_To_SavePage:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+20)-(index+20));
			if(LengthStrPref>0)
				PreferencesPara->Seccion_To_SavePage=(PreferenciasStrings.Substring(index+20,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Seccion_To_SavePage="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("Pagina_To_SavePage:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+19)-(index+19));
			if(LengthStrPref>0)
				PreferencesPara->Pagina_To_SavePage=(PreferenciasStrings.Substring(index+19,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Pagina_To_SavePage="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("Issue_To_SavePage:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
			if(LengthStrPref>0)
				PreferencesPara->Issue_To_SavePage=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Issue_To_SavePage="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("Date_To_SavePage:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
			if(LengthStrPref>0)
				PreferencesPara->Date_To_SavePage=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
			else
				PreferencesPara->Date_To_SavePage="";
		}
		
		
		
		index=PreferenciasStrings.IndexOfString("DirididoA_To_SavePage:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+22)-(index+22));
			if(LengthStrPref>0)
				PreferencesPara->DirididoA_To_SavePage=(PreferenciasStrings.Substring(index+22,LengthStrPref))->GrabCString();
			else
				PreferencesPara->DirididoA_To_SavePage="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("StatusNotaPageOut:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
			if(LengthStrPref>0)
				PreferencesPara->StatusNotaPageOut=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
			else
				PreferencesPara->StatusNotaPageOut="";
		}
		
		
		
		
		index=PreferenciasStrings.IndexOfString("SeccionNotaPageOut:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+19)-(index+19));
			if(LengthStrPref>0)
				PreferencesPara->SeccionNotaPageOut=(PreferenciasStrings.Substring(index+19,LengthStrPref))->GrabCString();
			else
				PreferencesPara->SeccionNotaPageOut="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("PaginaNotaPageOut:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
			if(LengthStrPref>0)
				PreferencesPara->PaginaNotaPageOut=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
			else
				PreferencesPara->PaginaNotaPageOut="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("IssueNotaPageOut:");
		if(index>=0)
		{
				LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
			if(LengthStrPref>0)
				PreferencesPara->IssueNotaPageOut=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
			else
				PreferencesPara->IssueNotaPageOut="";
		}
	
		
		index=PreferenciasStrings.IndexOfString("DateNotaPageOut:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+16)-(index+16));
			if(LengthStrPref>0)
				PreferencesPara->DateNotaPageOut=(PreferenciasStrings.Substring(index+16,LengthStrPref))->GrabCString();
			else
				PreferencesPara->DateNotaPageOut="";
		}
		
		
		index=PreferenciasStrings.IndexOfString("DirididoANotaPageOut:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+21)-(index+21));
			if(LengthStrPref>0)
				PreferencesPara->DirididoANotaPageOut=(PreferenciasStrings.Substring(index+21,LengthStrPref))->GrabCString();
			else
				PreferencesPara->DirididoANotaPageOut="";
		}
		
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::GetPreferencesParametrosOfFile fin");
	return(retval);
}
	
	
	
bool16 CheckInOutSuite::SetPreferencesParametros(PrefParamDialogInOutPage *PreferencesPara)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::SetPreferencesParametros ini");
	bool16 retval=kFalse;
	FILE *ArchivoPref;
	PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
	PathFile.Append("ParametrosDeDialogosN2PSQL.pfd");
	if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"w"))!=NULL)//FileUtils::OpenFile(PathFile.GrabCString(),"w"))!=NULL)
	{
		
		fprintf(ArchivoPref,"%s\n","Parametros De Paginas en News2Page SQL ");
		fprintf(ArchivoPref,"Estado_To_PageIn:%s\n",PreferencesPara->Estado_To_PageIn.GrabCString());
		fprintf(ArchivoPref,"Seccion_To_PageIn:%s\n",PreferencesPara->Seccion_To_PageIn.GrabCString());
		fprintf(ArchivoPref,"Pagina_To_PageIn:%s\n",PreferencesPara->Pagina_To_PageIn.GrabCString());
		fprintf(ArchivoPref,"Issue_To_PageIn:%s\n",PreferencesPara->Issue_To_PageIn.GrabCString());
		fprintf(ArchivoPref,"Date_To_PageIn:%s\n",PreferencesPara->Date_To_PageIn.GrabCString());
		fprintf(ArchivoPref,"DirididoA_To_PageIn:%s\n",PreferencesPara->DirididoA_To_PageIn.GrabCString());
		
		fprintf(ArchivoPref,"Seccion_To_SavePage:%s\n",PreferencesPara->Seccion_To_SavePage.GrabCString());
		fprintf(ArchivoPref,"Pagina_To_SavePage:%s\n",PreferencesPara->Pagina_To_SavePage.GrabCString());
		fprintf(ArchivoPref,"Estado_To_SavePage:%s\n",PreferencesPara->Estado_To_SavePage.GrabCString());
		fprintf(ArchivoPref,"Issue_To_SavePage:%s\n",PreferencesPara->Issue_To_SavePage.GrabCString());		
		fprintf(ArchivoPref,"Date_To_SavePage:%s\n",PreferencesPara->Date_To_SavePage.GrabCString());	
		fprintf(ArchivoPref,"DirididoA_To_SavePage:%s\n",PreferencesPara->DirididoA_To_SavePage.GrabCString());
		
		fprintf(ArchivoPref,"StatusNotaPageOut:%s\n",PreferencesPara->StatusNotaPageOut.GrabCString());
		fprintf(ArchivoPref,"SeccionNotaPageOut:%s\n",PreferencesPara->SeccionNotaPageOut.GrabCString());
		fprintf(ArchivoPref,"PaginaNotaPageOut:%s\n",PreferencesPara->PaginaNotaPageOut.GrabCString());
		fprintf(ArchivoPref,"IssueNotaPageOut:%s\n",PreferencesPara->IssueNotaPageOut.GrabCString());
		fprintf(ArchivoPref,"DateNotaPageOut:%s\n",PreferencesPara->DateNotaPageOut.GrabCString());
		fprintf(ArchivoPref,"DirididoANotaPageOut:%s\n",PreferencesPara->DirididoANotaPageOut.GrabCString());
		
		fclose(ArchivoPref);
		retval=kTrue;
	}
	else
	{
		//CAlert::InformationAlert("ParametrosDeDialogosN2PSQL.pfa Functiuon No pudo Abrir Archivo para guardar preferencia");
		retval=kFalse;
	}
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::SetPreferencesParametros fin");
	return(retval);
}


bool16 CheckInOutSuite::CreateFolder(PMString path,PMString foldername) 
{ 
	//CAlert::InformationAlert("Antes");
	IDFile file;
	FileUtils::PMStringToIDFile(path,file);
	FileUtils::CreateFolderIfNeeded(file,kTrue);
	
	PMString Element=path;
	#ifdef WINDOWS
		//hay que buscar el similar para Windows
	#endif
	#ifdef MACINTOSH
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777); 
	#endif
	
	//CAlert::InformationAlert("Despues"+Element);
	return kTrue; 
}

bool16 CheckInOutSuite::InsertEnMovimientos(PMString &Busqueda)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::InsertEnMovimientos ini");
	bool16 retval=kFalse;
	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
	
		if(SQLInterface->SQLSetInDataBase(StringConection,Busqueda))
			 retval=kTrue;
		else
			 retval=kFalse;
		
		//return(retval);}
	}while(false);
	return(retval);
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::InsertEnMovimientos fin");
}

PMString CheckInOutSuite::FechaYHora()
{
  time_t tiempo;
  char cad[80];
  struct tm *tmPtr;

  tiempo = time(NULL);
  tiempo = tiempo+86400;
  
  
 // setlocale(LC_ALL,"C");

  tmPtr = localtime(&tiempo);
  
  
  
  PMString Idioma(kN2PImplIdiomaStringKey);
  Idioma.Translate();
  Idioma.SetTranslatable(kTrue);
  

  if(Idioma=="English")
  {
  	
  	strftime( cad, 80, "%H:%M:%S^ %m/%d/%Y", tmPtr );			//%H:%M:%S^ %A, %B %d, %Y

  }
  else
  {
  	if(Idioma=="Spanish")
  	{
  		 strftime( cad, 80, "%H:%M:%S^ %m/%d/%Y", tmPtr );		//"%H:%M:%S^ %A %d de %B de %Y"
  	}
  }
 
  PMString Fecha(cad);
  Fecha.Translate();
  Fecha.SetTranslatable(kTrue);
  return Fecha;
}

PMString CheckInOutSuite::FechaYHoraActual()
{
  time_t tiempo;
  char cad[80];
  struct tm *tmPtr;

  tiempo = time(NULL);
  tiempo = tiempo;
  
  
 // setlocale(LC_ALL,"C");

  tmPtr = localtime(&tiempo);
  
  
  
  PMString Idioma(kN2PImplIdiomaStringKey);
  Idioma.Translate();
  Idioma.SetTranslatable(kTrue);
  

  if(Idioma=="English")
  {
  	
  	strftime( cad, 80, "%H:%M:%S^ %m/%d/%Y", tmPtr );			//%H:%M:%S^ %A, %B %d, %Y

  }
  else
  {
  	if(Idioma=="Spanish")
  	{
  		 strftime( cad, 80, "%H:%M:%S^ %m/%d/%Y", tmPtr );		//"%H:%M:%S^ %A %d de %B de %Y"
  	}
  }
 
  PMString Fecha(cad);
  Fecha.Translate();
  Fecha.SetTranslatable(kTrue);
  return Fecha;
}


bool16 CheckInOutSuite::TomarFotosDeDocumento(PMString &RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage,const bool16 isResize,const PMReal& alto,const PMReal& with)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::TomarFotosDeDocumento ini");
	
	PMString RutaParaJPG="";
	PMString PaginaParaSavePagina="";
	do
	{
		
		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF document is invalid");
			break;
		}
		
		InterfacePtr<IPageSetupPrefs> iPageSetupPrefs(static_cast<IPageSetupPrefs *>(::QueryPreferences(IID_IPAGEPREFERENCES, document)));
		if(iPageSetupPrefs==nil)
		{
			break;
		}
		
		PMRect PaginaTam= iPageSetupPrefs->GetPageSizePref();

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars() -5,nombre.NumUTF16TextChars() );
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF db is invalid");
			break;
		}

			//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		   UIDList pageUIDs = UIDList(::GetDataBase(document));
		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		   ASSERT(iPageList);
		   if(!iPageList) 
		   {
		   		ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			   break;
		   }
		   // Setup the page UID list to include
		   // the pages you want to export
		   int32 cPages = iPageList->GetPageCount();
		   PMString hb="";
		   hb.AppendNumber(cPages);
		  
		 
		   		
		   for (int32 iPage = 0; iPage < cPages && iPage<2; iPage++ )
		   {
		   		
		   		
			   UID uidPage = iPageList->GetNthPageUID(iPage);
			   pageUIDs.Append(uidPage);
			   
			   
			   PMString FotoVersionPagina=RutaToSaveFile;
			   PMString	FotoLastPagina=RutaTOJpg;
			
			   SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			   SDKUtilities::AppendPathSeparator(FotoLastPagina);
			  
			   
			   	//Adiciona el Folder que corresponde al numero de pagina
			   	FotoVersionPagina.Append("Pagina_");
			    FotoVersionPagina.AppendNumber(iPage+1);
			    
			 
			    FotoLastPagina.Append("Pagina_");
			    FotoLastPagina.AppendNumber(iPage+1);
			    
			   
			    
			   	if(CreateFolder(FotoVersionPagina,Pagina_To_SavePage))
				{
					PMString PathPrueba=FotoVersionPagina;
					
					SDKUtilities::AppendPathSeparator(PathPrueba);
					
					//FotoLastPagina.Append("\\");
					PathPrueba.Append(Pagina_To_SavePage);
					//FotoLastPagina.Append(Pagina);
					PathPrueba.Append("_1.jpg");
					//FotoLastPagina.Append("_1.jpg");
					
					
					IDFile filePrueba;
					if(SDKUtilities::AbsolutePathToSysFile(PathPrueba, filePrueba)!=kSuccess)
					{
						//CAlert::InformationAlert("Error in conversion to IDFile");//PMString p("Error in conversion to IDFile");
						break;
					}
					else
					{	
						if (SDKUtilities::FileExistsForRead(FileUtils::SysFileToPMString(filePrueba)) != kSuccess)
						{
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
	
							FotoVersionPagina.Append(Pagina_To_SavePage);
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_1.jpg");
							FotoLastPagina.Append("_last.jpg");
							
							
						}
						else
						{
							int32 count=Contador_Archivos_Carpeta(FotoVersionPagina)+1;
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
							FotoVersionPagina.Append(Pagina_To_SavePage);
						
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_");
							FotoLastPagina.Append("_");
							//system(FotoVersionPagina.GrabCString());
							FotoVersionPagina.AppendNumber(count);
						
							FotoLastPagina.Append("last");
							FotoVersionPagina.Append(".jpg");
					
							FotoLastPagina.Append(".jpg");
							
								
						}
					}
				}
				
			  //N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::TomarFotosDeDocumento antes de exportar imagen");
				///////////////////////////////////
				//Para tomar Fot Version de Pagina
				///////////////////////////////////
				UIDRef pageUIDRef = UIDRef(db,uidPage);
				//SnapshotUtils Snapshot(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				//SnapshotUtils Snapshot(pageUIDRef);//,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,SnapshotUtils::kXPHigh
				//CAlert::InformationAlert("TomarFotosDeDocumento");
			   PMReal yScaleFactor=1.0;
			  
				 
			   SnapshotUtils Snapshot(pageUIDRef,  yScaleFactor, yScaleFactor, 72.0, 72.0, 0.0, IShape::kPreviewMode|IShape::kPrinting,
										  kFalse, kFalse, kFalse, SnapshotUtils::kXPMaximum, 7.0, kTrue);
				
				ErrorCode MyErr = Snapshot.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				/* specify ouput file */
				IDFile sysFile;
				sysFile = FileUtils::PMStringToSysFile(FotoVersionPagina);
				InterfacePtr<IPMStream> Stream(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (Stream == NULL)
					break;
				//return false;
				/* export TIFF preview */
				
				//Snapshot.ExportImageToJPEG(Stream,SnapshotUtils::kSSJPEGProgressive,SnapshotUtils::kXPMaximum);
				int32 encoding = SnapshotUtils::kSSJPEGProgressive;
				int32 quality = SnapshotUtils::kSSJPEGLowQuality;
				 Snapshot.ExportImageToJPEG(Stream, (SnapshotUtils::SSJPEGEncoding)encoding, (SnapshotUtils::SSJPEGQuality)quality);
				if(iPage==0)
				{
					PaginaParaSavePagina = FotoVersionPagina;
			   		RutaParaJPG = FotoLastPagina;
				}
				
				///////////////////////////////////
				//Para Tomar Foto Last de Pagina
				///////////////////////////////////
				
				IDFile sysFileLast = FileUtils::PMStringToSysFile(FotoLastPagina);
				FileUtility deletedFile(sysFileLast);
				deletedFile.DeleteFile(); 
				if(isResize)//, PMReal with
				{
					PMReal xScaleFactor=(with*1.0)/PaginaTam.Width();
					PMReal yScaleFactor=(alto*1.0)/PaginaTam.Height();
					SnapshotUtils SnapshotLastResize(pageUIDRef,  xScaleFactor, yScaleFactor, 72.0, 72.0, 0.0, IShape::kPreviewMode|IShape::kPrinting,
										  kFalse, kFalse, kFalse, SnapshotUtils::kXPMaximum, 7.0, kTrue);
				   
					ErrorCode MyErr = SnapshotLastResize.GetStatus();
					if (MyErr == kFailure)
					   break;
					//return false;
					/* specify ouput file */
					//IDFile sysFile;
					//sysFile = FileUtils::PMStringToSysFile(FotoVersionPagina);
					InterfacePtr<IPMStream> Stream(StreamUtil::CreateFileStreamWrite(sysFileLast, kOpenOut | kOpenTrunc));
					if (Stream == NULL)
					   break;
					//return false;
					/* export TIFF preview */
				   
					//SnapshotLastResize.ExportImageToJPEG(Stream,SnapshotUtils::kSSJPEGProgressive,SnapshotUtils::kXPMaximum);
					int32 encoding = SnapshotUtils::kSSJPEGProgressive;
					int32 quality = SnapshotUtils::kSSJPEGLowQuality;
					SnapshotLastResize.ExportImageToJPEG(Stream, (SnapshotUtils::SSJPEGEncoding)encoding, (SnapshotUtils::SSJPEGQuality)quality);
				}
				else
				{
					//FileUtils::CopyFile(sysFile,sysFileLast);
						if(!FileUtils::CopyFile(sysFile,sysFileLast))
						{
							PMReal xScaleFactor=1.00;//(with*1.0)/PaginaTam.OutputWidth();
							PMReal yScaleFactor=1.00;//(alto*1.0)/PaginaTam.OutputHeight();
							SnapshotUtils SnapshotLastResize(pageUIDRef,  xScaleFactor, yScaleFactor, 72.0, 72.0, 0.0, IShape::kPreviewMode|IShape::kPrinting,
															 kFalse, kFalse, kFalse, SnapshotUtils::kXPMaximum, 7.0, kTrue);
							
							ErrorCode MyErr = SnapshotLastResize.GetStatus();
							if (MyErr == kFailure)
							{
								break;
							}
							//return false;
							/* specify ouput file */
							//IDFile sysFile;
							//sysFile = FileUtils::PMStringToSysFile(FotoVersionPagina);
							InterfacePtr<IPMStream> Stream(StreamUtil::CreateFileStreamWrite(sysFileLast, kOpenOut | kOpenTrunc));
							if (Stream == NULL)
								break;
							//return false;
							/* export TIFF preview */
							
							//SnapshotLastResize.ExportImageToJPEG(Stream,SnapshotUtils::kSSJPEGProgressive,SnapshotUtils::kXPMaximum);
							int32 encoding = SnapshotUtils::kSSJPEGProgressive;
							int32 quality = SnapshotUtils::kSSJPEGLowQuality;
							SnapshotLastResize.ExportImageToJPEG(Stream, (SnapshotUtils::SSJPEGEncoding)encoding, (SnapshotUtils::SSJPEGQuality)quality);
							
						}
					
				}
				
				//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::TomarFotosDeDocumento despues de exportar imagen");
/*				//SnapshotUtils SnapshotLast(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				SnapshotUtils SnapshotLast(pageUIDRef,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,SnapshotUtils::kXPHigh);
				MyErr = SnapshotLast.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				// specify ouput file 
				//IDFile sysFile;
				
				
				
				InterfacePtr<IPMStream> StreamLast(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (StreamLast == NULL)
					break;
				//return false;
				// export TIFF preview 
				SnapshotLast.ExportImageToJPEG(StreamLast);
*/				
				//CAlert::InformationAlert(FotoLastPagina);
				#ifdef WINDOWS
					//hay que buscar el similar para Windows
				#endif
				#ifdef MACINTOSH
					chmod(InterlasaUtilities::MacToUnix(FotoLastPagina).GrabCString(),0777);
					PMString Element=FotoLastPagina;
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					
					chmod(InterlasaUtilities::MacToUnix(FotoLastPagina).GrabCString(),0777);
					chmod(InterlasaUtilities::MacToUnix(FotoVersionPagina).GrabCString(),0777);
				#endif
				
				
		   }
		   
		   
		   
		   if(cPages<2)
		   {
				
				

		   		PMString  ParaBorrar=RutaTOJpg;//:Volumes:192.168.213.22
				SDKUtilities::AppendPathSeparator(ParaBorrar);
		   		
				PMString ParaRenombrar = RutaTOJpg ;
		   		SDKUtilities::AppendPathSeparator(ParaRenombrar);

		   		ParaRenombrar.Append("Pagina_2A");
				SDKUtilities::AppendPathSeparator(ParaRenombrar);

		   		ParaBorrar.Append("Pagina_2");//+Pagina_To_SavePage+"_last.jpg"
		   		SDKUtilities::AppendPathSeparator(ParaBorrar);

		   		rename(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString(), InterlasaUtilities::MacToUnix(ParaRenombrar).GrabCString() );
		   		
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString());
		   		
		   		
		   		
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString()); 
		   		ParaBorrar.Append( Pagina_To_SavePage+"_last.jpg" );
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString());
		   		
		   		
		   }
		   
		   
	}while(false);
	
		RutaToSaveFile = PaginaParaSavePagina;
		RutaTOJpg = RutaParaJPG;
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::TomarFotosDeDocumento fin");
	return(kTrue);
}


bool16 CheckInOutSuite::ActualizaFotosDeDocumento(PMString& RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::ActualizaFotosDeDocumento ini");
	do
	{
	
		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF document is invalid");
			break;
		}

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars() -5,nombre.NumUTF16TextChars() );
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF db is invalid");
			break;
		}

			//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		   UIDList pageUIDs = UIDList(::GetDataBase(document));
		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		   ASSERT(iPageList);
		   if(!iPageList) 
		   {
		   		ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			   break;
		   }
		   // Setup the page UID list to include
		   // the pages you want to export
		   int32 cPages = iPageList->GetPageCount();
		   
		 
		   		
		   for (int32 iPage = 0;  iPage < cPages && iPage<2; iPage++ )
		   {
		   		
		   		
		   		
			   UID uidPage = iPageList->GetNthPageUID(iPage);
			   pageUIDs.Append(uidPage);
			   
			   
			 
			   PMString FotoVersionPagina=RutaToSaveFile;
			   PMString	FotoLastPagina=RutaTOJpg;
			   
			   FotoVersionPagina.Append(Pagina_To_SavePage );
			   SDKUtilities::AppendPathSeparator(FotoVersionPagina);

			   FotoLastPagina.Append(Pagina_To_SavePage );
			   SDKUtilities::AppendPathSeparator(FotoLastPagina);

			   	//Adiciona el Folder que corresponde al numero de pagina
			   	FotoVersionPagina.Append("Pagina_");
			    FotoVersionPagina.AppendNumber(iPage+1);
			    
			    FotoLastPagina.Append("Pagina_");
			    FotoLastPagina.AppendNumber(iPage+1);
			   
			  
			
			     
			   	if(CreateFolder(FotoVersionPagina,Pagina_To_SavePage))
				{
					PMString PathPrueba=FotoVersionPagina;
					SDKUtilities::AppendPathSeparator(PathPrueba);
					
					//FotoLastPagina.Append("\\");
					PathPrueba.Append(Pagina_To_SavePage);
					//FotoLastPagina.Append(Pagina);
					PathPrueba.Append("_last.jpg");
					//FotoLastPagina.Append("_1.jpg");
					
					IDFile filePrueba;
					if(SDKUtilities::AbsolutePathToSysFile(PathPrueba, filePrueba)!=kSuccess)
					{
						//PMString p("Error in conversion to IDFile");
						break;
					}
					else
					{	
						if (SDKUtilities::FileExistsForRead(FileUtils::SysFileToPMString(filePrueba)) != kSuccess)
						{
							
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
						
							FotoVersionPagina.Append(Pagina_To_SavePage);
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_last.jpg");
							FotoLastPagina.Append("_last.jpg");
							
							//CAlert::InformationAlert("1");
							//CAlert::InformationAlert(FotoVersionPagina);
							//CAlert::InformationAlert(FotoLastPagina);
						}
						else
						{
							int32 count=Contador_Archivos_Carpeta(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
							FotoVersionPagina.Append(Pagina_To_SavePage);
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_");
							FotoLastPagina.Append("_");
							//system(FotoVersionPagina.GrabCString());
							FotoVersionPagina.Append("last");
							FotoLastPagina.Append("last");
							FotoVersionPagina.Append(".jpg");
							FotoLastPagina.Append(".jpg");
							
							//CAlert::InformationAlert(FotoVersionPagina);
							//CAlert::InformationAlert(FotoLastPagina);
							//CAlert::InformationAlert("2");
						}
					}
				}
				
				///////////////////////////////////
				//Para tomar Fot Version de Pagina
				///////////////////////////////////
				UIDRef pageUIDRef = UIDRef(db,uidPage);
				
			   //N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::ActualizaFotosDeDocumento ants de exportar imagen");
				//SnapshotUtils Snapshot(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				//SnapshotUtils Snapshot(pageUIDRef);//,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse,SnapshotUtils::kXPHigh
				//CAlert::InformationAlert("TomarFotosDeSpread");
				SnapshotUtils Snapshot(pageUIDRef,  1.0, 1.0, 72.0, 72.0, 0.0, IShape::kPreviewMode|IShape::kPrinting,
										kFalse, kFalse, kFalse, SnapshotUtils::kXPMaximum, 7.0, kTrue);
										
				ErrorCode MyErr = Snapshot.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				/* specify ouput file */
				IDFile sysFile;
				
				
				sysFile = FileUtils::PMStringToSysFile(FotoVersionPagina);
				InterfacePtr<IPMStream> Stream(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (Stream == NULL)
					break;
				//return false;
				/* export TIFF preview */
				int32 encoding = SnapshotUtils::kSSJPEGProgressive;
				int32 quality = SnapshotUtils::kSSJPEGLowQuality;
				 Snapshot.ExportImageToJPEG(Stream, (SnapshotUtils::SSJPEGEncoding)encoding, (SnapshotUtils::SSJPEGQuality)quality);
				///////////////////////////////////
				//Para Tomar Foto Last de Pagina
				///////////////////////////////////
				
				
				IDFile sysFileLast = FileUtils::PMStringToSysFile(FotoLastPagina);
				FileUtils::CopyFile(sysFile,sysFileLast);
			   
			   //N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::ActualizaFotosDeDocumento despues de exportar imagen");
				
/*				
				//SnapshotUtils SnapshotLast(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				SnapshotUtils SnapshotLast(pageUIDRef,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,SnapshotUtils::kXPHigh);
				MyErr = SnapshotLast.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				// specify ouput file /
				//IDFile sysFile;
							
				
				InterfacePtr<IPMStream> StreamLast(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (StreamLast == NULL)
					break;
				//return false;
				// export TIFF preview /
				SnapshotLast.ExportImageToJPEG(StreamLast);
*/				
				#ifdef WINDOWS
					// bUSCAR EL SIMILAR PARA WINDOWS
					
				#endif
				#ifdef MACINTOSH
					chmod(InterlasaUtilities::MacToUnix(FotoLastPagina).GrabCString(),0777);
					chmod(InterlasaUtilities::MacToUnix(FotoVersionPagina).GrabCString(),0777);
				#endif
		   }
		   
		   
		   
		   if(cPages<2)
		   {
				
				

		   		PMString  ParaBorrar=Pagina_To_SavePage;
				SDKUtilities::AppendPathSeparator(ParaBorrar);
		   		ParaBorrar.Append("Pagina_2");
				SDKUtilities::AppendPathSeparator(ParaBorrar);
				ParaBorrar.Append(Pagina_To_SavePage+"_last.jpg");
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString());
		   }
		 //  RutaToSaveFile = FotoVersionPagina;
		//	   	RutaTOJpg = FotoLastPagina;
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::ActualizaFotosDeDocumento fin");
	return(kTrue);
}


/*
	Obtiene los campos con que fue guardada (save revision) de una pagina en la base de datos
*/
bool16 CheckInOutSuite::GetPreferencesParametrosOfDB(PrefParamDialogInOutPage *PreferencesPara)
{
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::GetPreferencesParametrosOfDB ini");
	bool16 retval=kFalse;
	do
	{
		//Obtiene el nombre del docum ento que se encuentra en frente
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
			//Verifica que no se un documento nuevo
			PMString NameDocumentActual="";
			document->GetName(NameDocumentActual);
			NameDocumentActual.Remove(NameDocumentActual.NumUTF16TextChars()-5,5);
		
			
			
			
		//Obtiene las Preferencias de coneccion  a BD
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
		
			//Preferencias de Coneccion
			PreferencesConnection PrefConections;
			
			
		
			if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			//Arma la cadena para la conexion ODBC
				PMString StringConection="";
		
				StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
			//Para obtener las funciones que realizan las conexiones a la BD y hace la conexion  y la consulta
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
			
				K2Vector<PMString> QueryVector;
				
				
				PMString Busqueda="Set dateformat dmy";
				//this->InsertEnMovimientos(Busqueda);
				
				PMString ID_Pagina = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
				if(ID_Pagina.NumUTF16TextChars()>0)
				{
					//Arma la cadena para buscar los datos de la pagina de enfrente
					//Busqueda="Select * From Movimientos";
					Busqueda="SELECT Folio_Pagina, Nombre_Archivo, Id_Seccion, Id_Publicacion, Id_Estatus, Dirigido_a ,DATE_FORMAT(Fecha_Edicion,'%m/%d/%Y') AS SoloDia FROM Pagina";
					Busqueda.Append("  WHERE  ID = ");
					Busqueda.Append(ID_Pagina);
					Busqueda.Append(" ");/* and  Nombre_Archivo = '");
					Busqueda.Append(NameDocumentActual);
					Busqueda.Append("'");*/
					
					
					SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
					//Por si no existe uno con ese Query
					if(QueryVector.Length()>0)
					{
						
						PreferencesPara->Pagina_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",QueryVector[QueryVector.Length()-1]);
						PreferencesPara->Seccion_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[QueryVector.Length()-1]);
						PreferencesPara->Estado_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[QueryVector.Length()-1]);
						PreferencesPara->Issue_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[QueryVector.Length()-1]);
						PreferencesPara->Date_To_SavePage = SQLInterface->ReturnItemContentbyNameColumn("SoloDia",QueryVector[QueryVector.Length()-1]);
						PreferencesPara->DirididoA_To_SavePage =  SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[QueryVector.Length()-1]);	
					}
				}
				
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::GetPreferencesParametrosOfDB fin");
	return(retval);
}



bool16 CheckInOutSuite::StoreProcCheckInPage(K2Vector<PMString>& QueryVector)
{

	bool16 retval=kFalse;
	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
	
		if(SQLInterface->StoreProcedureCheckInPage(StringConection,QueryVector))
			 retval=kTrue;
		else
			 retval=kFalse;
		
	}while(false);
	return(retval);
	
}

bool16 CheckInOutSuite::StoreProcSaveRevPage(K2Vector<PMString>& QueryVector)
{

	bool16 retval=kFalse;
	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
	
		if(SQLInterface->StoreProcedureSaveRevPage(StringConection,QueryVector))
			 retval=kTrue;
		else
			 retval=kFalse;
		
	}while(false);
	return(retval);
	
}


bool16 CheckInOutSuite::StoreProcCheckOutPage(K2Vector<PMString>& QueryVector)
{

	bool16 retval=kFalse;
	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
	
		if(SQLInterface->StoreProcedureCheckOutPage(StringConection,QueryVector))
			 retval=kTrue;
		else
			 retval=kFalse;
		
	}while(false);
	return(retval);
	
}


bool16 CheckInOutSuite::GuardaDatosDeCheckOutDPaginas(ClassDialogCheckOutPaginaSets CheckOutPaginaSets)
{
	SetsCheckOutPagina.Status_PageOut= CheckOutPaginaSets.Status_PageOut;
	SetsCheckOutPagina.Seccion_PageOut= CheckOutPaginaSets.Seccion_PageOut;
	SetsCheckOutPagina.Pagina_PageOut= CheckOutPaginaSets.Pagina_PageOut;
	SetsCheckOutPagina.Issue_PageOut= CheckOutPaginaSets.Issue_PageOut;
	SetsCheckOutPagina.Date_PageOut= CheckOutPaginaSets.Date_PageOut;
	SetsCheckOutPagina.DirididoA_PageOut= CheckOutPaginaSets.DirididoA_PageOut;
	//CAlert::InformationAlert(SetsCheckOutPagina.Status_PageOut);
	return(kTrue);
}

bool16 CheckInOutSuite::ObtenerDatosDeCheckOutDPaginas(ClassDialogCheckOutPaginaSets& CheckOutPaginaSets)
{
	CheckOutPaginaSets.Status_PageOut=SetsCheckOutPagina.Status_PageOut;
	CheckOutPaginaSets.Seccion_PageOut=SetsCheckOutPagina.Seccion_PageOut;
	CheckOutPaginaSets.Pagina_PageOut=SetsCheckOutPagina.Pagina_PageOut;
	CheckOutPaginaSets.Issue_PageOut=SetsCheckOutPagina.Issue_PageOut;
	CheckOutPaginaSets.Date_PageOut=SetsCheckOutPagina.Date_PageOut;
	CheckOutPaginaSets.DirididoA_PageOut=SetsCheckOutPagina.DirididoA_PageOut;
	//CAlert::InformationAlert(CheckOutPaginaSets.Status_PageOut);
	return(kTrue);
}





bool16 CheckInOutSuite::TomarFotosDeSpread(PMString &RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage)
{
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::TomarFotosDeSpread ini");

	PMString RutaParaJPG="";
	PMString PaginaParaSavePagina="";
	do
	{
		
		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF document is invalid");
			break;
		}

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars() -5,nombre.NumUTF16TextChars() );
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF db is invalid");
			break;
		}

		//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		InterfacePtr<ISpreadList> spreadList(document, UseDefaultIID());
		if(spreadList==nil) 
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			break;
		}
		   	
		   	int32 numOfSpread=spreadList->GetSpreadCount();
		   	if(numOfSpread==0)
		   	{
		   		ASSERT_FAIL("numOfSpread");
			   break;
		   	}
		   
		   UIDRef spreadUIDRef(db, spreadList->GetNthSpreadUID(0));//Sacar foto de la primera spread
		   
		   InterfacePtr<IHierarchy> spreadHierarchy(spreadUIDRef, UseDefaultIID());
		   
//		   UIDList pageUIDs = UIDList(::GetDataBase(document));
//		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
//		   ASSERT(iPageList);
//		   if(!iPageList) 
//		   {
//		   		ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
//			   break;
//		   }
//		   // Setup the page UID list to include
//		   // the pages you want to export
//		   int32 cPages = iPageList->GetPageCount();
//		   PMString hb="";
//		   hb.AppendNumber(cPages);
		  
		 
		   		
//		   for (int32 iPage = 0; iPage < cPages && iPage<2; iPage++ )
//		   {
		   		
		   		
//			   UID uidPage = iPageList->GetNthPageUID(iPage);
//			   pageUIDs.Append(uidPage);
			   
			   
			   PMString FotoVersionPagina=RutaToSaveFile;
			   PMString	FotoLastPagina=RutaTOJpg;
			
			   SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			   SDKUtilities::AppendPathSeparator(FotoLastPagina);
			  
			   
			   	//Adiciona el Folder que corresponde al numero de pagina
			   	FotoVersionPagina.Append("Pagina_");
			    FotoVersionPagina.AppendNumber(1);
			    
			 
			    FotoLastPagina.Append("Pagina_");
			    FotoLastPagina.AppendNumber(1);
			    
			   
			    
			   	if(CreateFolder(FotoVersionPagina,Pagina_To_SavePage))
				{
					PMString PathPrueba=FotoVersionPagina;
					
					SDKUtilities::AppendPathSeparator(PathPrueba);
					
					//FotoLastPagina.Append("\\");
					PathPrueba.Append(Pagina_To_SavePage);
					//FotoLastPagina.Append(Pagina);
					PathPrueba.Append("_1.jpg");
					//FotoLastPagina.Append("_1.jpg");
					
					
					IDFile filePrueba;
					if(SDKUtilities::AbsolutePathToSysFile(PathPrueba, filePrueba)!=kSuccess)
					{
						//CAlert::InformationAlert("Error in conversion to IDFile");//PMString p("Error in conversion to IDFile");
						break;
					}
					else
					{	
						if (SDKUtilities::FileExistsForRead(FileUtils::SysFileToPMString(filePrueba)) != kSuccess)
						{
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
	
							FotoVersionPagina.Append(Pagina_To_SavePage);
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_1.jpg");
							FotoLastPagina.Append("_last.jpg");
							
							
						}
						else
						{
							int32 count=Contador_Archivos_Carpeta(FotoVersionPagina)+1;
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
							FotoVersionPagina.Append(Pagina_To_SavePage);
						
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_");
							FotoLastPagina.Append("_");
							//system(FotoVersionPagina.GrabCString());
							FotoVersionPagina.AppendNumber(count);
						
							FotoLastPagina.Append("last");
							FotoVersionPagina.Append(".jpg");
					
							FotoLastPagina.Append(".jpg");
							
								
						}
					}
				}
				
				///////////////////////////////////
				//Para tomar Fot Version de Pagina
				///////////////////////////////////
				//UIDRef pageUIDRef = UIDRef(db,uidPage);
				//SnapshotUtils Snapshot(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				//SnapshotUtils Snapshot(pageUIDRef);//,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,SnapshotUtils::kXPHigh
				//CAlert::InformationAlert("TomarFotosDeDocumento");
				SnapshotUtils Snapshot(spreadUIDRef,  1.0, 1.0, 72.0, 72.0, 0.0, IShape::kPreviewMode|IShape::kPrinting,
										kFalse, kFalse, kFalse, SnapshotUtils::kXPMaximum, 7.0, kTrue);
				
				ErrorCode MyErr = Snapshot.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				/* specify ouput file */
				IDFile sysFile;
				sysFile = FileUtils::PMStringToSysFile(FotoVersionPagina);
				InterfacePtr<IPMStream> Stream(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (Stream == NULL)
					break;
				//return false;
				/* export TIFF preview */
				
				//Snapshot.ExportImageToJPEG(Stream,SnapshotUtils::kSSJPEGProgressive,SnapshotUtils::kXPMaximum);
				int32 encoding = SnapshotUtils::kSSJPEGProgressive;
				int32 quality = SnapshotUtils::kSSJPEGLowQuality;
				 Snapshot.ExportImageToJPEG(Stream, (SnapshotUtils::SSJPEGEncoding)encoding, (SnapshotUtils::SSJPEGQuality)quality);
//				if(iPage==0)
//				{
//					PaginaParaSavePagina = FotoVersionPagina;
//			   		RutaParaJPG = FotoLastPagina;
//				}
				PaginaParaSavePagina = FotoVersionPagina;
				RutaParaJPG = FotoLastPagina;
				///////////////////////////////////
				//Para Tomar Foto Last de Pagina
				///////////////////////////////////
				
				IDFile sysFileLast = FileUtils::PMStringToSysFile(FotoLastPagina);
				FileUtils::CopyFile(sysFile,sysFileLast);
				
/*				//SnapshotUtils SnapshotLast(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				SnapshotUtils SnapshotLast(pageUIDRef,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,SnapshotUtils::kXPHigh);
				MyErr = SnapshotLast.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				// specify ouput file 
				//IDFile sysFile;
				
				
				
				InterfacePtr<IPMStream> StreamLast(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (StreamLast == NULL)
					break;
				//return false;
				// export TIFF preview 
				SnapshotLast.ExportImageToJPEG(StreamLast);
*/				
				//CAlert::InformationAlert(FotoLastPagina);
				#ifdef WINDOWS
					//hay que buscar el similar para Windows
				#endif
				#ifdef MACINTOSH
					chmod(InterlasaUtilities::MacToUnix(FotoLastPagina).GrabCString(),0777);
					PMString Element=FotoLastPagina;
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					SDKUtilities::RemoveLastElement(Element);
					chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
					
					chmod(InterlasaUtilities::MacToUnix(FotoLastPagina).GrabCString(),0777);
					chmod(InterlasaUtilities::MacToUnix(FotoVersionPagina).GrabCString(),0777);
				#endif
				
				
//		   }
		   
		   
		   
		   if(numOfSpread<2)
		   {
				
				

		   		PMString  ParaBorrar=RutaTOJpg;//:Volumes:192.168.213.22
				SDKUtilities::AppendPathSeparator(ParaBorrar);
		   		
				PMString ParaRenombrar = RutaTOJpg ;
		   		SDKUtilities::AppendPathSeparator(ParaRenombrar);

		   		ParaRenombrar.Append("Pagina_2A");
				SDKUtilities::AppendPathSeparator(ParaRenombrar);

		   		ParaBorrar.Append("Pagina_2");//+Pagina_To_SavePage+"_last.jpg"
		   		SDKUtilities::AppendPathSeparator(ParaBorrar);

		   		rename(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString(), InterlasaUtilities::MacToUnix(ParaRenombrar).GrabCString() );
		   		
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString());
		   		
		   		
		   		
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString()); 
		   		ParaBorrar.Append( Pagina_To_SavePage+"_last.jpg" );
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString());
		   		
		   		
		   }
		   
		   
	}while(false);
	
		RutaToSaveFile = PaginaParaSavePagina;
		RutaTOJpg = RutaParaJPG;
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutSuite::TomarFotosDeSpread fin");
	return(kTrue);
}



/* Abre dialogo para duplicar pagina 
*/
bool16 CheckInOutSuite::DuplicarPagina()
{
	bool16  retval=kFalse;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		
		//Para salir en caso de que sea incopy donde fue tecleada esta opcion
		PMString NomAplicacionActual = application->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		if(NomAplicacionActual.Contains("InCopy"))
		{
			break;
		}
		
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PCheckInOutPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kCheckInDuplicarPaginaDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}
		
		// Open the dialog.
		dialog->Open(); 
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
				
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		//Para indicar que se debe hacer un ChecjIn Pero solo a la base de datos
		
		
		dialog->WaitForDialog();
		
		PMString DialogwasCanceled =  dialogController->GetTextControlData(kToCheckInOnDBWidgetID);
		
		if(DialogwasCanceled!="Cancel" && DialogwasCanceled!="Cancelar")
		{
			retval=kTrue;
		}
	} while (false);	
	return(retval);		
}


/*
		0 cancelar
		1 no 
		2 si
*/
int32 CheckInOutSuite::OpenDialogoQuestion(PMString Question)
{
	int32  retval=3;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PCheckInOutPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kCheckOutN2PQuestionDialogoResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}
		
		// Open the dialog.
		dialog->Open(); 
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
				
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		//Para indicar que se debe hacer un ChecjIn Pero solo a la base de datos
		
		dialogController->SetTextControlData(kCheckInOutQuestionStaticTextWidgetID,Question);
		
		
		dialog->WaitForDialog();
		
		PMString DialogwasCanceled =  dialogController->GetTextControlData(kCheckInOutAceptQuestionWidgetID);
		
		if(DialogwasCanceled=="0")
		{
			retval=0;
		}
	} while (false);	
	return(retval);		
}



// End, CheckInOutSuite.cpp.