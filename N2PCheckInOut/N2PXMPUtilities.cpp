/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "SDKUtilities.h"
// Interface includes:
#include "ISelectionManager.h" // required by selection templates.
#include "ICommandSequence.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IDialog.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ISelectableDialogSwitcher.h"
#include "IDocument.h"
#include "IDocument.h"
#include "IMetaDataAccess.h"
//#include "IMetaDataPaths.h"
#include "ILayoutUIUtils.h"

// General includes:
#include "Utils.h"

// General includes:
#include "CPMUnknown.h"
#include "CmdUtils.h"	// so that the thing will compile (selectionasbtemplates.tpp)
#include "SelectionASBTemplates.tpp"
#include "ILayoutUtils.h"
#include "CAlert.h"

#include "IN2PXMPUtilities.h"
#include "N2PCheckInOutID.h"
//#include "InterlasaRegEditUtilities.h"
//#include "A2PPrefID.h"



/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class N2PXMPUtilities : public CPMUnknown<IN2PXMPUtilities>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	N2PXMPUtilities (IPMUnknown *boss);

	/** Destructor.
	 */
	//virtual ~N2PXMPUtilities (void);
	/*
	*/
	bool16 SaveXMPVar(PMString Variable,PMString valor);

	PMString GetXMPVar(PMString Variable);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PXMPUtilities, kN2PXMPUtilitiesImpl)


/* HelloWorld Constructor
*/
N2PXMPUtilities::N2PXMPUtilities(IPMUnknown* boss) 
: CPMUnknown<IN2PXMPUtilities>(boss)
{
}


bool16 N2PXMPUtilities::SaveXMPVar(PMString Variable,PMString value)
{
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			CAlert::InformationAlert(kN2PCheckInOutNoExisteDocumentoStringKey);
			break;
		}
		
		IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		metaDataAccess->Set("http://ns.adobe.com/xap/1.0/", Variable, value);
		//db->EndTransaction();
	} while(false);
	return(kTrue);
}


PMString N2PXMPUtilities::GetXMPVar(PMString Variable)
{
	PMString value;
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			CAlert::InformationAlert(kN2PCheckInOutNoExisteDocumentoStringKey);
			break;
		}
		IDataBase* db = ::GetDataBase(doc);
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
	//	MetaDataFeatures metaDataFeatures;

		metaDataAccess->Get("http://ns.adobe.com/xap/1.0/", Variable, value);
		//db->EndTransaction();
	} while(false);
	return(value);
}
