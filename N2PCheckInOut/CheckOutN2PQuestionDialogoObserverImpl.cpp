//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/framelabel/CheckOutN2PQuestionDialogoObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"

// Project includes:
#include "N2PCheckInOutID.h"

/**	Implements IObserver based on the partial implementation CDialogObserver;
	allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	
	@ingroup framelabel
	@author John Hake
*/
class CheckOutN2PQuestionDialogoObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckOutN2PQuestionDialogoObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~CheckOutN2PQuestionDialogoObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(CheckOutN2PQuestionDialogoObserver, kCheckOutN2PQuestionDialogoObserverImpl)

/* AutoAttach
*/
void CheckOutN2PQuestionDialogoObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckOutN2PQuestionDialogoObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to FrameLabel's info button widget.
		//AttachToWidget(kN2PSqlFrmLblIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void CheckOutN2PQuestionDialogoObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckOutN2PQuestionDialogoObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from FrameLabel's info button widget.
		//DetachFromWidget(kN2PSqlFrmLblIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void CheckOutN2PQuestionDialogoObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("CheckOutN2PQuestionDialogoObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		/*if (theSelectedWidget == kN2PSqlFrmLblIconSuiteWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			CAlert::ModalAlert
			(
				"N2P SQL",		// Alert string
				kOKString, 						// OK button
				kNullString, 					// No second button
				kNullString, 					// No third button
				1,								// Set OK button to default
				CAlert::eInformationIcon		// Information icon.
			);
		}*/

	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, CheckOutN2PQuestionDialogoObserver.cpp.




