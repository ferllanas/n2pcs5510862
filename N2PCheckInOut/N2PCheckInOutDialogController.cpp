//	File:	CheckInOutDialogController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//


#include "VCPlugInHeaders.h"

// Interface includes:

#include "IApplication.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "SDKUtilities.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ImportExportUIID.h"

#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "IDocumentCommands.h"
#include "ICommandMgr.h"
#include "ICoreFilename.h"
#include "IComposeScanner.h"
#include "IPageItemTypeUtils.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "IGeometry.h"
#include "IOutputPages.h"
#include "IPageList.h"
#include "IPDFExportPrefs.h"
#include "IPDFSecurityPrefs.h"
#include "ISysFileData.h"
#include "IUIFlagData.h"
#include "IBoolData.h"
#include "IPrintContentPrefs.h"
#include "IDocumentUtils.h"
#include "IDocumentUIUtils.h"
#include "IDocFileHandler.h"

#include "BookID.h" // for IID_IBOOKEXPORT

#include "FileUtils.h"
//#include "FrameUtils.h"
#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
#include "OpenPlaceID.h"
#include "ProgressBar.h"
#include "IInCopyDocUtils.h"


// none.

// General includes:
#include "PlatformFileSystemIterator.h"
#include "SDKFileHelper.h"
#include "SDKUtilities.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "SDKLayoutHelper.h"
#include "SDKUtilities.h"
#include "OpenPlaceID.h"

#ifdef WINDOWS
	#include <sys/types.h> 
	#include <sys/stat.h> 
#endif
#ifdef MACINTOSH
	#include <types.h> 
	#include <sys/stat.h>  
#endif
#include <time.h>
#include <locale.h> 
#include <Math.h> 

#include "SDKLayoutHelper.h"




// Project includes:
#include "N2PCheckInOutID.h"
#include "ICheckInOutSuite.h"
#include "IN2PXMPUtilities.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif




/** CheckInOutDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class CheckInOutDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckInOutDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CheckInOutDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
	private:

		void SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget);
		
		

		

		

		/**
			Obtiene ID-Usuario del ultimo usuario que a InDesign desde el regEdit 
			para buscar en la base de datos el Nombre del usuario correspondiente al ID que se obtuvo
			y poner el nombre subre su TextEdit
		*/
		PMString  Aplicar_Usuario_Actual();

		/**
			Obtiene el ID del usuario por medio de uhna consulta a la base de datos.
			@param Busqueda PMString cadena que contiene la consulta  la BD.
			@return PMString ID del usuario.
		*/
		PMString BuscarEnUsuarios(PMString &Busqueda);

		/**
			Funcion que llena el combo de usarios.
		*/
		void LLenar_Combo_DirigidoA();
		
		bool16 LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		bool16 LLenar_Combo_Seccion(PMString Id_SeccionText);	

		bool16 LLenar_Combo_EstatusPagina(PMString& Id_EstatusActual);

		

		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);


		PMString GetPMStringOfTextFrame(int32 UIDStringFrame);

		bool16 PonerNotasEnDBConstructor(PMString Seccion,PMString Publicacion,PMString Fecha,PMString NomPag,PMString RutaPDF);

		bool16 Grabar_en_BD(PMString Nota,PMString Fecha,
										  PMString Publicacion,PMString Seccion,
										  PMString Titulo,PMString Balazo,PMString Pie,
										  PMString NomPag,PMString RutaPDF);

		bool16 crearPDF(PMString FilePath);
		
		bool16 EnableDisableWidget(WidgetID widget,bool16 Enabled);
		
		ErrorCode CheckInCompleteDocument();
		

		PMString		fFilter;
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
		
		
		ErrorCode CheckInPagina();
		//
		
		PMString Aplicacion;			//Nombre de la aplicacion sobre la que nos encontramos trabajando
		PMString IDUsuario;			//ID del Usuario que se encuentra trabajando en este momento
		PMString HoraIn;				//Hora que entro a trabajar el usuario
		PMString FechaIn;			//Fecha que entro el usuario a trabajar
			
		PMString horaCrea;			//Datos de la ultima pagina chekineada
		PMString fechaCrea;
		PMString Seccion;
		PMString DirigidoA;
		PMString Estatus;
		PMString Pagina;
		PMString Date;
		PMString Issue;
		PMString NomAplicacionActual;
		PMString Busqueda;
		PMString File;
		PMString ID_Pagina;
		PMString ID_Evento;
		PMString N2PFolio_Pagina;
		PMString CadenaSinDosRutas;
		PMString CopiaToLast;
		PMString StringConection;
		PMString MySQLDate;

};

// CREATE_PMINTERFACE
// Binds the C++ implementation class onto its 
// ImplementationID making the C++ code callable by the 
// application.

CREATE_PMINTERFACE(CheckInOutDialogController, kCheckInDialogControllerImpl)



// ApplyDialogFields
void CheckInOutDialogController::InitializeDialogFields(IActiveContext* context) 
{
	
	
	PrefParamDialogInOutPage PreferencesPara;	//Preferencias
	
	PMString CopyPage="";	//Copia del nombre de la pagina
	PMString FrontDocName="";// documento actualPMString DirigidoA="";// documento actual

	
	//Limpia TextEdits
	SetTextControlData(kPaginaWidgetID,"");
	SetTextControlData(kDatePubliWidgetID,"");
	SetTextControlData(kComboBoxIssueWidgetID,"");

	////Llena Te4xtEdit del Usuario en el sistema
	PMString Usuario=this->Aplicar_Usuario_Actual();


	

	do
	{
	
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		if(NomAplicacionActual.Contains("InCopy"))
		{
			//this->EnableDisableWidget(kComboBoxStatusWidgetID,kFalse);
			//this->EnableDisableWidget(kComboBoxDirigidoAWidgetID,kFalse);
			this->EnableDisableWidget(kPaginaWidgetID,kFalse);
		}
		
		//
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
		
		//Obtiene el nombre del documento actual
		document->GetName(FrontDocName);
		
		
		
		//
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
		
		//obtiene Fecha y hora actuales del sistema
		PMString fecha = checkIn->FechaYHoraActual();
		PMString* Hora = fecha.Substring(0,fecha.IndexOfWChar(94));
		fecha.Remove(0,fecha.IndexOfWChar(94)+2);
		PMString hora = Hora->GrabCString();

		//Coloca fecha y hora actules del sistema
		fecha.SetTranslatable(kFalse);
		SetTextControlData(kFechaWidgetID,fecha);
		hora.SetTranslatable(kFalse);
		SetTextControlData(kHraCreacionWidgetID,hora);
	
	
		//Obtiene las Preferencias con que se modifico la ultima vez el documento
		checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
		
		
		CopyPage = PreferencesPara.Pagina_To_SavePage;
		
		
		
		
	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			break;
		}

		
		this->LLenar_Combo_DirigidoA();
		
		document->GetName(FrontDocName);
		
		
		//si el nombre del documento actual es diferente al nombre de la ultima pagina modificada
		// los datos se quedan en blanco
		if(CopyPage==FrontDocName) 
		{	
			this->SeleccionarCadenaEnComboBox(PreferencesPara.DirididoA_To_SavePage, kComboBoxDirigidoAWidgetID);
		}
		else
		{	
			this->SeleccionarCadenaEnComboBox(Usuario, kComboBoxDirigidoAWidgetID);
		}
		
		
			
		//Selecciona el Estado Anterior		
		LLenar_Combo_EstatusPagina(PreferencesPara.Estado_To_SavePage);
		//CAlert::InformationAlert("Estatus="+PreferencesPara.Estado_To_SavePage);
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Estado_To_SavePage, kComboBoxStatusWidgetID);
		
		
		//Selecciona la seccion Anterior
		this->LLenar_Combo_Publicacion(PreferencesPara.Issue_To_SavePage);		
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Issue_To_SavePage, kComboBoxIssueWidgetID)
		
		//Selecciona la seccion Anterior
		this->LLenar_Combo_Seccion(PreferencesPara.Seccion_To_SavePage);	
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Seccion_To_SavePage, kComboBoxSeccionWidgetID);
		
		
		
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
				(
					kN2PXMPUtilitiesBoss,	// Object boss/class
					IN2PXMPUtilities::kDefaultIID
				)));
		
		
		
		PMString Date = XMPUtils->GetXMPVar("A2PFechaEdicionDPagina");
		
		if(Date.NumUTF16TextChars()>0)
		{
			Date.Append("/");
			PMString *Anno=Date.GetItem("/",1);
			PMString *MM=Date.GetItem("/",2);
			PMString *DD=Date.GetItem("/",3);
		
			
			Date="";
			if(MM->NumUTF16TextChars()>0)
			{
				Date.Append(MM->GrabCString());
				Date.Append("/");
			}
			
			if(DD->NumUTF16TextChars()>0)
			{
				Date.Append(DD->GrabCString());
				Date.Append("/");
			}
			
			if(DD->NumUTF16TextChars()>0)
			{
				Date.Append(Anno->GrabCString());
			
			}
		}
		else
		{
			Date=InterlasaUtilities::FormatFechaYHoraManana("%m/%d/%Y");
		}
		
		
		
		
		PMString Pagina = XMPUtils->GetXMPVar("A2PFolioDPagina");
		
		if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()<=0)
		 	SetTextControlData(kDatePubliWidgetID,Date);
		else
		{
			PreferencesPara.Date_To_SavePage.SetTranslatable(kFalse);
			SetTextControlData(kDatePubliWidgetID,PreferencesPara.Date_To_SavePage);
		}
		
		if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars()<=0)
		 	SetTextControlData(kPaginaWidgetID,Pagina);
		else
		{
			PreferencesPara.Pagina_To_SavePage.SetTranslatable(kFalse);
			SetTextControlData(kPaginaWidgetID,PreferencesPara.Pagina_To_SavePage);
		}
		
		
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
		
		IControlView * ComboSeccionView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
			if(ComboSeccionView==nil)
			{
				
				break;
			}
			
		IControlView * ComboPublicacionView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
			if(ComboPublicacionView==nil)
			{
				
				break;
			}
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			if(NomAplicacionActual.Contains("InCopy"))
			{
				ComboSeccionView->Enable(kFalse);
				ComboPublicacionView->Enable(kFalse);
			}
			
			//CAlert::InformationAlert("tum tururtui");
		}
		else
		{
			
			ComboSeccionView->Enable(kTrue);	
			ComboPublicacionView->Enable(kTrue);
		}
		
	}while(false);
	//CAlert::InformationAlert("FIN");
}



/* ValidateDialogFields
*/
WidgetID CheckInOutDialogController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID retval = kNoInvalidWidgets;
	
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		
		
		PMString NombrePag =  this->GetTextControlData(kPaginaWidgetID);
		if(NombrePag.NumUTF16TextChars()<=0)
		{
			retval = kPaginaWidgetID;
			break;
		}
		
		PMString Publicacion =  this->GetTextControlData(kComboBoxIssueWidgetID);
		if(Publicacion.NumUTF16TextChars()<=0 )
		{
			retval = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Seccion =this->GetTextControlData(kComboBoxSeccionWidgetID);
		if(Seccion.NumUTF16TextChars()<=0)
		{
			retval = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Estatus =this->GetTextControlData(kComboBoxStatusWidgetID);
		if(Estatus.NumUTF16TextChars()<=0 && !NomAplicacionActual.Contains("InCopy"))
		{
			retval = kN2PsqlTextStatusWidgetID;
			break;
		}
	
		PMString DirigidoA =this->GetTextControlData(kComboBoxDirigidoAWidgetID);
		/*if(DirigidoA.NumUTF16TextChars()<=0)
		{
			retval = kN2PsqlTextStatusWidgetID;
			break;
		}*/
		
		ErrorCode error = kFailure;
		IAbortableCmdSeq* sequ = CmdUtils::BeginAbortableCmdSeq();//ICommandSequence* sequ = CmdUtils::BeginCommandSequence();
		SequenceMark sequenceMark = CmdUtils::SetSequenceMark(sequ);
		// Put code to validate widget values here.

		
	
		PMString sequName= PMString(kN2PCheckInCckInPageStringKey);
		sequName.SetTranslatable(kTrue);
		sequ->SetName(sequName);
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString CreacionDEDocumento = N2PSQLUtilities::GetXMPVar("CreateDate", document);
		if(CreacionDEDocumento.NumUTF16TextChars()>0)
		{
			
		
		
		if(UpdateStorys!=nil)
		{
			if(Estatus==PrefConections.EstadoParaExportarATWEB )//"E Page Done" || Estatus=="E Page Done" ||  Estatus=="E Page Done" ||  Estatus=="E Page Done"
			{
				if(UpdateStorys->AlgunElementoSeEncuentraCheckauteado())
				{
					retval = kComboBoxStatusWidgetID;
				}
				else
				{
					if(PrefConections.NombreEstatusParaNotasCompletas.NumUTF16TextChars()>0)
					{
						if(UpdateStorys->EstadoDeNotasCompleto(PrefConections.NombreEstatusParaNotasCompletas)==kFalse)
						{
							PMString Question(kN2PChekInCantCheckInStringKey);
							Question.Translate();
  							Question.SetTranslatable(kTrue);

							const int ActionAgreedValue=1;
							int32 result=-1;
							result=CAlert::ModalAlert(Question,
							kYesString, 
							kNoString,
							kNullString,
							ActionAgreedValue,CAlert::eQuestionIcon);
							/*kN2PCheckInOutAllowString, 
							kCancelString,
							kNullString,
							ActionAgreedValue,
							CAlert::eQuestionIcon);
							*/
							if(result==1)
							{
								//Name the command sequence:
						
						
								if(this->CheckInPagina()==kSuccess)
									retval = kNoInvalidWidgets;
								else
									retval = kComboBoxStatusWidgetID;
							}
							else
							{
								retval = kComboBoxStatusWidgetID;
							}
						}
						else
						{
							if(this->CheckInPagina()==kSuccess)
								retval = kNoInvalidWidgets;
							else
								retval = kPaginaWidgetID;
						}
					
					}
					else
					{
						if(this->CheckInPagina()==kSuccess)
								retval = kNoInvalidWidgets;
							else
								retval = kPaginaWidgetID;
				
					}
				}
			
			}
			else
			{
				if(this->CheckInPagina()==kSuccess)
					retval = kNoInvalidWidgets;
				else
					retval = kPaginaWidgetID;
			}
		}
		}
		else
		{
			CAlert::InformationAlert(kN2PCheckInOutPaginaSinXMPCoreStringKey);
			retval=kPaginaWidgetID;
		}
	
		if(error==kSuccess)
		{
			CmdUtils::EndCommandSequence(sequ);
		}
		else
		{
			CmdUtils::EndCommandSequence(sequ);
			//CmdUtils::RollBackCommandSequence(sequ,sequenceMark);
			//CmdUtils::AbortCommandSequence(sequ);		
		}
	}while(false);
	
	
	return retval;
}



/* ApplyDialogFields
*/
void CheckInOutDialogController::ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId) 
{
	
	SystemBeep();  
}




bool16 CheckInOutDialogController::PonerNotasEnDBConstructor(PMString Seccion,PMString Publicacion,PMString Fecha,PMString NomPag,PMString RutaPDF)
{
	bool16 retval=kFalse;

	do
	{
		struct StructNotas
		{
			PMString IDNota;
			PMString Nota;
			PMString Titulo;
			PMString Balazo;
			PMString PieFoto;
		};

		PMString IDsNotas[30];
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				ASSERT_FAIL("FilActActionComponent::DoSave: document invalid");
				break;
			}

		//Obtiene la cadena que contiene los registros de los campos fluidossobre la pagina actual
		PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		//Obtiene la cantidad de registros
		int32 CantRegXMP=N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
		//crea la estructura para llenar con los registros que se encuentran en la pagina actual
		ClassRegEnXMP *RegEnXMP= new ClassRegEnXMP[CantRegXMP];
		//llenado de la estructura con los registros que se encuentran en la pagina actual
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,RegEnXMP);
	
		bool16 Ya_existe_IDNota_EnVector=kFalse;
		int32 numIdNotas=0;
		//IDsNotas[0]=RegEnXMP[0].IDElemPadre;
		//numIdNotas++;
		//ciclo para obtener la cantidad de notas
		int32 numReg=0;
		int32 numNota=0;

		for(numReg=0; numReg<30 ;numReg++)
			IDsNotas[numNota]="";

		for(numReg=0; numReg<CantRegXMP ;numReg++)
		{
			for(numNota=0;numNota<numIdNotas && Ya_existe_IDNota_EnVector==kFalse;numNota++)
			{
				if(IDsNotas[numNota]==RegEnXMP[numReg].IDElemPadre)
				{
					Ya_existe_IDNota_EnVector=kTrue;
				}
			}
			if(!Ya_existe_IDNota_EnVector)
			{
			
				IDsNotas[numIdNotas]=RegEnXMP[numReg].IDElemPadre;
				numIdNotas++;
			}
			Ya_existe_IDNota_EnVector=kFalse;
		}

	
	
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
	
		for(int32 xx=0;xx<numIdNotas ;xx++)
		{
			PMString Consulta("INSERT INTO Hemeroteca (Id_Elemento) Values('");
	
			Consulta.Append(IDsNotas[xx]);
		
			Consulta.Append("')");
			if(SQLInterface->SQLSetInDataBase(StringConection,Consulta))
				retval=kTrue;
			else
			 	retval=kFalse;
		}
		
	}while(false);
	
	
	
/*	
	StructNotas *Notas=new StructNotas[numIdNotas];
	//coloca el numero de nota en la estructura
	for(numNota=0;numNota<numIdNotas;numNota++)
	{	
		Notas[numNota].IDNota=IDsNotas[numNota];
		for(numReg=0; numReg<CantRegXMP ;numReg++)
		{
			if(Notas[numNota].IDNota==RegEnXMP[numReg].IDElemPadre)
			{
				if(RegEnXMP[numReg].TipoDato=="ContenidoNota")
				{
					Notas[numNota].Nota=this->GetPMStringOfTextFrame(RegEnXMP[numReg].IDFrame);
				}

				if(RegEnXMP[numReg].TipoDato=="PieFoto")
				{
					Notas[numNota].PieFoto=this->GetPMStringOfTextFrame(RegEnXMP[numReg].IDFrame);
				}

				if(RegEnXMP[numReg].TipoDato=="Balazo")
				{
					Notas[numNota].Balazo=this->GetPMStringOfTextFrame(RegEnXMP[numReg].IDFrame);
				}

				if(RegEnXMP[numReg].TipoDato=="Titulo")
				{
					Notas[numNota].Titulo=this->GetPMStringOfTextFrame(RegEnXMP[numReg].IDFrame);
				}
			}
		}
	}
	
	//Para insertar las notas en la base de datos
	
	for(numNota=0;numNota<numIdNotas;numNota++)
	{
		if(this->Grabar_en_BD(Notas[numNota].Nota , Fecha, Publicacion, Seccion, Notas[numNota].Titulo, Notas[numNota].Balazo, Notas[numNota].PieFoto, NomPag, RutaPDF))
		{
			
		}
	}
	
	//crea la consulta(UPdate) para modificar la base de datos*/
	return(retval);
}


bool16 CheckInOutDialogController::Grabar_en_BD(PMString Nota,PMString Fecha,
										  PMString Publicacion,PMString Seccion,
										  PMString Titulo,PMString Balazo,PMString Pie,
										  PMString NomPag,PMString RutaPDF)
{
	bool16 retval=kFalse;
	do
	{
	
		PMString Consulta("INSERT INTO Hemeroteca (ContenidoNota, FechaEdicion, Publicacion, Seccion, Titulo, Balazo, PieFoto, NombrePagina, RutaPDF) Values('");
		Consulta.Append(Nota.GrabCString());
		Consulta.Append("','");
		Consulta.Append(Fecha.GrabCString());
		Consulta.Append("','");
		Consulta.Append(Publicacion.GrabCString());
		Consulta.Append("','");
		Consulta.Append(Seccion.GrabCString());
		Consulta.Append("','");
		Consulta.Append(Titulo.GrabCString());
		Consulta.Append("','");
		Consulta.Append(Balazo.GrabCString());
		Consulta.Append("','");
		Consulta.Append(Pie.GrabCString());
		Consulta.Append("','");
		Consulta.Append(NomPag.GrabCString());
		Consulta.Append("','");
		Consulta.Append(RutaPDF.GrabCString());
		Consulta.Append("')");
	
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
	
		if(SQLInterface->SQLSetInDataBase(StringConection,Consulta))
			 retval=kTrue;
		else
			 retval=kFalse;
	
		//CAlert::InformationAlert(kN2PCheckInOutNewItemOnDBStringKey);
		
	}while(false);
	return(retval);
}


//  Generated by Dolly build 17: template "Dialog".
// End, CheckInOutDialogController.cpp.

/**
	Obtiene ID-Usuario del ultimo usuario que a InDesign desde el regEdit 
	para buscar en la base de datos el Nombre del usuario correspondiente al ID que se obtuvo
	y poner el nombre subre su TextEdit
*/
PMString CheckInOutDialogController::Aplicar_Usuario_Actual()
{
	PMString retval="";

	InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{	
			retval = wrkSpcPrefs->GetIDUserLogedString();
			retval.SetTranslatable(kFalse);
			SetTextControlData(kComboBoxUsuarioWidgetID,retval);
		}
	return(retval);
}


void CheckInOutDialogController::LLenar_Combo_DirigidoA()
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxDirigidoAWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}

		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		PMString cadena ="";
		
		//Consulta
		PMString Busqueda="SELECT   Id_Usuario From Usuario ORDER BY Id_Usuario ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			cadena = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		PMString DirigidoA="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{	
			DirigidoA.Append(wrkSpcPrefs->GetDirigidoANota());
		}
		
		int32 Index=dropListData->GetIndex(DirigidoA);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		
		////////////////////////////
		
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
		(
			kN2PXMPUtilitiesBoss,	// Object boss/class
			IN2PXMPUtilities::kDefaultIID
		)));
		
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
		
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			QueryVector.clear();
			PMString Busqueda="SELECT Id_Usuario From Bitacora_Pagina WHERE Pagina_ID ="+IDPaginaXMP+" AND Id_Evento IN(5,6,13,31,32) ORDER BY ID DESC LIMIT 1";
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);

			cadena="";
			
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				
				cadena = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			
			
			
			}
			
			if(cadena.NumUTF16TextChars()>0)
				this->SetTextControlData(kCheckInUltimoUsuarioQModificoWidgetID,cadena);
			
		}
		
			
	}while(false);
}






PMString CheckInOutDialogController::BuscarEnUsuarios(PMString &Busqueda)
{

	PMString cadena="";;

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
		}
	}while(false);
		
	
	return(cadena);
}









PMString CheckInOutDialogController::GetPMStringOfTextFrame(int32 UIDModelString)	
{	
	PMString texto="";
	do
	{
		////////obtener Uid del TextModel
					
		PMString Consulta="";
		int32 intthis=UIDModelString;
		UID UIDTextModel=intthis;
					
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		////////////////////Obtener texto de Texframe
		UIDRef TextModelUIDRef(db,UIDTextModel);
		InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
		if(mytextmodel==nil)
		{
			break;
		}
		InterfacePtr<IComposeScanner> cs(mytextmodel, UseDefaultIID()); 
		// Setup a variable to retrieve the length of the text 
		if(cs==nil)
		{
			break;
		}
		// Setup a variable to retrieve the length of the text 
		int32 length = mytextmodel->TotalLength();//obtengo el total de caracteres de la cadena en el textframe
		int32 index=0;//numero de caracter leido
		WideString Temp;
		cs->CopyText(index, length-1, &Temp);
		// Setup a PMString to hold the text (depending on what you want to do with the text, the textchar may be ok) 
		// Load the text into the buf variable 
		//buf.SetXString(buffer, length); 
		texto="";
		texto.Append(Temp);
	}while (false); // only do once
	return(texto);
}

bool16 CheckInOutDialogController::crearPDF(PMString filePath)
{
	bool16 retval=kFalse;
	do
	{
		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF document is invalid");
			break;
		}

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars() -5,nombre.NumUTF16TextChars() );
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF db is invalid");
			break;
		}

		//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		   UIDList pageUIDs = UIDList(::GetDataBase(document));
		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		   ASSERT(iPageList);
		   if(!iPageList) 
		   {
		   		ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			   break;
		   }
		   // Setup the page UID list to include
		   // the pages you want to export
		   int32 cPages = iPageList->GetPageCount();
		   for (int32 iPage = 0; iPage < cPages; iPage++ )
		   {
			   UID uidPage = iPageList->GetNthPageUID(iPage);
			   pageUIDs.Append(uidPage);
		   }


		//PMString filePath=PrefConections.PathOfServerFile + ":apaginas:Archivos PDF:";
		//	filePath.Append(nombre);
		//	filePath.Append(".pdf");
		/********/
		// obtain command boss
        InterfacePtr<ICommand> pdfExportCmd(CmdUtils::CreateCommand(kPDFExportCmdBoss));
		if(pdfExportCmd==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF pdfExportCmd is invalid");
			break;
		}
		// set export prefs
		InterfacePtr<IPDFExportPrefs> pdfExportPrefs(pdfExportCmd, UseDefaultIID());
		if(pdfExportPrefs==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF pdfExportPrefs is invalid");
			break;
		}
		
		// set IDFileData
		PMString pmFilePath(filePath);
		pmFilePath.SetTranslatable(kFalse);
		IDFile sysFilePath = FileUtils::PMStringToSysFile(pmFilePath);
		
		InterfacePtr<ISysFileData> trgIDFileData(pdfExportCmd, IID_ISYSFILEDATA);
		if(trgIDFileData==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF trgIDFileData is invalid");
			break;
		}
		trgIDFileData->Set(sysFilePath);
		
		// set security preferences
		InterfacePtr<IPDFSecurityPrefs> securityPrefs(pdfExportCmd, UseDefaultIID());
		if(securityPrefs==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF securityPrefs is invalid");
			break;
		}
		// todo: add calls to specific methods...

		// set print content prefs
		InterfacePtr<IPrintContentPrefs> printContentPrefs(pdfExportCmd, IID_IPRINTCONTENTPREFS);
		if(printContentPrefs==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF printContentPrefs is invalid");
			break;
		}
		// todo: add calls to specific methods...

		// set to true if outputting a book file 
		InterfacePtr<IBoolData> bookExport(pdfExportCmd, IID_IBOOKEXPORT);
		if(bookExport==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF bookExport is invalid");
			break;
		}
		bookExport->Set(kFalse); 

		// set UI flags
		InterfacePtr<IUIFlagData> uiFlagData(pdfExportCmd, IID_IUIFLAGDATA);
		if(uiFlagData==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF uiFlagData is invalid");
			break;
		}
		uiFlagData->Set(uiFlags );
		
		// set UIDList containing pages to output (item list of command)
		UIDList theList(pageUIDs);
		pdfExportCmd->SetItemList(theList);
		
		// set UIDRefs containing pages to output (IOutputPages)
		InterfacePtr<IOutputPages> outputPages(pdfExportCmd, UseDefaultIID());
		if(outputPages==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF outputPages is invalid");
			break;
		}
		outputPages->Clear();
	//	IDataBase* db = theList.GetDataBase();
		outputPages->SetMasterDataBase(db);
		int32 nProgressItems = theList.Length(); 
		for (int32 index = 0 ; index < nProgressItems ; index++) 
		{
        	outputPages->AppendOutputPageUIDRef(theList.GetRef(index));
		}

		// set PDF document name (get name from IDocument)
		InterfacePtr<IDocument> doc ((IDocument*)db->QueryInstance(db->GetRootUID(), IID_IDOCUMENT));
		if(doc==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF doc is invalid");
			break;
		}
		PMString documentName; 
		doc->GetName(documentName);
		outputPages->SetName(documentName);
		
		// setup progress bar, if not suppressing the UI.
		K2::scoped_ptr<RangeProgressBar> deleteProgressBar;
		bool16 bShowImmediate = kTrue;
		if( uiFlags != kSuppressUI )
		{
			RangeProgressBar *progress = new RangeProgressBar( "Generating PDF", 0, nProgressItems, bShowImmediate, kTrue, nil, kTrue); 
			pdfExportPrefs->SetProgress(progress);
			deleteProgressBar.reset( progress );
		}
		
		// finally process command
		status = CmdUtils::ProcessCommand(pdfExportCmd);
		
		if(status==kSuccess)
			retval=kTrue;
	}while(false);
	
	return retval;	
}





void CheckInOutDialogController::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("IDDLDrComboBoxSelecPrefer");
			break;
		}

		
		int32 Index=dropListData->GetIndex(CadenaASeleccionar);
		
		if(Index < 0)
		{
			Index=dropListData->Length();
		}
		
		IDDLDrComboBoxSelecPrefer->Select(Index);
		
	}while(false);
}


bool16 CheckInOutDialogController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + GetTextControlData(kComboBoxIssueWidgetID) + "' ) ORDER BY Nombre_de_Seccion ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
		
			
	}while(false);
	return(kTrue);
}

bool16 CheckInOutDialogController::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	//kComboBoxIssueWidgetID
	
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		//Selecciona el item deseado o la Publicacion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
			
	}while(false);
	return(kTrue);
}


bool16 CheckInOutDialogController::LLenar_Combo_EstatusPagina(PMString& Id_EstatusActual)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxStatusWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Estatus,Nombre_Estatus From Estatus_Elemento WHERE Id_tipo_ele='2' ORDER BY Nombre_Estatus ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
		
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			PMString IdEstatusQuery=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			if(Id_EstatusActual==IdEstatusQuery)
			{
				Id_EstatusActual=cadena;
			}
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		
		
		int32 Index=dropListData->GetIndex(Id_EstatusActual);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
			
	}while(false);
	return(kTrue);
}


int32 CheckInOutDialogController::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}

bool16 CheckInOutDialogController::EnableDisableWidget(WidgetID widget,bool16 Enabled)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		if(Enabled)
		{
			ComboCView->Enable(kTrue,kTrue);
		}
		else
		{
			ComboCView->Disable(kTrue);
		}
		
	}while(false);
	return(retval);
}



ErrorCode CheckInOutDialogController::CheckInPagina()
{
	ErrorCode errorValue=kFailure;
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutDialogController::CheckInPagina ini");
	
	do
	{	
		
					
		Aplicacion="";			//Nombre de la aplicacion sobre la que nos encontramos trabajando
		IDUsuario="";			//ID del Usuario que se encuentra trabajando en este momento
		HoraIn="";				//Hora que entro a trabajar el usuario
		FechaIn="";			//Fecha que entro el usuario a trabajar
			
		horaCrea="";			//Datos de la ultima pagina chekineada
		fechaCrea="";
		Seccion="";
		DirigidoA="";
		Estatus="";
		Pagina="";
		Date="";
		Issue="";
		NomAplicacionActual="";
		Busqueda="";
		File="";
		ID_Pagina="0";
		ID_Evento="";
		N2PFolio_Pagina="";
		CadenaSinDosRutas="";
		CopiaToLast="";
		StringConection="";
		IDFile sysFile;
		SDKLayoutHelper helperLayout;
		PMString FileToRemove="";	
		PMString RutaPaInCopy="";
		
		
		
		 	
	 	//CAlert::InformationAlert("A");
		
		
		
		//////////Estructura de Preferencias
		PrefParamDialogInOutPage PreferencesPara;
		PreferencesConnection PrefConections;
			
		//////////Interface de Aplicacion
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
			
		/////////Para Obtener UIDRef del Documento
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			ASSERT_FAIL("FilActActionComponent::DoSave: document invalid");
			break;
		}
		
		UIDRef docUIDRef = ::GetUIDRef(document); //UIDRef del Documento
		IDataBase *db=docUIDRef.GetDataBase();	//DataBase del Documento
			
		////as,nksanjkas
		PMString PAthToCopyDocumentBackUp = "";
		if(PAthToCopyDocumentBackUp.NumUTF16TextChars()<=0)
		{
			//CAlert::InformationAlert("B");
		//Path de usuario
			char  *pathPtr;
			pathPtr = getenv( "HOME" );
			//CAlert::InformationAlert("C");
			
			
			
			PMString PathUser="";
			#ifdef MACINTOSH
				PathUser= PMString(pathPtr);
				SDKUtilities::convertToMacPath(PathUser); 
			#elif defined(WINDOWS)
				if(pathPtr==NULL)
				{
						
					pathPtr = getenv( "HOMEPATH" );
					if(pathPtr!=NULL)
					{
						PathUser= PMString(pathPtr);
						PathUser.Remove(0,1);
						SDKUtilities::convertToWinPath(PathUser);
					}
				}
				
			#endif
			
			

			PMString AppFolder="";
			SDKUtilities::GetApplicationFolder(AppFolder);
			
			IDFile 	FolderAppIDFile=FileUtils::PMStringToSysFile(AppFolder);
			
			
			PMString Vol="";
			FileUtils::GetVolumeName (&FolderAppIDFile, &Vol);
			
			
			PAthToCopyDocumentBackUp=Vol;
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(PathUser);
			//CAlert::InformationAlert(PAthToCopyDocumentBackUp);
			
			//Otro Rollo
			//IDFile filePackageFolder;
			//FileUtils::GetPackageFolder(&filePackageFolder);
			//PMString PackageFolder=	FileUtils::SysFileToPMString (filePackageFolder);
			//CAlert::InformationAlert(PackageFolder);
		}
		//CAlert::InformationAlert("E");
		////////Interface de Utiloidades del Documento
		InterfacePtr<IDocumentUtils> docUtils(GetExecutionContextSession(), IID_IDOCUMENTUTILS);
		if (docUtils == nil)
		{
			ASSERT_FAIL("FilActActionComponent::DoSave: docUtils invalid");
			break;
		}
			
				
		///////Obtiene la Interface ICheckInOutSuite para mandar hablar a sus metodos
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
			break;
			
			
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
			
		///////HaceUpdate a la base de datos de las notas que se estaban editando
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
			
		///////////
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}	
			
					
					
		////////
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
		(
			kN2PXMPUtilitiesBoss,	// Object boss/class
			IN2PXMPUtilities::kDefaultIID
		)));
				
				
		//Obtiene los datos de registro de usuarios si solo si se encuentra un usuario logeado en la aplicacion
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckInOutDialogController::ValidateDialogFields()");
			break;
		}
		
					
					
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
					
		//////
		UpdateStorys->CheckInNotasEnEdicion(kFalse);	//hace check in de las notas que se encuentren en edicion sobre el documento actual
			
		
			
		IDUsuario = wrkSpcPrefs->GetIDUserLogedString();	//Obtiene el Id_Usuario que se encuentra logeado actuamente
		Aplicacion.Append(wrkSpcPrefs->GetNameApp());			//Obtiene el nombre de la aplicacion en que se encuentra logeado el usuario
		HoraIn.Append(wrkSpcPrefs->GetHoraIn());				//Obtiene la Hora en se logeo el Usuario
		FechaIn.Append(wrkSpcPrefs->GetFechaIn());				//Obtiene la fecha en que se logeo el usuario
		
		
		
		//Datos de la ultima pagina chekineada que se muestran en el dialogo
		horaCrea = GetTextControlData(kHraCreacionWidgetID);			
		fechaCrea = GetTextControlData(kFechaWidgetID);
			
		DirigidoA = GetTextControlData(kComboBoxDirigidoAWidgetID);
		Estatus = GetTextControlData(kComboBoxStatusWidgetID);
		Pagina = GetTextControlData(kPaginaWidgetID);
		
		Date = GetTextControlData(kDatePubliWidgetID);
		
		MySQLDate = InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Date);
		
			
			
		Seccion=GetTextControlData(kComboBoxSeccionWidgetID);
		Issue=GetTextControlData(kComboBoxIssueWidgetID);
	
		PMString NombreSeccion=	Seccion;
		PMString NombrePublicacion=Issue;
		
		//Obtiene el index del nombre de la publicacio y de la seccion para despues buscalos en los vectors correcpondientes
		int32 indexOfIssue = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxIssueWidgetID, Issue);
		if(indexOfIssue>=0 && VectorIdPublicacion.Length()>0 && indexOfIssue< VectorIdPublicacion.Length())
		{
			Issue = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",VectorIdPublicacion[indexOfIssue]);
		}
		
		
		
		
		
		int32 indexOfSeccion = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxSeccionWidgetID, Seccion);
			
		this->LLenar_Combo_Seccion(Seccion);
		this->SeleccionarCadenaEnComboBox(Seccion , kComboBoxSeccionWidgetID);
		
		if(VectorIdSeccion.Length()>0)
		{
			if(indexOfSeccion>=0 && indexOfSeccion<VectorIdSeccion.Length())
			{
				Seccion = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",VectorIdSeccion[indexOfSeccion]);
			}
		}
		
		/////////////////////////////////////////
		////Para revisar que existe la pagina sobre la base de datos y  ademas de eso traernos la ultima ruta con que fue generada
		/////////////////////////////////////////
		K2Vector<PMString> QueryVector;	
		PMString Busqueda="SELECT P.Ruta_Elemento FROM  Pagina P";
				Busqueda.Append("  WHERE  P.ID = ");
				Busqueda.Append(XMPUtils->GetXMPVar("N2PSQLIDPagina"));
				Busqueda.Append("");
		
		
		/////////////////////////////////////////
			
		PreferencesPara.Estado_To_SavePage = Estatus;
		PreferencesPara.Seccion_To_SavePage = Seccion;
		PreferencesPara.Pagina_To_SavePage = Pagina;
		PreferencesPara.Issue_To_SavePage = Issue;
		PreferencesPara.Date_To_SavePage = Date;
		PreferencesPara.DirididoA_To_SavePage = DirigidoA;
		
		
		PMString N2PFolio_Pagina = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		PMString N2P_Pagina = N2PSQLUtilities::GetXMPVar("N2PPagina", document);
		//Oculta frames de overset para tomar fotos sin estos framaes	
		N2PCTUtils->OcultaNotasConFrameOverset();	
					
		PMString FotoVersionPagina="";
		PMString FotoLastPagina="";
		PMString fecha_Guion="";
					
					
					
					
		PMString FechaParaRuta = "";//InterlasaUtilities::FormatFechaYHoraManana("%Y%m%d");
		PMString *mm=fechaCrea.GetItem("/",1);
		PMString *dd=fechaCrea.GetItem("/",2);
		PMString *aa=fechaCrea.GetItem("/",3);
		FechaParaRuta=aa->GrabCString();
		FechaParaRuta.Append(mm->GrabCString());
		FechaParaRuta.Append(dd->GrabCString());
					
		

		/*if(N2P_Pagina.NumUTF16TextChars()>1)
		{
			if(N2P_Pagina!=Pagina)//e va a cambiar el nombre del documento
			{
							
				FileToRemove= PrefConections.PathOfServerFile ;
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				SDKUtilities::AppendPathSeparator(FileToRemove);
				FileToRemove.Append( N2P_Pagina );
				FileToRemove.Append(".indd");

							
				ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				fecha_Guion= PreferencesPara.Date_To_SavePage;
				SDKUtilities::Replace(fecha_Guion,"/","-");
				SDKUtilities::Replace(fecha_Guion,"/","-");
				
				//Arma la Ruta para el Documento InDesign
				File= PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(File);
				File.Append("apaginas");
				SDKUtilities::AppendPathSeparator(File);
				File.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(File);
				File.Append(FechaParaRuta);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( NombrePublicacion);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( NombreSeccion );
				SDKUtilities::AppendPathSeparator(File);
				File.Append( IDUsuario);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( Pagina );
				File.Append(".indd");
				
				if(NomAplicacionActual.Contains("InDesign"))//si nos encontramos en InDesign
				{
					sysFile=FileUtils::PMStringToSysFile(File);
					if(FileUtils::DoesFileExist(sysFile))
					{
						CAlert::InformationAlert(kN2PInOutYaExistePaginaConEsteFolioStringAlert);
						break;
					}
									
				}	
							
				//Arma la ruta para os ducumento de Versiones y fotos o previews
				FotoVersionPagina=PrefConections.PathOfServerFile ;
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(FechaParaRuta);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(NombrePublicacion); 
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(NombreSeccion);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(IDUsuario);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							
				FotoLastPagina=PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(FechaParaRuta );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(NombrePublicacion );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(NombreSeccion );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(IDUsuario);
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
						

				FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
				FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
				checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
				
				
				//Para ruta de copia de pagina
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append("News2Page");
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombrePublicacion);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(FechaParaRuta);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombreSeccion);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(IDUsuario);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(PreferencesPara.Pagina_To_SavePage +".indd");
				
				//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage);
				
				//Obtiene Folio del XMP si es que existe
			
				CadenaSinDosRutas=FotoVersionPagina;
				//Remueve todo hasta a paginas
				if(CadenaSinDosRutas.NumUTF16TextChars()>0)
				{
					int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
					if(indexapaginas>0)
						CadenaSinDosRutas.Remove(0, indexapaginas+9);
				}
							
				CopiaToLast=FotoLastPagina;
				CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
				CopiaToLast.Append("_last.jpg");
				fechaCrea.Append(" " + horaCrea);
							
				//Quiere Decir que es el Primer CheckIn
			
				//Si es en InDesign significa que si se cambio el nombre desde inDesign
				//Si es inCopy significa que cambiaron el nombre desde InDesign por lo tanto solo se actualiza datos iod evento 15
				if(NomAplicacionActual.Contains("InDesign"))
					ID_Evento="31";
				else
					ID_Evento="15";
						
				//Si Es el Primer CheckIn
				
				//Arma Folio
				N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
				N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
			}
			else
			{//Hace un save normal
				
				fecha_Guion = XMPUtils->GetXMPVar("N2PDate");
				ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				
				SDKUtilities::Replace(fecha_Guion,"/","-");
				SDKUtilities::Replace(fecha_Guion,"/","-");
									
				//Arma la Ruta para el Documento InDesign
				//Arma la Ruta para el Documento InDesign
				File= PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(File);
				File.Append("apaginas");
				SDKUtilities::AppendPathSeparator(File);
				File.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(File);
				File.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
				SDKUtilities::AppendPathSeparator(File);
				File.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
				SDKUtilities::AppendPathSeparator(File);
				File.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				SDKUtilities::AppendPathSeparator(File);
				File.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				SDKUtilities::AppendPathSeparator(File);
				File.Append( PreferencesPara.Pagina_To_SavePage  );
				File.Append(".indd");
				
				//Arma la ruta para os ducumento de Versiones y fotos o previews
							
				FotoVersionPagina=PrefConections.PathOfServerFile ;
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath")); 
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath"));
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoLastPagina=PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				SDKUtilities::AppendPathSeparator(FotoLastPagina);

				FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
				FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
				checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));	//Toma Foto de paginas para obtener el Preview
				
				
				//Para ruta de copia de pagina
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append("News2Page");
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PPagina")+".indd");
				
				//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));
							
				//Obtiene Folio del XMP si es que existe
			
				CadenaSinDosRutas=FotoVersionPagina;
				//Remueve todo hasta a paginas
				if(CadenaSinDosRutas.NumUTF16TextChars()>0)
				{
					int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
					if(indexapaginas>0)
						CadenaSinDosRutas.Remove(0, indexapaginas+9);
				}
							
				CopiaToLast=FotoLastPagina;
				CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
				CopiaToLast.Append("_last.jpg");
				fechaCrea.Append(" " + horaCrea);
							
				if(NomAplicacionActual.Contains("InDesign"))
					ID_Evento="5";
				else
					ID_Evento="13";
					
			}
			
			
			///////////////////////
			///Se Pregunta aqui pues se supone que es una pagina ya adiconada al sistema
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
			//CAlert::InformationAlert(Busqueda);
			
			if(QueryVector.Length()>0)
			{
				//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
				RutaPaInCopy =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
			}
			else
			{
				CAlert::InformationAlert("Error 101 no se encontraron datos para el id de pagina especificado");
				break;
			}
			///////////////////////
		}
		else
		{//Hace un primer save
			
			fecha_Guion= PreferencesPara.Date_To_SavePage;
			SDKUtilities::Replace(fecha_Guion,"/","-");
			SDKUtilities::Replace(fecha_Guion,"/","-");
			
			//Arma la Ruta para el Documento InDesign
			File= PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(File);
			File.Append("apaginas");
			SDKUtilities::AppendPathSeparator(File);
			File.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(File);
			File.Append(FechaParaRuta);
			SDKUtilities::AppendPathSeparator(File);
			File.Append( NombrePublicacion);
			SDKUtilities::AppendPathSeparator(File);
			File.Append( NombreSeccion );
			SDKUtilities::AppendPathSeparator(File);
			File.Append( IDUsuario);
			SDKUtilities::AppendPathSeparator(File);
			File.Append( PreferencesPara.Pagina_To_SavePage );
			File.Append(".indd");
				
			//Arma la ruta para os ducumento de Versiones y fotos o previews
				
			FotoVersionPagina=PrefConections.PathOfServerFile ;
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append("apaginas");
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(FechaParaRuta);
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(NombrePublicacion); 
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(NombreSeccion);
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(IDUsuario);
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);

			FotoLastPagina=PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append("apaginas");
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
            FotoLastPagina.Append(FechaParaRuta );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(NombrePublicacion );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(NombreSeccion );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(IDUsuario);
			SDKUtilities::AppendPathSeparator(FotoLastPagina);

			FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
			FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
			checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
			
			
			//Para ruta de copia de pagina
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append("News2Page");
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombrePublicacion );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(FechaParaRuta );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombreSeccion );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(IDUsuario);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(PreferencesPara.Pagina_To_SavePage+".indd");
			
			
			//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
				
			//Obtiene Folio del XMP si es que existe
			
			CadenaSinDosRutas=FotoVersionPagina;
					
			//Remueve todo hasta a paginas
			if(CadenaSinDosRutas.NumUTF16TextChars()>0)
			{
				int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
				if(indexapaginas>0)
					CadenaSinDosRutas.Remove(0, indexapaginas+9);
			}
						
			CopiaToLast=FotoLastPagina;
			CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
			CopiaToLast.Append("_last.jpg");
			fechaCrea.Append(" " + horaCrea);
						
			//Quiere Decir que es el Primer CheckIn
						
			ID_Evento="8";
						
			//Si Es el Primer CheckIn
				
			//Arma Folio
			N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
			N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
		}
		*/			
			
		
		if(NomAplicacionActual.Contains("InDesign"))
		{
			if(N2P_Pagina.NumUTF16TextChars()>0)//
			{
				if(N2P_Pagina!=Pagina)//e va a cambiar el nombre del documento
				{
							
					FileToRemove= PrefConections.PathOfServerFile ;
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( N2P_Pagina );
					FileToRemove.Append(".indd");

							
					ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
					fecha_Guion= PreferencesPara.Date_To_SavePage;
					SDKUtilities::Replace(fecha_Guion,"/","-");
					SDKUtilities::Replace(fecha_Guion,"/","-");
				
					//Arma la Ruta para el Documento InDesign
					File= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(File);
					File.Append("apaginas");
					SDKUtilities::AppendPathSeparator(File);
					File.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(File);
					File.Append(FechaParaRuta);
					SDKUtilities::AppendPathSeparator(File);
					File.Append( NombrePublicacion);
					SDKUtilities::AppendPathSeparator(File);
					File.Append( NombreSeccion );
					SDKUtilities::AppendPathSeparator(File);
					File.Append( IDUsuario);
					SDKUtilities::AppendPathSeparator(File);
					File.Append( Pagina );
					File.Append(".indd");
				
					if(NomAplicacionActual.Contains("InDesign"))//si nos encontramos en InDesign
					{
						sysFile=FileUtils::PMStringToSysFile(File);
						if(FileUtils::DoesFileExist(sysFile))
						{
							CAlert::InformationAlert(kN2PInOutYaExistePaginaConEsteFolioStringAlert);
							break;
						}
									
					}	
							
					//Arma la ruta para os ducumento de Versiones y fotos o previews
					FotoVersionPagina=PrefConections.PathOfServerFile ;
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(FechaParaRuta);
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(NombrePublicacion); 
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(NombreSeccion);
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(IDUsuario);
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							
					FotoLastPagina=PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(FechaParaRuta );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(NombrePublicacion );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(NombreSeccion );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(IDUsuario);
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
						

					FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
					FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
					if(Estatus==PrefConections.EstadoParaExportarATWEB)
						checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage,PrefConections.N2PResizePreview, PrefConections.N2PResizeHeight,PrefConections.N2PResizeWidth); //Toma Foto de paginas para obtener el Preview
					else
						checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage);
					
					//Para ruta de copia de pagina
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append("News2Page");
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(NombrePublicacion);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(FechaParaRuta);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(NombreSeccion);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(IDUsuario);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(PreferencesPara.Pagina_To_SavePage +".indd");
				
					//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage);
				
					//Obtiene Folio del XMP si es que existe
			
					CadenaSinDosRutas=FotoVersionPagina;
					//Remueve todo hasta a paginas
					if(CadenaSinDosRutas.NumUTF16TextChars()>0)
					{
						int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
						if(indexapaginas>0)
							CadenaSinDosRutas.Remove(0, indexapaginas+9);
					}
					//		CAlert::InformationAlert("Final "+CadenaSinDosRutas);
					CopiaToLast=FotoLastPagina;
					CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
					CopiaToLast.Append("_last.jpg");
					fechaCrea.Append(" " + horaCrea);
							
					//Quiere Decir que es el Primer CheckIn
			
					//Si es en InDesign significa que si se cambio el nombre desde inDesign
					//Si es inCopy significa que cambiaron el nombre desde InDesign por lo tanto solo se actualiza datos iod evento 15
					if(NomAplicacionActual.Contains("InDesign"))
						ID_Evento="31";
											
					//Si Es el Primer CheckIn
				
					//Arma Folio
					N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
					N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
				}
				else
				{//Hace un save normal
				
					fecha_Guion = XMPUtils->GetXMPVar("N2PDate");
					ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				
					SDKUtilities::Replace(fecha_Guion,"/","-");
					SDKUtilities::Replace(fecha_Guion,"/","-");
									
					//Arma la Ruta para el Documento InDesign
					//Arma la Ruta para el Documento InDesign
					File= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(File);
					File.Append("apaginas");
					SDKUtilities::AppendPathSeparator(File);
					File.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(File);
					File.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
					SDKUtilities::AppendPathSeparator(File);
					File.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( PreferencesPara.Pagina_To_SavePage  );
					File.Append(".indd");
				
					//Arma la ruta para os ducumento de Versiones y fotos o previews
							
					FotoVersionPagina=PrefConections.PathOfServerFile ;
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath")); 
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath"));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoLastPagina=PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(FotoLastPagina);

					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
					if(Estatus==PrefConections.EstadoParaExportarATWEB)
						checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"),PrefConections.N2PResizePreview, PrefConections.N2PResizeHeight,PrefConections.N2PResizeWidth);
					else
						checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));	//Toma Foto de paginas para obtener el Preview
					
					
				
					//Para ruta de copia de pagina
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append("News2Page");
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PPagina")+".indd");
				
					//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));
							
					//Obtiene Folio del XMP si es que existe
			
					CadenaSinDosRutas=FotoVersionPagina;
					//Remueve todo hasta a paginas
					if(CadenaSinDosRutas.NumUTF16TextChars()>0)
					{
						int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
						if(indexapaginas>0)
							CadenaSinDosRutas.Remove(0, indexapaginas+9);
					}
					//	CAlert::InformationAlert("G "+CadenaSinDosRutas);	
					CopiaToLast=FotoLastPagina;
					CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
					CopiaToLast.Append("_last.jpg");
					fechaCrea.Append(" " + horaCrea);
							
					if(NomAplicacionActual.Contains("InDesign"))
						ID_Evento="5";
					
					
				}
			
			
				/*///////////////////////
				///Se Pregunta aqui pues se supone que es una pagina ya adiconada al sistema
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
				//CAlert::InformationAlert(Busqueda);
			
				if(QueryVector.Length()>0)
				{
					//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
					RutaPaInCopy =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
				}
				else
				{
					CAlert::InformationAlert("Error 101 no se encontraron datos para el id de pagina especificado");
					break;
				}*/
			}
			else
			{
				//CAlert::InformationAlert("No existe un nombre para esta pagina en XMP por lo tanto se supone que es una pagina nueva");
				fecha_Guion= PreferencesPara.Date_To_SavePage;
				SDKUtilities::Replace(fecha_Guion,"/","-");
				SDKUtilities::Replace(fecha_Guion,"/","-");
			
				//Arma la Ruta para el Documento InDesign
				File= PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(File);
				File.Append("apaginas");
				SDKUtilities::AppendPathSeparator(File);
				File.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(File);
				File.Append(FechaParaRuta);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( NombrePublicacion);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( NombreSeccion );
				SDKUtilities::AppendPathSeparator(File);
				File.Append( IDUsuario);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( PreferencesPara.Pagina_To_SavePage );
				File.Append(".indd");
				
				//Verifica que no exista ningun archivo fisico con este mismo nombre sobre la ruta establecida
				sysFile=FileUtils::PMStringToSysFile(File);
				if(FileUtils::DoesFileExist(sysFile))
				{
					CAlert::InformationAlert(kN2PsqlYaExistePaginaConEsteNombreStringKey);
					break; 
				}


				//Arma la ruta para os ducumento de Versiones y fotos o previews
				
				FotoVersionPagina=PrefConections.PathOfServerFile ;
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(FechaParaRuta);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(NombrePublicacion); 
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(NombreSeccion);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(IDUsuario);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);

				FotoLastPagina=PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(FechaParaRuta );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(NombrePublicacion );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(NombreSeccion );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(IDUsuario);
				SDKUtilities::AppendPathSeparator(FotoLastPagina);

				FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
				FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
				if(Estatus==PrefConections.EstadoParaExportarATWEB)
					checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage,kTrue, 1772); //Toma Foto de paginas para obtener el Preview
				else
					checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage);
			
				//Para ruta de copia de pagina
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append("News2Page");
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombrePublicacion );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(FechaParaRuta );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombreSeccion );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(IDUsuario);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(PreferencesPara.Pagina_To_SavePage+".indd");
			
			
				//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
				
				//Obtiene Folio del XMP si es que existe
			
				CadenaSinDosRutas=FotoVersionPagina;
					
				//Remueve todo hasta a paginas
				if(CadenaSinDosRutas.NumUTF16TextChars()>0)
				{
					int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
					if(indexapaginas>0)
						CadenaSinDosRutas.Remove(0, indexapaginas+9);
				}
				//CAlert::InformationAlert("X "+CadenaSinDosRutas);
				CopiaToLast=FotoLastPagina;
				CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
				CopiaToLast.Append("_last.jpg");
				fechaCrea.Append(" " + horaCrea);
						
				//Quiere Decir que es el Primer CheckIn
						
				ID_Evento="8";
						
				//Si Es el Primer CheckIn
				
				//Arma Folio
				N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
				N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
			}
		}
		else
		{	//Hace un save normal en InCopy
			//Pues se supone que unicamente deposita
			fecha_Guion = XMPUtils->GetXMPVar("N2PDate");
			ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				
			SDKUtilities::Replace(fecha_Guion,"/","-");
			SDKUtilities::Replace(fecha_Guion,"/","-");
									
			//Arma la Ruta para el Documento InDesign
			//Arma la Ruta para el Documento InDesign
			File= PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(File);
			File.Append("apaginas");
			SDKUtilities::AppendPathSeparator(File);
			File.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(File);
			File.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
			SDKUtilities::AppendPathSeparator(File);
			File.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
			SDKUtilities::AppendPathSeparator(File);
			File.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
			SDKUtilities::AppendPathSeparator(File);
			File.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				SDKUtilities::AppendPathSeparator(File);
			File.Append( PreferencesPara.Pagina_To_SavePage  );
			File.Append(".indd");
			
			//Arma la ruta para os ducumento de Versiones y fotos o previews
							
			FotoVersionPagina=PrefConections.PathOfServerFile ;
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append("apaginas");
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath")); 
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath"));
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoLastPagina=PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append("apaginas");
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
			SDKUtilities::AppendPathSeparator(FotoLastPagina);

			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
			if(Estatus==PrefConections.EstadoParaExportarATWEB)
				checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"),PrefConections.N2PResizePreview, PrefConections.N2PResizeHeight,PrefConections.N2PResizeWidth);	//Toma Foto de paginas para obtener el Preview
			else
				checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));
			
				
			//Para ruta de copia de pagina
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append("News2Page");
			
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
				
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
			
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PPagina")+".indd");
				
			//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));
							
			//Obtiene Folio del XMP si es que existe
			
			CadenaSinDosRutas=FotoVersionPagina;
			//Remueve todo hasta a paginas
			if(CadenaSinDosRutas.NumUTF16TextChars()>0)
			{
				int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
				if(indexapaginas>0)
					CadenaSinDosRutas.Remove(0, indexapaginas+9);
			}
			//		CAlert::InformationAlert("Final "+CadenaSinDosRutas);		
			CopiaToLast=FotoLastPagina;
			CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
			CopiaToLast.Append("_last.jpg");
			fechaCrea.Append(" " + horaCrea);
						
			ID_Evento="13";
			
			///////////////////////
			///Se Pregunta aqui pues se supone que es una pagina ya adiconada al sistema
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
			//CAlert::InformationAlert(Busqueda);
			
			if(QueryVector.Length()>0)
			{
				//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
				RutaPaInCopy =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
			}
			else
			{
				CAlert::InformationAlert("Error 101 no se encontraron datos para el id de pagina especificado");
				break;
			}
					
		}
		////////////////
		
		
		PMString SPCallPaginnaChickIn="CALL PaginaCheckInStoreProc(";
		SPCallPaginnaChickIn.Append(ID_Pagina);
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(PreferencesPara.Issue_To_SavePage);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(MySQLDate);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Seccion_To_SavePage);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(N2PFolio_Pagina);
		SPCallPaginnaChickIn.Append("',");
		SPCallPaginnaChickIn.Append("1");	//PLIEGO
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("0");	//PAR/IMPAR
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("1");	//ID-Color
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(PreferencesPara.DirididoA_To_SavePage);	
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(IDUsuario);	//Proveniente_de
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("SQLServer");	//Servidor
		SPCallPaginnaChickIn.Append("','");
		
		
		if(NomAplicacionActual.Contains("InCopy"))
		{
			//CAlert::InformationAlert("KKKK "+RutaPaInCopy);
			SPCallPaginnaChickIn.Append(RutaPaInCopy);
			
		}
		else
		{
			//CAlert::InformationAlert("KKKK "+File);
			SPCallPaginnaChickIn.Append(File);	//Ruta_Elemento 
		}
		
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Pagina_To_SavePage);	//Nombre_Archivo 
		SPCallPaginnaChickIn.Append("',");
		SPCallPaginnaChickIn.Append("@A");	//Fecha_Creacion
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("@X");	//Fecha_Ult_Mod
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("0");	//Webable
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("10");	//Calificacion
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append("Pagina");	//Id_TipoElemento
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Estado_To_SavePage);	//Id_Estatus
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("No se que es Cameo");	//Cameo
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(IDUsuario);	//Id_Empleado
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("0");	//Fech_Evento
		SPCallPaginnaChickIn.Append("',");
				
		SPCallPaginnaChickIn.Append(ID_Evento);	//Id_Evento
				
		SPCallPaginnaChickIn.Append(",'");
		//CAlert::InformationAlert("Final "+CadenaSinDosRutas);
		SPCallPaginnaChickIn.Append(CadenaSinDosRutas);	//Ruta_Previo
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("Check In");	//Descripcion_Evento
		SPCallPaginnaChickIn.Append("','");
		
		if(NomAplicacionActual.Contains("InCopy"))
		{
			PMString CadenaToWindows=RutaPaInCopy;
			#ifdef WINDOWS
				PMString Diagonal="";
				Diagonal.AppendW(92);
				PMString DDiagonal="";
				DDiagonal.Append("\\\\");
				N2PSQLUtilities::ReplaceAllOcurrencias(CadenaToWindows,Diagonal,DDiagonal);
			#endif
			//CAlert::InformationAlert("WWWWW "+CadenaToWindows);
			SPCallPaginnaChickIn.Append(CadenaToWindows);
		}
		else
		{
			PMString CadenaToWindows=CopiaToLast;
			#ifdef WINDOWS
				PMString Diagonal="";
				Diagonal.AppendW(92);
				PMString DDiagonal="";
				DDiagonal.Append("\\\\");
				N2PSQLUtilities::ReplaceAllOcurrencias(CadenaToWindows,Diagonal,DDiagonal);
			#endif
			//CAlert::InformationAlert("WWWWW "+CadenaToWindows);
			SPCallPaginnaChickIn.Append(CadenaToWindows);	//Ruta_Elemento 
		}
		
		
		SPCallPaginnaChickIn.Append("');");
				
				
		//CAlert::InformationAlert(StringConection);	
		//CAlert::InformationAlert(SPCallPaginnaChickIn);	
				
		UpdateStorys->GuardarQuery(SPCallPaginnaChickIn);
		
		QueryVector.clear();	
		if(!SQLInterface->SQLQueryDataBase(StringConection,SPCallPaginnaChickIn,QueryVector))
		{
			CAlert::InformationAlert("Hubo un error y se va a salir del ciclo");
			break;
		}
		else
		{
			PMString cadena = SQLInterface->ReturnItemContentbyNameColumn("Fecha_CreacionIN",QueryVector[0]);
			PMString Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[0]);
			ID_Pagina = SQLInterface->ReturnItemContentbyNameColumn("Pagina_IDX",QueryVector[0]);
			//CAlert::InformationAlert(cadena);
			if(Resultado=="9")
			{
				CAlert::InformationAlert(kN2PsqlYaExistePaginaConEsteNombreStringKey);	
				break;
			}
			else
			{	if(cadena=="SinPrivilegios" || cadena.NumUTF16TextChars()<=0)
				{
					CAlert::InformationAlert(kN2PsqlSinPermisosToCheckinPageStringKey);		
					break;
				}
							
			}
		}
					
		//CAlert::InformationAlert("8");	
		if(PreferencesPara.Date_To_SavePage!=XMPUtils->GetXMPVar("N2PDate"))
		{
			XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);
		}

		if( ID_Evento=="8" || ID_Evento=="32")
		{
			//XMPUtils->CreatePropertiesN2P(document);
			XMPUtils->SaveXMPVar("N2PPublicacion",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PAplicacion",NomAplicacionActual);
			XMPUtils->SaveXMPVar("N2PEstatus",PreferencesPara.Estado_To_SavePage);
			XMPUtils->SaveXMPVar("N2PDirigidoA",PreferencesPara.DirididoA_To_SavePage);
			XMPUtils->SaveXMPVar("N2PSeccion",PreferencesPara.Seccion_To_SavePage);
			XMPUtils->SaveXMPVar("N2PPagina",PreferencesPara.Pagina_To_SavePage);
			XMPUtils->SaveXMPVar("N2PIssue",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PFechaCreacionPath",FechaParaRuta);
			XMPUtils->SaveXMPVar("N2PUsuarioCredor",IDUsuario); 
			XMPUtils->SaveXMPVar("N2PNPubliCreacionPath",NombrePublicacion);
			XMPUtils->SaveXMPVar("N2PNSecCreacionPath",NombreSeccion);
			//Poner la fecha actual
			if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()==0)
				XMPUtils->SaveXMPVar("N2PDate","00/00/0000");
			else
				XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);

			XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
			XMPUtils->SaveXMPVar("N2PSQLIDPagina",ID_Pagina);
			XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
		}
		
		
		if(ID_Evento=="31")
		{
			//XMPUtils->CreatePropertiesN2P(document);
			XMPUtils->SaveXMPVar("N2PPublicacion",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PAplicacion",NomAplicacionActual);
			XMPUtils->SaveXMPVar("N2PEstatus",PreferencesPara.Estado_To_SavePage);
			XMPUtils->SaveXMPVar("N2PDirigidoA",PreferencesPara.DirididoA_To_SavePage);
			XMPUtils->SaveXMPVar("N2PSeccion",PreferencesPara.Seccion_To_SavePage);
			XMPUtils->SaveXMPVar("N2PPagina",Pagina);
			XMPUtils->SaveXMPVar("N2PIssue",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PFechaCreacionPath",FechaParaRuta);
			XMPUtils->SaveXMPVar("N2PUsuarioCredor",IDUsuario); 
			XMPUtils->SaveXMPVar("N2PNPubliCreacionPath",NombrePublicacion);
			XMPUtils->SaveXMPVar("N2PNSecCreacionPath",NombreSeccion);
			//Poner la fecha actual
			if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()==0)
				XMPUtils->SaveXMPVar("N2PDate","00/00/0000");
			else
				XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);

			XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
			XMPUtils->SaveXMPVar("N2PSQLIDPagina",ID_Pagina);
			XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
		}
					
		if(NomAplicacionActual.Contains("InDesign"))//si nos encontramos en InDesign
		{
			sysFile=FileUtils::PMStringToSysFile(File);
			if(FileUtils::DoesFileExist(sysFile))
			{
				Utils<IDocumentUtils>()->DoSave(document); 
			}
			else
			{
						
				SDKLayoutHelper helperLayout;
				if(helperLayout.SaveDocumentAs(docUIDRef,sysFile,kFullUI)!=kSuccess)
				{
					break;
				}
			}
							
		}	
					
		if(ID_Evento=="31")
		{	
				
			remove(InterlasaUtilities::MacToUnix(FileToRemove).GrabCString());
			//CAlert::InformationAlert(FileToRemove);
		}
					
		//Para Guardar las notas de la pagina sobre la base de datos de InterAssetsTexto
		
		PMString RutaPDF="";
		bool16 ExportoPDF=kFalse;
		if(Estatus==PrefConections.EstadoParaExportarATWEB && PrefConections.LlenarTablaWEB==1)
		{
			if(!Aplicacion.Contains("InCopy"))//Se quito para enviar el contenido a notas web desde incopy tambien
			{
				
				
				/*
				 //Exportacion como PDF Generico
				 if(PrefConections.ExportarPaginaComoPDF)
				 {
				 PMString CopiaToLast=PrefConections.RutaDePaginaComoPDF;
				 if(CopiaToLast.NumUTF16TextChars()<=0)
				 {
				 CopiaToLast=FotoLastPagina;
				 CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
				 CopiaToLast.Append(".pdf");
				 }
				 else
				 {
				 PMString Separator="";
				 SDKUtilities::AppendPathSeparator(Separator);
				 if( CopiaToLast.LastIndexOfCharacter(Separator.GetChar(0)) != (CopiaToLast.NumUTF16TextChars()-1))
				 SDKUtilities::AppendPathSeparator(CopiaToLast);
				 
				 
				 CopiaToLast.Append(PreferencesPara.Pagina_To_SavePage);
				 CopiaToLast.Append(".pdf");
				 }
				 
				 // CAlert::InformationAlert(CopiaToLast);		
				 if(this->crearPDF(CopiaToLast))
				 ExportoPDF=kTrue;
				 }*/
				
				
				//Exportar Edicion Editorial
				if(PrefConections.ExportPDFPresets)
				{
					RutaPDF=PrefConections.RutaDePaginaComoPDF;
					if(RutaPDF.NumUTF16TextChars()<=0)
					{
						RutaPDF=FotoLastPagina;
						RutaPDF.Remove( RutaPDF.LastIndexOfWChar(95) , RutaPDF.NumUTF16TextChars()-RutaPDF.LastIndexOfWChar(95) );
						RutaPDF.Append(".pdf");
					}
					else
					{
						PMString Separator="";
						SDKUtilities::AppendPathSeparator(Separator);
						if( RutaPDF.LastIndexOfCharacter(Separator.GetChar(0)) != (RutaPDF.NumUTF16TextChars()-1))
							SDKUtilities::AppendPathSeparator(RutaPDF);
						
						
						RutaPDF.Append(PreferencesPara.Pagina_To_SavePage);
						RutaPDF.Append(".pdf");
					}
					N2PSQLUtilities::ExportPDFAsPresetName( RutaPDF,PrefConections.PresetTOPDFEditorial);
					//CAlert::InformationAlert(PrefConections.PresetTOPDFEditorial);
				}
				
				
				
				//Exportar Edicion WEB
				//PMString 
				RutaPDF=PrefConections.RutaDePaginaComoPDFWEB;
				if(PrefConections.ExportPDFPresetsEditWEB)
				{
					
					if(RutaPDF.NumUTF16TextChars()<=0)
					{
						RutaPDF=FotoLastPagina;
						RutaPDF.Remove( RutaPDF.LastIndexOfWChar(95) , RutaPDF.NumUTF16TextChars()-RutaPDF.LastIndexOfWChar(95) );
						RutaPDF.Append(".pdf");
					}
					else
					{
						PMString Separator="";
						SDKUtilities::AppendPathSeparator(Separator);
						if( RutaPDF.LastIndexOfCharacter(Separator.GetChar(0)) != (RutaPDF.NumUTF16TextChars()-1))
							SDKUtilities::AppendPathSeparator(RutaPDF);
						
						
						RutaPDF.Append(PreferencesPara.Pagina_To_SavePage);
						RutaPDF.Append(".pdf");
					}
					N2PSQLUtilities::ExportPDFAsPresetName(  RutaPDF,PrefConections.PresetTOPDFEditorialWEB);
					//CAlert::InformationAlert(PrefConections.PresetTOPDFEditorialWEB);
				}
				
				//Mete notas a Base dee datos IntrerAsetText o Hemeroteca
				this->PonerNotasEnDBConstructor(Seccion,Issue,MySQLDate,Pagina,RutaPDF);	
				
			}
			
			//Para Tabla N2P_Joomla_content
			UpdateStorys->ExportarNotasN2P_A_N2PJoomla(document, kTrue);
			UpdateStorys->ExportarNomFotosEnPagA_N2P_Joomla_Multimedia(document);
			UpdateStorys->ExportarFotosNoFluidas_DeNotasEnPagina_A_N2P_Joomla_Multimedia(document);
			///Para Tabla Notas WEB
			//UpdateStorys->ExportarNotasATablaWEB(document, kTrue);	
			//UpdateStorys->ExportarNomFotosEnPagATablaWEB(document);	
			
			if(ExportoPDF)
				CAlert::InformationAlert(kN2PCheckInOutPDFExportadoStringKey);		
			else
				CAlert::InformationAlert(kN2PCheckInOutNotasEnEdicionWEBAlertInfoStringKey);
			
			//CAlert::InformationAlert(RutaPDF);
			if(PrefConections.N2PHmteSendToHmca && !Aplicacion.Contains("InCopy") )
			{
				if(RutaPDF.NumUTF16TextChars()<=0)
				{
					//CAlert::InformationAlert(RutaPDF);
					//CAlert::InformationAlert(CopiaToLast);
					CAlert::InformationAlert("No fue creado el Archivo PDF.");
					
				}
				UpdateStorys->ExportarPaginaYNotasABDHemeroteca(document,RutaPDF,CadenaSinDosRutas,SPCallPaginnaChickIn,PrefConections);
			}
			
		}
		
		
		
			
		//Obtiene preferencias de Check in en documento de preferencias"ParametrosDeDialogosN2PSQL.pfd"
		checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
			
		PreferencesPara.Estado_To_SavePage=Estatus;
		PreferencesPara.Seccion_To_SavePage=Seccion;
		PreferencesPara.Pagina_To_SavePage=Pagina;
		PreferencesPara.Issue_To_SavePage=Issue;
		PreferencesPara.Date_To_SavePage=Date;
		PreferencesPara.DirididoA_To_SavePage=DirigidoA;
		//Guarda preferencias de Check in en documento de preferencias "ParametrosDeDialogosN2PSQL.pfd"
		checkIn->SetPreferencesParametros(&PreferencesPara);
		
		
		
		if(PrefConections.CreateBackUpPagina==1)
		{	
			#ifdef MACINTOSH
				N2PSQLUtilities::CreateFolder(PAthToCopyDocumentBackUp,PAthToCopyDocumentBackUp);
				IDFile sysxFile=SDKUtilities::PMStringToSysFile(&PAthToCopyDocumentBackUp); 
				//Guardar Copia de documento localmente
				InterlasaUtilities::SaveCopyOfDocumentAs(docUIDRef,	sysxFile,kFullUI);
			#elif defined(WINDOWS)
				IDFile sysxFile=SDKUtilities::PMStringToSysFile(&PAthToCopyDocumentBackUp);
				PMString pathName;
				FileUtils::GetPathOnly(sysxFile, pathName, kFalse);
				//CAlert::InformationAlert(pathName);
				N2PSQLUtilities::CreateFolder(pathName,pathName);
				//CAlert::InformationAlert(PAthToCopyDocumentBackUp);
				InterlasaUtilities::SaveCopyOfDocumentAs(docUIDRef,	sysxFile,kFullUI);
			#endif
			
		}
			
		
		//Cierra el documento
		SDKLayoutHelper Helper;
		errorValue = helperLayout.CloseDocument(docUIDRef,kFalse,kSuppressUI,kFalse,IDocFileHandler::kSchedule);
	
		UpdateStorys->LimpiarOLlenarListaDeNotas();
		//N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutDialogController::CheckInPagina ini");
	return errorValue;
}