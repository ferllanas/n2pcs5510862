/*
//	File:	ICheckInOutSuite.h
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#ifndef _ICheckInOutSuite_
#define _ICheckInOutSuite_

#include "IPMUnknown.h"
#include "N2PCheckInOutID.h"

struct PrefParamDialogInOutPage
{
	PMString Estado_To_PageIn;
	PMString Seccion_To_PageIn;
	PMString Pagina_To_PageIn;
	PMString Issue_To_PageIn;
	PMString Date_To_PageIn;
	PMString DirididoA_To_PageIn;
	
	PMString Estado_To_SavePage;
	PMString Seccion_To_SavePage;
	PMString Pagina_To_SavePage;
	PMString Issue_To_SavePage;
	PMString Date_To_SavePage;
	PMString DirididoA_To_SavePage;
	
	PMString StatusNotaPageOut;
	PMString SeccionNotaPageOut;
	PMString PaginaNotaPageOut;
	PMString IssueNotaPageOut;
	PMString DateNotaPageOut;
	PMString DirididoANotaPageOut;
};


struct ClassDialogCheckOutPaginaSets
{
	PMString Status_PageOut;
	PMString Seccion_PageOut;
	PMString Pagina_PageOut;
	PMString Issue_PageOut;
	PMString Date_PageOut;
	PMString DirididoA_PageOut;
};

class ICheckInOutSuite: public IPMUnknown
{
public:
	enum { kDefaultIID = IID_ICheckInOutSUITE };

	virtual bool16 CheckIn()=0;
	virtual bool16 CheckOut()=0;
	virtual bool16 SaveRevi(bool16 ToCheckInOnDB)=0;
	virtual bool16 AutomaticSaveRevi(IDocument* document,const bool16 DuplicandoPagina = kFalse)=0;
	
	virtual bool16 TomarFotosDeDocumento(PMString& FotoVersionPagina,
										 PMString& FotoLastPagina,
										 PMString NamePage,
										 const bool16 isResize=kFalse,
										 const PMReal& alto=1, 
										 const PMReal& with=1)=0;
	
	virtual bool16 ActualizaFotosDeDocumento(PMString& RutaToSaveFile,PMString& RutaTOJpg,PMString NamePage)=0;
	
	virtual bool16 GetPreferencesParametrosOfFile(PrefParamDialogInOutPage *PreferencesPara)=0;
	
	virtual bool16 SetPreferencesParametros(PrefParamDialogInOutPage *PreferencesPara)=0;
	
	virtual PMString FechaYHora()=0;
	
	virtual PMString FechaYHoraActual()=0;
	
	virtual bool16 InsertEnMovimientos(PMString &Busqueda)=0;
	
	virtual bool16 GetPreferencesParametrosOfDB(PrefParamDialogInOutPage *PreferencesPara)=0;
	
	virtual bool16 StoreProcCheckInPage(K2Vector<PMString>& QueryVector)=0;
	
	virtual bool16 StoreProcSaveRevPage(K2Vector<PMString>& QueryVector)=0;
	
	virtual bool16 StoreProcCheckOutPage(K2Vector<PMString>& QueryVector)=0;

	virtual bool16 ObtenerDatosDeCheckOutDPaginas(ClassDialogCheckOutPaginaSets& CheckOutPaginaSets)=0;

	virtual bool16 GuardaDatosDeCheckOutDPaginas(ClassDialogCheckOutPaginaSets CheckOutPaginaSets)=0;
	virtual bool16 TomarFotosDeSpread(PMString &RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage)=0;
	
	virtual bool16 DuplicarPagina()=0;
	
	/*
		0 cancelar
		1 no 
		2 si
	*/
	virtual int32 OpenDialogoQuestion(PMString Question)=0;
private:
//	CString QueryKey(HKEY hKey, LPTSTR sPsth, LPCTSTR lpSubKey);

	void DoDialogCheckIn();

	void DoDialogCheckOut();

	bool16 DoDialogGuardaRev();
	
	
	
};
#endif // _ICheckInOutSuite_
