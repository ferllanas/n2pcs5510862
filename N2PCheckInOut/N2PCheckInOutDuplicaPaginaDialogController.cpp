/*
//	File:	DuplicarPaginaDialogControler.cpp
//
//	Date:	28-Jan-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2004 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/





#include "VCPlugInHeaders.h"


#include "IFileUtility.h"
// Interface includes:
// Interface includes:
#include "IApplication.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"

#include "IDocument.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "ICoreFilename.h"
#include "IDocumentUtils.h"
#include "IDataBase.h"


#include "FileUtils.h"
#include "ErrorUtils.h"
#include "ILayoutUIUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
#include "OpenPlaceID.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "SDKUtilities.h"
#include "SDKFileHelper.h"
#include "SDKLayoutHelper.h"

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "PlatformFileSystemIterator.h"
#include "ImportExportUIID.h"

#ifdef WINDOWS
	#include <sys/types.h> 
	#include <sys/stat.h> 
#endif
#ifdef MACINTOSH
	#include <types.h> 
	#include <sys/stat.h>
#endif

#include <time.h>
#include <locale.h> 
#include <Math.h> 

// Project includes:
#include "N2PCheckInOutID.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "ICheckInOutSuite.h"
#include "IN2PXMPUtilities.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif
//#include <winreg.h>    //If you are using MS Visual C++ you do not need this
//#include <direct.h>
//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")

//#using <mscorlib.dll>

//using namespace System;
//using namespace System::IO;

/** DuplicarPaginaDialogControler
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class DuplicarPaginaDialogControler : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		DuplicarPaginaDialogControler(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~DuplicarPaginaDialogControler() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		virtual void UserCancelled();
		
	private:

		
		bool16 LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		bool16 LLenar_Combo_Seccion(PMString Id_SeccionText);	
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
		
		bool16 LLenar_Combo_EstatusPagina(PMString& Id_EstatusActual);
		
		PMString  Aplicar_Usuario_Actual();

		PMString BuscarEnUsuarios(PMString &Busqueda);

		void LLenar_Combo_DirigidoA();
		
		void SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget);
		
		bool16 EnableDisableWidget(WidgetID widget,bool16 Enabled);
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
		
		ErrorCode CheckInPagina(int32 Mantener_Notas);
		//
		
		PMString Aplicacion;			//Nombre de la aplicacion sobre la que nos encontramos trabajando
		PMString IDUsuario;			//ID del Usuario que se encuentra trabajando en este momento
		PMString HoraIn;				//Hora que entro a trabajar el usuario
		PMString FechaIn;			//Fecha que entro el usuario a trabajar
			
		PMString horaCrea;			//Datos de la ultima pagina chekineada
		PMString fechaCrea;
		PMString Seccion;
		PMString DirigidoA;
		PMString Estatus;
		PMString Pagina;
		PMString Date;
		PMString Issue;
		PMString NomAplicacionActual;
		PMString Busqueda;
		PMString File;
		PMString ID_Pagina;
		PMString ID_Evento;
		PMString N2PFolio_Pagina;
		PMString CadenaSinDosRutas;
		PMString CopiaToLast;
		PMString StringConection;
		PMString MySQLDate;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(DuplicarPaginaDialogControler, kCheckOutDuplicarPaginaDialogControlerImpl)

/* ApplyDialogFields
*/
void DuplicarPaginaDialogControler::InitializeDialogFields(IActiveContext* context) 
{
	
	PrefParamDialogInOutPage PreferencesPara;	//Preferencias
	
	PMString CopyPage="";	//Copia del nombre de la pagina
	PMString FrontDocName="";// documento actualPMString DirigidoA="";// documento actual

	
	//Limpia TextEdits
	SetTextControlData(kPaginaWidgetID,"");
	SetTextControlData(kDatePubliWidgetID,"");
	SetTextControlData(kComboBoxIssueWidgetID,"");

	////Llena Te4xtEdit del Usuario en el sistema
	PMString Usuario=this->Aplicar_Usuario_Actual();


	

	do
	{
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		if(NomAplicacionActual.Contains("InCopy"))
		{
			//this->EnableDisableWidget(kComboBoxStatusWidgetID,kFalse);
			//this->EnableDisableWidget(kComboBoxDirigidoAWidgetID,kFalse);
			this->EnableDisableWidget(kPaginaWidgetID,kFalse);
		}
		
		//
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
		
		//Obtiene el nombre del documento actual
		document->GetName(FrontDocName);
		
		
		
		//
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
		
			//obtiene Fecha y hora actuales del sistema
		
		
		PMString fecha = checkIn->FechaYHoraActual();
		PMString* Hora = fecha.Substring(0,fecha.IndexOfWChar(94));
		fecha.Remove(0,fecha.IndexOfWChar(94)+2);
		PMString hora = Hora->GrabCString();

		//Coloca fecha y hora actules del sistema
		fecha.SetTranslatable(kFalse);
		SetTextControlData(kFechaWidgetID,fecha);
		
		hora.SetTranslatable(kFalse);
		SetTextControlData(kHraCreacionWidgetID,hora);
	
	
		//Obtiene las Preferencias con que se modifico la ultima vez el documento
		checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
		
		
		CopyPage = PreferencesPara.Pagina_To_SavePage;
		CopyPage.Append(".indd");	
		
		
		
	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			break;
		}

		
		this->LLenar_Combo_DirigidoA();
		
		document->GetName(FrontDocName);
		
		
		//si el nombre del documento actual es diferente al nombre de la ultima pagina modificada
		// los datos se quedan en blanco
		if(CopyPage==FrontDocName) 
		{	
			this->SeleccionarCadenaEnComboBox(PreferencesPara.DirididoA_To_SavePage, kComboBoxDirigidoAWidgetID);
		}
		else
		{	
			this->SeleccionarCadenaEnComboBox(Usuario, kComboBoxDirigidoAWidgetID);
		}
		
			
		//Selecciona el Estado Anterior		
		LLenar_Combo_EstatusPagina(PreferencesPara.Estado_To_SavePage);
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Estado_To_SavePage, kComboBoxStatusWidgetID);
		
		//Selecciona la seccion Anterior
		this->LLenar_Combo_Publicacion(PreferencesPara.Issue_To_SavePage);		
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Issue_To_SavePage, kComboBoxIssueWidgetID);

		
		//Selecciona la seccion Anterior
		this->LLenar_Combo_Seccion(PreferencesPara.Seccion_To_SavePage);	
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Seccion_To_SavePage, kComboBoxSeccionWidgetID);
		
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
				(
					kN2PXMPUtilitiesBoss,	// Object boss/class
					IN2PXMPUtilities::kDefaultIID
				)));
				
		PMString Date = XMPUtils->GetXMPVar("A2PFechaEdicionDPagina");
		if(Date.NumUTF16TextChars()>0)
		{
			Date.Append("/");
			PMString *Anno=Date.GetItem("/",1);
			PMString *MM=Date.GetItem("/",2);
			PMString *DD=Date.GetItem("/",3);
		
			Date="";
			if(MM->NumUTF16TextChars()>0)
			{
				Date.Append(MM->GrabCString());
				Date.Append("/");
			}
			if(DD->NumUTF16TextChars()>0)
			{
				Date.Append(DD->GrabCString());
				Date.Append("/");
			}
			if(DD->NumUTF16TextChars()>0)
			{
				Date.Append(Anno->GrabCString());
			
			}
		}
		else
		{
			Date=InterlasaUtilities::FormatFechaYHoraManana("%m/%d/%Y");
		}
		
		if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()<=0)
		 	SetTextControlData(kDatePubliWidgetID,Date);
		else
		{
			PreferencesPara.Date_To_SavePage.SetTranslatable(kFalse);
			SetTextControlData(kDatePubliWidgetID,PreferencesPara.Date_To_SavePage);
		}
		
		
		//
		
		PMString Pagina = XMPUtils->GetXMPVar("A2PFolioDPagina");
		
		SetTextControlData(kPaginaWidgetID,"");
		/*if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars()<=0)
		 	SetTextControlData(kPaginaWidgetID,Pagina);
		else
		{
			PreferencesPara.Pagina_To_SavePage.SetTranslatable(kFalse);
			SetTextControlData(kPaginaWidgetID,PreferencesPara.Pagina_To_SavePage);
		}*/
		
		/**************************/
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
			
		IControlView * ComboSeccionView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
			if(ComboSeccionView==nil)
			{
				
				break;
			}
			
		IControlView * ComboPublicacionView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
			if(ComboPublicacionView==nil)
			{
				
				break;
			}
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			if(NomAplicacionActual.Contains("InCopy"))
			{
				ComboSeccionView->Enable(kFalse);
				ComboPublicacionView->Enable(kFalse);
			}
			
			//CAlert::InformationAlert("tum tururtui");
		}
		else
		{
			
			ComboSeccionView->Enable(kTrue);	
			ComboPublicacionView->Enable(kTrue);
		}
	}while(false);

}

/* ValidateDialogFields
*/
WidgetID DuplicarPaginaDialogControler::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result=kNoInvalidWidgets;
	
	do{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		
		
		
		PMString NombrePag =  this->GetTextControlData(kPaginaWidgetID);
		if(NombrePag.NumUTF16TextChars()<=0)
		{
			result = kPaginaWidgetID;
			break;
		}
		
		PMString Publicacion =  this->GetTextControlData(kComboBoxIssueWidgetID);
		if(Publicacion.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Seccion =this->GetTextControlData(kComboBoxSeccionWidgetID);
		if(Seccion.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Estatus =this->GetTextControlData(kComboBoxStatusWidgetID);
		if(Estatus.NumUTF16TextChars()<=0 && !NomAplicacionActual.Contains("InCopy"))
		{
			result = kN2PsqlTextStatusWidgetID;
			break;
		}
	
		PMString DirigidoA =this->GetTextControlData(kComboBoxDirigidoAWidgetID);
		/*if(DirigidoA.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlTextStatusWidgetID;
			break;
		}*/
	
		
		///////Obtiene la Interface ICheckInOutSuite para mandar hablar a sus metodos
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
			break;
			
		
		//////////
		//Preguntar si desligar las notas o dejarlas ligadas.
		PMString Question(kCheckInOutDuplicarNotasStringKey);
		Question.Translate();
  		Question.SetTranslatable(kTrue);
  					
		//int32 Accion=checkIn->OpenDialogoQuestion(Question);
		const int ActionAgreedValue=1;
		int32 resultado=-1;
		resultado=CAlert::ModalAlert(Question,
									kYesString, 
									kCancelString,kNoString,
									
									ActionAgreedValue,CAlert::eQuestionIcon);
									
									
		if(resultado==2)
		{
			
			break;			//
			//UpdateStorys->DesligarNotasYFotosDPagina(document);
		}
		
		
	
		if(this->CheckInPagina(resultado)==kSuccess)
		{
			result=kNoInvalidWidgets;
		}
		else
		{
			result=kPaginaWidgetID;
		}
	}while(false);
	SystemBeep();

	return result;
}

/* ApplyDialogFields
*/
void DuplicarPaginaDialogControler::ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId) 
{

}



/**
	Obtiene ID-Usuario del ultimo usuario que a InDesign desde el regEdit 
	para buscar en la base de datos el Nombre del usuario correspondiente al ID que se obtuvo
	y poner el nombre subre su TextEdit
*/
PMString DuplicarPaginaDialogControler::Aplicar_Usuario_Actual()
{
	PMString retval="";

	InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{	
			retval = wrkSpcPrefs->GetIDUserLogedString();
			retval.SetTranslatable(kFalse);
			SetTextControlData(kComboBoxUsuarioWidgetID,retval);
		}
	return(retval);
}



void DuplicarPaginaDialogControler::LLenar_Combo_DirigidoA()
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxDirigidoAWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Usuario From Usuario ORDER By Id_Usuario ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		PMString DirigidoA="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{	
			DirigidoA.Append(wrkSpcPrefs->GetDirigidoANota());
		}
		
		int32 Index=dropListData->GetIndex(DirigidoA);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		
		
		////////////////////////////
		
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
		(
			kN2PXMPUtilitiesBoss,	// Object boss/class
			IN2PXMPUtilities::kDefaultIID
		)));
		
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
		
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			QueryVector.clear();
			PMString Busqueda="SELECT Id_Usuario From Bitacora_Pagina WHERE Pagina_ID ="+IDPaginaXMP+" AND Id_Evento IN(5,6,13,31,32) ORDER BY ID DESC LIMIT 1";
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);

			PMString cadena="";
			
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				
				cadena = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			
			
			
			}
			
			if(cadena.NumUTF16TextChars()>0)
				this->SetTextControlData(kCheckInUltimoUsuarioQModificoWidgetID,cadena);
			
		}
	}while(false);
}



bool16 DuplicarPaginaDialogControler::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + GetTextControlData(kComboBoxIssueWidgetID) + "' ) ORDER BY Nombre_de_Seccion ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
		
			
	}while(false);
	return(kTrue);
}

		
bool16 DuplicarPaginaDialogControler::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	//kComboBoxIssueWidgetID
	
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		//Selecciona el item deseado o la Publicacion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
			
	}while(false);
	return(kTrue);
}

		
		
PMString DuplicarPaginaDialogControler::BuscarEnUsuarios(PMString &Busqueda)
{

	PMString cadena="";
	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Usuario",QueryVector[i]);
		}
		
	}while(false);
		
	
	return(cadena);
}





void DuplicarPaginaDialogControler::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			break;
		}

		
		int32 Index=dropListData->GetIndex(CadenaASeleccionar);
		
		if(Index < 0)
		{
			Index=dropListData->Length();
		}
		
		IDDLDrComboBoxSelecPrefer->Select(Index);
		
	}while(false);
}




bool16 DuplicarPaginaDialogControler::LLenar_Combo_EstatusPagina(PMString& Id_EstatusActual)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxStatusWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Estatus,Nombre_Estatus From Estatus_Elemento WHERE Id_tipo_ele='2' ORDER BY Nombre_Estatus ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
		
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			PMString IdEstatusQuery=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			if(Id_EstatusActual==IdEstatusQuery)
			{
				Id_EstatusActual=cadena;
			}
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		
		
		int32 Index=dropListData->GetIndex(Id_EstatusActual);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
			
	}while(false);
	return(kTrue);
}


int32 DuplicarPaginaDialogControler::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}


void DuplicarPaginaDialogControler::UserCancelled()
{
	SetTextControlData(kToCheckInOnDBWidgetID,"Cancel");
}

bool16 DuplicarPaginaDialogControler::EnableDisableWidget(WidgetID widget,bool16 Enabled)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		if(Enabled)
		{
			ComboCView->Enable(kTrue,kTrue);
		}
		else
		{
			ComboCView->Disable(kTrue);
		}
		
	}while(false);
	return(retval);
}




ErrorCode DuplicarPaginaDialogControler::CheckInPagina(int32 Mantener_Notas)
{
	ErrorCode errorValue=kFailure;
	IAbortableCmdSeq* seq = nil;
	ErrorCode result = kFailure;
	do
	{	
		
		
					
		seq = CmdUtils::BeginAbortableCmdSeq();
					
		Aplicacion="";			//Nombre de la aplicacion sobre la que nos encontramos trabajando
		IDUsuario="";			//ID del Usuario que se encuentra trabajando en este momento
		HoraIn="";				//Hora que entro a trabajar el usuario
		FechaIn="";			//Fecha que entro el usuario a trabajar
			
		horaCrea="";			//Datos de la ultima pagina chekineada
		fechaCrea="";
		Seccion="";
		DirigidoA="";
		Estatus="";
		Pagina="";
		Date="";
		Issue="";
		NomAplicacionActual="";
		Busqueda="";
		File="";
		ID_Pagina="0";
		ID_Evento="";
		N2PFolio_Pagina="";
		CadenaSinDosRutas="";
		CopiaToLast="";
		StringConection="";
		IDFile sysFile;
		SDKLayoutHelper helperLayout;
		PMString FileToRemove="";
			
		//////////Estructura de Preferencias
		PrefParamDialogInOutPage PreferencesPara;
		PreferencesConnection PrefConections;
			
		//////////Interface de Aplicacion
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
			
		/////////Para Obtener UIDRef del Documento
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			ASSERT_FAIL("FilActActionComponent::DoSave: document invalid");
			break;
		}
		
		UIDRef docUIDRef = ::GetUIDRef(document); //UIDRef del Documento
		IDataBase *db=docUIDRef.GetDataBase();	//DataBase del Documento
		
		///
		//db->BeginTransaction();
		
		////////Interface de Utiloidades del Documento
		InterfacePtr<IDocumentUtils> docUtils(GetExecutionContextSession(), IID_IDOCUMENTUTILS);
		if (docUtils == nil)
		{
			ASSERT_FAIL("FilActActionComponent::DoSave: docUtils invalid");
			break;
		}
			
				
		///////Obtiene la Interface ICheckInOutSuite para mandar hablar a sus metodos
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
			break;
			
		
		
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
			
		///////HaceUpdate a la base de datos de las notas que se estaban editando
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
			
		///////////
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			break;
		}	
			
					
					
		////////
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
		(
			kN2PXMPUtilitiesBoss,	// Object boss/class
			IN2PXMPUtilities::kDefaultIID
		)));
				
				
		//Obtiene los datos de registro de usuarios si solo si se encuentra un usuario logeado en la aplicacion
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckInOutDialogController::ValidateDialogFields()");
			break;
		}
		
					
					
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
					
		//////
		UpdateStorys->CheckInNotasEnEdicion(kFalse);	//hace check in de las notas que se encuentren en edicion sobre el documento actual
			
		
			
		IDUsuario = wrkSpcPrefs->GetIDUserLogedString();	//Obtiene el Id_Usuario que se encuentra logeado actuamente
		Aplicacion.Append(wrkSpcPrefs->GetNameApp());			//Obtiene el nombre de la aplicacion en que se encuentra logeado el usuario
		HoraIn.Append(wrkSpcPrefs->GetHoraIn());				//Obtiene la Hora en se logeo el Usuario
		FechaIn.Append(wrkSpcPrefs->GetFechaIn());				//Obtiene la fecha en que se logeo el usuario
		
		
		
		//Datos de la ultima pagina chekineada que se muestran en el dialogo
		horaCrea = GetTextControlData(kHraCreacionWidgetID);			
		fechaCrea = GetTextControlData(kFechaWidgetID);
			
		DirigidoA = GetTextControlData(kComboBoxDirigidoAWidgetID);
		Estatus = GetTextControlData(kComboBoxStatusWidgetID);
		Pagina = GetTextControlData(kPaginaWidgetID);
		
		Date = GetTextControlData(kDatePubliWidgetID);
		
		MySQLDate = InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Date);
		
			
	
		Seccion=GetTextControlData(kComboBoxSeccionWidgetID);
		Issue=GetTextControlData(kComboBoxIssueWidgetID);
		
		PMString NombreSeccion=	Seccion;
		PMString NombrePublicacion=Issue;
		
		//Obtiene el index del nombre de la publicacio y de la seccion para despues buscalos en los vectors correcpondientes
		int32 indexOfIssue = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxIssueWidgetID, Issue);
		if(indexOfIssue>=0 && VectorIdPublicacion.Length()>0 && indexOfIssue< VectorIdPublicacion.Length())
		{
			Issue = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",VectorIdPublicacion[indexOfIssue]);
		}
		
		
		
		
		
		int32 indexOfSeccion = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxSeccionWidgetID, Seccion);
		
		this->LLenar_Combo_Seccion(Seccion);
		this->SeleccionarCadenaEnComboBox(Seccion , kComboBoxSeccionWidgetID);
		
		if(VectorIdSeccion.Length()>0)
		{
			if(indexOfSeccion>=0 && indexOfSeccion<VectorIdSeccion.Length())
			{
				Seccion = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",VectorIdSeccion[indexOfSeccion]);
			}
		}
		
		
			
		PreferencesPara.Estado_To_SavePage = Estatus;
		PreferencesPara.Seccion_To_SavePage = Seccion;
		PreferencesPara.Pagina_To_SavePage = Pagina;
		PreferencesPara.Issue_To_SavePage = Issue;
		PreferencesPara.Date_To_SavePage = Date;
		PreferencesPara.DirididoA_To_SavePage = DirigidoA;
		
		
		PMString N2PFolio_Pagina = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		PMString N2P_Pagina = N2PSQLUtilities::GetXMPVar("N2PPagina", document);
					
		N2PCTUtils->OcultaNotasConFrameOverset();	
		PMString FotoVersionPagina="";
		PMString FotoLastPagina="";
		PMString fecha_Guion="";
					
					
					
		PMString FechaParaRuta = "";//InterlasaUtilities::FormatFechaYHoraManana("%Y%m%d");
		PMString *mm=fechaCrea.GetItem("/",1);
		PMString *dd=fechaCrea.GetItem("/",2);
		PMString *aa=fechaCrea.GetItem("/",3);
		FechaParaRuta=aa->GrabCString();
		FechaParaRuta.Append(mm->GrabCString());
		FechaParaRuta.Append(dd->GrabCString());
					
					
					
		
		
		checkIn->AutomaticSaveRevi(document,kTrue);//Para hacer un save revision pero indicando a la base de datos que se esta depositando la pagina

		fecha_Guion= PreferencesPara.Date_To_SavePage;
		SDKUtilities::Replace(fecha_Guion,"/","-");
		SDKUtilities::Replace(fecha_Guion,"/","-");
	
		//Arma la Ruta para el Documento InDesign
		File= PrefConections.PathOfServerFile;
		SDKUtilities::AppendPathSeparator(File);
		File.Append("apaginas");
		SDKUtilities::AppendPathSeparator(File);
		File.Append("N2PSQL");
		SDKUtilities::AppendPathSeparator(File);
		File.Append(FechaParaRuta);
		SDKUtilities::AppendPathSeparator(File);
		File.Append( NombrePublicacion);
		SDKUtilities::AppendPathSeparator(File);
		File.Append( NombreSeccion );
		SDKUtilities::AppendPathSeparator(File);
		File.Append( IDUsuario);
		SDKUtilities::AppendPathSeparator(File);
		File.Append( PreferencesPara.Pagina_To_SavePage );
		File.Append(".indd");
						
		//Arma la ruta para os ducumento de Versiones y fotos o previews
				
		FotoVersionPagina=PrefConections.PathOfServerFile ;
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);
		FotoVersionPagina.Append("apaginas");
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);
		FotoVersionPagina.Append("N2PSQL");
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);
		FotoVersionPagina.Append(FechaParaRuta);
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);
		FotoVersionPagina.Append(NombrePublicacion); 
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);
		FotoVersionPagina.Append(NombreSeccion);
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);
		FotoVersionPagina.Append(IDUsuario);
		SDKUtilities::AppendPathSeparator(FotoVersionPagina);

		FotoLastPagina=PrefConections.PathOfServerFile;
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
		FotoLastPagina.Append("apaginas");
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
		FotoLastPagina.Append("N2PSQL");
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
        FotoLastPagina.Append(FechaParaRuta );
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
		FotoLastPagina.Append(NombrePublicacion );
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
		FotoLastPagina.Append(NombreSeccion );
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
 		FotoLastPagina.Append(IDUsuario);
		SDKUtilities::AppendPathSeparator(FotoLastPagina);
		
		FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
		FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
		checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
		//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
						
		//N2PSQLUtilities::N2PExportPDFPages(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage);
		//Obtiene Folio del XMP si es que existe
			
		CadenaSinDosRutas=FotoVersionPagina;
						
		//Remueve todo hasta a paginas
		if(CadenaSinDosRutas.NumUTF16TextChars()>0)
		{
			int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
			if(indexapaginas>0)
					CadenaSinDosRutas.Remove(0, indexapaginas+9);
		}
						
		CopiaToLast=FotoLastPagina;
		CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
		CopiaToLast.Append("_last.jpg");
		fechaCrea.Append(" " + horaCrea);
					
		//Quiere Decir que es el Primer CheckIn
					
		ID_Evento="8";
						
		//Si Es el Primer CheckIn
				
		//Arma Folio
		N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
		N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
					
				
					
			
			
		PMString SPCallPaginnaChickIn="CALL PaginaCheckInStoreProc(";
		SPCallPaginnaChickIn.Append(ID_Pagina);
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(PreferencesPara.Issue_To_SavePage);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(MySQLDate);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Seccion_To_SavePage);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(N2PFolio_Pagina);
		SPCallPaginnaChickIn.Append("',");
		SPCallPaginnaChickIn.Append("1");	//PLIEGO
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("0");	//PAR/IMPAR
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("1");	//ID-Color
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(PreferencesPara.DirididoA_To_SavePage);	
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(IDUsuario);	//Proveniente_de
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("SQLServer");	//Servidor
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(File);	//Ruta_Elemento 
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Pagina_To_SavePage);	//Nombre_Archivo 
		SPCallPaginnaChickIn.Append("',");
		SPCallPaginnaChickIn.Append("@A");	//Fecha_Creacion
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("@X");	//Fecha_Ult_Mod
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("0");	//Webable
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("10");	//Calificacion
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append("Pagina");	//Id_TipoElemento
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Estado_To_SavePage);	//Id_Estatus
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("No se que es Cameo");	//Cameo
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(IDUsuario);	//Id_Empleado
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("0");	//Fech_Evento
		SPCallPaginnaChickIn.Append("',");
		
		SPCallPaginnaChickIn.Append(ID_Evento);	//Id_Evento
		
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(CadenaSinDosRutas);	//Ruta_Previo
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("Check In");	//Descripcion_Evento
		SPCallPaginnaChickIn.Append("','");
			
		SPCallPaginnaChickIn.Append(CopiaToLast);	//Descripcion_Evento
		SPCallPaginnaChickIn.Append("');");
			
		
		//CAlert::InformationAlert(StringConection);	
		//CAlert::InformationAlert(SPCallPaginnaChickIn);	
			
		UpdateStorys->GuardarQuery(SPCallPaginnaChickIn);
					
		K2Vector<PMString> QueryVector;	
				
		SQLInterface->SQLQueryDataBase(StringConection,SPCallPaginnaChickIn,QueryVector);
					
		if(QueryVector.Length()<0)
		{
			CAlert::InformationAlert("Hubo un error y se va a salir del ciclo");
			break;
		}
		else
		{
			PMString cadena = SQLInterface->ReturnItemContentbyNameColumn("Fecha_CreacionIN",QueryVector[0]);
			PMString Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[0]);
			ID_Pagina = SQLInterface->ReturnItemContentbyNameColumn("Pagina_IDX",QueryVector[0]);
			//CAlert::InformationAlert(ID_Pagina);
			if(Resultado=="9")
			{
				CAlert::InformationAlert(kN2PsqlYaExistePaginaConEsteNombreStringKey);	
				break;
			}
			else
			{	if(cadena=="SinPrivilegios" || cadena.NumUTF16TextChars()<=0)
				{
					CAlert::InformationAlert(kN2PsqlSinPermisosToCheckinPageStringKey);		
					break;
				}
						
			}
						
		}
					
		//CAlert::InformationAlert("8");	
		
		if(PreferencesPara.Date_To_SavePage!=XMPUtils->GetXMPVar("N2PDate"))
		{
			XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);
		}


		if( ID_Evento=="8"|| ID_Evento=="31")
		{
			//XMPUtils->CreatePropertiesN2P(document);
			XMPUtils->SaveXMPVar("N2PPublicacion",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PAplicacion",NomAplicacionActual);
			XMPUtils->SaveXMPVar("N2PEstatus",PreferencesPara.Estado_To_SavePage);
			XMPUtils->SaveXMPVar("N2PDirigidoA",PreferencesPara.DirididoA_To_SavePage);
			XMPUtils->SaveXMPVar("N2PSeccion",PreferencesPara.Seccion_To_SavePage);
			XMPUtils->SaveXMPVar("N2PPagina",PreferencesPara.Pagina_To_SavePage);
			XMPUtils->SaveXMPVar("N2PIssue",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PFechaCreacionPath",FechaParaRuta);
			XMPUtils->SaveXMPVar("N2PUsuarioCredor",IDUsuario);
			XMPUtils->SaveXMPVar("N2PNPubliCreacionPath",NombrePublicacion);
	 		XMPUtils->SaveXMPVar("N2PNSecCreacionPath",NombreSeccion);
			//Poner la fecha actual
			if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()==0)
				XMPUtils->SaveXMPVar("N2PDate","00/00/0000");
			else
				XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);
				XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
				XMPUtils->SaveXMPVar("N2PSQLIDPagina",ID_Pagina);
				XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
		}
					
		if(NomAplicacionActual.Contains("InDesign"))//si nos encontramos en InDesign
		{
			sysFile=FileUtils::PMStringToSysFile(File);
			if(FileUtils::DoesFileExist(sysFile))
			{
				Utils<IDocumentUtils>()->DoSave(document); 
			}
			else
			{
					
				SDKLayoutHelper helperLayout;
				if(helperLayout.SaveDocumentAs(docUIDRef,sysFile,kFullUI)!=kSuccess)
				{
					break;
				}
			}
							
		}		
					


		if(ID_Evento=="31")
		{	
				
			remove(InterlasaUtilities::MacToUnix(FileToRemove).GrabCString());
			//CAlert::InformationAlert(FileToRemove);
		}
		
					
				
			
		//Obtiene preferencias de Check in en documento de preferencias"ParametrosDeDialogosN2PSQL.pfd"
		checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
			
		PreferencesPara.Estado_To_SavePage=Estatus;
		PreferencesPara.Seccion_To_SavePage=Seccion;
		PreferencesPara.Pagina_To_SavePage=Pagina;
		PreferencesPara.Issue_To_SavePage=Issue;
		PreferencesPara.Date_To_SavePage=Date;
		PreferencesPara.DirididoA_To_SavePage=DirigidoA;
		//Guarda preferencias de Check in en documento de preferencias "ParametrosDeDialogosN2PSQL.pfd"
		checkIn->SetPreferencesParametros(&PreferencesPara);
					
					
		
		if(Mantener_Notas==3)	
		{
			UpdateStorys->DesligarNotasYFotosDPagina(document);
		}
  		
					
					
					
					
		errorValue=kSuccess;
		
	}while(false);
	
	if(errorValue==kFailure)
	{
		CmdUtils::RollBackCommandSequence(seq);
		CmdUtils::AbortCommandSequence(seq);
	}
	else
	{
		CmdUtils::EndCommandSequence(seq);
	}
	seq=nil;
	return errorValue;
}


