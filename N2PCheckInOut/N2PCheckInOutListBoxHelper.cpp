/*
//	$File: //depot/devtech/sdkanna/utilities/SDKListBoxHelper.cpp $
// 
//	Owner:	Ian Paterson
//
//	$Author: ipaterso $
//
//	$DateTime: 2002/10/16 08:12:04 $
//
//	$Revision: #6 $
//
//	$Change: 163058 $
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2002 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
//	Purpose:
//
*/
#include "VCPlugInHeaders.h"
// Interface includes

#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
// implem includes
#include "PersistUtils.h" // GetDatabase
//#include "PalettePanelUtils.h"
#include "CAlert.h"


#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "N2PCheckInOutID.h"
#include "N2PCheckInOutListBoxHelper.h"

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown * owner, int32 pluginId, WidgetID widget) : fOwner(owner), fOwnerPluginID(pluginId), fwidget(widget)
{

}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
	fwidget=nil;
}



IControlView * SDKListBoxHelper ::FindCurrentListBox()
{
	if(!verifyState())
	{
		CAlert::ErrorAlert("13");
		return nil;
	}

	IControlView * listBoxControlView = nil;
	do {
	
		/*WidgetID listBoxID = PalettePanelUtils::GetCurrentListBoxID(fOwnerPluginID);
		if(listBoxID== 0) 
		{
			break;
		}*/

		WidgetID listBoxID = fwidget;


		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		ASSERT_MSG(iPanelControlData != nil, "SDKListBoxHelper ::FindCurrentListBox() iPanelControlData nil");
		if(iPanelControlData == nil) 
		{
			break;
		}
		listBoxControlView = 	iPanelControlData->FindWidget(listBoxID);
		ASSERT_MSG(listBoxControlView != nil, "SDKListBoxHelper ::FindCurrentListBox() no listbox");
		if(listBoxControlView == nil) 
		{
			break;
		}
	
	} while(0);

	return listBoxControlView;
}

 

void SDKListBoxHelper::AddElement( const PMString & displayName, WidgetID updateWidgetId, int atIndex)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		// Create an instance of a list element
		InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
		if(listAttr == nil) {
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
				return;
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) {
			break;
		}
		this->addListElementWidget(newElView, displayName, updateWidgetId, atIndex);
	}
	while (false);			// false loop
}


void SDKListBoxHelper::RemoveElementAt(int indexRemove)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) {
			// Don't remove outside of list data bounds
			break;
		}
		listControlData->Remove(indexRemove);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);			// false loop
}



void SDKListBoxHelper::RemoveLastElement()
{
	if(!verifyState())
		return;

	do
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) {		
			listControlData->Remove(lastIndex);
			removeCellWidget(listBox, lastIndex);
		}
		
	}
	while (false);
}



int SDKListBoxHelper::GetElementCount() 
{
	int retval=0;
	do {
	
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {

		if(listBox==nil) break;
		// recall that when the element is added, it is added as a child of the cell-panel
		// widget. Therefore, navigate to the cell panel and remove the child at the specified
		// index. Simultaneously, remove the corresponding element from the list controldata.
		// +
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) {
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
		// -

	} while(0);

}


void SDKListBoxHelper::addListElementWidget(InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex)
{
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) {
		return;
	}

	do {
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) {
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if ( (nameTextView == nil)  ) {
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) {
			break;
		}	
		newEltext->SetString(displayName, kTrue, kTrue);
		
		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) {
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) {
			break;
		}

		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "SDKListBoxHelper::addListElementWidget() listData nil");
		if(listData == nil) { 
			break;
		}
		listData->Add(elView, atIndex);
	} while(0);

}


void SDKListBoxHelper::EmptyCurrentListBox()
{
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox();
		if(listBoxControlView == nil) {
			break;
		}
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) {
			break;
		}
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) {
			break;
		}
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}


void SDKListBoxHelper::AddElementListPag( const PMString & PagName,const PMString & SeccionName,const PMString & EstadoName, const PMString& Folio_Pagina, const PMString& Id_Publicacion, const PMString& Id_Seccion,WidgetID updateWidgetId, int atIndex)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) 
		{
			CAlert::ErrorAlert("1");
			break;
		}
		// Create an instance of a list element
		InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
		if(listAttr == nil) 
		{
			CAlert::ErrorAlert("2");
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
				return;
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) 
		{	
			CAlert::ErrorAlert("3");
			break;
		}
		this->addListPagElementWidget(newElView, PagName,SeccionName,EstadoName,Folio_Pagina,Id_Publicacion,Id_Seccion, updateWidgetId, 0);
	}
	while (false);			// false loop
}

void SDKListBoxHelper::addListPagElementWidget(InterfacePtr<IControlView> & elView, const PMString & PagName,const PMString & SeccionName,const PMString & EstadoName,const PMString & Folio_Pagina, const PMString& Id_Publicacion, const PMString& Id_Seccion,WidgetID updateWidgetId, int atIndex)
{
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) 
	{
		//CAlert::ErrorAlert("4");
		return;
	}

	do {
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) 
		{
			//CAlert::ErrorAlert("5");
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(kPaginaLabelWidgetID);
		if ( (nameTextView == nil)  ) 
		{
			//CAlert::ErrorAlert("6");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) 
		{
			//CAlert::ErrorAlert("7");
			break;
		}	
		newEltext->SetString(PagName, kTrue, kTrue);

	/*****************************/
		IControlView* nameSeccion = newElPanelData->FindWidget(kSeccionLabelWidgetID);
		if ( (nameSeccion == nil)  ) 
		{
			//CAlert::ErrorAlert("8");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newSeccion (nameSeccion,UseDefaultIID());
		if (newSeccion == nil) 
		{
			//CAlert::ErrorAlert("9");
			break;
		}	
		newSeccion->SetString(SeccionName, kTrue, kTrue);

	/**********************/
		IControlView* nameEstado = newElPanelData->FindWidget(kEstadoLabelWidgetID);
		if ( (nameSeccion == nil)  ) 
		{
			//CAlert::ErrorAlert("10");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEstado (nameEstado,UseDefaultIID());
		if (newEstado == nil) 
		{
			//CAlert::ErrorAlert("11");
			break;
		}	
		newEstado->SetString(EstadoName, kTrue, kTrue);
		

	
	/**********************/
		IControlView* nameFolio_Pag = newElPanelData->FindWidget(kFolio_PaginaLabelWidgetID);
		if ( (nameFolio_Pag == nil)  ) 
		{
			//CAlert::ErrorAlert("10");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newFolio_Pag (nameFolio_Pag,UseDefaultIID());
		if (newFolio_Pag == nil) 
		{
			//CAlert::ErrorAlert("11");
			break;
		}	
		newFolio_Pag->SetString(Folio_Pagina, kTrue, kTrue);
		
	/********Id_Publicacion**************/
		IControlView* Id_PublicacionView = newElPanelData->FindWidget(kLabelId_PublicacionLabelWidgetID);
		if ( (Id_PublicacionView == nil)  ) 
		{
			//CAlert::ErrorAlert("10");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newId_Publicacion (Id_PublicacionView,UseDefaultIID());
		if (newId_Publicacion == nil) 
		{
			//CAlert::ErrorAlert("11");
			break;
		}	
		newId_Publicacion->SetString(Id_Publicacion, kTrue, kTrue);
		
	/********Id_Publicacion**************/
		IControlView* Id_SeccionView = newElPanelData->FindWidget(kLabelId_SeccionLabelWidgetID);
		if ( (Id_SeccionView == nil)  ) 
		{
			//CAlert::ErrorAlert("10");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newId_Seccion (Id_SeccionView,UseDefaultIID());
		if (newId_Seccion == nil) 
		{
			//CAlert::ErrorAlert("11");
			break;
		}	
		newId_Seccion->SetString(Id_Seccion, kTrue, kTrue);
		
		
		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
		{
			//CAlert::ErrorAlert("12");
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			//CAlert::ErrorAlert("13");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
		{
			///CAlert::ErrorAlert("14");
			break;
		}

		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "SDKListBoxHelper::addListElementWidget() listData nil");
		if(listData == nil) 
		{
			///CAlert::ErrorAlert("15");
			break;
		}
		listData->Add(elView, atIndex);
	} while(0);

}

/**
	FUNCION QUE OBTIENE EL TEXTO DEL ACMPO SELECCIONADO SOBRE LA LISTA CONCURRENTE
*/
void SDKListBoxHelper::ObtenerTextoDeSeleccionActual( WidgetID widget1,PMString &Text1,WidgetID widget2,PMString &Text2,int32 indexSelected)
{
	
	do{

		IControlView * listBox = this->FindCurrentListBox();//obtiene la lista 
		if(listBox == nil) {
			break;
		}
			

		if(listBox==nil) break;
		
		//obtiene el panel control de la lista
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		//obtiene el control de vista de las celdas
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		//obtiene el control de datos del panel de las celdas
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		//verifica el numero de la celda celeccionada sea menor o igual a la cantidad de celdas
		//o tama�o de la lista y que se mayor a 0
		if(indexSelected < 0 || indexSelected >= cellPanelData->Length()) {
			break;
		}
		
		//busca y obtiene el control de la vista de la celda seleccionada
		IControlView* CellCView = cellPanelData->GetWidget(indexSelected);
		ASSERT_MSG(CellCView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(CellCView == nil) 
		{
			break;
		}


		//obtiene el control de datos del panel de la celda seleccionada
		InterfacePtr<IPanelControlData>	PanelCCell( CellCView, UseDefaultIID());
		if(PanelCCell==nil)
		{
			break;
		}

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView = PanelCCell->FindWidget(widget1);
		ASSERT_MSG(TextWigetCView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView == nil) 
		{
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto( TextWigetCView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			break;
		}

		//Extraigo el texto del Widget
		Text1=ControlDETexto->GetString();

		//busca el ca ja de texto sobre la celda seleccionada
		IControlView* TextWigetCView2 = PanelCCell->FindWidget(widget2);
		ASSERT_MSG(TextWigetCView2 != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(TextWigetCView2 == nil) 
		{
			break;
		}

		//obtiene le control de datos de texto de la celda celeccionada
		InterfacePtr<ITextControlData>	ControlDETexto2( TextWigetCView2, UseDefaultIID());
		if(ControlDETexto2==nil)
		{
			break;
		}

		Text2=ControlDETexto2->GetString();
	}while (false);			// false loop
}