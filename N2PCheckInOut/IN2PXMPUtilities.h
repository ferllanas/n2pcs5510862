/*
//	File:	IN2PXMPUtilities.h
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#ifndef _IN2PXMPUtilities_
#define _IN2PXMPUtilities_

#include "IPMUnknown.h"
#include "N2PCheckInOutID.h"

class IN2PXMPUtilities: public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IN2PXMPUtilities };

	virtual bool16 SaveXMPVar(PMString Variable,PMString valor)=0;
	virtual PMString GetXMPVar(PMString Variable)=0;
};
#endif // _IN2PXMPUtilities_