/*
//
//	File:	InterlasaRegEditUtilities.h
//
//
//	Date:	6-Mar-20010
//  
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#ifndef __N2PRegisterUsers_h__
#define __N2PRegisterUsers_h__

#include"IPMUnknown.h"
#include"N2PInOutID.h"


/**
	Esta es la interface del plug in para obtener las preferencias ode los usuarios
*/
class IN2PRegisterUsers:public IPMUnknown
{
public:

	enum { kDefaultIID = IID_IN2PREGISTERUSERS };
	
	/** 
			Set the preference value.
			@param newVal the new preference value.
	*/
	virtual void	SetUserValues(const bool16& newVal,const int32& UID_Usuario, 
								const PMString& NameApp, const PMString& FechaIn, 
								const PMString& HoraInUsr,
								const PMString& Estado_To_PageIn,
								const PMString& SeccionNota,
								const PMString& PaginaNota,
								const PMString& IssueNota,
								const PMString& DateNota,
								const PMString& DirididoANota,
								const PMString& IDUserLogedString,
								const PMString& StatusNotaPageOut,
								const PMString& SeccionNotaPageOut,
								const PMString& PaginaNotaPageOut,
								const PMString& IssueNotaPageOut,
								const PMString& DateNotaPageOut,
								const PMString& DirididoANotaPageOut) = 0;
	
		/**
			Get the preference value.
		*/
		virtual const	bool16& GetValue() = 0;
		
		virtual const	int32& GetID_Usuario() = 0;
		
		virtual const	PMString& GetIDUserLogedString() = 0;
		
		virtual const	PMString& GetNameApp() = 0;
		
		virtual const	PMString& GetFechaIn() = 0;
		
		virtual const	PMString& GetHoraIn() = 0;
		
		virtual const	PMString& GetEstatusNota() = 0;
		virtual const	PMString& GetSeccionNota() = 0;
		virtual const	PMString& GetPaginaNota() = 0;
		virtual const	PMString& GetIssueNota() = 0;
		virtual const	PMString& GetDateNota() = 0;
		virtual const	PMString& GetDirigidoANota() = 0;
		
		virtual const	PMString& GetEstatusNotaPageOut() = 0;
		virtual const	PMString& GetSeccionNotaPageOut() = 0;
		virtual const	PMString& GetPaginaNotaPageOut() = 0;
		virtual const	PMString& GetIssueNotaPageOut() = 0;
		virtual const	PMString& GetDateNotaPageOut() = 0;
		virtual const	PMString& GetDirigidoANotaPageOut() = 0;
		
		
};


#endif // __N2PRegisterUsers_h__

