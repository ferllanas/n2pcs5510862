/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "IPMStream.h"
#include "N2PRegisterUsers.h"
#include "N2PInOutID.h"
#include "CAlert.h"


/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class N2PRegisterUsers : public CPMUnknown<IN2PRegisterUsers>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	N2PRegisterUsers (IPMUnknown *boss);
	
	virtual ~N2PRegisterUsers() {}

	/**
			Set the preference values.
			
	*/
	
	virtual void	SetUserValues(const bool16& newVal,const int32& UID_Usuario,
						 		const PMString& NameApp, const PMString& FechaIn,
						 		const PMString& HoraInUsr,
						 		const PMString& Estado_To_PageIn,
								const PMString& SeccionNota,
								const PMString& PaginaNota,
								const PMString& IssueNota,
								const PMString& DateNota,
								const PMString& DirigidoANota,
								const PMString& IDUserLogedString,
								const PMString& StatusNotaPageOut,
								const PMString& SeccionNotaPageOut,
								const PMString& PaginaNotaPageOut,
								const PMString& IssueNotaPageOut,
								const PMString& DateNotaPageOut,
								const PMString& DirididoANotaPageOut);
	
		/**
			Get the preference value.
		*/
		virtual const bool16& GetValue();
		
		virtual const int32& GetID_Usuario();
		
		virtual const PMString& GetIDUserLogedString();
		
		virtual const PMString& GetNameApp();
		
		virtual const PMString& GetFechaIn();
		
		virtual const PMString& GetHoraIn();
		
		virtual const	PMString& GetEstatusNota();
		virtual const	PMString& GetSeccionNota();
		virtual const	PMString& GetPaginaNota();
		virtual const	PMString& GetIssueNota();
		virtual const	PMString& GetDateNota();
		virtual const	PMString& GetDirigidoANota();

		virtual const	PMString& GetEstatusNotaPageOut();
		virtual const	PMString& GetSeccionNotaPageOut();
		virtual const	PMString& GetPaginaNotaPageOut();
		virtual const	PMString& GetIssueNotaPageOut();
		virtual const	PMString& GetDateNotaPageOut();
		virtual const	PMString& GetDirigidoANotaPageOut();
		/** 
			Writes the preference value to a stream.
			@param s the persistent in and out stream.
			@param prop the implementation ID 			
		*/
		virtual void	ReadWrite(IPMStream* s, ImplementationID prop);
		
	private:
	
	bool16	value;
	int32 	UserID;
	PMString NameApp;
	PMString FechaIn;
	PMString HoraIn;
	PMString IDUserLogedString;
	
	PMString Estado_To_PageIn;
	PMString SeccionNota;
	PMString PaginaNota;
	PMString IssueNota;
	PMString DateNota;
	PMString DirigidoANota;
	
	PMString StatusNotaPageOut;
	PMString SeccionNotaPageOut;
	PMString PaginaNotaPageOut;
	PMString IssueNotaPageOut;
	PMString DateNotaPageOut;
	PMString DirigidoANotaPageOut;
	
	
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PERSIST_PMINTERFACE(N2PRegisterUsers, kN2PRegisterUsersImpl)


/* HelloWorld Constructor
*/
N2PRegisterUsers::N2PRegisterUsers(IPMUnknown* boss) 
: CPMUnknown<IN2PRegisterUsers>(boss)
{
	// do nothing.

	/* NOTE: 
		There used to be code that would set the preference value by default.
		That was removed in favor of the workspace responders.
		
		The responders get called, and they will try to setup the value
		for this custom preference.  It is one of the responders that will
		set the correct initial value for the cached preference, fPrefState.
		
		If the global workspace is already created from a previous 
		application session, the ReadWrite() method will set the 
		cached preference, fPrefState. The same ReadWrite() will be called when 
		an existing document is opened.
		
		Refer to CstPrfNewWSResponder.cpp and N2PRegisterUsers.cpp
		for details on how to set the workspace defaults. 
	*/
}


void N2PRegisterUsers::SetUserValues(const bool16& newVal,const int32& UID_Usuario, 
								const PMString& Applicacion, const PMString& FechaInUsr,
								const PMString& HoraInUsr, 
								const PMString& EstatusN,
								const PMString& SeccionN,
								const PMString& PaginaN,
								const PMString& IssueN,
								const PMString& DateN,
								const PMString& DirigidoAN,
								const PMString& IDUserLogedStringN,
								const PMString& EstatusNotaPageOutN,
								const PMString& SeccionNotaPageOutN,
								const PMString& PaginaNotaPageOutN,
								const PMString& IssueNotaPageOutN,
								const PMString& DateNotaPageOutN,
								const PMString& DirididoANotaPageOutN)
{
		value = newVal; 
		UserID = UID_Usuario;
		IDUserLogedString =	IDUserLogedStringN;
		NameApp = Applicacion;
		FechaIn = FechaInUsr;
		HoraIn  =  HoraInUsr;
		
		Estado_To_PageIn=EstatusN;
		SeccionNota=SeccionN;
		PaginaNota=PaginaN;
		IssueNota=IssueN;
		DateNota=DateN;
		DirigidoANota=DirigidoAN;
		
		
		StatusNotaPageOut = EstatusNotaPageOutN;
		SeccionNotaPageOut = SeccionNotaPageOutN;
		PaginaNotaPageOut = PaginaNotaPageOutN;
		IssueNotaPageOut = IssueNotaPageOutN;
		DateNotaPageOut = DateNotaPageOutN;
		DirigidoANotaPageOut = DirididoANotaPageOutN;
		PreDirty();
		
		
}
	
const bool16& N2PRegisterUsers::GetValue()
{
	return value;
}
		
const int32& N2PRegisterUsers::GetID_Usuario()
{
	return UserID;
	
}

const	PMString& N2PRegisterUsers::GetIDUserLogedString() 
{
	
	return IDUserLogedString;
	
}

const PMString& N2PRegisterUsers::GetNameApp()
{
	
	return NameApp;
	
}
		
const PMString& N2PRegisterUsers::GetFechaIn()
{
	return(FechaIn) ;
}


const PMString& N2PRegisterUsers::GetHoraIn()
{
	return(HoraIn);
}

const	PMString& N2PRegisterUsers::GetEstatusNota()
{
	return(Estado_To_PageIn);
}
const	PMString& N2PRegisterUsers::GetSeccionNota()
{
	return(SeccionNota);
}
const	PMString& N2PRegisterUsers::GetPaginaNota()
{
	return(PaginaNota);
}
const	PMString& N2PRegisterUsers::GetIssueNota()
{
	return(IssueNota);
}
const	PMString& N2PRegisterUsers::GetDateNota()
{
	return(DateNota);
}

const	PMString& N2PRegisterUsers::GetDirigidoANota()
{
	return(DirigidoANota);
}


const	PMString& N2PRegisterUsers::GetEstatusNotaPageOut()
{
	return(StatusNotaPageOut);
}
const	PMString& N2PRegisterUsers::GetSeccionNotaPageOut()
{
	return(SeccionNotaPageOut);
}
const	PMString& N2PRegisterUsers::GetPaginaNotaPageOut()
{
	return(PaginaNotaPageOut);
}
const	PMString& N2PRegisterUsers::GetIssueNotaPageOut()
{
	return(IssueNotaPageOut);
}
const	PMString& N2PRegisterUsers::GetDateNotaPageOut()
{
	return(DateNotaPageOut);
}

const	PMString& N2PRegisterUsers::GetDirigidoANotaPageOut()
{
	return(DirigidoANotaPageOut);
}


/** 
	Writes the preference value to a stream.
	@param s the persistent in and out stream.
	@param prop the implementation ID 			
*/
void	N2PRegisterUsers::ReadWrite(IPMStream* s, ImplementationID prop)
{
	s->XferBool(value);
	s->XferInt32(UserID);
	
	PMString Datos="News2Page Preferences\n";
	Datos.Append("IDUser:" + NameApp + "\n");
	Datos.Append("FechaIn:" + FechaIn + "\n");
	Datos.Append("HoraIn:" + HoraIn + "\n");
	Datos.Append("IDUserLogedString:" + IDUserLogedString + "\n");
	
	
	Datos.Append("Estado_To_PageIn:" + Estado_To_PageIn + "\n");
	Datos.Append("SeccionNota:" + SeccionNota + "\n");
	Datos.Append("PaginaNota:" + PaginaNota + "\n");
	Datos.Append("IssueNota:" + IssueNota + "\n");
	Datos.Append("DateNota:" + DateNota + "\n");
	Datos.Append("DirigidoA:" + DirigidoANota + "\n");
	
	Datos.Append("StatusNotaPageOut:" + StatusNotaPageOut + "\n");
	Datos.Append("SeccionNotaPageOut:" + SeccionNotaPageOut + "\n");
	Datos.Append("PaginaNotaPageOut:" + PaginaNotaPageOut + "\n");
	Datos.Append("IssueNotaPageOut:" + IssueNotaPageOut + "\n");
	Datos.Append("DateNotaPageOut:" + DateNotaPageOut + "\n");
	Datos.Append("DirigidoAPageOut:" + DirigidoANotaPageOut + "\n");
	
	s->XferByte((uchar *)Datos.GrabCString(), Datos.ByteLength());
}
		