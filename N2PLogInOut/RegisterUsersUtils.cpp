//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customprefs/RegisterUsersUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IBoolData.h"
#include "IIntData.h"
#include "ICommand.h"
#include "IDocument.h"
#include "IWorkspace.h"
#include "N2PRegisterUsers.h"
#include "UIDList.h"
#include "IRegisterUsersUtils.h"
#include "IStringData.h"

// Implementation includes:
#include "CmdUtils.h"
#include "CPMUnknown.h"

// Plug-in includes
#include "N2PInOutID.h"

/** RegisterUsersUtils
	Methods to provide acquiring and setting the preferences for
	the dialog controller and the script provider.
 
	@ingroup customprefs
	@author John Hake
*/
class RegisterUsersUtils : public CPMUnknown<IRegisterUsersUtils>
{
	public:
		/** Constructor
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RegisterUsersUtils(IPMUnknown* boss);

		/** Destructor
		*/
		~RegisterUsersUtils() {}

		/** CreateModPrefsCmd
			This utility function creates a command to change the custom
			preference values for the requested preference interface.
			@param prefIFace A pointer to the preference interface to be changed.  (App wrkspc or docwrkspc)
			@param prefVal The new preference value to be set.
			@return A command with it's data interface set. The client is 
			responsible for releasing this command object.
		*/
		virtual ICommand* CreateModPrefsCmd(IN2PRegisterUsers* prefIFace, bool16 prefVal,int32 IDUser,PMString Aplicacion);

		/** QueryCustomPrefs
			This utility function queries for IN2PRegisterUsers and returns
			a pointer to that interface.
			@param iFace Could be a pointer to any boss that aggregates
			IID_ICUSTOMPREFS. In this plugin, it will be eith kSessionBoss,
			or kDocBoss or even nil.
			@return A pointer to IN2PRegisterUsers. The client is responsible for 
			releasing this pointer.
		*/
		virtual IN2PRegisterUsers* QueryCustomPrefs(IPMUnknown* iFace);

};


/*	CREATE_PMINTERFACE
	This macro creates a class factory for the given class name
	and registers the ID with the host.
*/
CREATE_PMINTERFACE(RegisterUsersUtils, kRegisterUsersUtilsImpl)

/* Constructor
*/
RegisterUsersUtils::RegisterUsersUtils(IPMUnknown* boss):
			CPMUnknown<IRegisterUsersUtils>(boss) 
{
}



/*	QueryCustomPrefs
*/
IN2PRegisterUsers* RegisterUsersUtils::QueryCustomPrefs(IPMUnknown* iFace)
{
	// Initialize the return value
	IN2PRegisterUsers* prefsIFace = nil;

	// Convert nil input to session preferences request
	if (iFace == nil) 
	{
		iFace = GetExecutionContextSession();
	}

	do
	{

		// Try the obvious
		prefsIFace = (IN2PRegisterUsers*) iFace->QueryInterface(IN2PRegisterUsers::kDefaultIID);
		if (prefsIFace != nil)
		{
			break;
		}
		
		// Is iFace requesting Session preferences?
		if (iFace == GetExecutionContextSession())
		{
			// Get the session simple prefs
			InterfacePtr<IWorkspace> wrkSpc(GetExecutionContextSession()->QueryWorkspace());
			if(wrkSpc==nil)
				break;
	
			prefsIFace = (IN2PRegisterUsers*)wrkSpc->QueryInterface(IN2PRegisterUsers::kDefaultIID);
	
			break;
		}

		// Check for document
		InterfacePtr<IDocument> test4Doc(iFace, UseDefaultIID());
		if (test4Doc == nil)
		{
			ASSERT_FAIL("Invalid iFace input in RegisterUsersUtils::QueryCustomPrefs()");
			break;
		}
		UIDRef docWkSpc = test4Doc->GetDocWorkSpace();
		prefsIFace = (IN2PRegisterUsers*)docWkSpc.GetDataBase()->Instantiate(docWkSpc.GetUID(), IN2PRegisterUsers::kDefaultIID);

	}
	while (false);

	return prefsIFace;
}

/* CreateModPrefsCmd
*/
ICommand* RegisterUsersUtils::CreateModPrefsCmd(IN2PRegisterUsers* prefIFace, bool16 prefVal,int32 IDUser,PMString Aplicacion)
{

	ICommand* modPrefsCmd = nil;
	bool16 cmdComplete = kFalse;

	do
	{

		// Get the UIDRef for this persistent preferences interface
		UIDRef prefUIDRef = ::GetUIDRef(prefIFace);
		if (prefUIDRef.ExistsInDB() == kFalse)
		{	
			ASSERT_FAIL("Invalid prefs UIDRef in RegisterUsersUtils::CreateModPrefsCmd()");
			break;
		}

		// Create a command to change the preferences
		modPrefsCmd = CmdUtils::CreateCommand(kModRegisterUsersCmdBoss);
		if (modPrefsCmd == nil)
		{	
			ASSERT_FAIL("Can't create command in RegisterUsersUtils::CreateModPrefsCmd()");
			break;
		}

		//Point the command at the object to be modified
		UIDList itemList(prefUIDRef);
		modPrefsCmd->SetItemList(itemList);

		// Get a pointer to the command's (boolean) data interface
		InterfacePtr<IBoolData> cmdBoolData (modPrefsCmd, UseDefaultIID());
		if (cmdBoolData == nil)
		{
			ASSERT_FAIL("Couldn't get cmd IBOOLDATA interface in RegisterUsersUtils::CreateModPrefsCmd()");
			break;
		}
		// Set the preference data interface for the command
		cmdBoolData->Set(prefVal);
		
		InterfacePtr<IIntData> cmdIntData(modPrefsCmd, UseDefaultIID());
		if (cmdIntData == nil)
		{
			ASSERT_FAIL("Couldn't get cmd IIntData interface in RegisterUsersUtils::CreateModPrefsCmd()");
			break;
		}

		cmdIntData->Set(IDUser);
		
		InterfacePtr<IStringData> cmdStringData(modPrefsCmd, UseDefaultIID());
		if (cmdStringData == nil)
		{
			ASSERT_FAIL("Couldn't get cmd IStringData interface in RegisterUsersUtils::CreateModPrefsCmd()");
			break;
		}

		cmdStringData->Set(Aplicacion);
		
		// Signal the command is complete
		cmdComplete = kTrue;

	}
	while(false);

	// Return the command
	if (cmdComplete == kTrue) 
	{
		// Ok, return the actual command
		return modPrefsCmd;
	}
	else
	{
		// Failed to build command, release and return nil
		if (modPrefsCmd != nil)
		{
			modPrefsCmd->Release();
		}
		return nil;
	}

}

// End, RegisterUsersUtils.cpp.
