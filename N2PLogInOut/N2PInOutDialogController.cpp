//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

#include "IApplication.h"
#include "IActiveContext.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IPanelControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IStringData.h"
#include "IWorkspace.h"
#include "ICommand.h"
#include "UIDList.h"
#include "InCopySharedID.h"
#include "IIdleTask.h"
#include "IUserInfo.h"
#include "ILayoutUIUtils.h"

// General includes:
#include "CDialogController.h"
#include "CAlert.h"
#include "Utils.h"

// Project includes:
#include "N2PInOutID.h"
#include "N2PsqlLogInLogOutUtilities.h"

#ifdef MACINTOSH
	
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "IN2PSQLUtils.h"



	#include "N2PwsdlcltID.h"
	#include "N2PWSDLCltUtils.h"
#endif

#ifdef WINDOWS
	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"

	#include "..\InterlasaUltraProKeys\InterUltraProKyID.h"
	#include "..\InterlasaUltraProKeys\IInterUltraProKy.h"
	#include "..\InterlasaUltraProKeys\IInterUltraObjectProKy.h"
#endif

/** N2PInOutDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.
*/
class N2PInOutDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PInOutDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~N2PInOutDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
	private:
	/**
			Funcion que llena el combo de usuario haciendo una consulta  a la base de datos de usuarios.
		*/
		void LlenarComboUsuarios();

		/**
			Funcion nque envia un mensaje de error al ocurrir algun fallo en la coexion a la base 
			de datos.
		*/
	//	void ver_com_error(_com_error &e);

		/**
			Funcion que valida el Password haciendo una cosulta en la Base de datos 
			sobre la tabla de usuarios trallendo el password y comparandolo con el password que fue tecleado.
			@Return bool16, kTrue en caso de ser verdadero.
		*/
		bool16 ValidaPaswdYLogInDB();

		
		
		/**
			IDUsuario o nombre del usuario.
		*/
		PMString Pswd;
		PMString Usuario;
		PMString FechaLogin;
		PMString Resultado;
};

CREATE_PMINTERFACE(N2PInOutDialogController, kN2PInOutDialogControllerImpl)

/* ApplyFields
*/
void N2PInOutDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	CDialogController::InitializeDialogFields(dlgContext);
	// Put code to initialize widget values here.
	
	do
	{
		
		//Limpia el Texto de la caja de password
		SetTextControlData(kN2PInOutEnterPswWidgetID,"");
		//Llenado de del cmbo de usuarios
		this->LlenarComboUsuarios();
		
	}while(false);
	
}

/* ValidateFields
*/
WidgetID N2PInOutDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	ErrorCode error=kFailure;
	
	IAbortableCmdSeq* sequ = CmdUtils::BeginAbortableCmdSeq();//ICommandSequence* sequ = CmdUtils::BeginCommandSequence();
	SequenceMark sequenceMark = CmdUtils::SetSequenceMark(sequ);
	
	do
	{	
		
		// Put code to validate widget values here.
		Pswd = GetTextControlData(kN2PInOutEnterPswWidgetID);
		Usuario = GetTextControlData(kN2PInOutComboUsuarioWidgetID);
	
		if(Usuario.NumUTF16TextChars()<1)
		{
			CAlert::InformationAlert(kN2PInOutSelectUserStringKey);
			result = kN2PInOutComboUsuarioWidgetID;
		}
		else
		{
			if(Pswd.NumUTF16TextChars()<1)
			{
				CAlert::InformationAlert(kN2PInOutTeclearContrasenaStringKey);
				result=	kN2PInOutEnterPswWidgetID;
			}
			else
			{
			
			
				////////////
				PMString fecha=FechaLogin;
			
				//obtengo el nombre de la aplicacion
				InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
				PMString Aplicacion=app->GetApplicationName();
				Aplicacion.SetTranslatable(kFalse);
	
				PMString hora=FechaLogin;
		
				PMString query;

				if(sequ==nil)
				{
					ASSERT_FAIL("Could not create command sequence");
					break;
				}
				
				//Name the command sequence:
				PMString sequName= PMString(kN2PInOutLogInCmdStringKey);
				sequName.SetTranslatable(kTrue);
				sequ->SetName(sequName);
				
				//Gurda los datos del usuario logedo en esta sesion
				InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
				if (wrkSpcPrefs == nil)
				{	
					ASSERT_FAIL("Invalid workspace prefs in N2PInOutDialogController::ApplyDialogFields()");
					break;
				}
								
				PMString Datos="News2Page Preferences\n";
				Datos.Append("Aplicacion:"+ Aplicacion +"\n");
				Datos.Append("FechaIn:" + fecha +"\n");
				Datos.Append("HoraIn:" + hora + "\n");
				Datos.Append("IDUserLogedString:" + Usuario + "\n");
			
				//Obtiene los datos en caso de que anteriormente se hayan realizado algun check in o checkOut
				Datos.Append("Estado_To_PageIn:"+	wrkSpcPrefs->GetEstatusNota() +"\n");
				Datos.Append("SeccionNota:"+	wrkSpcPrefs->GetSeccionNota() +"\n");
				Datos.Append("PaginaNota:" +	wrkSpcPrefs->GetPaginaNota() + "\n");
				Datos.Append("IssueNota:"+		wrkSpcPrefs->GetIssueNota() +"\n");
				Datos.Append("DateNota:" +		wrkSpcPrefs->GetDateNota() +"\n");
				Datos.Append("DirigidoA:"+ 		wrkSpcPrefs->GetDirigidoANota() +"\n");
				
				Datos.Append("StatusNotaPageOut:"+ wrkSpcPrefs->GetEstatusNotaPageOut() + "\n");
				Datos.Append("SeccionNotaPageOut:"+ wrkSpcPrefs->GetSeccionNotaPageOut() + "\n");
				Datos.Append("PaginaNotaPageOut:"+	wrkSpcPrefs-> GetPaginaNotaPageOut() + "\n");
				Datos.Append("IssueNotaPageOut:"+	wrkSpcPrefs->GetIssueNotaPageOut() + "\n");
				Datos.Append("DateNotaPageOut:"+	wrkSpcPrefs->GetDateNotaPageOut() + "\n");
				Datos.Append("DirigidoAPageOut:"+	wrkSpcPrefs->GetDirigidoANotaPageOut() + "\n");					
					
				InterfacePtr<ICommand> modWrkSpcPrefsCmd(Utils<IRegisterUsersUtils>()->CreateModPrefsCmd(wrkSpcPrefs,kTrue,1,Datos));
				if (modWrkSpcPrefsCmd == nil)
				{	
					ASSERT_FAIL("Can't create workspace prefs mod command in N2PInOutDialogController::ApplyDialogFields()");
					break;
				}
			
					
				
			
			
				//Coloca el nombre del usuario sobre la paleta
				PMString Userstrng="";
				Userstrng.Append(kN2PInOutAUserLogedStringKey);
				Userstrng.Translate();
			
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));		
			
				if(UpdateStorys==nil)
				{
					CAlert::InformationAlert("UpdateStorys");
					break;
				}
			
				Userstrng.Append(" ");
				Userstrng.Append(Usuario);
				Userstrng.SetTranslatable(kFalse);
				
				
			
				//Inicializa las tareas que se realizaron cada determinado tiempo en N2P
				//Inspecciona las notas para ver si se necesita realizar un update por si
				// cambio algo en la base de datos o en el diseÒo de la pagina
				InterfacePtr<IIdleTask> task(GetExecutionContextSession(), IID_IN2PSQLInspeccionaNotasTask);
				ASSERT(task);
				if(!task)
				{	
					CAlert::InformationAlert("task");
					break;
				}
				
			
				//Para Actualizar  la foto de vista previa de las paginas del documento en que se esta trabajando
				/*InterfacePtr<IIdleTask> taskFoto(gSession, IID_IN2PSQLFotoPageTask);
				ASSERT(taskFoto);
				if(!taskFoto)
				{
					CAlert::InformationAlert("taskFoto");
					break;
				}*/
				
					
				//Para checar Cuando do se ha modificado el Documento de Enfrente
				/*InterfacePtr<IIdleTask> taskLogOff(gSession, IID_IN2PSQLLOGOFFTASK);
				ASSERT(taskLogOff);
				if(!taskLogOff)
				{
					CAlert::InformationAlert("taskLogOff");
					break;
				}
				*/
			
				
				//Para activar y checar los mensajes para ael actual usuario
				/*InterfacePtr<IIdleTask> taskMSN(gSession, IID_IN2PMSNMensajesTask);
				ASSERT(taskMSN);
				if(!taskMSN)
				{
					//CAlert::InformationAlert("taskMSN");
				
				}*/
				
				
				
				//Para activar As¡gneaciones
				/*InterfacePtr<IIdleTask> taskAssigment(gSession, IID_IN2PASNTTASKINTERFACE);
				ASSERT(taskAssigment);
				if(!taskAssigment)
				{
					//CAlert::InformationAlert("taskAssigment");
					
				}*/
				
				/////
				//aplicar Estilo
				
			
				/*	InterfacePtr<IWorkspace> theWS(::QueryActiveWorkspace());
					if(theWS==nil)
					{
						break;
					}
				
				InterfacePtr<IUserInfo> UserInfo(theWS, UseDefaultIID());
				if(UserInfo!=nil)
				{
					CAlert::InformationAlert(Usuario);
					UserInfo->SetUserName(Usuario);
				}
				*/

				//Limpia los datos de la nota que se encuentran sobre la paleta
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboPubliWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboSeccionWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextTituloWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextBalazoWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPieWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPageWidgetID,"");
				 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,"");
		 	
			 	
				//Cambio
				InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																							  (
																							   kN2PwsdlcltUtilsBoss,	// Object boss/class
																							   IN2PWSDLCltUtils::kDefaultIID
																							   )));
				
				if(!	N2PWSDLCltUtil)
				{
					CAlert::InformationAlert("N2PWSDLCltUtil fails");
					break;
				}
				
				PreferencesConnection PrefConections;
				
				if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
				{
					ASSERT_FAIL("No pudo Obtener UpdateStorys no se pudo obtener las preferencias");
					break;
				}
				
				
				bool16 retval=N2PWSDLCltUtil->realizaConneccion(Usuario,
																"192.6.2.2", 
																"",
																Aplicacion, 
																PrefConections.URLWebServices);
				
				if(!retval)
				{
					break;
				}
				
					
				
	
		
				///////////////////////
				if(ValidaPaswdYLogInDB())
				{

				
					ErrorCode status = CmdUtils::ProcessCommand(modWrkSpcPrefsCmd);
			
					//N2PSQLUtilities::CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta(kN2PSQLCkBoxShowEdgesEditWidgetID,kTrue);
					
					
					/* DONGLE VERDE RED
					//N2PSQLUtilities::ImprimeMensaje(" N2PInOutDialogController::ValidateDialogFields Sentinel check");
					InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
					(
						kInterUltraObjetProKyBoss,	// Object boss/class
						IInterUltraObjectProKy::kDefaultIID
					)));
				
					if(UltraProCheck==nil)
					{
						CAlert::InformationAlert("Error al cargar el sentinel plugin IInterUltraObjectProKy");
						break;
					}
			
					PMString ResultString="";
					bool16 retval=UltraProCheck->RedValueN2PPlugin(ResultString);
					if(!retval)
					{
				
						CAlert::InformationAlert(ResultString);
						
						N2PsqlLogInLogOutUtilities::DoLogOutUserWhenShutDownApp();
						error=kFailure;
						break;
					}*/
					//N2PSQLUtilities::ImprimeMensaje(" N2PInOutDialogController::ValidateDialogFields Sentinel check fin");
					/********* DONGLE ROJO LOCAL *************
					InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*> (CreateObject
					(
						kInterUltraProKyBoss,	// Object boss/class
						IInterUltraProKy::kDefaultIID
					)));

					if(UltraProCheck==nil)
					{
						
						break;
					}
					PMString ResultString="";
					bool16 retval=UltraProCheck->CheckDongleN2PPlugins(ResultString);
					if(!retval)
					{
				
						CAlert::InformationAlert(ResultString);
						
						N2PsqlLogInLogOutUtilities::DoLogOutUserWhenShutDownApp();
						break;
					}
*/
					
										
					///Muestra la paleta del news2page
					N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPanelWidgetID, kTrue ) ;
					
					UpdateStorys->PlaceUserNameLogedOnPalette(Userstrng);
					task->InstallTask(0);
					//taskFoto->InstallTask(0);
					//taskLogOff->InstallTask(0);
					//taskMSN->InstallTask(0);
					N2PsqlLogInLogOutUtilities::SetUserNameOnApp(Usuario);	
					
					//Muestra los adornament lapiz y Estado de la nota
					IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
					if(document!=nil)
					{
						UpdateStorys->ShowFrameEdgesDocument( document, kTrue);		
					}

					CAlert::InformationAlert(kN2PInOutWelcometoSystemStringKey);
					result = kInvalidWidgetID;
					error=kSuccess;
				}
				else
				{
					result = kN2PInOutEnterPswWidgetID;
				}
				
			}	
		}
	}while(false);
	
	if(error==kSuccess)
	{
		CmdUtils::EndCommandSequence(sequ);
	}
	else
	{
		CmdUtils::RollBackCommandSequence(sequ,sequenceMark);
		CmdUtils::AbortCommandSequence(sequ);
		//CAlert::InformationAlert("OK entro muy bien");
	}
	
	
	return result;
}


/* 
	ApplyFields
*/
void N2PInOutDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
	// Replace with code that gathers widget values and applies them.
	//LLenar tabla movimientos
	
	//N2PSQLUtilities::InicializaDebugeo();
	SystemBeep();  
}


void N2PInOutDialogController::LlenarComboUsuarios()
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("PreferenciasObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PInOutComboUsuarioWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("no se encontro combo");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Id_Usuario From Usuario ORDER BY Id_Usuario ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			ASSERT_FAIL("No pudo Obtener UpdateStorys*");
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
		{
			ASSERT_FAIL("No pudo Obtener UpdateStorys no se pudo obtener las preferencias");
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		//SQLInterface->HAYPAPA();
		
		if(SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector))
		{
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				PMString cadena = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
				cadena.SetTranslatable(kFalse);
				dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
			}
		}
		
		
		
	}while(false);
}


bool16 N2PInOutDialogController::ValidaPaswdYLogInDB()
{
	bool16 value=kFalse;
	
	InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
	PMString Aplicacion=app->GetApplicationName();
	Aplicacion.SetTranslatable(kFalse);
		
	FechaLogin="";
	Resultado="";
	
	K2Vector<PMString> QueryVector;
	QueryVector.push_back(Usuario);
	
	//Indicando el Id Del evento dependiendo la aplicacion
	if(Aplicacion.Contains("InDesign"))
		QueryVector.push_back("1");
	else
		QueryVector.push_back("10");
	
	QueryVector.push_back(Pswd);
	QueryVector.push_back(Resultado);
	QueryVector.push_back(FechaLogin);

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		
		//////////////////////////////////////////////////////////////////////
		// Obtiene las preferencias ue por defaul se encuentra este usuario //
		//////////////////////////////////////////////////////////////////////
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		
		
		PMString SPString("CALL LOGINUSERN2P('"+QueryVector[0]+"',"+QueryVector[1]+",'"+QueryVector[2]+"',@X,@A);");
		QueryVector.clear();
		
		
		if(SQLInterface->SQLQueryDataBase(StringConection,SPString,QueryVector))
		{
			Resultado="";	
			
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				Resultado=SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
				FechaLogin=SQLInterface->ReturnItemContentbyNameColumn("FechaOUT",QueryVector[i]);
			}
			
			if(Resultado.GetAsNumber()==0)
			{
				value=kTrue;
				//Saludo  de vienvenida al sistema ditorial N2P
				
			}
			else
			{
				if(Resultado.GetAsNumber()==1)
				{
					value=kFalse;
				}
				else
				{
					if(Resultado.GetAsNumber()==2)
					{
						CAlert::ErrorAlert(kN2PLogInOutPassErroneoStringKey);
						value=kFalse;
					}
					else
					{
						if(Resultado.GetAsNumber()==3)
						{
							value=kFalse;
						}
						else
						{
							if(Resultado.GetAsNumber()==4)
							{
								CAlert::ErrorAlert(kN2PLogInOutAnyUserLoggedStringKey);
								value=kFalse;
							}
						}
					}
				}
			}
		}
		
	
	}while(false);
	return value;
}
//  Code generated by DollyXS code generator