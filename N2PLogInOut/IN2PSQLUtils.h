/*/
*/
#ifndef __N2PSQLUtils_h__
#define __N2PSQLUtils_h__

#include"IPMUnknown.h"
#include"N2PInOutID.h"
#include"K2Vector.h"


/**
	Esta es la interface del plug in para obtener las preferencias ode los usuarios
*/
class IN2PSQLUtils:public IPMUnknown
{
public:

	enum { kDefaultIID = IID_IN2PSQLUTILS };
	
	
	/**
		crea un nuevo registro o actualiza
	*/
		virtual const bool16 SQLSetInDataBase(const PMString& StringConnection,const PMString& SentenciaSQL) = 0;
	
	/**
		Query en Base de datos
	*/
	
		virtual const bool16 SQLQueryDataBase(const PMString& StringConnection,const PMString& SentenciaSQL, K2Vector<PMString>& QueryVector) = 0;
	/**
		
	*/	
		virtual const PMString ReturnItemContentbyNameColumn(const PMString& NameCol, PMString& RegisterContent) = 0;
		
		
		
		virtual const bool16 TestConnection(const PMString& StringConnection) = 0;
		
		virtual const bool16 ExecProcedure(const PMString& StringConnection,const PMString& SentenciaSQL)=0;
		
		
		virtual const bool16 StoreProcedureCheckInPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureSaveRevPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureCheckOutPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureLOGINUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureLOGOUTUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureCHECKOUTNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureCHECKINNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector, const bool16& NotaEsCheckOut)=0;
		
		virtual const bool16 StoreProcedureSET_NOTA_COLO_CADA(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureIMPORTARNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector)=0;
		
		virtual const bool16 StoreProcedureCHECKINELEMENTONOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector, const bool16& NotaEnCheckOut)=0;
		
	//	virtual const bool16 HAYPAPA()=0;

};


#endif // __N2PRegisterUsers_h__