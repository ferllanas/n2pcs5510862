//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================





#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "PaletteRefUtils.h" //"IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IStringData.h"
#include "IWorkspace.h"
#include "ICommand.h"
#include "UIDList.h"
#include "InCopySharedID.h"
#include "IPalettePanelUtils.h" //"IPalettePanelUtils.h"

// Dialog-specific resource includes:

#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "IIdleTask.h"

#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ILayoutUtils.h"
// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "PreferenceUtils.h"

// Project includes


//#include "../../../source/Interlasa/N2PSQL/N2PSQLListBoxHelper.h"

#include "IN2PSQLUtils.h"
#include "N2PsqlLogInLogOutUtilities.h"
#include "N2PRegisterUsers.h"
#include "IRegisterUsersUtils.h"


#ifdef MACINTOSH
/*	#include "../../../source/Interlasa/InterlasaUltraProKeys/InterUltraProKyID.h"
	#include "../../../source/Interlasa/InterlasaUltraProKeys/IInterUltraProKy.h"
	#include "../../../source/Interlasa/InterlasaUltraProKeys/IInterUltraObjectProKy.h"
	
*/	#include "N2PSQLUtilities.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLListBoxHelper.h"

	#include "N2PwsdlcltID.h"
	#include "N2PWSDLCltUtils.h"
#endif

#ifdef WINDOWS
	#include "..\InterlasaUltraProKeys\InterUltraProKyID.h"
	#include "..\InterlasaUltraProKeys\IInterUltraProKy.h"
	#include "..\InterlasaUltraProKeys\IInterUltraObjectProKy.h"
	
	#include "..\N2PSQL\N2PSQLUtilities.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLListBoxHelper.h"
#endif

#ifdef WINDOWS
#define PLATFORM_PATH_DELIMITER kTextChar_ReverseSolidus
#endif
#ifdef MACINTOSH
#define PLATFORM_PATH_DELIMITER kTextChar_Colon
#endif


bool16 N2PsqlLogInLogOutUtilities::DoLogInUser()
{
	bool16 retval=kFalse;
	do
	{
		//CAlert::InformationAlert("A");
		if(!N2PsqlLogInLogOutUtilities::CanDoLogInUser())
		{
			//CAlert::InformationAlert(kN2PInOutAUsersLogedStringKey);
			break;
		}
		else
		{
			//CAlert::InformationAlert("B");
			if( N2PsqlLogInLogOutUtilities::DoOpenDialogLogInUser())
			{
				//CAlert::InformationAlert("D");
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
			
				if(UpdateStorys==nil)
				{
					ASSERT_FAIL("No pudo Obtener UpdateStorys*");
					break;
				}
				
				//CAlert::InformationAlert("E");
				UpdateStorys->SetShortCutToMenuFittingActions();
				retval=kTrue;
				break;
			}
		}
		
	}while(false);
	return(retval);	
}


bool16 N2PsqlLogInLogOutUtilities::CanDoLogInUser() 
{
	bool16 retval=kFalse;
	do
	{
	
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			//CAlert::InformationAlert(kN2PInOutAUsersLogedStringKey);
			 retval=kFalse;
			break;
		}
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			
			//CAlert::InformationAlert("No pudo Obtener UpdateStorys*");
			//CAlert::InformationAlert("a chinga no salio 11111");
			//ASSERT_FAIL("No pudo Obtener UpdateStorys*");
			break;
		}
		//CAlert::InformationAlert("a chinga no salio ﬁ");
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
		{
			//CAlert::InformationAlert("banana");
			CAlert::InformationAlert(kN2PLogInOutNopudoCargarPreferenciasStringKey);
			//CAlert::InformationAlert("a chinga no salio ssss");
			//ASSERT_FAIL("No pudo Obtener UpdateStorys no se pudo obtener las preferencias");
			break;
		}
		//CAlert::InformationAlert("a chinga no salio");
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			//CAlert::ErrorAlert("N2PSQLPreferDlgObserver::DoTestConection(),Invalid SQLInterface");
			break;
		}
		
		if(SQLInterface->TestConnection(StringConection))
		{
			 retval=kTrue;
		}
		else
		{
			CAlert::InformationAlert(kN2PSQLErrorConecctionDSNStringKey);
			break;
		}
		
		
		
		
	}while(false);
	
	//CAlert::InformationAlert("defw salir");
	return(retval);
		
}

bool16 N2PsqlLogInLogOutUtilities::ExistsLoggedUsers()
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			//CAlert::ErrorAlert("Invalid workspace prefs in N2PInOutActionComponent::OpenDlgLogIn()");
			break;
		}		
		
		PMString IDUsuario="";	
		PMString Aplicacion="";	
		IDUsuario.AppendNumber(wrkSpcPrefs->GetID_Usuario());
		Aplicacion.Append(wrkSpcPrefs->GetNameApp());
		
	
		if(IDUsuario.NumUTF16TextChars()>0 && IDUsuario.GetAsNumber()>=0 && Aplicacion.NumUTF16TextChars()>0)
		{
			retval=kTrue;
			break;
		}
		else
		{
			 retval=kFalse;
			 break;
		}
	}while(false);
	return(retval);
}

bool16 N2PsqlLogInLogOutUtilities::DoOpenDialogLogInUser()
{
	bool16 retval=kFalse;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {
			CAlert::InformationAlert("application == nil");
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			CAlert::InformationAlert("dialogMgr == nil");
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PInOutPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) 
		{
			CAlert::InformationAlert("dialog == nil");
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		retval=kTrue;
	}while(false);
	return(retval);	
}



bool16 N2PsqlLogInLogOutUtilities::CanDoLogOutUser()
{
	bool16 retval=kFalse;
	do
	{
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers()==kFalse)
		{
			retval=kFalse;
			break;
		}
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			ASSERT_FAIL("No pudo Obtener UpdateStorys*");
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
		{
			ASSERT_FAIL("No pudo Obtener UpdateStorys no se pudo obtener las preferencias");
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			//CAlert::ErrorAlert("N2PSQLPreferDlgObserver::DoTestConection(),Invalid SQLInterface");
			break;
		}
		
		
		
		if(SQLInterface->TestConnection(StringConection))
		{
			 retval=kTrue;
		}
		else
		{
			CAlert::InformationAlert(kN2PSQLErrorConecctionDSNStringKey);
			break;
		}
		
		
		
	}while(false);
	
	return(retval);
}


bool16 N2PsqlLogInLogOutUtilities::DoLogOutUser()
{
	bool16 retval=kFalse;
	
	ErrorCode error=kFailure;
	IAbortableCmdSeq* sequ = CmdUtils::BeginAbortableCmdSeq();//ICommandSequence* sequ = CmdUtils::BeginCommandSequence();
	SequenceMark sequenceMark = CmdUtils::SetSequenceMark(sequ);
	
	do
	{
		if(N2PsqlLogInLogOutUtilities::CanDoLogOutUser())	
		{
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			ASSERT(app);
			if (app == nil) {	
				break;
			}
			
			PMString NomAplicacionActual=app->GetApplicationName();
			NomAplicacionActual.SetTranslatable(kFalse);
			
			PMString Aplicacion=app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);
			
			PMString IDUsuario="";
			PMString HoraIn="";
			PMString FechaIn="";
	
			InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				ASSERT_FAIL("Invalid workspace prefs in N2PsqlLogInLogOutUtilities::DoLogOutUser()");
				CAlert::ErrorAlert("Invalid workspace prefs in N2PsqlLogInLogOutUtilities::DoLogOutUser()");
				break;
			}
		
			Aplicacion="";
			Aplicacion.Append(wrkSpcPrefs->GetNameApp());
		
			IDUsuario=wrkSpcPrefs->GetIDUserLogedString();
			
		
			HoraIn="";
			HoraIn.Append(wrkSpcPrefs->GetHoraIn());
		
			FechaIn="";
			FechaIn.Append(wrkSpcPrefs->GetFechaIn());
		
		
			K2Vector<PMString> QueryVector;
			QueryVector.push_back(IDUsuario);
			if(NomAplicacionActual.Contains("InDesign"))
				QueryVector.push_back("2");
			else
				QueryVector.push_back("11");
			QueryVector.push_back(FechaIn);
			QueryVector.push_back("0");
			
			PMString sequName= PMString(kN2PInOutLogOutCmdStringKey);
			sequName.SetTranslatable(kTrue);
			sequ->SetName(sequName);
			
			if(N2PsqlLogInLogOutUtilities::InsertEnMovimientos(QueryVector)==kFalse)
			{
				break;
			}
			

			// -- Make the command to change the workspace prefs
					
			PMString Datos="News2Page Preferences\n";
			Datos.Append("Aplicacion:\n");
			Datos.Append("FechaIn:\n");
			Datos.Append("HoraIn:\n");
			
			Datos.Append("Estado_To_PageIn:"+	wrkSpcPrefs->GetEstatusNota() +"\n");
			Datos.Append("SeccionNota:"+	wrkSpcPrefs->GetSeccionNota() +"\n");
			Datos.Append("PaginaNota:" +	wrkSpcPrefs->GetPaginaNota() + "\n");
			Datos.Append("IssueNota:"+		wrkSpcPrefs->GetIssueNota() +"\n");
			Datos.Append("DateNota:" +		wrkSpcPrefs->GetDateNota() +"\n");
			Datos.Append("DirigidoA:"+ 		wrkSpcPrefs->GetDirigidoANota() +"\n");
				
			Datos.Append("StatusNotaPageOut:"+ wrkSpcPrefs->GetEstatusNotaPageOut() + "\n");
			Datos.Append("SeccionNotaPageOut:"+ wrkSpcPrefs->GetSeccionNotaPageOut() + "\n");
			Datos.Append("PaginaNotaPageOut:"+	wrkSpcPrefs->GetPaginaNotaPageOut() + "\n");
			Datos.Append("IssueNotaPageOut:"+	wrkSpcPrefs->GetIssueNotaPageOut() + "\n");
			Datos.Append("DateNotaPageOut:"+	wrkSpcPrefs->GetDateNotaPageOut() + "\n");
			Datos.Append("DirigidoAPageOut:"+	wrkSpcPrefs->GetDirigidoANotaPageOut() + "\n");		
			
			InterfacePtr<ICommand> modWrkSpcPrefsCmd(Utils<IRegisterUsersUtils>()->CreateModPrefsCmd(wrkSpcPrefs,kTrue,-1,Datos));
			if (modWrkSpcPrefsCmd == nil)
			{	
				ASSERT_FAIL("Can't create workspace prefs mod command in N2PsqlLogInLogOutUtilities::DoLogOutUser()");
				break;
			}
			// -- Process the command
			
			ErrorCode status = CmdUtils::ProcessCommand(modWrkSpcPrefsCmd);
			
			//Muestra paleta para podder borra la paleta
			N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPanelWidgetID, kTrue ) ;
			N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPhotoPanelWidgetID, kTrue ) ;
			
			//Borra el nombre del actual usuario
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				ASSERT_FAIL("No pudo Obtener UpdateStorys*");
				break;
			}
			
			//limpia la lista de Busqueda de notas
			IControlView* iControViewList=GetAssociatedWidget(kN2PsqlListUpdateWidgetID);
			N2PSQLListBoxHelper listUpdateHelper(iControViewList, kN2PsqlPluginID,kN2PsqlListUpdateWidgetID);
			listUpdateHelper.EmptyCurrentListBox();
			
			
			//Cambia a vacio el nombre del  usuario logeado actualmente sobre la paleta
			UpdateStorys->PlaceUserNameLogedOnPalette("");
			
			//Oculta la paleta
			N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPanelWidgetID, kFalse ) ;
			N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPhotoPanelWidgetID, kFalse ) ;
			
			InterfacePtr<IIdleTask> task(GetExecutionContextSession(), IID_IN2PSQLInspeccionaNotasTask);
			ASSERT(task);
			if(!task)
			{
				break;
			}
			task->UninstallTask();
		
		
			/*InterfacePtr<IIdleTask> taskFoto(gSession, IID_IN2PSQLFotoPageTask);
			ASSERT(taskFoto);
			if(!taskFoto)
			{
				break;
			}
			taskFoto->UninstallTask();*/
			
			InterfacePtr<IIdleTask> taskLogOff(GetExecutionContextSession(), IID_IN2PSQLLOGOFFTASK);
			ASSERT(taskLogOff);
			if(!taskLogOff)
			{
				break;
			}
			taskLogOff->UninstallTask();
			
			//Para activar y checar los mensajes para ael actual usuario
			/*InterfacePtr<IIdleTask> taskMSN(gSession, IID_IN2PMSNMensajesTask);
			ASSERT(taskMSN);
			if(!taskMSN)
			{
				//CAlert::InformationAlert("taskMSN");
					
			}
			taskMSN->UninstallTask();
			*/
				
			//Para activar As¡gneaciones
			/*InterfacePtr<IIdleTask> taskAssigment(gSession, IID_IN2PASNTTASKINTERFACE);
			ASSERT(taskAssigment);
			if(!taskAssigment)
			{
				//CAlert::InformationAlert("taskAssigment");
					
			}
			taskAssigment->UninstallTask();*/
			
			N2PsqlLogInLogOutUtilities::SetUserNameOnApp("");
			
			//Destruye todos los dialogos
			/*PARA EL MENSAJERO DE NEWS2PAGE
			InterfacePtr< IN2PMSGDynPnPanelManager> dynPanelManager( IN2PMSGDynPnPanelManager::QueryDynamicPanelManager());
			ASSERT(dynPanelManager);
			if(!dynPanelManager) 
			{
				CAlert::InformationAlert("dynPanelManager");
				break;
			}
			int32 dynPanelCount = dynPanelManager->DestroyALL();*/
						
			
			CAlert::InformationAlert(kN2PInOutFinishedSesionStringKey);	
			error=kSuccess;
		}
	}while(false);
	
	/*	DONGLE VERDE RED
	////N2PSQLUtilities::ImprimeMensaje("N2PsqlLogInLogOutUtilities::DoLogOutUser() Sentinel check");
			InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
			(
				kInterUltraObjetProKyBoss,	// Object boss/class
				IInterUltraObjectProKy::kDefaultIID
			)));
			
			if(UltraProCheck==nil)
			{
				 retval=kFalse;
				
			}
		
			PMString ResultString="";
			retval=UltraProCheck->CheckDongleREDRelease_N2PPlugins(ResultString);
			if(!retval)
			{
			
				CAlert::InformationAlert(ResultString);
				 retval=kFalse;
				
			}*/
	////N2PSQLUtilities::ImprimeMensaje("N2PsqlLogInLogOutUtilities::DoLogOutUser() Sentinel check fin");
	/********* DONGLE ROJO LOCAL *************
					InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*> (CreateObject
					(
						kInterUltraProKyBoss,	// Object boss/class
						IInterUltraProKy::kDefaultIID
					)));

					if(UltraProCheck==nil)
					{
						
						retval=kFalse;
					}
					PMString ResultString="";
					retval=UltraProCheck->CheckDongleN2PPlugins(ResultString);
					if(!retval)
					{
				
						CAlert::InformationAlert(ResultString);
						
						 retval=kFalse;
					}
	
			
	*/
	
	
	if(error==kSuccess)
	{
		CmdUtils::EndCommandSequence(sequ);
	}
	else
	{
		CmdUtils::RollBackCommandSequence(sequ,sequenceMark);
		CmdUtils::AbortCommandSequence(sequ);
	}
	
	//N2PSQLUtilities::ImprimeMensaje("Finaliza session");
	//N2PSQLUtilities::FinalizaDebugeo();
	return(retval);
}

bool16 N2PsqlLogInLogOutUtilities::DoLogOutUserWhenShutDownApp()
{
	bool16 retval=kFalse;
	do
	{
		if(N2PsqlLogInLogOutUtilities::CanDoLogOutUser())	
		{
			PMString Aplicacion="";
			
			
			PMString IDUsuario="";
			PMString HoraIn="";
			PMString FechaIn="";
	
	
	
			InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				ASSERT_FAIL("Invalid workspace prefs in N2PsqlLogInLogOutUtilities::DoLogOutUser()");
				CAlert::ErrorAlert("Invalid workspace prefs in N2PsqlLogInLogOutUtilities::DoLogOutUser()");
				break;
			}
		
			Aplicacion.Append(wrkSpcPrefs->GetNameApp());
		
			IDUsuario=wrkSpcPrefs->GetIDUserLogedString();
			
		
			HoraIn="";
			HoraIn.Append(wrkSpcPrefs->GetHoraIn());
		
			FechaIn="";
			FechaIn.Append(wrkSpcPrefs->GetFechaIn());
		
		
			K2Vector<PMString> QueryVector;
			QueryVector.push_back(IDUsuario);
			if(Aplicacion.Contains("InDesign"))
				QueryVector.push_back("2");
			else
				QueryVector.push_back("11");
			QueryVector.push_back(FechaIn);
			QueryVector.push_back("0");
			
			if(N2PsqlLogInLogOutUtilities::InsertEnMovimientos(QueryVector)==kFalse)
			{
				break;
			}
			//Destruye todos los dialogos
			/*PARA EL MENSAJEROD DE NEWS2PAGE
			InterfacePtr< IN2PMSGDynPnPanelManager> dynPanelManager( IN2PMSGDynPnPanelManager::QueryDynamicPanelManager());
			ASSERT(dynPanelManager);
			if(!dynPanelManager) 
			{
				CAlert::InformationAlert("dynPanelManager");
				break;
			}*/
			
			/******DONGLE VERDE RED
			////N2PSQLUtilities::ImprimeMensaje("N2PsqlLogInLogOutUtilities::DoLogOutUserWhenShutDownApp() Sentinel check");
			InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
			(
				kInterUltraObjetProKyBoss,	// Object boss/class
				IInterUltraObjectProKy::kDefaultIID
			)));
			
			if(UltraProCheck==nil)
			{
				 retval=kFalse;
				
			}
		
			PMString ResultString="";
			retval=UltraProCheck->CheckDongleREDRelease_N2PPlugins(ResultString);
			if(!retval)
			{
			
				CAlert::InformationAlert(ResultString);
				 retval=kFalse;
				
			}*/
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			
			PreferencesConnection PrefConections;
			
			if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
			{
				break;
			}

			
			InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																						  (
																						   kN2PwsdlcltUtilsBoss,	// Object boss/class
																						   IN2PWSDLCltUtils::kDefaultIID
																						   )));
			
			if(!N2PWSDLCltUtil)
			{
				CAlert::InformationAlert("salio Logout");
				break;
			}
			retval=N2PWSDLCltUtil->realizaDesconeccion(IDUsuario,
															"192.6.2.2", 
															"",
															Aplicacion, 
															PrefConections.URLWebServices);//"http://localhost/N2PWebservices/N2PServerwsdl.php"
			if(!retval)
			{
				break;
			}
			
			////N2PSQLUtilities::ImprimeMensaje("N2PsqlLogInLogOutUtilities::DoLogOutUserWhenShutDownApp() Sentinel check fin");
#ifdef MACINTOSH
			//CAlert::InformationAlert(kN2PInOutFinishedSesionStringKey);	
#endif
		}
	}while(false);
	return(retval);
}

PMString N2PsqlLogInLogOutUtilities::FechaYHora()
{
  time_t tiempo;
  char cad[80];
  struct tm *tmPtr;

  tiempo = time(NULL);
  tiempo = tiempo+86400;
  
  
 // setlocale(LC_ALL,"C");

  tmPtr = localtime(&tiempo);
  
  
  
  PMString Idioma(kN2PInOutImplIdiomaStringKey);
  Idioma.Translate();
  Idioma.SetTranslatable(kTrue);
  

  if(Idioma=="English")
  {
  	
  	strftime( cad, 80, "%H:%M:%S^ %m/%d/%Y", tmPtr );			//%H:%M:%S^ %A, %B %d, %Y

  }
  else
  {
  	if(Idioma=="Spanish")
  	{
  		 strftime( cad, 80, "%H:%M:%S^ %m/%d/%Y", tmPtr );		//"%H:%M:%S^ %A %d de %B de %Y"
  	}
  }
 
  PMString Fecha(cad);
  Fecha.Translate();
  Fecha.SetTranslatable(kTrue);
  return Fecha;
}

bool16 N2PsqlLogInLogOutUtilities::InsertEnMovimientos(K2Vector<PMString>& QueryVector)
{

		
	bool16 retval=kFalse;
	do
	{
		
	
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetPreferencesConnectionForNameConection("Default",PrefConections))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		
		if(SQLInterface==nil)
		{
			break;
		}
		
		PMString SPString("CALL LOGOUTUSERN2P('"+QueryVector[0]+"',"+QueryVector[1]+",@X,@A);");
		QueryVector.clear();
		
		
		if(SQLInterface->SQLQueryDataBase(StringConection,SPString,QueryVector))
		{
			PMString Resultado="";	
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				Resultado=SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
				
			}
			
			//CAlert::InformationAlert(QueryVector[0]);
			//CAlert::InformationAlert(QueryVector[1]);
			if(Resultado.GetAsNumber()==0)
			{
				retval=kTrue;
				//FechaLogin=QueryVector[1];
				//Saludo  de vienvenida al sistema ditorial N2P
				//CAlert::InformationAlert(kN2PInOutWelcometoSystemStringKey);
			}
			else
			{
				if(Resultado.GetAsNumber()==1)
				{
					retval=kFalse;
				}
				else
				{
					if(Resultado.GetAsNumber()==2)
					{
						//CAlert::ErrorAlert(kN2PLogInOutPassErroneoStringKey);
						retval=kFalse;
					}
					else
					{
						if(Resultado.GetAsNumber()==3)
						{
							retval=kFalse;
						}
						else
						{
							if(Resultado.GetAsNumber()==4)
							{
								//CAlert::ErrorAlert(kN2PLogInOutAnyUserLoggedStringKey);
								retval=kFalse;
							}
						}
					}
				}
			}
		}
		/*SQLInterface->StoreProcedureLOGOUTUSER(StringConection, QueryVector);
		if(QueryVector[3].GetAsNumber()!=0)
		{
			retval=kFalse;
		}
		else
		{
			retval=kTrue;
		}*/
		
	
	}while(false);
	
	return retval;
}


/*

*/
IControlView * N2PsqlLogInLogOutUtilities::GetAssociatedWidget(WidgetID widgetID)
{
	IControlView * listControlView=nil;
	do{
		InterfacePtr<IPanelControlData> panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID));	
		ASSERT_MSG( panelData !=nil, "panelData nil");
		if(panelData==nil)
		{
			
			break;
		}
		listControlView = panelData->FindWidget(widgetID);
		ASSERT_MSG( listControlView != nil, "listControlView nil");
		if(listControlView == nil) {
			
			break;
		}
	} while(0);
	
	return listControlView;
}


//  Code generated by DollyXS code generator
void N2PsqlLogInLogOutUtilities::SetUserNameOnApp(PMString pmUserName)
{

	do
	{
		pmUserName.SetTranslatable(kFalse); 
	InterfacePtr<IWorkspace> iWorkspace(GetExecutionContextSession()->QueryWorkspace()); 
	ASSERT(iWorkspace);
	if (iWorkspace == nil) {	
		break;
	}
	
	InterfacePtr<ICommand> setUserNameCmd(CmdUtils::CreateCommand(kSetUserNameCmdBoss)); 
	ASSERT(setUserNameCmd);
	if (setUserNameCmd == nil) {	
		break;
	}
	
	setUserNameCmd->SetItemList(UIDList(::GetUIDRef(iWorkspace))); 
	InterfacePtr<IStringData> cmdStringData(setUserNameCmd, IID_ISTRINGDATA);
	ASSERT(cmdStringData);
	if (cmdStringData == nil) {	
		break;
	}


	cmdStringData->Set(pmUserName); 
	CmdUtils::ProcessCommand(setUserNameCmd);
	}while(false);
	
}



bool16 N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID(WidgetID widget, bool16 show) 
{
	bool16 retval=kFalse;
	do
	{
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			ASSERT(app);
			if(!app) {
				break;
			}
			/*
			CS2 -> CS3
			InterfacePtr<IPaletteMgr> palMgr(app->QueryPanelManager());
			ASSERT(palMgr);
			if(!palMgr) {
				break;
			}*/
			InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager(),UseDefaultIID());
			ASSERT(panelMgr);
			if(!panelMgr) {
				break;
			}

			//PanelMgrEntryList* ListaPaneles=panelMgr->GetPanelList();
			//PONER EL PANEL DE N2PSQL
			if(show)
			{
				if(!panelMgr->IsPanelWithWidgetIDShown(widget))
					panelMgr->ShowPanelByWidgetID( widget );
			}
			else
			{
				if(panelMgr->IsPanelWithWidgetIDShown(widget))
					panelMgr->HidePanelByWidgetID( widget );
			}
				
			
			 retval=kTrue;
	}while(false);
	return retval;
}