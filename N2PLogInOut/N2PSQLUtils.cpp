/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "IN2PSQLUtils.h"
#include "N2PInOutID.h"
#include "CAlert.h"


#include <stdio.h>
#include <stdlib.h>
#include <sql.h>
#include <sqlext.h>

#define MAX_BUF_LEN  30000//30000
#define DATA_BUFFER_LEN  8192

/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	author Juan Fernando Llanas Rdz
*/
class N2PSQLUtils : public CPMUnknown<IN2PSQLUtils>
{
public:
	/** Constructor.
		param boss boss object on which this interface is aggregated.
	 */
	N2PSQLUtils (IPMUnknown *boss);
	
	virtual ~N2PSQLUtils() {}



	/**
		crea un nuevo registro o actualiza
	*/
	virtual const bool16 SQLSetInDataBase(const PMString& StringConnection,const PMString& SentenciaSQL);
	
	/**
		Query en Base de datos
	*/
	
	virtual const bool16 SQLQueryDataBase(const PMString& StringConnection,const PMString& SentenciaSQL, K2Vector<PMString>& QueryVector);
	
	/**
	*/
	virtual const PMString ReturnItemContentbyNameColumn(const PMString& NameCol, PMString& RegisterContent);
	
	virtual const bool16 TestConnection(const PMString& StringConnection);
	
	virtual const bool16 ExecProcedure(const PMString& StringConnection,const PMString& SentenciaSQL);
	
	virtual const bool16 StoreProcedureCheckInPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureSaveRevPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureCheckOutPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureLOGINUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureLOGOUTUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureCHECKOUTNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector);

	virtual const bool16 StoreProcedureCHECKINNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector,  const bool16& NotaEnCheckOut);
	
	virtual const bool16 StoreProcedureSET_NOTA_COLO_CADA(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	
	virtual const bool16 StoreProcedureIMPORTARNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector);
	
	virtual const bool16 StoreProcedureCHECKINELEMENTONOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector, const bool16& NotaEnCheckOut);
 
 	//virtual const bool16 HAYPAPA();
private:
	
	bool16 ejecutaStoreProcedureIMPORTARNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );
	
	bool16 ejecutaStoreProcedureSET_NOTA_COLO_CADA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );

	bool16 ejecutaStoreProcedureCHECKOUTNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );


	bool16 ejecutaStoreProcedureLOGOUTUSER(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );
	 
	bool16 ejecutaStoreProcedureLOGINUSER(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );

	bool16 ejecutaStoreProcedureCheckOutPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );


	bool16 ejecutaStoreProcedureSaveRevPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );

	bool16 ejecutaStoreProcedureCheckInPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector );
	
	bool16 SQLInicializaConexion(const PMString& StringConnection,SQLHANDLE& stmt,SQLHANDLE& con,SQLHANDLE& env);
	
	bool16 SQLEjecutaSimpleSentencia(SQLHANDLE& stmt,PMString Sentencia);
	
	bool16 ejecutaStoreProcedureCHECKINNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector , const bool16& NotaEnCheckOut);
	
	bool16 SQLEjecutaQuerySentencia(SQLHANDLE& stmt,PMString Sentencia,K2Vector<PMString>& QueryVector  );
	
	PMString printBytes(char* fieldName, char* field, int fieldLen);
	
	bool16 ejecutaStoreProcedure(SQLHANDLE& stmt,PMString Sentencia);
	
	
	bool16 ejecutaStoreProcedureCHECKINELEMENTONOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector , const bool16& NotaEnCheckOut);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLUtils, kN2PSQLUtilsImpl)


/* HelloWorld Constructor
*/
N2PSQLUtils::N2PSQLUtils(IPMUnknown* boss) 
: CPMUnknown<IN2PSQLUtils>(boss)
{
	// do nothing.

	/* NOTE: 
		There used to be code that would set the preference value by default.
		That was removed in favor of the workspace responders.
		
		The responders get called, and they will try to setup the value
		for this custom preference.  It is one of the responders that will
		set the correct initial value for the cached preference, fPrefState.
		
		If the global workspace is already created from a previous 
		application session, the ReadWrite() method will set the 
		cached preference, fPrefState. The same ReadWrite() will be called when 
		an existing document is opened.
		
		Refer to CstPrfNewWSResponder.cpp and N2PSQLUtils.cpp
		for details on how to set the workspace defaults. 
	*/
}


const PMString N2PSQLUtils::ReturnItemContentbyNameColumn(const PMString& NameCampo, PMString& RegisterContent)
{
	
	//CAlert::InformationAlert(RegisterContent);
	//CAlert::InformationAlert(NameCampo);
	PMString BuscarString=NameCampo;
	BuscarString.SetTranslatable(kFalse);
	BuscarString.Append(":");
	PMString Retval=PMString("", PMString::kDontTranslateDuringCall);//"";
	
	bool16 isigualTextoBuscado=kFalse;
	int32 index=0;
	int32 indexNameCampo=0;
	do
	{
		indexNameCampo=RegisterContent.IndexOfString(BuscarString,index);
		if(indexNameCampo>=0)
		{
			//CAlert::InformationAlert("Si se encontro");
			Retval=(RegisterContent.Substring(indexNameCampo,(BuscarString.NumUTF16TextChars())))->GrabCString();
			//CAlert::InformationAlert("Obtiene el texto:"+Retval);
			if(Retval==BuscarString)
			{
				//CAlert::InformationAlert("OK se encontrooooo");
				isigualTextoBuscado=kTrue;
			}
			else
			{
				index=indexNameCampo+BuscarString.NumUTF16TextChars();
			}
		}
	}while(isigualTextoBuscado==kTrue && indexNameCampo==-1);
	
	Retval="";
	if(indexNameCampo==-1)
	{
		Retval="";
	}
	else
	{
		indexNameCampo=indexNameCampo + (BuscarString.NumUTF16TextChars());
		
		int32 indexChar=RegisterContent.IndexOfString("©",indexNameCampo);
		/*if(debug)
		{
			PMString CC="";
			CC.SetTranslatable(kFalse);
			CC=NameCampo+", ";
			
			CC.AppendNumber(indexChar);
			CC.Append(", ");
			CC.AppendNumber(indexNameCampo);
			CC.Append(":  ");
			CC.Append(RegisterContent);
			//CAlert::InformationAlert(CC);
			
			if((indexChar-indexNameCampo)==1)
			{
				CC="";
				CC.AppendNumber(RegisterContent.GetWChar(indexChar+1).GetValue());
				CAlert::InformationAlert(CC);
			}
		}*/
		if((indexChar-indexNameCampo)>0)
		{
			
			if((indexChar-indexNameCampo)==1)
			{
				if(RegisterContent.GetWChar(indexChar+1).GetValue()>0)
				{
					K2::scoped_ptr<PMString> ptrRightStr(RegisterContent.Substring(indexNameCampo,(indexChar-indexNameCampo-1)));
					if(ptrRightStr)
					{
						Retval=*ptrRightStr;
					}
				}
			}
			else 
			{
				if((indexChar-indexNameCampo)>1)
				{
					K2::scoped_ptr<PMString> ptrRightStr(RegisterContent.Substring(indexNameCampo,(indexChar-indexNameCampo-1)));
					if(ptrRightStr)
					{
						Retval=*ptrRightStr;
					}
				}
				else
					Retval="";
			}			
		}
		else
		{
			Retval="";
		}
	}
	return(Retval);
}


const bool16 N2PSQLUtils::TestConnection(const PMString& StringConnection)
{
	bool16 retval=kFalse;
	
	/*do
	{
	 	SQLHANDLE henv=NULL;
    	SQLHANDLE hdbc=NULL ;
 	
 		//* ... 
 
 	   //* allocate an environment handle
 	   SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv ) ;
 
  	  //* Connect to first data source
   
   
 	   SQLCHAR server[10] ;
  	  SQLCHAR uid[10] ;
  	  SQLCHAR pwd[10] ;
 
    	//* allocate a connection handle     
 	 	if ( SQLAllocHandle( SQL_HANDLE_DBC, henv, &hdbc) != SQL_SUCCESS ) 
   	 	{
   	     	CAlert::InformationAlert( ">---ERROR while allocating a connection handle-----\n" ) ;
       	 	break;
   	 	}
 
    	//* Set AUTOCOMMIT OFF 
    	if ( SQLSetConnectAttr( &hdbc, SQL_ATTR_AUTOCOMMIT, ( void * ) SQL_AUTOCOMMIT_OFF, SQL_NTS ) != SQL_SUCCESS ) 
   		 {
    	     CAlert::InformationAlert( ">---ERROR while setting AUTOCOMMIT OFF ------------\n" ) ;
    	     break;
    	}
 
  		//  printf( ">Enter Server Name:\n" ) ;
  		 // gets( ( char * ) server ) ;
 		// //  printf( ">Enter User Name:\n" ) ;
  		//  gets( ( char * ) uid ) ;
  		//  printf( ">Enter Password:\n" ) ;
  		//  gets( ( char * ) pwd ) ;
 
    	if ( SQLConnect( &hdbc, (SQLCHAR *)"SQLServer", SQL_NTS,  (SQLCHAR *)"Fernando",    SQL_NTS, (SQLCHAR *)"falcon",    SQL_NTS) != SQL_SUCCESS ) 
    	{
        	CAlert::InformationAlert( ">--- ERROR while connecting to %s -------------\n") ;
 
       		SQLDisconnect( &hdbc ) ;
        	SQLFreeHandle( SQL_HANDLE_DBC, &hdbc ) ;
       		break;
    	}
   		else              //* Print Connection Information
        	CAlert::InformationAlert( "Successful Connect to %s\n") ;
   		
 	}while(false);*/
	
 
   //PMString StringConnection2="DRIVER={MySQL ODBC 3.51 Driver};SERVER=192.168.213.23;DATA SOURCE=n2psql2008;UID=acceso;PWD=1;OPTION=3;PORT=3306;SOCKET=/tmp/mysql.sock;";
 
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
			
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
	}
	else
	{
		retval=kFalse;
	}
	return(retval);
}

const bool16 N2PSQLUtils::SQLSetInDataBase(const PMString& StringConnection,const PMString& SentenciaSQL)
{
	bool16 retval=kFalse;
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(SQLEjecutaSimpleSentencia(stmt,SentenciaSQL)==kTrue)
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			SQLDisconnect(con);
			SQLFreeHandle(SQL_HANDLE_DBC, con);
			SQLFreeHandle(SQL_HANDLE_ENV, env);
			//CAlert::InformationAlert("XXX");
			retval=kFalse;
		}
	}
	else
	{
	
		retval=kFalse;
	}
	return(retval);
}

const bool16 N2PSQLUtils::SQLQueryDataBase(const PMString& StringConnection,const PMString& SentenciaSQL, K2Vector<PMString>& QueryVector)
 {
 	
	
	//PMString StringConnection2="DRIVER={MySQL ODBC 3.51 Driver};SERVER=192.168.213.23;DATA SOURCE=n2psql2008;UID=acceso;PWD=1;OPTION=3;PORT=3306;SOCKET=/tmp/mysql.sock;";
 	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(SQLEjecutaQuerySentencia( stmt, SentenciaSQL, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			SQLDisconnect(con);
			SQLFreeHandle(SQL_HANDLE_DBC, con);
			SQLFreeHandle(SQL_HANDLE_ENV, env);
			//CAlert::InformationAlert("XXX");
			retval=kFalse;
		}
	}
	else
	{
		//CAlert::InformationAlert("ZZZ");
		retval=kFalse;
	}
	return retval;
 }
 
 const bool16 N2PSQLUtils::ExecProcedure(const PMString& StringConnection,const PMString& SentenciaSQL)
 {



 	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedure( stmt, SentenciaSQL))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
 }
 
 
 
 bool16 N2PSQLUtils::ejecutaStoreProcedure(SQLHANDLE& stmt,PMString Sentencia)
 {
 
 	
 	Sentencia="{call Fernando(?)}";
 	
 	K2Vector<PMString> VectorCampos;
 	
 	VectorCampos.push_back("Valor:1©TypeCampo:SQL_INTEGER©ISINPUT:1©");
 	VectorCampos.push_back("Valor:Fernando©TypeCampo:SQL_VARCHAR©ISINPUT:1©");
 	VectorCampos.push_back("Valor:loco©TypeCampo:SQL_VARCHAR©ISINPUT:0©");
 	
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;

	SQLCHAR sqlState[6];
	SQLINTEGER nativeError;
	SQLCHAR errorMsg[MAX_BUF_LEN];







	
	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	
	
	SQLCHAR Return[MAX_BUF_LEN]={'F','e','r','n','a','n','d','o'};
	
	



	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];//MAX_BUF_LEN
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 
		K2Vector<SQLSCHAR*> vc;
		
		//SQLCHAR OutStringArray[VectorCampos.Length()][MAX_BUF_LEN];
		long* LenOutStringArray=(long *) malloc(VectorCampos.Length() * sizeof(long));
		//SQLCHAR OutString2[MAX_BUF_LEN];
  		//SQLINTEGER cbOutString=SQL_NTS;
  		long cbReturnCode=SQL_NTS;
  		
  		for(int32 i=0;i<VectorCampos.Length();i++)
  		{
  			
  			PMString Valor=this->ReturnItemContentbyNameColumn("Valor", VectorCampos[i]);
  			PMString TypeDato=this->ReturnItemContentbyNameColumn("TypeCampo", VectorCampos[i]);
  			PMString EsEntrada=this->ReturnItemContentbyNameColumn("ISINPUT", VectorCampos[i]);
  			
  			
  			CAlert::InformationAlert("1=" + Valor);
  			
  			SQLSCHAR cad[MAX_BUF_LEN];
  			strcpy((char *)&cad, Valor.GrabCString());
  			
  			CAlert::InformationAlert((char *)&cad);
  			
  			vc.push_back(cad);
  			
  			CAlert::InformationAlert((char *)&vc[i]);
  			
  			LenOutStringArray[i] =Valor.NumUTF16TextChars();
  			
  			//Datos tipo entradas
  			if(EsEntrada=="1")
  			{
  				if(TypeDato=="SQL_INTEGER")
  				{
  					SQLINTEGER intCampo=Valor.GetAsNumber();
  					rc = SQLBindParameter(stmt, i+1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &intCampo,0,&cbReturnCode);
					if(rc!=SQL_SUCCESS)
					{
						return(kFalse);
					}
  				}
  				else
  				{
  					if(TypeDato=="SQL_VARCHAR")
  					{
  						
  						rc = SQLBindParameter(stmt,i+1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, MAX_BUF_LEN, 0, &vc[i],MAX_BUF_LEN,&LenOutStringArray[i]);
						if(rc!=SQL_SUCCESS)
						{
			
							return(kFalse);
			
						}
						
  					}
  				}
  			}
  			else
  			{	//Es salido
  				
  				if(TypeDato=="SQL_INTEGER")
  				{
  					SQLINTEGER intCampo=Valor.GetAsNumber();
  					rc = SQLBindParameter(stmt, i+1, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &intCampo,0,&cbReturnCode);
					if(rc!=SQL_SUCCESS)
					{
						return(kFalse);
					}
  				}
  				else
  				{
  					if(TypeDato=="SQL_VARCHAR")
  					{
  						
  						rc = SQLBindParameter(stmt,i+1, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, MAX_BUF_LEN, 0, &vc[i],MAX_BUF_LEN, &LenOutStringArray[i]);
						if(rc!=SQL_SUCCESS)
						{
							return(kFalse);
			
						}
  					}
  				}
  			}
  		}
		
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{
			for(int32 s=0;s<vc.Length();s++)
				CAlert::InformationAlert((char *)&vc[s]);
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="numero de error 22:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		//Sentencia=(char *)OutString;
		
		//rc = SQLExecDirect(stmt, sql, SQL_NTS);
	/*	CAlert::InformationAlert("4");
		
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{
		
			CAlert::InformationAlert("5");
			while((rc = SQLFetch(stmt)) == SQL_SUCCESS)
			{
				PMString Dato="";
				//rc = SQLGetData(stmt, 1,SQL_C_CHAR,szName,MAX_BUF_LEN,&cbName);
				SQLGetData(stmt, 1, SQL_C_CHAR, szName, MAX_BUF_LEN, &cbName);
				Dato.Append(printBytes("ninguno", (char *)szName,(int)cbName));
				Dato.Append("©");
				
				SQLGetData(stmt, 2,SQL_C_CHAR,szName,MAX_BUF_LEN,&cbName);
				Dato.Append(printBytes("ninguno", (char *)szName,(int)cbName));
				Dato.Append("©");
				CAlert::InformationAlert(Dato);
				//SQLGetData(stmt, i, SQL_C_CHAR, szName, MAX_BUF_LEN, &cbName);
			}
			
			/*SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
		
			PMString Erroi="numero de error 12:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);
			retval=kFalse;
			CAlert::InformationAlert(Erroi); ///
		}
		else
		{
		
			CAlert::InformationAlert("5_1");
			
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			CAlert::InformationAlert("5_2");
			
			PMString Erroi="numero de error 22:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			CAlert::InformationAlert("5_3");
			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
			return(kFalse);
		}
		
		
		PMString Erroi="Fernando:";
			Erroi.AppendNumber(nchecking);
			CAlert::InformationAlert(Erroi);
		*/
		//SQLFreeHandle(SQL_HANDLE_STMT,stmt);
		/*rc = SQLExecDirect(stmt, sql, SQL_NTS);

		*/
	}
	
	return(kTrue);
 }
 
 
 
 
 
 
 bool16 N2PSQLUtils::SQLInicializaConexion(const PMString& StringConnection,SQLHANDLE& stmt,SQLHANDLE& con,SQLHANDLE& env)
 {
 	bool16 retval=kFalse;
	SQLRETURN  rc;
    
    
	/* replace this string with your DSN, User ID and password ""*/
	SQLCHAR  dsn[MAX_BUF_LEN]; //(unsigned char)StringConnection.GrabCString(); 
	
	//for(int32 i=0;i<StringConnection.NumUTF16TextChars();i++)
	//	 dsn[i] = (StringConnection.GetChar(i)).GetAsOneByteChar();
	//CAlert::InformationAlert(StringConnection);
	strcpy((char *) dsn, StringConnection.GrabCString());
	//SQLCHAR  dsn[] = "DSN=SQLServer;UID=Fernando;PWD=falcon";
	 
/*	 SQLWCHAR * dsn="";
	 char *sourStr = StringConnection.GrabCString();
	 
	 size_t length;

  if (!sourStr || !destStr)
    return destStr;

  length = strlen (sourStr);
  if (length > 0)
    OPL_A2W (sourStr, destStr, length);
  destStr[length] = L'\0';*/
  
  
  
	
	
	//PMString Cons="Prop:";
	//Cons.Append(*dsn);
	
	
	SQLCHAR  outDsn[MAX_BUF_LEN];
	SQLSMALLINT  bufLen;
	SQLCHAR sqlState[6];
	SQLINTEGER nativeError;
	SQLCHAR errorMsg[MAX_BUF_LEN];
	
	/* Allocate An Environment Handle */
    rc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);

	if (rc == SQL_SUCCESS)
	{
		rc = SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (SQLPOINTER) SQL_OV_ODBC2, 0);
	}
	else
	{
		if (rc != SQL_SUCCESS)
		{
			SQLGetDiagRec(SQL_HANDLE_DBC, con, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			PMString Erroi="A numero de error:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);
			
			CAlert::InformationAlert( Erroi);
			
			//fprintf(stderr, "%s\n", errorMsg);
		}
	}

	/* Allocate A Connection Handle */
	if (rc == SQL_SUCCESS)
	{
		rc = SQLAllocHandle(SQL_HANDLE_DBC, env, &con);
	}
	else
	{
		if (rc != SQL_SUCCESS)
		{
			SQLGetDiagRec(SQL_HANDLE_DBC, con, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			PMString Erroi="B numero de error:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);
			
			CAlert::InformationAlert( Erroi);
			
			//fprintf(stderr, "%s\n", errorMsg);
		}
	}

	/* connect to the database */
	if (rc == SQL_SUCCESS)
	{
		rc = SQLDriverConnect (con, NULL, (SQLCHAR *)dsn, SQL_NTS, 
								outDsn, MAX_BUF_LEN, &bufLen, SQL_DRIVER_COMPLETE_REQUIRED);

		if (rc != SQL_SUCCESS)
		{
			SQLGetDiagRec(SQL_HANDLE_DBC, con, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			if(nativeError!=0)
			{
				PMString Erroi="AQUI PAPAPAAAPAPA PASA ALGO:";
				Erroi.AppendNumber(nativeError);
				Erroi.Append("\n");
				Erroi.Append(StringConnection);
				Erroi.Append("\n");
				for(int i=0;i<bufLen;i++)
				Erroi.Append((char)errorMsg[i]);
			
				//CAlert::InformationAlert( Erroi);
			}
			
			
			//fprintf(stderr, "%s\n", errorMsg);
		}
	}

	if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
	{
		rc = SQL_SUCCESS;
	}

	if (rc == SQL_SUCCESS)
	{
		/* Allocate An SQL Statement Handle */
		rc = SQLAllocHandle(SQL_HANDLE_STMT, con, &stmt);
	}
	
	
	if(rc==SQL_SUCCESS)
		retval=kTrue;
	else
		retval=kFalse;
	
	return retval;
 
 }
 
 bool16 N2PSQLUtils::SQLEjecutaSimpleSentencia(SQLHANDLE& stmt,PMString Sentencia)
 {
 
 	SQLRETURN  rc = SQL_SUCCESS;
 	SQLCHAR sqlState[6];
	SQLCHAR errorMsg[MAX_BUF_LEN];
 	SQLINTEGER nativeError;
 	SQLSMALLINT  bufLen;
 	
 	bool16 retval=kFalse;
 	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
	strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLExecDirect(stmt, sql, SQL_NTS);

		if (rc != SQL_SUCCESS)
		{
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			PMString Erroi="numero de error:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);
			
			CAlert::InformationAlert(Erroi);
		}
	}
	
	if (rc == SQL_SUCCESS)
	{
		retval=kTrue;
	}
	else
	{
		retval=kFalse;
	}
	return retval;
}
 
 bool16 N2PSQLUtils::SQLEjecutaQuerySentencia(SQLHANDLE& stmt,PMString Sentencia,K2Vector<PMString>& QueryVector)
{
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	int done;
	SQLCHAR sqlState[6];
	SQLINTEGER nativeError;
	SQLCHAR errorMsg[MAX_BUF_LEN];
	SQLCHAR colName[MAX_BUF_LEN];
	SQLSMALLINT colNameLen;
	SQLSMALLINT sqlType;
	long unsigned colDef;
	SQLSMALLINT ibScale;
	SQLSMALLINT nullable;
	SQLSMALLINT NumCols; //numero de columnas de consulta
	SQLCHAR szName[MAX_BUF_LEN];
	long cbName;
	
	
	
	//if(debug)CAlert::InformationAlert("SQLEjecutaQuerySentencia 1");
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLExecDirect(stmt, sql, SQL_NTS);
		
		if (rc != SQL_SUCCESS)
		{
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			PMString Erroi="numero de error 12:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);
			retval=kFalse;
			CAlert::InformationAlert(Sentencia + "\n" + Erroi);
		}
	}
	
	//if(debug)CAlert::InformationAlert("SQLEjecutaQuerySentencia 2");
	
	rc = SQLNumResultCols(stmt, &NumCols );
	
	
	//if(debug)CAlert::InformationAlert("SQLEjecutaQuerySentencia 3");
	
	if (rc == SQL_SUCCESS)
	{
		rc = SQLDescribeCol(stmt, 1, colName, MAX_BUF_LEN, &colNameLen, &sqlType, &colDef, &ibScale, &nullable);
		
		if (rc != SQL_SUCCESS )
		{
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
			
			PMString Erroi="numero de error 13:";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);
			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
			//fprintf(stderr, "%s\n", errorMsg);
		}
	}
	
	//if(debug)CAlert::InformationAlert("SQLEjecutaQuerySentencia 3");
	
	// fetch the table rows 
	if (rc == SQL_SUCCESS)
	{
		done = FALSE;
		
		while (!done)
		{
			rc = SQLFetch(stmt);
			
			if (rc == SQL_NO_DATA)
			{
				done = TRUE;
			}
			else 
			{
				if (rc != SQL_SUCCESS)
				{
					done = TRUE;
				}
				else
				{
					PMString Dato=PMString("", PMString::kDontTranslateDuringCall);
					for(int i=1;i<=NumCols;i++)
					{
						SQLDescribeCol(stmt, i, colName, MAX_BUF_LEN, &colNameLen, &sqlType, &colDef, &ibScale, &nullable);
						if(colDef>0)
						{
							Dato.Append(printBytes("ninguno", (char *)colName,(int)colNameLen));
							Dato.Append(":");
							SQLGetData(stmt, i, SQL_C_CHAR, szName, MAX_BUF_LEN, &cbName);
							if(cbName>0)
							{
								PMString MKL=PMString("", PMString::kDontTranslateDuringCall);
								
								MKL.Append(printBytes("ninguno", (char *)szName,(int)cbName));
								UTF32TextChar ppf=MKL.GetWChar(0);
								MKL="";
								MKL.AppendNumber(ppf.GetValue());
								
								if(ppf.GetValue()>0){
									//if(debug)CAlert::InformationAlert("Agrega Campo");
									Dato.Append(printBytes("ninguno", (char *)szName,(int)cbName));
								}
								
								
							}
							//if(debug)CAlert::InformationAlert("AASS:"+Dato);
							Dato.Append("©");
							Dato.SetTranslatable(kFalse);
							
							//if(debug)CAlert::InformationAlert("AASS2:"+Dato);
							
						}
						
					}
					//CAlert::InformationAlert(Dato);
					QueryVector.push_back(Dato);
				}
			}	
		}
		retval=kTrue;
	}
	
	//if(debug)CAlert::InformationAlert("SQLEjecutaQuerySentencia 5");
	
 	return retval;
}
 
 
 PMString N2PSQLUtils::printBytes(char* fieldName, char* field, int fieldLen)
{
	int i;
	PMString Cad="";
	//Cad.Append(fieldName);
	//Cad.Append(",");
	//Cad.AppendNumber(fieldLen);
	//Cad.Append(",");
	//fprintf(stdout, "%s[%d]: ", fieldName, fieldLen);
	
	for (i=0; i<fieldLen; i++)
	{
		Cad.Append( (unsigned char) field[i]);
		//fprintf(stdout, "%x ", (unsigned char) field[i]);
	}
	Cad.StripWhiteSpace(PMString::kTrailingWhiteSpace);
	return(Cad);
}





const bool16 N2PSQLUtils::StoreProcedureCheckInPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureCheckInPagina( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}

 bool16 N2PSQLUtils::ejecutaStoreProcedureCheckInPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
 /*	CREATE PROCEDURE CheckInPagina*/
 	PMString Sentencia="{call PaginaCheckInStoreProc(?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?, ?, ?, ? , ?)}";
//	Id_Publicacion VARCHAR(50), 
	SQLCHAR Id_Publicacion[MAX_BUF_LEN];
	strcpy((char *) &Id_Publicacion, QueryVector[0].GrabCString());
	long LenId_Publicacion=QueryVector[0].NumUTF16TextChars();
	
//	Fecha_Edicion VARCHAR(50),

	SQLCHAR Fecha_Edicion[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Edicion, QueryVector[1].GrabCString());
	long LenFecha_Edicion=QueryVector[1].NumUTF16TextChars();
	
//	Id_Seccion  VARCHAR(50),
	SQLCHAR Id_Seccion[MAX_BUF_LEN];
	strcpy((char *) &Id_Seccion, QueryVector[2].GrabCString());
	long LenId_Seccion=QueryVector[2].NumUTF16TextChars();
	
//	Folio_Pagina  VARCHAR(50),
 	SQLCHAR Folio_Pagina[MAX_BUF_LEN];
 	strcpy((char *) &Folio_Pagina, QueryVector[3].GrabCString());
 	long LenFolio_Pagina=QueryVector[3].NumUTF16TextChars();
 	
//	Pliego int, 
	SQLINTEGER Pliego;
	Pliego = QueryVector[4].GetAsNumber();
	
//	Par_Impar int,
 	SQLINTEGER Par_Impar;
 	Par_Impar = QueryVector[5].GetAsNumber();
 	
//	Id_Color VARCHAR(50),
	SQLCHAR Id_Color[MAX_BUF_LEN];
	strcpy((char *) &Id_Color, QueryVector[6].GrabCString());
	long LenId_Color=QueryVector[6].NumUTF16TextChars();
	
//	Dirigido_a VARCHAR(50),
 	SQLCHAR Dirigido_a[MAX_BUF_LEN];
 	strcpy((char *) &Dirigido_a, QueryVector[7].GrabCString());
 	long LenDirigido_a=QueryVector[7].NumUTF16TextChars();
 	
//	Proveniente_de VARCHAR(50),
 	SQLCHAR Proveniente_de[MAX_BUF_LEN];
 	strcpy((char *) &Proveniente_de, QueryVector[8].GrabCString());
 	long LenProveniente_de=QueryVector[8].NumUTF16TextChars();
 	
//	Servidor VARCHAR(50),
 	SQLCHAR Servidor[MAX_BUF_LEN];
 	strcpy((char *) &Servidor, QueryVector[9].GrabCString());
 	long LenServidor=QueryVector[9].NumUTF16TextChars();
 	
//	Ruta_Elemento VARCHAR(500),
 	SQLCHAR Ruta_Elemento[MAX_BUF_LEN];
 	strcpy((char *) &Ruta_Elemento, QueryVector[10].GrabCString());
 	long LenRuta_Elemento=QueryVector[10].NumUTF16TextChars();

//	Nombre_Archivo VARCHAR(50),
 	SQLCHAR Nombre_Archivo[MAX_BUF_LEN];
 	strcpy((char *) &Nombre_Archivo, QueryVector[11].GrabCString());
 	long LenNombre_Archivo=QueryVector[11].NumUTF16TextChars();
 	
//	Fecha_Creacion VARCHAR (50) OUTPUT,
 	SQLCHAR Fecha_Creacion[MAX_BUF_LEN];
 	strcpy((char *) &Fecha_Creacion, QueryVector[12].GrabCString());
 	long LenFecha_Creacion=QueryVector[12].NumUTF16TextChars();
 	
//	Fecha_Ult_Mod VARCHAR (50) OUTPUT,
 	SQLCHAR Fecha_Ult_Mod[MAX_BUF_LEN];
 	strcpy((char *) &Fecha_Ult_Mod, QueryVector[13].GrabCString());
 	long LenFecha_Ult_Mod=QueryVector[13].NumUTF16TextChars();
 	
//	Webable int, 
 	SQLINTEGER Webable;
 	Webable = QueryVector[14].GetAsNumber();
 	
//	Calificacion int, 
 	SQLINTEGER Calificacion;
 	Calificacion = QueryVector[15].GetAsNumber();
 	
//	Id_Tipo_Elemento VARCHAR(50),
 	SQLCHAR Id_Tipo_Elemento[MAX_BUF_LEN];
 	strcpy((char *) &Id_Tipo_Elemento, QueryVector[16].GrabCString());
 	long LenId_Tipo_Elemento=QueryVector[16].NumUTF16TextChars();
 	
//	Id_Estatus VARCHAR(50),
 	SQLCHAR Id_Estatus[MAX_BUF_LEN];
 	strcpy((char *) &Id_Estatus, QueryVector[17].GrabCString());
 	long LenId_Estatus=QueryVector[17].NumUTF16TextChars();
 	
//	Cameo VARCHAR(50),
 	SQLCHAR Cameo[MAX_BUF_LEN];
 	strcpy((char *) &Cameo, QueryVector[18].GrabCString());
 	long LenCameo=QueryVector[18].NumUTF16TextChars();
 	
//	IDEmpleado VARCHAR(50),
	SQLCHAR IDEmpleado[MAX_BUF_LEN];
	strcpy((char *) &IDEmpleado, QueryVector[19].GrabCString());
	long LenIDEmpleado=QueryVector[19].NumUTF16TextChars();
	
//	Fecha_Evento  VARCHAR(50),
	SQLCHAR Fecha_Evento[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Evento, QueryVector[20].GrabCString());
	long LenFecha_Evento=QueryVector[20].NumUTF16TextChars();

//	Id_Evento int,
 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[21].GetAsNumber();
 	
//	RutaPreview VARCHAR(500),
	SQLCHAR RutaPreview[MAX_BUF_LEN];
	strcpy((char *) &RutaPreview, QueryVector[22].GrabCString());
	long LenRutaPreview=QueryVector[22].NumUTF16TextChars();
	
//	Descripcion_Evento VARCHAR(100)
	SQLCHAR Descripcion_Evento[MAX_BUF_LEN];
	strcpy((char *) &Descripcion_Evento, QueryVector[23].GrabCString());
	long LenDescripcion_Evento=QueryVector[23].NumUTF16TextChars();
	
	SQLCHAR Ruta_lastPrview[MAX_BUF_LEN];
	strcpy((char *) &Ruta_lastPrview, QueryVector[24].GrabCString());
	long LenRuta_lastPrview=QueryVector[24].NumUTF16TextChars();
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 

		
		
		
  		long cbReturnCode=SQL_NTS;
  		
  

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Publicacion,0,&LenId_Publicacion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Edicion,0,&LenFecha_Edicion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Seccion,0,&LenId_Seccion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Folio_Pagina,0,&LenFolio_Pagina);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  			
  	
  		rc = SQLBindParameter(stmt, 5, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Pliego,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		
  		rc = SQLBindParameter(stmt, 6, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Par_Impar,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 7, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Color,0,&LenId_Color);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 8, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Dirigido_a,0,&LenDirigido_a);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 9, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Proveniente_de,0,&LenProveniente_de);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		
  		rc = SQLBindParameter(stmt, 10, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Servidor,0,&LenServidor);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 11, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Ruta_Elemento,0,&LenRuta_Elemento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 12, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Nombre_Archivo,0,&LenNombre_Archivo);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 13, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Creacion,MAX_BUF_LEN,&LenFecha_Creacion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 14, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Ult_Mod,MAX_BUF_LEN,&LenFecha_Ult_Mod);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 15, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Webable,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 16, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Calificacion,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 17, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Tipo_Elemento,0,&LenId_Tipo_Elemento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 18, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Estatus,0,&LenId_Estatus);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 19, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Cameo,0,&LenCameo);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
	
		rc = SQLBindParameter(stmt, 20, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &IDEmpleado,0,&LenIDEmpleado);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 21, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Evento,0,&LenFecha_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 22, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 23, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &RutaPreview,0,&LenRutaPreview);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
	
		rc = SQLBindParameter(stmt, 24, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Descripcion_Evento,0,&LenDescripcion_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 25, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Ruta_lastPrview,0,&LenRuta_lastPrview);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{
			
				PMString Result="";
				Result=(char *)Fecha_Creacion;
				if(Result=="1")
				{
					CAlert::InformationAlert(kN2PInOutYaExistePaginaConEsteFolioStringAlert);
					retval=kFalse;
				}
				else
				{
					if(Result=="SinPrivilegios")
					{
						CAlert::InformationAlert(kN2PInOutUserSinPriviPCheckInPageStringKey);
						retval=kFalse;
					}
					else
					{
						retval=kTrue;
					}
					
				}
				
		}
		else
		{
		
			PMString Result="";
			Result=(char *)Fecha_Creacion;
			CAlert::InformationAlert(Result);	
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureCheckInPagina numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }
 

const bool16 N2PSQLUtils::StoreProcedureSaveRevPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureSaveRevPagina( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}
 
bool16 N2PSQLUtils::ejecutaStoreProcedureSaveRevPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
 /*	CREATE PROCEDURE CheckInPagina*/
 	PMString Sentencia="{call PaginaSaveRevStoreProc(?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ? , ?)}";
//	Id_Publicacion VARCHAR(50), 
	SQLCHAR Id_Publicacion[MAX_BUF_LEN];
	strcpy((char *) &Id_Publicacion, QueryVector[0].GrabCString());
	long LenId_Publicacion=QueryVector[0].NumUTF16TextChars();
	
//	Fecha_Edicion VARCHAR(50),

	SQLCHAR Fecha_Edicion[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Edicion, QueryVector[1].GrabCString());
	long LenFecha_Edicion=QueryVector[1].NumUTF16TextChars();
	
//	Id_Seccion  VARCHAR(50),
	SQLCHAR Id_Seccion[MAX_BUF_LEN];
	strcpy((char *) &Id_Seccion, QueryVector[2].GrabCString());
	long LenId_Seccion=QueryVector[2].NumUTF16TextChars();
	
//	Folio_Pagina  VARCHAR(50),
 	SQLCHAR Folio_Pagina[MAX_BUF_LEN];
 	strcpy((char *) &Folio_Pagina, QueryVector[3].GrabCString());
 	long LenFolio_Pagina=QueryVector[3].NumUTF16TextChars();
 	

//	Dirigido_a VARCHAR(50),
 	SQLCHAR Dirigido_a[MAX_BUF_LEN];
 	strcpy((char *) &Dirigido_a, QueryVector[4].GrabCString());
 	long LenDirigido_a=QueryVector[4].NumUTF16TextChars();
 	
//	Proveniente_de VARCHAR(50),
 	SQLCHAR Proveniente_de[MAX_BUF_LEN];
 	strcpy((char *) &Proveniente_de, QueryVector[5].GrabCString());
 	long LenProveniente_de=QueryVector[5].NumUTF16TextChars();
 
//	Ruta_Elemento VARCHAR(500),
 	SQLCHAR Ruta_Elemento[MAX_BUF_LEN];
 	strcpy((char *) &Ruta_Elemento, QueryVector[6].GrabCString());
 	long LenRuta_Elemento=QueryVector[6].NumUTF16TextChars();

//	Fecha_Ult_Mod VARCHAR (50) OUTPUT,
 	SQLCHAR Fecha_Ult_Mod[MAX_BUF_LEN];
 	strcpy((char *) &Fecha_Ult_Mod, QueryVector[7].GrabCString());
 	long LenFecha_Ult_Mod=QueryVector[7].NumUTF16TextChars();
 	
//	Id_Estatus VARCHAR(50),
 	SQLCHAR Id_Estatus[MAX_BUF_LEN];
 	strcpy((char *) &Id_Estatus, QueryVector[8].GrabCString());
 	long LenId_Estatus=QueryVector[8].NumUTF16TextChars();

//	IDEmpleado VARCHAR(50),
	SQLCHAR IDEmpleado[MAX_BUF_LEN];
	strcpy((char *) &IDEmpleado, QueryVector[9].GrabCString());
	long LenIDEmpleado=QueryVector[9].NumUTF16TextChars();
	
//	Fecha_Evento  VARCHAR(50),
	SQLCHAR Fecha_Evento[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Evento, QueryVector[10].GrabCString());
	long LenFecha_Evento=QueryVector[10].NumUTF16TextChars();

//	Id_Evento int,
 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[11].GetAsNumber();
 	
//	RutaPreview VARCHAR(500),
	SQLCHAR RutaPreview[MAX_BUF_LEN];
	strcpy((char *) &RutaPreview, QueryVector[12].GrabCString());
	long LenRutaPreview=QueryVector[12].NumUTF16TextChars();
	
//	Descripcion_Evento VARCHAR(100)
	SQLCHAR Descripcion_Evento[MAX_BUF_LEN];
	strcpy((char *) &Descripcion_Evento, QueryVector[13].GrabCString());
	long LenDescripcion_Evento=QueryVector[13].NumUTF16TextChars();
	
 	//	Descripcion_Evento VARCHAR(100)
	SQLCHAR Ruta_lastPrview[MAX_BUF_LEN];
	strcpy((char *) &Ruta_lastPrview, QueryVector[14].GrabCString());
	long LenRuta_lastPrview=QueryVector[14].NumUTF16TextChars();

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 
		 		
  		long cbReturnCode=SQL_NTS;
  		

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Publicacion,0,&LenId_Publicacion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Edicion,0,&LenFecha_Edicion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Seccion,0,&LenId_Seccion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Folio_Pagina,0,&LenFolio_Pagina);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  			
  	 			
  		rc = SQLBindParameter(stmt, 5, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Dirigido_a,0,&LenDirigido_a);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Proveniente_de,0,&LenProveniente_de);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		  		 		
  		rc = SQLBindParameter(stmt, 7, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Ruta_Elemento,0,&LenRuta_Elemento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		
  		rc = SQLBindParameter(stmt, 8, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Ult_Mod,MAX_BUF_LEN,&LenFecha_Ult_Mod);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 9, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Estatus,0,&LenId_Estatus);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 10, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &IDEmpleado,0,&LenIDEmpleado);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 11, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Evento,0,&LenFecha_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 12, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 13, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &RutaPreview,0,&LenRutaPreview);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
	
		rc = SQLBindParameter(stmt, 14, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Descripcion_Evento,0,&LenDescripcion_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 15, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Ruta_lastPrview,0,&LenRuta_lastPrview);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{
				PMString result = (char *)Fecha_Ult_Mod;
				if(result=="SinPrivilegios")
				{
					retval=kFalse;
				}
				else
				{
					//CAlert::InformationAlert((char *)Fecha_Ult_Mod);
					retval=kTrue;
				}
				
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureSaveRevPagina numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }
 
 
 
const bool16 N2PSQLUtils::StoreProcedureCheckOutPage(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureCheckOutPagina( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}
 
bool16 N2PSQLUtils::ejecutaStoreProcedureCheckOutPagina(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
 /*	CREATE PROCEDURE CheckInPagina*/
 	PMString Sentencia="{call  PaginaCheckOutStoreProc(?, ?, ?,?, ?, ?,?, ?,?)}";
//	Id_Publicacion VARCHAR(50), 
	SQLCHAR Id_Publicacion[MAX_BUF_LEN];
	strcpy((char *) &Id_Publicacion, QueryVector[0].GrabCString());
	long LenId_Publicacion=QueryVector[0].NumUTF16TextChars();
	
//	Fecha_Edicion VARCHAR(50),

	SQLCHAR Fecha_Edicion[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Edicion, QueryVector[1].GrabCString());
	long LenFecha_Edicion=QueryVector[1].NumUTF16TextChars();
	
//	Id_Seccion  VARCHAR(50),
	SQLCHAR Id_Seccion[MAX_BUF_LEN];
	strcpy((char *) &Id_Seccion, QueryVector[2].GrabCString());
	long LenId_Seccion=QueryVector[2].NumUTF16TextChars();
	
//	Folio_Pagina  VARCHAR(50),
 	SQLCHAR Folio_Pagina[MAX_BUF_LEN];
 	strcpy((char *) &Folio_Pagina, QueryVector[3].GrabCString());
 	long LenFolio_Pagina=QueryVector[3].NumUTF16TextChars();
 	

//	IDEmpleado VARCHAR(50),
	SQLCHAR IDEmpleado[MAX_BUF_LEN];
	strcpy((char *) &IDEmpleado, QueryVector[4].GrabCString());
	long LenIDEmpleado=QueryVector[4].NumUTF16TextChars();
	
//	Fecha_Evento  VARCHAR(50),
	SQLCHAR Fecha_Evento[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Evento, QueryVector[5].GrabCString());
	long LenFecha_Evento=QueryVector[5].NumUTF16TextChars();

//	Id_Evento int,
 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[6].GetAsNumber();

//	Descripcion_Evento VARCHAR(100)
	SQLCHAR Descripcion_Evento[MAX_BUF_LEN];
	strcpy((char *) &Descripcion_Evento, QueryVector[7].GrabCString());
	long LenDescripcion_Evento=QueryVector[7].NumUTF16TextChars();
	
 	SQLCHAR Resultado[MAX_BUF_LEN];
	strcpy((char *) &Resultado, "");
	long LenResultado= MAX_BUF_LEN;
	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 
		 		
  		long cbReturnCode=SQL_NTS;
  		

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Publicacion,0,&LenId_Publicacion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Edicion,0,&LenFecha_Edicion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Seccion,0,&LenId_Seccion);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Folio_Pagina,0,&LenFolio_Pagina);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  			
  	 			
  		
		
		rc = SQLBindParameter(stmt, 5, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &IDEmpleado,0,&LenIDEmpleado);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Evento,0,&LenFecha_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 7, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
		rc = SQLBindParameter(stmt, 8, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Descripcion_Evento,0,&LenDescripcion_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 9, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Resultado,MAX_BUF_LEN,&LenResultado);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{
			
				PMString Result=(char *)Resultado;
				if(Result=="SinPrivilegios")
				{
					CAlert::InformationAlert(kN2PInOutUserSinPriviPCheckOutPageStringKey);
					retval=kFalse;
				}
				else
				{
					retval=kTrue;
				}
				
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureCheckOutPagina numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }
 
const bool16 N2PSQLUtils::StoreProcedureLOGINUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	PMString StringConnection2="DRIVER={MySQL ODBC 3.51 Driver};SERVER=192.168.213.23;DATA SOURCE=n2psql2008;UID=acceso;PWD=1;OPTION=3;PORT=3306;SOCKET=/tmp/mysql.sock;";
 
 
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	
	if(SQLInicializaConexion(StringConnection2,stmt,con,env))
	{
		if(ejecutaStoreProcedureLOGINUSER( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}
 
bool16 N2PSQLUtils::ejecutaStoreProcedureLOGINUSER(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
 	CAlert::InformationAlert("HAY MAMA jalas o no jalas we");
 /*	CREATE PROCEDURE CheckInPagina*/
 	PMString Sentencia="call LoginN2P( ?, ?, ?, ?, ?);";
//	Id_Publicacion VARCHAR(50), 
	SQLCHAR Id_User[MAX_BUF_LEN];
	strcpy((char *) &Id_User, QueryVector[0].GrabCString());
	long LenId_User=QueryVector[0].NumUTF16TextChars();
	
//	Fecha_Edicion VARCHAR(50),
		//	Id_Evento int,
 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[1].GetAsNumber();
 	
//	Password
	SQLCHAR Password[MAX_BUF_LEN];
	strcpy((char *) &Password, QueryVector[2].GrabCString());
	long LenPassword=QueryVector[2].NumUTF16TextChars();
	

	
//	Fecha_Evento  VARCHAR(50),
	SQLCHAR Fecha_Evento[MAX_BUF_LEN];
	//strcpy((char *) &Fecha_Evento, QueryVector[3].GrabCString());
	long LenFecha_Evento=0;

//	Id_Evento int,
 	SQLINTEGER resultado;
 	resultado = QueryVector[4].GetAsNumber();


	
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		CAlert::InformationAlert("DEWWWQ");
		
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			CAlert::InformationAlert("ZXS");
			return(kFalse);
			
		}
		 
		 		
  		long cbReturnCode=SQL_NTS;
  		long cbReturnCode2=sizeof( long );
  		

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_User,0,&LenId_User);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
  		rc = SQLBindParameter(stmt, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Password,0,&LenPassword);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		
		rc = SQLBindParameter(stmt, 
								4, 
								SQL_PARAM_INPUT, 
								SQL_C_LONG, 
								SQL_INTEGER, 
								0, 
								0, 
								&resultado,
								sizeof( resultado ),
								&cbReturnCode2);
		//rc = SQLBindParameter(m_hstmt, 1, SQL_PARAM_INPUT_OUTPUT, SQL_C_LONG,SQL_INTEGER, sizeof(m_lOption), 0, &m_lOption, sizeof(m_lOption), &lcbon),  &lcbOption);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 5, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Evento,MAX_BUF_LEN,&LenFecha_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		
		
  			
		CAlert::InformationAlert("antes de ");
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{		
				PMString mm="";
				QueryVector[0]=(char *) Id_User;
				mm.AppendNumber(Id_Evento);
				QueryVector[1]=mm;
				QueryVector[2]=(char *) Password;
				
				mm="";
				mm.AppendNumber(resultado);
				QueryVector[3]=mm;
				QueryVector[4]=(char *) Fecha_Evento;
				CAlert::InformationAlert(QueryVector[4]);
				retval=kTrue;
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureLOGINUSER numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }
 
 const bool16 N2PSQLUtils::StoreProcedureLOGOUTUSER(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureLOGOUTUSER( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}
 
bool16 N2PSQLUtils::ejecutaStoreProcedureLOGOUTUSER(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
 /*	CREATE PROCEDURE CheckInPagina*/
 	PMString Sentencia="{call LOGOUTUSERN2P( ?, ?, ?, ?)}";
//	Id_Publicacion VARCHAR(50), 
	SQLCHAR Id_User[MAX_BUF_LEN];
	strcpy((char *) &Id_User, QueryVector[0].GrabCString());
	long LenId_User=QueryVector[0].NumUTF16TextChars();
	
//	Fecha_Edicion VARCHAR(50),
//	Id_Evento int,
 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[1].GetAsNumber();
 	

//	Fecha_Evento  VARCHAR(50),
	SQLCHAR Fecha_Evento[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Evento, QueryVector[2].GrabCString());
	long LenFecha_Evento=QueryVector[2].NumUTF16TextChars();

//	Id_Evento int,
 	SQLINTEGER resultado;
 	resultado = QueryVector[3].GetAsNumber();


	
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 
		 		
  		long cbReturnCode=SQL_NTS;
  		

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_User,0,&LenId_User);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 3, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Evento,MAX_BUF_LEN,&LenFecha_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		
		rc = SQLBindParameter(stmt, 4, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &resultado,MAX_BUF_LEN,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  			
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{		
			PMString mm="";
				QueryVector[0]=(char *) Id_User;
				mm.AppendNumber(Id_Evento);
				QueryVector[1]=mm;
				QueryVector[2]=(char *) Fecha_Evento;
				mm="";
				mm.AppendNumber(resultado);
				QueryVector[3]=mm;
				retval=kTrue;
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureLOGOUTUSER numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }

 
const bool16 N2PSQLUtils::StoreProcedureCHECKOUTNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureCHECKOUTNOTA( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}


 
bool16 N2PSQLUtils::ejecutaStoreProcedureCHECKOUTNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
 /*	CREATE PROCEDURE CheckInPagina*/
 	PMString Sentencia="{call NOTAN2PCHECKOUT( ?, ?, ? , ?, ? )}";

	SQLCHAR Id_Elem_Padre[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_Padre, QueryVector[0].GrabCString());
	long LenId_Elem_Padre=QueryVector[0].NumUTF16TextChars();

	SQLCHAR Id_User[MAX_BUF_LEN];
	strcpy((char *) &Id_User, QueryVector[1].GrabCString());
	long LenId_User=QueryVector[1].NumUTF16TextChars();
	
	
 	SQLCHAR CadenError[MAX_BUF_LEN];
	strcpy((char *) &CadenError, QueryVector[2].GrabCString());
	long LenCadenError=QueryVector[2].NumUTF16TextChars();

	
	
//	Fecha_Evento  VARCHAR(50),
	SQLCHAR Fecha_Evento[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Evento, QueryVector[3].GrabCString());
	long LenFecha_Evento=QueryVector[3].NumUTF16TextChars();

	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[4].GetAsNumber();


	
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 
		 		
  		long cbReturnCode=SQL_NTS;
  		

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_Padre,MAX_BUF_LEN,&LenId_Elem_Padre);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_User,MAX_BUF_LEN,&LenId_User);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		rc = SQLBindParameter(stmt, 3, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, MAX_BUF_LEN, 0, &CadenError,MAX_BUF_LEN,&LenCadenError);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		
		
		rc = SQLBindParameter(stmt, 4, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Evento,MAX_BUF_LEN,&LenFecha_Evento);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt,5, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}	
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{		
			
			QueryVector[0]=(char *) Id_Elem_Padre;
			QueryVector[1]=(char *) Id_User;
			QueryVector[2]=(char *) CadenError;
			QueryVector[3]=(char *) Fecha_Evento;
			retval=kTrue;
		}
		else
		{
		
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureCHECKOUTNOTA numero de error : ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }
 
 
 /****/
 const bool16 N2PSQLUtils::StoreProcedureCHECKINNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector, const bool16& NotaEnCheckOut)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureCHECKINNOTA( stmt, QueryVector, NotaEnCheckOut))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}
 
bool16 N2PSQLUtils::ejecutaStoreProcedureCHECKINNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector , const bool16& NotaEnCheckOut)
 {
  PMString Sentencia="";
 if(NotaEnCheckOut)
 {
 	Sentencia="{call NOTAN2PCHECKINMejorWithPases( ?, ?, ?, ?, ? ,?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?,   ?, ?, ?, ?, ? ,   ?, ?, ?, ?, ?,    ?, ?, ?, ?, ? ,   ?, ?, ?, ?, ? ,  ?, ?, ?, ? ,  ?,?,?,?)}";

 }
 else
 {
 	Sentencia="{call		  NOTAN2PCHECKINMejor( ?, ?, ?, ?, ? ,?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?,   ?, ?, ?, ?, ? ,   ?, ?, ?, ?, ?,    ?, ?, ?, ?, ? ,   ?, ?, ?, ?, ? ,  ?, ?, ?, ? ,  ?,?,?,?)}";

 }
 
 /*	CREATE PROCEDURE CheckInPagina
 			

@ID_Elem_Padre VARCHAR(50),				1
@Id_USUARIO VARCHAR(50), 				2
@DIRIGIDO_A VARCHAR(50),				3
@ESTATUS_ELE VARCHAR(30),				4
@ID_Elem_Cont_Note VARCHAR(30),			5
@Id_Elem_Tit_Note VARCHAR(30),			6
@Id_Elem_PieFoto_Note VARCHAR(30),		7
@Id_Elem_Balazo_Note VARCHAR(30),		8
@ContenidoNota VARCHAR(8000),			9
@TopContNota INTEGER,					10
@LeftContNota INTEGER,					11
@BottomContNota INTEGER,				12
@RigthContNota INTEGER,					13
@LineFaltantesContNota INTEGER,			14
@LineRestantesContNota INTEGER,			15
@CarFaltantesContNota INTEGER,			16
@CarRestantesContNota INTEGER,			17
@CarContNota INTEGER,					18
@ContenidoTitulo VARCHAR(8000),			19
@TopContTitulo INTEGER,					20
@LeftContTitulo INTEGER,				21
@BottomContTitulo INTEGER,				22
@RigthContTitulo INTEGER,				23
@LineFaltantesContTitulo INTEGER,		24
@LineRestantesContTitulo INTEGER,		25
@CarFaltantesContTitulo INTEGER,		26
@CarRestantesContTitulo INTEGER,		27
@CarContTitulo INTEGER,					28

@ContenidoBalazo VARCHAR(8000),			29
@TopContBalazo INTEGER,					30
@LeftContBalazo INTEGER,				31
@BottomContBalazo INTEGER,				32
@RigthContBalazo INTEGER,				33
@LineFaltantesContBalazo  INTEGER,		34
@LineRestantesContBalazo INTEGER,		35
@CarFaltantesContBalazo  INTEGER,		36
@CarRestantesContBalazo INTEGER,		37
@CarContBalazo  INTEGER,				38

@ContenidoPieFoto  VARCHAR(8000),		39
@TopContPieFoto INTEGER,				40
@LeftContPieFoto INTEGER,				41
@BottomContPieFoto INTEGER,				42
@RigthContPieFoto INTEGER,				43
@LineFaltantesContPieFoto  INTEGER,		44
@LineRestantesContPieFoto INTEGER,		45
@CarFaltantesContPieFoto  INTEGER,		46
@CarRestantesContPieFoto INTEGER,		47
@CarContPieFoto INTEGER,				48

@FechaCheckInParamOUT VARCHAR(50) OUTPUT 49

@Id_Evento INTEGER,				50

	*/

	SQLCHAR ID_Elem_Padre[MAX_BUF_LEN];
	strcpy((char *) &ID_Elem_Padre, QueryVector[0].GrabCString());
	long LenID_Elem_Padre=QueryVector[0].NumUTF16TextChars();
	
	SQLCHAR Id_USUARIO[MAX_BUF_LEN];
	strcpy((char *) &Id_USUARIO, QueryVector[1].GrabCString());
	long LenId_USUARIO=QueryVector[1].NumUTF16TextChars();
	
	
	SQLCHAR DIRIGIDO_A[MAX_BUF_LEN];
	strcpy((char *) &DIRIGIDO_A, QueryVector[2].GrabCString());
	long LenDIRIGIDO_A=QueryVector[2].NumUTF16TextChars();
	
	
	SQLCHAR ESTATUS_ELE[MAX_BUF_LEN];
	strcpy((char *) &ESTATUS_ELE, QueryVector[3].GrabCString());
	long LenESTATUS_ELE=QueryVector[3].NumUTF16TextChars();
	
	SQLCHAR ID_Elem_Cont_Note[MAX_BUF_LEN];
	strcpy((char *) &ID_Elem_Cont_Note, QueryVector[4].GrabCString());
	long LenID_Elem_Cont_Note=QueryVector[4].NumUTF16TextChars();
	
	SQLCHAR Id_Elem_Tit_Note[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_Tit_Note, QueryVector[5].GrabCString());
	long LenId_Elem_Tit_Note=QueryVector[5].NumUTF16TextChars();
	
	SQLCHAR Id_Elem_PieFoto_Note[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_PieFoto_Note, QueryVector[6].GrabCString());
	long LenId_Elem_PieFoto_Note=QueryVector[6].NumUTF16TextChars();
	
	SQLCHAR Id_Elem_Balazo_Note[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_Balazo_Note, QueryVector[7].GrabCString());
	long LenId_Elem_Balazo_Note=QueryVector[7].NumUTF16TextChars();
	
	SQLCHAR ContenidoNota[MAX_BUF_LEN];
	strcpy((char *) &ContenidoNota, QueryVector[8].GrabCString());
	long LenContenidoNota=QueryVector[8].NumUTF16TextChars();
	
	
	SQLFLOAT TopContNota;
 	TopContNota = QueryVector[9].GetAsDouble();

	/*PMString ss="";
	ss.AppendNumber(TopContNota,5,kFalse,kFalse);
	CAlert::InformationAlert(ss);
*/


	SQLFLOAT LeftContNota;
 	LeftContNota = QueryVector[10].GetAsDouble();
	
	
	SQLFLOAT BottomContNota;
 	BottomContNota = QueryVector[11].GetAsDouble();
 	
 	SQLFLOAT RigthContNota;
 	RigthContNota = QueryVector[12].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContNota;
 	LineFaltantesContNota = QueryVector[13].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContNota;
 	LineRestantesContNota = QueryVector[14].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContNota;
 	CarFaltantesContNota = QueryVector[15].GetAsNumber();
 	
 	
 	SQLINTEGER CarRestantesContNota;
 	CarRestantesContNota = QueryVector[16].GetAsNumber();
 	
 	SQLINTEGER CarContNota;
 	CarContNota = QueryVector[17].GetAsNumber();
 	
 	///////////	
	SQLCHAR ContenidoTitulo[MAX_BUF_LEN];
	strcpy((char *) &ContenidoTitulo, QueryVector[18].GrabCString());
	long LenContenidoTitulo=QueryVector[18].NumUTF16TextChars();
	
	
	SQLFLOAT TopContTitulo;
 	TopContTitulo = QueryVector[19].GetAsDouble();
	
	SQLFLOAT LeftContTitulo;
 	LeftContTitulo = QueryVector[20].GetAsDouble();
	
	
	SQLFLOAT BottomContTitulo;
 	BottomContTitulo = QueryVector[21].GetAsDouble();
 	
 	SQLFLOAT RigthContTitulo;
 	RigthContTitulo = QueryVector[22].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContTitulo;
 	LineFaltantesContTitulo = QueryVector[23].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContTitulo;
 	LineRestantesContTitulo = QueryVector[24].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContTitulo;
 	CarFaltantesContTitulo = QueryVector[25].GetAsNumber();
 	
 	
 	
 	SQLINTEGER CarRestantesContTitulo;
 	CarRestantesContTitulo = QueryVector[26].GetAsNumber();
 	
 	SQLINTEGER CarContTitulo;
 	CarContTitulo = QueryVector[27].GetAsNumber();
 	
 		///////////	
	SQLCHAR ContenidoBalazo[MAX_BUF_LEN];
	strcpy((char *) &ContenidoBalazo, QueryVector[28].GrabCString());
	long LenContenidoBalazo=QueryVector[28].NumUTF16TextChars();
	
	
	SQLFLOAT TopContBalazo;
 	TopContBalazo = QueryVector[29].GetAsDouble();
	
	SQLFLOAT LeftContBalazo;
 	LeftContBalazo = QueryVector[30].GetAsDouble();
	
	
	SQLFLOAT BottomContBalazo;
 	BottomContBalazo = QueryVector[31].GetAsDouble();
 	
 	SQLFLOAT RigthContBalazo;
 	RigthContBalazo = QueryVector[32].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContBalazo;
 	LineFaltantesContBalazo = QueryVector[33].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContBalazo;
 	LineRestantesContBalazo = QueryVector[34].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContBalazo;
 	CarFaltantesContBalazo = QueryVector[35].GetAsNumber();
 	
 	
 	
 	SQLINTEGER CarRestantesContBalazo;
 	CarRestantesContBalazo = QueryVector[36].GetAsNumber();
 	
 	SQLINTEGER CarContBalazo;
 	CarContBalazo = QueryVector[37].GetAsNumber();
 	
 		///////////	
	SQLCHAR ContenidoPieFoto[MAX_BUF_LEN];
	strcpy((char *) &ContenidoPieFoto, QueryVector[38].GrabCString());
	SQLINTEGER LenContenidoPieFoto=QueryVector[38].NumUTF16TextChars();
	
	
	SQLFLOAT TopContPieFoto;
 	TopContPieFoto = QueryVector[39].GetAsDouble();
	
	SQLFLOAT LeftContPieFoto;
 	LeftContPieFoto = QueryVector[40].GetAsDouble();
	
	
	SQLFLOAT BottomContPieFoto;
 	BottomContPieFoto = QueryVector[41].GetAsDouble();
 	
 	SQLFLOAT RigthContPieFoto;
 	RigthContPieFoto = QueryVector[42].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContPieFoto;
 	LineFaltantesContPieFoto = QueryVector[43].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContPieFoto;
 	LineRestantesContPieFoto = QueryVector[44].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContPieFoto;
 	CarFaltantesContPieFoto = QueryVector[45].GetAsNumber();
 	
 	
 	SQLINTEGER CarRestantesContPieFoto;
 	CarRestantesContPieFoto = QueryVector[46].GetAsNumber();
 	
 	SQLINTEGER CarContPieFoto;
 	CarContPieFoto = QueryVector[47].GetAsNumber();
 	
 	SQLCHAR FechaCheckInParamOUT[MAX_BUF_LEN];
	strcpy((char *) &FechaCheckInParamOUT, QueryVector[48].GrabCString());
	long LenFechaCheckInParamOUT=QueryVector[48].NumUTF16TextChars();

 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[49].GetAsNumber();
 	
 	SQLCHAR Id_Publicacion[MAX_BUF_LEN];
	strcpy((char *) &Id_Publicacion, QueryVector[50].GrabCString());
	long LenId_Publicacion=QueryVector[50].NumUTF16TextChars();

	SQLCHAR Id_Seccion[MAX_BUF_LEN];
	strcpy((char *) &Id_Seccion, QueryVector[51].GrabCString());
	long LenId_Seccion=QueryVector[51].NumUTF16TextChars();
 	
 	SQLCHAR Fecha_Edicion[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Edicion, QueryVector[52].GrabCString());
	long LenFecha_Edicion=QueryVector[52].NumUTF16TextChars();

	
//	




	
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 

 		
  			long cbReturnCode=SQL_NTS;
 			

  				
				rc = SQLBindParameter(stmt,1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ID_Elem_Padre,0,&LenID_Elem_Padre);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_USUARIO,0,&LenId_USUARIO);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  		
  				rc = SQLBindParameter(stmt,3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &DIRIGIDO_A,0,&LenDIRIGIDO_A);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  		
  				rc = SQLBindParameter(stmt,4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ESTATUS_ELE,0,&LenESTATUS_ELE);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				//
				
				
				
							
				rc = SQLBindParameter(stmt,5, SQL_PARAM_INPUT_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ID_Elem_Cont_Note,MAX_BUF_LEN,&LenID_Elem_Cont_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,6, SQL_PARAM_INPUT_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_Tit_Note,MAX_BUF_LEN,&LenId_Elem_Tit_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,7, SQL_PARAM_INPUT_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_PieFoto_Note,MAX_BUF_LEN,&LenId_Elem_PieFoto_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  				
  				
				rc = SQLBindParameter(stmt,8, SQL_PARAM_INPUT_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_Balazo_Note,MAX_BUF_LEN,&LenId_Elem_Balazo_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				//
				
				///////////
				rc = SQLBindParameter(stmt,9, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoNota,0,&LenContenidoNota);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
 				rc = SQLBindParameter(stmt,10, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,11, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,12, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,13, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,14, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,15, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,16, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,17, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,18, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  				
  				///////////
				rc = SQLBindParameter(stmt,19, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoTitulo,0,&LenContenidoTitulo);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
 				rc = SQLBindParameter(stmt,20, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,21, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,22, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,23, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,24, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,25, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,26, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,27, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,28, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}			
				
				
				
				///////////
				rc = SQLBindParameter(stmt,29, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoBalazo,0,&LenContenidoBalazo);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
 				rc = SQLBindParameter(stmt,30, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,31, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,32, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,33, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,34, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,35, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,36, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,37, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,38, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}			
				
				
				///////////
				rc = SQLBindParameter(stmt,39, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoPieFoto,0,&LenContenidoBalazo);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
 				rc = SQLBindParameter(stmt,40, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,41, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,42, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,43, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,44, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,45, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,46, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,47, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,48, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}		
				
				
				rc = SQLBindParameter(stmt,49, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &FechaCheckInParamOUT,MAX_BUF_LEN,&LenFechaCheckInParamOUT);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,50, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,51, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Publicacion,MAX_BUF_LEN,&LenId_Publicacion);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,52, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Seccion,MAX_BUF_LEN,&LenId_Seccion);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,53, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Edicion,MAX_BUF_LEN,&LenFecha_Edicion);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
						

  		}
  		
  		
		
  			
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{		
			
			/*QueryVector[0]=(char *) Id_Elem_Padre;
			QueryVector[1]=(char *) Id_User;
			QueryVector[2]=(char *) CadenError;*/
			
			QueryVector[4]=(char *) ID_Elem_Cont_Note;
			QueryVector[5]=(char *) Id_Elem_Tit_Note;
			QueryVector[6]=(char *) Id_Elem_PieFoto_Note;
			QueryVector[7]=(char *) Id_Elem_Balazo_Note;
			
			QueryVector[48]=(char *) FechaCheckInParamOUT;
			retval=kTrue;
				
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureCHECKINNOTA numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	
	return(retval);
 }
 
 


const bool16 N2PSQLUtils::StoreProcedureSET_NOTA_COLO_CADA(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureSET_NOTA_COLO_CADA( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}


 
bool16 N2PSQLUtils::ejecutaStoreProcedureSET_NOTA_COLO_CADA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
/*	
CREATE PROCEDURE NOTA_COLOCA_Y_LIGA_A_PAGINA 
@ID_Elem_Padre VARCHAR (50) ,
@N2PSQLFolioPag VARCHAR(50),
@OTROSERVIDOR INTEGER,
@ID_Usuario VARCHAR(50),
@ErrorOcurrido INTEGER OUTPUT
@NuevaNotaValores VARCHAR OUTPUT
*/


	
	
 	PMString Sentencia="{call NOTA_COLOCA_Y_LIGA_A_PAGINA( ?, ?, ? , ?, ? , ?)}";

	SQLCHAR Id_Elem_Padre[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_Padre, QueryVector[0].GrabCString());
	long LenId_Elem_Padre=QueryVector[0].NumUTF16TextChars();

	SQLCHAR N2PSQLFolioPag[MAX_BUF_LEN];
	strcpy((char *) &N2PSQLFolioPag, QueryVector[1].GrabCString());
	long LenN2PSQLFolioPag=QueryVector[1].NumUTF16TextChars();
	
	SQLINTEGER OTROSERVIDOR;
 	OTROSERVIDOR = QueryVector[2].GetAsNumber();
 	
 	SQLCHAR ID_Usuario[MAX_BUF_LEN];
	strcpy((char *) &ID_Usuario, QueryVector[3].GrabCString());
	long LenID_Usuario=QueryVector[3].NumUTF16TextChars();

	SQLINTEGER ErrorOcurrido;
 	ErrorOcurrido = QueryVector[4].GetAsNumber();
 	
	
	SQLCHAR NuevaNotaValores[MAX_BUF_LEN];
	strcpy((char *) &NuevaNotaValores, QueryVector[3].GrabCString());
	long LenNuevaNotaValores = QueryVector[3].NumUTF16TextChars();
	
	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 
		 		
  		long cbReturnCode=SQL_NTS;
  		
  		

  		/////
  		rc = SQLBindParameter(stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_Padre,MAX_BUF_LEN,&LenId_Elem_Padre);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
  		
  		rc = SQLBindParameter(stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &N2PSQLFolioPag,MAX_BUF_LEN,&LenN2PSQLFolioPag);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 3, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &OTROSERVIDOR,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}	
		
		rc = SQLBindParameter(stmt, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, MAX_BUF_LEN, 0, &ID_Usuario,MAX_BUF_LEN,&LenID_Usuario);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
		rc = SQLBindParameter(stmt, 5, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &ErrorOcurrido,0,&cbReturnCode);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}	
		
		
		

		rc = SQLBindParameter(stmt, 6, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &NuevaNotaValores,MAX_BUF_LEN,&LenNuevaNotaValores);
		if(rc!=SQL_SUCCESS)
		{
			return(kFalse);
		}
		
  		
  		
		
		rc = SQLExecute(stmt);
		
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{	
			//CAlert::InformationAlert("Que loco");
			//CAlert::InformationAlert((char *)NuevaNotaValores);
			PMString ErrorA=(char*)NuevaNotaValores;
			if(ErrorA=="SinPrivilegios")
			{
				CAlert::InformationAlert("No Tiene Privilegios");
			}
			else
			{
				PMString aa="";
				aa.AppendNumber(ErrorOcurrido);
				QueryVector[4]=aa;
				retval=kTrue;
				QueryVector.push_back((char *)NuevaNotaValores);
			}
			
			
			
		}
		else
		{
		
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureSET_NOTA_COLO_CADA numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	}
	
	return(retval);
 }
 
 

 
 const bool16 N2PSQLUtils::StoreProcedureIMPORTARNOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureIMPORTARNOTA( stmt, QueryVector))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}


bool16 N2PSQLUtils::ejecutaStoreProcedureIMPORTARNOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector )
 {
 
  PMString Sentencia="{call NOTA_IMPORTAR_N2P( ?, ?, ?, ?, ? ,?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?,   ?, ?, ?, ?, ? ,   ?, ?, ?, ?, ?,    ?, ?, ?, ?, ? ,   ?, ?, ?, ?, ? ,  ?, ?, ?, ? , ? ,     ? ,?,?,?,? ,?,?)}";

 /*	CREATE PROCEDURE NOTA_IMPORTAR_N2P
@IdElemento VARCHAR(50) OUTPUT,						1
@NomElemento VARCHAR(50),							2
@IDFuente  VARCHAR(50),								3
@IDColor VARCHAR(50),								4
@Servidor VARCHAR(50),								5
@Id_USUARIO VARCHAR(50), 							6
@DigidoA VARCHAR(50),								7
@ESTATUS_ELE VARCHAR(30),							8
@Calificacion INTEGER,								9
@IdElemento_Contenido VARCHAR(30) OUTPUT ,			10
@IdElemento_Titulo VARCHAR(30)  OUTPUT ,			11
@IdElemento_PieFoto VARCHAR(30) OUTPUT,				12
@IdElemento_balazo VARCHAR(30) OUTPUT,				13

@Conte_Contenido VARCHAR(8000),						14
@TopContNota FLOAT,									15
@LeftContNota FLOAT,								16
@BottomContNota FLOAT,								17
@RigthContNota FLOAT,  								18
@LineFaltantesContNota INTEGER,						19
@LineRestantesContNota INTEGER,						20
@CarFaltantesContNota INTEGER,						21
@CarRestantesContNota INTEGER,						22
@CarContNota INTEGER,								23

@Conte_Titulo VARCHAR(8000),						24
@TopContTitulo FLOAT,								25
@LeftContTitulo FLOAT,								26
@BottomContTitulo FLOAT,							27
@RigthContTitulo FLOAT,								28
@LineFaltantesContTitulo INTEGER,					29
@LineRestantesContTitulo INTEGER,					30
@CarFaltantesContTitulo INTEGER,					31
@CarRestantesContTitulo INTEGER,					32
@CarContTitulo INTEGER,								33

@Conte_balazo VARCHAR(8000),						34
@TopContBalazo FLOAT,								35
@LeftContBalazo FLOAT,								36
@BottomContBalazo FLOAT,							37
@RigthContBalazo FLOAT,								38
@LineFaltantesContBalazo  INTEGER,					39
@LineRestantesContBalazo INTEGER,					40
@CarFaltantesContBalazo  INTEGER,					41
@CarRestantesContBalazo INTEGER,					42
@CarContBalazo  INTEGER,							43

@Conte_PieFoto  VARCHAR(8000),						44
@TopContPieFoto FLOAT,								45
@LeftContPieFoto FLOAT,								46
@BottomContPieFoto FLOAT,							47
@RigthContPieFoto FLOAT,							48
@LineFaltantesContPieFoto  INTEGER,					49
@LineRestantesContPieFoto INTEGER,					50
@CarFaltantesContPieFoto  INTEGER,					51
@CarRestantesContPieFoto INTEGER,					52
@CarContPieFoto INTEGER,							53
	
@FechaCheckInParamOUT VARCHAR(50) OUTPUT,			54
@N2PSQLFolioPag VARCHAR (50) 						55

	*/

	SQLCHAR ID_Elem_Padre[MAX_BUF_LEN];
	strcpy((char *) &ID_Elem_Padre, QueryVector[0].GrabCString());
	long LenID_Elem_Padre=QueryVector[0].NumUTF16TextChars();
	
	SQLCHAR Nombre_Elemnto[MAX_BUF_LEN];
	strcpy((char *) &Nombre_Elemnto, QueryVector[1].GrabCString());
	long LenNombre_Elemnto=QueryVector[1].NumUTF16TextChars();
	
	SQLCHAR IDFuente[MAX_BUF_LEN];
	strcpy((char *) &IDFuente, QueryVector[2].GrabCString());
	long LenIDFuente=QueryVector[2].NumUTF16TextChars();
	
	SQLCHAR IDColor[MAX_BUF_LEN];
	strcpy((char *) &IDColor, QueryVector[3].GrabCString());
	long LenIDColor=QueryVector[3].NumUTF16TextChars();
	
	SQLCHAR Servidor[MAX_BUF_LEN];
	strcpy((char *) &Servidor, QueryVector[4].GrabCString());
	long LenServidor=QueryVector[4].NumUTF16TextChars();
	
	
	SQLCHAR Id_USUARIO[MAX_BUF_LEN];
	strcpy((char *) &Id_USUARIO, QueryVector[5].GrabCString());
	long LenId_USUARIO=QueryVector[5].NumUTF16TextChars();
	
	
	SQLCHAR DIRIGIDO_A[MAX_BUF_LEN];
	strcpy((char *) &DIRIGIDO_A, QueryVector[6].GrabCString());
	long LenDIRIGIDO_A=QueryVector[6].NumUTF16TextChars();
	
	
	SQLCHAR ESTATUS_ELE[MAX_BUF_LEN];
	strcpy((char *) &ESTATUS_ELE, QueryVector[7].GrabCString());
	long LenESTATUS_ELE=QueryVector[7].NumUTF16TextChars();
	
	
	
	SQLINTEGER Calificacion;
 	Calificacion = QueryVector[8].GetAsNumber();
 	
	
	SQLCHAR ID_Elem_Cont_Note[MAX_BUF_LEN];
	strcpy((char *) &ID_Elem_Cont_Note, QueryVector[9].GrabCString());
	long LenID_Elem_Cont_Note=QueryVector[9].NumUTF16TextChars();
	
	SQLCHAR Id_Elem_Tit_Note[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_Tit_Note, QueryVector[10].GrabCString());
	long LenId_Elem_Tit_Note=QueryVector[10].NumUTF16TextChars();
	
	SQLCHAR Id_Elem_PieFoto_Note[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_PieFoto_Note, QueryVector[11].GrabCString());
	long LenId_Elem_PieFoto_Note=QueryVector[11].NumUTF16TextChars();
	
	SQLCHAR Id_Elem_Balazo_Note[MAX_BUF_LEN];
	strcpy((char *) &Id_Elem_Balazo_Note, QueryVector[12].GrabCString());
	long LenId_Elem_Balazo_Note=QueryVector[12].NumUTF16TextChars();
	
	SQLCHAR ContenidoNota[MAX_BUF_LEN];
	strcpy((char *) &ContenidoNota, QueryVector[13].GrabCString());
	long LenContenidoNota=QueryVector[13].NumUTF16TextChars();
	
	
	SQLFLOAT TopContNota;
 	TopContNota = QueryVector[14].GetAsDouble();

	/*PMString ss="";
	ss.AppendNumber(TopContNota,5,kFalse,kFalse);
	CAlert::InformationAlert(ss);
*/


	SQLFLOAT LeftContNota;
 	LeftContNota = QueryVector[15].GetAsDouble();
	
	
	SQLFLOAT BottomContNota;
 	BottomContNota = QueryVector[16].GetAsDouble();
 	
 	SQLFLOAT RigthContNota;
 	RigthContNota = QueryVector[17].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContNota;
 	LineFaltantesContNota = QueryVector[18].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContNota;
 	LineRestantesContNota = QueryVector[19].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContNota;
 	CarFaltantesContNota = QueryVector[20].GetAsNumber();
 	
 	
 	SQLINTEGER CarRestantesContNota;
 	CarRestantesContNota = QueryVector[21].GetAsNumber();
 	
 	SQLINTEGER CarContNota;
 	CarContNota = QueryVector[22].GetAsNumber();
 	
 	///////////	
	SQLCHAR ContenidoTitulo[MAX_BUF_LEN];
	strcpy((char *) &ContenidoTitulo, QueryVector[23].GrabCString());
	long LenContenidoTitulo=QueryVector[23].NumUTF16TextChars();
	
	
	SQLFLOAT TopContTitulo;
 	TopContTitulo = QueryVector[24].GetAsDouble();
	
	SQLFLOAT LeftContTitulo;
 	LeftContTitulo = QueryVector[25].GetAsDouble();
	
	
	SQLFLOAT BottomContTitulo;
 	BottomContTitulo = QueryVector[26].GetAsDouble();
 	
 	SQLFLOAT RigthContTitulo;
 	RigthContTitulo = QueryVector[27].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContTitulo;
 	LineFaltantesContTitulo = QueryVector[28].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContTitulo;
 	LineRestantesContTitulo = QueryVector[29].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContTitulo;
 	CarFaltantesContTitulo = QueryVector[30].GetAsNumber();
 	
 	
 	
 	SQLINTEGER CarRestantesContTitulo;
 	CarRestantesContTitulo = QueryVector[31].GetAsNumber();
 	
 	SQLINTEGER CarContTitulo;
 	CarContTitulo = QueryVector[32].GetAsNumber();
 	
 		///////////	
	SQLCHAR ContenidoBalazo[MAX_BUF_LEN];
	strcpy((char *) &ContenidoBalazo, QueryVector[33].GrabCString());
	long LenContenidoBalazo=QueryVector[33].NumUTF16TextChars();
	
	
	SQLFLOAT TopContBalazo;
 	TopContBalazo = QueryVector[34].GetAsDouble();
	
	SQLFLOAT LeftContBalazo;
 	LeftContBalazo = QueryVector[35].GetAsDouble();
	
	
	SQLFLOAT BottomContBalazo;
 	BottomContBalazo = QueryVector[36].GetAsDouble();
 	
 	SQLFLOAT RigthContBalazo;
 	RigthContBalazo = QueryVector[37].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContBalazo;
 	LineFaltantesContBalazo = QueryVector[38].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContBalazo;
 	LineRestantesContBalazo = QueryVector[39].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContBalazo;
 	CarFaltantesContBalazo = QueryVector[40].GetAsNumber();
 	
 	
 	
 	SQLINTEGER CarRestantesContBalazo;
 	CarRestantesContBalazo = QueryVector[41].GetAsNumber();
 	
 	SQLINTEGER CarContBalazo;
 	CarContBalazo = QueryVector[42].GetAsNumber();
 	
 		///////////	
	SQLCHAR ContenidoPieFoto[MAX_BUF_LEN];
	strcpy((char *) &ContenidoPieFoto, QueryVector[43].GrabCString());
	long LenContenidoPieFoto=QueryVector[43].NumUTF16TextChars();
	
	
	SQLFLOAT TopContPieFoto;
 	TopContPieFoto = QueryVector[44].GetAsDouble();
	
	SQLFLOAT LeftContPieFoto;
 	LeftContPieFoto = QueryVector[45].GetAsDouble();
	
	
	SQLFLOAT BottomContPieFoto;
 	BottomContPieFoto = QueryVector[46].GetAsDouble();
 	
 	SQLFLOAT RigthContPieFoto;
 	RigthContPieFoto = QueryVector[47].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContPieFoto;
 	LineFaltantesContPieFoto = QueryVector[48].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContPieFoto;
 	LineRestantesContPieFoto = QueryVector[49].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContPieFoto;
 	CarFaltantesContPieFoto = QueryVector[50].GetAsNumber();
 	
 	
 	SQLINTEGER CarRestantesContPieFoto;
 	CarRestantesContPieFoto = QueryVector[51].GetAsNumber();
 	
 	SQLINTEGER CarContPieFoto;
 	CarContPieFoto = QueryVector[52].GetAsNumber();
 	
 	SQLCHAR FechaCheckInParamOUT[MAX_BUF_LEN];
	strcpy((char *) &FechaCheckInParamOUT, QueryVector[53].GrabCString());
	long LenFechaCheckInParamOUT=QueryVector[53].NumUTF16TextChars();

 	SQLCHAR N2PSQLFolioPag[MAX_BUF_LEN];
 	strcpy((char *) &N2PSQLFolioPag, QueryVector[54].GrabCString());
	long LenN2PSQLFolioPag=QueryVector[54].NumUTF16TextChars();
 	
 	
 	SQLCHAR SeccionID[MAX_BUF_LEN];
 	strcpy((char *) &SeccionID, QueryVector[55].GrabCString());
	long LenSeccionID=QueryVector[55].NumUTF16TextChars();
	
	SQLCHAR PublicacionID[MAX_BUF_LEN];
 	strcpy((char *) &PublicacionID, QueryVector[56].GrabCString());
	long LenPublicacionID=QueryVector[56].NumUTF16TextChars();
//	




	
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 

 		
  			long cbReturnCode=SQL_NTS;
 			
				
				
  				
				rc = SQLBindParameter(stmt,1, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ID_Elem_Padre,MAX_BUF_LEN,&LenID_Elem_Padre);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Nombre_Elemnto,0,&LenNombre_Elemnto);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &IDFuente,0,&LenIDFuente);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &IDColor,0,&LenIDColor);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,5, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Servidor,0,&LenServidor);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_USUARIO,0,&LenId_USUARIO);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  		
  				rc = SQLBindParameter(stmt,7, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &DIRIGIDO_A,0,&LenDIRIGIDO_A);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  		
  				rc = SQLBindParameter(stmt,8, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ESTATUS_ELE,0,&LenESTATUS_ELE);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,9, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Calificacion,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
							
				rc = SQLBindParameter(stmt,10, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ID_Elem_Cont_Note,MAX_BUF_LEN,&LenID_Elem_Cont_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,11, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_Tit_Note,MAX_BUF_LEN,&LenId_Elem_Tit_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,12, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_PieFoto_Note,MAX_BUF_LEN,&LenId_Elem_PieFoto_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  				
  				
				rc = SQLBindParameter(stmt,13, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Elem_Balazo_Note,MAX_BUF_LEN,&LenId_Elem_Balazo_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				///////////
				rc = SQLBindParameter(stmt,14, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoNota,0,&LenContenidoNota);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
 				rc = SQLBindParameter(stmt,15, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,16, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,17, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,18, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,19, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,20, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,21, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,22, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,23, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  				
  				///////////
				rc = SQLBindParameter(stmt,24, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoTitulo,0,&LenContenidoTitulo);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
 				rc = SQLBindParameter(stmt,25, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,26, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,27, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,28, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,29, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,30, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,31, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,32, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,33, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContTitulo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}			
				
				
				
				///////////
				rc = SQLBindParameter(stmt,34, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoBalazo,0,&LenContenidoBalazo);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
 				rc = SQLBindParameter(stmt,35, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,36, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,37, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,38, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,39, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,40, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,41, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,42, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,43, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContBalazo,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}			
				
				
				///////////
				rc = SQLBindParameter(stmt,44, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoPieFoto,0,&LenContenidoBalazo);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
 				rc = SQLBindParameter(stmt,45, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,46, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,47, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,48, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,49, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,50, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,51, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,52, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,53, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContPieFoto,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}		
				
				rc = SQLBindParameter(stmt,54, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &FechaCheckInParamOUT,MAX_BUF_LEN,&LenFechaCheckInParamOUT);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,55, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &N2PSQLFolioPag,MAX_BUF_LEN,&LenN2PSQLFolioPag);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				
				rc = SQLBindParameter(stmt,56, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &SeccionID,MAX_BUF_LEN,&LenSeccionID);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,57, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &PublicacionID,MAX_BUF_LEN,&LenPublicacionID);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	

  		}
  		
  		
		
  			
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{		
			/*	@IdElemento VARCHAR(50) OUTPUT,						1
				@IdElemento_Contenido VARCHAR(30) OUTPUT ,			10
				@IdElemento_Titulo VARCHAR(30)  OUTPUT ,			11
				@IdElemento_PieFoto VARCHAR(30) OUTPUT,				12
				@IdElemento_balazo VARCHAR(30) OUTPUT,				13
				@FechaCheckInParamOUT VARCHAR(50) OUTPUT,			54*/
			
			QueryVector[0]=(char *) ID_Elem_Padre;
			QueryVector[9]=(char *) ID_Elem_Cont_Note;
			QueryVector[10]=(char *) Id_Elem_Tit_Note;
			QueryVector[11]=(char *) Id_Elem_PieFoto_Note;
			QueryVector[12]=(char *) Id_Elem_Balazo_Note;
			QueryVector[53]=(char *) FechaCheckInParamOUT;
			retval=kTrue;
				
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="StoreProcedureNOTA_IMPORTAR_N2P numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	
	return(retval);
 }
 
 


/****/
 const bool16 N2PSQLUtils::StoreProcedureCHECKINELEMENTONOTA(const PMString& StringConnection, K2Vector<PMString>& QueryVector, const bool16& NotaEnCheckOut)
{
	bool16 retval=kFalse;
	
	SQLHANDLE  env = NULL;
	SQLHANDLE  con = NULL; 
	SQLHANDLE  stmt = NULL; 
	if(SQLInicializaConexion(StringConnection,stmt,con,env))
	{
		if(ejecutaStoreProcedureCHECKINELEMENTONOTA( stmt, QueryVector, NotaEnCheckOut))
		{	
			if (stmt != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_STMT, stmt);
			}

			if (con != NULL)
			{
				SQLDisconnect(con);
				SQLFreeHandle(SQL_HANDLE_DBC, con);
			}

			if (env != NULL)
			{
				SQLFreeHandle(SQL_HANDLE_ENV, env);
			}
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
	}
	else
	{
		retval=kFalse;
	}
	return retval;
	
	
}
 
bool16 N2PSQLUtils::ejecutaStoreProcedureCHECKINELEMENTONOTA(SQLHANDLE& stmt, K2Vector<PMString>& QueryVector , const bool16& NotaEnCheckOut)
 {
  PMString Sentencia="";
 if(NotaEnCheckOut)
 {
 	Sentencia="{call NOTAELEMENTON2PCHECKINMejorWithPases( ?, ?, ?, ?, ? ,?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, )}";
 }
 else
 {
 	Sentencia="{call NOTAELEMENTON2PCHECKINMejor( ?, ?, ?, ?, ? ,?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
 }
 
 /*CREATE PROCEDURE NOTAELEMENTON2PCHECKINMejorWithPases
@ID_Elem_Padre VARCHAR(50),
@Id_USUARIO VARCHAR(50), 
@DIRIGIDO_A VARCHAR(50),
@ESTATUS_ELE VARCHAR(30),
@ID_Elem_Cont_Note VARCHAR(30)  OUTPUT ,
@ContenidoNota VARCHAR(8000),
@TopContNota FLOAT,
@LeftContNota FLOAT,
@BottomContNota FLOAT,
@RigthContNota FLOAT,  
@LineFaltantesContNota INTEGER,
@LineRestantesContNota INTEGER,
@CarFaltantesContNota INTEGER,
@CarRestantesContNota INTEGER,
@CarContNota INTEGER,
@FechaCheckInParamOUT VARCHAR(50) OUTPUT,
@Id_Evento INTEGER ,
@Id_Publicacion VARCHAR(50),
@Id_Seccion VARCHAR(50),
@Fecha_Edicion varchar(50)
AS*/


	SQLCHAR ID_Elem_Padre[MAX_BUF_LEN];
	strcpy((char *) &ID_Elem_Padre, QueryVector[0].GrabCString());
	long LenID_Elem_Padre=QueryVector[0].NumUTF16TextChars();
	
	SQLCHAR Id_USUARIO[MAX_BUF_LEN];
	strcpy((char *) &Id_USUARIO, QueryVector[1].GrabCString());
	long LenId_USUARIO=QueryVector[1].NumUTF16TextChars();
	
	
	SQLCHAR DIRIGIDO_A[MAX_BUF_LEN];
	strcpy((char *) &DIRIGIDO_A, QueryVector[2].GrabCString());
	long LenDIRIGIDO_A=QueryVector[2].NumUTF16TextChars();
	
	
	SQLCHAR ESTATUS_ELE[MAX_BUF_LEN];
	strcpy((char *) &ESTATUS_ELE, QueryVector[3].GrabCString());
	long LenESTATUS_ELE=QueryVector[3].NumUTF16TextChars();
	
	SQLCHAR ID_Elem_Cont_Note[MAX_BUF_LEN];
	strcpy((char *) &ID_Elem_Cont_Note, QueryVector[4].GrabCString());
	long LenID_Elem_Cont_Note=QueryVector[4].NumUTF16TextChars();
	
	
	
	SQLCHAR ContenidoNota[MAX_BUF_LEN];
	strcpy((char *) &ContenidoNota, QueryVector[5].GrabCString());
	long LenContenidoNota=QueryVector[5].NumUTF16TextChars();
	
	
	SQLFLOAT TopContNota;
 	TopContNota = QueryVector[6].GetAsDouble();

	/*PMString ss="";
	ss.AppendNumber(TopContNota,5,kFalse,kFalse);
	CAlert::InformationAlert(ss);
*/


	SQLFLOAT LeftContNota;
 	LeftContNota = QueryVector[7].GetAsDouble();
	
	
	SQLFLOAT BottomContNota;
 	BottomContNota = QueryVector[8].GetAsDouble();
 	
 	SQLFLOAT RigthContNota;
 	RigthContNota = QueryVector[9].GetAsDouble();
 	
 	SQLINTEGER LineFaltantesContNota;
 	LineFaltantesContNota = QueryVector[10].GetAsNumber();
 	
 	SQLINTEGER LineRestantesContNota;
 	LineRestantesContNota = QueryVector[11].GetAsNumber();
 	
 	SQLINTEGER CarFaltantesContNota;
 	CarFaltantesContNota = QueryVector[12].GetAsNumber();
 	
 	
 	SQLINTEGER CarRestantesContNota;
 	CarRestantesContNota = QueryVector[13].GetAsNumber();
 	
 	SQLINTEGER CarContNota;
 	CarContNota = QueryVector[14].GetAsNumber();
 	
 
 	
 	SQLCHAR FechaCheckInParamOUT[MAX_BUF_LEN];
	strcpy((char *) &FechaCheckInParamOUT, QueryVector[15].GrabCString());
	long LenFechaCheckInParamOUT=QueryVector[15].NumUTF16TextChars();

 	SQLINTEGER Id_Evento;
 	Id_Evento = QueryVector[16].GetAsNumber();
 	
 	SQLCHAR Id_Publicacion[MAX_BUF_LEN];
	strcpy((char *) &Id_Publicacion, QueryVector[17].GrabCString());
	long LenId_Publicacion=QueryVector[17].NumUTF16TextChars();

	SQLCHAR Id_Seccion[MAX_BUF_LEN];
	strcpy((char *) &Id_Seccion, QueryVector[18].GrabCString());
	long LenId_Seccion=QueryVector[18].NumUTF16TextChars();
 	
 	SQLCHAR Fecha_Edicion[MAX_BUF_LEN];
	strcpy((char *) &Fecha_Edicion, QueryVector[19].GrabCString());
	long LenFecha_Edicion=QueryVector[19].NumUTF16TextChars();

	
//	




	
 	

 	SQLINTEGER nativeError;
 	bool16 retval=kFalse;
 	SQLSMALLINT  bufLen;
	SQLRETURN  rc = SQL_SUCCESS;
	SQLCHAR sqlState[6];
	
	SQLCHAR errorMsg[MAX_BUF_LEN];

	int nchecking=0;
	int nSaving=0;
	SQLINTEGER nInd4=MAX_BUF_LEN;
	int nCmd=0;
	
	if (rc == SQL_SUCCESS)
	{
		SQLCHAR sql[Sentencia.NumUTF16TextChars()];
		strcpy((char *) sql, Sentencia.GrabCString());
		
		
		rc = SQLPrepare(stmt,sql,SQL_NTS); //CHECK error
		if(rc!=SQL_SUCCESS)
		{
			
			return(kFalse);
			
		}
		 

 		
  			long cbReturnCode=SQL_NTS;
 			

  				
				rc = SQLBindParameter(stmt,1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ID_Elem_Padre,0,&LenID_Elem_Padre);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_USUARIO,0,&LenId_USUARIO);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  		
  				rc = SQLBindParameter(stmt,3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &DIRIGIDO_A,0,&LenDIRIGIDO_A);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  		
  				rc = SQLBindParameter(stmt,4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ESTATUS_ELE,0,&LenESTATUS_ELE);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				//
				
				
				
							
				rc = SQLBindParameter(stmt,5, SQL_PARAM_INPUT_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ID_Elem_Cont_Note,MAX_BUF_LEN,&LenID_Elem_Cont_Note);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				
				//
				
				///////////
				rc = SQLBindParameter(stmt,6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &ContenidoNota,0,&LenContenidoNota);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
 				rc = SQLBindParameter(stmt,7, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &TopContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  			
  				rc = SQLBindParameter(stmt,8, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &LeftContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,9, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &BottomContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,10, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_FLOAT, 0, 0, &RigthContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,11, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineFaltantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,12, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &LineRestantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				
				rc = SQLBindParameter(stmt,13, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarFaltantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,14, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarRestantesContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
				
				rc = SQLBindParameter(stmt,15, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &CarContNota,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}
  			
  				
  				///////
				
							
				rc = SQLBindParameter(stmt,16, SQL_PARAM_OUTPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &FechaCheckInParamOUT,MAX_BUF_LEN,&LenFechaCheckInParamOUT);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,17, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &Id_Evento,0,&cbReturnCode);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,18, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Publicacion,MAX_BUF_LEN,&LenId_Publicacion);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,19, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Id_Seccion,MAX_BUF_LEN,&LenId_Seccion);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
				
				rc = SQLBindParameter(stmt,20, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 0, 0, &Fecha_Edicion,MAX_BUF_LEN,&LenFecha_Edicion);
				if(rc!=SQL_SUCCESS)
				{
					return(kFalse);
				}	
						

  		}
  		
  		
		
  			
		
		rc = SQLExecute(stmt);
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{		
			
			/*QueryVector[0]=(char *) Id_Elem_Padre;
			QueryVector[1]=(char *) Id_User;
			QueryVector[2]=(char *) CadenError;*/
			
			
			
			//CAlert::InformationAlert((char *) FechaCheckInParamOUT);
			QueryVector[4]=(char *) ID_Elem_Cont_Note;		
			QueryVector[15]=(char *) FechaCheckInParamOUT;
			//CAlert::InformationAlert(QueryVector[15]);
			retval=kTrue;
				
		}
		else
		{
						
			SQLGetDiagRec(SQL_HANDLE_STMT, stmt, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
						
			PMString Erroi="ejecutaStoreProcedureCHECKINELEMENTONOTA numero de error: ";
			Erroi.AppendNumber(nativeError);
			Erroi.Append(" ");
			for(int i=0;i<bufLen;i++)
				Erroi.Append((SQLCHAR)errorMsg[i]);

			
			CAlert::InformationAlert(Erroi);
			retval=kFalse;
		}
		
	
	return(retval);
 }
 
 
 
 
/*  bool16 N2PSQLUtils::SQLInicializaConexion(const PMString& StringConnection,SQLHANDLE& stmt,SQLHANDLE& hCon,SQLHANDLE& hEnv)
 {
 	
 	//SQLHENV hEnv;                      // Handle ODBC environment
	long    retc;                      // result of functions
	//SQLHDBC hCon;                     // Handle connection
	//SQLHANDLE hStmt;                        // Statement handle

	char                     V_OD_stat[10];         // Status SQL
	SQLINTEGER               V_OD_err,V_OD_rowanz,V_OD_id;
	SQLSMALLINT              V_OD_mlen,V_OD_colanz;
	char             V_OD_msg[200],V_OD_buffer[200];

 	int count = 0;

    // Allocate env handle
    retc=SQLAllocHandle(SQL_HANDLE_ENV,SQL_NULL_HANDLE,&hEnv);
    if ((retc != SQL_SUCCESS) && (retc != SQL_SUCCESS_WITH_INFO))
    {
         SQLGetDiagRec(SQL_HANDLE_DBC, hCon,1,
                          (SQLCHAR*)V_OD_stat, &V_OD_err,
                          (SQLCHAR*)V_OD_msg,100,&V_OD_mlen);
            PMString Erroi="numero de error:";
			Erroi.AppendNumber(V_OD_err);
			Erroi.Append(" ");
			for(int i=0;i<V_OD_mlen;i++)
				Erroi.Append((SQLCHAR)V_OD_msg[i]);
			
			CAlert::InformationAlert(Erroi);
        exit(1);
    }

    retc=SQLSetEnvAttr(hEnv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);
    if ((retc != SQL_SUCCESS) && (retc != SQL_SUCCESS_WITH_INFO))
    {
         SQLGetDiagRec(SQL_HANDLE_DBC, hCon,1,
                          (SQLCHAR*)V_OD_stat, &V_OD_err,
                          (SQLCHAR*)V_OD_msg,100,&V_OD_mlen);
            PMString Erroi="numero de error:";
			Erroi.AppendNumber(V_OD_err);
			Erroi.Append(" ");
			for(int i=0;i<V_OD_mlen;i++)
				Erroi.Append((SQLCHAR)V_OD_msg[i]);
			
			CAlert::InformationAlert(Erroi);
        SQLFreeHandle(SQL_HANDLE_ENV, hEnv);
        exit(1);
    }
    // Allocate connection handle
    retc = SQLAllocHandle(SQL_HANDLE_DBC, hEnv, &hCon);
    if ((retc != SQL_SUCCESS) && (retc != SQL_SUCCESS_WITH_INFO))
    {
        SQLGetDiagRec(SQL_HANDLE_DBC, hCon,1,
                          (SQLCHAR*)V_OD_stat, &V_OD_err,
                          (SQLCHAR*)V_OD_msg,100,&V_OD_mlen);
            PMString Erroi="numero de error:";
			Erroi.AppendNumber(V_OD_err);
			Erroi.Append(" ");
			for(int i=0;i<V_OD_mlen;i++)
				Erroi.Append((SQLCHAR)V_OD_msg[i]);
			
			CAlert::InformationAlert(Erroi);
        SQLFreeHandle(SQL_HANDLE_ENV, hEnv);
        exit(1);
    }

   // for( int i=0; i<=0; i++ )
  //  {
        // Try to connect
        retc = SQLConnect(hCon, (SQLCHAR*) "PlantilleroZA", SQL_NTS, (SQLCHAR*) "acceso",SQL_NTS, (SQLCHAR*) "1", SQL_NTS);
		
        if ((retc != SQL_SUCCESS) && (retc != SQL_SUCCESS_WITH_INFO))
        {
            SQLGetDiagRec(SQL_HANDLE_DBC, hCon,1,
                          (SQLCHAR*)V_OD_stat, &V_OD_err,
                          (SQLCHAR*)V_OD_msg,100,&V_OD_mlen);
            PMString Erroi="numero de error:";
			Erroi.AppendNumber(V_OD_err);
			Erroi.Append(" ");
			for(int i=0;i<V_OD_mlen;i++)
				Erroi.Append((SQLCHAR)V_OD_msg[i]);
			
			CAlert::InformationAlert(Erroi);
			
			return kFalse;
        }
      
        retc=SQLAllocHandle(SQL_HANDLE_STMT,hCon,&stmt);
	    if ((retc != SQL_SUCCESS) && (retc != SQL_SUCCESS_WITH_INFO))
    	{
        	 SQLGetDiagRec(SQL_HANDLE_DBC, hCon,1,
                          (SQLCHAR*)V_OD_stat, &V_OD_err,
                          (SQLCHAR*)V_OD_msg,100,&V_OD_mlen);
            PMString Erroi="numero de error:";
			Erroi.AppendNumber(V_OD_err);
			Erroi.Append(" ");
			for(int i=0;i<V_OD_mlen;i++)
				Erroi.Append((SQLCHAR)V_OD_msg[i]);
			
			CAlert::InformationAlert(Erroi);
	       return kFalse;
    	}
    	else
    	{
    		return kTrue;
    	}


      
  //  }

   
    //getchar();
   // SQLFreeHandle(SQL_HANDLE_DBC,hCon);
  //  SQLFreeHandle(SQL_HANDLE_ENV, hEnv);
   
	
	return kFalse;
 }
 */
 
 
 
/*const bool16 N2PSQLUtils::HAYPAPA()
{
 HENV			henv;
HDBC			hdbc;
HSTMT			hstmt;
SQLRETURN		sr;

SQLCHAR*		theDiagState = new SQLCHAR[50];
SQLINTEGER		theNativeState;
SQLCHAR*		theMessageText  = new SQLCHAR[255];
SQLSMALLINT		iOutputNo;


long param1 = 0;
long param2 = 0;
long param3 = 0;
SQLINTEGER cbValue1 = sizeof(long);
SQLINTEGER cbValue2= sizeof(long);
SQLINTEGER cbValue3= sizeof(long);

   SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, & henv );

   sr = SQLSetEnvAttr( henv, SQL_ATTR_ODBC_VERSION, ( void * ) SQL_OV_ODBC3, 0 ); 

   sr = SQLAllocHandle( SQL_HANDLE_DBC, henv, & hdbc );

   //Please note that the DSN name is LocalPubs here. Chenge the DSN name UserID 
   //and Password here.
   sr = SQLConnect( hdbc, (SQLCHAR*) "N2PConnect", SQL_NTS, 
	(SQLCHAR*) "acceso", SQL_NTS, 
	(SQLCHAR*) "1", SQL_NTS );

   sr = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, & hstmt );

   sr = SQLSetStmtAttr( hstmt, 
		SQL_ATTR_CURSOR_TYPE, 
		( void * ) SQL_CURSOR_DYNAMIC, 
		SQL_IS_INTEGER );

   sr= SQLBindParameter( hstmt, 
			1, 
			SQL_PARAM_OUTPUT, 
			SQL_C_LONG, 
			SQL_INTEGER, 
			sizeof( long ), 
			0,
			& param1, 
			sizeof( long ), 
			& cbValue1 );

   sr = SQLBindParameter( hstmt, 
			2, 
			SQL_PARAM_INPUT, 
			SQL_C_LONG, 
			SQL_INTEGER, 
			sizeof( long ), 
			0,
			& param2, 
			sizeof( long ), 
			& cbValue2 );
   sr = SQLBindParameter( hstmt, 
			3, 
			SQL_PARAM_OUTPUT, 
			SQL_C_LONG, 
			SQL_INTEGER, 
			sizeof( long ), 
			0,
			& param3, 
			sizeof( long ), 
			& cbValue3 );


   sr = SQLExecDirect( hstmt,( unsigned char * ) "{ ? = call sp_myproc(?, ?)}", SQL_NTS );

   if (sr != SQL_SUCCESS)
   {
        //With this bug you will get an error message. Check this message in theMessageText.
	SQLGetDiagRec(SQL_HANDLE_STMT,hstmt,1,theDiagState,&theNativeState,theMessageText,100,&iOutputNo);
	
		
            PMString Erroi="numero de error:";
			Erroi.AppendNumber(theNativeState);
			Erroi.Append(" ");
			for(int i=0;i<iOutputNo;i++)
				Erroi.Append((SQLCHAR)theMessageText[i]);
			
			CAlert::InformationAlert(Erroi);
        //SQLFreeHandle(SQL_HANDLE_ENV, hEnv);
   		
   }

   //With the fix, you should get the return value after calling this SQLMoreResult
   sr = SQLMoreResults( hstmt );

   //Free allocated memory and disconnect
   SQLFreeStmt( hstmt, SQL_CLOSE );
   SQLFreeStmt( hstmt, SQL_DROP );
   SQLDisconnect( hdbc ); 
   SQLFreeHandle( SQL_HANDLE_DBC, hdbc ); 
   SQLFreeHandle( SQL_HANDLE_ENV, henv );
   delete theMessageText; 
   delete theDiagState;	
   return(kTrue);
 }
 */