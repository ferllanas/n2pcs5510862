//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PInOutID_h__
#define __N2PInOutID_h__

#include "SDKDef.h"

// Company:
#define kN2PInOutCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PInOutCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2PInOutPluginName	"N2PLogInLogOut"			// Name of this plug-in.
#define kN2PInOutPrefixNumber	0x96C00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PInOutVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kN2PInOutAuthor		"Fernando Llanas"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PInOutPrefixNumber above to modify the prefix.)
#define kN2PInOutPrefix		RezLong(kN2PInOutPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PInOutStringPrefix	SDK_DEF_STRINGIZE(kN2PInOutPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PInOutMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PInOutMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PInOutPluginID, kN2PInOutPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2PInOutActionComponentBoss,	kN2PInOutPrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2PInOutDialogBoss,			kN2PInOutPrefix + 2)
DECLARE_PMID(kClassIDSpace, kN2PInOutStartupShutdownBoss,	kN2PInOutPrefix + 3)
DECLARE_PMID(kClassIDSpace, kN2PInOutFooCmdBoss,			kN2PInOutPrefix + 4)
DECLARE_PMID(kClassIDSpace, kN2PInOutSessionObserBoss,		kN2PInOutPrefix + 5)
DECLARE_PMID(kClassIDSpace, kN2PRegisterUsersBoss,			kN2PInOutPrefix + 6)
DECLARE_PMID(kClassIDSpace, kModRegisterUsersCmdBoss,		kN2PInOutPrefix + 7)
DECLARE_PMID(kClassIDSpace, kN2PRegisterUsersConversionProviderBoss,		kN2PInOutPrefix + 8)
DECLARE_PMID(kClassIDSpace, kN2RegisterUsersScriptProviderBoss,		kN2PInOutPrefix + 9)

DECLARE_PMID(kClassIDSpace, kN2PPrfDialogControllerBoss,		kN2PInOutPrefix + 10)

DECLARE_PMID(kClassIDSpace, kN2PSQLUtilsBoss,		kN2PInOutPrefix + 11)
DECLARE_PMID(kClassIDSpace, kN2PNonBrokenPasswordEditBoxWidgetBoss,		kN2PInOutPrefix + 12)
DECLARE_PMID(kClassIDSpace, kN2PInOutNewWSResponderBoss,		kN2PInOutPrefix + 13)

// InterfaceIDs:
// None in this plug-in.
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PINOUTFOODATA,		kN2PInOutPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PREGISTERUSERS,		kN2PInOutPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IREGISTERUSERSUTILS,	kN2PInOutPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQLUTILS,	kN2PInOutPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PLogInTask,	kN2PInOutPrefix + 4)

// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PInOutDialogControllerImpl,		kN2PInOutPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutDialogObserverImpl,		kN2PInOutPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutActionComponentImpl,		kN2PInOutPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutStartupShutdownImpl,		kN2PInOutPrefix + 3 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutFooCmdImpl,				kN2PInOutPrefix + 4 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutFoolDataImpl,				kN2PInOutPrefix + 5 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutObserverSessionImpl,		kN2PInOutPrefix + 6 )

DECLARE_PMID(kImplementationIDSpace, kN2PRegisterUsersImpl,			kN2PInOutPrefix + 7 )
DECLARE_PMID(kImplementationIDSpace, kModRegisterUsersCmdImpl,		kN2PInOutPrefix + 8 )
DECLARE_PMID(kImplementationIDSpace, kRegisterUsersUtilsImpl,		kN2PInOutPrefix + 9 )
DECLARE_PMID(kImplementationIDSpace, kN2RegisterUsersScriptProviderImpl, 	kN2PInOutPrefix + 10 )
DECLARE_PMID(kImplementationIDSpace, kN2PPrfDialogControllerImpl, 	kN2PInOutPrefix + 11 )
DECLARE_PMID(kImplementationIDSpace, kN2PPrfPanelCreatorImpl, 	kN2PInOutPrefix + 12 )

DECLARE_PMID(kImplementationIDSpace, kN2PSQLUtilsImpl, 	kN2PInOutPrefix + 13 )
DECLARE_PMID(kImplementationIDSpace, kN2PNonBrokenPasswordEventHandlerImpl, 	kN2PInOutPrefix + 14 )

DECLARE_PMID(kImplementationIDSpace, kN2PInOutNewWSResponderImpl, 	kN2PInOutPrefix + 15 )
DECLARE_PMID(kImplementationIDSpace, kN2PLogInTaskImpl, 	kN2PInOutPrefix + 16 )
DECLARE_PMID(kImplementationIDSpace, kN2PInOutCommandInterceptorImpl, 	kN2PInOutPrefix + 17 )

// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2PInOutAboutActionID,				kN2PInOutPrefix + 0)
DECLARE_PMID(kActionIDSpace, kN2PInOutDialogActionID, kN2PInOutPrefix + 4)
DECLARE_PMID(kActionIDSpace, kN2PInOutMenuItem1ActionID,		kN2PInOutPrefix + (10 + 1))


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kN2PInOutDialogWidgetID, 			kN2PInOutPrefix + 1)

DECLARE_PMID(kWidgetIDSpace, kLogInAndoutIconSuiteWidgetID, 	kN2PInOutPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kN2PInOutComboUsuarioWidgetID, 			kN2PInOutPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kN2PInOutEnterPswWidgetID, 				kN2PInOutPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kN2PLogoWidgetID,				 kN2PInOutPrefix + 5)

DECLARE_PMID(kWidgetIDSpace, kN2PPrfPanelWidgetID,				 kN2PInOutPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfGroupWkSpcWidgetID,				 kN2PInOutPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfWkSpcClusterWidgetID,				 kN2PInOutPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfWkSpcRadio1WidgetID,				 kN2PInOutPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfWkSpcRadio2WidgetID,				 kN2PInOutPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfGroupWkSpcTitleWidgetID,				 kN2PInOutPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfDocClusterWidgetID,				 kN2PInOutPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfDocRadio1WidgetID,				 kN2PInOutPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfDocRadio2WidgetID,				 kN2PInOutPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfGroupDocTitleWidgetID,				 kN2PInOutPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kN2PPrfGroupDocWidgetID,				 kN2PInOutPrefix + 16)


//Script Element IDs
//Suites
// DECLARE_PMID(kScriptInfoIDSpace, k***SuiteScriptElement, kCSTPrfPrefix + 0)
//Objects
DECLARE_PMID(kScriptInfoIDSpace, kN2PRegisterUserObjectScriptElement, kN2PInOutPrefix + 10)
//Events
// DECLARE_PMID(kScriptInfoIDSpace, k***EventScriptElement, kCSTPrfPrefix + 20)
//Properties
DECLARE_PMID(kScriptInfoIDSpace, kN2PRegisterUserPropertyScriptElement, kN2PInOutPrefix + 30)
DECLARE_PMID(kScriptInfoIDSpace, kN2PRegisterUsersPrefValuePropertyScriptElement, kN2PInOutPrefix + 31)


// "About Plug-ins" sub-menu:
#define kN2PInOutAboutMenuKey			kN2PInOutStringPrefix "kN2PInOutAboutMenuKey"
#define kN2PInOutAboutMenuPath			kSDKDefStandardAboutMenuPath kN2PInOutCompanyKey

// "Plug-ins" sub-menu:
#define kN2PInOutPluginsMenuKey 		kN2PInOutStringPrefix "kN2PInOutPluginsMenuKey"
#define kN2PInOutPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PInOutCompanyKey //kSDKDefDelimitMenuPath kN2PInOutPluginsMenuKey

// Menu item keys:
#define kN2PInOutMenuItem1MenuItemKey		kN2PInOutStringPrefix "kN2PInOutMenuItem1MenuItemKey"


// Other StringKeys:
#define kN2PInOutAboutBoxStringKey				kN2PInOutStringPrefix "kN2PInOutAboutBoxStringKey"
#define kN2PInOutMenuItem1StringKey			kN2PInOutStringPrefix "kN2PInOutMenuItem1StringKey"

#define kN2PInOutTargetMenuPath kN2PInOutPluginsMenuPath

#define kN2PInOutDialogTitleKey		kN2PInOutStringPrefix "kN2PInOutDialogTitleKey"
#define kN2PInOutAboutBoxStringKey		kN2PInOutStringPrefix "kN2PInOutAboutBoxStringKey"
#define kN2PInOutUsuarioStringKey		kN2PInOutStringPrefix "kN2PInOutUsuarioStringKey"
#define kN2PInOutPswdStringKey		kN2PInOutStringPrefix "kN2PInOutPswdStringKey"

#define kN2PInOutOpenDialogCmdStringKey		kN2PInOutStringPrefix "kN2PInOutOpenDialogCmdStringKey"
#define kN2PInOutSelectUserStringKey				kN2PInOutStringPrefix "kN2PInOutSelectUserStringKey"
#define kN2PInOutTeclearContrasenaStringKey			kN2PInOutStringPrefix "kN2PInOutTeclearContrasenaStringKey"
#define kN2PInOutWelcometoSystemStringKey			kN2PInOutStringPrefix "kN2PInOutkWelcometoSystemStringKey"
#define kN2PInOutFinishedSesionStringKey			kN2PInOutStringPrefix "kN2PInOutFinishedSesionStringKey"

#define kN2PInOutPrfGroupWkSpcTitleKey	kN2PInOutStringPrefix  "kN2PInOutPrfGroupWkSpcTitleKey"		
#define kN2PInOutPrfWkSpcRadio1Key		kN2PInOutStringPrefix  "kN2PInOutPrfWkSpcRadio1Key"		
#define kN2PInOutPrfWkSpcRadio2Key		kN2PInOutStringPrefix  "kN2PInOutPrfWkSpcRadio2Key"		

#define kN2PInOutPrfGroupDocTitleKey		kN2PInOutStringPrefix  "kN2PInOutPrfGroupDocTitleKey"		
#define kN2PInOutPrfDocRadio1Key			kN2PInOutStringPrefix  "kN2PInOutPrfDocRadio1Key"		
#define kN2PInOutkCstPrfDocRadio2Key			kN2PInOutStringPrefix  "kN2PInOutkCstPrfDocRadio2Key"		

#define kN2PInOutPrfCmdNameKey		kN2PInOutStringPrefix  "kN2PInOutPrfCmdNameKey"				

#define kN2PInOutErrorFailedModPrefsCmdKey	kN2PInOutStringPrefix  "kN2PInOutErrorFailedModPrefsCmdKey"


// Menu item positions:


#define kN2PInOutMenuItem1MenuItemPosition 2.0
// "Plug-ins" sub-menu item position for dialog:
#define kN2PInOutDialogMenuItemPosition	1.0
#define kN2PInOutDialogTitleKey		kN2PInOutStringPrefix "kN2PInOutDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kN2PInOutDialogMenuItemKey		kN2PInOutStringPrefix "kN2PInOutDialogMenuItemKey"

#define kN2PModRegisterUsersCmdNameKey		kN2PInOutStringPrefix "kN2PModRegisterUsersCmdNameKey"


#define kN2PInOutNoUsersLogedStringKey		kN2PInOutStringPrefix "kN2PInOutNoUsersLogedStringKey"
#define kN2PInOutNoDocumentoAbiertoStringKey		kN2PInOutStringPrefix "kN2PInOutNoDocumentoAbiertoStringKey"
#define kN2PInOutAUsersLogedStringKey		kN2PInOutStringPrefix "kN2PInOutAUsersLogedStringKey"
#define kN2PInOutAUserLogedStringKey		kN2PInOutStringPrefix "kN2PInOutAUserLogedStringKey"
#define kN2PInOutImplIdiomaStringKey		kN2PInOutStringPrefix "kN2PInOutImplIdiomaStringKey"
#define kN2PLogInOutPassErroneoStringKey		kN2PInOutStringPrefix "kN2PLogInOutPassErroneoStringKey"
#define kN2PLogInTaskKey		kN2PInOutStringPrefix "kN2PLogInTaskKey"


#define kN2PInOutYaExistePaginaConEsteFolioStringAlert		kN2PInOutStringPrefix "kN2PInOutYaExistePaginaConEsteFolioStringAlert"
#define kN2PInOutUserSinPriviPCheckInPageStringKey		kN2PInOutStringPrefix "kN2PInOutUserSinPriviPCheckInPageStringKey"
#define kN2PInOutUserSinPriviPCheckOutPageStringKey		kN2PInOutStringPrefix "kN2PInOutUserSinPriviPCheckOutPageStringKey"
#define kN2PLogInOutAnyUserLoggedStringKey		kN2PInOutStringPrefix "kN2PLogInOutAnyUserLoggedStringKey"
#define kN2PInOutLogInCmdStringKey		kN2PInOutStringPrefix "kN2PInOutLogInCmdStringKey"
#define kN2PInOutLogOutCmdStringKey		kN2PInOutStringPrefix "kN2PInOutLogOutCmdStringKey"
#define kN2PLogInOutNopudoCargarPreferenciasStringKey		kN2PInOutStringPrefix "kN2PLogInOutNopudoCargarPreferenciasStringKey"
#define kN2PLogInOutChnagesPlgStringKey		kN2PInOutStringPrefix "kN2PLogInOutChnagesPlgStringKey"


// Initial Data format IDs of the persistent data was first introduced in InDesign 1.5
// in the plug-in SimplePrefs
#define kN2PRegisterUsersInitialMajorFormat		kSDKDef_15_PersistMajorVersionNumber
#define kN2PRegisterUsersInitialMinorFormat		kSDKDef_15_PersistMinorVersionNumber

// Data format IDs used for SimplePrefs InDesign 1.0J. (No data format changes, but forced format version number change.)
#define kN2PRegisterUsers_1J_MajorFormat			kSDKDef_1J_PersistMajorVersionNumber
#define kN2PRegisterUsers_1J_MinorFormat			kSDKDef_1J_PersistMinorVersionNumber

// Note: no data format changes for InDesign 2.0, so no data format version number changes either.

// Format IDs for the PluginVersion resource 
#define kN2PRegisterUsersLastMajorFormatChange	kN2PRegisterUsers_1J_MajorFormat		// Last major format change for Custom Prefs
#define kN2PRegisterUsersLastMinorFormatChange	kN2PRegisterUsers_1J_MinorFormat		// Last minor format change for Custom Prefs


#define kN2P_CSRsrcID	128


// Initial data format version numbers
#define kN2PInOutFirstMajorFormatNumber  RezLong(1)
#define kN2PInOutFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PInOutCurrentMajorFormatNumber kN2PInOutFirstMajorFormatNumber
#define kN2PInOutCurrentMinorFormatNumber kN2PInOutFirstMinorFormatNumber

#endif // __N2PInOutID_h__

//  Code generated by DollyXs code generator
