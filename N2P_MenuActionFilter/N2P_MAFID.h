//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2P_MAFID_h__
#define __N2P_MAFID_h__

#include "SDKDef.h"

// Company:
#define kN2P_MAFCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2P_MAFCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2P_MAFPluginName	"N2PMenuActionFilter"			// Name of this plug-in.
#define kN2P_MAFPrefixNumber	0x96B00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2P_MAFVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kN2P_MAFAuthor		"Fernando Llanas"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2P_MAFPrefixNumber above to modify the prefix.)
#define kN2P_MAFPrefix		RezLong(kN2P_MAFPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2P_MAFStringPrefix	SDK_DEF_STRINGIZE(kN2P_MAFPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2P_MAFMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2P_MAFMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2P_MAFPluginID, kN2P_MAFPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2P_MAFActionComponentBoss, kN2P_MAFPrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2P_MAFActionFilterBoss, kN2P_MAFPrefix + 1)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kN2P_MAFBoss, kN2P_MAFPrefix + 25)


// InterfaceIDs:
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2P_MAFINTERFACE, kN2P_MAFPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2P_MAFActionComponentImpl, kN2P_MAFPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kN2P_MAFActionFilterImpl, kN2P_MAFPrefix + 1 )
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 2)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kN2P_MAFImpl, kN2P_MAFPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2P_MAFAboutActionID, kN2P_MAFPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kN2P_MAFActionID, kN2P_MAFPrefix + 25)


// WidgetIDs:
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kN2P_MAFWidgetID, kN2P_MAFPrefix + 25)


// "About Plug-ins" sub-menu:
#define kN2P_MAFAboutMenuKey			kN2P_MAFStringPrefix "kN2P_MAFAboutMenuKey"
#define kN2P_MAFAboutMenuPath		kSDKDefStandardAboutMenuPath kN2P_MAFCompanyKey

// "Plug-ins" sub-menu:
#define kN2P_MAFPluginsMenuKey 		kN2P_MAFStringPrefix "kN2P_MAFPluginsMenuKey"
#define kN2P_MAFPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2P_MAFCompanyKey kSDKDefDelimitMenuPath kN2P_MAFPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kN2P_MAFAboutBoxStringKey	kN2P_MAFStringPrefix "kN2P_MAFAboutBoxStringKey"
#define kN2P_MAFTargetMenuPath kN2P_MAFPluginsMenuPath

// Menu item positions:
#define kN2P_MAFNewActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFNewActionNameStringKey"
#define kN2P_MAFOpenActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFOpenActionNameStringKey"

#define kN2P_MAFCloseActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFCloseActionNameStringKey"
#define kN2P_MAFQuitActionNameStringKey		kN2P_MAFStringPrefix "kN2P_MAFQuitActionNameStringKey"
#define kN2P_MAFSaveAsActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFSaveAsActionNameStringKey"
#define kN2P_MAFSaveActionNameStringKey		kN2P_MAFStringPrefix "kN2P_MAFSaveActionNameStringKey"
#define kN2P_MAFSaveAsCopyActionNameStringKey		kN2P_MAFStringPrefix "kN2P_MAFSaveAsCopyActionNameStringKey"
#define kN2P_MAFPrintActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFPrintActionNameStringKey"
#define kN2P_MAFExportActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFExportActionNameStringKey"
#define kN2P_MAFNewActionNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFNewActionNameStringKey"
#define kN2P_MAFUpdateDesigNameStringKey	kN2P_MAFStringPrefix "kN2P_MAFUpdateDesigNameStringKey"

// Initial data format version numbers
#define kN2P_MAFFirstMajorFormatNumber  RezLong(1)
#define kN2P_MAFFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2P_MAFCurrentMajorFormatNumber kN2P_MAFFirstMajorFormatNumber
#define kN2P_MAFCurrentMinorFormatNumber kN2P_MAFFirstMinorFormatNumber

#endif // __N2P_MAFID_h__

//  Code generated by DollyXs code generator
