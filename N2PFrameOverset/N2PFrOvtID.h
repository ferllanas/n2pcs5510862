//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PFrOvtID_h__
#define __N2PFrOvtID_h__

#include "SDKDef.h"

// Company:
#define kN2PFrOvtCompanyKey	"Interlasa"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PFrOvtCompanyValue	"Interlasa"	// Company name displayed externally.

// Plug-in:
#define kN2PFrOvtPluginName	"N2PFrameOverset"			// Name of this plug-in.
#define kN2PFrOvtPrefixNumber	0xD5000 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PFrOvtVersion		"v1.0.8.4"						// Version of this plug-in (for the About Box).
#define kN2PFrOvtAuthor		"Interlasa.com S.A"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PFrOvtPrefixNumber above to modify the prefix.)
#define kN2PFrOvtPrefix		RezLong(kN2PFrOvtPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PFrOvtStringPrefix	SDK_DEF_STRINGIZE(kN2PFrOvtPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PFrOvtMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PFrOvtMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PFrOvtPluginID, kN2PFrOvtPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2PFrOvtActionComponentBoss, kN2PFrOvtPrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2PSQLSeleccionWidgetBoss, kN2PFrOvtPrefix + 3)
DECLARE_PMID(kClassIDSpace, kMyMovePageItemTrackerBoss, kN2PFrOvtPrefix + 4)
DECLARE_PMID(kClassIDSpace, kMyResizeTrackerBoss, kN2PFrOvtPrefix + 5)
DECLARE_PMID(kClassIDSpace, kN2PCTUtilitiesBoss, kN2PFrOvtPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kN2PFrOvtBoss, kN2PFrOvtPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PTCUtilities, kN2PFrOvtPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PFROVTINTERFACE, kN2PFrOvtPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtActionComponentImpl, kN2PFrOvtPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kN2PCTSelectionObserverImpl, kN2PFrOvtPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kMyMovePageItemTrackerImpl, kN2PFrOvtPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kResizeTrackerImpl, kN2PFrOvtPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kN2PCTUtilitiesImpl, kN2PFrOvtPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kN2PFrOvtImpl, kN2PFrOvtPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2PFrOvtAboutActionID, kN2PFrOvtPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kN2PFrOvtActionID, kN2PFrOvtPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kA2PExpButtonSeleccionWidgetID, kN2PFrOvtPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kN2PFrOvtWidgetID, kN2PFrOvtPrefix + 25)


// "About Plug-ins" sub-menu:
#define kN2PFrOvtAboutMenuKey			kN2PFrOvtStringPrefix "kN2PFrOvtAboutMenuKey"
#define kN2PFrOvtAboutMenuPath		kSDKDefStandardAboutMenuPath kN2PFrOvtCompanyKey

// "Plug-ins" sub-menu:
#define kN2PFrOvtPluginsMenuKey 		kN2PFrOvtStringPrefix "kN2PFrOvtPluginsMenuKey"
#define kN2PFrOvtPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PFrOvtCompanyKey kSDKDefDelimitMenuPath kN2PFrOvtPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kN2PFrOvtAboutBoxStringKey	kN2PFrOvtStringPrefix "kN2PFrOvtAboutBoxStringKey"
#define kN2PFrOvtTargetMenuPath kN2PFrOvtPluginsMenuPath

// Menu item positions:


// Initial data format version numbers
#define kN2PFrOvtFirstMajorFormatNumber  RezLong(1)
#define kN2PFrOvtFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PFrOvtCurrentMajorFormatNumber kN2PFrOvtFirstMajorFormatNumber
#define kN2PFrOvtCurrentMinorFormatNumber kN2PFrOvtFirstMinorFormatNumber

#endif // __N2PFrOvtID_h__

//  Code generated by DollyXs code generator
