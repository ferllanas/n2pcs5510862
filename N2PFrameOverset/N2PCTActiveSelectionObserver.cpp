//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/basicpersistinterface/N2PCTSelectionObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #9 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ICounterControlData.h"
#include "IPanelControlData.h"
#include "IGraphicFrameData.h"
#include "ITrackerRegister.h"
#include "ITrackerFactory.h"
#include "ITracker.h"
#include "ISession.h"
#include "ITextFrameColumn.h"
#include "IEventHandler.h"
#include "IControlView.h"
#include "IApplication.h"
#include "IEventDispatcher.h"
#include "IPanorama.h"
#include "IEventConverter.h"
#include "IWindow.h"
#include "IWidgetParent.h"
#include "IColumns.h"
#include "IWindowPort.h"
#include "IDocument.h"
#include "IKeyboard.h"
#include "IPageItemFocus.h"
#include "IItemContext.h"
#include "IRulerData.h"
#include "ITrackerTimer.h"
#include "ISelectionUtils.h"
#include "IDFactory.h"

//definictions IDs
#include "LayoutUIID.h"
#include "ZoomToolID.h"
#include "ImageID.h"
#include "SplineID.h"
#include "LayoutID.h"
#include "SpreadID.h"
#include "SplineUIID.h"

// General includes
#include "SelectionObserver.h"
#include "ILayoutUIUtils.h"
#include "EventUtilities.h"
#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "TransformUtils.h"
#include "AGMGraphicsContext.h"
#include "CrossPlatformTypes.h"
#include "CTracker.h"
#include "CAlert.h"

// Project includes
#include "N2PFrOvtID.h"



// ----- Implementation Includes -----




//#include "IN2PCTUtilities.h"




/** Implements a selection observer, IObserver based on ActiveSelectionObserver,
	that updates the user interface when the selection changes by calling
	IA2PProSuite to access the IA2PProData associated with the selection.

	When the active selection changes (from
	text selection to layout selection for example), or when an object is added or removed
	from the selection or when an attribute of selected objects change this
	observer gets called. It then determines if it needs to update the data in its
	widgets to reflect the IA2PProData associated with the selection.

	@ingroup basicpersistinterface
	
*/
class N2PCTSelectionObserver : public ActiveSelectionObserver
{
	public:

		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PCTSelectionObserver(IPMUnknown* boss);

		/**
			Destructor.
		*/
		virtual ~N2PCTSelectionObserver();
	

	protected:

		/**
 			When an object is added or removed from the selection or if the kind
			of selection changes (e.g. from a a layout selection to a text selection)
			this method is called which results in N2PCTSelectionObserver::UpdatePanel
			being called.
			@param message from selection describing change or nil if the active selection changed.
		*/
		virtual void HandleSelectionChanged(const ISelectionMessage* message);

		/**
 			If the selection's data changes this method gets called. We check
			that data we are interested in is affected before taking any action
			by calling ISelectionMessage::WasSuiteAffected and checking that
			a message from IA2PProSuite is present. If so we update the data in the
			panel's widgets to reflect that of the selection.
			@param message from selection indicating the attribute(s) affected
		*/
		virtual void HandleSelectionAttributeChanged (const ISelectionMessage* message);

		/**
			Update the panel to reflect selection's IA2PProSuite data.
		*/
		void		UpdatePanel();
		
	private:
	
		UID getUIDRefOfSelectionObserver();
		
};

/*
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PCTSelectionObserver, kN2PCTSelectionObserverImpl)

/* Constructor
*/
N2PCTSelectionObserver::N2PCTSelectionObserver(IPMUnknown* boss) :
	ActiveSelectionObserver(boss)
{
}

/* Destructor
*/
N2PCTSelectionObserver::~N2PCTSelectionObserver()
{
}

/*
*/
void N2PCTSelectionObserver::HandleSelectionChanged(const ISelectionMessage* message)
{
	this->UpdatePanel();
	//N2PExpPnlTrvUtils::RefreshTree();

}

/*
*/
void N2PCTSelectionObserver::HandleSelectionAttributeChanged( const ISelectionMessage* iSelectionMsg)
{
	// It is very important that you filter this call for a suite PMIID.
	// Your active selection observer is called when any attribute of the
	// selection changes, so you only want to update widgets when a change
	// that is of interest occurs.

	///if (iSelectionMsg->WasSuiteAffected(IA2PProSuite::kDefaultIID)) 
	//{
		// Always call ISelectionMessage::WasSuiteAffected before 
		// taking any action. Otherwise your implementation is inefficient.
	///	this->UpdatePanel();
	///}
}

/*
*/
void N2PCTSelectionObserver::UpdatePanel()
{
	do 
	{
		/*InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
					(
						kN2PCTUtilitiesBoss,	// Object boss/class
						IN2PCTUtilities::kDefaultIID
					)));
					
		int32 NumTxtFmOverset=N2PCTUtils->LengthXMP("N2PTxtFmeOversetArray");
 		StructOfTextFrameConOverset *ListTextFrOrSt = new StructOfTextFrameConOverset[NumTxtFmOverset]; 
		N2PCTUtils->ObtenerTextFrameOrStOnXMP(ListTextFrOrSt,NumTxtFmOverset);
 		
 		bool16 EsUnOversetFrame=kFalse;
 		
 		UID UIDFrameSelected = this->getUIDRefOfSelectionObserver();
 		int32 intUIFrame = UIDFrameSelected.Get();
 		intUIFrame=intUIFrame-1;
 		for(int32 i=0 ;i<NumTxtFmOverset && EsUnOversetFrame==kFalse;i++)
 		{
 			if(ListTextFrOrSt[i].UIDTextFrameOrSt==intUIFrame)
 			{
 				EsUnOversetFrame=kTrue;
 			}
 		}
 
 		if(EsUnOversetFrame==kTrue)
 		{
 			//Es un TextFrame conOverset
 			InterfacePtr<ITrackerFactory> factory1(gSession, UseDefaultIID()); 
			if (factory1) 
			{ 
		
				 InterfacePtr<ITracker> registeredTrackerResizeSpline(factory1->QueryTracker(kSplineItemBoss, kResizeToolBoss)); 		
	 			if (registeredTrackerResizeSpline) 
 				{	 
 					factory1->RemoveTracker(kSplineItemBoss, kResizeToolBoss); 
 					factory1->InstallTracker(kSplineItemBoss, kResizeToolBoss, kMyResizeTrackerBoss); 

 					

				 }
				 
				 
			}
 		}
 		else
 		{
 			
 			InterfacePtr<ITrackerFactory> factory1(gSession, UseDefaultIID()); 
			if (factory1) 
			{ 
		
				InterfacePtr<ITracker> registeredTracker(factory1->QueryTracker(kSplineItemBoss, kResizeToolBoss)); 		
	 			if (registeredTracker) 
 				{	 
 					factory1->RemoveTracker(kSplineItemBoss, kResizeToolBoss); 
 					factory1->InstallTracker(kSplineItemBoss, kResizeToolBoss, kBBoxResizeTrackerBoss); 

 					//factory1->RemoveTracker(kLayoutWidgetBoss, kZoomToolBoss); 
 					//factory1->InstallTracker(kLayoutWidgetBoss, kZoomToolBoss, kZoomToolTrackerBoss); 

				 }
			}
 		}
			
			*/
			
		//}
		
	//	CAlert::InformationAlert("");
	/*	else
		{
			InterfacePtr<ITrackerFactory> factory1(gSession, UseDefaultIID()); 
			if (factory1) 
			{ 
		
				InterfacePtr<ITracker> registeredTracker(factory1->QueryTracker(kSplineItemBoss, kResizeToolBoss)); 		
	 			if (registeredTracker) 
 				{	 
 					factory1->RemoveTracker(kSplineItemBoss, kResizeToolBoss); 
 					factory1->InstallTracker(kSplineItemBoss, kResizeToolBoss, kBBoxResizeTrackerBoss); 

 					//factory1->RemoveTracker(kLayoutWidgetBoss, kZoomToolBoss); 
 					//factory1->InstallTracker(kLayoutWidgetBoss, kZoomToolBoss, kZoomToolTrackerBoss); 

				 }
			}
		}

		*/
	}while (false);
	
}


UID N2PCTSelectionObserver::getUIDRefOfSelectionObserver()
{
	UID result = kInvalidUID;
	do
	{		
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			
			
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			Utils<ISelectionUtils> iSelectionUtils;
			if (iSelectionUtils == nil) 
			{
				break;
			}

		
			ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
			if(iSelectionManager==nil)
			{
				break;
			}

			//	Para textFrame 
			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
			if(pTextSel==nil)
			{
				ASSERT_FAIL("pTextSel");
				break;
			}


			UIDRef selectedFrameRef = UIDRef::gNull;
			UID selectedFrameItem = kInvalidUID;
			InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
			if (pLayoutTarget)
			{
				UIDList itemList = pLayoutTarget->GetUIDList(kStripStandoffs);
				if (itemList.Length() == 1) 
				{
						// get the database
						db = itemList.GetDataBase();
						// get the UID of the selected item
						selectedFrameItem = itemList.At(0);

						selectedFrameRef=itemList.GetRef(0);
				}
			}
			else
			{
				ASSERT_FAIL("False");
				break;
			}

			InterfacePtr<IGraphicFrameData> graphicFrameDataFlow(selectedFrameRef, UseDefaultIID());
			if (!graphicFrameDataFlow) 
			{
				break;
			}

			result = graphicFrameDataFlow->GetTextContentUID();
			
	}	while(false);
	return(result);
}

// End, N2PCTSelectionObserver.cpp.



