/*
//
//	File:	UpdateStorysAndDBUtilis.h
//
//
//	Date:	6-Mar-20010
//  
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#ifndef __IN2PCTUtilities_h__
#define __IN2PCTUtilities_h__

#include"IPMUnknown.h"
#include"IDocument.h"
#include"ITextModel.h"


#include "N2PFrOvtID.h"
#include "UpdateStorysAndDBUtilis.h"

#ifdef WINDOWS
	#include"..\N2PFrameOverset\N2PFrOvtID.h"
	#include"..\N2PSQL\UpdateStorysAndDBUtilis.h"
#endif



//Strructuras que guarda las preferencias de la coneccion a la base de datos

struct StructOfTextFrameConOverset
{
	int32 UIDTextModel;		//UID tel textmodel que tiene el texto sobrante
	int32 UIDTextFrameOrSt; //UID del textframe el texto sobrante
};
/**
	Encapsulates detail of working with the list-box API. 
	@author Ian Paterson
*/
class IN2PCTUtilities:public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IN2PTCUtilities };
		
	//virtual void MakeUpdate()=0;

	virtual bool16 MuestraNotasConFrameOverset(StructIdFramesElementosXIdNota* VectorNota,const int32 TotNotas)=0;
	
	virtual bool16 EsStoryOverset(InterfacePtr<ITextModel> textModel)=0;
	
	virtual  bool16 OcultaNotasConFrameOverset()=0;
	
	virtual bool16 ObtenerTextFrameOrStOnXMP(StructOfTextFrameConOverset *ListTextFrOrSt,int32 CantidadDeTextFrameOverset)=0;
	
	virtual int LengthXMP(PMString PathStruct)=0;
	
	virtual bool16 CanFlowTextOversetOnNewFrame( UID UIDTextModelofElementoNota, UIDRef& FrameWhitOversetText )=0;
 
 	virtual bool16 DeleteXMP(PMString Path)=0;
 
 	virtual bool16 GuardarTextFrameOrStOnXMP(StructOfTextFrameConOverset *ListTextFrOrSt,int32 CantidadDeTextFrameOverset)=0;
 
	virtual bool16 GetTamanoInterlineado(InterfacePtr<IFrameList> frameList,PMReal& LeadingOfLasParcel)=0;
	
	virtual ErrorCode EstimateTextDepth(const InterfacePtr<ITextParcelList>& textParcelList, 
										ParcelKey fromParcelKey, 
										int32 numberOfParcels, 
										PMReal& estimatedDepth,
										int32& NumLinesToOverset,
										int32& UltimoCaracterMostrado
										)=0;

	virtual ErrorCode EstimateStoryDepth(const InterfacePtr<ITextModel>& textModel, 
										PMReal& estimatedDepth, 
										int32& NumLinesToOverset
										)=0;
										
										
	virtual bool16 GETStoryParams_OfUIDTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel)=0;
 												
 	virtual ErrorCode CountWordsAndLines(UID UIDTextModelofElementoNota, const InterfacePtr<ITextParcelList>& textParcelList, ParcelKey fromParcelKey, 
											int32 numberOfParcels, PMReal& estimatedDepth,
											int32& NumeroDeLineaAntesDelOverset,int32& UltimoCaracterMostrado)=0;
};


#endif // __IN2PCTUtilities_h__

