/*
 *  N2PWSDLCltUtils.cpp
 *  N2PWSDLClient
 *
 *  Created by ADMINISTRADOR on 25/04/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "N2PwsdlcltID.h"
#include "N2PWSDLCltUtils.h"

#include "CAlert.h"
#include "webservices/N2PLocalwsdlBinding.nsmap"
#include "webservices/soapN2PLocalwsdlBindingProxy.h"


class N2PWSDLCltUtils : public CPMUnknown<IN2PWSDLCltUtils>
{
public:
	/** Constructor.
	 param boss boss object on which this interface is aggregated.
	 */
	N2PWSDLCltUtils (IPMUnknown *boss);
	
	const bool16 realizaConneccion(PMString usuario,
							 PMString ip, 
							 PMString cliente,
							 PMString aplicacion, 
							 PMString URL);
	const bool16 realizaDesconeccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL);

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
 */
CREATE_PMINTERFACE(N2PWSDLCltUtils, kN2PwsdlcltUtilsImpl)


/* HelloWorld Constructor
 */
N2PWSDLCltUtils::N2PWSDLCltUtils(IPMUnknown* boss) 
: CPMUnknown<IN2PWSDLCltUtils>(boss)
{
	// do nothing.
	
	/* NOTE: 
	 There used to be code that would set the preference value by default.
	 That was removed in favor of the workspace responders.
	 
	 The responders get called, and they will try to setup the value
	 for this custom preference.  It is one of the responders that will
	 set the correct initial value for the cached preference, fPrefState.
	 
	 If the global workspace is already created from a previous 
	 application session, the ReadWrite() method will set the 
	 cached preference, fPrefState. The same ReadWrite() will be called when 
	 an existing document is opened.
	 
	 Refer to CstPrfNewWSResponder.cpp and N2PSQLUtils.cpp
	 for details on how to set the workspace defaults. 
	 */
}



const bool16 N2PWSDLCltUtils::realizaConneccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL)
{
	bool16 retval=kFalse;
	ns1__STConnection STConnectionZZ;
	ns1__ValidaConnectionResponse param_1;
	
	STConnectionZZ.username = usuario.GrabCString();
	STConnectionZZ.ip = ip.GrabCString();
	STConnectionZZ.cliente = cliente.GrabCString();
	STConnectionZZ.aplicacion = aplicacion.GrabCString();
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.soap_endpoint = URL.GrabCString();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.ValidaConnection(&STConnectionZZ,param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		CAlert::InformationAlert(N2PLocal.soap_fault_string());
	}
	else
	{
		PMString  XCS="";
		XCS.Append(param_1.return_->errorstring.c_str());//;
		//CAlert::InformationAlert(XCS);

		if(XCS=="OK")
		{
			retval=kTrue;
		
		}
		else
			CAlert::InformationAlert(XCS);
	}
	return retval;
}

const bool16 N2PWSDLCltUtils::realizaDesconeccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL)
{
	bool16 retval=kFalse;
	ns1__STConnection STConnectionZZ;
	ns1__disconnectResponse param_1;
	
	STConnectionZZ.username = usuario.GrabCString();
	STConnectionZZ.ip = ip.GrabCString();
	STConnectionZZ.cliente = cliente.GrabCString();
	STConnectionZZ.aplicacion = aplicacion.GrabCString();
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.soap_endpoint = URL.GrabCString();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.disconnect(&STConnectionZZ, param_1);
	//.ValidaConnection(&STConnectionZZ,param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		CAlert::InformationAlert(N2PLocal.soap_fault_string());
	}
	else
	{
		PMString  XCS="";
		XCS.Append(param_1.return_->errorstring.c_str());//;
		//CAlert::InformationAlert(XCS);
		
		if(XCS=="OK")
		{
			retval=kTrue;
			
		}
		else
			CAlert::InformationAlert(XCS);
	}
	return retval;
}



