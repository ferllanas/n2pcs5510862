//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PwsdlcltID_h__
#define __N2PwsdlcltID_h__

#include "SDKDef.h"

// Company:
#define kN2PwsdlcltCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PwsdlcltCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2PwsdlcltPluginName	"N2PWSDLClient"			// Name of this plug-in.
#define kN2PwsdlcltPrefixNumber	0x159e00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PwsdlcltVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kN2PwsdlcltAuthor		"Fernando Llanas"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PwsdlcltPrefixNumber above to modify the prefix.)
#define kN2PwsdlcltPrefix		RezLong(kN2PwsdlcltPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PwsdlcltStringPrefix	SDK_DEF_STRINGIZE(kN2PwsdlcltPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PwsdlcltMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PwsdlcltMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PwsdlcltPluginID, kN2PwsdlcltPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2PwsdlcltActionComponentBoss, kN2PwsdlcltPrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2PwsdlcltUtilsBoss, kN2PwsdlcltPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kN2PwsdlcltBoss, kN2PwsdlcltPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTUTILSINTERFACE, kN2PwsdlcltPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PWSDLCLTINTERFACE, kN2PwsdlcltPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltActionComponentImpl, kN2PwsdlcltPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltUtilsImpl, kN2PwsdlcltPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 2)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kN2PwsdlcltImpl, kN2PwsdlcltPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2PwsdlcltAboutActionID, kN2PwsdlcltPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kN2PwsdlcltActionID, kN2PwsdlcltPrefix + 25)


// WidgetIDs:
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kN2PwsdlcltWidgetID, kN2PwsdlcltPrefix + 25)


// "About Plug-ins" sub-menu:
#define kN2PwsdlcltAboutMenuKey			kN2PwsdlcltStringPrefix "kN2PwsdlcltAboutMenuKey"
#define kN2PwsdlcltAboutMenuPath		kSDKDefStandardAboutMenuPath kN2PwsdlcltCompanyKey

// "Plug-ins" sub-menu:
#define kN2PwsdlcltPluginsMenuKey 		kN2PwsdlcltStringPrefix "kN2PwsdlcltPluginsMenuKey"
#define kN2PwsdlcltPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PwsdlcltCompanyKey kSDKDefDelimitMenuPath kN2PwsdlcltPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kN2PwsdlcltAboutBoxStringKey	kN2PwsdlcltStringPrefix "kN2PwsdlcltAboutBoxStringKey"
#define kN2PwsdlcltTargetMenuPath kN2PwsdlcltPluginsMenuPath

// Menu item positions:

// Initial data format version numbers
#define kN2PwsdlcltFirstMajorFormatNumber  RezLong(1)
#define kN2PwsdlcltFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PwsdlcltCurrentMajorFormatNumber kN2PwsdlcltFirstMajorFormatNumber
#define kN2PwsdlcltCurrentMinorFormatNumber kN2PwsdlcltFirstMinorFormatNumber

#endif // __N2PwsdlcltID_h__

//  Code generated by DollyXs code generator
